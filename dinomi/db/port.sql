-- call_center_pro.crm_sms_gateway_port definition

CREATE TABLE `crm_sms_gateway_port` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_gateway` bigint(20) DEFAULT NULL,
  `port` varchar(100) DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `crm_sms_gateway_port_FK` (`id_gateway`),
  CONSTRAINT `crm_sms_gateway_port_FK` FOREIGN KEY (`id_gateway`) REFERENCES `crm_sms_gateway` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;

INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (1,'1','A'),
	 (1,'2','A'),
	 (1,'3','A'),
	 (1,'4','A'),
	 (1,'5','A'),
	 (1,'6','A'),
	 (1,'7','A'),
	 (1,'8','A'),
	 (1,'9','A'),
	 (1,'10','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (1,'11','A'),
	 (1,'12','A'),
	 (1,'13','A'),
	 (1,'14','A'),
	 (1,'15','A'),
	 (1,'16','A'),
	 (1,'17','A'),
	 (1,'18','A'),
	 (1,'19','A'),
	 (1,'20','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (1,'21','A'),
	 (1,'22','A'),
	 (1,'23','A'),
	 (1,'24','A'),
	 (1,'25','A'),
	 (1,'26','A'),
	 (1,'27','A'),
	 (1,'28','A'),
	 (1,'29','A'),
	 (1,'30','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (1,'31','A'),
	 (1,'32','A'),
	 (2,'1','A'),
	 (2,'2','A'),
	 (2,'3','A'),
	 (2,'4','A'),
	 (2,'5','A'),
	 (2,'6','A'),
	 (2,'7','A'),
	 (2,'8','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (2,'9','A'),
	 (2,'10','A'),
	 (2,'11','A'),
	 (2,'12','A'),
	 (2,'13','A'),
	 (2,'14','A'),
	 (2,'15','A'),
	 (2,'16','A'),
	 (2,'17','A'),
	 (2,'18','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (2,'19','A'),
	 (2,'20','A'),
	 (2,'21','A'),
	 (2,'22','A'),
	 (2,'23','A'),
	 (2,'24','A'),
	 (2,'25','A'),
	 (2,'26','A'),
	 (2,'27','A'),
	 (2,'28','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (2,'29','A'),
	 (2,'30','A'),
	 (2,'31','A'),
	 (2,'32','A'),
	 (3,'1','A'),
	 (3,'2','A'),
	 (3,'3','A'),
	 (3,'4','A'),
	 (3,'5','A'),
	 (3,'6','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (3,'7','A'),
	 (3,'8','A'),
	 (3,'9','A'),
	 (3,'10','A'),
	 (3,'11','A'),
	 (3,'12','A'),
	 (3,'13','A'),
	 (3,'14','A'),
	 (3,'15','A'),
	 (3,'16','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (3,'17','A'),
	 (3,'18','A'),
	 (3,'19','A'),
	 (3,'20','A'),
	 (3,'21','A'),
	 (3,'22','A'),
	 (3,'23','A'),
	 (3,'24','A'),
	 (3,'25','A'),
	 (3,'26','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (3,'27','A'),
	 (3,'28','A'),
	 (3,'29','A'),
	 (3,'30','A'),
	 (3,'31','A'),
	 (3,'32','A'),
	 (4,'1','A'),
	 (4,'2','A'),
	 (4,'3','A'),
	 (4,'4','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (4,'5','A'),
	 (4,'6','A'),
	 (4,'7','A'),
	 (4,'8','A'),
	 (4,'9','A'),
	 (4,'10','A'),
	 (4,'11','A'),
	 (4,'12','A'),
	 (4,'13','A'),
	 (4,'14','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (4,'15','A'),
	 (4,'16','A'),
	 (4,'17','A'),
	 (4,'18','A'),
	 (4,'19','A'),
	 (4,'20','A'),
	 (4,'21','A'),
	 (4,'22','A'),
	 (4,'23','A'),
	 (4,'24','A');
INSERT INTO crm_sms_gateway_port (id_gateway,port,estado) VALUES
	 (4,'25','A'),
	 (4,'26','A'),
	 (4,'27','A'),
	 (4,'28','A'),
	 (4,'29','A'),
	 (4,'30','A'),
	 (4,'31','A'),
	 (4,'32','A');
