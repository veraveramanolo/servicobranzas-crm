<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With,Authorization");
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day

date_default_timezone_set('America/Guayaquil');
chdir('/var/www/html');
include_once "/var/www/html/libs/JSON.php";
require_once ("/var/www/html/libs/ccpro_ECCP.class.php");
include_once "/var/www/html/libs/paloSantoDB.class.php";
require_once "modules/ccpro_campaign_out/libs/paloSantoCampaignCC.class.php";

$params = array();
$pos = strpos($_SERVER['REQUEST_URI'], '?');
if (!$pos) $pos = strlen($_SERVER['REQUEST_URI']);
$request = substr($_SERVER['REQUEST_URI'], 0, $pos);
$parts = explode('/', $request);

switch ($parts[4])
{
    case "param_manualcall_ws": // 01 metodo para llamada Click2Call
        $trama_param_manualcall_ws = $_POST['trama_param_manualcall_ws'];
        param_manualcall_ws($trama_param_manualcall_ws);
    break;

    case "agentlogin": // metodo para llamar  login
        $trama_login = $_POST['trama_login_ws'];
        agentlogin($trama_login);

    break;

    case "agentlogout": // metodo para llamar  logout
        $trama_logout = $_POST['trama_logout_ws'];
        agentlogin($trama_logout);
    break;

    case "param_report":
        $param_report = $_POST['trama_param_report_ws'];
        param_reporte($param_report);
    break;

    default:
        $return = array(
            "codigo" => "0",
            "mensaje" => "Metodo no existe"
        );
        retornar($return);
    break;

}

function agentlogout($trama_logout)
{
    $arr = parse_ini_file('/etc/elastix.conf');
    $cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro_2";

    $pDB = new paloDB($cadena_dsn);
    $json = new Services_JSON();
    $call_data = $json->decode($trama_logout);
    $call_data = arrayCastRecursive($call_data);
    $agentname = $call_data['agentname'];
    $agentpass = $call_data['agentpassword'];

    $filterbyagent = false;

    $x = new ECCP();
    try
    {
        print "Connect...\n";
        $cr = $x->connect("localhost", "agentconsole", "agentconsole");
        if (isset($cr->failure)) die('Failed to connect to ECCP - ' . $cr
            ->failure->message . "\n");
        $x->setAgentNumber($agentname);
        if (getenv('ECCP_AUTHMODE') == 'agentnick') $x->useAgentNick(true);
        $x->setAgentPass($agentpass);
        print_r($x->logoutagent());
        print "Disconnect...\n";
        $x->disconnect();
    }
    catch(Exception $e)
    {
        print_r($e);
        print_r($x->getParseError());
    }

}

// login
function agentlogin($trama_login)
{

    $arr = parse_ini_file('/etc/elastix.conf');
    $cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro_2";

    $pDB = new paloDB($cadena_dsn);
    $json = new Services_JSON();
    $call_data = $json->decode($trama_login);
    $call_data = arrayCastRecursive($call_data);
    $agentname = $call_data['agentname'];
    $agentpass = $call_data['agentpassword'];

    //	print_r($call_data);
    $filterbyagent = false;

    /////////////////////////
    $x = new ECCP();
    try
    {
        //print "Connect...\n";
        

        $cr = $x->connect("localhost", "agentconsole", "agentconsole"); //
        if (isset($cr->failure)) die('Failed to connect to ECCP - ' . $cr
            ->failure->message . "\n");
        $x->setAgentNumber($agentname);

        if (getenv('ECCP_AUTHMODE') == 'agentnick') $x->useAgentNick(true);
        $x->setAgentPass($agentpass);
        $x->getAgentStatus(); //	print_r();
        //print "Login agent\n";
        $r = $x->loginagent($call_data['extension']);
        //	print_r($r);
        $bFalloLogin = false;
        if (!isset($r->failure) && !isset($r
            ->loginagent_response
            ->failure)) $return = (array(
            "codigo" => "1",
            "mensaje" => "Conexion Exitosa"
        ));

        print (json_encode($return));
        /*	while (!$bFalloLogin) {
        
        
        $x->wait_response(1);
        
        print_r($e);
        foreach ($e->children() as $ee) $evt = $ee;
        if ($evt->getName() == 'agentfailedlogin') {
        
        $bFalloLogin = TRUE;
        
        break;
        } elseif ($evt->getName() == 'agentloggedin') {
        
        
        if ($filterbyagent)
        
        $x->filterbyagent();	// print_r($x->filterbyagent());
        
        
        
        } elseif ($evt->getName() == 'agentloggedout') {
        
        if ($filterbyagent)
        $x->removefilterbyagent();
        
        
        }
        
        }*/
        //print "Disconnect...\n";
        //$return = array("codigo"=>"0","mensaje"=>"Error en la conexion");
        retornar($return);
        $x->disconnect();
    }
    catch(Exception $e)
    {
        //	print_r($e);
        $x->getParseError();
        $return = array(
            "codigo" => "0",
            "mensaje" => "Error en la conexion"
        );
        retornar($return);

    }

}

// click2call
function param_manualcall_ws($trama_param_manualcall_ws)
{

    if (empty($trama_param_manualcall_ws))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "Json es obligatorio"
        );
        retornar($return);
    }
    require_once "/var/www/html/libs/ccpro_ECCP.class.php";
    require_once "/var/www/html/libs/callCenterProUtils.class.php";
    require_once "modules/ccpro_campaign_manual/libs/paloContactInsert.class.php"; //Version previa RMA
    //require_once "modules/ccpro_campaign_manual/libs/ccproContactInsert_ManualDialing.class.php";//VERSION NUEVA COMENTAR
    require_once "modules/ccpro_campaign_manual/libs/paloSantoCampaignManual.class.php";
    $arr = parse_ini_file('/etc/elastix.conf');
    $cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro_2";
    $pDB = new paloDB($cadena_dsn);
    $json = new Services_JSON();
    $call_data = $json->decode($trama_param_manualcall_ws);
    $call_data = arrayCastRecursive($call_data);
    $id_manual_campaign = $call_data['id_manual_campaign'];
    $agenttype = strtolower(trim($call_data['agenttype']));
    if ($agenttype == 'agent')
    {
        $agenttype = ucfirst($agenttype);
    }
    else if ($agenttype == 'sip')
    {
        $agenttype = strtoupper($agenttype);
    }
    $agentnumber = trim($call_data['agentnumber']);
    $phone = trim($call_data['phone']);

    if (empty($id_manual_campaign))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "ID Campania Manual es obligatorio"
        );
        retornar($return);
    }
    else
    {
        $qCampania = "SELECT * FROM call_center_pro_2.manualdialing_campaign WHERE estatus = 'A' and date(datetime_init)<= date(now()) and date(datetime_end) >= date(now()) and id = ?";
        $rCampania = $pDB->getFirstRowQuery($qCampania, true, array(
            $id_manual_campaign
        ));
        if (empty($rCampania['id']))
        {
            $return = array(
                "codigo" => "0",
                "mensaje" => "Error en la Campania Manual con id: $id_manual_campaign . Compruebe que este creada, que este ACTIVA o dentro de los rangos de fecha de hoy."
            );
            retornar($return);
        }
    }

    if (($agenttype != 'Agent') && ($agenttype != 'SIP'))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "agenttype es incorrecto: " . $agenttype . ". Ingrese 'SIP' o 'Agent'."
        );
        retornar($return);
    }
    if (empty($agentnumber))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "Numero de agente esta vacio: " . $agentnumber
        );
        retornar($return);
    }
    if (empty($phone))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "Numero de telefono vacio, ingrese uno correcto: " . $phone
        );
        retornar($return);
    }

    $agentname = $agenttype . "/" . $agentnumber;
    $agent = getPassword($pDB, $agentname);

    if (empty($agent))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "No hay password para agente a consultar, verifique agent_voicechannel.datetime_end que sea nulo: " . $agentname
        );
        retornar($return);
    }
    else
    {
        $agentpass = $agent[0];
        $idAgent = $agent[1];
        $call_data['agentpass'] = $agentpass;
        $call_data['idAgent'] = $idAgent;
    }

    $x = new ECCP();
    $agentStatus = "";

    try
    {
        $cr = $x->connect("localhost", "agentconsole", "agentconsole");
        if (isset($cr->failure)) die('Failed to connect to ECCP - ' . $cr
            ->failure->message . "\n");
        $x->setAgentNumber($agentname);
        $agentStatus = (array)$x->getAgentStatus();
        //print_r($agentStatus);
        $x->disconnect();
    }
    catch(Exception $e)
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => $e
        );
        retornar($return);
    }

    if ($agentStatus["status"] != "online")
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "El agente no esta online, el estado actual es: " . $agentStatus["status"]
        );
        retornar($return);

    }

    $campaign = verificaCampania($pDB, $id_manual_campaign, $idAgent);
    $oCamp = new paloSantoCampaignManual($pDB);
    $oCamp->refrescarCampaniasDialer();
    $contacto = generaContacto($pDB, $phone, $campaign);
    $contact = $contacto[0];
    $dialnum = $contacto[1];

    $call_data['contact'] = $contact;
    $call_data['dialnum'] = $dialnum;

    //print_r($call_data);
    try
    {
        $cr = $x->connect("localhost", "agentconsole", "agentconsole");
        if (isset($cr->failure)) die('Failed to connect to ECCP - ' . $cr
            ->failure->message . "\n");
        $x->setAgentNumber($agentname);
        if (getenv('ECCP_AUTHMODE') == 'agentnick') $x->useAgentNick(true);
        $x->setAgentPass($agentpass);
        $x->manualdialingcall($campaign, $contact, $dialnum);
        $x->disconnect();
        //print_r($x);
        

        $return = array(
            "codigo" => "1",
            "mensaje" => "status dialing...",
            "contacto" => $contact
        );

        retornar($return);
    }
    catch(Exception $e)
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => $e
        );
        retornar($return);
    }
}

function getPassword($pDB, $agentname)
{
    $arrAgent = explode("/", $agentname);
    $qAgent = "SELECT agent.eccp_password,
	                    agent.id
				 FROM   agent,
				        agent_voicechannel 
                 WHERE  agent.id = agent_voicechannel.id_agent
				 AND    agent_voicechannel.datetime_end is null
				 AND    agent_voicechannel.type = ?
				 AND    agent_voicechannel.number = ?";

    $return = $pDB->getFirstRowQuery($qAgent, false, array(
        $arrAgent[0],
        $arrAgent[1]
    ));
    //print_r($agentname);
    return $return;
}

function verificaCampania($pDB, $id_manual_campaign, $idAgent)
{
    //Se asigna agente a campania
    $qCampania = "SELECT * FROM manualdialing_campaign_agent WHERE id_campaign = ? and id_agent = ?";
    $rCampania = $pDB->getFirstRowQuery($qCampania, true, array(
        $id_manual_campaign,
        $idAgent
    ));
    if (!empty($rCampania))
    {
        return $rCampania['id_campaign'];
    }
    $pDB->genQuery("INSERT INTO manualdialing_campaign_agent VALUES($id_manual_campaign ,$idAgent)");
    return $id_manual_campaign;
}

function generaContacto($pDB, $phone, $campaign)
{
    $oInsert = new paloContactInsert($pDB, $campaign); //Version Previa DESCOMENTAR RMA
    //$oInsert        = new ccproContactInsert_ManualDialing($pDB,$campaign);//VERSION NUEVA COMENTAR
    $numero = array(
        $phone
    );
    $campos[] = array(
        '0' => 'telefono',
        '1' => $phone
    );
    $campos[] = array(
        '0' => 'redlinks_ws',
        '1' => 'llamada_manual'
    );
    $id_contact = $oInsert->insertOneContact($numero, $campos);
    $return = $pDB->getFirstRowQuery("SELECT id FROM manualdialing_contact_number WHERE id_contact = ? ", false, array(
        $id_contact
    ));
    $datos_contacto = array(
        $id_contact,
        $return[0]
    );
    return $datos_contacto;
}

function arrayCastRecursive($array)
{
    if (is_array($array))
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $array[$key] = arrayCastRecursive($value);
            }
            if ($value instanceof stdClass)
            {
                $array[$key] = arrayCastRecursive((array)$value);
            }
        }
    }

    if ($array instanceof stdClass)
    {
        return arrayCastRecursive((array)$array);
    }
    return $array;
}

function retornar($arreglo)
{
    $json = new Services_JSON();
    $retorno = $json->encode($arreglo);
    echo $retorno;
    exit();
}

//funcion report
function param_reporte($param_report)
{
    require_once "/var/www/html/libs/ccpro_ECCP.class.php";
    require_once "/var/www/html/libs/callCenterProUtils.class.php";

    if (empty($param_report))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "Json es obligatorio"
        );
        retornar($return);
    }

    $arr = parse_ini_file('/etc/elastix.conf');
    $cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro_2";

    $_DB = new paloDB($cadena_dsn);
    $json = new Services_JSON();
    $call_data = $json->decode($param_report);
    $call_data = arrayCastRecursive($call_data);

    //print($call_data);
    $fecha_inicio = $call_data['start_date'] . " 00:00:00";
    $fecha_fin = $call_data['end_date'] . " 23:59:59";
    $phone = $call_data['phone'];
    $status = $call_data['status'];
    $agent = $call_data['agent'];

    $id_agent = null;

    if (!empty($agent))
    {
        $arrAgent = getPassword($_DB, $agent);
        $id_agent = $arrAgent[1];
    }

    // deberiamos llamar gncon comin_ llerdeta
    if (empty($fecha_inicio))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "La fecha de inicio es obligatorio"
        );
        retornar($return);
    }

    if (empty($fecha_fin))
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => "La fecha de fin  es obligatorio"
        );
        retornar($return);
    }

    try
    {

        $filtro_parametro = array(
            "date_start" => $fecha_inicio,
            "date_end" => $fecha_fin,
            "phone" => $phone,
            "status" => $status,
            "agent" => $id_agent,
            "nombres_rec" => "Y"
        );
        //print($fecha_fin);
        //print_r($filtro_parametro);
        $resultados_filtro = leerDetalleLlamadas($filtro_parametro);

        $return = array(
            "codigo" => "1",
            "mensaje" => "Reporte Exitoso",
            "data" => $resultados_filtro
        );

        retornar($return);
    }
    catch(Exception $e)
    {
        $return = array(
            "codigo" => "0",
            "mensaje" => $e
        );
        retornar($return);
    }
}

// Construir condición WHERE común a llamadas entrantes y salientes
function _construirWhere($param)
{
    $condSQL = array();
    $paramSQL = array();

    // Selección del agente que atendió la llamada
    if (isset($param['agent']) && preg_match('/^\d+$/', $param['agent']))
    {
        $condSQL[] = 'agent.id = ?';
        $paramSQL[] = $param['agent'];
    }

    return array(
        $condSQL,
        $paramSQL
    );
}
function _construirWhere2($param)
{
    $condSQL = array();
    $paramSQL = array();

    // Selección del agente que atendió la llamada
    if (isset($param['agent']) && preg_match('/^\d+$/', $param['agent']))
    {
        $condSQL[] = "(agent.id = ? OR (exists(SELECT id FROM ring_noanswer_incoming rnai WHERE rnai.id_call = call_entry.id AND rnai.id_agent = ?) AND call_entry.status = 'abandonada'))";
        $paramSQL[] = $param['agent'];
        $paramSQL[] = $param['agent'];
    }
    return array(
        $condSQL,
        $paramSQL
    );
}

function _construirWhere_incoming($param)
{
    list($condSQL, $paramSQL) = _construirWhere2($param);

    // Selección de la cola por la que pasó la llamada
    if (isset($param['queue']) && preg_match('/^\d+$/', $param['queue']))
    {
        $condSQL[] = 'queue_call_entry.queue = ?';
        $paramSQL[] = $param['queue'];
    }

    // Filtrar por patrón de número telefónico de la llamada
    if (isset($param['phone']) && preg_match('/^\d+$/', $param['phone']))
    {
        $condSQL[] = 'IF(contact.telefono IS NULL, call_entry.callerid, contact.telefono) LIKE ?';
        $paramSQL[] = '%' . $param['phone'] . '%';
    }

    // Filtrar por ID de campaña entrante
    if (isset($param['id_campaign_in']) && preg_match('/^\d+$/', $param['id_campaign_in']))
    {
        $condSQL[] = 'campaign_entry.id = ?';
        $paramSQL[] = (int)$param['id_campaign_in'];
    }

    // Fecha y hora de inicio y final del rango
    $sRegFecha = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/';
    if (isset($param['date_start']) && preg_match($sRegFecha, $param['date_start']))
    {
        $condSQL[] = 'call_entry.datetime_entry_queue >= ?';
        $paramSQL[] = $param['date_start'];
    }
    if (isset($param['date_end']) && preg_match($sRegFecha, $param['date_end']))
    {
        $condSQL[] = 'call_entry.datetime_entry_queue <= ?';
        $paramSQL[] = $param['date_end'];
    }

    if (isset($param['id_supervisor']) && preg_match('/^\d+$/', $param['id_supervisor']))
    {
        $condSQL[] = 'campaign_entry.id_team IN (SELECT id_team FROM sp_team WHERE id_supervisor=?)';
        $paramSQL[] = $param['id_supervisor'];
    }

    if (isset($param['team']) && preg_match('/^\d+$/', $param['team']))
    {
        $condSQL[] = 'campaign_entry.id_team=?';
        $paramSQL[] = $param['team'];
    }
    if (!empty($param['status']))
    {
        if ($param['status'] == "Success")
        {
            $condSQL[] = "call_entry.status = 'terminada' ";
        }
        else if ($param['status'] == "Abandoned")
        {
            $condSQL[] = "call_entry.status = 'abandonada' ";
        }
        //$paramSQL[] = $param['team'];
        
    }
    // Construir fragmento completo de sentencia SQL
    $where = array(
        implode(' AND ', $condSQL) ,
        $paramSQL
    );
    if ($where[0] != '') $where[0] = ' AND ' . $where[0];
    return $where;
}

function _construirWhere_outgoing($param)
{
    list($condSQL, $paramSQL) = _construirWhere($param);

    // Selección de la cola por la que pasó la llamada
    if (isset($param['queue']) && preg_match('/^\d+$/', $param['queue']))
    {
        $condSQL[] = 'queue_call_entry.queue = ?';
        $paramSQL[] = $param['queue'];
    }

    // Filtrar por patrón de número telefónico de la llamada
    if (isset($param['phone']) && preg_match('/^\d+$/', $param['phone']))
    {
        $condSQL[] = 'calls.phone LIKE ?';
        $paramSQL[] = '%' . $param['phone'] . '%';
    }

    // Filtrar por ID de campaña saliente
    if (isset($param['id_campaign_out']) && preg_match('/^\d+$/', $param['id_campaign_out']))
    {
        $condSQL[] = 'campaign.id = ?';
        $paramSQL[] = (int)$param['id_campaign_out'];
    }

    // Fecha y hora de inicio y final del rango
    $sRegFecha = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/';
    if (isset($param['date_start']) && preg_match($sRegFecha, $param['date_start']))
    {
        $condSQL[] = 'calls.fecha_llamada >= ?';
        $paramSQL[] = $param['date_start'];
    }
    if (isset($param['date_end']) && preg_match($sRegFecha, $param['date_end']))
    {
        $condSQL[] = 'calls.fecha_llamada <= ?';
        $paramSQL[] = $param['date_end'];
    }

    if (isset($param['id_supervisor']) && preg_match('/^\d+$/', $param['id_supervisor']))
    {
        $condSQL[] = 'campaign.id_team IN (SELECT id_team FROM sp_team WHERE id_supervisor=?)';
        $paramSQL[] = $param['id_supervisor'];
    }

    if (isset($param['team']) && preg_match('/^\d+$/', $param['team']))
    {
        $condSQL[] = 'campaign.id_team=?';
        $paramSQL[] = $param['team'];
    }
    if ($param['status'] == "Success")
    {
        $condSQL[] = "calls.status in ('Success','ShortCall','Finished') ";
    }
    else if ($param['status'] == "Abandoned")
    {
        $condSQL[] = "calls.status in ('Abandoned','Failure','NoAnswer') ";
    }
    // Construir fragmento completo de sentencia SQL
    $where = array(
        implode(' AND ', $condSQL) ,
        $paramSQL
    );
    if ($where[0] != '') $where[0] = ' AND ' . $where[0];
    return $where;
}

function _construirWhere_manualdialing($param)
{
    list($condSQL, $paramSQL) = _construirWhere($param);

    // Filtrar por patrón de número telefónico de la llamada
    if (isset($param['phone']) && preg_match('/^\d+$/', $param['phone']))
    {
        $condSQL[] = 'cn.phone LIKE ?';
        $paramSQL[] = '%' . $param['phone'] . '%';
    }

    // Filtrar por ID de campaña saliente
    if (isset($param['id_campaign_manualdialing']) && preg_match('/^\d+$/', $param['id_campaign_manualdialing']))
    {
        $condSQL[] = 'c.id = ?';
        $paramSQL[] = (int)$param['id_campaign_manualdialing'];
    }

    // Fecha y hora de inicio y final del rango
    $sRegFecha = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/';
    if (isset($param['date_start']) && preg_match($sRegFecha, $param['date_start']))
    {
        $condSQL[] = 'ca.datetime_originate_start >= ?';
        $paramSQL[] = $param['date_start'];
    }
    if (isset($param['date_end']) && preg_match($sRegFecha, $param['date_end']))
    {
        $condSQL[] = 'ca.datetime_originate_start <= ?';
        $paramSQL[] = $param['date_end'];
    }

    if (isset($param['id_supervisor']) && preg_match('/^\d+$/', $param['id_supervisor']))
    {
        $condSQL[] = 'c.id_team IN (SELECT id_team FROM sp_team WHERE id_supervisor=?)';
        $paramSQL[] = $param['id_supervisor'];
    }

    if (isset($param['team']) && preg_match('/^\d+$/', $param['team']))
    {
        $condSQL[] = 'c.id_team=?';
        $paramSQL[] = $param['team'];
    }
    if ($param['status'] == "Success")
    {
        $condSQL[] = "ca.status in ('Success','ShortCall') ";
    }
    else if ($param['status'] == "Abandoned")
    {
        $condSQL[] = "ca.status in ('Failure','NoAnswer') ";
    }

    // Construir fragmento completo de sentencia SQL
    $where = array(
        implode(' AND ', $condSQL) ,
        $paramSQL
    );
    if ($where[0] != '') $where[0] = ' AND ' . $where[0];
    return $where;
}

function leerDetalleLlamadas($param, $limit = NULL, $offset = 0)
{

    include_once "/var/www/html/libs/paloSantoDB.class.php";
    $arr = parse_ini_file('/etc/elastix.conf');
    $cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro_2";
    $pDB = new paloDB($cadena_dsn);

    if (!is_array($param))
    {
        $errMsg = '(internal) Invalid parameter array';
        return $errMsg;
    }

    $bNombresRec = (isset($param['nombres_rec']));
    unset($param['nombres_rec']);
    $sCamposRec = $sTablaRec = '';
    if ($bNombresRec)
    {
        $sTablaRec = ' , call_recording';
        $sCamposRec = " , CONCAT('https://192.168.30.200/bajaraudio.php?archivo=',call_recording.recordingfile)  as recordingfile";
    }

    $sPeticion_incoming = <<<SQL_INCOMING
SELECT agent.agentnick, agent.name, call_entry.datetime_entry_queue AS start_date,
    call_entry.datetime_end AS end_date, call_entry.duration,
    call_entry.duration_wait, queue_call_entry.queue, 'Inbound' AS type,
    IF(contact.telefono IS NULL, call_entry.callerid, contact.telefono) AS telefono,
    call_entry.transfer, call_entry.status, call_entry.id AS idx
    $sCamposRec
FROM (call_entry, queue_call_entry $sTablaRec)
LEFT JOIN contact
    ON contact.id = call_entry.id_contact
LEFT JOIN agent
    ON agent.id = call_entry.id_agent
LEFT JOIN campaign_entry
    ON campaign_entry.id = call_entry.id_campaign
WHERE call_entry.id_queue_call_entry = queue_call_entry.id
    AND (agent.id IS NOT NULL OR call_entry.duration IS NULL)
SQL_INCOMING;
    if ($bNombresRec) $sPeticion_incoming .= ' AND call_entry.id = call_recording.id_call_incoming';
    list($sWhere_incoming, $param_incoming) = _construirWhere_incoming($param);
    $sPeticion_incoming .= $sWhere_incoming;

    $sPeticion_outgoing = <<<SQL_OUTGOING
SELECT agent.agentnick, agent.name, calls.start_time AS start_date,
    calls.end_time AS end_date, calls.duration,
    calls.duration_wait, queue_call_entry.queue, 'Outbound' AS type,
    calls.phone AS telefono,
    calls.transfer, calls.status, calls.id AS idx
    $sCamposRec
FROM (calls, campaign, queue_call_entry $sTablaRec)
LEFT JOIN agent
    ON agent.id = calls.id_agent
WHERE campaign.id = calls.id_campaign
    AND campaign.id_queue_call_entry = queue_call_entry.id
    AND (agent.id IS NOT NULL OR calls.duration IS NULL)
SQL_OUTGOING;
    if ($bNombresRec) $sPeticion_outgoing .= ' AND calls.id = call_recording.id_call_outgoing';
    list($sWhere_outgoing, $param_outgoing) = _construirWhere_outgoing($param);
    $sPeticion_outgoing .= $sWhere_outgoing;

    $sPeticion_manualdialing = <<<SQL_MANUALDIALING
SELECT agent.agentnick, agent.name, ca.datetime_originate_start AS start_date,
    ca.datetime_link_end AS end_date, ca.duration, 0 AS duration_wait,
    NULL as queue, 'ManualDialing' AS type, cn.phone AS telefono,
    ca.transfer, ca.status, ca.id AS idx
    $sCamposRec
	FROM 		(manualdialing_contact_number_callattempt AS ca, agent)
	inner join manualdialing_contact_number AS cn	on ca.id_contact_number = cn.id 
			
	left join  call_recording        on  ca.id=call_recording.id_call_manualdialing 
	inner join   manualdialing_contact AS	ct on        cn.id_contact=ct.id 
	inner  join  manualdialing_campaign AS c         on    ct.id_campaign=c.id
  WHERE   ca.id_agent=agent.id
SQL_MANUALDIALING;
    //  if ($bNombresRec) $sPeticion_manualdialing .= ' AND ca.id = call_recording.id_call_manualdialing';
    list($sWhere_manualdialing, $param_manualdialing) = _construirWhere_manualdialing($param);
    $sPeticion_manualdialing .= $sWhere_manualdialing;

    // Construir la unión SQL en caso necesario
    $sPeticionSQL = NULL;
    $paramSQL = NULL;
    if (!isset($param['calltype'])) $param['calltype'] = 'any';
    switch ($param['calltype'])
    {
        case 'incoming':
            $sPeticionSQL = $sPeticion_incoming;
            $paramSQL = $param_incoming;
        break;
        case 'outgoing':
            $sPeticionSQL = $sPeticion_outgoing;
            $paramSQL = $param_outgoing;
        break;
        case 'manualdialing':
            $sPeticionSQL = $sPeticion_manualdialing;
            $paramSQL = $param_manualdialing;
        break;
        default:
            $sPeticionSQL = "($sPeticion_incoming) UNION ($sPeticion_outgoing) UNION ($sPeticion_manualdialing)";
            $paramSQL = array_merge($param_incoming, $param_outgoing, $param_manualdialing);
            print_r($sPeticionSQL);
        break;
    }

    /* TODO: start_date sólo es válido si la llamada se conectó. Llamadas no
     * conectadas tienen NULL y serán listadas al final. */
    $sPeticionSQL .= ' ORDER BY start_date DESC, telefono';
    if (!empty($limit))
    {
        $sPeticionSQL .= " LIMIT ? OFFSET ?";
        array_push($paramSQL, $limit, $offset);
    }
    //print($sPeticionSQL);
    //print_r($paramSQL);
    // Ejecutar la petición SQL para todos los datos
    //print "<pre>$sPeticionSQL</pre>";
    $recordset = $pDB->fetchTable($sPeticionSQL, true, $paramSQL);
    if (!is_array($recordset))
    {
        $errMsg = '(internal) Failed to fetch CDRs - ' . $pDB->errMsg;
        return errMsg;
    }

    if (!$bNombresRec)
    {
        /* Buscar grabaciones para las llamadas leídas. No se usa un LEFT JOIN
         * en el query principal porque pueden haber múltiples grabaciones por
         * registro (múltiples intentos en caso outgoing) y la cuenta de
         * registros no considera esta duplicidad. */
        $sqlfield = array(
            'Inbound' => 'id_call_incoming',
            'Outbound' => 'id_call_outgoing',
            'ManualDialing' => 'id_call_manualdialing',
        );
        foreach (array_keys($recordset) as $i)
        {
            /* Se asume que el tipo de llamada está en la columna 7 y el ID del
             * intento de llamada en la columna 11. */
            $sql = 'SELECT id, datetime_entry FROM call_recording WHERE ' . $sqlfield[$recordset[$i]["idx"]] . ' = ? ORDER BY datetime_entry DESC';
            $r2 = $pDB->fetchTable($sql, true, array(
                $recordset[$i][11]
            ));
            if (!is_array($r2))
            {
                $errMsg = '(internal) Failed to fetch recordings for CDRs - ' . $pDB->errMsg;
                return $errMsg;
            }

            //	$direccion = "https://192.168.30.200/bajaraudio.php?archivo=";
            $recordset[$i]["recording"] = $r2;
        }
    }

    //LLAMADAS ABANDONADAS 11
    if (isset($param['agent']) && preg_match('/^\d+$/', $param['agent']))
    {
        $sQuery = "SELECT agentchannel,id_agent FROM ring_noanswer_incoming WHERE id_call = ? AND id_agent = ?";
    }
    else $sQuery = "SELECT agentchannel,id_agent FROM ring_noanswer_incoming WHERE id_call = ?";
    $sQueryAgent = "SELECT name,agentnick FROM agent WHERE id = ?";
    foreach ($recordset as $idx => $registro)
    {
        if ($registro['status'] == "abandonada")
        {
            if (isset($param['agent']) && preg_match('/^\d+$/', $param['agent']))
            {
                $arrLlamada = $pDB->getFirstRowQuery($sQuery, true, array(
                    $registro['idx'],
                    $param['agent']
                ));
            }
            else
            {
                $arrLlamada = $pDB->getFirstRowQuery($sQuery, true, array(
                    $registro['idx']
                ));
            }
            $arrTmp = $pDB->getFirstRowQuery($sQueryAgent, true, array(
                $arrLlamada['id_agent']
            ));
            $recordset[$idx]['agentnick'] = $arrTmp["agentnick"]; //Agentnick
            $recordset[$idx]['name'] = $arrTmp["name"]; //Agentname
            
        }
    }

    return $recordset;
}

?>