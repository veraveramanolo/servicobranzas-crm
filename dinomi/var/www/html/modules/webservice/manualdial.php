<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With,Authorization");
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day

chdir("/var/www/html");

include_once "/var/www/html/libs/JSON.php";
require_once "/var/www/html/libs/ccpro_ECCP.class.php";
require_once "/var/www/html/libs/callCenterProUtils.class.php";
require_once "modules/ccpro_campaign_out/libs/ccproOutgoingCampaign.class.php";
require_once "modules/ccpro_campaign_manual/libs/ccproContactInsert_ManualDialing.class.php";

$arr = parse_ini_file('/etc/elastix.conf');
$cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro";
$oDB = new paloDB($cadena_dsn);

$agentname = $_GET['agentname'];
$telefono = $_GET['phone'];
$campania = "campman_integracion_".date('Ym');

$agent = getPassword($oDB,$agentname);
$agentpass = $agent[0];
$idAgent = $agent[1];

$x = new ECCP();
$agentStatus = "";
try {
    $cr = $x->connect("localhost", "agentconsole", "agentconsole");
    if (isset($cr->failure))
    {
        $return = array(
            "codigo" => "-1",
            "estado" => "error",
            "mensaje" => "Error: ".$cr->failure->message
        );
        retornar($return);  
    }
    $x->setAgentNumber($agentname);
    $agentStatus = (array)$x->getAgentStatus();
    $x->disconnect();
} catch (Exception $e) {
    $return = array(
            "codigo" => "-1",
            "estado" => "error",
            "mensaje" => "Error: ".$x->getParseError()
        );
    retornar($return);  
}
if($agentStatus["status"]!="online"){
    $return = array(
        "codigo" => "-1",
        "estado" => "error",
        "mensaje" => "Error: Agente en llamada"
    );
    retornar($return);  
}

//Se verifica si existe la campaña 
$campaign = verificaCampania($oDB,$idAgent,$campania);
$contacto = generaContacto($oDB,$telefono,$campaign);
$contact = $contacto[0];
$dialnum = $contacto[1];

try {
    $cr = $x->connect("localhost", "agentconsole", "agentconsole");
    if (isset($cr->failure))
    {
        $return = array(
            "codigo" => "-1",
            "estado" => "error",
            "mensaje" => "Error3: ".$cr->failure->message
        );
        retornar($return);  
    }
    $x->setAgentNumber($agentname);
    if (getenv('ECCP_AUTHMODE') == 'agentnick') $x->useAgentNick(TRUE);
    $x->setAgentPass($agentpass);
    $r = $x->manualdialingcall(
        $campaign,
        $contact,
        $dialnum);
    $x->disconnect();
    if(isset($r->failure))
    {
        $return = array(
                    "codigo" => -1,
                    "estado" => "error",
                    "mensaje" => "Error: ".$r->failure->message
                );
        retornar($return);    
    }
    $return = array(
                "codigo" => 1,
                "estado" => "success",
                "mensaje" => ""
            );
    retornar($return);
} catch (Exception $e) {
    $return = array(
            "codigo" => "-1",
            "estado" => "error",
            "mensaje" => "Error4: ".$x->getParseError()
        );
    retornar($return); 
}

function verificaCampania($oDB,$idAgent,$campaign, $contexto = 'from-internal')
{
    $anio = date("Y");
    $mes = date("m");
    if(empty($contexto)) $contexto = "from-internal";
    $return = $oDB->getFirstRowQuery("SELECT id FROM manualdialing_campaign WHERE name = ? ",false, array("campman_integracion_".$anio.$mes));
    if(empty($return)){
        $sPeticionSQL = "INSERT INTO manualdialing_campaign (name, trunk, context,
                        datetime_init, datetime_end, daytime_init, daytime_end, script)
                        VALUES (?,?,?,?,?,?,?,?)";
        $oDB->genQuery($sPeticionSQL,array("campman_integracion_".$anio.$mes,null,$contexto,"$anio-01-01","$anio-12-31","00:00:00","23:59:59","--"));
        $id_campaign = $oDB->getLastInsertId();
        $oCamp = new ccproOutgoingCampaign($oDB);
        $oCamp->refrescarCampaniasDialer();
    }else{
        $id_campaign = $return[0];
    }
    
    //Se asigna agente a campania
    $return = $oDB->getFirstRowQuery("SELECT * FROM manualdialing_campaign_agent WHERE id_campaign = $id_campaign and id_agent = $idAgent",false);
    if(!empty($return)){
        return $id_campaign; 
    }
    $oDB->genQuery(" INSERT INTO manualdialing_campaign_agent VALUES($id_campaign,$idAgent)");
    return $id_campaign;
}

function generaContacto($pDB, $phone, $campaign)
{
    $oInsert = new ccproContactInsert_ManualDialing($pDB, $campaign);
    $numero = array(
        $phone
    );
    $campos[] = array(
        '0' => 'telefono',
        '1' => $phone
    );
    $id_contact = $oInsert->insertOneContact($numero, $campos);
    $return = $pDB->getFirstRowQuery("SELECT id FROM manualdialing_contact_number WHERE id_contact = ? ", false, array(
        $id_contact
    ));
    $datos_contacto = array(
        $id_contact,
        $return[0]
    );
    return $datos_contacto;
}

function getPassword($pDB, $agentname)
{
    $arrAgent = explode("/", $agentname);
    $qAgent = "SELECT agent.eccp_password,
                        agent.id
                 FROM   agent,
                        agent_voicechannel 
                 WHERE  agent.id = agent_voicechannel.id_agent
                 AND    agent_voicechannel.datetime_end is null
                 AND    agent_voicechannel.type = ?
                 AND    agent_voicechannel.number = ?";

    $return = $pDB->getFirstRowQuery($qAgent, false, array(
        $arrAgent[0],
        $arrAgent[1]
    ));
    return $return;
}

function retornar($arreglo)
{
    $json = new Services_JSON();
    //$retorno = $json->encode($arreglo);
    $retorno = json_encode($arreglo);
    echo $retorno;
    exit();
}

?>