<?php
chdir("/var/www/html");
require_once ("/var/www/html/libs/ccpro_ECCP.class.php");
require_once "libs/callCenterProUtils.class.php";
require_once "modules/ccpro_campaign_manual/libs/paloContactInsert.class.php";
require_once "modules/ccpro_campaign_manual/libs/paloSantoCampaignManual.class.php";

$oDB = new paloDB(getCallCenterDBString());
$agentname = $_GET["agenttype"]."/".$_GET["agentnumber"];
$agent = getPassword($oDB,$agentname);
$agentpass = $agent[0];
$idAgent = $agent[1];

$x = new ECCP();
try {
    $cr = $x->connect("localhost", "agentconsole", "agentconsole");
    if (isset($cr->failure)) die('Failed to connect to ECCP - '.$cr->failure->message."\n");
    $x->setAgentNumber($agentname);
    if (getenv('ECCP_AUTHMODE') == 'agentnick') $x->useAgentNick(TRUE);
    $x->setAgentPass($agentpass);
    $x->hangup();
    $x->disconnect();
    echo json_encode(array("status"=>"processing"));
} catch (Exception $e) {
    echo json_encode(array("status"=>"error"));
}


function getPassword($oDB,$agent){
    $arrAgent = explode("/",$agent);
    $return = $oDB->getFirstRowQuery("SELECT agent.eccp_password,agent.id FROM agent, agent_voicechannel 
    WHERE agent.id = agent_voicechannel.id_agent AND agent_voicechannel.type = ? and agent_voicechannel.number = ?
    AND  agent_voicechannel.datetime_end is null ",
                           false,array($arrAgent[0],$arrAgent[1])
                          );
    return $return;
}

?>