<?php
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With,Authorization");
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day

chdir("/var/www/html");

include_once "/var/www/html/libs/JSON.php";
include_once "libs/paloSantoDB.class.php";
require_once "/var/www/html/modules/webservice/php-mailer/class.phpmailer.php";
require_once "/var/www/html/modules/webservice/php-mailer/class.smtp.php";

$arr = parse_ini_file('/etc/elastix.conf');
$cadena_dsn = "mysql://root:" . $arr['mysqlrootpwd'] . "@localhost/call_center_pro";
$oDB = new paloDB($cadena_dsn);

$sQuery = "SELECT csg.ip_server, csg.`user`, csg.pass, csgp.port 
from crm_sms_gateway csg 
JOIN crm_sms_gateway_port csgp on (csg.id = csgp.id_gateway)
where csg.estado ='A'
and csgp.estado ='A'
order by rand()
limit 1 ";

$arrGateway = $oDB->getFirstRowQuery($sQuery,true);

$mensaje = $_POST['mensaje'];
$telefono = $_POST['telefono'];

$curl = curl_init();
$url = "http://{$arrGateway['ip_server']}/sendsms?username={$arrGateway['user']}&password={$arrGateway['pass']}&port={$arrGateway['port']}&phonenumber={$telefono}&message={$mensaje}";
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$data = curl_exec($curl);
curl_close($curl);

$arrData = json_decode($data,true);


$return = array(
                "codigo" => 1,
                "respuesta" => $arrData['report'][1]['result'],
                "mensaje" => "",
            );
retornar($return);

function retornar($arreglo)
{
    $json = new Services_JSON();
    //$retorno = $json->encode($arreglo);
    $retorno = json_encode($arreglo);
    echo $retorno;
    exit();
}