import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

@Component({
  selector: "app-modal-notificacion",
  templateUrl: "./modal-notificacion.component.html",
  styleUrls: ["./modal-notificacion.component.css"],
})
export class ModalNotificacionComponent implements OnInit {
  datos: any;

  constructor(
    public modalRef: BsModalRef,
    private readonly router: Router,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {}

  seleccionarItem(item: any): void {
    console.log(item.dataItem);
    this.modalService.setDismissReason("notificacion");
    this.modalRef.hide();
    this.router.navigate([
      `gestionar/gestionar/gestionid/${item.dataItem.idcampania}/${item.dataItem.idproducto}/alerta`,
    ]);
  }
}
