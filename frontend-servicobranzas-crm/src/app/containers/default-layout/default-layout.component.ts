import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivationEnd } from "@angular/router";
import { NotificationsService, NotificationType } from "angular2-notifications";
import { MessageService } from "primeng/api";
import { interval, Observable, Subscription } from "rxjs";
import { isNullOrUndefined } from "util";
import { ModeloRespuesta } from "../../models/ModeloRespuesta.models";
import { AuthenticationService } from "../../services/authentication.service";
import { VariableGlobalService } from "../../services/variable-global.service";
import { NotificationService } from "@progress/kendo-angular-notification";
import { SnotifyService } from "ng-snotify";
import {
  ActualizarNotificacionInterface,
  NotificacionTiempoService,
} from "../../services/notificacion.service";
import { ModalNotificacionComponent } from "../modal-notificacion/modal-notificacion.component";
import { BsModalService } from "ngx-bootstrap/modal";
import { NavItemsCabecera } from "../../models/sidebar-nav";
import { filter, map } from "rxjs/operators";
export enum estadoNotificacion {
  activo = "V",
  inactivo = "F",
}

@Component({
  selector: "app-dashboard",
  templateUrl: "./default-layout.component.html",
  styles: [
    `
      .estilo {
        font-size: 170%;
      }
      ,
      .button-notification {
        padding: 10px 5px;
        color: #313536;
      }
    `,
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {
  public titulo: string;
  public tituloSubs$: Subscription;
  public type: any;
  public intervalSubs: Subscription;
  public sidebarMinimized = false;
  public navItems: NavItemsCabecera[] = [];

  constructor(
    public global: VariableGlobalService,
    private authenticationService: AuthenticationService,
    private router: Router,
    // private notifications: NotificationsService,
    // private messageService: MessageService,
    // private notificationService: NotificationService,
    private snotifyService: SnotifyService,
    private readonly notificacionTimeService: NotificacionTiempoService,
    private readonly modalService: BsModalService
  ) {
    this.tituloSubs$ = this.getArgumentosRuta().subscribe(({ titulo }) => {
      console.log("el titulo", titulo);
      this.titulo = titulo;
      document.title = `Servicobranzas - ${this.titulo}`;
    });
    console.log(this.global.usuarioLogueado);
    //this.navItems = this.global.navItems;
    //this.navItems = navItems;
    this.ConsultarNavItems();
  }
  ngOnDestroy(): void {
    this.intervalSubs.unsubscribe();
    this.tituloSubs$.unsubscribe();
  }

  getArgumentosRuta() {
    return this.router.events.pipe(
      filter((event) => event instanceof ActivationEnd),
      filter((event: ActivationEnd) => event.snapshot.firstChild === null),
      map((event: ActivationEnd) => event.snapshot.data)
    );
  }

  async ngOnInit(): Promise<void> {
    this.mostrarNotificacionesApi();
    console.log("alerta actualizada!");
    this.notificacion();
    console.log(this.global.usuarioLogueado);
    if (isNullOrUndefined(this.global.usuarioLogueado)) {
      this.global.usuarioLogueado = await JSON.parse(
        localStorage.getItem(this.global.valorusuarioLogueadoSessionStorage)
      );
    }
    // await this.authenticationService.ConsultarPermiosAccesos(
    //   this.global.usuarioLogueado.idUsuario
    // );
  }

  retornaIntervalo(): Observable<number> {
    return interval(300000);
  }

  ConsultarNavItems() {
    this.navItems = this.global.ConsultarNavItems();
    console.log("menuuu", this.navItems);
  }
  mostrarNotificacionesApi(): void {
    this.notificacionTimeService
      .notificacionesTiempo()
      .subscribe((respuesta) => {
        console.log("respuesta notificacion", respuesta.data);
        if (respuesta.data) {
          this.actualizarNotificacion(respuesta.data[0].idAcuerdo);
          const config = {
            showProgressBar: false,
            closeOnClick: false,
            pauseOnHover: false,
            buttons: [
              {
                text: "Ver",
                action: (toast) => {
                  this.open(respuesta.data);
                  console.log(respuesta.data);

                  this.snotifyService.remove(toast.id);
                },
                bold: false,
              },

              {
                text: "Cerrar",
                action: (toast) => {
                  console.log("Clicked: No");
                  this.snotifyService.remove(toast.id);
                },
                bold: true,
              },
            ],
          };
          this.snotifyService.confirm("Alerta", config);
        }
      });
  }
  actualizarNotificacion(id: number): void {
    const datos: ActualizarNotificacionInterface = {
      id,
      estadoNot: estadoNotificacion.inactivo,
    };
    console.log(datos);
    this.notificacionTimeService
      .actualizarEstado(datos)
      .subscribe((data) => console.log(data));
  }

  notificacion(): void {
    this.intervalSubs = this.retornaIntervalo().subscribe((intervalo) => {
      console.log(intervalo);
      this.mostrarNotificacionesApi();
    });
  }

  open(datos: any): void {
    this.modalService.show(ModalNotificacionComponent, {
      initialState: {
        datos,
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  // async ConsultarPermiosAccesos(idUser: number) {
  //   let respuesta: ModeloRespuesta = await this.authenticationService.ConsultarPermiosAccesos(
  //     idUser
  //   );
  //   this.global.navItems = respuesta.data;
  //   this.global.navItems.unshift(this.global.cabeceraNasItems);
  //   console.log(this.global.navItems);
  // }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  logOut(): void {
    localStorage.removeItem("token");
    this.router.navigate([`/login`]);
  }
}
