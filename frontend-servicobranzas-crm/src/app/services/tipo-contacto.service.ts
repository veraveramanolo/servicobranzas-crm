import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { VariableGlobalService } from "./variable-global.service";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";

@Injectable({
  providedIn: 'root'
})
export class TipoContactoService {

  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) { }

  GetAclTipoContacto(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/AclTipoContacto/GetAclTipoContacto`
    );
  }

}
