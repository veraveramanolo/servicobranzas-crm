import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ManejoErrorMetodo } from "../common/manejo-error";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";
import { catchError, map } from "rxjs/operators";
import { CrmCampania } from "../models/CrmCampania";
import { CrmTareas } from "../models/CrmTareas";
import { Observable } from "rxjs";
import { CrmConsultaGestionCampania } from "../models/CrmConsultaGestionCampania";

@Injectable({
  providedIn: "root",
})
export class CampaniaService {
  constructor(
    private http: HttpClient,
    private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  //Retorna la lista de campañas. Se consulta para generar botones en la pantalla de gestor
  async All() {
    return await this.http
      .get<ModeloRespuesta>(`${this.global.url}/CrmCampania/All`)
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  //Consulta clientes dependiendo de la campaña seleccionada en el botón.

  async GetCuentasCampania(idCampania: number, idUsuario: number) {
    return await this.http
      .get<ModeloRespuesta>(
        `${this.global.url}/CrmCampaniaCuentas/GetCuentas/?idCampania=${idCampania}&idUsuario=${idUsuario}`
      )
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  // --------------------------------------Usuario ID--------------------------------------------
  // Consulta del evento al dar click en la fila de la tabla principal de clientes del gestor.
  async Campania(id: number) {
    return await this.http
      .get<ModeloRespuesta>(`${this.global.url}/CrmCampania/Campania?id=${id}`)
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  async Modificar(crmCampania: CrmCampania) {
    return await this.http
      .post<ModeloRespuesta>(
        `${this.global.url}/CrmCampania/Modificar/`,
        crmCampania
      )
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  async Crear(crmCampania: CrmCampania) {
    return await this.http
      .post<ModeloRespuesta>(
        `${this.global.url}/CrmCampania/Crear/`,
        crmCampania
      )
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  async CambiarEstado(id: number, estado: string, UsrModificacion: string) {
    return await this.http
      .get<ModeloRespuesta>(
        `${this.global.url}/CrmCampania/CambiarEstado?id=${id}&estado=${estado}&UsrModificacion=${UsrModificacion}`
      )
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }
  //-----------------------------------------TAREAS----------------------------------------------
  async AllTareas() {
    //Consulta todas las tareas
    return await this.http
      .get<ModeloRespuesta>(`${this.global.url}/CrmTareas/All`)
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  AcuerdosPagoCuentaCampania(
    idCuentaCampania: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/GetAcuerdoPago?idCuentaCampania=${idCuentaCampania}`
    );
  }

  PagoCuentaCampania(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaPagos/GetPagos?idCuentaCampania=${idCuentaCampania}`
    );
  }
  ProductoCuentaCampania(
    idCuentaCampania: number
  ): Observable<ModeloRespuesta> {
    console.log(idCuentaCampania);
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaProductos/GetProductos?idCuentaCampania=${idCuentaCampania}`
    );
  }

  DeudasCarteras(
    idCuenta: number,
    idcuentacampania: number
  ): Observable<ModeloRespuesta> {
    console.log(idCuenta);
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaProductos/GetDeudasCarteras?idCuenta=${idCuenta}&idCuentaCampania=${idcuentacampania}`
    );
  }

  mostrarDetalleCliente(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCampaniaCuentas/GetDetalleCliente?idCuentaCampania=${idCuentaCampania}`
    );
  }

  mostrarTelefono(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasTelefono/GetTelefono?idCuentaCampania=${idCuentaCampania}`
    );
  }

  mostrarHistoricoCliente(
    idCuentaCampania: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaDetalleHistorico/GetHistoricoCliente?idCuentaCampania=${idCuentaCampania}`
    );
  }

  mostrarIdNav(
    idCuentaCampania: number, idproducto: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmConsultaGestionCampaniaData/ObtenerNavBusq?idcuentacampania=${idCuentaCampania}&idproducto=${idproducto}`
    );
  }

  async GetCampaniaGestor(idGestor: number) {
    return await this.http
      .get<ModeloRespuesta>(
        `${this.global.url}/CrmCampania/CampaniaGestor?idGestor=${idGestor}`
      )
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  async GetConsultaCampania() {
    return await this.http
      .get<ModeloRespuesta>(
        `${this.global.url}/CrmCampania/ConsultaCampania`
      )
      .pipe(
        map((resp) => {
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }


  cargarCliente(cedula,idCartera,idCampania){

    console.log(this.global.url+"/CrmCuentas/BusquedaIdNom?filtroid="+cedula);
     
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentas/BusquedaIdNom?filtroid=${cedula}`
    );
  }

  guardarCaja(idCartera,idCampania){

    const datos = {
      idcartera: idCartera,//(<HTMLInputElement>document.getElementById("combo_cartera").getElementsByTagName("input")[0]).value,
      idcampania: idCampania,//(<HTMLInputElement>document.getElementById("combo_campania").getElementsByTagName("input")[0]).value,
      identificacion: (<HTMLInputElement>document.getElementById("identificacion")).value,
      nombres: (<HTMLInputElement>document.getElementById("nombres")).value,
      //apellidos: (<HTMLInputElement>document.getElementById("apellidos")).value,
      comentario: (<HTMLInputElement>document.getElementById("comentario")).value,
      valor: (<HTMLInputElement>document.getElementById("valor")).value,
      estado: 'A',
    };
    console.log(datos);
    
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmGestion/GuardarCaja`,
      datos
    );
  }

  guardarGestionCaja(idProducto, idCuentaCampania, idCuenta, detalleGestion){
    const datos = {
      idCuentasCampaniaProductos: idProducto,
      idTelefono: null,
      horaGestion: null,
      idGestionDinomi: 1,
      tiempoLlamada: 2,
      estadoLlamada: "Suspendida",
      idArbolDecision: 81,
      detalleGestion: detalleGestion,
      tipoGestion: "Caja",
      idCuenta: idCuenta,
      idCuentaCampania: Number(idCuentaCampania),
      //idCuentasCampaniaProductos: gestion.idProducto,
    };
    console.log(datos);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmGestion/Crear`,
      datos
    );
  }
  
  guardarCSV(idCartera, idCampania, fileToUpload){
    
    const formData: FormData = new FormData();
    formData.append('archivocsv', fileToUpload, fileToUpload.name);
    formData.append('idCartera', idCartera);
    formData.append('idCampania', idCampania);
    
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmGestion/CSVupload`,
      formData
    );
  }

  guardarGestion(idCuentaCampania: number, gestion: any): any {
    const datos = {
      idCuentasCampaniaProductos: gestion.idProducto,
      idTelefono: gestion.idTelefono,
      horaGestion: null,
      idGestionDinomi: 1,
      tiempoLlamada: 2,
      estadoLlamada: "Suspendida",
      idArbolDecision: gestion.idArbolDecision,
      detalleGestion: gestion.detalleGestion,
      tipoGestion: gestion.tipoGestion,
      idCuenta: gestion.idcuenta,
      idCuentaCampania: Number(idCuentaCampania),
      //idCuentasCampaniaProductos: gestion.idProducto,
    };
    console.log(datos);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmGestion/Crear`,
      datos
    );
  }

  mostrarDetalleProducto(
    idCampania: number,
    idProducto: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDetalle/GetCuentasDetalle?idCuentaCampania=${idCampania}&idCuentaCampaniaProducto=${idProducto}`
    );
  }
  busquedaPorEmail(email: string): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasEmail/BusquedaEmail?filtemail=${email}`
    );
  }

  busquedaPorIdNombre(
    identificacion: string,
    nombre: string
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentas/BusquedaIdNom?filtroid=${identificacion}&filtronom=${nombre}`
    );
  }

  busquedaPorTelefono(telefono: string): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasTelefono/BusquedaTelefono?filtfono=${telefono}`
    );
  }

  enviarDatos(
    datos: CrmConsultaGestionCampania[]
  ): Observable<ModeloRespuesta> {
    console.log(datos);
    //const datosJson = { datos: datos };
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmConsultaGestionCampaniaData/InsertaBD`,
      datos
    );
  }
  botonAnteriorSiguiente(
    id: number,
    tipo: string
  ): Observable<ModeloRespuesta> {
    console.log(id, tipo);
    let siguiente: any = "";
    let anterior: any = "";
    if (tipo === "siguiente") {
      siguiente = id;
    } else {
      anterior = id;
    }
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmConsultaGestionCampaniaData/GetBD?idsgte=${siguiente}&idantes=${anterior}`
    );
  }

  obtenerListaEstados(
    idCampania: number,
    idUsuario: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCampaniaCuentas/GetEstados?idCampania=${idCampania}&idUsuario=${idUsuario}`
    );
  }

  obtenerListaEstadosBusqueda(
    idCampania: number,
    idUsuario: number,
    descripcion: string
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCampaniaCuentas/GetBDfiltro?idCampania=${idCampania}&idUsuario=${idUsuario}&descripcion=${descripcion}`
    );
  }

  obtenerArticulos(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaProductosArticulos/GetProductosArticulos?idCuentaCampania=${idCuentaCampania}`
    );
  }

  obtenerTareas(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmTareas/TareasUsuario`
    );
  }
  obtenerCab(idTarea: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmTareas/BaseTareas?idtarea=${idTarea}`
    );
  }

  // api/CrmTareas/TareasUsuario
}
