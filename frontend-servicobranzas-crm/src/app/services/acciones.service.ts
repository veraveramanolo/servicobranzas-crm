import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { AclAcciones } from '../models/AclAcciones';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { VariableGlobalService } from './variable-global.service';

@Injectable({
  providedIn: 'root'
})
export class AccionesService {

  constructor(
    private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) { 
    
  }


  async All() {
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclAcciones/All`)
      .pipe(
        map( resp =>{
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      ).toPromise();
  }

  async TipoModulo( id:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclAcciones/Perfil?id=${id}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(aclAcciones:AclAcciones){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclAcciones/Modificar/`, aclAcciones)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(aclAcciones: AclAcciones){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclAcciones/Crear/`, aclAcciones)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async CambiarEstado(id: number, estado: number, UsrModificacion: string){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclAcciones/CambiarEstado?id=${id}&estado=${estado}&UsrModificacion=${UsrModificacion}`)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

}
