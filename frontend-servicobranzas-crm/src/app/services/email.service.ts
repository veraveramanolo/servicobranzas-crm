import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EmailService {
  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  mostrarEmail(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasEmail/GetEmail?idCuentaCampania=${idCuentaCampania}`
    );
  }

  guardarEmail(email: any): Observable<ModeloRespuesta> {
    console.log(email);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasEmail/Crear`,
      email
    );
  }

  editarEmail(email: any): Observable<ModeloRespuesta> {
    console.log(email);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasEmail/Modificar`,
      email
    );
  }

  inactivarEmail(inactivarEmail: any): Observable<ModeloRespuesta> {
    console.log(inactivarEmail);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasEmail/Inactivar`,
      inactivarEmail
    );
  }
}
