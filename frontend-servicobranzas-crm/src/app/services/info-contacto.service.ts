import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";
import {
  editarCuentaTelefonoModel,
  CrearCuentaTelefonoModel,
} from "../models/CrmCuentasTelefono";

@Injectable({
  providedIn: "root",
})
export class InfoContactoService {
  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  mostrarInfoContacto(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasTelefono/GetTelefono?idCuentaCampania=${idCuentaCampania}`
    );
  }

  guardarTelefonoContacto(
    idCuentaCampania: number,
    telefono: any
  ): Observable<ModeloRespuesta> {
    const telefonoJson: CrearCuentaTelefonoModel = {
      idCuenta: Number(idCuentaCampania),
      valor: telefono.telefono,
      tipo: telefono.tipo,
      idDepto: telefono.depto || null,
      idCiudad: telefono.ciudad || null,
      idDistrito: telefono.distrito || null,
      info2: telefono.info2 || null,
    };
    console.log(telefonoJson);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasTelefono/Crear`,
      telefonoJson
    );
  }

  editarTelefonoContacto(
    idTelefono: number,
    idCuentaCampania: number,
    telefono: any
  ): Observable<ModeloRespuesta> {
    console.log("telefono", telefono);
    const telefonoJson: editarCuentaTelefonoModel = {
      id: idTelefono,
      idCuenta: Number(idCuentaCampania),
      valor: telefono.telefono,
      tipo: telefono.tipo,
      idDepto: telefono.depto || null,
      idCiudad: telefono.ciudad || null,
      idDistrito: telefono.distrito || null,
      info2: telefono.info2 || null,
    };
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasTelefono/Modificar`,
      telefonoJson
    );
  }

  inactivarTelefono(data: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasTelefono/Inactivar`,
      data
    );
  }
}
