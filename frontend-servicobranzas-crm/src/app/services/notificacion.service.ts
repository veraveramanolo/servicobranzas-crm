import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

export interface ActualizarNotificacionInterface {
  id: number;
  estadoNot: string;
}

@Injectable({
  providedIn: "root",
})
export class NotificacionTiempoService {
  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) {}

  notificacionesTiempo(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/GetAlertasHora`
    );
  }

  actualizarEstado(
    datos: ActualizarNotificacionInterface
  ): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/ModificarAlertaNot`,
      datos
    );
  }
}
