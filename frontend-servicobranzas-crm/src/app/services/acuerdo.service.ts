import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AcuerdoService {
  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  mostrarAcuerdos(CampaniaID: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/GetAcuerdoPago?idCuentaCampania=${CampaniaID}`
    );
  }

  guardarAcuerdo(acuerdo: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/CrearAcuerdosPago`,
      acuerdo
    );
  }

  editarAcuerdo(acuerdo: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/ModificarAcuerdosPago`,
      acuerdo
    );
  }

  mostrarAlertas(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmCuentasCampaniaAcuerdos/GetAlertas`);
  }
}
