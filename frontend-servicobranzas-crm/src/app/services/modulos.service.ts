import { AclModulos } from './../models/AclModulos';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { VariableGlobalService } from './variable-global.service';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModulosService {

  constructor(
    private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) { 

  }

  async All(){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclModulos/All`)
    .pipe(
      map( resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(aclModulos:AclModulos){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclModulos/Modificar/`, aclModulos)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(aclModulos: AclModulos){
    debugger
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclModulos/Crear/`, aclModulos)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }
}
