import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";
import { format } from "date-fns";
import { CrmTareaObjetivos } from "../models/CrmTareaObjetivos";

@Injectable({
  providedIn: "root",
})
export class AsignacionService {
  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) {}

  obtenerUsuariosAsignados(idCampania: number): Observable<any> {
    return this.http.get<any>(
      `${this.global.url}/AclUsuario/UsuariosAsignados?idCampania=${idCampania}`
    );
  }

  crearAsignacionEqui(dato: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmAsignacionCab/AsignacionAsesor`,
      dato
    );
  }
  crearAsignacionManual(dato: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmAsignacionCab/AsignacionAsesorManual`,
      dato
    );
  }
  obtenerColumnas(clientes: any): Observable<any> {
    const datos = {
      clientes,
    };
    console.log("clientes a enviar", datos);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmAsignacionCab/ListaAsignacionColumna`,
      datos
    );
  }
  crearAsignacionColumna(dato: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmAsignacionCab/AsignacionColumna`,
      dato
    );
  }
  obtenerEstados(): Observable<any> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCampania/CampaniaCiclos?estado=`
    );
  }

  cambiarEstado(id: number): Observable<ModeloRespuesta> {
    return this.http.put<ModeloRespuesta>(
      `${this.global.url}/CrmCampania/ModificarEstado?id=${id}`,
      {}
    );
  }

  eliminarAsignacion(datos: any): Observable<ModeloRespuesta> {
    return this.http.put<ModeloRespuesta>(
      `${this.global.url}/CrmAsignacionCab/RemoverAsignacion`,
      datos
    );
  }
}
//CrmAsignacionCab/AsignacionColumna

//CrmAsignacionCab/RemoverAsignacion
