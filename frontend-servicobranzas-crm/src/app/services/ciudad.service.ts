import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CiudadService {
  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  mostrarCiudadesPorDepartamento(
    DepartamentoID: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/AclCiudad/GetCiudad?idDepto=${DepartamentoID}`
    );
  }
}
