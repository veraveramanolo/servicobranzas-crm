import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { VariableGlobalService } from './variable-global.service';
import { catchError, map } from 'rxjs/operators';
import { AclTipoModulos } from '../models/AclTipoModulos';

@Injectable({
  providedIn: 'root'
})
export class TipoModulosService {

  constructor(private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService) { 

  }


  async All(){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclTipoModulos/All`)
    .pipe(
      map( resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async TipoModulo( id:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclTipoModulos/TipoModulo?id=${id}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(aclTipoModulos:AclTipoModulos){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclTipoModulos/Modificar/`, aclTipoModulos)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(aclTipoModulos:AclTipoModulos){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclTipoModulos/Crear/`, aclTipoModulos)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async CambiarEstado(id: number, estado: number, UsrModificacion: string){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclTipoModulos/CambiarEstado?id=${id}&estado=${estado}&UsrModificacion=${UsrModificacion}`)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


}
