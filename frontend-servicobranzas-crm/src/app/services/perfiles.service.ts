import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { catchError, map } from 'rxjs/operators';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { VariableGlobalService } from './variable-global.service';
import { AclPerfiles } from '../models/AclPerfiles';

@Injectable({
  providedIn: 'root'
})
export class PerfilesService {



  constructor(private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService) { }

  async All() {
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclPerfiles/All`)
      .pipe(
        map( resp =>{
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      ).toPromise();
  }

  async TipoModulo( id:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclPerfiles/Perfil?id=${id}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(aclPerfiles:AclPerfiles){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclPerfiles/Modificar/`, aclPerfiles)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(aclPerfiles: AclPerfiles){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclPerfiles/Crear/`, aclPerfiles)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }







}