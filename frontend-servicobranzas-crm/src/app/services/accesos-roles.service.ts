import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { VariableGlobalService } from './variable-global.service';
import { AclAccesosRoles } from '../models/AclAccesosRoles';

@Injectable({
  providedIn: 'root'
})
export class AccesosRolesService {

  constructor(
    private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) { }

  async GetAccesos(idRol: number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclAccesosRoles/GetAccesos?idRol=${idRol}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(aclAccesosRoles:AclAccesosRoles){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclAccesosRoles/Modificar/`, aclAccesosRoles)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(aclAccesosRoles: AclAccesosRoles){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclAccesosRoles/Crear/`, aclAccesosRoles)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }
}
