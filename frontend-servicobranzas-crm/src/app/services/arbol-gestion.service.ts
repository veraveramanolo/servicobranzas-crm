import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { CrmArbolGestion } from '../models/CrmArbolGestion';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { catchError, map, tap } from 'rxjs/operators';

import { VariableGlobalService } from './variable-global.service';



@Injectable({
  providedIn: 'root'
})
export class ArbolGestionServiceService {

  constructor(private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService) {

  }


  guardar(gestion:CrmArbolGestion):Observable<ModeloRespuesta>{
    return this.http.post<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/Crear`,gestion)
   }
 
  editar(gestion:CrmArbolGestion):Observable<ModeloRespuesta>{
    return this.http.post<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/Modificar`,gestion)
   } 

  all():Observable<ModeloRespuesta>{
    return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/All`);
  }

  primerNivel(IdCuenta:number):Observable<ModeloRespuesta>{
    return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/NivelUno?IdCuenta=${IdCuenta}`);
  }

  tipoGestion():Observable<ModeloRespuesta>{
    return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestionTipoContacto`);
  }

  tipoContacto():Observable<ModeloRespuesta>{
    return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestionTipoContacto`);
  }

  /*primerNivel_():Observable<ModeloRespuesta>{
    return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/NivelUno`)
    .pipe(
      map( resp =>{
      console.log("Entro en el mapa");
      return resp;
    }),
    catchError(this.manejoError.ManejoError)
    );
  }*/

   subNivel(IdParent:number):Observable<ModeloRespuesta>{
      return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/SubNiveles?IdParent=${IdParent}`);
  }

  recibosCaja(fechaini, fechafin):Observable<ModeloRespuesta>{
      return this.http.get<ModeloRespuesta>(`${this.global.url}/CrmArbolGestion/reciboCaja?fechaini=${fechaini}&fechafin=${fechafin}`);
  }
}
