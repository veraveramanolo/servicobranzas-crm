import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";
import { format } from "date-fns";

@Injectable({
  providedIn: 'root'
})
export class MatrizConciliacionService {

  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) { }

  GetAcuerdosXasignacion(idCampania: number, fechinicio: any, fechfin: any): Observable<ModeloRespuesta> {
    const fechaInicio = format(new Date(fechinicio), "yyyy-MM-dd H:m:s");
    const fechaFin = format(new Date(fechfin), "yyyy-MM-dd H:m:s");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/GetAcuerdosXasignacion/?idcampania=${idCampania}&fechainicio=${fechaInicio}&fechafin=${fechaFin}`
    );
  }

  GetAcuerdosXgestion(idCampania: number, fechinicio: any, fechfin: any): Observable<ModeloRespuesta> {
    const fechaInicio = format(new Date(fechinicio), "yyyy-MM-dd H:m:s");
    const fechaFin = format(new Date(fechfin), "yyyy-MM-dd H:m:s");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaAcuerdos/GetAcuerdosXgestion/?idcampania=${idCampania}&fechainicio=${fechaInicio}&fechafin=${fechaFin}`
    );
  }

  GetPagosXasignacion(idCampania: number, fechinicio: any, fechfin: any): Observable<ModeloRespuesta> {
    const fechaInicio = format(new Date(fechinicio), "yyyy-MM-dd H:m:s");
    const fechaFin = format(new Date(fechfin), "yyyy-MM-dd H:m:s");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasCampaniaPagos/GetPagosXasignacion/?idcampania=${idCampania}&fechainicio=${fechaInicio}&fechafin=${fechaFin}`
    );
  }
}
