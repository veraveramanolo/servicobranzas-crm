import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { VariableGlobalService } from "./variable-global.service";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) { }

  GetAclParametros(
    idModulo: number,
    estado: string,
    submodulo: string
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/AclParametros/GetParametros?idModulo=${idModulo}&estado=${estado}&submodulo=${submodulo}`
    );
  }
}
