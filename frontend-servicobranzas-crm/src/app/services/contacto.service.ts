import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";
import { CrmCuentasContacto } from "../models/CrmCuentasContacto";

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) { }

  mostrarContactos(CampaniaID: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasContacto/GetContacto?idCuentaCampania=${CampaniaID}`
    );
  }

  guardarContacto(contacto:CrmCuentasContacto):Observable<ModeloRespuesta>{
    return this.http.post<ModeloRespuesta>(`${this.global.url}/CrmCuentasContacto/Crear`,contacto)
  }
  editarContacto(contacto:CrmCuentasContacto):Observable<ModeloRespuesta>{
    return this.http.post<ModeloRespuesta>(`${this.global.url}/CrmCuentasContacto/Modificar`,contacto)
  }

  mostrarRelacion(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/AclRelacion/All`
    );
  }

  inactivarContacto(inactivarCont: any): Observable<ModeloRespuesta> {
    console.log(inactivarCont);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasContacto/Inactivar`,
      inactivarCont
    );
  }

  /*guardarContacto(
    idCuentaCampania: number,
    contacto: any
  ): Observable<ModeloRespuesta> {
    const contactoJson: CrmCuentasContacto = {
      id: contacto.id,
      idCuenta: Number(idCuentaCampania),
      nombre: contacto.nombre,
      documento: contacto.documento,
      idRelacion: contacto.relacion || null,
      observacion: contacto.observacion,
      idCuentaNavigation: contacto.CrmCuentas || null,
      idRelacionNavigation: contacto.AclRelacion || null
    };
    console.log(contactoJson);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasContacto/Crear`,
      contactoJson
    );
  }*/
}
