import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DepartamentoService {
  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  mostrarDepartamentos(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(`${this.global.url}/AclDepto/All`);
  }
}
