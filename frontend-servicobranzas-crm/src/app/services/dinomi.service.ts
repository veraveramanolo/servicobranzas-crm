import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ManejoErrorMetodo } from "../common/manejo-error";

@Injectable({
  providedIn: "root",
})
export class DinomiService {
  url: string = "192.168.2.183/modules/webservice";
  constructor(private http: HttpClient) {}

  loginDinomi(respuesta): Observable<any> {
    let data = {
      agentnumber: respuesta.data.usuario.agentname,
      agentpassword: respuesta.data.usuario.agentpass,
      agentname: respuesta.data.usuario.agentname,
      extension: respuesta.data.usuario.agentname,
    };
    const post = JSON.stringify(data);
    let body = new HttpParams();
    body = body.set("trama_login_ws", post);
    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");

    // const payload = new HttpParams();
    // payload.set("trama_login_ws", respuesta.toString());

    // const data = new FormData();
    // data.append("trama_login_ws", respuesta.toString());
    // const data = {
    //   agentnumber: "444",
    //   agentpassword: "123",
    //   agentname: "SIP/444",
    //   extension: "444",
    // };
    console.log(body);
    return this.http.post<any>(
      `https://${this.url}/RestControllerWS.php/agentlogin`,
      body,
      { headers, params: { trama_login_ws: post } }
    );
  }

  llamadaDinomi(extencion: string, numero: string): Observable<any> {
    console.log("extencion nueva", extencion);
    return this.http.get(
      `https://${this.url}/manualdial.php?agentname=${extencion}&phone=${numero}`
    );
  }

  smsDinomi(numero: string, mensaje: string): Observable<any> {
    return this.http.get(
      `https://${this.url}/smsopenvox.php?telefono=${numero}&mensaje=${mensaje}`
    );
  }

  enviarSmsMasivo(data: any): Observable<any> {
    return this.http.post(`http://${this.url}/carga_sms.php`, data);
  }

  enviarEmailMasivo(data: any): Observable<any> {
    return this.http.post(`http://${this.url}/carga_email.php`, data);
  } 

  emailDinomi(data: any): Observable<any> {
    const post = JSON.stringify(data);
    console.log(post);
    const emaile = JSON.stringify(data.email);
    const subjecte = JSON.stringify(data.subject);
    const bodye = JSON.stringify(data.body);
    let body = new HttpParams();
    body = body.set("email", emaile);
    body = body.set("body", bodye);
    body = body.set("subject", subjecte);
    //const envio = JSON.parse(data);
    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "application/x-www-form-urlencoded");
    return this.http.post(`https://${this.url}/sendemail.php`, body, {
      headers,
      params: { email: emaile, body: bodye, subject: subjecte },
    });
  }
}
