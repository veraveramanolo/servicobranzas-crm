import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ManejoErrorMetodo } from "../common/manejo-error";
import { Router } from "@angular/router";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { catchError, map } from "rxjs/operators";
import { VariableGlobalService } from "./variable-global.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PlantillaService {
  userToken: string;

  constructor(
    private http: HttpClient,
    private manejoError: ManejoErrorMetodo,
    private rutas: Router,
    private global: VariableGlobalService
  ) {}

  ConsultarPlantillas(): Observable<any> {
    return this.http.get<ModeloRespuesta>(`${this.global.url}/AclPlantilla`);
  }
  ConsultarPlantillasTipo(tipo: string): Observable<any> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/AclPlantilla/PlantillasTipo?tipo=${tipo}`
    );
  }

  leerToken() {
    if (localStorage.getItem("token")) {
      this.userToken = localStorage.getItem("token");
    } else {
      this.userToken = "";
    }

    return this.userToken;
  }

  isAuthenticated(): boolean {
    if (this.leerToken()) {
      return true;
    }
    return false;
  }
}
