import { Injectable } from "@angular/core";

import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ComunicacionService {
  // Observable navItem source
  private _navItemSource = new BehaviorSubject<number>(0);
  // Observable navItem stream
  public navItem$ = this._navItemSource.asObservable();

  private _clientes = new BehaviorSubject<any[]>([]);
  // Observable navItem stream
  public clientes$ = this._clientes.asObservable();
  constructor() {}

  asignarCampania(idcampania: number): void {
    this._navItemSource.next(idcampania);
  }

  asignarClientes(clientes: any[]): void {
    this._clientes.next(clientes);
  }
}
