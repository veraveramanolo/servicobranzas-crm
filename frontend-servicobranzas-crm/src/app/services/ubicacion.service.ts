import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";

import { Observable } from "rxjs";
import { CrmCuentasDireccion } from "../models/CrmCuentasDireccion";

@Injectable({
  providedIn: "root",
})
export class UbicacionService {
  constructor(
    private http: HttpClient,
    // private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService
  ) {}

  mostrarUbicaciones(CampaniaID: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDireccion/GetDireccion?idCuentaCampania=${CampaniaID}`
    );
  }

  mostrarZona(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(`${this.global.url}/AclZona/All`);
  }

  mostrarArea(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(`${this.global.url}/AclArea/All`);
  }

  guardarDireccion(
    idCampania: number,
    direccion: any
  ): Observable<ModeloRespuesta> {
    const direccionJson: CrmCuentasDireccion = {
      idCuenta: Number(idCampania),
      tipo: direccion.tipo,
      valor: direccion.direccion || null,
      idDepto: direccion.depto || null,
      idCiudad: direccion.ciudad || null,
      idDistrito: direccion.distrito || null,
      idZona: direccion.zona || null,
      idArea: direccion.area || null,
      barrio: direccion.barrio || null,
      info1: direccion.info1 || null,
    };
    console.log("direccion", direccionJson);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDireccion/Crear`,
      direccionJson
    );
  }

  editarDireccion(
    idDireccion: number,
    idCampania: number,
    direccion: any
  ): Observable<ModeloRespuesta> {
    const direccionJson: CrmCuentasDireccion = {
      id: idDireccion,
      idCuenta: Number(idCampania),
      tipo: direccion.tipo,
      valor: direccion.direccion || null,
      idDepto: direccion.depto || null,
      idCiudad: direccion.ciudad || null,
      idDistrito: direccion.distrito || null,
      idZona: direccion.zona || null,
      idArea: direccion.area || null,
      barrio: direccion.barrio || null,
      info1: direccion.info1 || null,
    };
    console.log(direccionJson);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDireccion/Modificar`,
      direccionJson
    );
  }

  inactivarDireccion(inactivarDir: any): Observable<ModeloRespuesta> {
    console.log(inactivarDir);
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDireccion/Inactivar`,
      inactivarDir
    );
  }
}
