import { AclUsuario } from "./../models/AclUsuario";
import { Injectable } from "@angular/core";
import { INavData } from "@coreui/angular";
import { DataStateChangeEvent } from "@progress/kendo-angular-grid";
import { State } from "@progress/kendo-data-query";
import { CrmCartera } from "../models/CrmCartera";
import { CrmCampania } from "../models/CrmCampania";
import { ValorDropGestionNormal } from "../models/ValoresFiltros";
import { NavItemsCabecera } from "../models/sidebar-nav";

@Injectable({
  providedIn: "root",
})
export class VariableGlobalService {
  ConsultarUsuarioLogin(): AclUsuario {
    return JSON.parse(
      localStorage.getItem(this.valorusuarioLogueadoSessionStorage)
    );
  }

  ConsultarNavItems(): NavItemsCabecera[] {
    return JSON.parse(
      localStorage.getItem(this.valornavItemsSessionStorage)
    );
  }


  // Filtros Gestión Normal
  public valorDropGestionNormal: ValorDropGestionNormal;

  public cabeceraNasItems = {
    name: "Dashboard",
    url: "/dashboard",
    icon: "icon-speedometer",
    badge: {
      variant: "info",
      text: "NEW",
    },
  };

  public usuarioLogueado: AclUsuario;
  public navItems: NavItemsCabecera [] = [];

  public valorusuarioLogueadoSessionStorage = "VULSS";

  public valornavItemsSessionStorage = "VNISS";

  constructor() {}

  //public url = "https://localhost:44388/api";

  public url = 'http://52.201.43.63/api';

  FuncionTrimFiltroGrid(estadoGrid: DataStateChangeEvent): State {
    let filtroGrid = [];

    if (typeof estadoGrid.filter != "undefined") {
      if (estadoGrid.filter.filters.length > 0) {
        filtroGrid = estadoGrid.filter.filters;
        filtroGrid.forEach((x) => {
          if (typeof x.value === "string") x.value = x.value.trim();
        });
        estadoGrid.filter.filters = filtroGrid;
      }
    }
    return estadoGrid;
  }
}
