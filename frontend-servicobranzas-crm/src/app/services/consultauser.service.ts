import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { Router } from '@angular/router';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AclUsuario } from '../models/AclUsuario';
import { VariableGlobalService } from '../services/variable-global.service';

@Injectable({
  providedIn: 'root'
})
export class ConsultauserService {

  userToken: string;

  constructor(private http: HttpClient, private manejoError: ManejoErrorMetodo, private global: VariableGlobalService,
    private rutas: Router) {

  }


   ConsultarUsuarios() {
    return this.http.get<ModeloRespuesta>(`${this.global.url}/AclUsuario/All`)
        .pipe(
          map( resp =>{
            console.log('Entro en el mapa')
            //this.guardarToken(resp[0]);
            return resp;
          }),
          //retry(2),
          catchError(this.manejoError.ManejoError)
        );
  }

  async ConsultarPermiosAccesos(idUser: number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/Autentificacion/accesos?idUser=${idUser}`)
    .pipe(
      map( resp =>{
        console.log('Entro en el mapa')
        return resp;
      }),
      //retry(2),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  guardarToken( token: string){
    this.userToken = token;
    localStorage.setItem('token', token);
  }

  leerToken() {
    if(localStorage.getItem('token')){
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }
 
  isAuthenticated(): boolean{
  
    if (this.leerToken()) {
      return true;
    }
    return false
  }

  guardar(usuario:AclUsuario):Observable<ModeloRespuesta>{
   return this.http.post<ModeloRespuesta>(`${this.global.url}/AclUsuario/Crear`,usuario)
  }

   editar(usuario:AclUsuario):Observable<ModeloRespuesta>{
   return this.http.post<ModeloRespuesta>(`${this.global.url}/AclUsuario/Modificar`,usuario)
  }

  async Usuario(id: number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclUsuario/Usuario?id=${id}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }
}
