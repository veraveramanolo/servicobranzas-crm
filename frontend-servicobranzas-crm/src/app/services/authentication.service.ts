import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { retry, catchError, map } from "rxjs/operators";
import { ManejoErrorMetodo } from "../common/manejo-error";
import { AuthRequest } from "../models/AuthRequest.model";
import { Router } from "@angular/router";
import { VariableGlobalService } from "./variable-global.service";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  userToken: string;

  constructor(
    private http: HttpClient,
    private manejoError: ManejoErrorMetodo,
    private _rutas: Router,
    private global: VariableGlobalService
  ) {}

  async login(authRequest: AuthRequest) {
    console.log(authRequest);
    return await this.http
      .post<ModeloRespuesta>(
        `${this.global.url}/Autentificacion/login/`,
        authRequest
      )
      .pipe(
        map((resp) => {
          console.log("Entro en el mapa");
          this.guardarToken(resp[0]);
          return resp;
        }),
        //retry(2),
        catchError(this.manejoError.ManejoError)
      )
      .toPromise();
  }

  async recuperarPassword() {}

  // async ConsultarPermiosAccesos(idUser: number) {
  //   return await this.http
  //     .get<ModeloRespuesta>(
  //       `${this.global.url}/Autentificacion/accesos?idUser=${idUser}`
  //     )
  //     .pipe(
  //       map((resp) => {
  //         console.log("Entro en el mapa");
  //         return resp;
  //       }),
  //       //retry(2),
  //       catchError(this.manejoError.ManejoError)
  //     )
  //     .toPromise();
  // }

  guardarToken(token: string) {
    this.userToken = token;
    localStorage.setItem("token", token);
  }

  leerToken() {
    if (localStorage.getItem("token")) {
      this.userToken = localStorage.getItem("token");
    } else {
      this.userToken = "";
    }

    return this.userToken;
  }

  isAuthenticated(): boolean {
    if (this.leerToken()) {
      return true;
    }
    return false;
  }

  deslog(): any {
    localStorage.removeItem("token");
  }
}
