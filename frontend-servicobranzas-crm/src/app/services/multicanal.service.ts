import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { MultiselectResponse } from "../models/MultiselectResponse.models";
import { VariableGlobalService } from "./variable-global.service";
import { format } from "date-fns";
import { CrmTareaObjetivos } from "../models/CrmTareaObjetivos";

@Injectable({
  providedIn: "root",
})
export class MulticanalService {
  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) {}

  obtenerMulticanal(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCampaniaCuentas/GetMulticanal/?idCuentaCampania=${idCuentaCampania}`
    );
  }

  obtenerFiltros(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalCab/GetMulticanalCab?idCampania=${idCuentaCampania}`
    );
  }

  obtenerDetalleFiltros(
    idCuentaCampania: number,
    idMulticanal: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalCab/GetMulticanalFiltro?idCampania=${idCuentaCampania}&idMulticanal=${idMulticanal}`
    );
  }

  crearCabeceraFiltro(cabecera: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalCab/CrearFiltro`,
      cabecera
    );
  }

  // crearDetalleFiltro(detalle: any): Observable<ModeloRespuesta> {
  //   return this.http.post<ModeloRespuesta>(
  //     `${this.global.url}/CrmMulticanalCab/InsertaDetalleMulticanal`,
  //     detalle
  //   );
  // }

  eliminarFiltro(idFiltro: number): Observable<ModeloRespuesta> {
    const data = { Idmulticanal: idFiltro };
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalCab/Inactivar`,
      data
    );
  }

  eliminarTarea(idTarea: number): Observable<ModeloRespuesta> {
    const data = { Id: idTarea };
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmTareas/Inactivar`,
      data
    );
  }

  editarTarea(tarea: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmTareas/ModificarTarea`,
      tarea
    );
  }

  crearTarea(tarea: any): Observable<ModeloRespuesta> {
    console.log("tarea enviada al back", tarea);

    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmTareas/CrearTarea`,
      tarea
    );
  }

  crearMarcador(marcador: any): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalMarcadores/CrearMarcador`,
      marcador
    );
  }

  obtenerMarcadores(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalMarcadores/GetMarcadores?idCampania=${idCuentaCampania}`
    );
  }

  obtenerTareas(idCuentaCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmTareas/AllTareas?idCampania=${idCuentaCampania}`
    );
  }

  filtroFecha(
    idCuentaCampania: number,
    fechaFiltro: Date
  ): Observable<ModeloRespuesta> {
    const dato = format(new Date(fechaFiltro), "yyyy-MM-dd");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCampaniaCuentas/GetMulticanalFecha?idCampania=17&fechafiltro=${dato} 00:00:00`
    );
  }

  obtenerTipoObjetivo(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmTareaObjetivos/GetTipoObjetivos`
    );
  }

  obtenerGetParamObj(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmTareaObjetivos/GetParamObj`
    );
  }

  crearObjetivo(objetivo: CrmTareaObjetivos): Observable<ModeloRespuesta> {
    return this.http.post<ModeloRespuesta>(
      `${this.global.url}/CrmTareaObjetivos/CrearObjetivo`,
      objetivo
    );
  }
  obtenerAsesores(): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/AclUsuario/AllAsesor`
    );
  }
  insertarAsignacion(data: any): Observable<MultiselectResponse> {
    return this.http.post<MultiselectResponse>(
      `${this.global.url}/CrmAsignacionCab/InsertAsignaciones`,
      data
    );
  }
  asignaAsesor(id: number, idtarea: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmAsignacionCab/AsignaAsesor?idAsignacion=${id}&idTarea=${idtarea}`
    );
  }

  obtenerMultiselect(idCampania: number, fecchaFiltro: any): Observable<any> {
    let fecha: any = "";
    if (fecchaFiltro) {
      fecha = format(new Date(fecchaFiltro), "yyyy-MM-dd");
    }
    return this.http.get<any>(
      `${this.global.url}/CrmMulticanalCab/GetMulticanalEstados?idCampania=${idCampania}&fechafiltro=${fecha}`
    );
  }

  obtenerFiltrosGestion(idCampania: number, usercreacion: string): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalCab/GetMulticanalCabGestion?idCampania=${idCampania}&usercreacion=${usercreacion}`
    );
  }
  obtenerDetalleFiltrosGestion(
    idCampania: number,
    idMulticanal: number
  ): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmMulticanalCab/GetMulticanalFiltroGestion?idCampania=${idCampania}&idMulticanal=${idMulticanal}`
    );
  }
}
//api/CrmTareaObjetivos/GetParamObj
