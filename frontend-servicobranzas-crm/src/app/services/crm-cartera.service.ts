import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { VariableGlobalService } from './variable-global.service';
import { catchError, map } from 'rxjs/operators';
import { CrmCartera } from '../models/CrmCartera';

@Injectable({
  providedIn: 'root'
})
export class CrmCarteraService {

  constructor(private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService) { }


  async All() {
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/CrmCartera/All`)
      .pipe(
        map( resp =>{
          return resp;
        }),
        catchError(this.manejoError.ManejoError)
      ).toPromise();
  }


  async Cartera( id:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/CrmCartera/Cartera?id=${id}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(crmCartera:CrmCartera){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/CrmCartera/Modificar/`, crmCartera)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(crmCartera: CrmCartera){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/CrmCartera/Crear/`, crmCartera)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async CarteraCampania( idCatera:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/CrmCartera/CarteraCampania?idCatera=${idCatera}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async GetCarteraGestor( idUsuario:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/CrmCartera/GetCarteraGestor?idUsuario=${idUsuario}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async GetCarteras(){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/CrmCartera/GetCarteras`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

}
