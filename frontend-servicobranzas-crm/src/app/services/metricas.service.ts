import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ModeloRespuesta } from "../models/ModeloRespuesta.models";
import { VariableGlobalService } from "./variable-global.service";
import { format } from "date-fns";

@Injectable({
  providedIn: 'root'
})
export class MetricasService {

  constructor(
    private http: HttpClient,
    private global: VariableGlobalService
  ) { }

  GetGestiones(idCampania: number, fechinicio: Date, fechfin: Date): Observable<ModeloRespuesta> {
    const fechaInicio = format(new Date(fechinicio), "yyyy-MM-dd H:m:s");
    const fechaFin = format(new Date(fechfin), "yyyy-MM-dd H:m:s");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/Metricas/GetGestiones/?idcampania=${idCampania}&fechainicio=${fechaInicio}&fechafin=${fechaFin}`
    );
  }

  GetGestionesHora(idCampania: number, fechinicio: Date, fechfin: Date): Observable<ModeloRespuesta> {
    const fechaInicio = format(new Date(fechinicio), "yyyy-MM-dd H:m:s");
    const fechaFin = format(new Date(fechfin), "yyyy-MM-dd H:m:s");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/Metricas/GetGestionesHora/?idcampania=${idCampania}&fechainicio=${fechaInicio}&fechafin=${fechaFin}`
    );
  }
  
  GetEstadosCliente(idCampania: number, fechinicio: Date, fechfin: Date): Observable<ModeloRespuesta> {
    const fechaInicio = format(new Date(fechinicio), "yyyy-MM-dd H:m:s");
    const fechaFin = format(new Date(fechfin), "yyyy-MM-dd H:m:s");
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/Metricas/GetEstadosCliente/?idcampania=${idCampania}&fechainicio=${fechaInicio}&fechafin=${fechaFin}`
    );
  }

  mostrarDetalleCuentaCampania(idCampania: number): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDetalle/GetDetalleCuentasCampania?idCampania=${idCampania}`
    );
  }

  mostrarFiltraValoresXnombre(idCampania: number, nombre: string): Observable<ModeloRespuesta> {
    return this.http.get<ModeloRespuesta>(
      `${this.global.url}/CrmCuentasDetalle/FiltraValoresXnombre?idCampania=${idCampania}&nombre=${nombre}`
    );
  }
}
