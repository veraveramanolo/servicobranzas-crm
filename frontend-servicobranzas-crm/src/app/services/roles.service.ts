import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { ManejoErrorMetodo } from '../common/manejo-error';
import { AclRoles } from '../models/AclRoles';
import { ModeloRespuesta } from '../models/ModeloRespuesta.models';
import { VariableGlobalService } from './variable-global.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient, private manejoError: ManejoErrorMetodo,
    private global: VariableGlobalService) { 

  }


  async All(){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclRoles/All`)
    .pipe(
      map( resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async TipoModulo( id:number){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclRoles/TipoModulo?id=${id}`)
    .pipe(
      map(resp => {
        return resp
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async Modificar(aclRoles: AclRoles){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclRoles/Modificar/`, aclRoles)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }


  async Crear(aclRoles: AclRoles){
    return await this.http.post<ModeloRespuesta>(`${this.global.url}/AclRoles/Crear/`, aclRoles)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

  async CambiarEstado(id: number, estado: number, UsrModificacion: string){
    return await this.http.get<ModeloRespuesta>(`${this.global.url}/AclRoles/CambiarEstado?id=${id}&estado=${estado}&UsrModificacion=${UsrModificacion}`)
    .pipe(
      map(resp =>{
        return resp;
      }),
      catchError(this.manejoError.ManejoError)
    ).toPromise();
  }

}
