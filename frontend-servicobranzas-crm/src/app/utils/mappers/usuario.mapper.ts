import { AclUsuario } from "../../models/AclUsuario";
import { Md5 } from "md5-typescript";

export class UsuarioMapper {
  public static editar(usuario: any, idUsuario: number): AclUsuario {
    console.log(idUsuario);
    const partiaUsuario: AclUsuario = {
      idRol: usuario.rol ?? null,
      aclUsuarioPerfil: [],
      agentname: usuario.agentname ?? null,
      agentpass: usuario.agentpass ?? null,
      anulado: usuario.anulado ?? null,
      apellidos: usuario.apellidos ?? null,
      cargo: usuario.cargo ?? null,
      cedula: usuario.cedula ?? null,
      codigo: usuario.codigo ?? null,
      departamento: usuario.departamento ?? null,
      email: usuario.email ?? null,
      fechaCreacion: new Date(),
      fechaModificacion: usuario.fechaModificacion ?? null,
      idUsuario: idUsuario,
      md5Password: "81dc9bdb52d04dc20036dbd8313ed055",
      nombres: usuario.nombres ?? null,
      telefono: usuario.telefono ?? null,
      userName: usuario.userName ?? null,
      usrCreacion: "jcontreras",
      usrModificacion: usuario.usrModificacion ?? null,
    };
    Object.keys(partiaUsuario).forEach(
      (key) => partiaUsuario[key] === null && delete partiaUsuario[key]
    );

    return partiaUsuario;
  }

  public static crear(usuario: any): AclUsuario {
    const partiaUsuario: AclUsuario = {
      idRol: usuario.rol ?? null,
      aclUsuarioPerfil: [],
      agentname: usuario.agentname ?? null,
      agentpass: usuario.agentpass ?? null,
      anulado: usuario.anulado ?? null,
      apellidos: usuario.apellidos ?? null,
      cargo: usuario.cargo ?? null,
      cedula: usuario.cedula ?? null,
      codigo: usuario.codigo ?? null,
      departamento: usuario.departamento ?? null,
      email: usuario.email ?? null,
      fechaCreacion: new Date(),

      fechaModificacion: usuario.fechaModificacion ?? null,
      idUsuario: 0,
      md5Password: Md5.init(usuario.md5Password) ?? null,
      nombres: usuario.nombres ?? null,
      telefono: usuario.telefono ?? null,
      userName: usuario.userName ?? null,
      usrCreacion: "jcontreras",
      usrModificacion: usuario.usrModificacion ?? null,
    };

    return partiaUsuario;
  }
}
