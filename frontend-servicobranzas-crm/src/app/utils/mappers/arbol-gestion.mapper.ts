import { CrmArbolGestion } from "../../models/CrmArbolGestion";

export class GestionMapper {
  public static editar(gestion: any, id: number): CrmArbolGestion {
    console.log(id);
    const partiaGestion: CrmArbolGestion = {
      id: id,
      idParent: gestion.idParent ?? null,
      descripcion: gestion.descripcion ?? null,
      peso: Number(gestion.peso) ?? null,
      estado: gestion.estado ?? null,
      idTipoGestion: gestion.idTipoGestion ?? null,
      idTipoContacto: gestion.idTipoContacto ?? null,
      alerta: gestion.alerta ?? null,
      idParentNavigation: gestion.idParentNavigation ?? null,
      idTipoContactoNavigation: gestion.idTipoContactoNavigation ?? null,
      idTipoGestionNavigation: gestion.idTipoGestionNavigation ?? null,
      crmCuentasCampania: gestion.crmCuentasCampania ?? null,
      crmGestion: gestion.crmGestion ?? null,
      inverseIdParentNavigation: gestion.inverseIdParentNavigation ?? null,
      labelEstado: gestion.labelEstado ?? null,
      labelNivel2: gestion.labelNivel2 ?? null,
    };
    Object.keys(partiaGestion).forEach(
      (key) => partiaGestion[key] === null && delete partiaGestion[key]
    );

    return partiaGestion;
  }

  public static crear(gestion: any): CrmArbolGestion {
    const partiaGestion: CrmArbolGestion = {
      id: 0,
      idParent: gestion.idParent ?? null,
      descripcion: gestion.descripcion ?? null,
      peso: Number(gestion.peso) ?? null,
      estado: gestion.estado ?? null,
      idTipoGestion: gestion.idTipoGestion ?? null,
      idTipoContacto: gestion.idTipoContacto ?? null,
      alerta: gestion.alerta ?? null,
      idParentNavigation: gestion.idParentNavigation ?? null,
      idTipoContactoNavigation: gestion.idTipoContactoNavigation ?? null,
      idTipoGestionNavigation: gestion.idTipoGestionNavigation ?? null,
      crmCuentasCampania: gestion.crmCuentasCampania ?? null,
      crmGestion: gestion.crmGestion ?? null,
      inverseIdParentNavigation: gestion.inverseIdParentNavigation ?? null,
      labelEstado: gestion.labelEstado ?? null,
      labelNivel2: gestion.labelNivel2 ?? null,
    };

    return partiaGestion;
  }
}
