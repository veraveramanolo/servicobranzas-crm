import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class ManejoErrorMetodo {
    public ManejoError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // Error de red o del lado del cliente.
            console.error('Un error ha ocurrido: ', error.error.message);
            console.log(error);
            if(error.status == 401){
            	swal.fire('Alerta del Sistema', 'Sesión caducada.', 'error');
            	window.location.href = '/#/login';
            }else{
            	swal.fire('Alerta del Sistema', 'Algo salió mal. Por favor verifique su conexión.', 'error');
            }
        } else {
            // El backend retornó un código de error.
            console.error(`Backend retorna codigo ${error.status}, ` + `cuerpo de petición: ${error.error}`);
            if(error.status == 401){
            	swal.fire('Alerta del Sistema', 'Sesión caducada.', 'error');
            	window.location.href = '/#/login';
            }else{
            	swal.fire('Alerta del Sistema', 'Algo salió mal. Por favor intente luego.', 'error');
            }
        }
        // retorna un observable con el mensaje del error.
        return throwError(error);
    };
}
