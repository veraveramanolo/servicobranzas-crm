import { NavItemsCabecera } from "./models/sidebar-nav";

export const navItems: NavItemsCabecera[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  // {
  //   title: true,
  //   name: 'Theme'
  // },
  // {
  //   name: 'Colors',
  //   url: '/theme/colors',
  //   icon: 'icon-drop'
  // },
  // {
  //   name: 'Typography',
  //   url: '/theme/typography',
  //   icon: 'icon-pencil'
  // },

  {
    name: 'Mantenimientos',
    url: '/mantenimientos',
    icon: 'fa fa-cog',
    children: [
      // {
      //   name: 'Perfiles',
      //   url: '/mantenimientos/perfil'
      // },
      {
        name: 'Roles y Accesos',
        url: '/mantenimientos/roles-accesos'
      },
      {
        name: 'Usuarios',
        url: '/mantenimientos/usuario',
      },
      {
        name: 'Acciones',
        url: '/mantenimientos/acciones'
      },
      // {
      //   name: 'Tipos de Módulos',
      //   url: '/mantenimientos/tipos-modulos'
      // },
      {
        name: 'Módulos',
        url: '/mantenimientos/modulos'
      },

      {
        name: 'Plantillas',
        url: '/mantenimientos/plantilla'
      }
    ]
  },
  {
    name: 'Gestionar',
    url: '/gestionar',
    icon: 'fa fa-tasks',
    children: [
      {
        name: 'Gestión Normal',
        url: '/gestionar/gestion-normal'
      },
      {
        name: 'Tareas',
        url: '/gestionar/tareas'
      }
    ]
  },
  {
    name: 'Multicanal',
    url: '/multicanal',
    icon: 'fa fa-feed',
    children: [
      {
        name: 'Correo',
        url: '/multicanal/correo'
      },
      {
        name: 'Dinomi',
        url: '/multicanal/dinomi'
      },
      {
        name: 'Sms',
        url: '/multicanal/sms'
      },
      {
        name: 'Text to Speech',
        url: '/multicanal/text-to-speech'
      }
    ]
  },
  {
    name: 'Tablero Control',
    url: '/tablero-control',
    icon: 'fa fa-line-chart',
    children: [
      {
        name: 'Productitvidad',
        url: '/tablero-control/productividad'
      },
      {
        name: 'Efectividad',
        url: '/tablero-control/efectividad'
      }
    ]
  },
  {
    name: 'Metricas',
    url: '/metricas',
    icon: 'fa fa-line-chart',
    children: [
      {
        name: 'Indicadores',
        url: '/metricas/indicadores'
      },
      {
        name: 'Efectividad',
        url: '/metricas/matriz-conciliacion'
      }
    ]
  },
  {
    name: 'Asignación',
    url: '/asignacion',
    icon: 'fa fa-line-chart',
    children: [
      {
        name: 'Asignacion',
        url: '/asignacion/asignacion'
      },
      {
        name: 'Cambiar Estado',
        url: '/asignacion/cambiar-estado'
      },
      {
        name: 'Remover asignación',
        url: '/asignacion/remover-asignacion'
      }
    ]
  },
  {
    name: 'Comfiguración de Campañas',
    url: '/configuracion-campanias',
    icon: 'fa fa-line-chart',
    children: [
      {
        name: 'Componentes',
        url: '/configuracion-campanias/componentes'
      },
      {
        name: 'Arbol de Gestiones',
        url: '/configuracion-campanias/arbol-gestiones'
      },
      {
        name: 'Campañas Multicanal',
        url: '/configuracion-campanias/campanias-multicanal'
      }
    ]
  },
  {
    name: 'Generar Visitas',
    url: '/generar',
    icon: 'icon-calendar'
  },
  {
    name: 'Paleta de gestion',
    url: '/paletagestion',
    icon: 'icon-drop'
  },
  {
    name: 'Carga de contactos',
    url: '/camp_cargas',
    icon: 'icon-pencil'
  },
  {
    name: 'Configuracion de campañas',
    url: '/camp_configuracion',
    icon: 'icon-pencil'
  },

  {
    name: 'Multicanal',
    url: '/multicanal',
    icon: 'icon-paper-plane',
    children: [
      {
        name: 'Sms',
        url: '/multicanal/sms',
        icon: 'icon-paper-plane'
      },
      {
        name: 'Correos electrónicos',
        url: '/multicanal/correos',
        icon: 'icon-paper-plane'
      },
      {
        name: 'Dinomi',
        url: '/multicanal/dinomi',
        icon: 'icon-paper-plane'
      },
      {
        name: 'Whatsapp',
        url: '/multicanal/whatsapp',
        icon: 'icon-paper-plane'
      }
    ]
  },
 
];
