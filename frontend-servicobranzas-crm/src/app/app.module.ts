import { BrowserModule } from "@angular/platform-browser";
import { LOCALE_ID, NgModule } from "@angular/core";
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { formatDate } from '@angular/common';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

import { AppComponent } from "./app.component";

import { DefaultLayoutComponent } from "./containers";

import { P404Component } from "./views/error/404.component";
import { P500Component } from "./views/error/500.component";
import { LoginComponent } from "./views/login/login.component";
import { RegisterComponent } from "./views/register/register.component";

const APP_CONTAINERS = [DefaultLayoutComponent];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from "@coreui/angular";

// Import routing module
import { AppRoutingModule } from "./app.routing";

// Import 3rd party components
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TabsModule } from "ngx-bootstrap/tabs";
import { ChartsModule } from "ng2-charts";
import { GridModule } from "@progress/kendo-angular-grid";
import { HttpConfigInterceptor } from "./helpers/httpconfig.interceptor";

//import { NgxCaptchaModule } from 'ngx-captcha';
import { ErrorInterceptorService } from "./helpers/error.interceptor";
import { LayoutModule } from "@progress/kendo-angular-layout";
import "@progress/kendo-angular-intl/locales/es-EC/all";
import { DialogsModule } from "@progress/kendo-angular-dialog";
import "@progress/kendo-angular-intl/locales/es/all";

//notificaciones

import { SimpleNotificationsModule } from "angular2-notifications";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "../environments/environment";

import { NotificationModule } from "@progress/kendo-angular-notification";

//captcha
import { CaptchaModule } from "primeng/captcha";
import { ToastModule } from "primeng/toast";

import { SnotifyModule, SnotifyService, ToastDefaults } from "ng-snotify";
import { ModalNotificacionComponent } from "./containers/modal-notificacion/modal-notificacion.component";

//modal
import { ModalModule } from "ngx-bootstrap/modal";
import { UploadModule } from '@progress/kendo-angular-upload';
import { LloacanaComponent } from './lloacana/lloacana.component';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    SnotifyModule.forRoot(),
    ToastModule,
    NotificationModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule,
    HttpClientModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    LayoutModule,
    DialogsModule,
    SimpleNotificationsModule.forRoot(),

    StoreModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    CaptchaModule,
    ModalModule.forRoot(),
    UploadModule,

    //NgxCaptchaModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    ModalNotificacionComponent,
    LloacanaComponent,
  ],
  entryComponents: [ModalNotificacionComponent],
  providers: [
    {
      provide: "SnotifyToastConfig",
      useValue: ToastDefaults,
    },
    SnotifyService,

    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true,
    },
    { provide: LOCALE_ID, useValue: "es-EC" },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
