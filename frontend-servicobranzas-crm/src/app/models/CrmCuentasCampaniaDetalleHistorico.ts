import { CrmCuentasCampania } from "./CrmCuentasCampania";

export interface CrmCuentasCampaniaDetalleHistorico {
    id: number;
    idCuentaCampania: number | null;
    campo: string;
    valor: string;
    idCuentaCampaniaNavigation: CrmCuentasCampania;
}