export interface AclPlantilla {
    idPlantilla: number;
    nombrePlantilla: string;
    bodyPlantilla: string;
    estadoPlantilla: string;
    fechaCreacion: string;
    fechaActualizacion: string | null;
    usrCreacion: string;
    usrModificacion: string;
    tipoPlantilla: string;
}