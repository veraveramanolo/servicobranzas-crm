import { AclPerfiles } from "./AclPerfiles";
import { AclUsuario } from "./AclUsuario";

export interface AclUsuarioPerfil {
    idUsuarioPerfil: number;
    idUsuario: number;
    idPerfil: number;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string;
    usrModificacion: string;
    fechaModificacion: string | null;
    idPerfilNavigation: AclPerfiles;
    idUsuarioNavigation: AclUsuario;
}