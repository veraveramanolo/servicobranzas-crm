import { AclAccesosRoles } from "./AclAccesosRoles";
import { AclModuloAcciones } from "./AclModuloAcciones";

export interface AclModulos {
    idModulo: number;
    codigo: string;
    nombre: string;
    icono: string;
    moduloPadre: string;
    rutaDirectorio: string;
    nombreParentModulo: string;
    idParentModulo: number | null;
    visible: string;
    ordenPresentacion: number | null;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string | null;
    usrModificacion: string;
    fechaModificacion: string | null;
    idParentModuloNavigation: AclModulos;
    aclAccesosRoles: AclAccesosRoles[];
    aclModuloAcciones: AclModuloAcciones[];
    inverseIdParentModuloNavigation: AclModulos[];
}