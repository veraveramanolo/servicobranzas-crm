import { AclModulos } from "./AclModulos";

export interface AclTipoModulos {
    idTipoModulos: number;
    descripcion: string;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string;
    usrModificacion: string;
    fechaModificacion: string;
    aclModulos: AclModulos[];
}