import { CrmCartera } from './CrmCartera';
import { CrmCampania } from './CrmCampania';

export interface ValorDropGestionNormal{
    cartera?: CrmCartera,
    campania?: CrmCampania
}