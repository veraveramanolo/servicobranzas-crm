/*export interface CrmConsultaGestionCampania {
  id?: string;
  idcuenta: number | null;
  idcuentacampania: number | null;
  asesor: string;
  nombrecliente: string;
  cedula: string;
  ultimagestion: string;
  acuerdo: string;
  valoracuerdo: number | null;
  pago: string;
  valorpago: number | null;
  cuotaspendientes: number | null;
  montoinicial: number | null;
  estado: string;
  idproducto?: number;
}*/

export interface CrmConsultaGestionCampania {
  id?: string;
  idcuentacampania: number | null;
  idcuenta: number | null;
  asesor: string;
  nombrecliente: string;
  cedula: string;
  ultimagestion: string;
  acuerdo: string;
  valoracuerdo: number | null;
  pago: string;
  valorpago: number | null;
  idproducto: number | null;
  cuotaspendientes: number | null;
  deudavencida: number | null;
  deudatotal: number | null;
  estado: string | null;
  extra1: string | null;
  extra2: string | null;
  extra3: string | null;
  extra4: string | null;
  extra5: string | null;
  extra6: string | null;
  extra7: string | null;
  extra8: string | null;
  extra9: string | null;
  extra10: string | null;
  labelEstado: string | null;
}
