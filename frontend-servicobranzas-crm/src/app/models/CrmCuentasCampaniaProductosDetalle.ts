import { CrmCuentasCampaniaProductos } from "./CrmCuentasCampaniaProductos";

export interface CrmCuentasCampaniaProductosDetalle {
    id: number;
    idCuentaCampaniaProducto: number | null;
    descripcion: string;
    valor: number | null;
    idCuentaCampaniaProductoNavigation: CrmCuentasCampaniaProductos;
}