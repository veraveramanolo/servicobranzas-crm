import { CrmCampania } from "./CrmCampania";
import { CrmCartera } from "./CrmCartera";
import { CrmCuentasCampania } from "./CrmCuentasCampania";
import { CrmCuentasDetalle } from "./CrmCuentasDetalle";
import { CrmCuentasDireccion } from "./CrmCuentasDireccion";
import { CrmCuentasTelefono } from "./CrmCuentasTelefono";
//import { CrmCuentasEmail } from "./CrmCuentasEmail";

export interface CrmCuentas {
    id: number;
    nombre: string;
    idCarteraInicial: number | null;
    idCampaniaInicial: number | null;
    identificacion: string;
    fechaCreacion: string | null;
    estado: string;
    idCampaniaInicialNavigation: CrmCampania;
    idCarteraInicialNavigation: CrmCartera;
    crmCuentasCampania: CrmCuentasCampania[];
    crmCuentasDetalle: CrmCuentasDetalle[];
    crmCuentasDireccion: CrmCuentasDireccion[];
   // crmCuentasEmail: CrmCuentasEmail[];
    crmCuentasTelefono: CrmCuentasTelefono[];
}
