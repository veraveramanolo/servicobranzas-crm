import { CrmCartera } from "./CrmCartera";
import { CrmCuentas } from "./CrmCuentas";
import { CrmCuentasCampania } from "./CrmCuentasCampania";

export interface CrmCampania {
    id: number;
    nombre: string;
    idCartera: number | null;
    estado: string;
    idCarteraNavigation: CrmCartera;
    crmCuentas: CrmCuentas[];
    crmCuentasCampania: CrmCuentasCampania[];
}