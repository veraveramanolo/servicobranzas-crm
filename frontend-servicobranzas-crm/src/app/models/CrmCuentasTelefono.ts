import { CrmCuentas } from "./CrmCuentas";
import { CrmGestion } from "./CrmGestion";
import { AclCiudad } from "./AclCiudad";
import { AclDepto } from "./AclDepto";
import { AclDistrito } from "./AclDistrito";

export interface CrmCuentasTelefono {
  id: number;
  idCuenta: number | null;
  tipo: string;
  valor: string;
  idDepto: number | null;
  idCiudad: number | null;
  idDistrito: number | null;
  info2: string;
  fechaCreacion: string | null;
  fechaModificacion: string | null;
  estado: string;
  idCiudadNavigation: AclCiudad;
  idCuentaNavigation: CrmCuentas;
  idDeptoNavigation: AclDepto;
  idDistritoNavigation: AclDistrito;
  crmGestion: CrmGestion[];
}

export interface CrearCuentaTelefonoModel {
  idCuenta: number;
  tipo?: string | null;
  valor: string;
  idDepto?: number | null;
  idCiudad?: number | null;
  idDistrito?: number | null;
  info2?: string;
}

export interface editarCuentaTelefonoModel {
  id: number;
  idCuenta: number;
  tipo?: string | null;
  valor: string;
  idDepto?: number | null;
  idCiudad?: number | null;
  idDistrito?: number | null;
  info2?: string;
  estado?: string;
  fechaModificacion?: string | null;
}
