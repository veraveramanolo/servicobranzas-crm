import { CrmCuentasCampaniaProductos } from "./CrmCuentasCampaniaProductos";
import { AclUsuario } from "./AclUsuario";
import { CrmCuentasCampaniaPagos } from "./CrmCuentasCampaniaPagos";
import { CrmCuentasDireccion } from "./CrmCuentasDireccion";

export interface CrmCuentasCampaniaAcuerdos {
    id: number;
    fechaCreacion: string | null;
    fechaAcuerdo: string | null;
    tipoAcuerdo: string;
    valorAcuerdo: number | null;
    idCuentaCampaniaProducto: number | null;
    idGestor: number | null;
    efecto: string;
    estado: string;
    cuota: number | null;
    plazo: string;
    probabilidad: string;
    comentario: string;
    fechaVisita: string | null;
    horaAcuerdo: string | null;
    idDireccion: number | null;
    estadoNot: string | null;
    idCuentaCampaniaProductoNavigation: CrmCuentasCampaniaProductos;
    idDireccionNavigation: CrmCuentasDireccion;
    idGestorNavigation: AclUsuario;
    crmCuentasCampaniaPagos: CrmCuentasCampaniaPagos[];
}