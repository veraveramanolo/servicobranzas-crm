import { AclModulos } from "./AclModulos";
import { AclRoles } from "./AclRoles";

export interface AclAccesosRoles {
    idAccesoRol: number;
    idRol: number | null;
    idModulo: number | null;
    anulado: string;
    nombreModulo: string;
    usrCreacion: string;
    fechaCreacion: string;
    usrModificacion: string;
    fechaModificacion: string | null;
    idModuloNavigation: AclModulos;
    idRolNavigation: AclRoles;
}