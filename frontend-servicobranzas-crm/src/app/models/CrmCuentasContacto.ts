import { CrmCuentas } from "./CrmCuentas";
import { AclRelacion } from "./AclRelacion";

export interface CrmCuentasContacto {
    id: number;
    idCuenta: number | null;
    nombre: string;
    documento: string;
    idRelacion: number | null;
    observacion: string;
    estado: string;
    fechaCreacion: string | null;
    fechaModificacion: string | null;
    idCuentaNavigation: CrmCuentas;
    idRelacionNavigation: AclRelacion;
}