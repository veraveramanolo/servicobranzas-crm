import { CrmCuentas } from "./CrmCuentas";

export interface CrmCuentasDetalle {
    id: number;
    idCuenta: number | null;
    campo: string;
    valor: string;
    idCuentaNavigation: CrmCuentas;
}