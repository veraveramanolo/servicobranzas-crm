export interface LeerMulticanal {
  asesor: string;
  nocuenta: number;
  nombrecompleto: string;
  identificacion: string;
  ultimagestion: Date;
  gestion: string;
  diasmora: string | null;
  mejorgestion: string;
  deudatotal: number | null;
  statuscartera: string;
  fechaacuerdo: Date | null;
  fechamejorgestion: Date | null;
  valoracuerdo: number;
  valorpago: number;
  fechaPago: Date | null;
  totalacuerdo: number;
  totalpago: number;
  cuota: number;
  totalcuotas: number;
}
