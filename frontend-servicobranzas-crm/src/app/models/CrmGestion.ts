export interface CrmGestion {
    id: number;
    idCuentasCampaniaProductos: number | null;
    idTelefono: number | null;
    horaGestion: string | null;
    idGestionDinomi: number | null;
    tiempoLlamada: number | null;
    estadoLlamada: string;
    //idCuentasCampaniaProductosNavigation: CrmCuentasCampaniaProductos;
}