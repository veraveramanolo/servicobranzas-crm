import { CrmArbolGestion } from "./CrmArbolGestion";

export interface CrmArbolGestionTipoGestion {
    id: number;
    texto: string;
    crmArbolGestion: CrmArbolGestion[];
}