import { CrmCuentas } from "./CrmCuentas";
export interface CrmCuentasEmail {
    id: number;
    idCuenta: number | null;
    email: string;
    tipo: string;
    info: string;
    fechaCreacion: string | null;
    fechaModificacion: string | null;
    estado: string;
    idCuentaNavigation: CrmCuentas;
}