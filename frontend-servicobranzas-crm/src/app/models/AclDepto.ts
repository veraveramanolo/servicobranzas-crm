import { AclCiudad } from "./AclCiudad";
import { CrmCuentasDireccion } from "./CrmCuentasDireccion";
import { CrmCuentasTelefono } from "./CrmCuentasTelefono";

export interface AclDepto {
    idDepto: number;
    deptoValor: string;
    aclCiudad: AclCiudad[];
    crmCuentasDireccion: CrmCuentasDireccion[];
    crmCuentasTelefono: CrmCuentasTelefono[];
}