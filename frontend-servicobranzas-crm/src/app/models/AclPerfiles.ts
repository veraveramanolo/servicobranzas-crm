import { AclPerfilRol } from "./AclPerfilRol";
import { AclUsuarioPerfil } from "./AclUsuarioPerfil";

export interface AclPerfiles {
    idPerfil: number;
    descripcion: string;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string | null;
    usrModificacion: string;
    fechaModificacion: string | null;
    aclPerfilRol: AclPerfilRol[];
    aclUsuarioPerfil: AclUsuarioPerfil[];
}