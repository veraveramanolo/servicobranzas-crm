import { CrmArbolGestion } from "./CrmArbolGestion";

export interface CrmArbolGestionTipoContacto {
    id: number;
    texto: string;
    crmArbolGestion: CrmArbolGestion[];
}