import { AclCiudad } from "./AclCiudad";
import { CrmCuentasDireccion } from "./CrmCuentasDireccion";
import { CrmCuentasTelefono } from "./CrmCuentasTelefono";

export interface AclDistrito {
    idDistrito: number;
    idCiudad: number | null;
    distritoValor: string;
    idCiudadNavigation: AclCiudad;
    crmCuentasDireccion: CrmCuentasDireccion[];
    crmCuentasTelefono: CrmCuentasTelefono[];
}