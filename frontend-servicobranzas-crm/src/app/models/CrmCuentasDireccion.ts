import { CrmCuentas } from "./CrmCuentas";
import { AclArea } from "./AclArea";
import { AclCiudad } from "./AclCiudad";
import { AclDepto } from "./AclDepto";
import { AclDistrito } from "./AclDistrito";
import { AclZona } from "./AclZona";
import { CrmCuentasCampaniaAcuerdos } from "./CrmCuentasCampaniaAcuerdos";

// export interface CrmCuentasDireccion {
//   id?: number;
//   idCuenta: number | null;
//   tipo: string;
//   valor: string;
//   idDepto: number | null;
//   idCiudad: number | null;
//   idDistrito: number | null;
//   idZona: number | null;
//   idArea: string;
//   barrio: string;
//   info1: string;
//   fechaCreacion?: string | null;
//   fechaModificacion?: string | null;
//   idCuentaNavigation?: CrmCuentas;
// }
export interface CrmCuentasDireccion {
  id?: number;
  idCuenta: number | null;
  tipo: string;
  valor: string;
  idDepto: number | null;
  idCiudad: number | null;
  idDistrito: number | null;
  idZona: number | null;
  idArea: number | null;
  barrio: string;
  info1: string;
  fechaCreacion?: string | null;
  fechaModificacion?: string | null;
  estado?: string;
  idAreaNavigation?: AclArea;
  idCiudadNavigation?: AclCiudad;
  idCuentaNavigation?: CrmCuentas;
  idDeptoNavigation?: AclDepto;
  idDistritoNavigation?: AclDistrito;
  idZonaNavigation?: AclZona;
  crmCuentasCampaniaAcuerdos?: CrmCuentasCampaniaAcuerdos[];
}
