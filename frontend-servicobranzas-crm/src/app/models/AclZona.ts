import { CrmCuentasDireccion } from "./CrmCuentasDireccion";

export interface AclZona {
    idZona: number;
    zonaValor: string;
    crmCuentasDireccion: CrmCuentasDireccion[];
}