import { CrmCuentasCampania } from "./CrmCuentasCampania";
import { CrmCuentasCampaniaProductosDetalle } from "./CrmCuentasCampaniaProductosDetalle";
import { CrmGestion } from "./CrmGestion";

export interface CrmCuentasCampaniaProductos {
    id: number;
    idCuentaCampania: number | null;
    valor: number | null;
    descripcion: string;
    idCuentaCampaniaNavigation: CrmCuentasCampania;
    crmCuentasCampaniaProductosDetalle: CrmCuentasCampaniaProductosDetalle[];
    crmGestion: CrmGestion[];
}