import { AclUsuarioPerfil } from "./AclUsuarioPerfil";

export interface AclUsuario {
  idRol: number;
  idUsuario?: number;
  userName: string;
  codigo: string;
  cedula: string;
  nombres: string;
  apellidos: string;
  email: string;
  cargo: string;
  departamento: string;
  telefono: string;
  agentname?: string;
  agentpass?: string;
  anulado: string;
  md5Password: string;
  usrCreacion: string;
  fechaCreacion: Date;
  usrModificacion: string;
  fechaModificacion: string | null;
  aclUsuarioPerfil: AclUsuarioPerfil[];
}
