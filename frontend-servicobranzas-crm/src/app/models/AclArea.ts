import { CrmCuentasDireccion } from "./CrmCuentasDireccion";

export interface AclArea {
    idArea: number;
    valorArea: string;
    crmCuentasDireccion: CrmCuentasDireccion[];
}