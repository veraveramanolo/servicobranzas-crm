export interface Child {
    name?: string;
    url?: string;
    icon?: string;
}

export interface NavItemsResponse {
    name?: string;
    url?: string;
    icon?: string;
    children?: Child[];
}

export interface NavItemsCabecera {
    name?: string;
    url?: string;
    icon?: string;
    children?: NavItemsResponse[];
}
