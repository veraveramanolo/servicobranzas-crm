import { AclModuloAcciones } from "./AclModuloAcciones";

export interface AclAcciones {
    idAccion: number;
    codigo: string;
    nombre: string;
    icono: string;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string | null;
    usrModificacion: string;
    fechaModificacion: string | null;
    aclModuloAcciones: AclModuloAcciones[];
}