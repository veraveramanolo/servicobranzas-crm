export interface CrmTareaObjetivos {
  idTarea: number;
  idTipoobjetivo: number;
  idParametro: number | null;
  caValorparametro: number | null;
  valorparametroHabilitar: boolean | null;
  valorparametroFinalizar: boolean | null;
  caValor: number | null;
  caValorhabilitar: boolean | null;
  caValorfinalizar: boolean | null;
  caCantidad: number | null;
  caCantidadhabilitar: boolean | null;
  caCantidadfinalizar: boolean | null;
  idgestionPrincipal: number | null;
  idgestionSec1: number | null;
  idgestionSec2: number | null;
  tcCantidad: number | null;
  tcCantidadhabilitar: boolean | null;
  tcCantidadfinalizar: boolean | null;
  tcValor: number | null;
  tcValorhabilitar: boolean | null;
  tcValorfinalizar: boolean | null;
  pCantidad: number | null;
  pCantidadhabilitar: boolean | null;
  pCantidadfinalizar: boolean | null;
}
