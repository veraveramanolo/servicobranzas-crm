import { CrmCuentasContacto } from "./CrmCuentasContacto";

export interface AclRelacion {
    idRelacion: number;
    valorRelacion: string;
    crmCuentasContacto: CrmCuentasContacto[];
}