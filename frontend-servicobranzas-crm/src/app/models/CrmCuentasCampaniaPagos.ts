import { CrmCuentasCampaniaAcuerdos } from "./CrmCuentasCampaniaAcuerdos";
import { CrmCuentasCampaniaProductos } from "./CrmCuentasCampaniaProductos";

export interface CrmCuentasCampaniaPagos {
    id: number;
    fechaCreacion: string | null;
    fechaPago: string | null;
    valorPago: number | null;
    idCuentaCampaniaProducto: number | null;
    idCuentaCampaniaAcuerdo: number | null;
    tipoPago: string;
    numeroRecibo: string;
    cuota: number | null;
    plazo: string;
    medioPago: string;
    idCuentaCampaniaAcuerdoNavigation: CrmCuentasCampaniaAcuerdos;
    idCuentaCampaniaProductoNavigation: CrmCuentasCampaniaProductos;
}