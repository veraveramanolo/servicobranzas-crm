import { CrmCampania } from "./CrmCampania";
import { CrmCuentas } from "./CrmCuentas";
import { CrmCuentasCampaniaDetalleHistorico } from "./CrmCuentasCampaniaDetalleHistorico";
import { CrmCuentasCampaniaProductos } from "./CrmCuentasCampaniaProductos";

export interface CrmCuentasCampania {
    id: number;
    idCuenta: number | null;
    idCampania: number | null;
    estado: string;
    idCampaniaNavigation: CrmCampania;
    idCuentaNavigation: CrmCuentas;
    crmCuentasCampaniaDetalleHistorico: CrmCuentasCampaniaDetalleHistorico[];
    crmCuentasCampaniaProductos: CrmCuentasCampaniaProductos[];
}