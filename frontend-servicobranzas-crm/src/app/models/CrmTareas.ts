//import { CrmTareaGestor } from "./CrmTareaGestor";
//import { CrmTareaCuentaCampania } from "./CrmTareaCuentaCampania;

export interface CrmTareas {
    id: number;
    descripcion: string;
    fecha_creacion: string | null;
    fecha_actualizacion: string | null;
    estado: string;
    fecha_inicio: string | null;
    fecha_fin: string | null;
    meta: Float64Array | null;
    //crmTareaGestor: CrmTareaGestor[];
    //crmTareaCuentaCampania: CrmTareaCuentaCampania[];
}