import { AclDepto } from "./AclDepto";
import { AclDistrito } from "./AclDistrito";
import { CrmCuentasDireccion } from "./CrmCuentasDireccion";
import { CrmCuentasTelefono } from "./CrmCuentasTelefono";

export interface AclCiudad {
    idCiudad: number;
    idDepto: number | null;
    ciudadValor: string;
    idDeptoNavigation: AclDepto;
    aclDistrito: AclDistrito[];
    crmCuentasDireccion: CrmCuentasDireccion[];
    crmCuentasTelefono: CrmCuentasTelefono[];
}