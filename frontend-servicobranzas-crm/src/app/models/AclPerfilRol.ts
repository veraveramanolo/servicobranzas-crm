import { AclPerfiles } from "./AclPerfiles";
import { AclRoles } from "./AclRoles";

export interface AclPerfilRol {
    idPerfilRol: number;
    idPerfil: number;
    idRol: number;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string | null;
    usrModificacion: string;
    fechaModificacion: string | null;
    //idPerfilNavigation: AclPerfiles;
    idRolNavigation: AclRoles;
}