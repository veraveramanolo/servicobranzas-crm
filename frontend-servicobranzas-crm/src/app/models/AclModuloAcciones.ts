import { AclAcciones } from "./AclAcciones";
import { AclModulos } from "./AclModulos";

export interface AclModuloAcciones {
    idModuloAccion: number;
    idModulo: number;
    idAccion: number;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string;
    usrModificacion: string;
    fechaModificacion: string | null;
    idAccionNavigation: AclAcciones;
    idModuloNavigation: AclModulos;
}