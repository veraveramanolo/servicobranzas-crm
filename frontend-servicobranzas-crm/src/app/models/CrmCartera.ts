import { CrmCampania } from "./CrmCampania";
import { CrmCuentas } from "./CrmCuentas";

export interface CrmCartera {
    id: number;
    nombre: string;
    tipo: string;
    estado: string;
    crmCampania: CrmCampania[];
    crmCuentas: CrmCuentas[];
}