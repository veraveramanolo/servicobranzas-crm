import { CrmArbolGestionTipoContacto } from "./CrmArbolGestionTipoContacto";
import { CrmArbolGestionTipoGestion } from "./CrmArbolGestionTipoGestion";
import { CrmCuentasCampania } from "./CrmCuentasCampania";
import { CrmGestion } from "./CrmGestion";

export interface CrmArbolGestion {
    id: number;
    idParent: number | null;
    descripcion: string;
    peso: number | null;
    estado: string;
    idTipoGestion: number | null;
    idTipoContacto: number | null;
    alerta: string;
    idParentNavigation: CrmArbolGestion;
    idTipoContactoNavigation: CrmArbolGestionTipoContacto;
    idTipoGestionNavigation: CrmArbolGestionTipoGestion;
    crmCuentasCampania: CrmCuentasCampania[];
    crmGestion: CrmGestion[];
    inverseIdParentNavigation: CrmArbolGestion[];
    labelEstado: string | null,
    labelNivel2: string | null,
}