import { AclAccesosRoles } from "./AclAccesosRoles";
import { AclPerfilRol } from "./AclPerfilRol";

export interface AclRoles {
    idRol: number;
    descripcion: string;
    anulado: string;
    usrCreacion: string;
    fechaCreacion: string | null;
    usrModificacion: string;
    fechaModificacion: string | null;
    aclAccesosRoles: AclAccesosRoles[];
    aclPerfilRol: AclPerfilRol[];
}