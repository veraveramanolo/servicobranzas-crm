import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Import Containers
import { DefaultLayoutComponent } from "./containers";

import { P404Component } from "./views/error/404.component";
import { P500Component } from "./views/error/500.component";
import { LoginComponent } from "./views/login/login.component";
import { RegisterComponent } from "./views/register/register.component";
import { AuthGuard } from "./security/auth.guard";

export const routes: Routes = [
  { path: "404", component: P404Component, data: { title: "Page 404" } },
  { path: "500", component: P500Component, data: { title: "Page 500" } },
  { path: "login", component: LoginComponent, data: { title: "Login Page" } },
  {
    path: "register",
    component: RegisterComponent,
    data: { title: "Register Page" },
  },
  {
    path: "",
    component: DefaultLayoutComponent,
    data: { title: "Home" },
    children: [
      {
        path: "mantenimientos",
        loadChildren: () =>
          import("./views/mantenimientos/mantenimientos.module").then(
            (m) => m.MantenimientosModule
          ),
      },
      {
        path: "gestionar",
        loadChildren: () =>
          import("./views/gestionar/gestionar.module").then(
            (m) => m.GestionarModule
          ),
      },
      {
        path: "configuracion",
        loadChildren: () =>
          import("./views/configuracion/configuracion.module").then(
            (m) => m.ConfiguracionModule
          ),
      },
      {
        path: "supervisor",
        loadChildren: () =>
          import("./views/supervisor/supervisor.module").then(
            (m) => m.SupervisorModule
          ),
      },
      {
        path: "multicanal",
        loadChildren: () =>
          import("./views/multicanal/multicanal.module").then(
            (m) => m.MulticanalModule
          ),
      },
      {
        path: "asignacion",
        loadChildren: () =>
          import("./views/asignacion/asignacion.module").then(
            (m) => m.AsignacionModule
          ),
      },
      {
        path: "configuracion-campanias",
        loadChildren: () =>
          import(
            "./views/configuracion-campanias/configuracion-campanias.module"
          ).then((m) => m.ConfiguracionCampaniasModule),
      },
      {
        path: "metricas",
        loadChildren: () =>
          import("./views/metricas/metricas.module").then(
            (m) => m.MetricasModule
          ),
      },
      {
        path: "tablero-control",
        loadChildren: () =>
          import("./views/tablero-control/tablero-control.module").then(
            (m) => m.TableroControlModule
          ),
      },
      {
        path: "caja",
        loadChildren: () =>
          import("./views/caja/caja.module").then(
            (m) => m.CajaModule
          ),
      },
      {
        path: "usuarios",
        loadChildren: () =>
          import("./views/usuarios/usuarios.module").then(
            (m) => m.UsuariosModule
          ),
      },
      {
        path: "generar",
        loadChildren: () =>
          import("./views/generar/generar.module").then((m) => m.GenerarModule),
      },
      {
        path: "paletagestion",
        loadChildren: () =>
          import("./views/paletagestion/paletagestion.module").then(
            (m) => m.PaletaModule
          ),
      },
      {
        path: "base",
        loadChildren: () =>
          import("./views/base/base.module").then((m) => m.BaseModule),
      },
      {
        path: "buttons",
        loadChildren: () =>
          import("./views/buttons/buttons.module").then((m) => m.ButtonsModule),
      },
      {
        path: "charts",
        loadChildren: () =>
          import("./views/chartjs/chartjs.module").then((m) => m.ChartJSModule),
      },
      {
        path: "dashboard",
        loadChildren: () =>
          import("./views/dashboard/dashboard.module").then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: "icons",
        loadChildren: () =>
          import("./views/icons/icons.module").then((m) => m.IconsModule),
      },
      {
        path: "notifications",
        loadChildren: () =>
          import("./views/notifications/notifications.module").then(
            (m) => m.NotificationsModule
          ),
      },
      {
        path: "theme",
        loadChildren: () =>
          import("./views/theme/theme.module").then((m) => m.ThemeModule),
      },
      {
        path: "widgets",
        loadChildren: () =>
          import("./views/widgets/widgets.module").then((m) => m.WidgetsModule),
      },
    ],
    canActivate: [AuthGuard],
  },
  { path: "**", component: P404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
