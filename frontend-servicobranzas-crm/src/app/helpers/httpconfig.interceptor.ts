import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');

        if (token) {

            // if (req.url.indexOf('/Autentificacion/login/') 

            // ) {
            //     return next.handle(req);
            // }
            
            if (req.url.indexOf('/CSVupload') ) 
            {
                 req = req.clone({
                    setHeaders: {
                        'Accept': 'application/json',
                        'Authorization': `Bearer ${token}`,
                    },
                });
            }
            else{
                //req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
                req = req.clone({
                    setHeaders: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept': 'application/json',
                        'Authorization': `Bearer ${token}`,
                    },
                });
            }

        }
        return next.handle(req);
    }
}
