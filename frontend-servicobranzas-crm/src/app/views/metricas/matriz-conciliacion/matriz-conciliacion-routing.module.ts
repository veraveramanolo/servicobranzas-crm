import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcuerdosComponent } from './acuerdos/acuerdos.component';
import { PagosComponent } from './pagos/pagos.component';


const routes: Routes = [
  {
    path: '',
    data: {title: 'Matriz de Conciliación'},
    children:[
      {
        path: '',
        redirectTo: 'matriz-conciliacion'
      },
      {
        path: 'matriz-conciliacion/acuerdos',
        component: AcuerdosComponent,
        data: {title: 'Acuerdos'}
      },
      {
        path: 'matriz-conciliacion/pagos',
        component: PagosComponent,
        data: {title: 'Pagos'}
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatrizConciliacionRoutingModule { }
