import { Component, OnInit } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { FormBuilder, FormGroup } from '@angular/forms';
import { format } from "date-fns";
import { MatrizConciliacionService } from "../../../../../services/matriz-conciliacion.service";

@Component({
  selector: 'app-modal-eliminar-pago',
  templateUrl: './modal-eliminar-pago.component.html',
  styleUrls: ['./modal-eliminar-pago.component.css']
})
export class ModalEliminarPagoComponent implements OnInit {

  idCampania: number;
  listaPagos: any;
  dateinitial: Date = new Date();
  datefinal: Date = new Date();
  myForm: FormGroup;
  fechainicio = null;
  fechafinal = null;
  totalpagos: any;

  constructor(
    public modalRef: BsModalRef,
    private readonly fb: FormBuilder,
    private _matrizConciliacionService: MatrizConciliacionService,
  ) { 
    this.myForm = this.fb.group({
      time_initial: [null],
      time_final: [null],
    });
   }

  ngOnInit(): void {
    console.log(this.idCampania);
    console.log(this.listaPagos);
  }

  EliminarPagos(event: any): void {
    console.log(this.idCampania);
    console.log(this.listaPagos);
  }

  BuscarPagos() {
    this.fechainicio = format(this.myForm.value.time_initial, "yyyy-MM-dd 00:00:00");
    this.fechafinal = format(this.myForm.value.time_final, "yyyy-MM-dd 23:59:59");
    console.log(this.fechainicio);
    console.log(this.fechafinal);
    this.MostrarPagosXasignacion(this.idCampania);
  }

  MostrarPagosXasignacion(idCampania: number): void {
    this._matrizConciliacionService.GetPagosXasignacion(idCampania,this.fechainicio,this.fechafinal).subscribe((respuesta) => {
      this.listaPagos = respuesta.data.consultaPagosXasignacion;
      console.log("this.listaPagos ",this.listaPagos);
      this.totalpagos = this.listaPagos.length;
      console.log(this.totalpagos);
    });
  }
}
