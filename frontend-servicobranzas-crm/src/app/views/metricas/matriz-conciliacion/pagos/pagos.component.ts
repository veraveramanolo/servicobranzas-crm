import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { ParametrosService } from "../../../../services/parametros.service";
import { MatrizConciliacionService } from "../../../../services/matriz-conciliacion.service";
import { BsModalService, ModalOptions } from "ngx-bootstrap/modal";
import { Message } from 'primeng/api';
import { format } from "date-fns";
import { GridComponent } from "@progress/kendo-angular-grid";
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalEliminarPagoComponent } from "./modal-eliminar-pago/modal-eliminar-pago.component";
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent implements OnInit {

  time = new Observable<string>((observer: Observer<string>) => {
    setTimeout(() => observer.next(this.imprimir()), 300000);
  });

  @ViewChild('cbb1') p1: ElementRef;
  @ViewChild('cbb2') p2: ElementRef;
  @ViewChild('cbb3') p3: ElementRef;
  ListaTitleCbb: any[] = [];
  ListaParametros: any[] = [];

  ListaCbb1: any[] = [];
  ListaCbb2: any[] = [];
  ListaCbb3: any[] = [];

  public selectedItemCbb1 = [];
  public selectedItemCbb2 = [];
  public selectedItemCbb3 = [];

  dateinitial: Date = new Date();
  datefinal: Date = new Date();

  msgs1: Message[];

  @Input('idcampania') idcampania: number;

  ListaPagos: any[] = [];
  ListaPagosSelect: any[] = [];
  ListaGrupoPagos: any[] = [];

  fechainicio = null;
  fechafinal = null;

  deudatotal: boolean = true;
  deudavencida: boolean;
  tblusuario: boolean = true;

  fileName: string;
  fileNameTotal: string;

  myForm: FormGroup;

  constructor(
    private _parametrosService: ParametrosService,
    private _matrizConciliacionService: MatrizConciliacionService,
    private readonly modalService: BsModalService,
    private readonly fb: FormBuilder
  ) { 
    this.myForm = this.fb.group({
      time_initial: [null],
      time_final: [null],
    });
   }

  ngOnInit(): void {
    this.tblusuario;
    this.MostrarAclParametros();
  }

  public imprimir = () => {
    this.MostrarPagosXasignacion(this.idcampania);
    return format(new Date(), "H:mm");
  };

  MostrarAclParametros(): void {
    this._parametrosService.GetAclParametros(8,'A','Pagos').subscribe((respuesta) => {
      this.ListaParametros = respuesta.data;
      console.log("this.ListaParametros ",this.ListaParametros);
      this.LlenaCombobox(this.ListaParametros);
    });
  }

  ngAfterViewInit() {
    this.ListaTitleCbb = [this.p1.nativeElement.innerHTML,this.p2.nativeElement.innerHTML,this.p3.nativeElement.innerHTML];
    return ;
  }

  LlenaCombobox(lista: any) {
    this.ngAfterViewInit();
    this.ListaTitleCbb.forEach(element => {
      switch (element) {
        case 'Valor medido':
          this.ListaCbb1 = lista.filter(
            (s) => s.nombreParametro == element
          );
          this.selectedItemCbb1 = this.ListaCbb1[0];
          break;
        case 'Ver por':
          this.ListaCbb2 = lista.filter(
            (s) => s.nombreParametro == element
          );
          this.selectedItemCbb2 = this.ListaCbb2[0];
          break;
        case 'Agrupar por':
          this.ListaCbb3 = lista.filter(
            (s) => s.nombreParametro == element
          );
          this.selectedItemCbb3 = this.ListaCbb3[0];
          break;
        default:
          break;
      }
    });
  }

  public EventoCbb(value: any): void {
    console.log("valueChange", value);
    console.log("valueChange nombreParametro", value.nombreParametro);
    
    switch (value.nombreParametro) {
      case 'Valor medido':
        if (value.valorParametro == "DEUDA TOTAL") {
          this.deudatotal = true;
          this.deudavencida = false;
          console.log("total");
        }; if(value.valorParametro == "DEUDA VENCIDA") {
          this.deudavencida = true;
          this.deudatotal = false;
          console.log("vencida");
        }
        break;
      case 'Ver por':
        if (value.valorParametro == "Usuario") {
          this.tblusuario = true;
        } else if (value.valorParametro == "Cuenta"){
          this.tblusuario = false;
        }
        break;
      case 'Agrupar por':
        if (value.valorParametro == "Acuerdos por asignación") {
          this.MostrarPagosXasignacion(this.idcampania);
          console.log("asignacion");
        } /*else {
          this.MostrarAcuerdosXgestion(this.idcampania);
          console.log("gestion");
        }*/
        break;
      default:
        break;
    }
  }

  EliminarPagos(){
    console.log("Hola");
    if (!this.idcampania) {
      this.msgs1 = [({severity:'warn', summary:'Atención', detail:'Seleccione campañia!'})];
      return;
    }
    this.msgs1 = [];
    this.modalService.show(ModalEliminarPagoComponent, {
      initialState: {
        // detalle: datafiltro,
        idCampania: this.idcampania,
        listaPagos: this.ListaPagos,
        // filtro: this.estadoGrid,
        // idCampania: Number(this.params.GestionID),
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      class: "modal-lg",
    });
  }

  MostrarPagosXasignacion(idCampania: number): void {
    this.msgs1 = [];
    this._matrizConciliacionService.GetPagosXasignacion(idCampania,this.fechainicio,this.fechafinal).subscribe((respuesta) => {
      this.ListaGrupoPagos = respuesta.data.listaGrupoPagosXasignacion;
      console.log("this.ListaGrupoPagos ",this.ListaGrupoPagos);
      this.ListaPagos = respuesta.data.consultaPagosXasignacion;
      console.log("this.ListaPagos ",this.ListaPagos);
      // this.addTotal(this.ListaGrupoPagos);
    });
  }

  onExpand(e) {
    console.log('Row expanded ', e.dataItem.key);
    this.fileName = e.dataItem.key+"_"+format(new Date(), "yyyy-MM-dd")+".xlsx";
    console.log('Row  ', e.index);
    this.ListaPagosSelect = [];
    let asesor = e.dataItem.key;
    this.ListaPagosSelect = this.ListaPagos.filter(
      (s) => s.agente === asesor);
    console.log("this.ListaPagosSelect ", this.ListaPagosSelect);
  }

  public exportToExcel(grid: GridComponent): void {
    this.fileNameTotal = "Pagos"+format(new Date(), "yyyy-MM-dd")+".xlsx";
    grid.saveAsExcel();
  }

  ChangeDate() {
    console.log(this.myForm.value.time_initial);
    console.log(this.myForm.value.time_final);
    if (this.idcampania == undefined) {
      this.dateinitial = new Date();
      this.msgs1 = [({severity:'warn', summary:'Atención', detail:'Seleccione campañia!'})];
      return;
    }
    this.fechainicio = format(this.myForm.value.time_initial, "yyyy-MM-dd 00:00:00");
    this.fechafinal = format(this.myForm.value.time_final, "yyyy-MM-dd 23:59:59");
    console.log(this.fechainicio);
    console.log(this.fechafinal);
    this.MostrarPagosXasignacion(this.idcampania);
  }

  VerTodo() {
    if (this.idcampania == undefined) {
      this.msgs1 = [({severity:'warn', summary:'Atención', detail:'Seleccione campañia!'})];
      return;
    }
    this.dateinitial = null;
    this.datefinal = null;
    this.fechainicio = null;
    this.msgs1 = [];
    this.MostrarPagosXasignacion(this.idcampania);
  }

}
