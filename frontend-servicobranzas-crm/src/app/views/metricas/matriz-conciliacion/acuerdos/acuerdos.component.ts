import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { ParametrosService } from "../../../../services/parametros.service";
import { MatrizConciliacionService } from "../../../../services/matriz-conciliacion.service";
import { format } from "date-fns";
import { Message } from 'primeng/api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Observer } from 'rxjs';
import { GridComponent } from "@progress/kendo-angular-grid";

const distinctNombre= (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x, idx, xs) => {
      // console.log(x, idx, xs);
      return xs.findIndex((y) => y === x) === idx;
    });

@Component({
  selector: 'app-acuerdos',
  templateUrl: './acuerdos.component.html',
  styleUrls: ['./acuerdos.component.css']
})
export class AcuerdosComponent implements OnInit {

  time = new Observable<string>((observer: Observer<string>) => {
    setTimeout(() => observer.next(this.imprimir()), 300000);
  });

  @ViewChild('cbb1') p1: ElementRef;
  @ViewChild('cbb2') p2: ElementRef;
  @ViewChild('cbb3') p3: ElementRef;
  ListaTitleCbb: any[] = [];
  ListaParametros: any[] = [];
  ListaAcuerdosXasignacion: any[] = [];
  ListaAcuerdosXasignacionSelect: any[] = [];
  ListaGrupoAcuerdosXasignacion: any[] = [];
  msgs1: Message[];
  total: any[] = [];

  @Input('idcampania') idcampania: number;

  tipoParametro: any[] = [
    { name: "Valor medido" },
    { name: "Ver por" },
    { name: "Agrupar por" },
  ];

  ListaCbb1: any[] = [];
  ListaCbb2: any[] = [];
  ListaCbb3: any[] = [];

  public selectedItemCbb1 = [];
  public selectedItemCbb2 = [];
  public selectedItemCbb3 = [];

  dateinitial: Date = new Date();
  datefinal: Date = new Date();

  fechainicio = null;
  fechafinal = null;

  myForm: FormGroup;

  deudatotal: boolean = true;
  deudavencida: boolean;

  tipoacuerdo: number;
  horaactualizada: any;

  fileName: string;
  fileNameTotal: string;

  constructor(
    private _parametrosService: ParametrosService,
    private _matrizConciliacionService: MatrizConciliacionService,
    private readonly fb: FormBuilder
  ) { 
    this.myForm = this.fb.group({
      time_initial: [null],
      time_final: [null],
    });
  }

  ngOnInit(): void {
    this.MostrarAclParametros();
    this.horaactualizada = format(new Date(), "H:mm");
  }

  public imprimir = () => {
    this.ExecMetodoAcuerdo(this.tipoacuerdo);
    return format(new Date(), "H:mm");
  };

  MostrarAclParametros(): void {
    this._parametrosService.GetAclParametros(8,'A','Acuerdos').subscribe((respuesta) => {
      this.ListaParametros = respuesta.data;
      console.log("this.ListaParametros ",this.ListaParametros);
      this.LlenaCombobox(this.ListaParametros);
    });
  }

  ngAfterViewInit() {
    // console.log(this.span.nativeElement); // Etiqueta Span
    // Valores de etiqueta
    this.ListaTitleCbb = [this.p1.nativeElement.innerHTML,this.p2.nativeElement.innerHTML,this.p3.nativeElement.innerHTML];
    return ;
  }

  LlenaCombobox(lista: any) {
    this.ngAfterViewInit();
    this.ListaTitleCbb.forEach(element => {
      console.log(element);
      switch (element) {
        case 'Valor medido':
          this.ListaCbb1 = lista.filter(
            (s) => s.nombreParametro == element
          );
          this.selectedItemCbb1 = this.ListaCbb1[0];
          break;
        case 'Ver por':
          this.ListaCbb2 = lista.filter(
            (s) => s.nombreParametro == element
          );
          this.selectedItemCbb2 = this.ListaCbb2[0];
          break;
        case 'Agrupar por':
          this.ListaCbb3 = lista.filter(
            (s) => s.nombreParametro == element
          );
          this.selectedItemCbb3 = this.ListaCbb3[0];
          break;
        default:
          break;
      }
      
    });
  }

  

  LlamadoMatriz(idCampania: number): void {
    this.fechainicio = format(new Date(), "yyyy-MM-dd 00:00:00");
    this.fechafinal = format(new Date(), "yyyy-MM-dd 23:59:59");
    this.MostrarAcuerdosXasignacion(idCampania);
  }

  ExecMetodoAcuerdo(tipoacuerdo: number){
    switch (tipoacuerdo) {
      case 0:
        this.MostrarAcuerdosXasignacion(this.idcampania);
        break;
      case 1:
        this.MostrarAcuerdosXgestion(this.idcampania);
        break;
      default:
        break;
    }
  }

  MostrarAcuerdosXasignacion(idCampania: number): void {
    this.msgs1 = [];
    this.tipoacuerdo = 0;
    this._matrizConciliacionService.GetAcuerdosXasignacion(idCampania,this.fechainicio,this.fechafinal).subscribe((respuesta) => {
      this.ListaGrupoAcuerdosXasignacion = respuesta.data.listaGrupoAcuerdosXasignacion;
      console.log("this.ListaGrupoAcuerdosXasignacion ",this.ListaGrupoAcuerdosXasignacion);
      this.ListaAcuerdosXasignacion = respuesta.data.consultaAcuerdosXasignacion;
      console.log("this.ListaAcuerdosXasignacion ",this.ListaAcuerdosXasignacion);
      this.addTotal(this.ListaGrupoAcuerdosXasignacion);
    });
  }

  MostrarAcuerdosXgestion(idCampania: number): void {
    this.msgs1 = [];
    this.tipoacuerdo = 1;
    this._matrizConciliacionService.GetAcuerdosXgestion(idCampania,this.fechainicio,this.fechafinal).subscribe((respuesta) => {
      this.ListaGrupoAcuerdosXasignacion = respuesta.data.listaGrupoAcuerdosXgestion;
      console.log("this.ListaGrupoAcuerdosXgestion ",this.ListaGrupoAcuerdosXasignacion);
      this.ListaAcuerdosXasignacion = respuesta.data.consultaAcuerdosXgestion;
      console.log("this.ListaAcuerdosXgestion ",this.ListaAcuerdosXasignacion);
      this.addTotal(this.ListaGrupoAcuerdosXasignacion);
    });
  }

  VerTodo() {
    if (this.idcampania == undefined) {
      this.msgs1 = [({severity:'warn', summary:'Atención', detail:'Seleccione campañia!'})];
      return;
    }
    this.dateinitial = null;
    this.datefinal = null;
    this.fechainicio = null;
    this.msgs1 = [];
    this.ExecMetodoAcuerdo(this.tipoacuerdo);
  }

  ChangeDate() {
    console.log(this.myForm.value.time_initial);
    console.log(this.myForm.value.time_final);
    if (this.idcampania == undefined) {
      this.dateinitial = new Date();
      this.msgs1 = [({severity:'warn', summary:'Atención', detail:'Seleccione campañia!'})];
      return;
    }
    this.fechainicio = format(this.myForm.value.time_initial, "yyyy-MM-dd 00:00:00");
    this.fechafinal = format(this.myForm.value.time_final, "yyyy-MM-dd 23:59:59");
    console.log(this.fechainicio);
    console.log(this.fechafinal);
    this.ExecMetodoAcuerdo(this.tipoacuerdo);
  }

  onExpand(e) {
    console.log('Row expanded ', e.dataItem.key);
    this.fileName = e.dataItem.key+"_"+format(new Date(), "yyyy-MM-dd")+".xlsx";
    console.log('Row  ', e.index);
    this.ListaAcuerdosXasignacionSelect = [];
    let asesor = e.dataItem.key;
    this.ListaAcuerdosXasignacionSelect = this.ListaAcuerdosXasignacion.filter(
      (s) => s.agenteasignado === asesor);
    console.log("this.ListaAcuerdosXasignacionSelect ", this.ListaAcuerdosXasignacionSelect);
  }
  
  // onCollapse(e) {
  //   console.log('Row collapsed ', e)
  // }

  public EventoCbb(value: any): void {
    console.log("valueChange", value);
    console.log("valueChange nombreParametro", value.nombreParametro);
    
    switch (value.nombreParametro) {
      case 'Valor medido':
        if (value.valorParametro == "DEUDA TOTAL") {
          this.deudatotal = true;
          this.deudavencida = false;
          console.log("total");
        }; if(value.valorParametro == "DEUDA VENCIDA") {
          this.deudavencida = true;
          this.deudatotal = false;
          console.log("vencida");
        }
        break;
      case 'Agrupar por':
        if (value.valorParametro == "Acuerdos por asignación") {
          this.MostrarAcuerdosXasignacion(this.idcampania);
          console.log("asignacion");
        } else {
          this.MostrarAcuerdosXgestion(this.idcampania);
          console.log("gestion");
        }
        break;
      default:
        break;
    }
  }

  public exportToExcel(grid: GridComponent): void {
    this.fileNameTotal = "Acuerdos"+format(new Date(), "yyyy-MM-dd")+".xlsx";
    grid.saveAsExcel();
  }

  addTotal(lista: any): void {
    let totalnumeroacuerdos: number = 0;
    let totalvaloracuerdos: number = 0;
    let totalnumeropagos: number = 0;
    let totalvalorpagos: number = 0;
    let totalsaldodeuda: number = 0;
    let totalsaldovencido: number = 0;
    let cont: number = 0;
    for (const value of lista) {
      totalnumeroacuerdos += Number(value.noacuerdos);
      totalvaloracuerdos += Number(value.valoracuerdos);
      totalnumeropagos += Number(value.nopagos);
      totalvalorpagos += Number(value.valorpagos);
      totalsaldodeuda += Number(value.saldodeuda);
      totalsaldovencido += Number(value.saldovencido);
      cont++;
    }
      const elemento = {
        noacuerdos: totalnumeroacuerdos,
        valoracuerdos: totalvaloracuerdos,
        nopagos: totalnumeropagos,
        valorpagos: totalvalorpagos,
        saldodeuda: totalsaldodeuda,
        saldovencido: totalsaldovencido,
      };
      this.total = [];
      this.total.push(elemento);
  }
}
