import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { TipoContactoService } from "../../../services/tipo-contacto.service";
import { ParametrosService } from "../../../services/parametros.service";
import { MetricasService } from "../../../services/metricas.service";

@Component({
  selector: 'app-modal-filtro-indicadores-gestion',
  templateUrl: './modal-filtro-indicadores-gestion.component.html',
  styleUrls: ['./modal-filtro-indicadores-gestion.component.css']
})
export class ModalFiltroIndicadoresGestionComponent implements OnInit {

  // Importados
  displayPosition: boolean;
  positionDialog: string;
  @Input('master') masterName: number;
  @Input('indexTabIndicadores') indexTabIndicadores: number;

  tipoDeContacto: any[] = [];
  ListaParametros: any[] = [];
  ListaParametrosEmergente: any[] = [];
  ListaParametrosEmergente2: any[] = [];
  ListaValores: any[] = [];
  ListaValores2: any[] = [];
  ListaValores3: any[] = [];
  selectedFiltro: any = null;
  selectedFiltro2: any = null;
  selectedFiltro3: any = null;
  selectedValor: any = null;
  selectedValor2: any = null;
  selectedValor3: any = null;
  activar1: boolean = true;
  activar2: boolean = false;
  activar3: boolean = false;
  paramselect: string = null;

  constructor(
    private readonly fb: FormBuilder,
    private _tipoContactoService: TipoContactoService,
    private _parametrosService: ParametrosService,
    private readonly _metricasService: MetricasService,
  ) { }

  ngOnInit(): void {
    this.MostrarAclTipoContacto();
    this.MostrarAclParametros();
    this.selectedFiltro = this.ListaParametros;
    this.selectedFiltro2 = this.ListaParametrosEmergente;
    this.selectedFiltro3 = this.ListaParametrosEmergente2;
    this.selectedValor = this.ListaValores;
    this.selectedValor2 = this.ListaValores2;
    this.selectedValor3 = this.ListaValores3;
  }

  MostrarAclTipoContacto(): void {
    this._tipoContactoService.GetAclTipoContacto().subscribe((respuesta) => {
      this.tipoDeContacto = respuesta.data;
    });
  }

  MostrarAclParametros(): void {
    this._parametrosService.GetAclParametros(7,'A','filtros').subscribe((respuesta) => {
      this.ListaParametros = respuesta.data;
      console.log("this.ListaParametros ",this.ListaParametros);
    });
    
  }

  MostrarValoresXnombre(idCampania: number, nombre: string, numdiv: number): void {
    this._metricasService.mostrarFiltraValoresXnombre(idCampania,nombre).subscribe((respuesta) => {
      switch (numdiv) {
        case 1:
          this.ListaValores = respuesta.data;
          break;
        case 2:
          this.ListaValores2 = respuesta.data;
          break;
        case 3:
          this.ListaValores3 = respuesta.data;
          break;
        default:
          break;
      }
    });
  }

  FiltraValores(event) {
    this.selectedValor=null;
    this.MostrarValoresXnombre(this.masterName,event.value.valorParametro, 1);
    this.paramselect = event.value.valorParametro;
  }

  FiltraValores2(event) {
    this.selectedValor2=null;
    this.MostrarValoresXnombre(this.masterName,event.value.valorParametro, 2);
    this.paramselect = event.value.valorParametro;
  }

  FiltraValores3(event) {
    this.selectedValor3=null;
    this.MostrarValoresXnombre(this.masterName,event.value.valorParametro, 3);
    this.paramselect = event.value.valorParametro;
  }

  SeleccionaValor(event){
    console.log("event valor ", event.value); 
    if (event.value != null) {
      this.activar2 = true;
      this.ListaParametrosEmergente = this.ListaParametros.filter(
        (s) => s.valorParametro !== this.paramselect
      );
      console.log("this.ListaParametrosEmergente ",this.ListaParametrosEmergente);
    }
  }

  SeleccionaValor2(event){
    console.log("event valor ", event.value); 
    if (event.value != null) {
      this.activar3 = true;
      this.ListaParametrosEmergente2 = this.ListaParametrosEmergente.filter(
        (s) => s.valorParametro !== this.paramselect
      );
      console.log("this.ListaParametrosEmergente2 ",this.ListaParametrosEmergente2);
    }
  }

  EliminarDivb1(){
    this.selectedFiltro = null;
    this.ListaValores = null;
    let d2 = document.getElementById('div2');
    d2.removeAttribute;
    this.activar2 = false;
    this.EliminarDivb2();
  }
  EliminarDivb2(){
    this.selectedFiltro2 = null;
    this.ListaValores2 = null;
    let d3 = document.getElementById('div3');
    d3.removeAttribute;
    this.activar3 = false;
  }

  EliminarDivb3(){
    this.selectedFiltro3 = null;
    this.ListaValores3 = null;
  }
}
