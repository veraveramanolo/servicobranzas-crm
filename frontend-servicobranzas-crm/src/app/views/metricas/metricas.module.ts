import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MetricasRoutingModule } from './metricas-routing.module';
import { IndicadoresComponent } from './indicadores/indicadores.component';
import { MatrizConciliacionComponent } from './matriz-conciliacion/matriz-conciliacion.component';
import { AcuerdosComponent } from './matriz-conciliacion/acuerdos/acuerdos.component';
import { PagosComponent } from './matriz-conciliacion/pagos/pagos.component';
import { IntlModule } from "@progress/kendo-angular-intl";
import { ModalModule } from "ngx-bootstrap/modal";
//kendo
import { ComboBoxModule } from "@progress/kendo-angular-dropdowns";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { GridModule, ExcelModule } from "@progress/kendo-angular-grid";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
// import { ChartsModule } from "@progress/kendo-angular-charts";
// import "hammerjs";

//primeng
import { CardModule } from "primeng/card";
import { TabViewModule } from "primeng/tabview";
import {ChartModule} from 'primeng/chart';
import { ButtonModule } from "primeng/button";
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import {TooltipModule} from 'primeng/tooltip';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

import { ModalFiltroIndicadoresGestionComponent } from './modal-filtro-indicadores-gestion/modal-filtro-indicadores-gestion.component';
import { ModalFiltroIndicadoresEstadosclienteComponent } from './modal-filtro-indicadores-estadoscliente/modal-filtro-indicadores-estadoscliente.component';
import { ModalEliminarPagoComponent } from './matriz-conciliacion/pagos/modal-eliminar-pago/modal-eliminar-pago.component';

@NgModule({
  declarations: [
    IndicadoresComponent,
    MatrizConciliacionComponent,
    AcuerdosComponent,
    PagosComponent, 
    ModalFiltroIndicadoresGestionComponent, ModalFiltroIndicadoresEstadosclienteComponent, ModalEliminarPagoComponent,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "es-ES" }],
  entryComponents: [
    ModalFiltroIndicadoresGestionComponent,
    ModalFiltroIndicadoresEstadosclienteComponent,
    ModalEliminarPagoComponent
  ],
  imports: [
    CommonModule,
    MetricasRoutingModule,
    CardModule,
    TabViewModule,
    ChartModule,
    ComboBoxModule,
    IntlModule,
    ButtonsModule,
    GridModule,
    ReactiveFormsModule,
    FormsModule,
    DateInputsModule,
    // ChartsModule,
    ButtonModule,
    RadioButtonModule,
    DropdownModule,
    MultiSelectModule,
    DialogModule,
    CalendarModule,
    TooltipModule,
    MessagesModule,
    MessageModule,
    ExcelModule,
    ModalModule.forRoot(),
  ]
})
export class MetricasModule { }
