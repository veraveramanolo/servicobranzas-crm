import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndicadoresComponent } from './indicadores/indicadores.component';
import { MatrizConciliacionComponent } from './matriz-conciliacion/matriz-conciliacion.component';


const routes: Routes = [
  {
    path: "",
    data: {title : "Metricas"},
    children: [
      {
        path: "",
        redirectTo: "metricas"
      },
      {
        path: "metricas/indicadores",
        component: IndicadoresComponent,
        data: {title: "Indicadores"}
      },
      {
        path: "metricas/matriz-conciliacion",
        component: MatrizConciliacionComponent,
        data: {title: "Matriz Conciliacion"}
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MetricasRoutingModule { }
