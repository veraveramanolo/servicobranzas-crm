import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionCampaniasRoutingModule } from './configuracion-campanias-routing.module';
import { ComponentesComponent } from './componentes/componentes.component';
import { ArbolGestionesComponent } from './arbol-gestiones/arbol-gestiones.component';
import { CampaniasMulticanalComponent } from './campanias-multicanal/campanias-multicanal.component';


@NgModule({
  declarations: [ComponentesComponent, ArbolGestionesComponent, CampaniasMulticanalComponent],
  imports: [
    CommonModule,
    ConfiguracionCampaniasRoutingModule
  ]
})
export class ConfiguracionCampaniasModule { }
