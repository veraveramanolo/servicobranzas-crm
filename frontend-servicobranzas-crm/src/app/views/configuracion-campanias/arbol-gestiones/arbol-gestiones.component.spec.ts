import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbolGestionesComponent } from './arbol-gestiones.component';

describe('ArbolGestionesComponent', () => {
  let component: ArbolGestionesComponent;
  let fixture: ComponentFixture<ArbolGestionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbolGestionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbolGestionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
