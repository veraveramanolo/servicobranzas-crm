import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaniasMulticanalComponent } from './campanias-multicanal.component';

describe('CampaniasMulticanalComponent', () => {
  let component: CampaniasMulticanalComponent;
  let fixture: ComponentFixture<CampaniasMulticanalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaniasMulticanalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaniasMulticanalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
