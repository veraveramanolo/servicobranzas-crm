import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentesComponent } from './componentes/componentes.component';
import { ArbolGestionesComponent } from './arbol-gestiones/arbol-gestiones.component';
import { CampaniasMulticanalComponent } from './campanias-multicanal/campanias-multicanal.component';


const routes: Routes = [
  {
    path: '',
    data: {title: 'Configuración de Campañas'},
    children: [
      {
        path: '',
        redirectTo: 'configuracion-campanias'
      },
      {
        path: 'configuracion-campanias/componentes',
        component: ComponentesComponent,
        data: {title: 'Componentes'}
      },
      {
        path: 'configuracion-campanias/arbol-gestiones',
        component: ArbolGestionesComponent,
        data: {title: 'Arbol de Gestiones'}
      },
      {
        path: 'configuracion-campanias/campanias-multicanal',
        component: CampaniasMulticanalComponent,
        data: {title: 'Campañas multicanal'}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionCampaniasRoutingModule { }
