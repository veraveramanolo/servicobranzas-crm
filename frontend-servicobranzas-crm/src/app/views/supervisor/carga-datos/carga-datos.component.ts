import { Component, OnInit } from "@angular/core";
import {
  DataStateChangeEvent,
  GridDataResult,
} from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import { BsModalService } from "ngx-bootstrap/modal";
import { ModalCargaDatosComponent } from "../modal-carga-datos/modal-carga-datos.component";

@Component({
  selector: "app-carga-datos",
  templateUrl: "./carga-datos.component.html",
  styleUrls: ["./carga-datos.component.css"],
})
export class CargaDatosComponent implements OnInit {
  public cargaDatos: any[] = [];
  public datoGrid: GridDataResult;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: "and",
      filters: [],
    },
  };

  constructor(private readonly modalService: BsModalService) {}

  ngOnInit(): void {}

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.datoGrid = process(this.cargaDatos, this.state);
  }
  open(): void {
    this.modalService.show(ModalCargaDatosComponent, {
      // initialState: {
      //   idCampania: Number(this.params.GestionID),
      //   arbolSelecciondo: this.arbolSelecciondo,
      //   idProducto: Number(this.params.ProductoID),
      // },
      backdrop: "static",
      class: "gray modal-lg",
    });
  }
}
