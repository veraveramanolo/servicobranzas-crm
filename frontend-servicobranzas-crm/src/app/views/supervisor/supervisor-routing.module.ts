import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CargaDatosComponent } from "./carga-datos/carga-datos.component";
import { ExportarComponent } from "./exportar/exportar.component";
import { ImportarComponent } from "./importar/importar.component";

const routes: Routes = [
  {
    path: "",
    data: { title: "Supervisor" },
    children: [
      {
        path: "",
        redirectTo: "supervisor",
      },
      {
        path: "supervisor/carga-dato",
        component: CargaDatosComponent,
        data: { title: "Carga de datos" },
      },
      {
        path: "supervisor/exportar",
        component: ExportarComponent,
        data: { title: "Exportar" },
      },
      {
        path: "supervisor/importar",
        component: ImportarComponent,
        data: { title: "Importar" },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SupervisorRoutingModule {}
