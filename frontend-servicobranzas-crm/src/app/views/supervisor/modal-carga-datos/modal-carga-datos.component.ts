import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import {
  FileRestrictions,
  RemoveEvent,
  SelectEvent,
} from "@progress/kendo-angular-upload";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

@Component({
  selector: "app-modal-carga-datos",
  templateUrl: "./modal-carga-datos.component.html",
  styleUrls: ["./modal-carga-datos.component.css"],
})
export class ModalCargaDatosComponent implements OnInit {
  public cargaForm: FormGroup;

  constructor(
    public modalRef: BsModalRef,

    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.form();
  }

  guardar(): void {}

  private form(): void {
    this.cargaForm = this.fb.group({
      // nombres: [""],
      // apellidos: [""],
      // cedula: [
      //   "",
      //   [
      //     Validators.required,
      //     Validators.pattern(/^[0-9]+$/),
      //     Validators.minLength(10),
      //   ],
      // ],
      // telefono: ["", Validators.required],
      // email: ["", [Validators.email]],
      // cargo: [""],
      // departamento: ["", [Validators.required]],
      // userName: [""],
      // agentpass: [""],
      // // codigo:[''],
    });
  }

  public events: string[] = [];
  public imagePreviews: any[] = [];

  public fileRestrictions: FileRestrictions = {
    allowedExtensions: [".csv", ".CSV"],
  };

  public removeEventHandler(e: RemoveEvent): void {
    this.log(`Removido ${e.files[0].name}`);

    const index = this.imagePreviews.findIndex(
      (item) => item.uid === e.files[0].uid
    );

    if (index >= 0) {
      this.imagePreviews.splice(index, 1);
    }
  }

  public selectEventHandler(e: SelectEvent): void {
    const that = this;

    e.files.forEach((file) => {
      that.log(`Archivo Seleccionado: ${file.name}`);

      if (!file.validationErrors) {
        const reader = new FileReader();

        reader.onload = function (ev) {
          const image = {
            src: ev.target["result"],
            uid: file.uid,
          };

          that.imagePreviews.unshift(image);
        };

        reader.readAsDataURL(file.rawFile);
      }
    });
  }

  private log(event: string): void {
    this.events.unshift(`${event}`);
  }
}
