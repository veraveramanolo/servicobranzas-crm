import { LOCALE_ID, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { formatDate } from '@angular/common';

import { SupervisorRoutingModule } from "./supervisor-routing.module";
import { CargaDatosComponent } from "./carga-datos/carga-datos.component";
import { FormsModule } from "@angular/forms";

//kendo tabla
import { GridModule } from "@progress/kendo-angular-grid";
import { ModalCargaDatosComponent } from "./modal-carga-datos/modal-carga-datos.component";

//modal ngx
import { ModalModule } from "ngx-bootstrap/modal";
import { ReactiveFormsModule } from "@angular/forms";

//kendo upload
import { UploadsModule } from "@progress/kendo-angular-upload";
import { EventLogComponent } from "./modal-carga-datos/event-log.component";
import { ExportarComponent } from "./exportar/exportar.component";
import { ImportarComponent } from "./importar/importar.component";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { IntlModule } from "@progress/kendo-angular-intl";
import {registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import {
  ComboBoxModule,
  MultiSelectModule,
} from "@progress/kendo-angular-dropdowns";
registerLocaleData(localeEs);

@NgModule({
  entryComponents: [ModalCargaDatosComponent],
  declarations: [
    CargaDatosComponent,
    ModalCargaDatosComponent,
    EventLogComponent,
    ExportarComponent,
    ImportarComponent,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "es-ES" }],

  imports: [
    CommonModule,
    SupervisorRoutingModule,
    GridModule,
    ModalModule.forRoot(),
    FormsModule,
    ComboBoxModule,
    ReactiveFormsModule,
    UploadsModule,
    DateInputsModule,
    IntlModule,
    MultiSelectModule,
    DropDownsModule,
  ],
})
export class SupervisorModule {}
