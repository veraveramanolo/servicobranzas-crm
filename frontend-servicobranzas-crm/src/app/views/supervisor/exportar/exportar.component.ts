import { Component, OnDestroy, OnInit } from "@angular/core";
import { State, process } from "@progress/kendo-data-query";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import {
  DataStateChangeEvent,
  GridDataResult,
  FilterService,
} from "@progress/kendo-angular-grid";
import { VariableGlobalService } from "../../../services/variable-global.service";
import { CrmArbolGestion } from "../../../models/CrmArbolGestion";
import Swal from "sweetalert2";
import { ArbolGestionServiceService } from "../../../services/arbol-gestion.service";
import { GestionMapper } from "../../../utils/mappers/arbol-gestion.mapper";
import { BsModalService } from "ngx-bootstrap/modal";
import { ModalDinamicoGestionComponent } from "./modal-dinamico-gestion/modal-dinamico-gestion.component";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { RolesService } from "../../../services/roles.service";
import { formatDate } from '@angular/common';

const itemIndex = (item: any, data: any[]): number => {
  for (let idx = 0; idx < data.length; idx++) {
    if (data[idx].idlote === item.idlote) {
      return idx;
    }
  }
  return -1;
};

@Component({
  selector: "exportar",
  templateUrl: "./exportar.component.html",
  styleUrls: ["./exportar.component.css"],
})
export class ExportarComponent implements OnInit, OnDestroy {
  private stop$ = new Subject<void>();

  registerForm: FormGroup;

  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public cargandoDato: boolean;
  public checkSeleccionado: number[] = [];
  private updatedItems: any[] = [];
  private createdItems: any[] = [];
  public fechaidate: any;
  public fechafdate: any;
  public infoAclGestion: CrmArbolGestion[] = []; //ITA 31/03/2022 AclGestion

  constructor(
    private _arbolGestionService: ArbolGestionServiceService,

    private global: VariableGlobalService,
    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService
  ) {}
  ngOnDestroy(): void {
    this.stop();
  }

  ngOnInit(): void {
    this.formGuardar();
    this.estadoGrid = {
      skip: 0,
      take: 10,
      filter: { logic: "and", filters: [] },
    };
    //this.ConsultarArbolGestion();
    this.cambiosModal();
  }

  open(titulo: string, gestion?: any): void {
    console.log(gestion);
    let gestionEdit: CrmArbolGestion;
    if (gestion) {
      gestionEdit = gestion.dataItem;
      console.log(gestionEdit);
    }
    this.modalService.show(ModalDinamicoGestionComponent, {
      initialState: { titulo, gestion: gestionEdit },
      backdrop: "static",
      // class:'gray modal-lg'
    });
  }

  filterExport(valor){
    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();
    if(typeof this.fechaidate == 'undefined'  || typeof this.fechafdate == 'undefined'){
      Swal.close();
      return 0;
    }

    var fechaini = formatDate(this.fechaidate, 'yyyy-MM-dd', 'es-ES');
    var fechafin = formatDate(this.fechafdate, 'yyyy-MM-dd', 'es-ES');

    this._arbolGestionService.recibosCaja(fechaini,fechafin).subscribe((respuesta) => {
      if (respuesta.exito === 0) {
        Swal.close();
        return Swal.fire({
          allowOutsideClick: false,
          text: respuesta.mensage,
          icon: "error",
        });
      }
      if (respuesta.exito === 1) {
        console.log(respuesta.data);
        this.infoAclGestion = respuesta.data;
        this.datoGrid = respuesta.data;
      }
      Swal.close();
    });
  }

  ConsultarArbolGestion() {
    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();
    //this.perfilesRequest = {user : '', password :''};
    this._arbolGestionService.all().subscribe((respuesta) => {
      if (respuesta.exito === 0) {
        Swal.close();
        return Swal.fire({
          allowOutsideClick: false,
          text: respuesta.mensage,
          icon: "error",
        });
      }
      if (respuesta.exito === 1) {
        console.log(respuesta.data);
        this.infoAclGestion = respuesta.data;
        this.datoGrid = respuesta.data;
      }
      Swal.close();
    });
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoAclGestion, this.estadoGrid);
  }

  EventoCheckSeleccionado(evento) {}

  cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {}

  cellCloseHandler(args: any) {
    const { formGroup, dataItem } = args;

    if (!formGroup.valid) {
      // prevent closing the edited cell if there are invalid values.
      args.preventDefault();
    } else if (formGroup.dirty) {
      this.assignValues(dataItem, formGroup.value);
      this.update(dataItem);
    }
  }

  assignValues(target: any, source: any): void {
    Object.assign(target, source);
  }

  update(item: any): void {
    if (!this.isNew(item)) {
      const index = itemIndex(item, this.updatedItems);
      if (index !== -1) {
        this.updatedItems.splice(index, 1, item);
      } else {
        this.updatedItems.push(item);
      }
    } else {
      const index = this.createdItems.indexOf(item);
      this.createdItems.splice(index, 1, item);
    }
  }

  isNew(item: any): boolean {
    return !item.idlote;
  }
  public telefonoValue: string = "";
  public telefonoMask: string = "(999) 000-00-00-00";

  public dptos: Array<string> = ["Sistemas", "Financiero"];

  public submitForm(): void {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      const gestion: CrmArbolGestion = GestionMapper.crear(this.registerForm.value);
      this._arbolGestionService.guardar(gestion).subscribe(
        (respuesta) => {
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Gestion agregada correctamente",
              showConfirmButton: false,
              timer: 1500,
            });
            this.ConsultarArbolGestion();
            this.registerForm.reset();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
      console.log(gestion);
    }
  }

  public clearForm(): void {
    this.registerForm.reset();
  }

  private formGuardar(): void {
    this.registerForm = this.fb.group({
      descripcion: ["", Validators.required],
      idParent: [""],
      peso: [
        "",
        [
          Validators.pattern(/^[0-9]+$/),
        ],
      ],
      idTipogestion: ["", Validators.required],
      idTipoContacto: ["", [Validators.required]],
      alerta: [""],
      estado: ["", [Validators.required]],
      // codigo:[''],
    });
  }

  cambiosModal(): void {
    this.modalService.onHidden.pipe(takeUntil(this.stop$)).subscribe((data) => {
      if (data === "ok") {
        this.ConsultarArbolGestion();
      }
    });
  }
  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
}
