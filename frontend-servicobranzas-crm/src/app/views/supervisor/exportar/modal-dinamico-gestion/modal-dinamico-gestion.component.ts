import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subject } from "rxjs";
import Swal from "sweetalert2";
import { CrmArbolGestion } from "../../../../models/CrmArbolGestion";
import { CrmArbolGestionTipoContacto } from "../../../../models/CrmArbolGestionTipoContacto";
import { CrmArbolGestionTipoGestion } from "../../../../models/CrmArbolGestionTipoGestion";
import { ArbolGestionServiceService } from "../../../../services/arbol-gestion.service";
import { GestionMapper } from "../../../../utils/mappers/arbol-gestion.mapper";
import { botones } from "../../../gestionar/gestion-normal/mock";

@Component({
  selector: "app-modal-dinamico-gestion",
  templateUrl: "./modal-dinamico-gestion.component.html",
  styleUrls: ["./modal-dinamico-gestion.component.css"],
})
export class ModalDinamicoGestionComponent implements OnInit {
  public Gestiones: any = [];
  public TipoContactos: any = [];
  public TipoGestiones: any = [];
  public PrimerNivel: any = [];
  //public SegundoNivel: CrmArbolGestion[] = [];
  //public tContactos: Array<string> = ["Sistemas", "Financiero"];
  public tGestiones: Array<any> = [{id: 1, texto: "Positivo"}, 
                                    {id: 2, texto: "Negativo"}, 
                                    {id: 4, texto: "YA PAGO"}];
  public estados: Array<string> = ["A", "I"];
  registerForm: FormGroup;
  private stop$ = new Subject<void>();
  titulo: string;
  gestion: CrmArbolGestion;
  //tipoContacto: CrmArbolGestionTipoContacto;
  //tipoGestion: CrmArbolGestionTipoGestion;

  constructor(
    public modalRef: BsModalRef,
    private _arbolGestionService: ArbolGestionServiceService,
    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService,
  ) {}

  async ngOnInit(): Promise<void> {
    this.fromDinamico();
    await this.allGestion();
    await this.primerNivel();
    await this.allTipoContacto();
  }

  async allGestion() {
    this._arbolGestionService.all().subscribe((respuesta) => {
      if (respuesta.exito === 0) {
        Swal.close();
        return Swal.fire({
          allowOutsideClick: false,
          text: respuesta.mensage,
          icon: "error",
        });
      }
      if (respuesta.exito === 1) {
        this.Gestiones = respuesta.data;
      }
      Swal.close();
    });
  }

  async primerNivel() {
    this._arbolGestionService.primerNivel().subscribe((respuesta) => {
      if (respuesta.exito === 0) {
        Swal.close();
        return Swal.fire({
          allowOutsideClick: false,
          text: respuesta.mensage,
          icon: "error",
        });
      }
      if (respuesta.exito === 1) {
        this.primerNivel = respuesta.data;
      }
      Swal.close();
    });
  }

  async allTipoContacto() {
    this._arbolGestionService.tipoContacto().subscribe((respuesta) => {
      this.TipoContactos = respuesta;
      console.log(this.TipoContactos);
      Swal.close();
    });
  }

  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
  
  private fromDinamico(): void {
    console.log(this.gestion);
    if (!this.gestion) {
      console.log("form guardar");
      this.formGuardar();
    } else {
      this.formEditar();
    }
  }

  public submitForm(event: any): void {
    event.preventDefault();
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      
      var item73 = this.PrimerNivel.filter(function(item) {
        console.log(this.registerForm.value);
        console.log(this.registerForm.value.IdParent);
        return item.idParent === this.registerForm.value.idParent;
      })[0];
      console.log(item73);
      //this.registerForm.value.labelEstado = this.Gestiones;
      const gestion: CrmArbolGestion = GestionMapper.crear(this.registerForm.value);
      this._arbolGestionService.guardar(gestion).subscribe(
        (respuesta) => {
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Gestion agregada correctamente",
              showConfirmButton: false,
              timer: 1500,
            });
            this.registerForm.reset();

            this.modalService.setDismissReason("ok");
            this.modalRef.hide();
            // this.Consultargestions();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
      console.log(gestion);
    }
  }

  guardadoDinamico(event: any): void {
    if (!this.gestion) {
      this.submitForm(event);
    } else {
      this.editarGestion(event);
    }
  }

  editarGestion(event: any): void {
    event.preventDefault();
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      
        console.log(this.registerForm);
        console.log(this.registerForm.value);
      var item73 = this.PrimerNivel.filter(function(item) {
        return item.idParent === this.registerForm.value.idParent;
      })[0];
      console.log(item73);
      //this.registerForm.value.labelEstado = this.Gestiones;
      const gestionEditar: CrmArbolGestion = GestionMapper.editar(
        this.registerForm.value,
        this.gestion.id
      );
      console.log(gestionEditar);
      this._arbolGestionService.editar(gestionEditar).subscribe(
        (respuesta) => {
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Gestion editada correctamente",
              showConfirmButton: false,
              timer: 1500,
            });
            this.registerForm.reset();

            this.modalService.setDismissReason("ok");
            this.modalRef.hide();
            // this.Consultargestions();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
    }
  }

  private formGuardar(): void {
    this.registerForm = this.fb.group({
      descripcion: ["", [Validators.required]],
      idParent: [],
      peso: [, [Validators.pattern(/^[0-9]+$/),],],
      idTipoGestion: ["", Validators.required],
      idTipoContacto: ["", [Validators.required]],
      estado: ["", [Validators.required]],
      //alerta: [""],
      labelEstado: [],
      labelNivel2: [],
    });
  }

  private formEditar(): void {
    this.registerForm = this.fb.group({
      descripcion: [this.gestion.descripcion, [Validators.required]],
      idParent: [this.gestion.idParent],
      peso: [this.gestion.peso,[Validators.pattern(/^[0-9]+$/),],],
      idTipoGestion: [this.gestion.idTipoGestion, Validators.required],
      idTipoContacto: [this.gestion.idTipoContacto, Validators.required],
      estado: [this.gestion.estado, [Validators.required]],
      //alerta: [this.gestion.alerta],
      labelEstado: [this.gestion.labelEstado],
      labelNivel2: [this.gestion.labelNivel2],
    });
  }
}
