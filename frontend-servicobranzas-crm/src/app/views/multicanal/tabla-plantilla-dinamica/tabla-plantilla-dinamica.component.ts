import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from "@angular/core";
import { GridDataResult } from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import { BsModalService } from "ngx-bootstrap/modal";
import { ModalDinamicoPlantillaComponent } from "../modal-dinamico-plantilla/modal-dinamico-plantilla.component";
import { LeerPlantillaModel } from "../models/leer-plantilla.model";

@Component({
  selector: "app-tabla-plantilla-dinamica",
  templateUrl: "./tabla-plantilla-dinamica.component.html",
  styleUrls: ["./tabla-plantilla-dinamica.component.css"],
})
export class TablaPlantillaDinamicaComponent implements OnInit, OnChanges {
  @Input() data: LeerPlantillaModel[];
  @Input() hola;
  // @Output() Seleccionados: EventEmitter<any[]> = new EventEmitter();
  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public cargandoGrid: boolean;
  public responseSms: any;
  constructor(private readonly modalService: BsModalService) {}
  ngOnChanges(changes: SimpleChanges): void {
    console.log("cambios hijo ", changes);
    if (changes.data.currentValue) {
      this.cargarTabla();
    }
  }

  ngOnInit(): void {
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
  }
  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log(estadoGrid);
    console.log("estados del grid sms", estadoGrid);

    this.estadoGrid = estadoGrid;
    this.datoGrid = process(this.responseSms, this.estadoGrid);
  }

  cargarTabla(): void {
    this.datoGrid = process(this.data, this.estadoGrid);
  }

  editarPlantilla(dataItem: LeerPlantillaModel): void {
    console.log("editar", dataItem);
    this.modalService.show(ModalDinamicoPlantillaComponent, {
      initialState: {
        data: dataItem,
        titulo: "Editar",
        tipo: this.hola,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  crearPlantilla(): void {
    this.modalService.show(ModalDinamicoPlantillaComponent, {
      initialState: {
        titulo: "Crear",
        tipo: this.hola,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  eliminarPlantilla(dataItem: LeerPlantillaModel): void {
    console.log("eliminar", dataItem);
  }
}
