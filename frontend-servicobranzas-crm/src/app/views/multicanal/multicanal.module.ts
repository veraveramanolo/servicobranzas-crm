import { LOCALE_ID, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MulticanalRoutingModule } from "./multicanal-routing.module";
import { SmsComponent } from "./sms/sms.component";
import { CorreoComponent } from "./correo/correo.component";
import { DinomiComponent } from "./dinomi/dinomi.component";
import { WhatsappComponent } from "./whatsapp/whatsapp.component";
import { TextToSpeechComponent } from "./text-to-speech/text-to-speech.component";
import { ContenedorComponent } from "./contenedor/contenedor.component";
import { ModalMarcadoresComponent } from "./modal-marcadores/modal-marcadores.component";
//primeng
import { CardModule } from "primeng/card";
import { TabViewModule } from "primeng/tabview";
import { AccordionModule } from "primeng/accordion";
import { OrderListModule } from "primeng/orderlist";

//kendo
import { GridModule } from "@progress/kendo-angular-grid";
import { ListViewModule } from "@progress/kendo-angular-listview";
import { LayoutModule } from "@progress/kendo-angular-layout";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { PopupModule } from "@progress/kendo-angular-popup";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { ButtonModule } from "primeng/button";
import { ModalGuardarFiltroComponent } from "./modal-guardar-filtro/modal-guardar-filtro.component";
import { LabelModule } from "@progress/kendo-angular-label";
import { DropDownListModule } from "@progress/kendo-angular-dropdowns";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";

//ngx-bootstrap
import { ModalModule } from "ngx-bootstrap/modal";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ModalTareaComponent } from "./modal-tarea/modal-tarea.component";
import { ModalObjetivoComponent } from "./modal-objetivo/modal-objetivo.component";
import { ModalAsignacionComponent } from "./modal-asignacion/modal-asignacion.component";
import { ModalSmsComponent } from "./modal-sms/modal-sms.component";
import { ModalEmailComponent } from "./modal-email/modal-email.component";
import { ModalDinomiComponent } from "./modal-dinomi/modal-dinomi.component";
import { ContenedorPlantillaComponent } from "./contenedor-plantilla/contenedor-plantilla.component";
import { ModalDinamicoPlantillaComponent } from "./modal-dinamico-plantilla/modal-dinamico-plantilla.component";
import { TablaPlantillaDinamicaComponent } from "./tabla-plantilla-dinamica/tabla-plantilla-dinamica.component";
import { NgxSpinnerModule } from "ngx-spinner";
// import "hammerjs";

@NgModule({
  declarations: [
    SmsComponent,
    CorreoComponent,
    DinomiComponent,
    WhatsappComponent,
    TextToSpeechComponent,
    ContenedorComponent,
    ModalGuardarFiltroComponent,
    ModalMarcadoresComponent,
    ModalTareaComponent,
    ModalObjetivoComponent,
    ModalAsignacionComponent,
    ModalSmsComponent,
    ModalEmailComponent,
    ModalDinomiComponent,
    ContenedorPlantillaComponent,
    ModalDinamicoPlantillaComponent,
    TablaPlantillaDinamicaComponent,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "es-ES" }],
  entryComponents: [
    ModalGuardarFiltroComponent,
    ModalMarcadoresComponent,
    ModalTareaComponent,
    ModalObjetivoComponent,
    ModalAsignacionComponent,
    ModalSmsComponent,
  ],
  imports: [
    CommonModule,
    MulticanalRoutingModule,
    CardModule,
    TabViewModule,
    GridModule,
    AccordionModule,
    OrderListModule,
    ListViewModule,
    LayoutModule,
    InputsModule,
    PopupModule,
    ButtonsModule,
    ButtonModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    LabelModule,
    DropDownListModule,
    DropDownsModule,
    DateInputsModule,
    NgxSpinnerModule,
  ],
})
export class MulticanalModule {}
