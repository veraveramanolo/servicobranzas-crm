import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ContenedorPlantillaComponent } from "./contenedor-plantilla/contenedor-plantilla.component";
import { ContenedorComponent } from "./contenedor/contenedor.component";

const routes: Routes = [
  {
    path: "",
    data: { titulo: "Multicanal" },
    children: [
      {
        path: "",
        redirectTo: "multicanal",
      },
      {
        path: "multicanal/contenedor",
        component: ContenedorComponent,
        data: { titulo: "Multicanal" },
      },
      {
        path: "multicanal/plantilla",
        component: ContenedorPlantillaComponent,
        data: { titulo: "Plantilla" },
      },
      // {
      //   path: 'multicanal/correo',
      //   component: CorreoComponent,
      //   data: {title: 'Correo'}
      // },
      // {
      //   path: 'multicanal/dinomi',
      //   component: DinomiComponent,
      //   data: {title: 'Dinomi'}
      // },
      // {
      //   path: 'multicanal/sms',
      //   component: SmsComponent,
      //   data: {title: 'Sms'}
      // },
      // {
      //   path: 'multicanal/text-to-speech',
      //   component: TextToSpeechComponent,
      //   data: {title: 'Text to Speech'}
      // },
      // {
      //   path: 'multicanal/whatsapp',
      //   component: WhatsappComponent,
      //   data: {title: 'Whatsapp'}
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MulticanalRoutingModule {}
