import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DinomiComponent } from './dinomi.component';

describe('DinomiComponent', () => {
  let component: DinomiComponent;
  let fixture: ComponentFixture<DinomiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinomiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DinomiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
