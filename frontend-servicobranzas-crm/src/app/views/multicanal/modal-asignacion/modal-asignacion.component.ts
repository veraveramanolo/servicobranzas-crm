import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { DataBindingDirective } from "@progress/kendo-angular-grid";
import { process } from "@progress/kendo-data-query";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { MulticanalService } from "../../../services/multicanal.service";

@Component({
  selector: "app-modal-asignacion",
  templateUrl: "./modal-asignacion.component.html",
  styleUrls: ["./modal-asignacion.component.css"],
})
export class ModalAsignacionComponent implements OnInit {
  tarea: any;
  asignacionForm: FormGroup;
  public gridData: any[] = [];
  public gridView: any[];
  public mySelection: string[] = [];
  ids = [];
  manual: boolean;
  constructor(
    private readonly _multicanalService: MulticanalService,
    public modalRef: BsModalRef,
    private readonly modalService: BsModalService,
    private fb: FormBuilder
  ) {}
  public ngOnInit(): void {
    this.obtenerAsesores();
  }
  private formulario(): void {
    this.asignacionForm = this.fb.group({});
  }
  verInfo(): void {
    console.log("mi seleccion", this.mySelection);
  }
  obtenerAsesores(): void {
    this._multicanalService.obtenerAsesores().subscribe((respuesta) => {
      console.log("asesores", respuesta);
      this.gridData = respuesta.data;
      this.gridView = this.gridData;
    });
  }
  submitForm(event: any): void {
    // let eviarDato: any;
    if (this.model.gender) {
      if (this.mySelection.length > 0 && this.model.gender === "M") {
        for (const id of this.mySelection) {
          const idU = { idUsuario: id };
          this.ids.push(idU);
        }
      } else {
        for (const usuario of this.gridData) {
          const idU = { idUsuario: usuario.idUsuario };
          this.ids.push(idU);
        }
      }

      const eviarDato = {
        idTarea: this.tarea.id,
        tipoasignacion: this.model.gender,
        crmAsignacionDet: this.ids,
      };

      console.log("datos enviar", eviarDato);
      this._multicanalService
        .insertarAsignacion(eviarDato)
        .subscribe((response) => {
          if (response.exito === 1) {
            this._multicanalService
              .asignaAsesor(response.data,response.asesores)
              .subscribe((respuesta) => {
                if (respuesta.exito === 1) {
                  this.modalService.setDismissReason("crear asignacion");
                  this.modalRef.hide();
                  Swal.fire({
                    title: "Correcto!",
                    html: "Asignacion creada!",
                    timer: 3000,
                    timerProgressBar: true,
                    showConfirmButton: false,
                    icon: "success",
                  });
                } else {
                  Swal.fire({
                    allowOutsideClick: false,
                    text: "error al guardar la asignacion!",
                    icon: "error",
                  });
                }
              });
          } else {
            Swal.fire({
              allowOutsideClick: false,
              text: "error al guardar la asignacion!",
              icon: "error",
            });
          }
          console.log("respuesta del insertar", response);
        });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "Seleccione tipo Manual o Automatico!",
        icon: "error",
      });
    }
  }
  cambioEstadoManual(event: any): void {
    console.log(event);
    this.manual = event;
  }
  public model = {
    gender: null,
  };
}
