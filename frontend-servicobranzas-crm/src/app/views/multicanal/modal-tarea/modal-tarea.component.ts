import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { MulticanalService } from "../../../services/multicanal.service";

@Component({
  selector: "app-modal-tarea",
  templateUrl: "./modal-tarea.component.html",
  styleUrls: ["./modal-tarea.component.css"],
})
export class ModalTareaComponent implements OnInit {
  tarea: any;
  idcampania: number;
  idmulticanal: number;
  tareaForm: FormGroup;
  tipo = [{ nombre: "Si" }, { nombre: "No" }];
  constructor(
    public modalRef: BsModalRef,
    private fb: FormBuilder,
    private readonly _multicanalService: MulticanalService,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.formDinamico();
  }

  private formDinamico(): void {
    if (this.tarea) {
      this.formEditar();
    } else {
      this.formGuardar();
    }
  }

  private formGuardar(): void {
    this.tareaForm = this.fb.group({
      Descripcion: ["", Validators.required],
      Estado: [null],
      FechaInicio: [null, Validators.required],
      FechaFin: [null, Validators.required],
      Meta: [1],
      // Idmulticanal: [null],
      // IdCampania: 17,
      Obligatorio: ["", Validators.required],

      // codigo:[''],
    });
  }

  public guardarDinamico(event: any): void {
    console.log("tarea enviada", this.tarea);
    if (!this.tarea) {
      this.submitForm(event);
    } else {
      this.submitFormEditar(event);
    }
  }

  private formEditar(): void {
    console.log("tarea enviada al form", this.tarea);
    this.tareaForm = this.fb.group({
      Descripcion: [this.tarea.descripcion, Validators.required],
      Estado: [this.tarea.estado, Validators.required],
      FechaInicio: [new Date(this.tarea.fechaInicio), Validators.required],
      FechaFin: [new Date(this.tarea.fechaFin), Validators.required],
      // Idmulticanal: [null],
      // IdCampania: 17,
      Obligatorio: [this.tarea.obligatorio, Validators.required],

      // codigo:[''],
    });
  }

  public submitForm(event: any): void {
    console.log("datos del form tarea", this.idcampania);
    this.tareaForm.markAllAsTouched();
    if (this.tareaForm.valid) {
      const tarea: any = this.tareaForm.value;
      tarea.IdCampania = this.idcampania;
      tarea.Idmulticanal = this.idmulticanal;

      console.log("datos del form tarea", tarea);
      this._multicanalService.crearTarea(tarea).subscribe(
        (respuesta) => {
          console.log("respuesta de guardar tarea", respuesta);
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            this.modalService.setDismissReason("tarea");
            this.modalRef.hide();
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "tarea creada correctamente",
              showConfirmButton: false,
              timer: 1500,
            });

            this.tareaForm.reset();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
    }
  }

  public submitFormEditar(event: any): void {
    this.tareaForm.markAllAsTouched();
    if (this.tareaForm.valid) {
      const tarea: any = this.tareaForm.value;
      tarea.Id = this.tarea.id;
      console.log("datos del form tarea editar", tarea);
      this._multicanalService.editarTarea(tarea).subscribe(
        (respuesta) => {
          console.log("respuesta de editar tarea", respuesta);
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            this.modalService.setDismissReason("tarea");
            this.modalRef.hide();
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "tarea editada correctamente",
              showConfirmButton: false,
              timer: 1500,
            });

            this.tareaForm.reset();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
    }
  }
}
