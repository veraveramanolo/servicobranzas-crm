import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { LeerPlantillaModel } from "../models/leer-plantilla.model";

@Component({
  selector: "app-modal-dinamico-plantilla",
  templateUrl: "./modal-dinamico-plantilla.component.html",
  styleUrls: ["./modal-dinamico-plantilla.component.css"],
})
export class ModalDinamicoPlantillaComponent implements OnInit {
  public data: LeerPlantillaModel;
  public tipo: string;
  public titulo: string;
  public dinamicForm: FormGroup;
  public numeroCaracteres: number = 160;
  public valorText: string;
  public sms: string =
    "Ejemplo: Estimado(a): ${nombre} le recordamos que mantiene una deuda con ${campania} por: ${deuda} por favor acerquese a la brevedad";

  constructor(
    public modalRef: BsModalRef,
    private fb: FormBuilder,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.formDinamico();
  }
  private formDinamico(): void {
    if (this.data) {
      this.formEditar();
    } else {
      this.formGuardar();
    }
  }

  private formGuardar(): void {
    this.dinamicForm = this.fb.group({
      nombrePlantilla: [null, Validators.required],
      bodyPlantilla: [null, Validators.required],
      tipoPlantilla: [this.tipo, Validators.required],
    });
  }

  public submitDinamico(event: any): void {
    console.log("plantilla enviada", this.data);
    if (!this.data) {
      this.submitForm(event);
    } else {
      this.submitFormEditar(event);
    }
  }

  private formEditar(): void {
    console.log("plantilla enviada al form", this.data);
    this.dinamicForm = this.fb.group({
      nombrePlantilla: [this.data.nombrePlantilla, [Validators.required]],
      bodyPlantilla: [this.data.bodyPlantilla, Validators.required],
      tipoPlantilla: [this.tipo, Validators.required],
    });
  }

  public submitForm(event: any): void {
    this.dinamicForm.markAllAsTouched();
    if (this.dinamicForm.valid) {
      // const tarea: any = this.dinamicForm.value;
      // tarea.IdCampania = this.idcampania;
      // tarea.Idmulticanal = this.idmulticanal;
      // console.log("datos del form tarea", tarea);
      // this._multicanalService.crearTarea(tarea).subscribe(
      //   (respuesta) => {
      //     console.log("respuesta de guardar tarea", respuesta);
      //     if (respuesta.exito === 0) {
      //       Swal.close();
      //       Swal.fire({
      //         allowOutsideClick: false,
      //         text: respuesta.mensage,
      //         icon: "error",
      //       });
      //     } else {
      //       this.modalService.setDismissReason("guardar plantilla");
      //       this.modalRef.hide();
      //       Swal.fire({
      //         position: "top-end",
      //         icon: "success",
      //         title: "tarea creada correctamente",
      //         showConfirmButton: false,
      //         timer: 1500,
      //       });
      //       this.dinamicForm.reset();
      //     }
      //   },
      //   (err) =>
      //     Swal.fire({
      //       icon: "error",
      //       title: "Oops...",
      //       text: err.message,
      //     })
      // );
    }
  }

  public submitFormEditar(event: any): void {
    // this.dinamicForm.markAllAsTouched();
    // if (this.dinamicForm.valid) {
    //   const tarea: any = this.dinamicForm.value;
    //   tarea.Id = this.tarea.id;
    //   console.log("datos del form tarea editar", tarea);
    //   this._multicanalService.editarTarea(tarea).subscribe(
    //     (respuesta) => {
    //       console.log("respuesta de editar tarea", respuesta);
    //       if (respuesta.exito === 0) {
    //         Swal.close();
    //         Swal.fire({
    //           allowOutsideClick: false,
    //           text: respuesta.mensage,
    //           icon: "error",
    //         });
    //       } else {
    //         this.modalService.setDismissReason("tarea");
    //         this.modalRef.hide();
    //         Swal.fire({
    //           position: "top-end",
    //           icon: "success",
    //           title: "tarea editada correctamente",
    //           showConfirmButton: false,
    //           timer: 1500,
    //         });
    //         this.dinamicForm.reset();
    //       }
    //     },
    //     (err) =>
    //       Swal.fire({
    //         icon: "error",
    //         title: "Oops...",
    //         text: err.message,
    //       })
    //   );
    // }
  }

  get oBodyPlantilla(): any {
    return this.dinamicForm.get("bodyPlantilla");
  }
  cambioTexto(value: string): void {
    console.log("valor cambio", value);

    if (value.length <= 160) {
      this.valorText = value;
      this.numeroCaracteres = 160 - value.length;
    } else {
      this.dinamicForm.controls["bodyPlantilla"].setValue(this.valorText);
    }
  }
}
