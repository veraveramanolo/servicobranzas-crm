export interface CrearPlantillaModelo {
  nombrePlantilla: string;
  bodyPlantilla: string;
  tipoPlantilla: string;
}
