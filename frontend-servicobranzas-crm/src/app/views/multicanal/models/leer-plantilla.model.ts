export interface LeerPlantillaModel {
  idPlantilla: number;
  bodyPlantilla: string;
  nombrePlantilla: string;
}
