import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { MulticanalService } from "../../../services/multicanal.service";

@Component({
  selector: "app-modal-guardar-filtro",
  templateUrl: "./modal-guardar-filtro.component.html",
  styleUrls: ["./modal-guardar-filtro.component.css"],
})
export class ModalGuardarFiltroComponent implements OnInit {
  detalle: any;
  filtro: any;
  idcampania: number;
  Item: any;
  filtroForm: FormGroup;
  constructor(
    public modalRef: BsModalRef,
    private readonly modalService: BsModalService,
    private readonly fb: FormBuilder,
    private readonly multicanalService: MulticanalService
  ) {}

  ngOnInit(): void {
    this.form();
  }
  private form(): void {
    this.filtroForm = this.fb.group({
      descripcion: ["", Validators.required],
    });
  }

  guardarFiltro(event: any): void {
    const contenido = this.filtroForm.value;
    const cabecera = {
      descripcion: contenido.descripcion,
      Filtro: JSON.stringify(this.filtro),
      IdCampania: this.idcampania,
      FechaCreacion: null,
      UserCreacion: null,
      crmMulticanalDetalle: this.detalle,
    };
    console.log("guardar cabecera", cabecera);
    this.multicanalService
      .crearCabeceraFiltro(cabecera)
      .subscribe((response) => {
        if (response.exito === 1) {
          // this.multicanalService
          //   .crearDetalleFiltro(this.detalle)
          //   .subscribe((respuesta) => {
          // console.log("detalle de la cabecera datos", this.detalle);
          // console.log("detalle de la cabecera respuesta ", respuesta);
          // if (respuesta.exito === 1) {
          this.filtroForm.reset();

          this.modalService.setDismissReason("filtro");
          this.modalRef.hide();
          Swal.fire({
            title: "Correcto!",
            html: "filtro creado!",
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
          // } else {
          //   Swal.fire({
          //     allowOutsideClick: false,
          //     text: "error al guardar la cabecera del filtro!",
          //     icon: "error",
          //   });
          // }
          //});
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: "error al guardar la cabecera del filtro!",
            icon: "error",
          });
        }

        console.log(response);
      });
    console.log(contenido);
    // this._dinomiService
    //   .smsDinomi(this.Item.numero, contenido.mensaje)
    //   .subscribe((respuesta) => {
    //     if (respuesta.respuesta === "success") {
    //       this.filtroForm.reset();

    //       this.modalService.setDismissReason("ok");
    //       this.modalRef.hide();
    //       Swal.fire({
    //         title: "Correcto!",
    //         html: "Mensaje enviado!",
    //         timer: 3000,
    //         timerProgressBar: true,
    //         showConfirmButton: false,
    //         icon: "success",
    //       });
    //     } else {
    //       Swal.fire({
    //         allowOutsideClick: false,
    //         text: "Error al enviar el mnsaje!",
    //         icon: "error",
    //       });
    //     }
    //   });
  }
}
