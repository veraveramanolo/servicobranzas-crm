import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { CrmTareaObjetivos } from "../../../models/CrmTareaObjetivos";
import { ArbolGestionServiceService } from "../../../services/arbol-gestion.service";
import { MulticanalService } from "../../../services/multicanal.service";

@Component({
  selector: "app-modal-objetivo",
  templateUrl: "./modal-objetivo.component.html",
  styleUrls: ["./modal-objetivo.component.css"],
})
export class ModalObjetivoComponent implements OnInit {
  objetivoForm: FormGroup;
  tipoSeleccionado: string;
  idTarea: number;
  idTipoobjetivo: number;
  public checked = true;
  GetParamObj = [];
  placeh = null;
  arbol: any = [];

  tipo = [
    // { nombre: "Crear Acuerdos", id: 1 },
    // { nombre: "Confirmar Pagos", id: 2 },
    // { nombre: "Tipificar Cliente", id: 3 },
    // { nombre: "Contactar", id: 4 },
    // { nombre: "Perfilar", id: 5 },
    // { nombre: "Actualizar Datos", id: 6 },
  ];

  constructor(
    public modalRef: BsModalRef,
    private fb: FormBuilder,
    private readonly modalService: BsModalService,
    private readonly _multicanalService: MulticanalService,
    private readonly _arbolGestionService: ArbolGestionServiceService
  ) {}

  ngOnInit(): void {
    this.formulario();
    this.obtenerTipoObjetivo();
    this.obtenerGetParamObj();
    this.arbolGestion();
  }
  private formulario(): void {
    this.objetivoForm = this.fb.group({
      idTipoobjetivo: [null],
      idParametro: [null],
      caValorparametro: [null],
      valorparametroHabilitar: [null],
      valorparametroFinalizar: [null],
      caValor: [null],
      caValorhabilitar: [null],
      caValorfinalizar: [null],
      caCantidad: [null],
      caCantidadhabilitar: [null],
      caCantidadfinalizar: [null],
      hola: [null],
      idgestionPrincipal: [null],
      idgestionSec1: [null],
      idgestionSec2: [null],
      tcCantidad: [null],
      tcCantidadhabilitar: [null],
      tcCantidadfinalizar: [null],
      tcValor: [null],
      tcValorhabilitar: [null],
      tcValorfinalizar: [null],
      pCantidad: [null],
      pCantidadhabilitar: [null],
      pCantidadfinalizar: [null],
    });
  }
  arbolGestion(): void {
    this._arbolGestionService.primerNivel().subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.arbol = respuesta.data;
        console.log("arbol de gestion", respuesta.data);
      }
    });
  }
  devolverNull(value: any, nombre: string): any {
    let salida = null;
    if (value) {
      salida = value[nombre];
    }
    return salida;
  }

  submitForm(event: any): void {
    // let idparam = null;
    const formulario = this.objetivoForm.value;
    // if (formulario.idParametro) {
    //   idparam = formulario.idParametro.idParametro;
    // }
    const datoEnviar: CrmTareaObjetivos = {
      idTarea: this.idTarea,
      idTipoobjetivo: 1,
      idParametro: this.devolverNull(formulario.idParametro, "idParametro"),
      caValorparametro: formulario.caValorparametro,
      valorparametroHabilitar: formulario.valorparametroHabilitar,
      valorparametroFinalizar: formulario.valorparametroFinalizar,
      caValor: formulario.caValor,
      caValorhabilitar: formulario.caValorhabilitar,
      caValorfinalizar: formulario.caValorfinalizar,
      caCantidad: formulario.caCantidad,
      caCantidadhabilitar: formulario.caCantidadhabilitar,
      caCantidadfinalizar: formulario.caCantidadfinalizar,
      idgestionPrincipal: this.devolverNull(
        formulario.idgestionPrincipal,
        "id"
      ),
      idgestionSec1: this.devolverNull(formulario.idgestionSec1, "id"),
      idgestionSec2: this.devolverNull(formulario.idgestionSec2, "id"),
      tcCantidad: formulario.tcCantidad,
      tcCantidadhabilitar: formulario.tcCantidadhabilitar,
      tcCantidadfinalizar: formulario.tcCantidadfinalizar,
      tcValor: formulario.tcValor,
      tcValorhabilitar: formulario.tcValorhabilitar,
      tcValorfinalizar: formulario.tcValorfinalizar,
      pCantidad: formulario.pCantidad,
      pCantidadhabilitar: formulario.pCantidadhabilitar,
      pCantidadfinalizar: formulario.pCantidadfinalizar,
    };
    console.log("formulario", datoEnviar);

    this._multicanalService.crearObjetivo(datoEnviar).subscribe((respuesta) => {
      console.log("guardar objetivo", respuesta);
      if (respuesta.exito === 1) {
        this.objetivoForm.reset();

        this.modalService.setDismissReason("crear objetivo");
        this.modalRef.hide();
        Swal.fire({
          title: "Correcto!",
          html: "Objetivo creado!",
          timer: 3000,
          timerProgressBar: true,
          showConfirmButton: false,
          icon: "success",
        });
      } else {
        Swal.fire({
          allowOutsideClick: false,
          text: "error al guardar el objetivo!",
          icon: "error",
        });
      }
    });
  }

  CambioTipoObjetivo(event: any): void {
    // this.objetivoForm.reset();
    this.idTipoobjetivo = event.idTipoobjetivo;
    const entrada = event.valorTipoobjetivo.toUpperCase();
    if (entrada === "CREAR ACUERDOS" || entrada === "CONFIRMAR PAGOS") {
      console.log("1", entrada);
      this.tipoSeleccionado = "1";
    } else if (entrada === "TIPIFICAR CLIENTE" || entrada === "CONTACTAR") {
      console.log("2", entrada);
      this.tipoSeleccionado = "2";
    } else if (entrada === "PERFILAR" || entrada === "ACTUALIZAR DATOS") {
      console.log("3", entrada);
      this.tipoSeleccionado = "3";
    }
    console.log(event);
  }
  CambioValorUltimoPago(event: any): void {
    console.log("CambioValorUltimoPago", event);
    this.placeh = event.valorParametro;
  }
  obtenerTipoObjetivo(): void {
    this._multicanalService.obtenerTipoObjetivo().subscribe((respuesta) => {
      console.log("obtenerTipoObjetivo", respuesta);
      this.tipo = respuesta.data;
    });
  }
  obtenerGetParamObj(): void {
    this._multicanalService.obtenerGetParamObj().subscribe((respuesta) => {
      console.log("obtenerGetParamObj", respuesta);
      this.GetParamObj = respuesta.data;
    });
  }
}
