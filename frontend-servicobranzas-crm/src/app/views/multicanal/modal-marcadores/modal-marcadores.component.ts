import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { MulticanalService } from "../../../services/multicanal.service";

@Component({
  selector: "app-modal-marcadores",
  templateUrl: "./modal-marcadores.component.html",
  styleUrls: ["./modal-marcadores.component.css"],
})
export class ModalMarcadoresComponent implements OnInit {
  idcampania: number;
  multicanalForm: FormGroup;
  tipo = [{ nombre: "Automatica" }, { nombre: "manual" }];
  constructor(
    public modalRef: BsModalRef,
    private fb: FormBuilder,
    private readonly _multicanalService: MulticanalService,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.formGuardar();
  }

  private formGuardar(): void {
    this.multicanalForm = this.fb.group({
      Nombre: ["", Validators.required],
      Opcion: ["", Validators.required],
      FechaInicio: [null, Validators.required],
      FechaFin: [null, Validators.required],
      NumeroCola: ["", Validators.required],
      IdGrupo: ["", Validators.required],
      IdUrl: ["", Validators.required],

      // codigo:[''],
    });
  }

  public submitForm(event: any): void {
    console.log("datos del form multicanal", this.idcampania);
    this.multicanalForm.markAllAsTouched();
    if (this.multicanalForm.valid) {
      const multicanal: any = this.multicanalForm.value;
      multicanal.IdCampania = this.idcampania;
      multicanal.FechaCreacion = null;
      multicanal.UserCreacion = "1";

      console.log("datos del form multicanal", multicanal);
      this._multicanalService.crearMarcador(multicanal).subscribe(
        (respuesta) => {
          console.log("respuesta de guardar marcador", respuesta);
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            this.modalService.setDismissReason("marcador");
            this.modalRef.hide();
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Marcador creado correctamente",
              showConfirmButton: false,
              timer: 1500,
            });

            this.multicanalForm.reset();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
    }
  }
}
