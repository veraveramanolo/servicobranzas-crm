import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FilterService, GridDataResult } from "@progress/kendo-angular-grid";
import { format } from "date-fns";
import {
  State,
  process,
  filterBy,
  FilterDescriptor,
  CompositeFilterDescriptor,
} from "@progress/kendo-data-query";
import { BsModalService } from "ngx-bootstrap/modal";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import Swal from "sweetalert2";
import { AclUsuario } from "../../../models/AclUsuario";
import { CrmCampania } from "../../../models/CrmCampania";
import { CrmCartera } from "../../../models/CrmCartera";
import { LeerMulticanal } from "../../../models/CrmMulticanal";
import { ModeloRespuesta } from "../../../models/ModeloRespuesta.models";
import { CampaniaService } from "../../../services/campania.service";
import { CrmCarteraService } from "../../../services/crm-cartera.service";
import { MulticanalService } from "../../../services/multicanal.service";
import { VariableGlobalService } from "../../../services/variable-global.service";
import { ModalGuardarFiltroComponent } from "../modal-guardar-filtro/modal-guardar-filtro.component";
import { ModalMarcadoresComponent } from "../modal-marcadores/modal-marcadores.component";
import { ModalTareaComponent } from "../modal-tarea/modal-tarea.component";
import { exportCSVFile } from "./export-csv";
import { ModalObjetivoComponent } from "../modal-objetivo/modal-objetivo.component";
import { ModalAsignacionComponent } from "../modal-asignacion/modal-asignacion.component";
import { ModalSmsComponent } from "../modal-sms/modal-sms.component";
import { ModalEmailComponent } from "../modal-email/modal-email.component";
const flatten = (filter) => {
  const filters = (filter || {}).filters;
  if (filters) {
    return filters.reduce(
      (acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]),
      []
    );
  }
  return [];
};

const distinctNombre = (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x, idx, xs) => {
      // console.log(x, idx, xs);
      return xs.findIndex((y) => y === x) === idx;
    });

const distinctExtra = (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x: string, idx, xs) => {
      let limpiar;
      if (x != null) {
        limpiar = x.trim();
      } else {
        limpiar = x;
      }

      // console.log(x, idx, xs);
      return (
        xs.findIndex(
          (y) => y === limpiar && limpiar != null && limpiar != ""
        ) === idx
      );
    });

@Component({
  selector: "app-contenedor",
  templateUrl: "./contenedor.component.html",
  styleUrls: ["./contenedor.component.css"],
})
export class ContenedorComponent implements OnInit, OnDestroy {
  index: number = 0;
  private stop$ = new Subject<void>();
  public InfoCampania: CrmCampania[] = [];
  public listaCartera: CrmCartera[] = [];
  public InfoCartera: CrmCartera[] = [];
  public valorComboCampania: CrmCampania;
  public listaCampania: CrmCampania[] = [];
  public listaCampaniafiltrada: CrmCampania[] = [];
  public listaCampaniaEmergente: CrmCampania[] = [];
  public usuarioLogin: AclUsuario;
  campaniaSeleccionada: CrmCampania;
  tareaSeleccionada: any;
  public position: "top" | "bottom" | "both" = "top";
  params: any;
  public mostrarTodoBotones = false;
  public mostrarBotonesTarea = false;
  public cargandoGrid: boolean;
  public cargandoGridTarea: boolean;
  responseMulticanal: any;
  responseTarea: any;
  dataDescargarCsv: any;
  busquedaDinamicaGrid: GridDataResult;
  public datoGrid: GridDataResult;
  public datoGridTarea: GridDataResult;
  public estadoGrid: State;
  public estadoGridTarea: State;
  public icon = "cog";
  fechaFiltro = null;
  public estadofiltro: any = [];
  public estadoListaCantidad: any = [];
  private estadoFilter: any[] = [];
  public data: Array<any> = [
    {
      text: "Email",
      icon: "k-icon k-i-envelop",
    },
    {
      text: "Sms",
      icon: "k-icon k-i-track-changes",
    },
    {
      text: "Tarea",
      icon: "k-icon k-i-table-align-middle-center",
    },
    {
      text: "Dinomi",
      icon: "k-icon k-i-grid",
    },
    {
      text: "Eliminar",
      icon: "k-icon k-i-close-circle",
    },
  ];

  public toggleText = "VER";
  public show = false;

  public toggleTextMarcador = "VER";
  public showMarcador = false;

  public toggleTextTarea = "VER";
  public showTarea = false;

  public toggleTextSms = "VER";
  public showSms = false;
  marcadores = [];
  tareas = [];
  sms = [];
  filtros = [];
  public filtrados = [];
  public asesores = [];
  public todosLosFiltros = [];

  public nombres = [];
  public nocuentas = [];
  public cedulas = [];
  public extra1 = [];
  public extra2 = [];
  public extra3 = [];
  public extra4 = [];
  public extra5 = [];
  public extra6 = [];
  public extra7 = [];
  public extra8 = [];
  public extra9 = [];
  public extra10 = [];

  constructor(
    private readonly _multicanalService: MulticanalService,
    private readonly modalService: BsModalService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private global: VariableGlobalService,
    private serviceCrmCartera: CrmCarteraService,
    private serviceCarmpania: CampaniaService
  ) {
    this.ConsultarUsuarioLogin();
  }
  ngOnDestroy(): void {
    this.stop();
  }

  async ngOnInit() {
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    this.estadoGridTarea = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    // this.obtenerMulticanal();
    await this.GetCarteraGestor(this.usuarioLogin.idUsuario);
    await this.GetCampaniaGestor(this.usuarioLogin.idUsuario);
    this.cambiosModal();
    // this.obtenerDetalleFiltro();
  }
  obtenerListaEstados(idcampania: number, fecha: any): void {
    this._multicanalService
      .obtenerMultiselect(idcampania, fecha)
      .subscribe((data) => {
        console.log("listado estados", data);
        // this.estadoListaCantidad = data.data;
        this.asesores = data.asesores;
        this.todosLosFiltros = data.data;
        // this.todosLosFiltros[1] = {
        //   gestion: "SIN GESTION",
        //   mejorgestion: "SIN GESTION",
        // };

        // this.todosLosFiltros.splice(1, 1);

        // this.estadofiltro = [...this.asesores];
        // this.estadofiltro.pop();
        // console.log("Last: ", this.estadofiltro);
      });
  }
  public estadoFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.estadoFilter.splice(
      0,
      this.estadoFilter.length,
      ...flatten(filter).map(({ value }) => value)
    );
    console.log("estadoFilter", this.estadoFilter);
    return this.estadoFilter;
  }

  public estadoChange(
    values: any[],
    filterService: FilterService,
    tipo: string
  ): void {
    console.log("estadoChange", values);
    filterService.filter({
      filters: values.map((value) => ({
        field: tipo,
        operator: "eq",
        value,
      })),
      logic: "or",
    });
  }

  cellClickHandler(event: any): void {
    this.mostrarBotonesTarea = true;
    this.tareaSeleccionada = event.dataItem;
    this.index = 1;
    console.log("seleccionar una tarea", event);
  }

  agregarObtetivo(): void {
    this.modalService.show(ModalObjetivoComponent, {
      initialState: {
        idTarea: this.tareaSeleccionada.id,
        // idcampania: this.campaniaSeleccionada.id,
        // idmulticanal: Number(this.tareaSeleccionada.idmulticanal),
        // tarea: this.tareaSeleccionada,
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
    console.log("agregar objetivo");
  }
  asignar(): void {
    console.log("asignar manual o automatico");

    this.modalService.show(ModalAsignacionComponent, {
      initialState: {
        // idcampania: this.campaniaSeleccionada.id,
        // idmulticanal: Number(this.tareaSeleccionada.idmulticanal),
        tarea: this.tareaSeleccionada,
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  editarTarea(): void {
    console.log("editar Tarea");

    this.modalService.show(ModalTareaComponent, {
      initialState: {
        idcampania: this.campaniaSeleccionada.id,
        idmulticanal: Number(this.tareaSeleccionada.idmulticanal),
        tarea: this.tareaSeleccionada,
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  filtrarFecha(event: Date): void {
    this.mostrarBotonesTarea = false;
    this.mostrarTodoBotones = true;
    this.cargandoGrid = true;
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    this._multicanalService
      .filtroFecha(this.campaniaSeleccionada.id, event)
      .subscribe((respuesta) => {
        this.cargandoGrid = false;
        this.datoGrid = process(respuesta.data, this.estadoGrid);
        console.log("respuesta del filtro de fecha", respuesta);
        this.obtenerListaEstados(this.campaniaSeleccionada.id, event);
      });
  }

  eliminarTarea(): void {
    console.log("eliminar Tarea");
    Swal.fire({
      title: "Esta seguro?",
      text: "No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminalo!",
    }).then((result) => {
      if (result.isConfirmed) {
        this._multicanalService
          .eliminarTarea(this.tareaSeleccionada.id)
          .subscribe((respuesta) => {
            if (respuesta.exito === 1) {
              this.obtenerTareas();
              Swal.fire(
                "Eliminado!",
                "La tarea fue Eliminada correctamente.",
                "success"
              );
            } else {
              Swal.fire({
                allowOutsideClick: false,
                text: "error al eliminar la tarea!",
                icon: "error",
              });
            }
          });
      }
    });
  }

  regresarClientes(): void {
    this.mostrarBotonesTarea = false;
    console.log("regresar a clientes");
  }

  obtenerMarcadores(): void {
    this._multicanalService
      .obtenerMarcadores(this.campaniaSeleccionada.id)
      .subscribe((respuesta) => {
        this.marcadores = respuesta.data;
        console.log("obtener marcadores", respuesta);
      });
  }

  async GetCampaniaGestor(idUsuario: number) {
    this.listaCampania = [];
    this.InfoCampania = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetCampaniaGestor(idUsuario);

    this.InfoCampania = resp.data;
    this.listaCampania = this.InfoCampania;
  }

  FiltroComboCampania(valor) {
    this.listaCampaniafiltrada = this.listaCampaniaEmergente.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }
  async GetCarteraGestor(idUsuario: number) {
    this.listaCartera = [];
    this.InfoCartera = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCrmCartera.GetCarteraGestor(idUsuario);

    this.InfoCartera = resp.data;
    this.listaCartera = this.InfoCartera;
  }

  ConsultarUsuarioLogin() {
    this.usuarioLogin = this.global.ConsultarUsuarioLogin();
  }

  FiltroComboCartera(valor) {
    this.listaCartera = this.InfoCartera.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }

  EventoComboCartera(valor: CrmCartera) {
    this.datoGrid = undefined;
    this.valorComboCampania = undefined;
    if (valor) {
      this.listaCampaniafiltrada = [];
      this.listaCampaniafiltrada = this.listaCampania.filter(
        (x) => x.idCartera === valor.id
      );
      this.listaCampaniaEmergente = this.listaCampaniafiltrada;
    } else {
      this.listaCampaniafiltrada = [];
    }

    // this.global.valorDropGestionNormal.cartera = valor;
    // this.GuardarValorDrop(this.global.valorDropGestionNormal);
  }

  obtenerMulticanal(valor: CrmCampania): void {
    this.fechaFiltro = null;
    this.mostrarBotonesTarea = false;
    if (valor) {
      this.mostrarTodoBotones = false;
      this.estadoGrid = {
        skip: 0,
        take: 8,
        filter: { logic: "and", filters: [] },
      };
      this.cargandoGrid = true;
      this._multicanalService
        .obtenerMulticanal(valor.id)
        .subscribe((response) => {
          this.nombres = distinctNombre(response.data, "nombrecompleto");
          this.nocuentas = distinctNombre(response.data, "nocuenta");
          this.cedulas = distinctNombre(response.data, "identificacion");
          this.extra1 = distinctExtra(response.data, "extra1");
          this.extra2 = distinctExtra(response.data, "extra2");
          this.extra3 = distinctExtra(response.data, "extra3");
          this.extra4 = distinctExtra(response.data, "extra4");
          this.extra5 = distinctExtra(response.data, "extra5");
          this.extra6 = distinctExtra(response.data, "extra6");
          this.extra7 = distinctExtra(response.data, "extra7");
          this.extra8 = distinctExtra(response.data, "extra8");
          this.extra9 = distinctExtra(response.data, "extra9");
          this.extra10 = distinctExtra(response.data, "extra10");
          this.obtenerListaEstados(valor.id, this.fechaFiltro);
          this.cargandoGrid = false;
          this.campaniaSeleccionada = valor;
          console.log("respuesta de multicanal", response);
          this.responseMulticanal = response.data;
          this.obtenerTareas();
          this.obtenerFiltro();
          this.obtenerMarcadores();
          this.dataDescargarCsv = response.data;
          this.datoGrid = process(this.responseMulticanal, this.estadoGrid);
        });
    } else {
      this.cargandoGrid = false;
      this.campaniaSeleccionada = null;
      this.responseMulticanal = null;
      this.dataDescargarCsv = null;
      this.datoGrid = null;
      this.filtros = null;
      this.filtrados = null;
      this.estadoGrid = {
        skip: 0,
        take: 8,
        filter: { logic: "and", filters: [] },
      };
      this.mostrarTodoBotones = false;
    }
  }

  obtenerFiltro(): void {
    this._multicanalService
      .obtenerFiltros(this.campaniaSeleccionada.id)
      .subscribe((response) => {
        this.filtros = response.data;
        this.filtrados = [...this.filtros];
        console.log("respuesta de filtros", response);
      });
  }

  obtenerTareas(): void {
    this.estadoGridTarea = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    this.cargandoGridTarea = true;
    this._multicanalService
      .obtenerTareas(this.campaniaSeleccionada.id)
      .subscribe((response) => {
        if (response.data) {
          for (let index = 0; index < response.data.length; index++) {
            const element = response.data[index];
            if (element.fechaCreacion) {
              response.data[index].fechaCreacion = format(
                new Date(element.fechaCreacion),
                "yyyy/MM/dd"
              );
            }
          }
        }

        console.log("respuesta de tareas", response);
        this.cargandoGridTarea = false;
        this.responseTarea = response.data;

        this.datoGridTarea = process(this.responseTarea, this.estadoGridTarea);
      });
  }

  obtenerDetalleFiltro(filtro: any): void {
    this.mostrarTodoBotones = true;
    this.cargandoGrid = true;
    this._multicanalService
      .obtenerDetalleFiltros(this.campaniaSeleccionada.id, filtro.idmulticanal)
      .subscribe((response) => {
        if (filtro.filtro) {
          this.estadoGrid = JSON.parse(filtro.filtro);
        } else {
          this.estadoGrid = {
            skip: 0,
            take: 8,
            filter: { logic: "and", filters: [] },
          };
        }
        this.cargandoGrid = false;
        this.responseMulticanal = response.data;
        this.dataDescargarCsv = response.data;
        this.datoGrid = process(this.responseMulticanal, this.estadoGrid);
        this.datoGrid.data = response.data;
        console.log(this.responseMulticanal);
        console.log(this.estadoGrid);
        console.log(this.datoGrid);
        console.log("respuesta dettale de fiiltro", response);
        // const jose2 = JSON.stringify(this.jose);
        // console.log("gridfiltro string", JSON.stringify(this.jose));
        // console.log("gridfiltro string", JSON.parse(jose2));
      });
  }
  open(): void {
    // this.params = this.activatedRoute.snapshot.params;
    this.modalService.show(ModalGuardarFiltroComponent, {
      initialState: {
        detalle: this.dataDescargarCsv,
        idcampania: this.campaniaSeleccionada.id,
        filtro: this.estadoGrid,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  openSms(): void {
    // this.params = this.activatedRoute.snapshot.params;
    this.modalService.show(ModalSmsComponent, {
      initialState: {
        detalle: this.dataDescargarCsv,
        campania: this.campaniaSeleccionada,
        filtro: this.estadoGrid,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  openEmail(): void {
    // this.params = this.activatedRoute.snapshot.params;
    this.modalService.show(ModalEmailComponent, {
      initialState: {
        detalle: this.dataDescargarCsv,
        campania: this.campaniaSeleccionada,
        filtro: this.estadoGrid,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  openMarcador(): void {
    // this.params = this.activatedRoute.snapshot.params;
    this.modalService.show(ModalMarcadoresComponent, {
      initialState: {
        idcampania: this.campaniaSeleccionada.id,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  openTarea(filtro: any): void {
    // this.params = this.activatedRoute.snapshot.params;
    this.modalService.show(ModalTareaComponent, {
      initialState: {
        idcampania: this.campaniaSeleccionada.id,
        idmulticanal: Number(filtro.idmulticanal),
        tarea: null,
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  public handleFilterChange(query: string): void {
    const normalizedQuery = query.toLowerCase();
    const filterExpession = (item: any) =>
      item.name.toLowerCase().indexOf(normalizedQuery) !== -1; //||
    // item.categoryName.toLowerCase().indexOf(normalizedQuery) !== -1;

    this.filtrados = this.filtros.filter(filterExpession);
  }

  public onToggle(): void {
    this.show = !this.show;
    this.toggleText = this.show ? "OCULTAR" : "VER";
  }

  public onToggleMarcador(): void {
    this.showTarea = false;
    this.showSms = false;
    this.showMarcador = !this.showMarcador;
    this.toggleTextMarcador = this.showMarcador ? "OCULTAR" : "VER";
  }
  public onToggleTarea(): void {
    this.showMarcador = false;
    this.showSms = false;
    this.showTarea = !this.showTarea;
    this.toggleTextTarea = this.showTarea ? "OCULTAR" : "VER";
  }
  public onToggleSms(): void {
    this.showTarea = false;
    this.showMarcador = false;
    this.showSms = !this.showSms;
    this.toggleTextSms = this.showSms ? "OCULTAR" : "VER";
  }

  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log(estadoGrid);
    console.log("estados del grid", estadoGrid);
    if (estadoGrid.filter.filters.length === 0 && !this.fechaFiltro) {
      this.mostrarTodoBotones = false;
    } else {
      this.mostrarTodoBotones = true;
    }
    this.estadoGrid = estadoGrid;
    this.datoGrid = process(this.responseMulticanal, this.estadoGrid);
    console.log(
      "nuevos datos",
      process(this.responseMulticanal, this.estadoGrid)
    );

    console.log("nuevos datos del grid", this.datoGrid);
    let estadoEditado = { ...estadoGrid };
    estadoEditado.skip = 0;
    estadoEditado.take = this.datoGrid.total;

    console.log(
      "nuevos datos pero ya con la maldad xd!",
      process(this.responseMulticanal, estadoEditado)
    );
    this.busquedaDinamicaGrid = process(this.responseMulticanal, estadoEditado);
    this.dataDescargarCsv = process(this.responseMulticanal, estadoEditado);
    this.dataDescargarCsv = this.dataDescargarCsv.data;
  }

  EventoEstadoGridTarea(estadoGrid: any) {
    // debugger;

    console.log("estados del grid Tarea", estadoGrid);

    this.estadoGridTarea = estadoGrid;
    this.datoGridTarea = process(this.responseTarea, this.estadoGridTarea);
  }

  descargar(): void {
    let descargar = [];
    if (this.dataDescargarCsv) {
      var headers = {
        asesor: "Asesor",
        nocuenta: "N# Cuenta",
        nombrecompleto: "Nombre Completo",
        identificacion: "Cedula",
        ultimagestion: "Ultima Gestion",
        gestion: "Gestion",
        diasmora: "Dias Mora",
        mejorgestion: "Mejor Gestion",
        deudatotal: "Deuda Total",
        statuscartera: "Estado Cartera",
        fechaacuerdo: "Fecha Acuerdo",
        valoracuerdo: "Valor Acuerdo",
        valorpago: "Valor Pago",
        fechaPago: "Fecha Pago",
        totalacuerdo: "Total Acuerdo",
        totalpago: "Total Pago",
        cuota: "Cuota",
        totalcuotas: "Total Cuotas",
        deudavencida: "Deuda Vencida",
        fechamejorgestion: "Fecha Mejor Gestion",
      };
      for (const iterator of this.dataDescargarCsv) {
        // if (iterator.asesor === "") {
        //   iterator.asesor = null;
        // }
        const dato = {
          asesor: this.revisar(iterator.asesor),
          nocuenta: this.revisar(iterator.nocuenta),
          nombrecompleto: this.revisar(iterator.nombrecompleto),
          identificacion: this.revisar(iterator.identificacion),
          ultimagestion: this.revisar(iterator.ultimagestion),
          gestion: this.revisar(iterator.gestion),
          diasmora: this.revisar(iterator.diasmora),
          mejorgestion: this.revisar(iterator.mejorgestion),
          deudatotal: this.revisar(iterator.deudatotal),
          statuscartera: this.revisar(iterator.statuscartera),
          fechaacuerdo: this.revisar(iterator.fechaacuerdo),
          valoracuerdo: this.revisar(iterator.valoracuerdo),
          valorpago: this.revisar(iterator.iterator),
          fechaPago: this.revisar(iterator.fechaPago),
          totalacuerdo: this.revisar(iterator.totalacuerdo),
          totalpago: this.revisar(iterator.totalpago),
          cuota: this.revisar(iterator.cuota),
          totalcuotas: this.revisar(iterator.totalcuotas),
          deudavencida: this.revisar(iterator.deudavencida),
          fechamejorgestion: this.revisar(iterator.fechamejorgestion),

          // idUsuario: 1,
        };
        descargar.push(dato);
      }
      console.log("datos a descargar", descargar);
      var fileTitle = new Date(); // or 'my-unique-title'

      exportCSVFile(headers, descargar, fileTitle);
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "No existen datos para descargar!",
        icon: "error",
      });
    }
    // const itemsNotFormatted = [
    //   {
    //     model: "Samsung S7",
    //     chargers: "55",
    //     cases: "56",
    //     earphones: "57",
    //     scratched: "2",
    //   },
    //   {
    //     model: "Pixel XL",
    //     chargers: "77",
    //     cases: "78",
    //     earphones: "79",
    //     scratched: "4",
    //   },
    //   {
    //     model: "iPhone 7",
    //     chargers: "88",
    //     cases: "89",
    //     earphones: "90",
    //     scratched: "6",
    //   },
    // ];

    // var itemsFormatted = [];

    // format the data
    // itemsNotFormatted.forEach((item) => {
    //   itemsFormatted.push({
    //     model: item.model.replace(/,/g, ""), // remove commas to avoid errors,
    //     chargers: item.chargers,
    //     cases: item.cases,
    //     earphones: item.earphones,
    //   });
    // });
  }
  revisar(data: any): any {
    if (data === "") {
      return null;
    } else {
      return data;
    }
  }
  abrirModales(texto: string, filtro: any): void {
    console.log("aliminar filtro", filtro);
    switch (texto) {
      case "Marcadores":
        this.openMarcador();
        break;
      case "Eliminar":
        this.eliminarFiltro(filtro.idmulticanal);
        break;
      case "Tarea":
        this.openTarea(filtro);
        break;
      case "Sms":
        this.openSms();
        break;
      case "Email":
        this.openEmail();
        break;

      default:
        break;
    }
  }

  limpiarFiltros(): void {
    this.obtenerMulticanal(this.campaniaSeleccionada);
  }

  eliminarFiltro(idmulticanal: number): void {
    Swal.fire({
      title: "Esta seguro?",
      text: "No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminalo!",
    }).then((result) => {
      if (result.isConfirmed) {
        this._multicanalService
          .eliminarFiltro(idmulticanal)
          .subscribe((respuesta) => {
            if (respuesta.exito === 1) {
              this.obtenerFiltro();
              Swal.fire(
                "Eliminado!",
                "El filtro fue Eliminado correctamente.",
                "success"
              );
            } else {
              Swal.fire({
                allowOutsideClick: false,
                text: "error al eliminar el filtro!",
                icon: "error",
              });
            }
          });
      }
    });
  }

  cambiosModal(): void {
    this.modalService.onHidden.pipe(takeUntil(this.stop$)).subscribe((data) => {
      if (data === "filtro") {
        this.obtenerFiltro();
      } else if (data === "marcador") {
        this.obtenerMarcadores();
      } else if (data === "tarea") {
        this.obtenerTareas();
      }
    });
  }
  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
}
