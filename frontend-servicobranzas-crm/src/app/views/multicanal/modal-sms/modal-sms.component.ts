import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { DinomiService } from "../../../services/dinomi.service";
import { MulticanalService } from "../../../services/multicanal.service";

@Component({
  selector: "app-modal-sms",
  templateUrl: "./modal-sms.component.html",
  styleUrls: ["./modal-sms.component.css"],
})
export class ModalSmsComponent implements OnInit {
  tarea: any;
  filtro: any;
  detalle: any;
  campania: any;
  sms: string =
    "Estimado(a): ${nombre} le recordamos que mantiene una deuda con ${campania} por: ${deuda} por favor acerquese a la brevedad";
  // idmulticanal: number;
  smsForm: FormGroup;
  // tipo = [{ nombre: "Si" }, { nombre: "No" }];
  constructor(
    public modalRef: BsModalRef,
    private fb: FormBuilder,
    private readonly _dinomiService: DinomiService,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.formSms();
    console.log("detalle enviado al modal", this.detalle);
    console.log("campania enviado al modal", this.campania);
  }

  private formSms(): void {
    this.smsForm = this.fb.group({
      FechaInicio: [null, Validators.required],
      FechaFin: [null, Validators.required],

      // Idmulticanal: [null],
      // IdCampania: 17,
      texto: [this.sms, Validators.required],

      // codigo:[''],
    });
  }

  public guardarDinamico(event: any): void {
    console.log("tarea enviada", this.tarea);
    if (!this.tarea) {
      this.submitForm(event);
    } else {
      // this.submitFormEditar(event);
    }
  }

  // private formEditar(): void {
  //   console.log("tarea enviada al form", this.tarea);
  //   this.smsForm = this.fb.group({
  //     Descripcion: [this.tarea.descripcion, Validators.required],
  //     Estado: [this.tarea.estado, Validators.required],
  //     FechaInicio: [new Date(this.tarea.fechaInicio), Validators.required],
  //     FechaFin: [new Date(this.tarea.fechaFin), Validators.required],
  //     // Idmulticanal: [null],
  //     // IdCampania: 17,
  //     Obligatorio: [this.tarea.obligatorio, Validators.required],

  //     // codigo:[''],
  //   });
  // }

  public submitForm(event: any): void {
    const data = [];
    // console.log("datos del form tarea", this.idcampania);
    this.smsForm.markAllAsTouched();
    if (this.smsForm.valid) {
      const formulario = this.smsForm.value;

      console.log("formulario a enviar", formulario);
      for (const cliente of this.detalle) {
        let texto = formulario.texto.replace(
          "${nombre}",
          cliente.nombrecompleto
        );
        texto = texto.replace("${deuda}", cliente.deudavencida);
        texto = texto.replace("${campania}", this.campania.nombre);

        for (const telefono of cliente.telefonos) {
          const clienteTelefono = {
            telefono,
            sms: texto,
          };
          data.push(clienteTelefono);
        }
      }
      const enviar = {
        nombre: this.campania.nombre,
        fecha_inicio: formulario.FechaInicio,
        fecha_fin: formulario.FechaFin,
        hora_inicio: formulario.FechaInicio,
        hora_fin: formulario.FechaFin,
        data,
      };
      console.log("datos listos para enviar", enviar);
      this._dinomiService.enviarSmsMasivo(enviar).subscribe((respuesta) => {
        console.log("respuesta de envio masivo", respuesta);
        this.modalService.setDismissReason("sms");
        this.modalRef.hide();
      });

      // console.log("datos del form tarea", tarea);
      // this._multicanalService.crearTarea(tarea).subscribe(
      //   (respuesta) => {
      //     console.log("respuesta de guardar tarea", respuesta);
      //     if (respuesta.exito === 0) {
      //       Swal.close();
      //       Swal.fire({
      //         allowOutsideClick: false,
      //         text: respuesta.mensage,
      //         icon: "error",
      //       });
      //     } else {
      //       this.modalService.setDismissReason("tarea");
      //       this.modalRef.hide();
      //       Swal.fire({
      //         position: "top-end",
      //         icon: "success",
      //         title: "tarea creada correctamente",
      //         showConfirmButton: false,
      //         timer: 1500,
      //       });
      //       this.smsForm.reset();
      //     }
      //   },
      //   (err) =>
      //     Swal.fire({
      //       icon: "error",
      //       title: "Oops...",
      //       text: err.message,
      //     })
      // );
    }
  }

  // public submitFormEditar(event: any): void {
  //   this.smsForm.markAllAsTouched();
  //   if (this.smsForm.valid) {
  //     const tarea: any = this.smsForm.value;
  //     tarea.Id = this.tarea.id;
  //     console.log("datos del form tarea editar", tarea);
  //     this._multicanalService.editarTarea(tarea).subscribe(
  //       (respuesta) => {
  //         console.log("respuesta de editar tarea", respuesta);
  //         if (respuesta.exito === 0) {
  //           Swal.close();
  //           Swal.fire({
  //             allowOutsideClick: false,
  //             text: respuesta.mensage,
  //             icon: "error",
  //           });
  //         } else {
  //           this.modalService.setDismissReason("tarea");
  //           this.modalRef.hide();
  //           Swal.fire({
  //             position: "top-end",
  //             icon: "success",
  //             title: "tarea editada correctamente",
  //             showConfirmButton: false,
  //             timer: 1500,
  //           });

  //           this.smsForm.reset();
  //         }
  //       },
  //       (err) =>
  //         Swal.fire({
  //           icon: "error",
  //           title: "Oops...",
  //           text: err.message,
  //         })
  //     );
  //   }
  // }
}
