import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { PlantillaService } from "../../../services/plantilla.service";
import { LeerPlantillaModel } from "../models/leer-plantilla.model";

@Component({
  selector: "app-contenedor-plantilla",
  templateUrl: "./contenedor-plantilla.component.html",
  styleUrls: ["./contenedor-plantilla.component.css"],
})
export class ContenedorPlantillaComponent implements OnInit {
  public index: number = 0;
  public plantilla: LeerPlantillaModel[];
  public titulo: string;

  constructor(
    private readonly _plantillaService: PlantillaService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.obtenerPlantillasTipo(this.index);
  }
  cambioTab(evento: any): void {
    console.log("evento", evento.index);
    this.obtenerPlantillasTipo(evento.index);
  }

  obtenerPlantillasTipo(index: number): void {
    this.spinner.show();
    const tipo: string = this.tipoSeleccion(index);
    this._plantillaService
      .ConsultarPlantillasTipo(tipo)
      .subscribe((respuesta) => {
        this.spinner.hide();
        console.log("respuesa de la plantilla por tipo", tipo);
        this.plantilla = respuesta.data;
        this.titulo = tipo;
      });
  }
  tipoSeleccion(index: number): string {
    if (index === 0) {
      return "Sms";
    } else {
      return "Email";
    }
  }
}
