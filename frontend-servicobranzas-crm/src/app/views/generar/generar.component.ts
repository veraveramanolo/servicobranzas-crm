import { Component, OnInit } from '@angular/core';
import { Day } from '@progress/kendo-date-math';

@Component({
  selector: 'app-generar',
  templateUrl: './generar.component.html',
  styleUrls: ['./generar.component.css']
})
export class GenerarComponent implements OnInit {

  /*constructor() { }*/

  ngOnInit(): void {
  }

  public focusedDate: Date = new Date();
    /*public disabledDates = (date: Date): boolean => {
      return date.getDate() % 2 === 0;
    }*/

  public disabledDates: Day[] = [Day.Saturday, Day.Sunday];

}
