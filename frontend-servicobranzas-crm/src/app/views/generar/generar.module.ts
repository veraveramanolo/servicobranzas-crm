import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntlModule } from '@progress/kendo-angular-intl';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { GenerarComponent } from './generar.component';
import { GenerarRoutingModule } from './generar-routing.module';

@NgModule({
  declarations: [GenerarComponent],
  imports: [
    CommonModule,
    IntlModule,
    DateInputsModule,
    LabelModule,
    GenerarRoutingModule
  ]
})
export class GenerarModule { }
