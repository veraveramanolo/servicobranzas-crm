import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductividadComponent } from './productividad/productividad.component';
import { EfectividadComponent } from './efectividad/efectividad.component';


const routes: Routes = [
  {
    path: '',
    data: {title: 'Tablero de Control'},
    children: [
      {
        path: '',
        redirectTo: 'tablero-control'
      },
      {
        path: 'tablero-control/productividad',
        component: ProductividadComponent,
        data: {title: 'Productividad'}
      },
      {
        path: 'tablero-control/efectividad',
        component: EfectividadComponent,
        data: {title: 'Efectividad'}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TableroControlRoutingModule { }
