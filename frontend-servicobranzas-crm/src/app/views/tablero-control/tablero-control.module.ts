import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableroControlRoutingModule } from './tablero-control-routing.module';
import { ProductividadComponent } from './productividad/productividad.component';
import { EfectividadComponent } from './efectividad/efectividad.component';


@NgModule({
  declarations: [ProductividadComponent, EfectividadComponent],
  imports: [
    CommonModule,
    TableroControlRoutingModule
  ]
})
export class TableroControlModule { }
