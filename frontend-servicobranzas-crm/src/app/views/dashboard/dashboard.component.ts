import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { CrmCartera } from '../../models/CrmCartera';
import { AclUsuario } from '../../models/AclUsuario';
import { VariableGlobalService } from '../../services/variable-global.service';
import Swal from 'sweetalert2';
import { ModeloRespuesta } from '../../models/ModeloRespuesta.models';
import { isNullOrUndefined } from 'util';
import { CrmCarteraService } from '../../services/crm-cartera.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CrmCampania } from '../../models/CrmCampania';
import { CampaniaService } from '../../services/campania.service';
import { Router } from '@angular/router';

const codigoIng = 'ING';
const codigoMod = 'MOD';
const listaSiNo = [{ 'Codigo': 'A', 'Descripcion': 'Activo' }, { 'Codigo': 'I', 'Descripcion': 'Inactivo' }];
const tipoCartera = [{'Codigo': 'Cedente'}, {'Codigo': 'Vendida'}];


@Component({
  templateUrl: 'dashboard.component.html',
})


export class DashboardComponent implements OnInit {

  public tipo = tipoCartera;
  public datoGrid: GridDataResult;
  public datoGridCampania: GridDataResult;
  public estadoGrid: State;
  public estadoGridCampania: State;
  public infoCrmCartera: CrmCartera[];
  public infoCrmCampania: CrmCampania[];
  private editedRowIndex: number;
  public formulario: FormGroup;
  public creacionEdicion: boolean;
  public cargandoGrid: boolean;
  public cargandoGridCampania: boolean;
  public datoAnulado = listaSiNo;
  public codigoProceso: string;
  public usuarioLogin: AclUsuario;
  public carteraSeleccionada: CrmCartera;

  constructor(public global: VariableGlobalService, 
    public serviceCrmCartera: CrmCarteraService,
    public serviceCrmCampania: CampaniaService,
    private router: Router) {
    
  }

  ngOnInit(): void {
    this.estadoGrid = { skip: 0, take: 8, filter: { logic: 'and', filters: [] }};
    this.estadoGridCampania = { skip: 0, take: 8, filter: { logic: 'and', filters: [] }};
    this.ConsultarUsuarioLogin();
    this.ConsultarAllCarteras();
  }


  ConsultarUsuarioLogin(){
    this.usuarioLogin =  this.global.ConsultarUsuarioLogin();
  }


  async ConsultarAllCarteras() {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.serviceCrmCartera.All();
    this.infoCrmCartera = respuesta.data;
    this.infoCrmCartera.forEach(cartera => {
      cartera.estado = cartera.estado === 'A' ? 'Activo' : 'Inactivo'
    });
    this.datoGrid = process(this.infoCrmCartera, this.estadoGrid);
    this.cargandoGrid = false;

    console.log(this.infoCrmCartera);
  }

  async ConsultarCarteraCampania(idCartera: number){
    this.cargandoGridCampania = true;
    this.datoGridCampania = undefined;


    let resp: ModeloRespuesta = await this.serviceCrmCartera.CarteraCampania(idCartera);
    this.infoCrmCampania = resp.data;
    this.infoCrmCampania.forEach(campania => {
      campania.estado = campania.estado === 'A' ? 'Activo' : 'Inactivo'
    });
    this.datoGridCampania = process(this.infoCrmCampania, this.estadoGridCampania);
    this.cargandoGridCampania = false;

    console.log(this.infoCrmCampania);
    
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoCrmCartera, this.estadoGrid);
  }

  EventoEstadoGridCampania(estadoGrid: DataStateChangeEvent): void {
    this.estadoGridCampania = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGridCampania = process(this.infoCrmCampania, this.estadoGridCampania);
  }

  
  AgregarRegistroGrid({sender}){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.minLength(3)]),
      estado: new FormControl('A', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]),
      tipo: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]),
    });

    sender.addRow(this.formulario);

  }

  AgregarRegistroGridCampania({sender}){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.minLength(3)]),
      estado: new FormControl('A', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]),
      idCartera: new FormControl(this.carteraSeleccionada.id)
    });

    sender.addRow(this.formulario);

  }

  CancelarEdicionFila({ sender, rowIndex }) {
    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
  }

  CerrarFilaGrid(grid, rowIndex = this.editedRowIndex){
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formulario = undefined;
  }

  EditarFilaGrid({ sender, rowIndex, dataItem }){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoMod;
    
    this.formulario = new FormGroup({
      nombre: new FormControl(dataItem.nombre, [Validators.required, Validators.minLength(3)]),
      estado: new FormControl(dataItem.estado === 'Activo' ? 'A' : 'I', [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      id : new FormControl(dataItem.id),
      tipo: new FormControl(dataItem.tipo, [Validators.required, Validators.pattern('^[a-zA-Z]+$')]),

    });

    this.formulario.controls['id'].disable();

    this.creacionEdicion = true;
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formulario);
  }

  EditarFilaGridCampania({ sender, rowIndex, dataItem }){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoMod;
    
    this.formulario = new FormGroup({
      nombre: new FormControl(dataItem.nombre, [Validators.required, Validators.minLength(3)]),
      estado: new FormControl(dataItem.estado === 'Activo' ? 'A' : 'I', [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      id : new FormControl(dataItem.id),
      idCartera: new FormControl(dataItem.idCartera)
    });

    this.formulario.controls['id'].disable();

    this.creacionEdicion = true;
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formulario);
  }


  async GuardarFilaGrid({ sender, rowIndex, formGroup: FormGroup, isNew }){
    let infoCartera: CrmCartera = await this.ObtenerInfoCartera(this.formulario);

    if(this.formulario.invalid){
      return Swal.fire('Información Invalida', 'Debe haber un error el la información ingresada', 'error');
    }

    if(this.InfoDuplicada(this.infoCrmCartera  ,infoCartera)){
      return Swal.fire('Informacióm duplicada', 'Ya existe un registro con esos detalles. Favor modifique e intente nuevamente', 'error');
    }

    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
      icon: 'info'
    },
    );
    Swal.showLoading();

    let resp: ModeloRespuesta = undefined;
    if(this.codigoProceso === codigoIng)
      resp = await this.serviceCrmCartera.Crear(infoCartera);

    if(this.codigoProceso === codigoMod)
      resp = await this.serviceCrmCartera.Modificar(infoCartera);

    
    if (resp.exito === 0) {
      return Swal.fire('Error al grabar', 'Se produjo un error en el sistema intente nuevamente', 'error');
      Swal.close();
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire('Datos Grabados', 'La información se grabo con exito', 'success');
    return await this.ConsultarAllCarteras();
    Swal.close();
  }

  async GuardarFilaGridCampania({ sender, rowIndex, formGroup: FormGroup, isNew }){
    let infoCarteraCampania: CrmCampania = await this.ObtenerInfoCampania(this.formulario);

    if(this.formulario.invalid){
      return Swal.fire('Información Invalida', 'Debe haber un error el la información ingresada', 'error');
    }

    if(this.InfoDuplicadaCampania(this.infoCrmCampania  ,infoCarteraCampania)){
      return Swal.fire('Informacióm duplicada', 'Ya existe un registro con esos detalles. Favor modifique e intente nuevamente', 'error');
    }
    
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
      icon: 'info'
    },
    );
    Swal.showLoading();

    let resp: ModeloRespuesta = undefined;
    if(this.codigoProceso === codigoIng)
      resp = await this.serviceCrmCampania.Crear(infoCarteraCampania);

    if(this.codigoProceso === codigoMod)
      resp = await this.serviceCrmCampania.Modificar(infoCarteraCampania);
    
    if (resp.exito === 0) {
      return Swal.fire('Error al grabar', 'Se produjo un error en el sistema intente nuevamente', 'error');
      Swal.close();
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire('Datos Grabados', 'La información se grabo con exito', 'success');
    return await this.ConsultarCarteraCampania(this.carteraSeleccionada.id);
    Swal.close();

  }


  
  ObtenerInfoCartera(formulario: FormGroup): CrmCartera {
    debugger
    return {
     crmCampania: undefined,
     crmCuentas: undefined,
     estado: formulario.controls['estado'].value,
     id: this.codigoProceso === codigoMod ? formulario.controls['id'].value : undefined,
     nombre: formulario.controls['nombre'].value,
     tipo: formulario.controls['tipo'].value
   };
 }

 ObtenerInfoCampania(formulario: FormGroup): CrmCampania {
   return {
     estado: formulario.controls['estado'].value,
     crmCuentas : undefined,
     crmCuentasCampania: undefined,
     id: this.codigoProceso === codigoMod ? formulario.controls['id'].value : undefined,
     idCartera: formulario.controls['idCartera'].value,
     idCarteraNavigation : undefined,
     nombre:  formulario.controls['nombre'].value,
   }
 }


  InfoDuplicada(ListinfoCrmCartera: CrmCartera[], crmCartera: CrmCartera): boolean {
    let infoDuplicada : CrmCartera;
    let infoFiltrada: CrmCartera[] = ListinfoCrmCartera.filter(x => x.id !== crmCartera.id);
    infoDuplicada = infoFiltrada.find(x => x.nombre.trim().toLocaleUpperCase() === crmCartera.nombre.trim().toLocaleUpperCase()
    && x.tipo.trim().toLocaleUpperCase() === crmCartera.tipo.trim().toLocaleUpperCase());

    if (isNullOrUndefined(infoDuplicada)) {
      return false
    } else {
      return true
    }
  }

  InfoDuplicadaCampania(ListinfoCrmCampania: CrmCampania[], crmCampania: CrmCampania): boolean {
    let infoDuplicada : CrmCampania;
    let infoFiltrada: CrmCampania[] = ListinfoCrmCampania.filter(x => x.id !== crmCampania.id);
    infoDuplicada = infoFiltrada.find(x => x.nombre.trim().toLocaleUpperCase() === crmCampania.nombre.trim().toLocaleUpperCase());

    if (isNullOrUndefined(infoDuplicada)) {
      return false
    } else {
      return true
    }
  }

  async cellClickHandler ({ dataItem, rowIndex }) {
    this.carteraSeleccionada = dataItem
    await this.ConsultarCarteraCampania(this.carteraSeleccionada.id)
  }

  cellClickHandlerCampania({ dataItem, rowIndex }){
    return;
    /*this.router.navigate([
      `/gestionar/gestion-normal/${dataItem.id}`,
    ]);*/
  }
}
