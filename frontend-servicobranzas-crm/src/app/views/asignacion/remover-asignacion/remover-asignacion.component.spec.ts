import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoverAsignacionComponent } from './remover-asignacion.component';

describe('RemoverAsignacionComponent', () => {
  let component: RemoverAsignacionComponent;
  let fixture: ComponentFixture<RemoverAsignacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoverAsignacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoverAsignacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
