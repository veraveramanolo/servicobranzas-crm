import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "estado",
})
export class EstadoPipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): unknown {
    if (value === "I") {
      return "Inactivo";
    } else {
      return "Activo";
    }
  }
}
