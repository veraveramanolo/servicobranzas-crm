import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FilterService, GridDataResult } from "@progress/kendo-angular-grid";
import {
  State,
  process,
  CompositeFilterDescriptor,
  FilterDescriptor,
} from "@progress/kendo-data-query";
import { BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { AclUsuario } from "../../../../../models/AclUsuario";
import { CrmCampania } from "../../../../../models/CrmCampania";
import { CrmCartera } from "../../../../../models/CrmCartera";
import { ModeloRespuesta } from "../../../../../models/ModeloRespuesta.models";
import { AsignacionService } from "../../../../../services/asignacion.service";
import { CampaniaService } from "../../../../../services/campania.service";
import { CrmCarteraService } from "../../../../../services/crm-cartera.service";
import { MulticanalService } from "../../../../../services/multicanal.service";
import { VariableGlobalService } from "../../../../../services/variable-global.service";
import { ModalAsignarComponent } from "../modal-asignar/modal-asignar.component";
import { ModalCiclosComponent } from "../modal-ciclos/modal-ciclos.component";
import { exportCSVFile } from "./export-csv";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

const flatten = (filter) => {
  const filters = (filter || {}).filters;
  if (filters) {
    return filters.reduce(
      (acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]),
      []
    );
  }
  return [];
};

const distinctNombre = (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x, idx, xs) => {
      // console.log(x, idx, xs);
      return xs.findIndex((y) => y === x) === idx;
    });

const distinctExtra = (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x: string, idx, xs) => {
      let limpiar;
      if (x != null) {
        limpiar = x.trim();
      } else {
        limpiar = x;
      }

      // console.log(x, idx, xs);
      return (
        xs.findIndex(
          (y) => y === limpiar && limpiar != null && limpiar != ""
        ) === idx
      );
    });

@Component({
  selector: "app-botones-superiores",
  templateUrl: "./botones-superiores.component.html",
  styleUrls: ["./botones-superiores.component.css"],
})
export class BotonesSuperioresComponent implements OnInit {
  private stop$ = new Subject<void>();
  campaniaSeleccionada: CrmCampania;
  public listaCartera: CrmCartera[] = [];
  public InfoCartera: CrmCartera[] = [];
  public listaCampania: CrmCampania[] = [];
  public listaCampaniafiltrada: CrmCampania[] = [];
  public listaCampaniaEmergente: CrmCampania[] = [];
  public valorComboCampania: CrmCampania;
  public datoGrid: GridDataResult;
  public fechaFiltro = null;
  public estadoGrid: State;
  public cargandoGrid: boolean;
  public responseMulticanal: any;
  public dataDescargarCsv: any;
  public asesores = [];
  public todosLosFiltros = [];
  public mySelection: any[] = [];
  public usuarioLogin: AclUsuario;
  public InfoCampania: CrmCampania[] = [];
  public busquedaDinamicaGrid: GridDataResult;
  private estadoFilter: any[] = [];
  public idCampania: number;
  public nombres = [];
  public nocuentas = [];
  public cedulas = [];
  public extra1 = [];
  public extra2 = [];
  public extra3 = [];
  public extra4 = [];
  public extra5 = [];
  public extra6 = [];
  public extra7 = [];
  public extra8 = [];
  public extra9 = [];
  public extra10 = [];
  @Output() campania: EventEmitter<number> = new EventEmitter();
  @Output() clientes: EventEmitter<any[]> = new EventEmitter();
  constructor(
    private readonly _multicanalService: MulticanalService,
    private global: VariableGlobalService,
    private serviceCrmCartera: CrmCarteraService,
    private serviceCarmpania: CampaniaService,
    private readonly modalService: BsModalService,
    private readonly _asignacionService: AsignacionService,
    private spinner: NgxSpinnerService
  ) {
    this.ConsultarUsuarioLogin();
  }

  ngOnDestroy(): void {
    this.stop();
  }
  
  async ngOnInit() {
    this.cambiosModal();
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };

    // await this.GetCarteraGestor(this.usuarioLogin.idUsuario);
    // await this.GetCampaniaGestor(this.usuarioLogin.idUsuario);
    await this.GetCarteras(); //ITA
    await this.GetConsultaCampania(); //ITA
  }
  // async GetCarteraGestor(idUsuario: number) {
  //   this.listaCartera = [];
  //   this.InfoCartera = [];

  //   let resp: ModeloRespuesta = null;
  //   resp = await this.serviceCrmCartera.GetCarteraGestor(idUsuario);

  //   this.InfoCartera = resp.data;
  //   this.listaCartera = this.InfoCartera;
  //   console.log("listaCartera",this.listaCartera);
  // }
  //ITA
  async GetCarteras() {
    this.listaCartera = [];
    this.InfoCartera = [];

    let resp: ModeloRespuesta = null;
    resp = await this.serviceCrmCartera.GetCarteras();

    this.InfoCartera = resp.data;
    this.listaCartera = this.InfoCartera;
    console.log("listaCartera",this.listaCartera);
  }

  onSelectedKeysChange(event) {
    this.clientes.emit(this.mySelection);
    console.log("cambios tabla principal evento", event);
    console.log(
      "cambios tabla principal evento mi seleccion",
      this.mySelection
    );
  }
  public selectedCallback = (args) => {
    return args.dataItem;
  };

  // async GetCampaniaGestor(idUsuario: number) {
  //   this.listaCampania = [];
  //   this.InfoCampania = [];

  //   let resp: ModeloRespuesta = undefined;
  //   resp = await this.serviceCarmpania.GetCampaniaGestor(idUsuario);

  //   this.InfoCampania = resp.data;
  //   this.listaCampania = this.InfoCampania;
  //   console.log("lista campania", this.listaCampania);
  // }
  //ITA
  async GetConsultaCampania() {
    this.listaCampania = [];
    this.InfoCampania = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetConsultaCampania();

    this.InfoCampania = resp.data;
    this.listaCampania = this.InfoCampania;
    console.log("lista campania", this.listaCampania);
  }
  public estadoFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.estadoFilter.splice(
      0,
      this.estadoFilter.length,
      ...flatten(filter).map(({ value }) => value)
    );
    console.log("estadoFilter", this.estadoFilter);
    return this.estadoFilter;
  }

  ConsultarUsuarioLogin() {
    this.usuarioLogin = this.global.ConsultarUsuarioLogin();
  }

  FiltroComboCartera(valor) {
    this.listaCartera = this.InfoCartera.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }
  FiltroComboCampania(valor) {
    this.listaCampaniafiltrada = this.listaCampaniaEmergente.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }
  EventoComboCartera(valor: CrmCartera) {
    this.datoGrid = null;
    this.valorComboCampania = null;
    if (valor) {
      this.listaCampaniafiltrada = [];
      this.listaCampaniafiltrada = this.listaCampania.filter(
        (x) => x.idCartera === valor.id
      );
      this.listaCampaniaEmergente = this.listaCampaniafiltrada;
    } else {
      this.listaCampaniafiltrada = [];
    }

    // this.global.valorDropGestionNormal.cartera = valor;
    // this.GuardarValorDrop(this.global.valorDropGestionNormal);
  }

  obtenerMulticanal(valor: CrmCampania): void {
    this.mySelection = [];
    this.fechaFiltro = null;

    if (valor) {
      this.idCampania = valor.id;
      this.campania.emit(valor.id);
      this.estadoGrid = {
        skip: 0,
        take: 8,
        filter: { logic: "and", filters: [] },
      };
      this.cargandoGrid = true;
      this._multicanalService
        .obtenerMulticanal(valor.id)
        .subscribe((response) => {
          this.obtenerListaEstados(valor.id, this.fechaFiltro);
          this.cargandoGrid = false;
          this.campaniaSeleccionada = valor;
          console.log("respuesta de multicanal", response);
          this.responseMulticanal = response.data;
          this.nombres = distinctNombre(response.data, "nombrecompleto");
          this.nocuentas = distinctNombre(response.data, "nocuenta");
          this.cedulas = distinctNombre(response.data, "identificacion");
          this.extra1 = distinctExtra(response.data, "extra1");
          this.extra2 = distinctExtra(response.data, "extra2");
          this.extra3 = distinctExtra(response.data, "extra3");
          this.extra4 = distinctExtra(response.data, "extra4");
          this.extra5 = distinctExtra(response.data, "extra5");
          this.extra6 = distinctExtra(response.data, "extra6");
          this.extra7 = distinctExtra(response.data, "extra7");
          this.extra8 = distinctExtra(response.data, "extra8");
          this.extra9 = distinctExtra(response.data, "extra9");
          this.extra10 = distinctExtra(response.data, "extra10");

          console.log(
            "filtrados",
            distinctNombre(response.data, "nombrecompleto")
          );

          this.dataDescargarCsv = response.data;
          this.datoGrid = process(this.responseMulticanal, this.estadoGrid);
        });
    } else {
      this.cargandoGrid = false;
      this.campaniaSeleccionada = null;
      this.responseMulticanal = null;
      this.dataDescargarCsv = null;
      this.datoGrid = null;
      // this.filtros = null;
      // this.filtrados = null;
      this.estadoGrid = {
        skip: 0,
        take: 8,
        filter: { logic: "and", filters: [] },
      };
      // this.mostrarTodoBotones = false;
    }
  }
  public estadoChange(
    values: any[],
    filterService: FilterService,
    tipo: string
  ): void {
    console.log("estadoChange", values);
    filterService.filter({
      filters: values.map((value) => ({
        field: tipo,
        operator: "eq",
        value,
      })),
      logic: "or",
    });
  }
  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log(estadoGrid);
    console.log("estados del grid", estadoGrid);
    // if (estadoGrid.filter.filters.length === 0 && !this.fechaFiltro) {
    //   this.mostrarTodoBotones = false;
    // } else {
    //   this.mostrarTodoBotones = true;
    // }
    this.estadoGrid = estadoGrid;
    this.datoGrid = process(this.responseMulticanal, this.estadoGrid);
    console.log(
      "nuevos datos",
      process(this.responseMulticanal, this.estadoGrid)
    );

    console.log("nuevos datos del grid", this.datoGrid);
    let estadoEditado = { ...estadoGrid };
    estadoEditado.skip = 0;
    estadoEditado.take = this.datoGrid.total;

    console.log(
      "nuevos datos pero ya con la maldad xd!",
      process(this.responseMulticanal, estadoEditado)
    );
    this.busquedaDinamicaGrid = process(this.responseMulticanal, estadoEditado);
    this.dataDescargarCsv = process(this.responseMulticanal, estadoEditado);
    this.dataDescargarCsv = this.dataDescargarCsv.data;
  }

  obtenerListaEstados(idcampania: number, fecha: any): void {
    this._multicanalService
      .obtenerMultiselect(idcampania, fecha)
      .subscribe((data) => {
        console.log("listado estados", data);
        // this.estadoListaCantidad = data.data;
        this.asesores = data.asesores;
        this.todosLosFiltros = data.data;
        // this.todosLosFiltros[1] = {
        //   gestion: "SIN GESTION",
        //   mejorgestion: "SIN GESTION",
        // };

        // this.todosLosFiltros.splice(1, 1);

        // this.estadofiltro = [...this.asesores];
        // this.estadofiltro.pop();
        // console.log("Last: ", this.estadofiltro);
      });
  }
  open(): void {
    let enviarClientesModal = [];
    if (this.mySelection.length > 0) {
      enviarClientesModal = this.mySelection;
    } else {
      enviarClientesModal = this.responseMulticanal;
    }
    console.log("identificador de campaña", this.idCampania);
    if (this.idCampania) {
      this.modalService.show(ModalAsignarComponent, {
        initialState: {
          // detalle: this.dataDescargarCsv,
          idCampania: this.idCampania,
          totalClientes: enviarClientesModal,
          // filtro: this.estadoGrid,
          // idCampania: Number(this.params.GestionID),
          // arbolSelecciondo: this.arbolSelecciondo,
          // idProducto: Number(this.params.ProductoID),
        },
        backdrop: "static",
        class: "gray modal-lg",
      });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "Debe seleccionar una campaña!",
        icon: "error",
      });
    }
    // this.params = this.activatedRoute.snapshot.params;
  }

  openCiclos(): void {
    this.modalService.show(ModalCiclosComponent, {
      initialState: {
        // detalle: this.dataDescargarCsv,
        // filtro: this.estadoGrid,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      class: "gray modal-lg",
    });

    // this.params = this.activatedRoute.snapshot.params;
  }

  eliminarAsignacion(): void {
    if (this.mySelection.length > 0) {
      this.spinner.show();
      let clientes = [];
      for (const cliente of this.mySelection) {
        const add = {
          nocuenta: cliente.nocuenta,
          // valorpago: cliente.valorpago,
          // deudatotal: cliente.deudatotal,
          // deudavencida: cliente.deudavencida,
        };
        clientes.push(add);
      }
      const datos = {
        asesores: [],
        clientes,
        idcampania: this.idCampania,
      };
      console.log(datos);
      Swal.fire({
        title: "Estas Seguro?",
        text: "No podras revertirlo!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Borralo!",
      }).then((result) => {
        if (result.isConfirmed) {
          this._asignacionService
            .eliminarAsignacion(datos)
            .subscribe((respuesta) => {
              console.log("respuesta de eliminar asignacion", respuesta);
              this.spinner.hide();
              if (respuesta.exito === 1) {
                console.log("campania ",this.campaniaSeleccionada);
                this.obtenerMulticanal(this.campaniaSeleccionada);
                Swal.fire({
                  title: "Correcto!",
                  html: "Asignacion eliminada!",
                  timer: 3000,
                  timerProgressBar: true,
                  showConfirmButton: false,
                  icon: "success",
                });
              } else {
                Swal.fire({
                  allowOutsideClick: false,
                  text: respuesta.mensage,
                  icon: "error",
                });
              }
            });
        }
      });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "Seleccione clientes!",
        icon: "error",
      });
    }
  }

  descargar(): void {
    let descargar = [];
    if (this.dataDescargarCsv) {
      var headers = {
        asesor: "Asesor",
        nocuenta: "N# Cuenta",
        nombrecompleto: "Nombre Completo",
        identificacion: "Cedula",
        ultimagestion: "Ultima Gestion",
        gestion: "Gestion",
        diasmora: "Dias Mora",
        mejorgestion: "Mejor Gestion",
        deudatotal: "Deuda Total",
        statuscartera: "Estado Cartera",
        fechaacuerdo: "Fecha Acuerdo",
        valoracuerdo: "Valor Acuerdo",
        valorpago: "Valor Pago",
        fechaPago: "Fecha Pago",
        totalacuerdo: "Total Acuerdo",
        totalpago: "Total Pago",
        cuota: "Cuota",
        totalcuotas: "Total Cuotas",
        deudavencida: "Deuda Vencida",
        fechamejorgestion: "Fecha Mejor Gestion",
      };
      for (const iterator of this.dataDescargarCsv) {
        // if (iterator.asesor === "") {
        //   iterator.asesor = null;
        // }
        const dato = {
          asesor: this.revisar(iterator.asesor),
          nocuenta: this.revisar(iterator.nocuenta),
          nombrecompleto: this.revisar(iterator.nombrecompleto),
          identificacion: this.revisar(iterator.identificacion),
          ultimagestion: this.revisar(iterator.ultimagestion),
          gestion: this.revisar(iterator.gestion),
          diasmora: this.revisar(iterator.diasmora),
          mejorgestion: this.revisar(iterator.mejorgestion),
          deudatotal: this.revisar(iterator.deudatotal),
          statuscartera: this.revisar(iterator.statuscartera),
          fechaacuerdo: this.revisar(iterator.fechaacuerdo),
          valoracuerdo: this.revisar(iterator.valoracuerdo),
          valorpago: this.revisar(iterator.iterator),
          fechaPago: this.revisar(iterator.fechaPago),
          totalacuerdo: this.revisar(iterator.totalacuerdo),
          totalpago: this.revisar(iterator.totalpago),
          cuota: this.revisar(iterator.cuota),
          totalcuotas: this.revisar(iterator.totalcuotas),
          deudavencida: this.revisar(iterator.deudavencida),
          fechamejorgestion: this.revisar(iterator.fechamejorgestion),

          // idUsuario: 1,
        };
        descargar.push(dato);
      }
      console.log("datos a descargar", descargar);
      var fileTitle = new Date(); // or 'my-unique-title'

      exportCSVFile(headers, descargar, fileTitle);
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "No existen datos para descargar!",
        icon: "error",
      });
    }
    // const itemsNotFormatted = [
    //   {
    //     model: "Samsung S7",
    //     chargers: "55",
    //     cases: "56",
    //     earphones: "57",
    //     scratched: "2",
    //   },
    //   {
    //     model: "Pixel XL",
    //     chargers: "77",
    //     cases: "78",
    //     earphones: "79",
    //     scratched: "4",
    //   },
    //   {
    //     model: "iPhone 7",
    //     chargers: "88",
    //     cases: "89",
    //     earphones: "90",
    //     scratched: "6",
    //   },
    // ];

    // var itemsFormatted = [];

    // format the data
    // itemsNotFormatted.forEach((item) => {
    //   itemsFormatted.push({
    //     model: item.model.replace(/,/g, ""), // remove commas to avoid errors,
    //     chargers: item.chargers,
    //     cases: item.cases,
    //     earphones: item.earphones,
    //   });
    // });
  }
  revisar(data: any): any {
    if (data === "") {
      return null;
    } else {
      return data;
    }
  }

  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
  cambiosModal(): void {
    this.modalService.onHidden.pipe(takeUntil(this.stop$)).subscribe((data) => {
      switch (data) {
        case "asignar":
          this.obtenerMulticanal(this.campaniaSeleccionada);
          break;
      }
    });
  }
}
