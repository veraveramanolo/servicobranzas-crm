import { Component, OnInit } from "@angular/core";
import { FilterService, GridDataResult } from "@progress/kendo-angular-grid";
import {
  CompositeFilterDescriptor,
  FilterDescriptor,
  State,
  process,
} from "@progress/kendo-data-query";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { AsignacionService } from "../../../../../services/asignacion.service";
const flatten = (filter) => {
  const filters = (filter || {}).filters;
  if (filters) {
    return filters.reduce(
      (acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]),
      []
    );
  }
  return [];
};

@Component({
  selector: "app-modal-ciclos",
  templateUrl: "./modal-ciclos.component.html",
  styleUrls: ["./modal-ciclos.component.css"],
})
export class ModalCiclosComponent implements OnInit {
  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public responseEstados: any = [];
  public cargandoGrid: boolean;
  private estadoFilter: any[] = [];
  public estados = [
    { nombre: "Activo", value: "A" },
    { nombre: "Incativo", value: "I" },
  ];

  // @Output() Seleccionados: EventEmitter<any[]> = new EventEmitter();

  constructor(
    public modalRef: BsModalRef,
    private readonly modalService: BsModalService,
    private readonly _asignacionService: AsignacionService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    this.obtenerDatosTable();
  }
  public estadoChange(
    values: any[],
    filterService: FilterService,
    tipo: string
  ): void {
    console.log("estadoChange", values);
    filterService.filter({
      filters: values.map((value) => ({
        field: tipo,
        operator: "eq",
        value,
      })),
      logic: "or",
    });
  }
  public estadoFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.estadoFilter.splice(
      0,
      this.estadoFilter.length,
      ...flatten(filter).map(({ value }) => value)
    );
    console.log("estadoFilter", this.estadoFilter);
    return this.estadoFilter;
  }
  obtenerDatosTable(): void {
    this._asignacionService.obtenerEstados().subscribe((respuesta) => {
      console.log("datos de la tabla estados", respuesta);
      this.responseEstados = respuesta.data;
      this.datoGrid = process(this.responseEstados, this.estadoGrid);
    });
  }
  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log(estadoGrid);
    console.log("estados del grid", estadoGrid);
    // if (estadoGrid.filter.filters.length === 0 && !this.fechaFiltro) {
    //   this.mostrarTodoBotones = false;
    // } else {
    //   this.mostrarTodoBotones = true;
    // }
    this.estadoGrid = estadoGrid;
    this.datoGrid = process(this.responseEstados, this.estadoGrid);
    console.log("nuevos datos", process(this.responseEstados, this.estadoGrid));
  }

  cambiarEstado(id: number): void {
    this._asignacionService.cambiarEstado(id).subscribe((respuesta) => {
      console.log("respuesta metodo cambiar estado", respuesta);
      if (respuesta.exito === 1) {
        this.obtenerDatosTable();
        Swal.fire({
          title: "Correcto!",
          html: "Cambio de estado exitoso!",
          timer: 3000,
          timerProgressBar: true,
          showConfirmButton: false,
          icon: "success",
        });
      } else {
        this.spinner.hide();
        Swal.fire({
          allowOutsideClick: false,
          text: respuesta.mensage,
          icon: "error",
        });
      }
    });
  }
}
