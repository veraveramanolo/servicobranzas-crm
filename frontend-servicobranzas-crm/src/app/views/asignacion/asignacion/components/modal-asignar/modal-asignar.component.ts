import { Component, OnInit } from "@angular/core";

import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
// import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { AsignacionService } from "../../../../../services/asignacion.service";
import { MulticanalService } from "../../../../../services/multicanal.service";

@Component({
  selector: "app-modal-asignar",
  templateUrl: "./modal-asignar.component.html",
  styleUrls: ["./modal-asignar.component.css"],
})
export class ModalAsignarComponent implements OnInit {
  tarea: any;
  public idCampania: number;
  public totalClientes: any[];
  public asignados: any[] = [];
  public noAsignados: any[] = [];
  public totalAsignados: number;
  public totalNoAsignados: number;
  public total: number;
  public directo: boolean;
  tipo = [
    { nombre: "Deudafec", id: 1 },
    { nombre: "Pagos", id: 2 },
    { nombre: "Valorven", id: 3 },
    { nombre: "Valor Por Vencer", id: 4 },
    { nombre: "Valor A Pagar", id: 5 },
    // { nombre: "Actualizar Datos", id: 6 },
  ];
  inicial = this.tipo[0];

  public gridData: any[] = [];
  public gridView: any[];
  public mySelection: any[] = [];
  public ids = [];
  public manual: boolean;
  public columnas: any;
  public columnaValue: string;
  public usuario: any;
  constructor(
    // private readonly _multicanalService: MulticanalService,
    private readonly _asignacionService: AsignacionService,
    private spinner: NgxSpinnerService, // private spinner: NgxSpinnerService
    public modalRef: BsModalRef,
    private readonly modalService: BsModalService
  ) {}
  public ngOnInit(): void {
    this.separarClientes();
    this.obtenerColumnas();
  }

  CambioTipo(event: any): void {}

  verInfo(): void {
    console.log("mi seleccion", this.mySelection);
  }
  asignarSeleccionados(event: any): void {
    console.log("eventos del hijo al padre", event);

    this.mySelection = event;
  }
  separarClientes(): void {
    for (const variable of this.totalClientes) {
      if (variable.asesor === "") {
        this.noAsignados.push(variable);
      } else {
        this.asignados.push(variable);
      }
    }
    this.totalAsignados = this.asignados.length;
    this.totalNoAsignados = this.noAsignados.length;
    this.total = this.totalClientes.length;
  }
  validarNumeroClientes(numero: number, revisar: any[]): boolean {
    let total = 0;
    for (const iterator of revisar) {
      total = total + iterator.cantidad;
    }
    if (total > numero) {
      return false;
    } else {
      return true;
    }
  }

  guardarDirecto() {
    this.spinner.show();

    console.log("elementos listos para enviar", this.mySelection);
    let clientes = [];
    // let eviarDato: any;

    for (const cliente of this.totalClientes) {
      const add = {
        nocuenta: cliente.nocuenta,
        // valorpago: cliente.valorpago,
        // deudatotal: cliente.deudatotal,
        // deudavencida: cliente.deudavencida,
      };
      clientes.push(add);
    }
    const data = {
      asesores: [
        {
          idUsuario: this.usuario.idUsuario,
          cantidad: this.total,
        },
      ],
      clientes: clientes,
      idcampania: this.idCampania
    };
    console.log("array guaradar directo", data);
    this._asignacionService
      .crearAsignacionManual(data)
      .subscribe((respuesta) => {
        this.spinner.hide();
        console.log("respuesta guardar manual", respuesta);

        if (respuesta.exito === 1) {
          this.modalService.setDismissReason("asignar");
          this.modalRef.hide();
          Swal.fire({
            title: "Correcto!",
            html: "Asignacion directa creada!",
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: "error al guardar la asignacion directa!",
            icon: "error",
          });
        }
      });
  }
  guardarEquitativo() {
    this.spinner.show();
    if (this.mySelection.length > 0) {
      let clientes = [];
      // let eviarDato: any;
      console.log("asesores seleccionados", this.mySelection);
      console.log("clientes seleccionados", this.totalClientes);
      console.log("informacion del select", this.inicial);
      for (const cliente of this.totalClientes) {
        const add = {
          nocuenta: cliente.nocuenta,
          valorpago: cliente.valorpago,
          deudatotal: cliente.deudatotal,
          deudavencida: cliente.deudavencida,
        };
        clientes.push(add);
      }
      const data = {
        tipo: this.inicial.nombre,
        asesores: this.mySelection,
        clientes: clientes,
        idcampania: this.idCampania
      };
      console.log("datos a enviar modal ", data);
      this._asignacionService
        .crearAsignacionEqui(data)
        .subscribe((respuesta) => {
          this.spinner.hide();
          if (respuesta.exito === 1) {
            this.modalRef.hide();
            this.modalService.setDismissReason("asignar");
            Swal.fire({
              allowOutsideClick: false,
              title: "Correcto!",
              html: "Asignacion equitativa creada!",
              timer: 3000,
              timerProgressBar: true,
              showConfirmButton: false,
              icon: "success",
            });
            Swal.showLoading();
          } else {
            this.spinner.hide();
            Swal.fire({
              allowOutsideClick: false,
              text: "error al guardar la asignacion equitativa!",
              icon: "error",
            });
          }
        });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "Seleccione Asesores!",
        icon: "error",
      });
    }
  }
  obtenerColumnas(): void {
    this.spinner.show();
    let clientes = [];
    for (const cliente of this.totalClientes) {
      const add = {
        nocuenta: cliente.nocuenta,
        // valorpago: cliente.valorpago,
        // deudatotal: cliente.deudatotal,
        // deudavencida: cliente.deudavencida,
      };
      clientes.push(add);
    }
    this._asignacionService.obtenerColumnas(clientes).subscribe((respuesta) => {
      this.spinner.hide();
      console.log("respuesta de columnas", respuesta);
      this.columnas = respuesta.data;
    });
  }
  guardarManual(): void {
    this.spinner.show();
    if (this.mySelection.length > 0) {
      console.log("elementos listos para enviar", this.mySelection);
      let clientes = [];
      // let eviarDato: any;

      for (const cliente of this.totalClientes) {
        const add = {
          nocuenta: cliente.nocuenta,
          // valorpago: cliente.valorpago,
          // deudatotal: cliente.deudatotal,
          // deudavencida: cliente.deudavencida,
        };
        clientes.push(add);
      }
      const data = {
        asesores: this.mySelection,
        clientes: clientes,
        idcampania: this.idCampania
      };
      console.log("array guaradar manual", data);
      this._asignacionService
        .crearAsignacionManual(data)
        .subscribe((respuesta) => {
          this.spinner.hide();
          console.log("respuesta guardar manual", respuesta);

          if (respuesta.exito === 1) {
            this.modalRef.hide();
            this.modalService.setDismissReason("asignar");
            Swal.fire({
              allowOutsideClick: false,
              title: "Correcto!",
              html: "Asignacion manual creada!",
              timer: 3000,
              timerProgressBar: true,
              showConfirmButton: false,
              icon: "success",
            });
            Swal.showLoading();
          } else {
            Swal.fire({
              allowOutsideClick: false,
              text: "error al guardar la asignacion manual!",
              icon: "error",
            });
          }
        });
    } else {
      this.spinner.hide();
      Swal.fire({
        allowOutsideClick: false,
        text: "Seleccione Asesores!",
        icon: "error",
      });
    }
  }
  guardarColumna(): void {
    this.spinner.show();
    let clientes = [];
    for (const cliente of this.totalClientes) {
      const add = {
        nocuenta: cliente.nocuenta,
        // valorpago: cliente.valorpago,
        // deudatotal: cliente.deudatotal,
        // deudavencida: cliente.deudavencida,
      };
      clientes.push(add);
    }
    const datos = {
      tipo: this.columnaValue,
      asesores: [],
      clientes,
    };
    console.log("datos enviar a guardar por columna", datos);
    this._asignacionService
      .crearAsignacionColumna(datos)
      .subscribe((respuesta) => {
        this.spinner.hide();
        console.log("respuesta al guardar por columna", respuesta);
        if (respuesta.exito === 1) {
          this.modalService.setDismissReason("asignar");
          this.modalRef.hide();
          Swal.fire({
            allowOutsideClick: false,
            title: "Correcto!",
            html: "Asignacion por columna creada!",
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
          Swal.showLoading();
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: respuesta.mensage,
            icon: "error",
          });
        }
      });
  }

  submitForm(event: any): void {
    if (!this.directo) {
      switch (this.model.gender) {
        case "E":
          this.guardarEquitativo();
          break;
        case "C":
          if (this.columnaValue) {
            this.guardarColumna();
          } else {
            Swal.fire({
              allowOutsideClick: false,
              text: "Seleccione la columna!",
              icon: "error",
            });
          }

          break;
        case "M":
          if (
            this.validarNumeroClientes(
              this.totalClientes.length,
              this.mySelection
            )
          ) {
            this.guardarManual();
          } else {
            Swal.fire({
              allowOutsideClick: false,
              text: "La cantidad supera a los clientes seleccionados!",
              icon: "error",
            });
          }
          break;

        default:
          break;
      }
    } else {
      this.guardarDirecto();
    }

    // const eviarDato = {
    //   idTarea: this.tarea.id,
    //   tipoasignacion: 1,
    //   crmAsignacionDet: this.ids,
    // };
    // console.log("datos enviar", eviarDato);
    // this._multicanalService
    //   .insertarAsignacion(eviarDato)
    //   .subscribe((response) => {
    //     if (response.exito === 1) {
    //       this._multicanalService
    //         .asignaAsesor(response.data)
    //         .subscribe((respuesta) => {
    //           if (respuesta.exito === 1) {
    //             this.modalService.setDismissReason("Asignar");
    //             this.modalRef.hide();
    //             Swal.fire({
    //               title: "Correcto!",
    //               html: "Asignacion creada!",
    //               timer: 3000,
    //               timerProgressBar: true,
    //               showConfirmButton: false,
    //               icon: "success",
    //             });
    //           } else {
    //             Swal.fire({
    //               allowOutsideClick: false,
    //               text: "error al guardar la asignacion!",
    //               icon: "error",
    //             });
    //           }
    //         });
    //     } else {
    //       Swal.fire({
    //         allowOutsideClick: false,
    //         text: "error al guardar la asignacion!",
    //         icon: "error",
    //       });
    //     }
    //     console.log("respuesta del insertar", response);
    //   });
  }
  cambioEstadoManual(event: any): void {
    console.log(event);
    this.manual = event;
  }
  public model = {
    gender: "E",
  };
}
