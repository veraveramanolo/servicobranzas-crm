import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FilterService, GridDataResult } from "@progress/kendo-angular-grid";
import {
  CompositeFilterDescriptor,
  FilterDescriptor,
  State,
  process,
} from "@progress/kendo-data-query";
import { AsignacionService } from "../../../../../services/asignacion.service";
import { MulticanalService } from "../../../../../services/multicanal.service";

const flatten = (filter) => {
  const filters = (filter || {}).filters;
  if (filters) {
    return filters.reduce(
      (acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]),
      []
    );
  }
  return [];
};

@Component({
  selector: "app-table-editable",
  templateUrl: "./table-editable.component.html",
  styleUrls: ["./table-editable.component.css"],
})
export class TableEditableComponent implements OnInit {
  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public responseAsignacion: any = [];
  public cargandoGrid: boolean;
  public mySelection: any[] = [];
  public asesores = [];
  public asesoresManual = [];
  @Input() idCampania: number;
  @Output() Seleccionados: EventEmitter<any[]> = new EventEmitter();
  private estadoFilter: any[] = [];

  constructor(
    private readonly _multicanalService: MulticanalService,
    private readonly _asignacionService: AsignacionService
  ) {}

  ngOnInit(): void {
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    console.log("identificador campaña modal", this.idCampania);
    this.obtenerListaEstados(this.idCampania, null);
    this.obtenerDatosTable();
  }

  public selectedCallback = (args) => {
    return args.dataItem;
  };

  cambioCantidad(data, cantidadInput: any = 0) {
    let identificador;
    let asesor;
    let nuevo = true;
    const cantidad: number = Number(cantidadInput);
    if (cantidad > -1) {
      for (let index = 0; index < this.asesoresManual.length; index++) {
        const element = this.asesoresManual[index];
        if (data.idUsuario === element.idUsuario) {
          nuevo = false;
          console.log("cantidad del input", cantidad);
          if (cantidad === 0) {
            this.asesoresManual.splice(index, 1);
          } else {
            identificador = index;
            asesor = {
              idUsuario: element.idUsuario,
              cantidad: cantidad,
            };
          }

          break;
        }
      }
      if (identificador && asesor) {
        this.asesoresManual[identificador] = asesor;
      } else if (nuevo) {
        const newAsesor = {
          idUsuario: data.idUsuario,
          cantidad: cantidad,
        };
        this.asesoresManual.push(newAsesor);
      }
    }
    this.Seleccionados.emit(this.asesoresManual);
    console.log("datos enviados al modal desde la tabla", this.asesoresManual);
  }

  onSelectedKeysChange(event) {
    console.log("cambios y eventos", event);
    this.Seleccionados.emit(event);
  }

  public estadoChange(
    values: any[],
    filterService: FilterService,
    tipo: string
  ): void {
    console.log("estadoChange", values);
    filterService.filter({
      filters: values.map((value) => ({
        field: tipo,
        operator: "eq",
        value,
      })),
      logic: "or",
    });
  }
  // prueba() {
  //   console.log("items seleccionados", this.mySelection);
  // }

  public estadoFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.estadoFilter.splice(
      0,
      this.estadoFilter.length,
      ...flatten(filter).map(({ value }) => value)
    );
    console.log("estadoFilter", this.estadoFilter);
    return this.estadoFilter;
  }
  obtenerListaEstados(idcampania: number, fecha: any): void {
    this._multicanalService
      .obtenerMultiselect(idcampania, fecha)
      .subscribe((data) => {
        console.log("listado estados", data);
        // this.estadoListaCantidad = data.data;
        this.asesores = data.asesores;

        // this.todosLosFiltros.splice(1, 1);

        // this.estadofiltro = [...this.asesores];
        // this.estadofiltro.pop();
        // console.log("Last: ", this.estadofiltro);
      });
  }

  obtenerDatosTable(): void {
    this._asignacionService
      .obtenerUsuariosAsignados(this.idCampania)
      .subscribe((respuesta) => {
        console.log("datos de la tabla modal", respuesta);
        this.responseAsignacion = respuesta.data;
        this.datoGrid = process(this.responseAsignacion, this.estadoGrid);
      });
  }

  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log(estadoGrid);
    console.log("estados del grid", estadoGrid);
    // if (estadoGrid.filter.filters.length === 0 && !this.fechaFiltro) {
    //   this.mostrarTodoBotones = false;
    // } else {
    //   this.mostrarTodoBotones = true;
    // }
    this.estadoGrid = estadoGrid;
    this.datoGrid = process(this.responseAsignacion, this.estadoGrid);
    console.log(
      "nuevos datos",
      process(this.responseAsignacion, this.estadoGrid)
    );
  }
}
