import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from "@angular/core";
import { FilterService, GridDataResult } from "@progress/kendo-angular-grid";
import {
  State,
  process,
  CompositeFilterDescriptor,
  FilterDescriptor,
} from "@progress/kendo-data-query";
import { BsModalService } from "ngx-bootstrap/modal";
import { Observable, Subscription } from "rxjs";
import Swal from "sweetalert2";
import { AsignacionService } from "../../../../../services/asignacion.service";
import { ComunicacionService } from "../../../../../services/comunicacion.service";
import { MulticanalService } from "../../../../../services/multicanal.service";
import { ModalAsignarComponent } from "../modal-asignar/modal-asignar.component";

const flatten = (filter) => {
  const filters = (filter || {}).filters;
  if (filters) {
    return filters.reduce(
      (acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]),
      []
    );
  }
  return [];
};

@Component({
  selector: "app-topes-asignacion",
  templateUrl: "./topes-asignacion.component.html",
  styleUrls: ["./topes-asignacion.component.css"],
})
export class TopesAsignacionComponent implements OnInit, OnDestroy {
  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public responseAsignacion: any = [];
  public cargandoGrid: boolean;
  public mySelection: any[] = [];
  public asesores = [];
  public idCampania: number;
  public clientes = [];
  // @Output() Seleccionados: EventEmitter<any[]> = new EventEmitter();
  private estadoFilter: any[] = [];
  selecionados$: Observable<any>;
  subscription: Subscription;
  subscription2: Subscription;

  constructor(
    private readonly _multicanalService: MulticanalService,
    private readonly _asignacionService: AsignacionService,
    private _comunicacion: ComunicacionService,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };

    this.subscription = this._comunicacion.navItem$.subscribe((item) => {
      console.log("observable realizada manualmente", item);
      if (item) {
        this.idCampania = item;
        console.log("identificador campaña modal", this.idCampania);
        this.obtenerListaEstados(this.idCampania, null);
        this.obtenerDatosTable();
      }
    });
    this.obtenerClientes();
  }
  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }
  private obtenerClientes(): void {
    this.subscription2 = this._comunicacion.clientes$.subscribe((respuesta) => {
      this.clientes = respuesta;
    });
  }

  public estadoChange(
    values: any[],
    filterService: FilterService,
    tipo: string
  ): void {
    console.log("estadoChange", values);
    filterService.filter({
      filters: values.map((value) => ({
        field: tipo,
        operator: "eq",
        value,
      })),
      logic: "or",
    });
  }
  // prueba() {
  //   console.log("items seleccionados", this.mySelection);
  // }

  public estadoFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.estadoFilter.splice(
      0,
      this.estadoFilter.length,
      ...flatten(filter).map(({ value }) => value)
    );
    console.log("estadoFilter", this.estadoFilter);
    return this.estadoFilter;
  }
  obtenerListaEstados(idcampania: number, fecha: any): void {
    this._multicanalService
      .obtenerMultiselect(idcampania, fecha)
      .subscribe((data) => {
        console.log("listado estados", data);
        // this.estadoListaCantidad = data.data;
        this.asesores = data.asesores;

        // this.todosLosFiltros.splice(1, 1);

        // this.estadofiltro = [...this.asesores];
        // this.estadofiltro.pop();
        // console.log("Last: ", this.estadofiltro);
      });
  }

  obtenerDatosTable(): void {
    this._asignacionService
      .obtenerUsuariosAsignados(this.idCampania)
      .subscribe((respuesta) => {
        console.log("datos de la tabla modal", respuesta);
        this.responseAsignacion = respuesta.data;
        this.datoGrid = process(this.responseAsignacion, this.estadoGrid);
      });
  }

  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log(estadoGrid);
    console.log("estados del grid", estadoGrid);
    // if (estadoGrid.filter.filters.length === 0 && !this.fechaFiltro) {
    //   this.mostrarTodoBotones = false;
    // } else {
    //   this.mostrarTodoBotones = true;
    // }
    this.estadoGrid = estadoGrid;
    this.datoGrid = process(this.responseAsignacion, this.estadoGrid);
    console.log(
      "nuevos datos",
      process(this.responseAsignacion, this.estadoGrid)
    );
  }

  open(usuario: any): void {
    if (this.idCampania && this.clientes.length > 0) {
      this.modalService.show(ModalAsignarComponent, {
        initialState: {
          // detalle: this.dataDescargarCsv,
          idCampania: this.idCampania,
          totalClientes: this.clientes,
          directo: true,
          usuario: usuario,
          // filtro: this.estadoGrid,
          // idCampania: Number(this.params.GestionID),
          // arbolSelecciondo: this.arbolSelecciondo,
          // idProducto: Number(this.params.ProductoID),
        },
        backdrop: "static",
        class: "gray modal-lg",
      });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: "Debe seleccionar una campaña y cliente(s)!",
        icon: "error",
      });
    }
    // this.params = this.activatedRoute.snapshot.params;
  }
}
