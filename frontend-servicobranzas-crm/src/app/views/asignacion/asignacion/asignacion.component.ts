import { Component, OnInit } from "@angular/core";
import { ComunicacionService } from "../../../services/comunicacion.service";

@Component({
  selector: "app-asignacion",
  templateUrl: "./asignacion.component.html",
  styleUrls: ["./asignacion.component.css"],
})
export class AsignacionComponent implements OnInit {
  botones = {
    Asignacion: "Asignacion",
    Estructuras: "Estructuras",
    Arbol: "Arbol",
    Componentes: "Componentes",
    Cargues: "cargues",
  };
  btnSeleccionado = "Asignacion";

  constructor(private _comunicacion: ComunicacionService) {}

  ngOnInit(): void {}

  cambiarClassStep(btn: string) {
    this.btnSeleccionado = btn;
  }

  obtenerCampaniaHijo(event: number): void {
    this._comunicacion.asignarCampania(event);
  }
  obtenerClientes(event: any): void {
    this._comunicacion.asignarClientes(event);
  }
}
