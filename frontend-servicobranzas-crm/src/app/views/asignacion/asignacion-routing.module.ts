import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RemoverAsignacionComponent } from "./remover-asignacion/remover-asignacion.component";
import { AsignacionComponent } from "./asignacion/asignacion.component";
import { CambiarEstadoComponent } from "./cambiar-estado/cambiar-estado.component";

const routes: Routes = [
  {
    path: "",
    data: { titulo: "Asignación" },
    children: [
      {
        path: "",
        redirectTo: "asignacion",
      },
      {
        path: "asignacion/remover-asignacion",
        component: RemoverAsignacionComponent,
        data: { titulo: "Remover asignación" },
      },
      {
        path: "asignacion/asignacion",
        component: AsignacionComponent,
        data: { titulo: "Asignación" },
      },
      {
        path: "asignacion/cambiar-estado",
        component: CambiarEstadoComponent,
        data: { titulo: "Cambiar estado de Asignación" },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsignacionRoutingModule {}
