import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AsignacionRoutingModule } from "./asignacion-routing.module";
import { AsignacionComponent } from "./asignacion/asignacion.component";
import { RemoverAsignacionComponent } from "./remover-asignacion/remover-asignacion.component";
import { CambiarEstadoComponent } from "./cambiar-estado/cambiar-estado.component";
import { BotonesSuperioresComponent } from "./asignacion/components/botones-superiores/botones-superiores.component";

//primeng
import { CardModule } from "primeng/card";
import { ButtonModule } from "primeng/button";

//kendo
import { GridModule } from "@progress/kendo-angular-grid";
import { ListViewModule } from "@progress/kendo-angular-listview";
import { LayoutModule } from "@progress/kendo-angular-layout";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { PopupModule } from "@progress/kendo-angular-popup";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { LabelModule } from "@progress/kendo-angular-label";
import { DropDownListModule } from "@progress/kendo-angular-dropdowns";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { TopesAsignacionComponent } from "./asignacion/components/topes-asignacion/topes-asignacion.component";
import { ModalAsignarComponent } from "./asignacion/components/modal-asignar/modal-asignar.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TableNormalComponent } from "./asignacion/components/table-normal/table-normal.component";
import { TableEditableComponent } from "./asignacion/components/table-editable/table-editable.component";

import { ModalModule } from "ngx-bootstrap/modal";

import { NgxSpinnerModule } from "ngx-spinner";
import { ModalCiclosComponent } from './asignacion/components/modal-ciclos/modal-ciclos.component';
import { EstadoPipe } from './asignacion/pipes/estado.pipe';

@NgModule({
  declarations: [
    AsignacionComponent,
    RemoverAsignacionComponent,
    CambiarEstadoComponent,
    BotonesSuperioresComponent,
    TopesAsignacionComponent,
    ModalAsignarComponent,
    TableNormalComponent,
    TableEditableComponent,
    ModalCiclosComponent,
    EstadoPipe,
  ],
  entryComponents: [ModalAsignarComponent, TableNormalComponent],
  imports: [
    CommonModule,
    AsignacionRoutingModule,
    CardModule,
    ButtonModule,
    GridModule,
    InputsModule,
    DateInputsModule,
    DropDownsModule,
    DropDownListModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    NgxSpinnerModule,
  ],
})
export class AsignacionModule {}
