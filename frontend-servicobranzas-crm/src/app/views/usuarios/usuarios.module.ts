import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
//import { Routes, RouterModule } from '@angular/router';


// Components
import { UsuarioComponent } from './usuario/usuario.component';
import { RegisterComponent } from './usuario/register.component';
import { PerfilComponent } from './perfil/perfil.component';
import { GrupoComponent } from './perfil/grupo.component';
import { PermisosComponent } from './perfil/permisos/permisos.component';
import { TreeViewModule } from '@progress/kendo-angular-treeview';

// Notifications Routing
import { UsuariosRoutingModule } from './usuarios-routing.module';

/*@NgModule({
  declarations: [UsuarioComponent, RegisterComponent ,PerfilComponent, GrupoComponent],
  imports: [
    CommonModule
  ]
})*/
@NgModule({
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    TreeViewModule
  ],
  declarations: [
    UsuarioComponent,
    RegisterComponent,
    PerfilComponent,
    GrupoComponent,
    PermisosComponent
  ]
})


export class UsuariosModule { }
