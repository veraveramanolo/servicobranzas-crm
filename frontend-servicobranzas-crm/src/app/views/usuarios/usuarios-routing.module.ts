import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuarioComponent } from './usuario/usuario.component';
import { RegisterComponent } from './usuario/register.component';
import { PerfilComponent } from './perfil/perfil.component';
import { GrupoComponent } from "./perfil/grupo.component";
import { PermisosComponent } from "./perfil/permisos/permisos.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Usuarios'
    },
    children: [
      {
        path: '',
        redirectTo: 'usuarios'
      },
      {
        path: 'usuario/usuario',
        component: UsuarioComponent,
        data: {
          title: 'Usuario'
        }
      },
      {
        path: 'usuario/register',
        component: RegisterComponent,
        data: {
          title: 'Registro'
        }
      },
      {
        path: 'perfil/perfil',
        component: PerfilComponent,
        data: {
          title: 'Perfil'
        }
      },
      {
        path: 'perfil/grupo',
        component: GrupoComponent,
        data: {
          title: 'Grupo'
        }
      },
      {
        path: 'perfil/permisos/permisos',
        component: PermisosComponent,
        data: {
          title: 'Permisos de Grupo'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule {}
