import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.css']
})
export class PermisosComponent implements OnInit {

  /*constructor() { }*/

  ngOnInit(): void {
  }

  public checkedKeys: any[] = ['0'];

    public data: any[] = [{
        text: 'Datos', items: [
            { text: 'Agregar' },
            { text: 'Actualizar' },
            { text: 'Eliminar' }
        ]
    }, {
        text: 'Plantillas', items: [
            { text: 'Agregar' },
            { text: 'Actualizar' },
            { text: 'Eliminar' }
        ]
    }
    ]
    public soporte: any[] = [{
        text: 'Pantallas', items: [
            { text: 'Cuentas de Usuario',
            items: [
                { text: 'Crear' },
                { text: 'Eliminar' }
              ] },
            { text: 'Multicanal' }
        ]
    }, {
        text: 'Gestores', items: [
            { text: 'Agregar Cliente' },
            { text: 'Actualizar Crédito' }
        ]
    }];

    public fetchChildren(node: any): Observable<any[]> {
        // Return the parent node's items collection as children
        return of(node.items);
    }

    public hasChildren(node: any): boolean {
        // Check if the parent node has children
        return node.items && node.items.length > 0;
    }

}
