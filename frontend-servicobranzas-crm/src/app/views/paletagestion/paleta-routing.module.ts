import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaletaComponent } from './paletagestion.component';

const routes: Routes = [
  {
    path: '',
    component: PaletaComponent,
    data: {
      title: 'Paleta de gestion'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaletaRoutingModule {}
