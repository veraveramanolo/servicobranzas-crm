import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { PaletaComponent } from './paletagestion.component';
import { PaletaRoutingModule } from './paleta-routing.module';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaletaRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ModalModule.forRoot()
  ],
  declarations: [ PaletaComponent ]
})
export class PaletaModule { }
