import { Component, OnInit,ViewChild } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

@Component({
  templateUrl: 'paletagestion.component.html'
})
export class PaletaComponent implements OnInit {

level1=[];
level2=[];
level3=[];
selected = {select1:false,value1:"0",select2:false,value2:"0",select3:false,value3:"0" };
opt1 = "";
opt2 = "";
opt3 = "";
@ViewChild('smallModal') public smallModal: ModalDirective;
@ViewChild('deleteModal') public deleteModal: ModalDirective;

  ngOnInit(): void {
    // generate random values for mainChart
    this.level1 = [
	    {key:1,value:{texto:'CONTACTO CONYUGE',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:2,value:{texto:'CONTACTO TERCERO',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:3,value:{texto:'CONTACTO TITULAR',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:4,value:{texto:'NO CONTACTO',tipogestion:'',tipocontacto:'',estado:'A'}},
    ];
  }

  changeLevel1() {
    console.log(this.opt1);
    this.selected.select1 = true;
    this.selected.value1 = this.opt1
    this.level2 = [
	    {key:2,value:{texto:'CLIENTE FALLECIDO',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:3,value:{texto:'MENSAJE ENTREGADO',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:4,value:{texto:'NO RECIBEN MENSAJES',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:5,value:{texto:'RECIBEN MENSAJE',tipogestion:'',tipocontacto:'',estado:'A'}},
    ];
  }
  changeLevel2() {
    console.log(this.opt2);
    this.selected.select2 = true;
    this.selected.value2 = this.opt2
    this.level3 = [
	    {key:2,value:{texto:'CLIENTE FALLECIDO',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:3,value:{texto:'MENSAJE ENTREGADO',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:4,value:{texto:'NO RECIBEN MENSAJES',tipogestion:'',tipocontacto:'',estado:'A'}},
	    {key:5,value:{texto:'RECIBEN MENSAJE',tipogestion:'',tipocontacto:'',estado:'A'}},
    ];
  }

  changeLevel3() {
    console.log(this.opt3);
    this.selected.select3 = true;
    this.selected.value3 = this.opt3
  }
}
