import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MetricasService } from "../../../services/metricas.service";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { format } from "date-fns";
import { CrmCartera } from "../../../models/CrmCartera";
import { CrmCampania } from "../../../models/CrmCampania";
import {
  DataStateChangeEvent,
  GridDataResult,
  FilterService,
} from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import { VariableGlobalService } from "../../../services/variable-global.service";
import { isNullOrUndefined } from "util";
import { ModeloRespuesta } from "../../../models/ModeloRespuesta.models";
import { CrmCarteraService } from "../../../services/crm-cartera.service";
import { CampaniaService } from "../../../services/campania.service";
import { ConsultauserService } from "../../../services/consultauser.service";
import { TipoContactoService } from "../../../services/tipo-contacto.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ModalFiltroIndicadoresGestionComponent } from "../modal-filtro-indicadores-gestion/modal-filtro-indicadores-gestion.component";

const distinctEstados = (data, tipo: string) =>
      data
        .map((x) => x[tipo])
        .filter((x, idx, xs) => {
          let cont;
          cont = xs.length;
          console.log("cont ",cont);
          // console.log(x, idx, xs);
          return xs.findIndex((y) => y === x) === idx;
        });

const distinctItem = (data, tipo: string) =>
data
  .map((x) => x[tipo])
  .filter((x, idx, xs) => {
    // console.log(x, idx, xs);
    return xs.findIndex((y) => y === x) === idx;
  });
interface Valor {
  campo: string
}

@Component({
  selector: 'app-indicadores',
  templateUrl: './indicadores.component.html',
  styleUrls: ['./indicadores.component.css']
})
export class IndicadoresComponent implements OnInit {
  master: number;
  public titulo: string;
  listaGestiones: any[] = [];
  listaGestionesxAsesor: any[] = [];
  listaGestionescont: any[] = [];
  listaGestionesdol: any[] = [];
  listaGestionesper: any[] = [];
  listaEstadosper: any[] = [];
  listaEstadosdol: any[] = [];
  listaEstadostotal: any[] = [];
  listaEstadostotaldol: any[] = [];
  listaMostrar: any[] = [];
  total: any[] = [];
  params: any;
  fechafin = null;
  fechainicio = null;
  public fechaidate: Date;
  public fechafdate: Date;
  hora = null;
  public position: "top" | "bottom" | "both" = "top";
  public datoGrid: GridDataResult;
  public InfoCartera: CrmCartera[] = [];
  public InfoCampania: CrmCampania[] = [];
  public listaCartera: CrmCartera[] = [];
  public listaCampania: CrmCampania[] = [];
  public listaCampaniafiltrada: CrmCampania[] = [];
  public listaCampaniaEmergente: CrmCampania[] = [];
  public valorComboCartera: CrmCartera;
  public valorComboCampania: CrmCampania = null;
  admin: number;
  idcampania: number;
  tipoDias: number = 2;
  tipoMontos: number = 1;
  listaDias: any[] = [
    { id: 1, texto: "Total" },
    { id: 2, texto: "Hoy", selected: true},
    { id: 3, texto: "Escoger fecha" },
    // { id: 4, texto: "15 Días antes" },
    // { id: 5, texto: "30 Días antes" },
  ];
  listaTipoMontosGestion: any[] = [
    { id: 1, texto: "Valor acuerdo/pago" },
    { id: 2, texto: "Valor vencido" },
    { id: 3, texto: "Deuda total" },
  ];
  listaTipoMontosEstados: any[] = [
    { id: 1, texto: "Deuda total" },
    { id: 2, texto: "Deuda vencida" },
  ];
  day: number;
  public events: string[] = [];
  colores: any[] = ['#64B5F6','#DAF7A6','#AB47BC','#EC407A','#F9E79F','#42A5F5','#7E57C2',
    '#66BB6A','#FFCA28','#26A69A','#C70039','#99A3A4','#FFA07A','#2471A3','#D8BFD8','#8B5742',
    '#9AC0CD','#6495ED','#7CFC00','#EEE8AA'];
  

  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: "and",
      filters: [],
    },
  };
  public estadoseleccionado: boolean = false;
  listaEstados: any[] = [];
  listaEstadoscont: any[] = [];
  totalEstados: any[] = [];
  public mySelection: any[] = [];
  @Output() estadoseleccionadotbl: EventEmitter<any[]> = new EventEmitter();
  public mixtoseleccionado: boolean = false;
  public totalcantidad: number;
  public totalmonto: number;
  public positivos: any;
  public negativos: any;
  public positivosPorcent: number;
  public listaAgentes: any[];
  public listaPerfilaciones: any[];
  public listaAgenteEstadoselect: any[];
  public totaltab: number;
  public listaPerfilacionEstadoselect: any[];
  public listaGetEstados: any[];
  // ------CHARTS------
  data: any;
  chartOptions: any;
  basicDataEstados: any;
  basicOptionsEstados: any;
  indexTabDerecho: any = 0;
  indexTabIndicadores: any = 0;
  basicDataAgentes: any;
  horizontalOptions: any;
  basicDataGestiones: any;
  basicOptionsGestiones: any;

  // -------DIALOG--------
  displayPosition: boolean;
  positionDialog: string;
  tipoDeContacto: any[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly _metricasService: MetricasService,
    private serviceCarmpania: CampaniaService,
    private serviceCrmCartera: CrmCarteraService,
    private serviceUsuario: ConsultauserService,
    private global: VariableGlobalService,
    private readonly modalService: BsModalService,
    private _tipoContactoService: TipoContactoService,
  ) { }

  async ngOnInit() {
    await this.GetCarteras(); //ITA
    await this.GetConsultaCampania(); //ITA
    this.showTime();
    // -- DIALOGS
    this.selectedContacto = this.tipoDeContacto;
    // this.mostrarDetalleCuentaCampania();
    // this.selectedFiltro = this.DETALLECAMPANIA;
  }

  async Usuario(id: number) {
    let resp: ModeloRespuesta = null;
    resp = await this.serviceUsuario.Usuario(id);
    let user = resp.data;
    this.admin = user.idRol;
    console.log(this.admin);
  }

  async GetCarteras() {
    this.listaCartera = [];
    this.InfoCartera = [];

    let resp: ModeloRespuesta = null;
    resp = await this.serviceCrmCartera.GetCarteras();

    this.InfoCartera = resp.data;
    this.listaCartera = this.InfoCartera;
    console.log("listaCartera",this.listaCartera);
  }

  async GetConsultaCampania() {
    this.listaCampania = [];
    this.InfoCampania = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetConsultaCampania();

    this.InfoCampania = resp.data;
    this.listaCampania = this.InfoCampania;
    console.log("listaCampania", this.listaCampania);
  }
  
  FiltroComboCartera(valor) {
    this.listaCartera = this.InfoCartera.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }

  FiltroComboCampania(valor) {
    console.log("FiltroComboCampania(valor) ",valor);
    this.listaCampaniafiltrada = this.listaCampaniaEmergente.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
    console.log("this.listaCampaniafiltrada ",this.listaCampaniafiltrada);
    // this.mostrarEstados(this.valorComboCampania);
  }

  handleChangeIndicadores(e) {
    this.indexTabIndicadores = e.index;
    console.log("this.indexTabIndicadores ", this.indexTabIndicadores);
    this.closeDialog();
  }

  Agentes(id: any){
    console.log("id ",id);
    this.listaAgenteEstadoselect = this.listaAgentes.filter(
      (s) => s.id === id);
    console.log("this.listaAgenteEstadoselect ",this.listaAgenteEstadoselect);
    this.ChartAgentes(this.listaAgenteEstadoselect);
  }

  Perfilaciones(descripcion: any){
    this.listaPerfilacionEstadoselect = this.listaPerfilaciones.filter(
      (s) => s.contacto === descripcion);
    let perfilacionesa: any[] = [];
    let valoresa: any[] = [];
    this.listaPerfilacionEstadoselect.forEach(element => {
      perfilacionesa.push(element.perfilaciones);
      valoresa.push(element.valor);
    });
    console.log("perfilaciones ",perfilacionesa);
    console.log("valores ", valoresa);
    this.data = {
      labels: perfilacionesa,
      datasets: [
          {
              data: valoresa,
              backgroundColor: this.colores
          }
      ]
    };
    this.chartOptions = {
      title: {
        display: true,
        text: 'MOTIVO DE INCUMPLIMIENTO',
        fontSize: 16
      },
      responsive: true,
      legend: {
        position: 'right',
        labels: {
          boxWidth: 12
        }
      },
    };
  }

  ChartEstados(lista: any){
    let datasetsEstados: any[] = [];
    let cont = 0;
    lista.forEach(element => {
      datasetsEstados.push({
                              label: element.descripcion,
                              backgroundColor: this.colores[cont],
                              data: [element.valor]
                            });
      cont++;
    });
    this.basicDataEstados = {
      labels: ['Estados'],
      datasets: datasetsEstados
      // [
      //   {label: 'NO CONTESTA', backgroundColor: '#42A5F5', data: [4]},
      //   {label: 'MENSAJE BUZON DE VOZ', backgroundColor: '#FFA726', data: [1]},
      //   {label: 'NO RECIBEN MENSAJES', backgroundColor: '#FFCA28', data: [2]},
      //   {label: 'RECIBEN MENSAJE', backgroundColor: '#26A69A', data: [3]}
      // ]
    };
    console.log("this.basicDataEstados ",this.basicDataEstados);

    this.basicOptionsEstados = {
      responsive: true,
      scales: {
        yAxes: [{
           ticks: {
              beginAtZero: true,
           }
        }]
      },
      legend: {
        position: 'right',
        labels: {
          boxWidth: 12
        }
      },
    };
  }

  ChartAgentes(lista: any){
    let datasetsAgentes: any[] = [];
    let cont = 0;
    // ---- p-chart
    lista.forEach(element => {
      datasetsAgentes.push({
                              label: element.asesor,
                              backgroundColor: this.colores[cont],
                              data: [element.valor]
                            });
      cont++;
    });
    this.basicDataAgentes = {
      labels: ['Agentes'],
      datasets: datasetsAgentes
    };
    console.log("this.basicDataAgentes ",this.basicDataAgentes);

    this.horizontalOptions = {
      indexAxis: 'y',
      scales: {
        yAxes: [{
           ticks: {
              beginAtZero: true,
           }
        }]
      },
      responsive: true,
      legend: {
        position: 'right',
        labels: {
          boxWidth: 12
        }
      },
    };
  }

  ChartGestiones(lista: any){
    let datasetsGestiones: any[] = [];
    let cont = 0;
    let label: any;
    // ---- p-chart
    lista.forEach(element => {
      if (this.tipo.tiempo == "D") {
        label = element.asesor;
      } else {
        label = element.hora;
      }
      datasetsGestiones.push({
                              label: label,
                              backgroundColor: this.colores[cont],
                              data: [element.gestiones,element.clientes,element.directos,element.efectivos,element.acuerdos]
                            });
      cont++;
    });
    this.basicDataGestiones = {
      labels: ['Gestiones', 'Clientes', 'Directos', 'Efectivos', 'Acuerdos'],
      datasets: datasetsGestiones
    };
    console.log("this.basicDataGestiones ",this.basicDataGestiones);

    this.basicOptionsGestiones = {
      responsive: true,
      scales: {
        yAxes: [{
           ticks: {
              beginAtZero: true,
           }
        }]
      },
      legend: {
        position: 'right',
        labels: {
          boxWidth: 12
        }
      },
    };
  }

  EventoComboCartera(valor: CrmCartera) {
    this.datoGrid = undefined;
    this.valorComboCampania = undefined;
    if (valor) {
      this.listaCampaniafiltrada = [];
      this.listaCampaniafiltrada = this.listaCampania.filter(
        (x) => x.idCartera === valor.id
      );
      this.listaCampaniaEmergente = this.listaCampaniafiltrada;
    } else {
      this.listaCampaniafiltrada = [];
    }
  }

  EventoComboCampania(valor: CrmCampania) {
    console.log("EventoComboCampania(valor ",valor);
    this.datoGrid = undefined;
    if (!isNullOrUndefined(valor)) {
      setTimeout(() => {
        this.valorComboCampania = valor;
        this.mostrarGestiones(this.valorComboCampania);
        this.mostrarEstados(this.valorComboCampania);
      }, 200);
    }
  }
  
  mostrarGestiones(valor: CrmCampania): void {
    this.params = this.activatedRoute.snapshot.params;
    // this.fechainicio = null;
    if(valor.id !== undefined){
      this.idcampania = valor.id;
    }
    // this.fechaahora = format(
    //   new Date(),
    //   "yyyy-MM-dd H:mm:s"
    // );
    // let fechafin = this.fechaahora;
    console.log("valor.id ",this.idcampania);
    if(this.valor.present == null){
      this.valor.present = "N";
    }
    if(this.tipo.tiempo == null){
      this.tipo.tiempo = "D";
    }
    console.log("this.tipoDias ", this.tipoDias);
    console.log("fechainicio antes ",this.fechainicio);
    console.log("fechafin antes ",this.fechafin);
    switch (Number(this.tipoDias)) {
      case 1:
        this.fechainicio = null;
        this.fechafin = format(new Date(), "yyyy-MM-dd H:mm:s");
        console.log("fechainicio 1 ",this.fechainicio);
        console.log("fechafin 1 ",this.fechafin);
        break;
      case 2:
        this.fechainicio = format(new Date(), "yyyy-MM-dd 00:00:00");
        this.fechafin = format(new Date(), "yyyy-MM-dd H:mm:s");
        console.log("fechainicio 2",this.fechainicio);
        console.log("fechafin 2 ",this.fechafin);
        break;
      case 3:
        this.fechainicio = format(this.fechaidate, "yyyy-MM-dd H:mm:s");
        this.fechafin = format(this.fechafdate, "yyyy-MM-dd H:mm:s");
        console.log("fechainicio 3 ",this.fechainicio);
        break;
      // case 4:
      //   this.day = new Date().getDate()-15;
      //   this.fechainicio = format(new Date(), "yyyy-MM-"+this.day+" H:mm:s");
      //   break;
      default:
        break;
    }
    if (valor) {
      if (this.tipo.tiempo == "D") {
        this._metricasService
        .GetGestiones(this.idcampania,this.fechainicio,this.fechafin)
        .subscribe((respuesta) => {
          if (respuesta.exito == 1) {
            console.log("Gestiones", respuesta.data);
            this.listaGestiones = respuesta.data.listaGestiones;
            this.listaGestionescont = respuesta.data.results;
            console.log("this.listaGestiones ",this.listaGestiones);
            console.log("this.listaGestionescont", this.listaGestionescont);
            switch (Number(this.tipoMontos)) {
              case 1:
                this.listaGestionesdol = respuesta.data.resultsdolar;
                break;
              case 2:
                this.listaGestionesdol = respuesta.data.resultsdolarvv;
                break;
              case 3:
                this.listaGestionesdol = respuesta.data.resultsdolardt;
                break;
              default:
                break;
            }
            this.listaGestionesper = respuesta.data.resultsporc;
            console.log("this.valor.present ",this.valor.present);
            this.ListaXvalor(this.valor.present);
            this.addTotal();
            console.log("this.listaMostrar chart ",this.listaMostrar);
            this.ChartGestiones(this.listaMostrar);
          }
        });
      } else {
        this.mostrarGestionesXhora(this.idcampania,this.fechainicio,this.fechafin);
      }

      console.log("this.total ",this.total);
      console.log(" ----------------------- FIN GESTIONES");
        
    } else {
      this.vaciarListasGestiones();
    }
  }

  mostrarGestionesXhora(idcampania: number, fechainicio: any, fechafin: any): void {
    console.log("g x h idcampania ",idcampania);
    console.log("fechainicio ",fechainicio);
    console.log("fechafin ",fechafin);
    this._metricasService
        .GetGestionesHora(idcampania,fechainicio,fechafin)
        .subscribe((respuesta) => {
          if (respuesta.exito == 1) {
            this.listaGestionescont = respuesta.data.listaHoraAll;
            switch (Number(this.tipoMontos)) {
              case 1:
                this.listaGestionesdol = respuesta.data.listaHoraDolarVA;
                break;
              case 2:
                this.listaGestionesdol = respuesta.data.listaHoraDolarDV;
                break;
              case 3:
                this.listaGestionesdol = respuesta.data.listaHoraDolar;
                break;
              default:
                break;
            }
            this.listaGestionesper = respuesta.data.listaHoraPerc;
            console.log("this.listaGestionescont ",this.listaGestionescont);
            this.ListaXvalor(this.valor.present);
            this.addTotal();
            console.log("this.listaMostrar chart ",this.listaMostrar);
            this.ChartGestiones(this.listaMostrar);
          } else {
            this.vaciarListasGestiones();
          }
    });
  }

  ListaXvalor(present: any): void {
    switch (present) {
      case "N":
        this.listaMostrar = this.listaGestionescont;
        break;
      case "D":
        this.listaMostrar = this.listaGestionesdol;
        break;
      case "P":
        this.listaMostrar = this.listaGestionesper;
        break;

      default:
        break;
    }
    if (this.tipo.tiempo == "H") {
      this.listaMostrar.forEach(element => {
        if (element.hora > 11) {
          element.hora = element.hora + " pm"
        }else{
          element.hora = element.hora + " am"
        }
      });
    }
  }

  vaciarListasGestiones(): void {
    this.listaGestiones = null;
    this.listaGestionescont = null;
    this.listaGestionesdol = null;
    this.listaGestionesper = null;
    this.basicDataGestiones = null;
  }

  addTotal(): void {
    let totalasesores: number = 0;
    let totalgestiones: number = 0;
    let totalclientes: number = 0;
    let totaldirectos: number = 0;
    let totalefectivos: number = 0;
    let totalacuerdos: number = 0;
    let cont: number = 0;
    totalasesores = this.listaMostrar.length;
    for (const value of this.listaMostrar) {
      totalgestiones += Number(value.gestiones);
      totalclientes += Number(value.clientes);
      totaldirectos += Number(value.directos);
      totalefectivos += Number(value.efectivos);
      totalacuerdos += Number(value.acuerdos);
      cont++;
    }
    console.log("tiempo total ",this.tipo.tiempo);
    if (this.tipo.tiempo == "D") {
      const elemento = {
        asesor: "Total: "+ totalasesores,
        gestiones: totalgestiones,
        clientes: totalclientes,
        directos: totaldirectos,
        efectivos: totalefectivos,
        acuerdos: totalacuerdos,
      };
      this.total = [];
      this.total.push(elemento);
    } else {
      const elemento = {
        gestiones: this.roundToTwo(totalgestiones/cont),
        clientes: this.roundToTwo(totalclientes/cont),
        directos: this.roundToTwo(totaldirectos/cont),
        efectivos: this.roundToTwo(totalefectivos/cont),
        acuerdos: this.roundToTwo(totalacuerdos/cont),
      };
      this.total = [];
      this.total.push(elemento);
    }
  }

  GestionesXasesor(listaclientes: any){
    console.log(listaclientes.dataItem.idUsuario);
    this.listaGestionesxAsesor = this.listaGestiones.filter(
      (s) => s.idUsuario === listaclientes.dataItem.idUsuario);
    console.log("GestionesXasesor ", this.listaGestionesxAsesor);
  }

  roundToTwo(num: number) {    
    return num = Math.round((num + Number.EPSILON) * 100) / 100;
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.datoGrid = process(this.listaMostrar, this.state);
  }

  update(event: Event) {
    this.listaMostrar;
  }

  public tipo = {
    tiempo: null,
  };

  public valor = {
    present: null,
  };
  public valorec = {
    presentec: null,
  };

  cambiarListaDias(lista: any): void {
    console.log(lista.target.value);
    this.tipoDias = lista.target.value;
    if (this.tipoDias == 1 || this.tipoDias == 2) {
      this.mostrarGestiones(this.valorComboCampania);
    }
  }

  cambiarListaTipoMontosGestion(listat: any): void {
    console.log("TipoMontosGestion ",listat.target.value);
    this.tipoMontos = listat.target.value;
    if (this.valor.present == "D") {
      this.mostrarGestiones(this.valorComboCampania);
    }
  }

  showTime(){
    let myDate = new Date();
    this.hora = myDate.getHours()+":" +myDate.getMinutes();
    setTimeout("showTime()", 60000);  
  }

  private log(event: string, arg: any = null): void {
    this.events.push(`${event}`);
    console.log(arg);
  }

  MasFiltros(){
    console.log("Hola");
    let datafiltro = [];
    this.modalService.show(ModalFiltroIndicadoresGestionComponent, {
      initialState: {
        detalle: datafiltro,
        idcampania: this.valorComboCampania.id,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      class: "gray modal-sm",
    });
    console.log("datafiltro ",datafiltro);
    console.log("this.valorComboCampania.id ",this.valorComboCampania.id);
  }

  // ---------------------------------------ESTADOS CLIENTE---------------------------------------
  mostrarEstados(valor: CrmCampania): void {
    console.log("valor mostrarestados ",valor);
    this.mixtoseleccionado = false;
    this.params = this.activatedRoute.snapshot.params;
    // this.valorComboCampania = valor;
    if(valor.id !== undefined){
      this.idcampania = valor.id;
    }
    console.log("valor.id Estados ",this.idcampania);
    if(this.valorec.presentec == null){
      this.valorec.presentec = "N";
    }
    console.log("this.tipoDias ", this.tipoDias);
    console.log("fechainicio antes ",this.fechainicio);
    console.log("fechafin antes ",this.fechafin);
    switch (Number(this.tipoDias)) {
      case 1:
        this.fechainicio = null;
        this.fechafin = format(new Date(), "yyyy-MM-dd H:mm:s");
        console.log("fechainicio 1 ",this.fechainicio);
        console.log("fechafin 1 ",this.fechafin);
        break;
      case 2:
        this.fechainicio = format(new Date(), "yyyy-MM-dd 00:00:00");
        this.fechafin = format(new Date(), "yyyy-MM-dd H:mm:s");
        console.log("fechainicio 2",this.fechainicio);
        console.log("fechafin 2 ",this.fechafin);
        break;
      case 3:
        this.fechainicio = format(this.fechaidate, "yyyy-MM-dd H:mm:s");
        this.fechafin = format(this.fechafdate, "yyyy-MM-dd H:mm:s");
        console.log("fechainicio 3 ",this.fechainicio);
        break;
      default:
        break;
    }
    if (valor) {
      this._metricasService
        .GetEstadosCliente(this.idcampania,this.fechainicio,this.fechafin)
        .subscribe((respuesta) => {
          // console.log("Estados", respuesta.data);
          this.listaEstados = respuesta.data.listaEstados;
          this.listaEstadoscont = respuesta.data.tblprincipal;
          this.listaGetEstados = respuesta.data.listaEstados;
          // console.log("this.listaEstados ",this.listaEstados);
          // console.log("this.listaEstadoscont", this.listaEstadoscont);
          // this.listaGestionesdol = respuesta.data.resultsdolar;
          this.listaEstadosper = respuesta.data.tblMetricasPerc;
          this.listaEstadosdol = respuesta.data.tblMetricasDol;
          this.listaEstadostotal = respuesta.data.tblEstadosTotal;
          this.listaEstadostotaldol = respuesta.data.tblEstadosTotalDol;
          this.totalcantidad = respuesta.data.tblEstadosTotal.totalValor;
          this.totalmonto = respuesta.data.tblEstadosTotalDol.totalValor;
          this.listaAgentes = respuesta.data.tblAsesoresxestado;
          this.listaPerfilaciones = respuesta.data.tblEstadoPerfilaciones;
          this.listaEstadostotal.forEach(element => {
            this.totalcantidad = element.totalValor;
          });
          // console.log("this.listaEstadostotal ",this.listaEstadostotal);
          this.listaEstadostotaldol.forEach(element => {
            this.totalmonto = element.totalValor;
            element.totalValor = "$ "+element.totalValor;
          });
          console.log("this.valorec.presentec ",this.valor.present);
          console.log("this.totalmonto ",this.totalmonto);
          // console.log("this.totalcantidad ",this.totalcantidad);

          switch (this.valorec.presentec) {
            case "N":
              this.listaEstados = this.listaEstadoscont;
              this.listaEstadostotal.forEach(element => {
                this.positivos = element.positivos;
                this.negativos = element.negativos;
                this.positivosPorcent = element.positivosPorcent;
              });
              console.log("N ",this.listaEstados);
              break;
            case "D":
              if (this.tipoMontos == 2) {
                this.listaEstadosdol = respuesta.data.tblMetricasDolDV;
                this.listaEstadostotaldol = respuesta.data.tblEstadosTotalDolDV;
              }
              this.listaEstados = this.listaEstadosdol;
              console.log("this.listaEstadostotaldol ",this.listaEstadostotaldol);
              this.listaEstadostotaldol.forEach(element => {
                this.positivos = "$ "+element.positivos;
                this.negativos = "$ "+element.negativos;
                this.positivosPorcent = element.positivosPorcent;
              });
              this.listaEstadostotal = this.listaEstadostotaldol;
              console.log("D ",this.listaEstados);
              break;
            case "P":
              console.log("P ",this.listaEstados);
              // this.listaEstadosper.forEach(element => {
              //   console.log(element.valor);
              //   element.valor = "% "+element.valor;
              // });
              this.listaEstados = this.listaEstadosper;
              console.log("estados p per ",this.listaEstadosper); 
              console.log("estados p ",this.listaEstados);  
              break;
            case "M":
              this.mixtoseleccionado = true;
              this.listaEstados = this.listaGestionesper;
              console.log("M ",this.listaEstados);
              break;
            default:
              break;
          }
          this.listaEstados.forEach(element => {
            if(element.idTipoGestion==1){
              element.idTipoGestion = "+";
            }else{
              element.idTipoGestion = "-";
            }
          });
          this.ChartEstados(this.listaEstados);
        });
    } else {
      this.listaEstados = null;
      this.listaEstadoscont = null;
      this.listaEstadosdol = null;
      this.listaEstadosper = null;
    }
  }

  onSelectedKeysChange(event, id: number) {
    this.estadoseleccionadotbl.emit(this.mySelection);
    console.log("cambios tabla principal evento", event);
    console.log(
      "cambios tabla principal evento mi seleccion",
      this.mySelection
    );
    
    if (this.mySelection.length>0) {
      this.estadoseleccionado = true;
    } else {
      this.estadoseleccionado = false;
    }
    let totalms = this.mySelection.length-1;
    console.log(totalms);
    let seleccionado = this.mySelection[totalms];
    this.totaltab = seleccionado.valor;
    this.Agentes(seleccionado.id);
    this.Perfilaciones(seleccionado.descripcion);
  }
  public selectedCallback = (args) => {
    return args.dataItem;
  };

  cambiarListaDiasEst(lista: any): void {
    console.log(lista.target.value);
    this.tipoDias = lista.target.value;
    if (this.tipoDias == 1 || this.tipoDias == 2) {
      this.mostrarEstados(this.valorComboCampania);
    }
  }

  cambiarListaTipoMontosEst(listat: any): void {
    console.log(listat.target.value);
    this.tipoMontos = listat.target.value;
    if (this.valorec.presentec == "D") {
      this.mostrarEstados(this.valorComboCampania);
    }
  }

  handleChange(e) {
    this.indexTabDerecho = e.index;
  }

//------------------------ DIALOG MÁS FILTROS ESTADOS CLIENTE---------------

  selectedContacto: any = null;
  selectedFiltro: any = null;

  DETALLECAMPANIA: any[] = [];
  FILTROS: any[] = [] ;
  VALORES = [];
  FiltroFilter= [];
  ValorFilter= [];
  selectedValor: Valor[];

  showDialog(position: string) {
    this.positionDialog = position;
    this.displayPosition = true;
    console.log("-----------HOLA-----------");
    this.master = this.valorComboCampania.id;
    // this.MostrarAclTipoContacto();
  }

  closeDialog() {
    // this.displayPosition = false;
  }

  // MostrarAclTipoContacto(): void {
  //   this._tipoContactoService.GetAclTipoContacto().subscribe((respuesta) => {
  //     this.tipoDeContacto = respuesta.data;
  //   });
  // }

  // mostrarDetalleCuentaCampania(): void {
  //   let campo: string = "";
  //   this._metricasService.mostrarDetalleCuentaCampania(this.valorComboCampania.id).subscribe((respuesta) => {
  //     this.DETALLECAMPANIA = respuesta.data;
  //     this.FiltroFilter = distinctItem(respuesta.data,"campo");
  //     for(const value of this.FiltroFilter){
  //       const elemento = {
  //         campo : value,
  //       }
  //       this.FILTROS.push(elemento);
  //     };      
  //   });
  // }

  // FiltraValores(event) {
  //   console.log(event.value) ;
  //   if (event.value) {
  //     this.VALORES = [];
  //     this.ValorFilter = this.DETALLECAMPANIA.filter(
  //       (x) => x.campo === event.value.campo
  //     );
  //     this.ValorFilter = distinctItem(this.ValorFilter,"valor");
  //     for(const value of this.ValorFilter){
  //       const elemento = {
  //         campo : value,
  //       }
  //       this.VALORES.push(elemento);
  //     };
  //   }
  // }

  SeleccionaValor(event){
    console.log("event valor ", event.value);
  }

}
