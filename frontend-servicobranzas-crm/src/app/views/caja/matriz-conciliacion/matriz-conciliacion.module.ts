import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatrizConciliacionRoutingModule } from './matriz-conciliacion-routing.module';
import { AcuerdosComponent } from './acuerdos/acuerdos.component';
import { PagosComponent } from './pagos/pagos.component';
import { ModalEliminarPagoComponent } from './pagos/modal-eliminar-pago/modal-eliminar-pago.component';


@NgModule({
  declarations: [AcuerdosComponent, PagosComponent, ModalEliminarPagoComponent],
  imports: [
    CommonModule,
    MatrizConciliacionRoutingModule
  ]
})
export class MatrizConciliacionModule { }
