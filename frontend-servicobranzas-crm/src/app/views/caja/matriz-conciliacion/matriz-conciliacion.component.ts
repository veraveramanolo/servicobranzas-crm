import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { CrmCartera } from "../../../models/CrmCartera";
import { CrmCampania } from "../../../models/CrmCampania";
import { DataStateChangeEvent, GridDataResult, FilterService} from "@progress/kendo-angular-grid";
import { isNullOrUndefined } from "util";
import { ModeloRespuesta } from "../../../models/ModeloRespuesta.models";
import { ConsultauserService } from "../../../services/consultauser.service";
import { CrmCarteraService } from "../../../services/crm-cartera.service";
import { CampaniaService } from "../../../services/campania.service";
import { AcuerdosComponent } from "./acuerdos/acuerdos.component";
import { PagosComponent } from './pagos/pagos.component';

@Component({
  selector: 'app-matriz-conciliacion',
  templateUrl: './matriz-conciliacion.component.html',
  styleUrls: ['./matriz-conciliacion.component.css']
})
export class MatrizConciliacionComponent implements OnInit {

  public position: "top" | "bottom" | "both" = "top";
  public InfoCartera: CrmCartera[] = [];
  public InfoCampania: CrmCampania[] = [];
  public listaCartera: CrmCartera[] = [];
  public listaCampania: CrmCampania[] = [];
  public listaCampaniafiltrada: CrmCampania[] = [];
  public listaCampaniaEmergente: CrmCampania[] = [];
  public datoGrid: GridDataResult;
  public valorComboCampania: CrmCampania = null;
  admin: number;
  idcampania: number;

  @ViewChild(AcuerdosComponent) hijoAcuerdos: AcuerdosComponent;
  @ViewChild(PagosComponent) hijoPagos: PagosComponent;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private serviceUsuario: ConsultauserService,
    private serviceCrmCartera: CrmCarteraService,
    private serviceCarmpania: CampaniaService,
  ) { }

  async ngOnInit() {
    await this.GetCarteras();
    await this.GetConsultaCampania(); 
  }

  async Usuario(id: number) {
    let resp: ModeloRespuesta = null;
    resp = await this.serviceUsuario.Usuario(id);
    let user = resp.data;
    this.admin = user.idRol;
    console.log(this.admin);
  }

  async GetCarteras() {
    this.listaCartera = [];
    this.InfoCartera = [];

    let resp: ModeloRespuesta = null;
    resp = await this.serviceCrmCartera.GetCarteras();

    this.InfoCartera = resp.data;
    this.listaCartera = this.InfoCartera;
    console.log("listaCartera",this.listaCartera);
  }

  async GetConsultaCampania() {
    this.listaCampania = [];
    this.InfoCampania = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetConsultaCampania();

    this.InfoCampania = resp.data;
    this.listaCampania = this.InfoCampania;
    console.log("listaCampania", this.listaCampania);
  }

  FiltroComboCartera(valor) {
    this.listaCartera = this.InfoCartera.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }

  FiltroComboCampania(valor) {
    console.log("FiltroComboCampania(valor) ",valor);
    this.listaCampaniafiltrada = this.listaCampaniaEmergente.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
    console.log("this.listaCampaniafiltrada ",this.listaCampaniafiltrada);
    // this.mostrarEstados(this.valorComboCampania);
  }

  EventoComboCartera(valor: CrmCartera) {
    this.datoGrid = undefined;
    this.valorComboCampania = undefined;
    if (valor) {
      this.listaCampaniafiltrada = [];
      this.listaCampaniafiltrada = this.listaCampania.filter(
        (x) => x.idCartera === valor.id
      );
      this.listaCampaniaEmergente = this.listaCampaniafiltrada;
    } else {
      this.listaCampaniafiltrada = [];
    }
  }

  EventoComboCampania(valor: CrmCampania) {
    console.log("EventoComboCampania(valor ",valor);
    this.datoGrid = undefined;
    if (!isNullOrUndefined(valor)) {
      setTimeout(() => {
        this.valorComboCampania = valor;
        this.idcampania = valor.id;
        console.log("this.idcampania cbb", this.idcampania);
        this.hijoAcuerdos.LlamadoMatriz(this.idcampania);
        this.hijoPagos.MostrarPagosXasignacion(this.idcampania);
        // this.mostrarGestiones(this.valorComboCampania);
        // this.mostrarEstados(this.valorComboCampania);
      }, 200);
    }
  }
}
