import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CajaComponent } from './caja/caja.component';
import { MatrizConciliacionComponent } from './matriz-conciliacion/matriz-conciliacion.component';


const routes: Routes = [
  {
    path: "",
    data: {title : "Caja"},
    children: [
      {
        path: "",
        redirectTo: "caja"
      },
      {
        path: "caja/caja",
        component: CajaComponent,
        data: {title: "Caja"}
      },
      {
        path: "caja/matriz-conciliacion",
        component: MatrizConciliacionComponent,
        data: {title: "Efectividad"}
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CajaRoutingModule { }
