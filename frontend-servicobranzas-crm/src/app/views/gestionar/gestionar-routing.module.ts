import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GestionNormalComponent } from "./gestion-normal/gestion-normal.component";
import { GestionByIdComponent } from "./gestion-normal/gestion-id.component";
import { TareasComponent } from "./tareas/tareas.component";

const routes: Routes = [
  {
    path: "",
    data: { titulo: "Gestionar" },
    children: [
      {
        path: "",
        redirectTo: "gestionar",
      },
      {
        path: "gestionar/gestion-normal",
        component: GestionNormalComponent,
        data: { titulo: "Gestion Cobranzas" },
      },
      {
        path: "gestionar/tareas",
        component: TareasComponent,
        data: { titulo: "Tareas" },
      },
      {
        path: "gestionar/gestionid/:GestionID/:ProductoID/:Fila",
        component: GestionByIdComponent,
        data: { titulo: "Gestion Cobranzas" },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionarRoutingModule {}
