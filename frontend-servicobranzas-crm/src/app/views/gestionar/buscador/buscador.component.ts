import { Component, OnInit } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { BuscadorModalComponent } from "../buscador-modal/buscador-modal.component";

@Component({
  selector: "app-buscador",
  templateUrl: "./buscador.component.html",
  styleUrls: ["./buscador.component.scss"],
})
export class BuscadorComponent implements OnInit {
  public items: any;
  public mostrarInput: boolean;
  public opciones: string[] = ["Identificacion", "Nombre", "Email", "Telefono"];
  public texto: string = `Buscar  ${this.opciones[0]}...`;
  public tipoBusqueda: string = this.opciones[0];
  public cajaTexto: string = "";
  constructor(private readonly modalService: BsModalService) {}

  ngOnInit(): void {
    this.asignarItem();
  }

  private asignarItem(): void {
    this.items = [
      {
        label: this.opciones[0],
        icon: "pi pi-id-card",
        command: () => {
          this.asignarPlace(this.opciones[0]);
        },
      },
      {
        label: this.opciones[1],
        icon: "pi pi-chevron-circle-up",
        command: () => {
          this.asignarPlace(this.opciones[1]);
        },
      },
      {
        label: this.opciones[2],
        icon: "pi pi-envelope",
        command: () => {
          this.asignarPlace(this.opciones[2]);
        },
      },
      {
        label: this.opciones[3],
        icon: "pi pi-mobile",
        command: () => {
          this.asignarPlace(this.opciones[3]);
        },
      },
      // { label: this.opciones[2], icon: "pi pi-info", url: "http://angular.io" },
      // { separator: true },
      // { label: "Setup", icon: "pi pi-cog", routerLink: ["/setup"] },
    ];
  }

  asignarPlace(texto: string): void {
    this.tipoBusqueda = texto;
    this.texto = `Buscar  ${texto}...`;
    // this.cajaTexto = "";
  }

  buscar(): void {
    if (this.mostrarInput) {
      if (this.cajaTexto != "") {
        console.log(this.cajaTexto);
        this.openModal();
      } else {
        this.mostrarInput = false;
      }
    } else {
      this.mostrarInput = true;
    }
  }

  onSearchKeyUp(texto: string): void {
    this.cajaTexto = texto;
  }

  openModal(): void {
    this.modalService.show(BuscadorModalComponent, {
      initialState: {
        tipoBusqueda: this.tipoBusqueda,
        cajaTexto: this.cajaTexto,
      },
      backdrop: "static",
      class: "gray modal-lg",
    });
  }
}
