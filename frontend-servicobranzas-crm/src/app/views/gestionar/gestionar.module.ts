import { LOCALE_ID, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { GestionarRoutingModule } from "./gestionar-routing.module";
import { GestionNormalComponent } from "./gestion-normal/gestion-normal.component";
import { TareasComponent } from "./tareas/tareas.component";
import { GestionByIdComponent } from "./gestion-normal/gestion-id.component";

import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { GridModule } from "@progress/kendo-angular-grid";
import { ListViewModule } from "@progress/kendo-angular-listview";
import {
  ComboBoxModule,
  MultiSelectModule,
} from "@progress/kendo-angular-dropdowns";

import { DropDownListModule } from "@progress/kendo-angular-dropdowns";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { LayoutModule } from "@progress/kendo-angular-layout";
import { IntlModule } from "@progress/kendo-angular-intl";

import "@progress/kendo-angular-intl/locales/de/all";
import "@progress/kendo-angular-intl/locales/es/all";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { LabelModule } from "@progress/kendo-angular-label";
//import { BrowserModule } from "@angular/platform-browser";
//import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { TabsModule } from "ngx-bootstrap/tabs";
import { ModalModule } from "ngx-bootstrap/modal";
import { ModalGestionIdAlertComponent } from "./modal-gestion-id-alert/modal-gestion-id-alert.component";
import { ModalActivarDesactivarComponent } from "./modal-activar-desactivar/modal-activar-desactivar.component";
import { ModalArchivoAdjuntoComponent } from "./modal-archivo-adjunto/modal-archivo-adjunto.component";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ButtonModule } from "primeng/button";
import { CardModule } from "primeng/card";
import { TabViewModule } from "primeng/tabview";
import { TooltipModule } from "primeng/tooltip";
import { SplitterModule } from "primeng/splitter";
import { TableModule } from "primeng/table";
import { EstadoPipe } from "./pipes/estado.pipe";
import { ModalSmsComponent } from "./modal-sms/modal-sms.component";
import { ModalEmailComponent } from "./modal-email/modal-email.component";
import { MostrarDetalleProductoComponent } from "./mostrar-detalle-producto/mostrar-detalle-producto.component";
import { BuscadorModalComponent } from "./buscador-modal/buscador-modal.component";
import { BuscadorComponent } from "./buscador/buscador.component";
import { SplitButtonModule } from "primeng/splitbutton";
import { ModalVerInfoGestionidComponent } from "./modal-ver-info-gestionid/modal-ver-info-gestionid.component";

//redux
import { CalendarModule } from "primeng/calendar";
import { PopupModule } from "@progress/kendo-angular-popup";

import * as fromReducers from "./store/reducers";
import { StoreModule } from "@ngrx/store";
import { CambioNombreTotalCampaniaPipe } from "./pipes/cambio-nombre-total-campania.pipe";
import { ModalGuardarFiltroGestionComponent } from './modal-guardar-filtro-gestion/modal-guardar-filtro-gestion.component';
import { ModalVerInfoClienteComponent } from './modal-ver-info-cliente/modal-ver-info-cliente.component';
import { PDFExportModule } from "@progress/kendo-angular-pdf-export";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";

@NgModule({
  declarations: [
    GestionNormalComponent,
    TareasComponent,
    GestionByIdComponent,
    ModalGestionIdAlertComponent,
    ModalActivarDesactivarComponent,
    ModalArchivoAdjuntoComponent,
    EstadoPipe,
    ModalSmsComponent,
    ModalEmailComponent,
    MostrarDetalleProductoComponent,
    BuscadorModalComponent,
    BuscadorComponent,
    ModalVerInfoGestionidComponent,
    CambioNombreTotalCampaniaPipe,
    ModalGuardarFiltroGestionComponent,
    ModalVerInfoClienteComponent,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "es-ES" }],
  entryComponents: [
    ModalGestionIdAlertComponent,
    ModalActivarDesactivarComponent,
    ModalArchivoAdjuntoComponent,
    ModalSmsComponent,
    ModalEmailComponent,
    MostrarDetalleProductoComponent,
    BuscadorModalComponent,
    ModalVerInfoGestionidComponent,
    ModalGuardarFiltroGestionComponent,
    ModalVerInfoClienteComponent,
  ],
  imports: [
    CommonModule,
    GestionarRoutingModule,
    FormsModule,
    ChartsModule,
    BsDropdownModule,
    GridModule,
    ButtonsModule,
    ListViewModule,
    MultiSelectModule,
    DropDownListModule,
    InputsModule,
    LayoutModule,
    DateInputsModule,
    LabelModule,
    TabsModule.forRoot(),
    ButtonModule,
    CardModule,
    TabViewModule,
    TooltipModule,
    SplitterModule,
    TableModule,
    CalendarModule,
    IntlModule,
    //BrowserModule,
    //BrowserAnimationsModule,
    ReactiveFormsModule,
    ComboBoxModule,
    MatTooltipModule,
    ModalModule.forRoot(),
    SplitButtonModule,
    PopupModule,
    PDFExportModule,
    DropDownsModule,
    // StoreModule.forFeature("gestion-normal-module", fromReducers.reducers, {
    //   metaReducers: fromReducers.metaReducers,
    // }),
  ],
})
export class GestionarModule {}
