import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { CampaniaService } from "../../../services/campania.service";
import { ModalVerInfoGestionidComponent } from "../modal-ver-info-gestionid/modal-ver-info-gestionid.component";

@Component({
  selector: "app-buscador-modal",
  templateUrl: "./buscador-modal.component.html",
  styleUrls: ["./buscador-modal.component.scss"],
})
export class BuscadorModalComponent implements OnInit {
  public clienteSeleccionado: boolean = false;
  public cliente: any;
  public infoBusqueda: any = [];
  public tipoBusqueda: string;
  public cajaTexto: string;
  public opciones: string[] = ["Identificacion", "Nombre", "Email", "Telefono"];
  public idnav: number;
  constructor(
    public modalRef: BsModalRef,
    private router: Router,
    private readonly modalService: BsModalService,
    private readonly campaniaService: CampaniaService
  ) {}

  ngOnInit(): void {
    this.mostrarLabusqueda();
  }

  seleccionarBusqueda(fila: any): void {
    console.log("cliente seleccionado", fila);
    this.clienteSeleccionado = true;
    this.cliente = fila.dataItem;
    this.campaniaService.mostrarIdNav(this.cliente.idcuentacampania, this.cliente.idproducto)
    .subscribe((data) => {
      console.log("idnav", data);
      this.idnav = data.data;
    });
  }
  mostrarLabusqueda(): void {
    const vacio: string = "";
    switch (this.tipoBusqueda) {
      case this.opciones[0]:
        this.mostrarPorIdentificacionNombre(this.cajaTexto, vacio);
        break;
      case this.opciones[1]:
        this.mostrarPorIdentificacionNombre(vacio, this.cajaTexto);
        break;
      case this.opciones[2]:
        this.mostrarPorEmail(this.cajaTexto);
        break;
      case this.opciones[3]:
        this.mostrarPorTelefono(this.cajaTexto);
        break;

      default:
        break;
    }
  }

  mostrarPorTelefono(telefono: string): void {
    this.campaniaService
      .busquedaPorTelefono(telefono)
      .subscribe((respuesta) => {
        console.log("busqueda por telefono", respuesta.data);
        this.infoBusqueda = respuesta.data;
      });
  }
  mostrarPorIdentificacionNombre(
    identificacion: string = null,
    nombre: string = null
  ): void {
    console.log("identificacion", identificacion);
    console.log("nombre", nombre);
    this.campaniaService
      .busquedaPorIdNombre(identificacion, nombre)
      .subscribe((respuesta) => {
        console.log("busqueda", respuesta);
        this.infoBusqueda = respuesta.data;
      });
  }
  mostrarPorEmail(email: string): void {
    this.campaniaService
      .busquedaPorEmail(email)
      .subscribe((respuesta) => (this.infoBusqueda = respuesta.data));
  }

  gestionarCliente(): void {
    this.modalService.setDismissReason("ok");
    this.modalRef.hide();
    this.router.navigate([
      `/gestionar/gestionar/gestionid/${this.cliente.idcuentacampania}/${this.cliente.idproducto}/${this.idnav}`,
    ]);

    // this.router
    //   .navigateByUrl("/", { skipLocationChange: true })
    //   .then(() =>
    //     this.router.navigate([`/gestionar/gestionar/gestionid/15/6?fresh=1`])
    //   );
    // window.location.href = window.location.href;

    // this.router.navigateByUrl("/DummyComponent");
    // this.router.navigate([`gestionar/gestionar/gestionid/15/6`]);
  }

  verInformacion(): void {
    this.modalService.setDismissReason("ok");
    this.modalRef.hide();
    this.modalService.show(ModalVerInfoGestionidComponent, {
      initialState: {
        gestionGlobal: Number(this.cliente.idcuentacampania),
      },
      backdrop: "static",
      class: "gray modal-lg",
    });
  }
}
