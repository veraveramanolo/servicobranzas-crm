export interface Comandato {
    asesor: string;
    numberID: string;
    nombreCli: string;
    fechaUltGest: Date;
    EstadoID: number;
    EstadoName: string;
    unitPrice: number;
    unitsInStock: number;
    discontinued: boolean;
}

export const comandato: any[] = [
    {
        numberID: '097700',
        nombreCli: 'Irina Zoraya',
        fechaUltGest: new Date(2020,23,12),
        Estado: {
            EstadoID: 1,
            EstadoName: 'Abono',
        },
        unitPrice: 18,
        unitsInStock: 39,
        discontinued: false,
        asesor: 'Servico1'
    },
    {
        numberID: '093000',
        nombreCli: 'Zoraya Irina',
        fechaUltGest: new Date().toLocaleDateString(),
        Estado: {
            EstadoID: 1,
            EstadoName: 'Promesa de pago',
        },
        unitPrice: 17.45,
        unitsInStock: 29,
        discontinued: false,
        asesor: 'Servico2'
    },
    {
        numberID: '096500',
        nombreCli: 'Lopez Cordova',
        fechaUltGest: new Date().toLocaleDateString(),
        Estado: {
            EstadoID: 1,
            EstadoName: 'Sin contactar',
        },
        unitPrice: 13,
        unitsInStock: 32,
        discontinued: false,
        asesor: 'Servico3'
    }
];

export interface Deprati {
    asesor: string;
    numberID: string;
    nombreCli: string;
    unitPrice: number;
    unitsInStock: number;
    discontinued: boolean;
}

export const deprati: any[] = [
    {
        numberID: '09000000',
        nombreCli: 'Ingrid Lilibeth',
        unitPrice: 18,
        unitsInStock: 39,
        discontinued: false,
        asesor: 'Servico3'
    },
    {
        numberID: '0980000',
        nombreCli: 'Lilibeth Ingrid',
        unitPrice: 17.45,
        unitsInStock: 29,
        discontinued: false,
        asesor: 'Servico4'
    },
    {
        numberID: '0904000',
        nombreCli: 'Tacuri Lopez',
        unitPrice: 13,
        unitsInStock: 32,
        discontinued: false,
        asesor: 'Servico1'
    }
];

export const buttons: any[] = [
    /*{
        text: 'Item1',
        items: [{ text: 'Item1.1' }, { text: 'Item1.2', items: [{ text: 'Item1.2.1' }, { text: 'Item1.2.2' }] }]
    },*/
    { text: 'Comandato', icon: 'k-i-clock', color: '#f0c505' },
    { text: 'DePrati', icon: 'k-i-check-circle', color: '#10b507' }
];

export const estados = [{
    "EstadoName": "TOTAL CAMPAÑAS",
    "EstadoTotal": 0
},{
    "EstadoID": 1,
    "EstadoName": "Mensaje con tercero",
    "EstadoTotal": 0
}, {
    "EstadoID": 2,
    "EstadoName": "Sin Contactar",
    "EstadoTotal": 0
}, {
    "EstadoID": 3,
    "EstadoName": "Abono",
    "EstadoTotal": 0
}
];

export const alertas = [{
    "AlertaEstado": "Sin Gestión",
    "AlertaName": "Entre 0-0",
    "AlertaTotal": 0
}, {
    "AlertaEstado": "Sin Gestión",
    "AlertaName": "Entre 8-14",
    "AlertaTotal": 0
}, {
    "AlertaEstado": "Sin Gestión",
    "AlertaName": "Mayor a 15",
    "AlertaTotal": 0
}
];

export interface Clientes {
    categoryName: string;
    productID: number;
    productName: string;
    unitPrice: number;
    unitsInStock: number;
    discontinued: boolean;
}

export const clientes: any[] = [
    {
        productID: 1,
        productName: 'Irina',
        unitPrice: 200,
        unitsInStock: 100,
        discontinued: false,
        categoryName: 'Alta'
    },
    {
        productID: 16,
        productName: 'Lopez',
        unitPrice: 80,
        unitsInStock: 20,
        discontinued: false,
        categoryName: 'Baja'
    },
    {
        productID: 77,
        productName: 'Cordova',
        unitPrice: 130,
        unitsInStock: 20,
        discontinued: false,
        categoryName: 'Media'
    }
];