import { Component, OnInit, OnDestroy } from "@angular/core";
import { sampleProducts, visitar, email, personaContacto } from "../products";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { ArbolGestionServiceService } from "../../../services/arbol-gestion.service";
import { CampaniaService } from "../../../services/campania.service";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";

import { format } from "date-fns";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { ModalGestionIdAlertComponent } from "../modal-gestion-id-alert/modal-gestion-id-alert.component";
import { ModalActivarDesactivarComponent } from "../modal-activar-desactivar/modal-activar-desactivar.component";
import { ModalArchivoAdjuntoComponent } from "../modal-archivo-adjunto/modal-archivo-adjunto.component";
import { depto, tipo } from "../fake";
import { DepartamentoService } from "../../../services/departamento.service";
import { CiudadService } from "../../../services/ciudad.service";
import { DistritoService } from "../../../services/distrito.service";
import { InfoContactoService } from "../../../services/info-contacto.service";
import { UbicacionService } from "../../../services/ubicacion.service";
import Swal from "sweetalert2";
import { EmailService } from "../../../services/email.service";
import { ContactoService } from "../../../services/contacto.service";
import { DinomiService } from "../../../services/dinomi.service";
import { ModalSmsComponent } from "../modal-sms/modal-sms.component";
import { ModalEmailComponent } from "../modal-email/modal-email.component";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { MostrarDetalleProductoComponent } from "../mostrar-detalle-producto/mostrar-detalle-producto.component";
export enum tipoBoton {
  FijoMovil = "fijo/movil",
  Direcciones = "direcciones",
  Email = "email",
  PersonasContacto = "personaContacto",
}

@Component({
  selector: "app-modal-ver-info-gestionid",
  templateUrl: "./modal-ver-info-gestionid.component.html",
  styleUrls: ["./modal-ver-info-gestionid.component.css"],
})
export class ModalVerInfoGestionidComponent implements OnInit, OnDestroy {
  gestionGlobal: any;
  arbolSelecciondo: string;
  private stop$ = new Subject<void>();
  tipoSeleccionado: string = "";
  extencion: string;
  tipoGestion: string = "LLamada Out";
  detalleGestion: string;
  idArbolDecision: number;
  idCuentasCampaniaProductos: number;
  idTelefono: number;
  TIPO: Array<{ id: number; descripcion: string }> = tipo;
  DEPTO: any[] = [];
  CIUDAD: [] = [];
  DISTRITO: [] = [];
  ZONA: [] = [];
  AREA: [] = [];
  RELACION: [] = [];

  contacto: any;
  ubicacion: any;
  email: any;
  listaContacto: any;

  infoContacto: any[] = [];
  ubicaciones: any[] = [];
  emails: any[] = [];
  listaContactos: any[] = [];

  tipoMantenimiento: string;
  formTelefono: FormGroup;
  formTelefonoEditar: FormGroup;
  formTelefonoVer: FormGroup;

  formUbicacion: FormGroup;
  formUbicacionEditar: FormGroup;

  formEmail: FormGroup;
  formEmailEditar: FormGroup;

  formContacto: FormGroup;
  formContactoEditar: FormGroup;

  tipoDeGestion: any[] = [
    { id: 1, texto: "LLamada Out" },
    { id: 2, texto: "LLamada In" },
  ];
  mostrarForm = false;
  public tipoResidencia: string[] = ["RESIDENCIA", "TRABAJO", "MIXTRO", "OTRO"];
  public comboBoxValue: string;
  public pagos: any[] = [];
  public acuerdos: any[] = [];
  public productos: any[] = [];
  public historicoCliente: any[] = [];
  public detalleCliente = null;
  public nombreClinete: string;
  public cedula: string;
  tipobot = tipoBoton;
  tipo: string = tipoBoton.FijoMovil;
  botones = [];
  botonbase = [];
  anterior = [];
  subnivel = [];
  descrip = [];
  botonSeleccionado: string = tipoBoton.FijoMovil;
  public otrasCarteras: any[] = [];

  mostarBotonEditar = false;
  mostrarBotonStop = true;
  mostarBotonExtra = true;

  params: any;

  constructor(
    private readonly _arbolGestionService: ArbolGestionServiceService,
    private readonly _campaniaService: CampaniaService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly modalService: BsModalService,
    private readonly fb: FormBuilder,
    private readonly _departamentoService: DepartamentoService,
    private readonly _ciudadService: CiudadService,
    private readonly _distritoService: DistritoService,
    private readonly _infoContactoService: InfoContactoService,
    private readonly _ubicacionService: UbicacionService,
    private readonly _emailService: EmailService,
    private readonly _contactosService: ContactoService,
    private readonly router: Router,
    private readonly _dinomiService: DinomiService,
    public modalRef: BsModalRef
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        // trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        // if you need to scroll back to top, here is the right place
        window.scrollTo(0, 0);
      }
    });
    this.formTelefonoEditar = this.fb.group({
      telefono: [""],
      tipo: [""],
      depto: [""],
      ciudad: [""],
      distrito: [""],
      ciudadb: [""],
      info2: [""],
      contactos: [""],
    });
  }
  ngOnDestroy(): void {
    this.stop();
  }

  ngOnInit() {
    this.extencion = localStorage.getItem("extencion");
    this.fromTelefono();
    this.nombreClinete = localStorage.getItem("nombreCliente");
    this.cedula = localStorage.getItem("cedula");
    // this.botonbase= botones.data;
    // this.botones= botones.data;
    this.mostrarDetalleCliente();
    this.mostrarTelefono();
    this.mostrarHistoricoCliente();

    this.PagoCuentaCampania();
    this.AcuerdosPagoCuentaCampania();
    this.ProductoCuentaCampania();
    this.mostrarDepartamentos();
    this.mostratInfoContacto();
    this.mostrarUbicaciones();
    this.formUbicacionIniciar();
    this.mostrarArea();
    this.mostrarZonas();

    this.mostrarEmails();

    this.mostrarContactos();
    this.mostrarRelacion();

    this.cambiosModal();
  }

  editarEmail(event: any): void {
    event.preventDefault();
    const email = this.formEmailEditar.value;
    // this.params = this.activatedRoute.snapshot.params;
    email.idCuenta = Number(this.detalleCliente.idCuenta);
    email.id = this.email.idEmail;
    this._emailService.editarEmail(email).subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.mostrarEmails();
        Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: `${respuesta.mensage}`,
        });
      }
      // console.log("respuesta", respuesta);
    });
    this.formEmail.reset();
  }

  guardarEmail(event: any): void {
    event.preventDefault();
    const email = this.formEmail.value;
    // this.params = this.activatedRoute.snapshot.params;
    email.idCuenta = Number(this.detalleCliente.idCuenta);
    this._emailService.guardarEmail(email).subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.mostrarEmails();
        Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: `${respuesta.mensage}`,
        });
      }
      // console.log("respuesta", respuesta);
    });
    this.formEmail.reset();
  }

  private formEmailIniciar(): void {
    this.formEmail = this.fb.group({
      email: [""],
      info: [""],
    });
  }

  private formEmailEditarIniciar(): void {
    this.formEmailEditar = this.fb.group({
      email: [""],
      info: [""],
    });
  }

  seleccionarEmail(email: any): void {
    if (email.dataItem.estado === "A") {
      this.tipoMantenimiento = "ver";
      this.tipo = this.tipobot.Email;
      this.habilitarOpciones();
      console.log(email.dataItem);
      this.email = email.dataItem;
    }
  }

  mostrarEmails(): void {
    this._emailService
      .mostrarEmail(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log(respuesta.data);
        this.emails = respuesta.data;
      });
  }

  mostrarZonas(): void {
    this._ubicacionService.mostrarZona().subscribe((respuesta) => {
      this.ZONA = respuesta.data;
    });
  }
  mostrarArea(): void {
    this._ubicacionService.mostrarArea().subscribe((respuesta) => {
      this.AREA = respuesta.data;
    });
  }

  mostrarRelacion(): void {
    this._contactosService.mostrarRelacion().subscribe((respuesta) => {
      console.log("RELACION", respuesta);
      this.RELACION = respuesta.data;
    });
  }

  private formUbicacionIniciar(): void {
    this.formUbicacion = this.fb.group({
      direccion: [""],
      tipo: [""],
      depto: [""],
      ciudad: [""],
      distrito: [""],
      ciudadb: [""],
      barrio: [""],
      area: [""],
      zona: [""],
      contactos: [""],
    });
  }

  private formUbicacionEditarIniciar(): void {
    this.formUbicacionEditar = this.fb.group({
      direccion: [""],
      tipo: [""],
      depto: [""],
      ciudad: [""],
      distrito: [""],
      ciudadb: [""],
      barrio: [""],
      area: [""],
      zona: [""],
      contactos: [""],
    });
  }

  private formContactoIniciar(): void {
    this.formContacto = this.fb.group({
      nombre: [""],
      documento: [""],
      idRelacion: [""],
      observacion: [""],
    });
  }

  private formContactoEditarIniciar(): void {
    this.formContactoEditar = this.fb.group({
      nombre: [""],
      documento: [""],
      idRelacion: [""],
      observacion: [""],
    });
  }

  guardarTelefonoCuenta(event: any): void {
    event.preventDefault();
    const telefono = this.formTelefono.value;
    const validacion = this.validacionTelefono(telefono.telefono);
    // this.params = this.activatedRoute.snapshot.params;
    if (validacion.status) {
      this._infoContactoService
        .guardarTelefonoContacto(this.detalleCliente.idCuenta, telefono)
        .subscribe((respuesta) => {
          if (respuesta.exito === 1) {
            this.mostratInfoContacto();
            Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
          } else {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: `${respuesta.mensage}`,
            });
          }
          console.log("respuesta", respuesta);
        });
      this.formTelefono.reset();
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: `${validacion.message}`,
      });
    }
  }

  validacionTelefono(telefono: string): { status: boolean; message: string } {
    let salida = {
      status: true,
      message: "correcto!",
    };
    if (
      this.tipoSeleccionado === "MOVIL PERSONAL" ||
      this.tipoSeleccionado === "MOVIL OTRO"
    ) {
      const digitos = telefono.slice(0, 2);
      if (telefono.length != 10 || digitos != "09") {
        salida.status = false;
        salida.message = "El movil debe tener 10 digitos y empezar con 09!";
      }
    } else if (this.tipoSeleccionado === "RECIDENCIA") {
      if (telefono.length != 9) {
        salida.status = false;
        salida.message = "El telefono recidencial debe tener 9 digitos!";
      }
    }
    return salida;
  }

  guardarDireccionCuenta(event: any): void {
    event.preventDefault();
    const direccion = this.formUbicacion.value;
    console.log("direccion ts", direccion);
    // this.params = this.activatedRoute.snapshot.params;
    this._ubicacionService
      .guardarDireccion(this.detalleCliente.idCuenta, direccion)
      .subscribe((respuesta) => {
        if (respuesta.exito === 1) {
          this.mostrarUbicaciones();
          this.formUbicacion.reset();
          Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: `${respuesta.mensage}`,
          });
        }
        console.log("respuesta", respuesta);
      });
  }

  guardarContactoCuenta(event: any): void {
    event.preventDefault();
    const contacto = this.formContacto.value;
    console.log(contacto);
    // this.params = this.activatedRoute.snapshot.params;
    contacto.idCuenta = Number(this.detalleCliente.idCuenta);
    this._contactosService.guardarContacto(contacto).subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.mostrarContactos();
        Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: `${respuesta.mensage}`,
        });
      }
      console.log("respuesta", respuesta);
    });
    this.formContacto.reset();
  }

  editarDireccionCuenta(event: any): void {
    event.preventDefault();
    const direccion = this.formUbicacionEditar.value;
    // this.params = this.activatedRoute.snapshot.params;
    this._ubicacionService
      .editarDireccion(
        this.ubicacion.idDireccion,
        this.detalleCliente.idCuenta,
        direccion
      )
      .subscribe((respuesta) => {
        if (respuesta.exito === 1) {
          this.mostrarUbicaciones();
          this.formUbicacion.reset();
          Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: `${respuesta.mensage}`,
          });
        }
      });
  }

  editarTelefonoCuenta(event: any): void {
    event.preventDefault();
    const telefono = this.formTelefonoEditar.value;
    console.log(telefono);
    // this.params = this.activatedRoute.snapshot.params;
    this._infoContactoService
      .editarTelefonoContacto(
        this.contacto.idtelefono,
        this.detalleCliente.idCuenta,
        telefono
      )
      .subscribe((respuesta) => {
        if (respuesta.exito === 1) {
          this.mostratInfoContacto();
          Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: `${respuesta.mensage}`,
          });
        }
        console.log("respuesta", respuesta);
      });
  }

  editarContactoCuenta(event: any): void {
    event.preventDefault();
    const contacto = this.formContactoEditar.value;
    console.log(contacto);
    // this.params = this.activatedRoute.snapshot.params;
    contacto.idCuenta = Number(this.detalleCliente.idCuenta);
    contacto.id = Number(this.listaContacto.idContacto);
    this._contactosService.editarContacto(contacto).subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.mostrarContactos();
        Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: `${respuesta.mensage}`,
        });
      }
      console.log("respuesta", respuesta);
    });
  }

  seleccionarUbicacion(ubicacion: any): void {
    if (ubicacion.dataItem.estado === "A") {
      this.tipoMantenimiento = "ver";
      this.tipo = this.tipobot.Direcciones;
      this.habilitarOpciones();
      this.ubicacion = ubicacion.dataItem;
    }
  }

  mostrarUbicaciones(): void {
    this._ubicacionService
      .mostrarUbicaciones(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log("ubicaciones", respuesta.data);
        this.ubicaciones = respuesta.data;
      });
  }

  mostratInfoContacto(): void {
    this._infoContactoService
      .mostrarInfoContacto(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log("respuesta info", respuesta.data);
        this.infoContacto = respuesta.data;
      });
  }

  mostrarDepartamentos(): void {
    this._departamentoService.mostrarDepartamentos().subscribe((respuesta) => {
      this.DEPTO = respuesta.data;
    });
  }

  mostrarEnCadena(): void {
    this._ciudadService
      .mostrarCiudadesPorDepartamento(this.contacto.departamento.idDepto)
      .subscribe((respuesta) => {
        this.CIUDAD = respuesta.data;
        this._distritoService
          .mostrarDistritosPorCiudades(this.contacto.ciudad.idCiudad)
          .subscribe((respuesta) => {
            this.DISTRITO = respuesta.data;
            this.fromTelefonoedit();
          });
      });
  }

  mostrarEnCadenaUbicacion(): void {
    this._ciudadService
      .mostrarCiudadesPorDepartamento(this.ubicacion.departamento.idDepto)
      .subscribe((respuesta) => {
        this.CIUDAD = respuesta.data;
        this._distritoService
          .mostrarDistritosPorCiudades(this.ubicacion.ciudad.idCiudad)
          .subscribe((respuesta) => {
            this.DISTRITO = respuesta.data;
            this.fromDireccionEdit();
          });
      });
  }

  cambioDepartamento(departamento: number): void {
    // console.log(departamento);
    this._ciudadService
      .mostrarCiudadesPorDepartamento(departamento)
      .subscribe((respuesta) => {
        console.log(respuesta.data);
        this.CIUDAD = respuesta.data;
      });
  }

  cambioCiudad(ciudad: number): void {
    this._distritoService
      .mostrarDistritosPorCiudades(ciudad)
      .subscribe((respuesta) => (this.DISTRITO = respuesta.data));
  }

  cambiarMostrarForm(): void {
    this.CIUDAD = [];
    this.DISTRITO = [];
    this.tipoMantenimiento = "agregar";
    this.habilitarOpciones();
  }

  cambiarMostrarFormEditar(): void {
    switch (this.tipo) {
      case tipoBoton.FijoMovil:
        if (this.contacto) {
          this.tipoMantenimiento = "editar";
          this.habilitarOpciones();
          this.mostrarEnCadena();
        }
        break;
      case tipoBoton.Direcciones:
        if (this.ubicacion) {
          this.tipoMantenimiento = "editar";
          this.habilitarOpciones();
          this.mostrarEnCadenaUbicacion();
        }
        break;

      case tipoBoton.Email:
        if (this.email) {
          this.tipoMantenimiento = "editar";
          this.habilitarOpciones();
          this.fromEmailEdit();
        }
        break;

      case tipoBoton.PersonasContacto:
        if (this.listaContacto) {
          this.tipoMantenimiento = "editar";
          this.habilitarOpciones();
          this.fromContactoEdit();
        }
        break;

      default:
        break;
    }
  }

  habilitarOpciones(): void {
    this.mostrarForm = true;
    this.mostarBotonEditar = true;
  }

  // form(): void {
  //   // if (this.tipo === tipobot.te) {
  //   // }
  // }

  private fromTelefono(): void {
    this.formTelefono = this.fb.group({
      telefono: [{ value: "", disabled: false }],
      tipo: [""],
      depto: [""],
      ciudad: [""],
      distrito: [""],
      ciudadb: [""],
      info2: [""],
      contactos: [""],
    });
    // this.formTelefono.get("telefono").enable();
  }

  private fromTelefonoedit() {
    if (this.contacto) {
      this.formTelefonoEditar.controls["telefono"].setValue(
        this.contacto.numero
      );
      // this.formTelefonoEditar.controls["tipo"].setValue(this.contacto.tipo);
      this.formTelefonoEditar.controls["depto"].setValue(
        this.contacto.departamento.idDepto
      );
      this.formTelefonoEditar.controls["ciudad"].setValue(
        this.contacto.ciudad.idCiudad
      );
      this.formTelefonoEditar.controls["distrito"].setValue(
        this.contacto.districto.idDistrito
      );
      this.formTelefonoEditar.controls["info2"].setValue(this.contacto.info2);
    }
  }

  private fromDireccionEdit() {
    if (this.ubicacion) {
      this.formUbicacionEditar.controls["direccion"].setValue(
        this.ubicacion.direccion
      );
      // this.formTelefonoEditar.controls["tipo"].setValue(this.ubicacion.tipo);
      this.formUbicacionEditar.controls["depto"].setValue(
        this.ubicacion.departamento.idDepto
      );
      this.formUbicacionEditar.controls["ciudad"].setValue(
        this.ubicacion.ciudad.idCiudad
      );
      this.formUbicacionEditar.controls["distrito"].setValue(
        this.ubicacion.distrito.idDistrito
      );
      this.formUbicacionEditar.controls["ciudadb"].setValue(
        this.ubicacion.ciudadb
      );
      this.formUbicacionEditar.controls["barrio"].setValue(
        this.ubicacion.barrio
      );
      this.formUbicacionEditar.controls["area"].setValue(
        this.ubicacion.area.idArea
      );
      this.formUbicacionEditar.controls["zona"].setValue(
        this.ubicacion.zona.idZona
      );
    }
  }

  private fromEmailEdit() {
    if (this.email) {
      console.log(email);
      this.formEmailEditar.controls["email"].setValue(this.email.email);
      // this.formTelefonoEditar.controls["tipo"].setValue(this.ubicacion.tipo);
      this.formEmailEditar.controls["info"].setValue(this.email.informacion);
    }
  }

  private fromContactoEdit() {
    if (this.listaContacto) {
      console.log(this.listaContacto);
      this.formContactoEditar.controls["nombre"].setValue(
        this.listaContacto.contacto
      );
      // this.formTelefonoEditar.controls["tipo"].setValue(this.ubicacion.tipo);
      this.formContactoEditar.controls["documento"].setValue(
        this.listaContacto.documento
      );
      this.formContactoEditar.controls["observacion"].setValue(
        this.listaContacto.observacion
      );
      this.formContactoEditar.controls["idRelacion"].setValue(
        this.listaContacto.relacion.idRelacion
      );
    }
  }

  cambiarInfoContacto(tipo: string): void {
    this.tipoMantenimiento = "";
    this.cambiarInfoSelect(tipo);
    this.tipo = tipo;
    this.botonSeleccionado = tipo;
    this.mostrarForm = false;
    this.mostarBotonEditar = false;
    if (tipo !== this.tipobot.FijoMovil) {
      this.mostrarBotonStop = false;
    } else {
      this.mostrarBotonStop = true;
    }

    if (tipo === this.tipobot.Email || tipo === this.tipobot.PersonasContacto) {
      this.mostarBotonExtra = false;
    } else {
      this.mostarBotonExtra = true;
    }
    // console.log(tipo);
  }

  seleccionarContacto(contacto: any) {
    if (contacto.dataItem.estado === "A") {
      this.idTelefono = contacto.dataItem.idtelefono;
      this.tipoMantenimiento = "ver";
      this.habilitarOpciones();
      this.tipo = this.tipobot.FijoMovil;
      this.contacto = contacto.dataItem;
      this.resetearBotonesExtraArbolGestion();
      this.primerNivel();
      this.descrip = [];
      this.anterior = [];
      this.idArbolDecision = null;
      this.form.controls.valor.setValue("");
      this.peso = null;
    }

    // this.mostrarEnCadena(contacto.dataItem);
    // // this.cambiarInfoContacto(this.tipo);

    // await this.habilitarOpciones();
  }

  seleccionarListaContacto(contacto: any) {
    if (contacto.dataItem.estado === "A") {
      this.tipoMantenimiento = "ver";
      this.habilitarOpciones();
      this.tipo = this.tipobot.PersonasContacto;
      this.listaContacto = contacto.dataItem;
      console.log("Seleccionar Contacto: ", this.listaContacto);
    }

    // this.mostrarEnCadena(contacto.dataItem);
    // // this.cambiarInfoContacto(this.tipo);

    // await this.habilitarOpciones();
  }

  cambiarInfoSelect(tipo: string): void {
    switch (tipo) {
      case this.tipobot.FijoMovil:
        this.tipoDeGestion = [
          { id: 1, texto: "LLamada Out" },
          { id: 2, texto: "LLamada In" },
        ];
        this.tipoGestion = "LLamada Out";
        break;
      case this.tipobot.Direcciones:
        this.tipoDeGestion = [{ id: 1, texto: "Direcciones" }];
        this.tipoGestion = "Direcciones";

        break;
      case this.tipobot.Email:
        this.tipoDeGestion = [{ id: 1, texto: "Email" }];
        this.tipoGestion = "Email";
        break;
      case this.tipobot.PersonasContacto:
        this.tipoDeGestion = [
          { id: 1, texto: "LLamada Out" },
          { id: 2, texto: "LLamada In" },
        ];
        this.tipoGestion = "LLamada Out";
        break;

      default:
        break;
    }
  }

  prueba(): boolean {
    return true;
  }

  estadoAnterior: boolean = false;
  estadoFinalizado: boolean = false;

  primerNivel(): void {
    this._arbolGestionService.primerNivel().subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.botonbase = respuesta.data;
        this.botones = respuesta.data;
      }
    });
  }

  resetearBotonesExtraArbolGestion(): void {
    this.estadoAnterior = false;
    this.estadoFinalizado = false;
  }

  public ema: any[] = email;
  public persona: any[] = personaContacto;

  public value: Date = new Date();

  public peso: number;

  asignar(arbol: any): void {
    const descripcion = arbol.descripcion;
    console.log("asignar", this.descrip);
    let valor = "";
    let c = 0;
    let c2 = 0;
    for (const val of this.descrip) {
      if (val === descripcion) {
        c = 1;
      }
    }
    if (c === 0) {
      if (this.peso) {
        this.descrip.pop();
        this.peso = null;
      }
      this.descrip.push(descripcion);
    }

    for (const value of this.descrip) {
      if (c2 === 0) {
        valor = value;
        c2 = 1;
      } else {
        valor = `${valor};${value}`;
      }
    }
    this.form.controls.valor.setValue(valor);
    if (arbol.peso) {
      this.peso = arbol.peso;
    }
  }

  cambiar(value: any): void {
    console.log("valor arbol de gestion", value.descripcion);
    this.arbolSelecciondo = value.descripcion;
    if (value.alerta !== "1") {
      this.asignar(value);
      this._arbolGestionService.subNivel(value.id).subscribe((respuesta) => {
        console.log(respuesta.data);
        if (respuesta.exito === 1) {
          if (respuesta.data.length === 0) {
            this.estadoFinalizado = true;
            this.estadoAnterior = false;
          } else {
            if (respuesta.data.alerta !== 1) {
              //  this.asignar(value.descripcion)
              this.estadoAnterior = true;
              this.estadoFinalizado = false;
              //  if (respuesta.data.idParent) {
              this.anterior.push(this.botones);
              //  }
              this.botones = respuesta.data;
            } else {
              this.open();
            }
            // this.idArbolDecision = respuesta.
          }
        }
      });
    } else {
      this.open2();
    }
    if (value.peso) {
      this.idArbolDecision = value.id;
    }
  }

  open(): void {
    this.modalService.show(ModalGestionIdAlertComponent, {
      initialState: { arbolSelecciondo: this.arbolSelecciondo },
      backdrop: "static",
      class: "gray modal-lg",
    });
  }

  open2(): void {
    this.modalService.show(ModalGestionIdAlertComponent, {
      initialState: {
        idCampania: Number(this.gestionGlobal),
        arbolSelecciondo: this.arbolSelecciondo,
        idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
      class: "gray modal-lg",
    });
  }

  // opeArchivoAdjunto(): void {
  //   this.modalService.show(ModalArchivoAdjuntoComponent, {
  //     // initialState: { titulo, usuario: usuarioEdit },
  //     backdrop: "static",
  //     // class:'gray modal-lg'
  //   });
  // }

  openActivarDesactivar(): void {
    let dato: any;
    switch (this.tipo) {
      case this.tipobot.FijoMovil:
        dato = this.contacto;
        break;
      case this.tipobot.Direcciones:
        dato = this.ubicacion;
        break;
      case this.tipobot.Email:
        dato = this.email;
        break;
      case this.tipobot.PersonasContacto:
        dato = this.listaContacto;
        break;

      default:
        break;
    }
    this.modalService.show(ModalActivarDesactivarComponent, {
      initialState: { Dato: dato, Tipo: this.tipo },
      backdrop: "static",
      // class:'gray modal-lg'
    });
  }
  regresar(): void {
    this.peso = null;
    if (this.anterior.length > 0) {
      this.estadoFinalizado = false;
      this.estadoAnterior = true;
      this.botones = this.anterior[this.anterior.length - 1];
      this.anterior.pop();
    } else {
      this.estadoAnterior = false;
    }
    if (this.descrip.length < 2) {
      this.descrip = [];
    } else {
      this.descrip.pop();
    }
    this.eliminarValor();
  }
  eliminarValor(): void {
    let valor = "";
    let c2 = 0;
    for (const value of this.descrip) {
      if (c2 === 0) {
        valor = value;
        c2 = 1;
      } else {
        valor = `${valor};${value}`;
      }
    }
    console.log(this.anterior, this.descrip.length);

    if (this.anterior.length === 0) {
      valor = "";
      this.descrip = [];
      this.resetearBotonesExtraArbolGestion();
    }
    this.form.controls.valor.setValue(valor);
  }

  arbolFinalizado(): boolean {
    return this.botones.length < 1;
  }

  botonRegresar(): void {}

  public data: Array<any> = [
    {
      text: "Identificación",
      icon: "k-icon k-i-exe",
      click: () => {
        console.log("Busqueda por  Identificación");
      },
    },
    {
      text: "Teléfono",
      icon: "k-icon k-i-custom-format",
      click: () => {
        console.log("Busqueda por Telefono");
      },
    },
    {
      text: "Cuenta",
      icon: "k-icon  k-i-window",
      click: () => {
        console.log("Busqueda por  Cuenta");
      },
    },
    {
      text: "Nombre",
      icon: "k-icon k-i-spell-checker",
      click: () => {
        console.log("Busqueda por Nombre");
      },
    },
    {
      text: "E-mail",
      icon: "email",
      click: () => {
        console.log("Busqueda por E-mail ");
      },
    },
  ];

  public onPaste(): void {
    console.log("Paste");
  }
  //public solicitud: Date;
  public form: FormGroup = new FormGroup({
    username: new FormControl(),
    valor: new FormControl(),
    parentezco: new FormControl(),
    comentario: new FormControl(),
    date_solicitud: new FormControl(this.value),
    date_ejecucion: new FormControl(this.value),
  });

  public clearForm(): void {
    this.form.reset();
  }

  AcuerdosPagoCuentaCampania(): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService
      .AcuerdosPagoCuentaCampania(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log(respuesta);
        for (let index = 0; index < respuesta.data.length; index++) {
          const element = respuesta.data[index];
          respuesta.data[index].fecha = format(
            new Date(element.fecha),
            "yyyy/MM/dd-H:m:s"
          );
        }
        this.acuerdos = respuesta.data;
      });
  }

  PagoCuentaCampania(): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService
      .PagoCuentaCampania(this.gestionGlobal)
      .subscribe((respuesta) => {
        // console.log(respuesta.data);
        for (let index = 0; index < respuesta.data.length; index++) {
          const element = respuesta.data[index];
          respuesta.data[index].fecha = format(
            new Date(element.fecha),
            "yyyy/MM/dd-H:m:s"
          );
        }
        this.pagos = respuesta.data;
      });
  }

  ProductoCuentaCampania(): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService
      .ProductoCuentaCampania(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log("productos", respuesta.data);
        this.productos = respuesta.data;
      });
  }

  mostrarHistoricoCliente(): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService
      .mostrarHistoricoCliente(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log("HISTORICO", respuesta.data);
        this.historicoCliente = respuesta.data;
      });
  }

  mostrarDetalleCliente(): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService
      .mostrarDetalleCliente(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log("detalle del cliente", respuesta.data[0]);
        this.detalleCliente = respuesta.data[0];
        this.DeudasCarteras(this.detalleCliente.idCuenta);
      });
  }

  DeudasCarteras(idCuenta: number): void {
    this._campaniaService.DeudasCarteras(idCuenta, Number(this.gestionGlobal)).subscribe((respuesta) => {
      console.log("Otras Carteras", respuesta.data);
      this.otrasCarteras = respuesta.data;
    });
  }

  mostrarTelefono(): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService
      .mostrarTelefono(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log(respuesta.data);
        // this.historicoCliente = respuesta.data;
      });
  }

  seleccionarProducto(producto: any): void {
    this.params = this.activatedRoute.snapshot.params;
    this._campaniaService.mostrarTelefono(this.gestionGlobal);
    this.idCuentasCampaniaProductos = producto.dataItem.cuenta_producto;
    this.modalService.show(MostrarDetalleProductoComponent, {
      initialState: {
        Producto: producto.dataItem,
        idCampania: Number(this.gestionGlobal),
      },
      backdrop: "static",
      class: "gray modal-lg",
    });
  }
  cambioDescripcion(descripcion: string): void {
    this.detalleGestion = descripcion;
  }
  cambiarTipoGestion(tipo: any): void {
    console.log(tipo.target.value);
    this.tipoGestion = tipo.target.value;
  }

  guardarGestion(): void {
    const validacion = this.validacion();
    if (validacion.status) {
      const datos = {
        tipoGestion: this.tipoGestion,
        detalleGestion: this.detalleGestion,
        idArbolDecision: this.idArbolDecision,
        idCuentasCampaniaProductos: this.idCuentasCampaniaProductos,
        idTelefono: this.idTelefono,
      };
      this.params = this.activatedRoute.snapshot.params;
      this._campaniaService
        .guardarGestion(this.gestionGlobal, datos)
        .subscribe((respuesta) => {
          console.log(respuesta.data);
          if (respuesta.exito === 1) {
            this.router.navigate([`gestionar/gestionar/gestion-normal`]);
            Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
          } else {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: `${respuesta.mensage}`,
            });
          }
        });
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: `${validacion.message}`,
      });
    }
  }
  mostrarContactos(): void {
    this.params = this.activatedRoute.snapshot.params; //recibe el idCuentaCampania
    this._contactosService
      .mostrarContactos(this.gestionGlobal)
      .subscribe((respuesta) => {
        console.log("Contactos", respuesta.data);
        this.listaContactos = respuesta.data;
      });
  }
  validacion(): { status: boolean; message: string } {
    console.log("telefono id ", this.idTelefono);
    if (!this.detalleGestion) {
      return { status: false, message: "La descripcion es requerida!" };
    }
    if (!this.idArbolDecision) {
      return { status: false, message: "Finalice el arbol de gestion!" };
    }
    if (!this.idTelefono) {
      return { status: false, message: "El telefono es requerido!" };
    }
    return { status: true, message: "correcto!" };
  }

  llamadaDinomi(contacto: any): void {
    console.log(contacto);
    this._dinomiService
      .llamadaDinomi(this.extencion, contacto.dataItem.numero)
      .subscribe((respuesta) => {
        console.log(respuesta);

        Swal.fire({
          title: "Correcto!",
          html: "LLamando....",
          timer: 3000,
          timerProgressBar: true,
          showConfirmButton: false,
          icon: "success",
        });
      });
  }
  smsDinomi(item: any): void {
    console.log(item);
    this.modalService.show(ModalSmsComponent, {
      initialState: { Item: item },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  emailDinomi(email: any): void {
    console.log(email);
    this.modalService.show(ModalEmailComponent, {
      initialState: { Email: email },
      backdrop: "static",
      // class: "gray modal-lg",
    });
  }

  cabioTipo(tipo: string): void {
    this.tipoSeleccionado = tipo;
  }

  cambiosModal(): void {
    this.modalService.onHidden.pipe(takeUntil(this.stop$)).subscribe((data) => {
      switch (data) {
        case this.tipobot.FijoMovil:
          this.mostratInfoContacto();
          break;
        case this.tipobot.Direcciones:
          this.mostrarUbicaciones();
          break;
        case this.tipobot.Email:
          this.mostrarEmails();
          break;
        case this.tipobot.PersonasContacto:
          this.mostrarContactos();
          break;
        case "acuerdo":
          this.AcuerdosPagoCuentaCampania();
          break;
        default:
          break;
      }
    });
  }

  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
}
