import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { CampaniaService } from "../../../services/campania.service";
import { AcuerdoService } from "../../../services/acuerdo.service";
import { UbicacionService } from "../../../services/ubicacion.service";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-modal-gestion-id-alert",
  templateUrl: "./modal-gestion-id-alert.component.html",
  styleUrls: ["./modal-gestion-id-alert.component.css"],
})
export class ModalGestionIdAlertComponent implements OnInit {
  idProducto: number;
  arbolSelecciondo: any;
  idCampania: number;
  public tipoResidencia: string[] = [
    "No le prestaron el dinero",
    "Cuentas por cobrar",
    "Disminucion de ingresos",
  ];
  productos: any[] = [];
  acuerdos: any[] = [];
  direcciones: any[] = [];
  params: any;
  value: Date;

  ngOnInit(): void {
    this.mostrarProductos();
    //this.mostrarAcuerdos();
    this.mostrarDirecciones();
    this.formGuardar();
    console.log("arbol seleccionado", this.arbolSelecciondo);
    this.listaItems.push(this.arbolSelecciondo);
  }

  public phoneNumberValue: string = "";
  public phoneNumberMask: string = "(999) 000-00-00-00";
  public comboBoxValue: string;
  public form: FormGroup;
  //public format = "hh:mm:ss";

  public data: any = {
    fullName: "",
    email: "",
    phoneNumber: this.phoneNumberValue,
    arrivalDate: null,
    numberOfNights: null,
    numberOfGuests: null,
    terms: false,
    comments: "",
  };

  public listItems: Array<string> = ["Cocina", "Refrigeradora", "Nevera"];
  public listaItems: Array<string> = [
    // "Abono",
    // "Efecto de Acuerdo",
    // "Cuota",
    // "Pago con Descuento",
    // "Pago Total",
  ];
  constructor(
    public modalRef: BsModalRef,
    private readonly _campaniaService: CampaniaService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly _acuerdoService: AcuerdoService,
    private readonly _direccionService: UbicacionService,
    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService
  ) {}
  mostrarProductos(): void {
    this._campaniaService
      .ProductoCuentaCampania(this.idCampania)
      .subscribe((respuesta) => {
        console.log(respuesta.data);
        this.productos = respuesta.data;
      });
  }
  mostrarDirecciones(): void {
    this._direccionService
      .mostrarUbicaciones(this.idCampania)
      .subscribe((respuesta) => {
        // console.log("mostrarDirecciones: ", respuesta.data);
        this.direcciones = respuesta.data;
      });
  }
  mostrarAcuerdos(): void {
    this._acuerdoService
      .mostrarAcuerdos(this.idCampania)
      .subscribe((respuesta) => {
        console.log("mostrarAcuerdos: ", respuesta.data);
        this.acuerdos = respuesta.data;
      });
  }
  private formGuardar(): void {
    this.form = this.fb.group({
      idCuentaCampaniaProducto: [this.idProducto],
      tipoAcuerdo: [""],
      valorAcuerdo: [""],
      fechaAcuerdo: [null],
      estado: [""],
      //cuota: [""],
      //plazo: [""],
      probabilidad: [""],
      comentario: [""],
      fechaVisita: [null],
      //horaAcuerdo: ["2021-02-04T18:11:02.710Z"]
      horaAcuerdo: [null],
      terms: [false],
      idDireccion: [null],
    });
  }

  guardarAcuerdoPago(event: any): void {
    event.preventDefault();
    const acuerdo = this.form.value;
    acuerdo.efecto = this.arbolSelecciondo;
    console.log("Acuerdo: ", acuerdo);
    this._acuerdoService.guardarAcuerdo(acuerdo).subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.mostrarAcuerdos();
        Swal.fire("Correcto!", `${respuesta.mensage} `, "success");
        this.modalService.setDismissReason("acuerdo");
        this.modalRef.hide();
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: `${respuesta.mensage}`,
        });
      }
      console.log("respuesta", respuesta);
    });
    this.form.reset();
  }

  public submitForm(): void {
    this.form.markAllAsTouched();
  }

  public clearForm(): void {
    this.form.reset();
  }

  public model = {
    gender: null,
  };
}
