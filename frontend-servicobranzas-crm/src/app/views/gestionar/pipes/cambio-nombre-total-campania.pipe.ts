import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cambioNombreTotalCampania'
})
export class CambioNombreTotalCampaniaPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if(!value){
      return 'TOTAL CAMPAÑA'
    }else{
      return value;
    }
    
  }

}
