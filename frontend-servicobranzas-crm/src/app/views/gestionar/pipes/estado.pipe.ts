import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "estado",
})
export class EstadoPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): string {
    let salida = "";
    switch (value) {
      case "A":
        salida = "ACTIVO";
        break;
      case "I":
        salida = "INACTIVO";
        break;

      default:
        break;
    }
    return salida;
  }
}
