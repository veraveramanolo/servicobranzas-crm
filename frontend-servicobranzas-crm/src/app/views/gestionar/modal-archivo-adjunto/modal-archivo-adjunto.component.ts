import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal";

@Component({
  selector: "app-modal-archivo-adjunto",
  templateUrl: "./modal-archivo-adjunto.component.html",
  styleUrls: ["./modal-archivo-adjunto.component.css"],
})
export class ModalArchivoAdjuntoComponent implements OnInit {
  public imgTemp: any = null;
  public imagenSubir: File;
  constructor(public modalRef: BsModalRef) {}

  ngOnInit(): void {}

  cambiarImagen(file: File): void {
    // const foto = file.target.files[0];
    this.imagenSubir = file;
    // this.file = file;

    if (!file) {
      return (this.imgTemp = null);
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      this.imgTemp = reader.result;
      // this.cd.markForCheck();
    };
  }
}
