import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { CampaniaService } from "../../../services/campania.service";

@Component({
  selector: "app-mostrar-detalle-producto",
  templateUrl: "./mostrar-detalle-producto.component.html",
  styleUrls: ["./mostrar-detalle-producto.component.css"],
})
export class MostrarDetalleProductoComponent implements OnInit {
  Producto: any;
  idCampania: number;
  detalleProducto: any[] = [];

  constructor(
    public modalRef: BsModalRef,
    private readonly modalService: BsModalService,
    private readonly _campaniaService: CampaniaService
  ) {}

  ngOnInit(): void {
    this.mostrarDetalleProducto();
  }

  mostrarDetalleProducto(): void {
    console.log("ids", this.idCampania, this.Producto.cuenta_producto);
    this._campaniaService
      .mostrarDetalleProducto(this.idCampania, this.Producto.cuenta_producto)
      .subscribe((respuesta) => {
        console.log(respuesta.data);
        this.detalleProducto = respuesta.data;
      });
  }
}
