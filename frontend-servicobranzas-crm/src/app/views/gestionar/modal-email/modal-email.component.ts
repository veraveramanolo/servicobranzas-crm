import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { DinomiService } from "../../../services/dinomi.service";

@Component({
  selector: "app-modal-email",
  templateUrl: "./modal-email.component.html",
  styleUrls: ["./modal-email.component.css"],
})
export class ModalEmailComponent implements OnInit {
  emailForm: FormGroup;
  Email: any;

  constructor(
    public modalRef: BsModalRef,
    private readonly fb: FormBuilder,
    private readonly _dinomiService: DinomiService,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.form();
  }
  private form(): void {
    this.emailForm = this.fb.group({
      body: ["", [Validators.required]],
      subject: ["", Validators.required],
    });
  }

  emailDinomi(event: any): void {
    console.log(this.emailForm);
    const contenido = this.emailForm.value;
    const email = {
      subject: contenido.subject,
      email: this.Email.email,
      body: contenido.body,
    };
    this._dinomiService.emailDinomi(email).subscribe((respuesta) => {
      if (respuesta.respuesta === "success") {
        this.emailForm.reset();

        this.modalService.setDismissReason("ok");
        this.modalRef.hide();
        Swal.fire({
          title: "Correcto!",
          html: "Correo enviado!",
          timer: 3000,
          timerProgressBar: true,
          showConfirmButton: false,
          icon: "success",
        });
      } else {
        Swal.fire({
          allowOutsideClick: false,
          text: "Error al enviar el correo!",
          icon: "error",
        });
      }
    });
  }
}
