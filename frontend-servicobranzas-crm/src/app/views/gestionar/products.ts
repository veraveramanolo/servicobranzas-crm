export interface User {
  birthDate: Date;
}

export const sampleProducts = [
  {
    idTelefono: 1,
    telefono: "0978654785",
    relacion: { id: 1, descripcion: "cliente" },
    tipo: { id: 1, descripcion: "RESIDENCIA" },
    contacto: "OTRO NO EFECTIVO",
  },
  {
    idTelefono: 2,
    telefono: "0978654786",
    relacion: { id: 1, descripcion: "cliente" },
    tipo: { id: 1, descripcion: "RESIDENCIA" },
    contacto: "Directo",
  },
  {
    idTelefono: 3,
    telefono: "0978654787",
    relacion: { id: 1, descripcion: "cliente" },
    tipo: { id: 3, descripcion: "OTRO" },
    contacto: "OTRO NO EFECTIVO",
  },
];

export const visitar = [
  {
    ProductID: "",
    Ubicacion: "AV.EL PARQUE Y ALONSO DE TORRES",
    SupplierID: 1,
    CategoryID: 1,
    QuantityPerUnit: "10 boxes x 20 bags",
    Tipo: "",
    Contacto: "",
    UnitsOnOrder: 0,
    ReorderLevel: 10,
    Discontinued: false,
    Visitar: true,
    Verificar: false,
    Relacion: "Cliente",

    FirstOrderedOn: new Date(1996, 8, 20),
  },
];

export const email = [
  {
    ProductID: "",
    Email: "email@email.com",
    Informacion: "",
    Verificacion: false,
    Autorizar: true,
    SupplierID: 1,
    CategoryID: 1,
    QuantityPerUnit: "10 boxes x 20 bags",
    Tipo: "",
    Contacto: "",
    UnitsOnOrder: 0,
    ReorderLevel: 10,
    Discontinued: false,
    Visitar: true,
    Verificar: false,
    Relacion: "Cliente",

    FirstOrderedOn: new Date(1996, 8, 20),
  },
];

export const personaContacto = [
  {
    ProductID: "",
    NombreCompleto: "ALINA RAMIREZ",
    Informacion: "",
    Verificacion: false,
    Autorizar: true,
    SupplierID: 1,
    CategoryID: 1,
    QuantityPerUnit: "10 boxes x 20 bags",
    Tipo: "",
    Contacto: "",
    UnitsOnOrder: 0,
    ReorderLevel: 10,
    Discontinued: false,
    Visitar: true,
    Verificar: false,
    Relacion: "Cliente",

    FirstOrderedOn: new Date(1996, 8, 20),
  },
];
