import { Component, OnInit } from "@angular/core";
import {
  DataStateChangeEvent,
  GridDataResult,
  FilterService,
} from "@progress/kendo-angular-grid";
import {
  State,
  process,
  filterBy,
  FilterDescriptor,
  CompositeFilterDescriptor,
} from "@progress/kendo-data-query";
import { VariableGlobalService } from "../../../services/variable-global.service";
import { CrmConsultaGestionCampania } from "../../../models/CrmConsultaGestionCampania";
import { ActivatedRoute, Router } from "@angular/router";
import { CampaniaService } from "../../../services/campania.service";
import Swal from "sweetalert2";
import { ModeloRespuesta } from "../../../models/ModeloRespuesta.models";
import { CrmCartera } from "../../../models/CrmCartera";
import { CrmCampania } from "../../../models/CrmCampania";
import { AclUsuario } from "../../../models/AclUsuario";
import { CrmCarteraService } from "../../../services/crm-cartera.service";
import { async } from "@angular/core/testing";
import { isNullOrUndefined } from "util";
import { ValorDropGestionNormal } from "../../../models/ValoresFiltros";
import { isNullOrEmptyString } from "@progress/kendo-angular-grid/dist/es2015/utils";
import { AcuerdoService } from "../../../services/acuerdo.service";
import * as gestionNormalActions from "../store/actions";

import { v4 as uuidv4 } from "uuid";
import { GestionNormalModuleState } from "../store/states";
import { Store } from "@ngrx/store";
import { DomSanitizer, SafeStyle } from "@angular/platform-browser";
import { ConsultauserService } from "../../../services/consultauser.service";
import { format } from "@progress/kendo-angular-intl";
import { BsModalService } from "ngx-bootstrap/modal";
import { ModalGuardarFiltroGestionComponent } from "../modal-guardar-filtro-gestion/modal-guardar-filtro-gestion.component";
import { MulticanalService } from "../../../services/multicanal.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

const valorDropGestionNormal = "VDGN";

const flatten = (filter) => {
  const filters = (filter || {}).filters;
  if (filters) {
    return filters.reduce(
      (acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]),
      []
    );
  }
  return [];
};
const distinctNombre = (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x, idx, xs) => {
      // console.log(x, idx, xs);
      return xs.findIndex((y) => y === x) === idx;
    });

const distinctExtra = (data, tipo: string) =>
  data
    .map((x) => x[tipo])
    .filter((x: string, idx, xs) => {
      let limpiar;
      if (x != null) {
        limpiar = x.trim();
      } else {
        limpiar = x;
      }

      // console.log(x, idx, xs);
      return (
        xs.findIndex(
          (y) => y === limpiar && limpiar != null && limpiar != ""
        ) === idx
      );
    });

/*const estadoListaCantidad = data => data
  .map(x => x.estadoListaCantidad)
  .filter((x, idx, xs) => xs.findIndex(y => y.estado === x.estado) === idx);*/

@Component({
  selector: "app-gestion-normal",
  templateUrl: "./gestion-normal.component.html",
  styleUrls: ["./gestion-normal.component.css"],
})
export class GestionNormalComponent implements OnInit {
  public nombres = [];
  public labelEstados = [];
  public nocuentas = [];
  public asesores = [];
  public cedulas = [];
  public extra1 = [];
  public extra2 = [];
  public extra3 = [];
  public extra4 = [];
  public extra5 = [];
  public extra6 = [];
  public extra7 = [];
  public extra8 = [];
  public extra9 = [];
  public extra10 = [];
  public mejorestado = [];
  public datoGridTarea: GridDataResult;
  public estadoGridTarea: State;
  public cargandoGridTarea: boolean;
  responseTarea: any;
  public estadoListaCantidad: any = [];
  public estadofiltro: any = [];
  public datoGrid: GridDataResult;
  public busquedaDinamicaGrid: GridDataResult;
  public estadoGrid: State;
  public cargandoGrid: boolean;
  public infoCrmCuentasCampania: CrmConsultaGestionCampania[];
  public infoCrmCuentasCampaniaApi: CrmConsultaGestionCampania[];
  public infoCrmCuentasCampaniaseleccionada: CrmConsultaGestionCampania;
  public parametroIdCampania: any = undefined;
  public InfoCartera: CrmCartera[] = [];
  public InfoCampania: CrmCampania[] = [];
  public listaCartera: CrmCartera[] = [];
  public listaCampania: CrmCampania[] = [];
  public listaCampaniafiltrada: CrmCampania[] = [];
  public listaCampaniaEmergente: CrmCampania[] = [];
  public valorComboCartera: CrmCartera;
  public valorComboCampania: CrmCampania = null;
  public position: "top" | "bottom" | "both" = "top";
  public usuarioLogin: AclUsuario;
  public datoAlertas: any[] = [];
  total: number; // ITA
  totalfiltro: number; // ITA
  admin: number;
  public filtroactivado: boolean = false;
  public filtroscreados = [];
  private stop$ = new Subject<void>();

  constructor(
    private global: VariableGlobalService,
    private activateRouter: ActivatedRoute,
    private serviceCarmpania: CampaniaService,
    private serviceCrmCartera: CrmCarteraService,
    private serviceUsuario: ConsultauserService, //ITA
    private router: Router,
    private serviceAcuerdo: AcuerdoService,
    private readonly store: Store<GestionNormalModuleState>,
    private sanitizer: DomSanitizer,
    private readonly modalService: BsModalService,
    private readonly _multicanalService: MulticanalService,
  ) {
    this.ConsultarUsuarioLogin();
    this.total = 0; // ITA
    this.totalfiltro = 0; // ITA
  }

  async ngOnInit() {
    this.estadoGrid = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    this.estadoGridTarea = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };

    if (!isNullOrUndefined(this.global.valorDropGestionNormal)) {
      this.LlenaDropGlobalGestionNormal(this.global.valorDropGestionNormal);
      setTimeout(() => {
        this.GetCuentasCampania(
          this.valorComboCampania.id,
          this.usuarioLogin.idUsuario
        );
        this.obtenerListaEstados(
          this.valorComboCampania.id,
          this.usuarioLogin.idUsuario
        );
      }, 200);
    } else if (
      !isNullOrEmptyString(sessionStorage.getItem(valorDropGestionNormal))
    ) {
      this.global.valorDropGestionNormal = this.LeerFiltroString(
        sessionStorage.getItem(valorDropGestionNormal)
      );
      this.LlenaDropGlobalGestionNormal(
        this.LeerFiltroString(sessionStorage.getItem(valorDropGestionNormal))
      );
      setTimeout(() => {
        this.GetCuentasCampania(
          this.valorComboCampania.id,
          this.usuarioLogin.idUsuario
        );
        this.obtenerListaEstados(
          this.valorComboCampania.id,
          this.usuarioLogin.idUsuario
        );
        this.obtenerListaEstados(
          this.valorComboCampania.id,
          this.usuarioLogin.idUsuario
        );
      }, 200);
    } else {
      this.global.valorDropGestionNormal = {};
    }
    //ITA
    await this.Usuario(this.usuarioLogin.idUsuario);
    if (this.admin == 1 || this.admin == 3) {
      await this.GetCarteras(); //ITA
      await this.GetConsultaCampania(); //ITA
    } else {
      await this.GetCarteraGestor(this.usuarioLogin.idUsuario);
      await this.GetCampaniaGestor(this.usuarioLogin.idUsuario);
    }
    //FIN ITA
    this.MostrarAlertas();
    this.obtenerTareas();
    this.cambiosModal();
    //this.parametroIdCampania =  await this.activateRouter.snapshot.paramMap.get("IdCampania")
  }
  cellClickHandlerTarea({ dataItem, rowIndex }) {
    this.valorComboCartera = null;
    this.valorComboCampania = null;
    this.listaCampaniafiltrada = [];
    this.serviceCarmpania.obtenerCab(dataItem.id).subscribe((respuesta) => {
      this.infoCrmCuentasCampania = respuesta.data;
      console.log("click en la celda", dataItem);
      this.total = this.infoCrmCuentasCampania.length; // ITA
      console.log("tareas: ",this.total); // ITA

      console.log(
        "listado al seleccionar una tarea",
        this.infoCrmCuentasCampania
      );
      this.datoGrid = process(this.infoCrmCuentasCampania, this.estadoGrid);
      this.cargandoGrid = false;
      this.totalfiltro = this.infoCrmCuentasCampania.length;  // ITA

      if (respuesta.exito === 0) {
        return Swal.fire(
          "Error al Consultar",
          "Se produjo un error en el sistema intente nuevamente",
          "error"
        );
      }
      Swal.close();
    });

    // for (const iterator of this.infoCrmCuentasCampania) {
    //   iterator.id = uuidv4();
    //   const selector = gestionNormalActions.agregarGestionNormal({
    //     gestionNormal: iterator,
    //   });
    //   this.store.dispatch(selector);
    // }
  }

  EventoEstadoGridTarea(estadoGrid: any) {
    // debugger;

    console.log("estados del grid Tarea", estadoGrid);

    this.estadoGridTarea = estadoGrid;
    this.datoGridTarea = process(this.responseTarea, this.estadoGridTarea);
  }

  obtenerTareas(): void {
    this.estadoGridTarea = {
      skip: 0,
      take: 8,
      filter: { logic: "and", filters: [] },
    };
    this.cargandoGridTarea = true;
    this.serviceCarmpania.obtenerTareas().subscribe((response) => {
      // if (response.data) {
      //   for (let index = 0; index < response.data.length; index++) {
      //     const element = response.data[index];
      //     if (element.fechaCreacion) {
      //       response.data[index].fechaCreacion = format(
      //         new Date(element.fechaCreacion),
      //         "yyyy/MM/dd"
      //       );
      //     }
      //   }
      // }

      console.log("respuesta de tareas", response);
      this.cargandoGridTarea = false;
      this.responseTarea = response.data;

      this.datoGridTarea = process(this.responseTarea, this.estadoGridTarea);
    });
  }

  addTotal(): void {
    let total: number = 0;
    for (const value of this.estadoListaCantidad) {
      total += Number(value.cantidad);
    }
    const elemento = {
      alerta: null,
      cantidad: total,
      estado: "TOTAL CAMPAÑA",
      id: null,
      texto: "",
    };
    this.estadoListaCantidad.push(elemento);
    this.total = total; // ITA
    console.log(total);
  }

  obtenerListaEstados(idcampania: number, idusuario: number): void {
    this.serviceCarmpania
      .obtenerListaEstados(idcampania, idusuario)
      .subscribe((data) => {
        console.log("listado estados", data);
        this.estadoListaCantidad = data.data;
        this.addTotal();
        this.estadofiltro = [...this.estadoListaCantidad];
        this.estadofiltro.pop();
        console.log("Last: ", this.estadofiltro);
      });
  }

  ConsultarUsuarioLogin() {
    this.usuarioLogin = this.global.ConsultarUsuarioLogin();
  }

  MostrarAlertas(): void {
    this.serviceAcuerdo.mostrarAlertas().subscribe((respuesta) => {
      console.log("Alertas: ", respuesta.data);
      this.datoAlertas = respuesta.data;
    });
  }

  async GetCarteraGestor(idUsuario: number) {
    this.listaCartera = [];
    this.InfoCartera = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCrmCartera.GetCarteraGestor(idUsuario);

    this.InfoCartera = resp.data;
    this.listaCartera = this.InfoCartera;
  }

  async GetCampaniaGestor(idUsuario: number) {
    this.listaCampania = [];
    this.InfoCampania = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetCampaniaGestor(idUsuario);

    this.InfoCampania = resp.data;
    this.listaCampania = this.InfoCampania;
  }

  //ITA
  async Usuario(id: number) {
    let resp: ModeloRespuesta = null;
    resp = await this.serviceUsuario.Usuario(id);
    let user = resp.data;
    this.admin = user.idRol;
    console.log(this.admin);
  }

  async GetCarteras() {
    this.listaCartera = [];
    this.InfoCartera = [];

    let resp: ModeloRespuesta = null;
    resp = await this.serviceCrmCartera.GetCarteras();

    this.InfoCartera = resp.data;
    this.listaCartera = this.InfoCartera;
    console.log("listaCartera",this.listaCartera);
  }

  async GetConsultaCampania() {
    this.listaCampania = [];
    this.InfoCampania = [];

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetConsultaCampania();

    this.InfoCampania = resp.data;
    this.listaCampania = this.InfoCampania;
    console.log("listaCampania", this.listaCampania);
  }

  // FIN ITA

  async GetCuentasCampania(IdCampania, IdUsuario) {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();

    let resp: ModeloRespuesta = undefined;
    resp = await this.serviceCarmpania.GetCuentasCampania(
      IdCampania,
      IdUsuario
    );
    this.nombres = distinctNombre(resp.data, "nombrecliente");
    this.labelEstados = distinctExtra(resp.data, "labelEstado");
    this.nocuentas = distinctNombre(resp.data, "idcuenta");
    this.asesores = distinctNombre(resp.data, "asesor");
    this.cedulas = distinctNombre(resp.data, "cedula");
    this.extra1 = distinctExtra(resp.data, "extra1");
    this.extra2 = distinctExtra(resp.data, "extra2");
    this.extra3 = distinctExtra(resp.data, "extra3");
    this.extra4 = distinctExtra(resp.data, "extra4");
    this.extra5 = distinctExtra(resp.data, "extra5");
    this.extra6 = distinctExtra(resp.data, "extra6");
    this.extra7 = distinctExtra(resp.data, "extra7");
    this.extra8 = distinctExtra(resp.data, "extra8");
    this.extra9 = distinctExtra(resp.data, "extra9");
    this.extra10 = distinctExtra(resp.data, "extra10");
    this.mejorestado = distinctExtra(resp.data, "mejorestado");
    resp.data.map(item => {
      console.log("------fecha -----",item.ultimagestion);
      if (item.ultimagestion !== null) {
        item.ultimagestion = new Date(item.ultimagestion);
      }
      if (item.fechamejorestado !== null) {
        item.fechamejorestado = new Date(item.fechamejorestado);
      }
      return item;
      });

    this.infoCrmCuentasCampania = resp.data;
    console.log("listado", this.infoCrmCuentasCampania);
    // for (const iterator of this.infoCrmCuentasCampania) {
    //   iterator.id = uuidv4();
    //   const selector = gestionNormalActions.agregarGestionNormal({
    //     gestionNormal: iterator,
    //   });
    //   this.store.dispatch(selector);
    // }

    this.datoGrid = process(this.infoCrmCuentasCampania, this.estadoGrid);
    this.cargandoGrid = false;

    if (resp.exito === 0) {
      return Swal.fire(
        "Error al Consultar",
        "Se produjo un error en el sistema intente nuevamente",
        "error"
      );
    }
    Swal.close();
    //Swal.fire('Consulta Exitosa', '', 'success');
  }

  GetCuentasCampaniaBusquedaEstado(IdCampania, IdUsuario, descripcion) {
    this.cargandoGrid = true;
    this.datoGrid = undefined;
    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();

    this.serviceCarmpania
      .obtenerListaEstadosBusqueda(IdCampania, IdUsuario, descripcion)
      .subscribe((resp) => {
        if (resp.exito === 0) {
          return Swal.fire(
            "Error al Consultar",
            "Se produjo un error en el sistema intente nuevamente",
            "error"
          );
        } else {
          this.infoCrmCuentasCampania = resp.data;
          console.log("listado", this.infoCrmCuentasCampania);
          // for (const iterator of this.infoCrmCuentasCampania) {
          //   iterator.id = uuidv4();
          //   const selector = gestionNormalActions.agregarGestionNormal({
          //     gestionNormal: iterator,
          //   });
          //   this.store.dispatch(selector);
          // }
          this.totalfiltro = this.infoCrmCuentasCampania.length; // ITA
          console.log("total normal estados: ",this.totalfiltro); // ITA

          this.datoGrid = process(this.infoCrmCuentasCampania, this.estadoGrid);
          this.cargandoGrid = false;
        }
        Swal.close();
      });

    //Swal.fire('Consulta Exitosa', '', 'success');
  }

  EventoEstadoGrid(estadoGrid: any) {
    // debugger;
    console.log("estados del grid", estadoGrid);

    // console.log("ver ",estadoGrid.filter);
    console.log("ver ",estadoGrid.filter.filters);
    let filtrovalue = estadoGrid.filter.filters;
    filtrovalue.forEach(element => {
      console.log("element ",element.filters);
      let ele = element.filters;
      ele.forEach(element => {
        console.log("element2 ",element.value);
        let valor = element.value;
        // console.log("si ", valor.getDate());
        console.log("typeof ",typeof valor);
        if (typeof valor == Date()) {
          console.log("si3 ", valor.toLocaleDateString());
          
          let fechai = format(valor.toLocaleDateString(), "yyyy-MM-ddT00:00:00");
          fechai = new Date(fechai.toString().split('UTC')[0]).toISOString();
          console.log("fechai ", fechai);
          // console.log(new Date(fechai.toString().split('GMT')[0]+' UTC'));
          // console.log(new Date(fechai.toString().split('GMT')[0]));
          // console.log(new Date(fechai.toString().split('GMT')[0]).toISOString());
          // console.log(new Date(fechai.toString().split('GMT')[0]).toUTCString());
          // console.log(new Date(fechai.toString().split('GMT')[0]).toLocaleDateString());
          // console.log(new Date(fechai.toString().split('UTC')[0]));
          // console.log(new Date(fechai.toString().split('UTC')[0]).toISOString());
          element.value = fechai;
        }
      });
    });

    console.log("estados del grid mod ", estadoGrid);
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoCrmCuentasCampania, this.estadoGrid);
    console.log(
      "nuevos datos",
      process(this.infoCrmCuentasCampania, this.estadoGrid)
    );
    let estadoEditado = { ...estadoGrid };
    estadoEditado.skip = 0;
    estadoEditado.take = this.datoGrid.total;

    console.log(
      "nuevos datos pero ya con la maldad xd!",
      process(this.infoCrmCuentasCampania, estadoEditado)
    );
    this.busquedaDinamicaGrid = process(
      this.infoCrmCuentasCampania,
      estadoEditado
    );
    this.totalfiltro = process(this.infoCrmCuentasCampania, estadoEditado).data.length; // ITA
    console.log("filtro tbl normal: ", this.totalfiltro); // ITA

    // const result = estadoGrid.filter.filters.map((f) => f.value);
    // console.log("resultado recorrido", result);
    // if (!isNullOrUndefined(estadoGrid.filter.filters)) {
    //   if (!isNullOrEmptyString(estadoGrid.filter.filters[0].value)) {
    //     this.infoCrmCuentasCampaniaApi = this.infoCrmCuentasCampania.filter(
    //       (x) =>
    //         x.nombrecliente
    //           .toLowerCase()
    //           .indexOf(
    //             estadoGrid.filter.filters[0].value.toString().toLowerCase()
    //           ) !== -1
    //     );
    //   }
    // }
    if (this.totalfiltro>0) {
      this.filtroactivado = true;
      console.log("this.filtroactivado ", this.filtroactivado);
    }

    console.log(this.infoCrmCuentasCampaniaApi);
  }

  cellClickHandler({ dataItem, rowIndex }) {
    console.log("click en la celda", rowIndex);
    const fila = Number(rowIndex) + 1;
    let datosDinamicos: any;
    this.infoCrmCuentasCampaniaseleccionada = dataItem;
    console.log("toda la info", this.infoCrmCuentasCampania);
    console.log("info filtrada", this.infoCrmCuentasCampaniaApi);
    if (!this.busquedaDinamicaGrid) {
      datosDinamicos = this.infoCrmCuentasCampania;
    } else {
      datosDinamicos = this.busquedaDinamicaGrid.data;
    }
    console.log("resultados dinamicos filtrados", datosDinamicos);
    this.serviceCarmpania
      .enviarDatos(datosDinamicos)
      .subscribe((data) => console.log("resuesta cargar datos", data));
    this.router.navigate([
      `gestionar/gestionar/gestionid/${this.infoCrmCuentasCampaniaseleccionada.idcuentacampania}/${this.infoCrmCuentasCampaniaseleccionada.idproducto}/${fila}`,
    ]);
  }

  FiltroComboCartera(valor) {
    this.listaCartera = this.InfoCartera.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }

  FiltroComboCampania(valor) {
    this.listaCampaniafiltrada = this.listaCampaniaEmergente.filter(
      (s) => s.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }

  EventoComboCartera(valor: CrmCartera) {
    this.datoGrid = undefined;
    this.valorComboCampania = undefined;
    if (!isNullOrUndefined(valor)) {
      this.listaCampaniafiltrada = [];
      this.listaCampaniafiltrada = this.listaCampania.filter(
        (x) => x.idCartera === valor.id
      );
      this.listaCampaniaEmergente = this.listaCampaniafiltrada;
    } else {
      this.listaCampaniafiltrada = [];
    }

    this.global.valorDropGestionNormal.cartera = valor;
    this.GuardarValorDrop(this.global.valorDropGestionNormal);
  }

  EventoComboCampania(valor: CrmCampania) {
    this.datoGrid = undefined;
    if (!isNullOrUndefined(valor)) {
      setTimeout(() => {
        this.GetCuentasCampania(valor.id, this.usuarioLogin.idUsuario);
        this.obtenerListaEstados(valor.id, this.usuarioLogin.idUsuario);
        this.obtenerFiltroCreado();
        this.global.valorDropGestionNormal.campania = valor;
        console.log("valor: ", this.global.valorDropGestionNormal.campania);
        this.GuardarValorDrop(this.global.valorDropGestionNormal);
      }, 200);
    }
  }

  LimpiarFiltrosGrid(evento) {
    this.estadoGrid = {
      skip: 0,
      take: 10,
      filter: { logic: "and", filters: [] },
    };

    this.valorComboCampania = undefined;
    this.valorComboCartera = undefined;
    this.LimpiarDropGlobalGestionNormal();
    this.infoCrmCuentasCampania = [];
    this.datoGrid = process(this.infoCrmCuentasCampania, this.estadoGrid);
    this.totalfiltro = null; // ITA
    this.total = null; // ITA
    this.filtroscreados = [];
    this.filtroactivado = false;
  }

  LimpiarDropGlobalGestionNormal() {
    this.global.valorDropGestionNormal.cartera = this.valorComboCartera;
    this.global.valorDropGestionNormal.campania = this.valorComboCampania;
    sessionStorage.removeItem(valorDropGestionNormal);
  }

  TableroTieneFiltro() {
    return this.estadoGrid.filter.filters.length > 0 ||
      !isNullOrUndefined(this.valorComboCartera) ||
      !isNullOrUndefined(this.valorComboCampania)
      ? true
      : false;
  }

  GuardarValorDrop(valorGlobal: ValorDropGestionNormal) {
    this.global.valorDropGestionNormal = valorGlobal;
    sessionStorage.setItem(
      valorDropGestionNormal,
      btoa(JSON.stringify(valorGlobal))
    );
  }

  LlenaDropGlobalGestionNormal(valorGlobal: ValorDropGestionNormal) {
    this.listaCampaniafiltrada = [];
    this.listaCampaniafiltrada.push(valorGlobal.campania);
    this.valorComboCartera = valorGlobal.cartera;
    this.valorComboCampania = valorGlobal.campania;
  }

  LeerFiltroString(filtro: string) {
    return JSON.parse(atob(filtro));
  }
  //
  // public handleOpen(event: any): void {
  //   // call event.preventDefault() to prevent the open attempt
  //     console.log('open', event);
  // }

  // public handleClose(event: any): void {
  //     // call event.preventDefault() to prevent the close attempt
  //     console.log('close', event);
  // }

  // POP UP ALERTAS
  public toggleText = "VER";
  public show = false;
  public showEstado = false;
  public toggleTextEstado = "VER";

  public onToggle(): void {
    this.show = !this.show;
    this.toggleText = this.show ? "OCULTAR" : "VER";
  }

  public onToggleEstado(): void {
    this.showEstado = !this.showEstado;
    this.toggleTextEstado = this.showEstado ? "OCULTAR" : "VER";
  }

  seleccionarAlerta(evento: any): void {
    console.log(evento.dataItem);
    this.router.navigate([
      `gestionar/gestionar/gestionid/${evento.dataItem.idcuentacampania}/${evento.dataItem.idproducto}/alerta`,
    ]);
  }

  seleccionarEstado(evento: any): void {
    console.log(evento.dataItem);
    if (evento.dataItem.estado != "TOTAL CAMPAÑA") {
      this.GetCuentasCampaniaBusquedaEstado(
        this.valorComboCampania.id,
        this.usuarioLogin.idUsuario,
        evento.dataItem.estado
      );
    } else {
      this.GetCuentasCampania(
        this.valorComboCampania.id,
        this.usuarioLogin.idUsuario
      );
    }
  }

  public colorCode(code: string): SafeStyle {
    let result;

    switch (code) {
      case "TOTAL CAMPAÑA":
        result = "#FFBA80";
        break;

      default:
        result = "transparent";
        break;
    }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public colorCodeNumero(code: any): SafeStyle {
    let upper;
    let result;
    if (code.estado) {
      upper = code.estado.toUpperCase();
    } else {
      upper = code.estado;
    }

    if (upper === "SIN GESTION" || code.texto === "Negativo") {
      result = "#e60e11";
    } else if (code.texto === "Positivo" && !code.alerta) {
      result = "#f1f52a";
    } else if (code.texto === "Positivo" && code.alerta) {
      result = "#25c22d";
    }

    // switch (upper) {
    //   case "SIN GESTION":
    //     result = "#d1737d";
    //     break;
    //   case "NEGATIVO":
    //     result = "#d1737d";
    //     break;
    //   case "POSITIVA":
    //     result = "#ede06d";
    //     break;

    //   default:
    //     result = "transparent";
    //     break;
    // }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public filter: CompositeFilterDescriptor;
  // public estadoss: any[] = filterBy(infoCrmCuentasCampania, this.filter);

  // public estadolist = this.estadoListaCantidad;

  private estadoFilter: any[] = [];
  private previousEstadoFilter: CompositeFilterDescriptor[] = [];

  public filterChange(filter: CompositeFilterDescriptor): void {
    console.log("filterChange", filter);

    let catFilter = filter.filters
      .map((f: CompositeFilterDescriptor) => {
        return f.filters.find(
          (f: FilterDescriptor) => f.field === "estado"
        ) as CompositeFilterDescriptor;
      })
      .filter((x) => !!x);

    console.log("catFilter", catFilter);
    console.log("catFilter.length", catFilter.length);
    console.log("this.previousEstadoFilter", this.previousEstadoFilter);
    console.log(
      "this.previousEstadoFilter.length",
      this.previousEstadoFilter.length
    );

    if (this.previousEstadoFilter.length === 0 && catFilter.length > 0) {
      console.log("save value");
      this.previousEstadoFilter = catFilter;
    } else if (this.previousEstadoFilter.length > 0 && catFilter.length === 0) {
      console.log("clear");
      this.previousEstadoFilter = [];
    }

    this.filter = filter;
    // this.infoCrmCuentasCampania = filterBy(this.infoCrmCuentasCampania, filter);
  }

  public estadoChange(values: any[], filterService: FilterService): void {
    filterService.filter({
      filters: values.map((value) => ({
        field: "estado",
        operator: "eq",
        value,
      })),
      logic: "or",
    });
    console.log("estadoChange 2", values);
  }
  public estadoChange2(
    values: any[],
    filterService: FilterService,
    tipo: string
  ): void {
    console.log("estadoChange", values);
    filterService.filter({
      filters: values.map((value) => ({
        field: tipo,
        operator: "eq",
        value,
      })),
      logic: "or",
    });
    console.log("fechaa ",tipo);
    console.log(filterService);
    console.log(values);
  }

  public estadoFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.estadoFilter.splice(
      0,
      this.estadoFilter.length,
      ...flatten(filter).map(({ value }) => value)
    );
    return this.estadoFilter;
  }

  revisar(data: any): any {
    if (data === "") {
      return null;
    } else {
      return data;
    }
  }

  GuardarFiltro(){
    console.log("Hola");
    let datafiltro = [];
    for (const iterator of this.busquedaDinamicaGrid.data) {
      const dato = {
        asesor: this.revisar(iterator.asesor),
        nocuenta: this.revisar(iterator.idcuenta),
        nombrecompleto: this.revisar(iterator.nombrecliente),
        identificacion: this.revisar(iterator.cedula),
        ultimagestion: this.revisar(iterator.ultimagestion),
        gestion: this.revisar(iterator.estado),
        diasmora: this.revisar(iterator.idcuentacampania),
        mejorgestion: this.revisar(iterator.mejorestado),
        fechamejorgestion: this.revisar(iterator.fechamejorestado),
        deudatotal: this.revisar(iterator.deudatotal),
        statuscartera: this.revisar(iterator.statuscartera),
        fechaacuerdo: this.revisar(iterator.acuerdo),
        valoracuerdo: this.revisar(iterator.valoracuerdo),
        valorpago: this.revisar(iterator.valorpago),
        fechaPago: this.revisar(iterator.pago),
        totalacuerdo: this.revisar(iterator.totalacuerdo),
        totalpago: this.revisar(iterator.totalpago),
        cuota: this.revisar(iterator.cuotaspendientes),
        totalcuotas: this.revisar(iterator.idproducto),
        deudavencida: this.revisar(iterator.deudavencida),
        extra1: this.revisar(iterator.extra1),
        extra2: this.revisar(iterator.extra2),
        extra3: this.revisar(iterator.extra3),
        extra4: this.revisar(iterator.extra4),
        extra5: this.revisar(iterator.extra5),
        extra6: this.revisar(iterator.extra6),
        extra7: this.revisar(iterator.extra7),
        extra8: this.revisar(iterator.extra8),
        extra9: this.revisar(iterator.extra9),
        extra10: this.revisar(iterator.extra10),
      };
      datafiltro.push(dato);
    }
    this.modalService.show(ModalGuardarFiltroGestionComponent, {
      initialState: {
        detalle: datafiltro,
        idcampania: this.valorComboCampania.id,
        filtro: this.estadoGrid,
        // idCampania: Number(this.params.GestionID),
        // arbolSelecciondo: this.arbolSelecciondo,
        // idProducto: Number(this.params.ProductoID),
      },
      backdrop: "static",
    });
    console.log("datafiltro ",datafiltro);
    console.log("this.valorComboCampania.id ",this.valorComboCampania.id);
    console.log("this.estadoGrid ",this.estadoGrid);
  }

  obtenerFiltroCreado(): void {
    let usuario = this.usuarioLogin.idUsuario.toString();
    this._multicanalService
      .obtenerFiltrosGestion(this.valorComboCampania.id,usuario)
      .subscribe((response) => {
        this.filtroscreados = response.data;
        // this.filtrados = [...this.filtros];
        console.log("filtros creados", response);
      });
  }

  cellClickHandlerFiltro({ dataItem, rowIndex }) {
    // this.valorComboCartera = null;
    // this.valorComboCampania = null;
    this.listaCampaniafiltrada = [];
    console.log("dataItem.idmulticanal ",dataItem.idmulticanal);
    this._multicanalService.obtenerDetalleFiltrosGestion(this.valorComboCampania.id,dataItem.idmulticanal).subscribe((respuesta) => {
      this.infoCrmCuentasCampania = respuesta.data;
      console.log("click en la celda", dataItem);
      this.total = this.infoCrmCuentasCampania.length; // ITA
      console.log("filtros: ",this.total); // ITA
      console.log(
        "listado al seleccionar un filtro",
        this.infoCrmCuentasCampania
      );
      this.datoGrid = process(this.infoCrmCuentasCampania, this.estadoGrid);
      this.cargandoGrid = false;
      this.totalfiltro = this.infoCrmCuentasCampania.length;  // ITA

      if (respuesta.exito === 0) {
        return Swal.fire(
          "Error al Consultar",
          "Se produjo un error en el sistema intente nuevamente",
          "error"
        );
      }
      Swal.close();
    });
  }

  cambiosModal(): void {
    this.modalService.onHidden.pipe(takeUntil(this.stop$)).subscribe((data) => {
      switch (data) {
        case "filtro":
          this.obtenerFiltroCreado();
          break;
        default:
          break;
      }
    });
  }
}
