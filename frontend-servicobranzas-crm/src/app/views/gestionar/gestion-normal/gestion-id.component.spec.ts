
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionByIdComponent } from './gestion-id.component';

describe('GestionByIdComponent', () => {
  let component: GestionByIdComponent;
  let fixture: ComponentFixture<GestionByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
