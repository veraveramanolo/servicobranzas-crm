export const tipo: Array<{ id: number; descripcion: string }> = [
  {
    id: 1,
    descripcion: "RESIDENCIA",
  },
  {
    id: 2,
    descripcion: "INTERNACIONAL",
  },
  {
    id: 3,
    descripcion: "OTRO",
  },
  {
    id: 4,
    descripcion: "MOVIL OTRO",
  },
  {
    id: 5,
    descripcion: "TRABAJO",
  },
  {
    id: 6,
    descripcion: "MOVIL PERSONAL",
  },
  {
    id: 1,
    descripcion: "FAMILIAR",
  },
];

export const depto = [
  {
    id: 1,
    descripcion: "AZUAY",
  },
  {
    id: 2,
    descripcion: "GUAYAS",
  },
];

export const ciudad = [
  {
    id: 1,
    idDepto: 1,
    descripcion: "CAMILO PONCE",
  },
  {
    id: 2,
    idDepto: 1,
    descripcion: "CUENCA",
  },
  {
    id: 3,
    idDepto: 2,
    descripcion: "BALAO",
  },
  {
    id: 4,
    idDepto: 2,
    descripcion: "BALZAR",
  },
];

export const distrito = [
  {
    id: 1,
    idCiudad: 1,
    descripcion: "CAMILO PONCE",
  },
  {
    id: 2,
    idCiudad: 1,
    descripcion: "BAÑOS",
  },
];
