import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { DinomiService } from "../../../services/dinomi.service";

@Component({
  selector: "app-modal-sms",
  templateUrl: "./modal-sms.component.html",
  styleUrls: ["./modal-sms.component.css"],
})
export class ModalSmsComponent implements OnInit {
  Item: any;
  smsForm: FormGroup;
  constructor(
    public modalRef: BsModalRef,
    private readonly fb: FormBuilder,
    private readonly _dinomiService: DinomiService,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.form();
  }
  private form(): void {
    this.smsForm = this.fb.group({
      mensaje: ["", Validators.required],
    });
  }

  enviarSms(event: any): void {
    const contenido = this.smsForm.value;
    this._dinomiService
      .smsDinomi(this.Item.numero, contenido.mensaje)
      .subscribe((respuesta) => {
        if (respuesta.respuesta === "success") {
          this.smsForm.reset();

          this.modalService.setDismissReason("ok");
          this.modalRef.hide();
          Swal.fire({
            title: "Correcto!",
            html: "Mensaje enviado!",
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: "Error al enviar el mnsaje!",
            icon: "error",
          });
        }
      });
  }
}
