import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import Swal from "sweetalert2";
import { ContactoService } from "../../../services/contacto.service";
import { EmailService } from "../../../services/email.service";
import { InfoContactoService } from "../../../services/info-contacto.service";
import { UbicacionService } from "../../../services/ubicacion.service";
export enum tipoBoton {
  FijoMovil = "fijo/movil",
  Direcciones = "direcciones",
  Email = "email",
  PersonasContacto = "personaContacto",
}
@Component({
  selector: "app-modal-activar-desactivar",
  templateUrl: "./modal-activar-desactivar.component.html",
  styleUrls: ["./modal-activar-desactivar.component.css"],
})
export class ModalActivarDesactivarComponent implements OnInit {
  tipobot = tipoBoton;
  form: FormGroup;
  Dato: any;
  Tipo: string;
  constructor(
    public modalRef: BsModalRef,
    private readonly _infoContactoService: InfoContactoService,
    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService,
    private readonly _emailService: EmailService,
    private readonly _ubicacionService: UbicacionService,
    private readonly _contactoService: ContactoService
  ) {}

  ngOnInit(): void {
    this.formulario();
  }

  private formulario(): void {
    this.form = this.fb.group({
      observacionI: ["", Validators.required],
    });
  }

  inactivarDinamico(event: any): void {
    switch (this.Tipo) {
      case this.tipobot.FijoMovil:
        this.inactivarTelefono(event);
        break;
      case this.tipobot.Direcciones:
        this.incativarDireccion(event);
        break;
      case this.tipobot.Email:
        this.incativarEmail(event);
        break;
      case this.tipobot.PersonasContacto:
        this.incativarPersonaContacto(event);
        break;

      default:
        break;
    }
  }

  incativarDireccion(event: any): void {
    console.log(this.Dato);
    event.preventDefault();
    const contenido = this.form.value;
    contenido.id = Number(this.Dato.idDireccion);
    contenido.estado = "I";
    this._ubicacionService
      .inactivarDireccion(contenido)
      .subscribe((respuesta) => {
        if (respuesta.exito === 1) {
          this.form.reset();

          this.modalService.setDismissReason(this.Tipo);
          this.modalRef.hide();
          Swal.fire({
            title: "Correcto!",
            html: `${respuesta.mensage}`,
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: `${respuesta.mensage}`,
            icon: "error",
          });
        }
      });
  }

  incativarEmail(event: any): void {
    console.log(this.Dato);
    event.preventDefault();
    const contenido = this.form.value;
    contenido.id = Number(this.Dato.idEmail);
    contenido.estado = "I";
    this._emailService.inactivarEmail(contenido).subscribe((respuesta) => {
      if (respuesta.exito === 1) {
        this.form.reset();

        this.modalService.setDismissReason(this.Tipo);
        this.modalRef.hide();
        Swal.fire({
          title: "Correcto!",
          html: `${respuesta.mensage}`,
          timer: 3000,
          timerProgressBar: true,
          showConfirmButton: false,
          icon: "success",
        });
      } else {
        Swal.fire({
          allowOutsideClick: false,
          text: `${respuesta.mensage}`,
          icon: "error",
        });
      }
    });
  }

  incativarPersonaContacto(event: any): void {
    console.log("persona contacto", this.Dato);
    event.preventDefault();
    const contenido = this.form.value;
    contenido.id = Number(this.Dato.idContacto);
    contenido.estado = "I";
    this._contactoService
      .inactivarContacto(contenido)
      .subscribe((respuesta) => {
        if (respuesta.exito === 1) {
          this.form.reset();

          this.modalService.setDismissReason(this.Tipo);
          this.modalRef.hide();
          Swal.fire({
            title: "Correcto!",
            html: `${respuesta.mensage}`,
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: `${respuesta.mensage}`,
            icon: "error",
          });
        }
      });
  }

  inactivarTelefono(event: any): void {
    console.log(this.Dato);
    event.preventDefault();
    const contenido = this.form.value;
    contenido.id = Number(this.Dato.idtelefono);
    contenido.estado = "I";
    this._infoContactoService
      .inactivarTelefono(contenido)
      .subscribe((respuesta) => {
        if (respuesta.exito === 1) {
          this.form.reset();

          this.modalService.setDismissReason(this.Tipo);
          this.modalRef.hide();
          Swal.fire({
            title: "Correcto!",
            html: `${respuesta.mensage}`,
            timer: 3000,
            timerProgressBar: true,
            showConfirmButton: false,
            icon: "success",
          });
        } else {
          Swal.fire({
            allowOutsideClick: false,
            text: `${respuesta.mensage}`,
            icon: "error",
          });
        }
      });
  }
}
