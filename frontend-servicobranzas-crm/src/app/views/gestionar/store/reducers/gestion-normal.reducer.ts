import { createReducer, on } from "@ngrx/store";
import * as gestionNomralActions from "../actions";
import {
  GestionNomalListadoAdapter,
  GestionNomalListadoState,
} from "../states/gestion-normal-listado.state";

export const initialState: GestionNomalListadoState = GestionNomalListadoAdapter.getInitialState();

export const gestionNormalReducer = createReducer(
  initialState,

  on(gestionNomralActions.agregarGestionNormal, (state, { gestionNormal }) => {
    return GestionNomalListadoAdapter.addOne(gestionNormal, state);
  })
);
