import { GestionNormalModuleState } from "../states/gestion-normal.state";
import { gestionNormalReducer } from "./gestion-normal.reducer";

import {
  ActionReducerMap,
  createFeatureSelector,
  MetaReducer,
} from "@ngrx/store";
import { environment } from "../../../../../environments/environment";

export const reducers: ActionReducerMap<GestionNormalModuleState> = {
  gestionNormalListado: gestionNormalReducer,
};

export const metaReducers: MetaReducer<GestionNormalModuleState>[] = !environment.production
  ? []
  : [];

export const getMenuModuleState = createFeatureSelector<GestionNormalModuleState>(
  "gestion-normal-module"
);
