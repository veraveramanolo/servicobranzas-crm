import { createAction, props } from "@ngrx/store";
import { CrmConsultaGestionCampania } from "../../../../models/CrmConsultaGestionCampania";

export const agregarGestionNormal = createAction(
  "[GestionarModule] agregarListaDeGestionNormal",
  props<{ gestionNormal: CrmConsultaGestionCampania }>()
);
