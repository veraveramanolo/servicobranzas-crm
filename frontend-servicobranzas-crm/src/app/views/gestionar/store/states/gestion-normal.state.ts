import { GestionNomalListadoState } from "./gestion-normal-listado.state";

export interface GestionNormalModuleState {
  gestionNormalListado: GestionNomalListadoState;
}
