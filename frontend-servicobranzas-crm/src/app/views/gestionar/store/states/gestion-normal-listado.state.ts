import { EntityState, EntityAdapter, createEntityAdapter } from "@ngrx/entity";
import { CrmConsultaGestionCampania } from "../../../../models/CrmConsultaGestionCampania";

// tslint:disable-next-line: no-empty-interface
export interface GestionNomalListadoState
  extends EntityState<CrmConsultaGestionCampania> {}
// export function selectMenuId(a: CrmConsultaGestionCampania): number {
//   //In this case this would be optional since primary key is id
//   return a.idcuenta;
// }

export const GestionNomalListadoAdapter: EntityAdapter<CrmConsultaGestionCampania> = createEntityAdapter<CrmConsultaGestionCampania>();
//   {
//     selectId: selectMenuId,
//   }
