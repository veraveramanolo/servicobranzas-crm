import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { groupBy } from "@progress/kendo-data-query";
import { format } from "date-fns";
import { CampaniaService } from "../../../services/campania.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-modal-ver-info-cliente',
  templateUrl: './modal-ver-info-cliente.component.html',
  styleUrls: ['./modal-ver-info-cliente.component.css']
})
export class ModalVerInfoClienteComponent implements OnInit {

  idCuentaCampania: number;
  listaTelefonos: any;
  listaDirecciones: any;
  detalleCliente: any;
  data: any;
  public groupedDataDir: any;
  public groupedDataTel: any;

  public DirSelect: any;
  public TelSelect: any;

  fileName: string;
  public impresiontime = null;

  constructor(
    public modalRef: BsModalRef,
    private readonly _campaniaService: CampaniaService,
  ) { }

  ngOnInit(): void {
    this.listaDirecciones;
    this.groupedDataDir = groupBy(this.listaDirecciones, [{ field: "tipo" }]);
    this.groupedDataTel = groupBy(this.listaTelefonos, [{ field: "tipo" }]);
    this.fileName = this.detalleCliente.nombre+"_"+format(new Date(), "yyyy-MM-dd")+".pdf";
    this.impresiontime = format(new Date(), "yyyy-MMM-dd H:mm:ss");
    this.guardarGestion();
  }
  
  guardarGestion(): void {
    this._campaniaService
      .guardarGestion(this.idCuentaCampania, this.data)
      .subscribe((respuesta) => {
        console.log(respuesta.data);
        if (respuesta.exito !== 1) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: `${respuesta.mensage}`,
          });
        }
      });
  }

}
