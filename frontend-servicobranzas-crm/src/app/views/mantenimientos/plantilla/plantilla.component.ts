import { Component, OnInit } from "@angular/core";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { VariableGlobalService } from "../../../services/variable-global.service";
import Swal from "sweetalert2";
import { State, process } from "@progress/kendo-data-query";
import { PlantillaService } from "../../../services/plantilla.service";
import { AclPlantilla } from "../../../models/AclPlantilla";
import { Observable } from "rxjs";

@Component({
  selector: "app-plantilla",
  templateUrl: "./plantilla.component.html",
  styleUrls: ["./plantilla.component.css"],
})
export class PlantillaComponent implements OnInit {
  //public gridState: State;  falta api
  public infoAclPlantilla: Observable<AclPlantilla[]>; //Llena tabla Tareas

  constructor(
    private plantillaService: PlantillaService,
    private global: VariableGlobalService
  ) {}

  ngOnInit(): void {
    //this.gridState = { sort: [], skip: 0, take: 10, filter: { logic: 'and', filters: [] } } falta api
    this.ConsultarPlantillas();
  }

  ConsultarPlantillas() {
    // Swal.fire({
    //   allowOutsideClick: false,
    //   text: "Espere por favor...",
    //   icon: "info",
    // });
    // Swal.showLoading();
    //this.perfilesRequest = {user : '', password :''};
    // let respuesta =  this.plantillaService.ConsultarPlantillas();
    // console.log("lista de plantillas: ", respuesta);
    this.infoAclPlantilla = this.plantillaService.ConsultarPlantillas();
    // if (respuesta.exito === 0) {
    //   Swal.close();
    //   return Swal.fire({
    //     allowOutsideClick: false,
    //     text: respuesta.mensage,
    //     icon: "error",
    //   });
    // }
    // if (respuesta.exito === 1) {
    //   console.log("array de plantillas: ", respuesta.data);
    //   this.infoAclPlantilla = respuesta.data;
    // }
    // Swal.close();
  }
  //arma Kendo Dialog
  /*public active = false;
  public editForm: FormGroup = new FormGroup({
      ProductID: new FormControl(),
      ProductName: new FormControl('', Validators.required),
      UnitPrice: new FormControl(0),
      UnitsInStock: new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[0-9]{1,3}')])),
      Discontinued: new FormControl(false)
  });*/
  //kendo dialog
}
