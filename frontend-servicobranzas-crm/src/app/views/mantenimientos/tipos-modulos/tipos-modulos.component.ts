import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { VariableGlobalService } from '../../../services/variable-global.service';
import { AclTipoModulos } from '../../../models/AclTipoModulos';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModeloRespuesta } from '../../../models/ModeloRespuesta.models';
import { TipoModulosService } from '../../../services/tipo-modulos.service';
import Swal from 'sweetalert2';
import { isNullOrUndefined } from 'util';

const codigoIng = 'ING';
const listaSiNo = [{ 'Codigo': 'SI', 'Descripcion': 'SI' }, { 'Codigo': 'NO', 'Descripcion': 'NO' }];
@Component({
  selector: 'app-tipos-modulos',
  templateUrl: './tipos-modulos.component.html',
  styleUrls: ['./tipos-modulos.component.css']
})
export class TiposModulosComponent implements OnInit {

  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public infoAclTipoModulo: AclTipoModulos[];
  private editedRowIndex: number;
  public formulario: FormGroup;
  public creacionEdicion: boolean;
  public cargandoGrid: boolean;
  public datoAnulado = listaSiNo;
  public codigoProceso: string;


  constructor(private global: VariableGlobalService, 
    private servicioTipoModulos: TipoModulosService) { }

  ngOnInit(): void {
    this.estadoGrid = { skip: 0, take: 10, filter: { logic: 'and', filters: [] }};
    this.ConsultarAllTiposModulos();


  }


  async ConsultarAllTiposModulos(){
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.servicioTipoModulos.All();
    this.infoAclTipoModulo = respuesta.data;
    debugger
    this.datoGrid = process(this.infoAclTipoModulo, this.estadoGrid);
    this.cargandoGrid = false;

    console.log(this.infoAclTipoModulo);
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoAclTipoModulo, this.estadoGrid);
  }

  AgregarRegistroGrid({sender}){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      descripcion: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      anulado: new FormControl('NO', Validators.required),

    });

    sender.addRow(this.formulario);

  }

  CerrarFilaGrid(grid, rowIndex = this.editedRowIndex){
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formulario = undefined;
  }

  CancelarEdicionFila({ sender, rowIndex }) {
    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
  }

  EditarFilaGrid({ sender, rowIndex, dataItem }){
    this.CerrarFilaGrid(sender);

  }

  async GuardarFilaGrid({ sender, rowIndex, formGroup: FormGroup, isNew }){
    debugger
    let infoTipoModulo: AclTipoModulos = await this.ObtenerInfoTipoModulos(this.formulario);

    if(this.formulario.invalid){
      return Swal.fire('Información Invalida', 'Debe haber un error el la imformación ingresada', 'error');
    }

    if(this.InfoDuplicada(this.infoAclTipoModulo  ,infoTipoModulo)){
      return Swal.fire('Informacióm duplicada', 'Ya existe un registro con esos detalles. Favor modifique e intente nuevamente', 'error');
    }
    
    let resp: ModeloRespuesta = await this.servicioTipoModulos.Crear(infoTipoModulo); 
    if (resp.exito === 0) {
      return Swal.fire('Error al grabar', 'Se produjo un error en el sistema intente nuevamente', 'error');
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire('Datos Grabados', 'La información se grabo con exito', 'success');
    return await this.ConsultarAllTiposModulos();
  }


  InfoDuplicada(infoAclTipoModulo: AclTipoModulos[], infoTipoModulo: AclTipoModulos): boolean {
    let infoDuplicada : AclTipoModulos;
    infoDuplicada = infoAclTipoModulo.find(x => x.descripcion.trim().toLocaleUpperCase() === infoTipoModulo.descripcion.trim().toLocaleUpperCase());
    if (isNullOrUndefined(infoDuplicada)) {
      return false
    } else {
      return true
    }
  }






   ObtenerInfoTipoModulos(formulario: FormGroup): AclTipoModulos {
     return {
      descripcion : formulario.controls['descripcion'].value,
      anulado : formulario.controls['anulado'].value,
      aclModulos: undefined,
      fechaCreacion: undefined,
      fechaModificacion: undefined,
      idTipoModulos: undefined,
      usrCreacion: 'Jcontreras',
      usrModificacion: undefined
    };
  }






}
