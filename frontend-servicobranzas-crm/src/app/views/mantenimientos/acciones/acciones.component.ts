import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import Swal from 'sweetalert2';
import { isNullOrUndefined } from 'util';
import { AclAcciones } from '../../../models/AclAcciones';
import { AclUsuario } from '../../../models/AclUsuario';
import { ModeloRespuesta } from '../../../models/ModeloRespuesta.models';
import { AccionesService } from '../../../services/acciones.service';
import { VariableGlobalService } from '../../../services/variable-global.service';
const codigoIng = 'ING';
const codigoMod = 'MOD';
const listaSiNo = [{ 'Codigo': 'SI', 'Descripcion': 'SI' }, { 'Codigo': 'NO', 'Descripcion': 'NO' }];

@Component({
  selector: 'app-acciones',
  templateUrl: './acciones.component.html',
  styleUrls: ['./acciones.component.css']
})
export class AccionesComponent implements OnInit {

  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public infoAclAcciones: AclAcciones[];
  private editedRowIndex: number;
  public formulario: FormGroup;
  public creacionEdicion: boolean;
  public cargandoGrid: boolean;
  public datoAnulado = listaSiNo;
  public codigoProceso: string;
  public usuarioLogin: AclUsuario;

  constructor(private serviveAcciones: AccionesService,  private global: VariableGlobalService) { }

  ngOnInit(): void {
    this.estadoGrid = { skip: 0, take: 10, filter: { logic: 'and', filters: [] }};
    this.ConsultarUsuarioLogin();
    this.ConsultarAllAcciones();
  }


  ConsultarUsuarioLogin(){
    this.usuarioLogin =  this.global.ConsultarUsuarioLogin();
  }


  async ConsultarAllAcciones() {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.serviveAcciones.All();
    this.infoAclAcciones = respuesta.data;
    debugger
    this.datoGrid = process(this.infoAclAcciones, this.estadoGrid);
    this.cargandoGrid = false;

    console.log(this.infoAclAcciones);
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoAclAcciones, this.estadoGrid);
  }

  AgregarRegistroGrid({sender}){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      anulado: new FormControl('NO', Validators.required),
      codigo: new FormControl('', [Validators.required, Validators.minLength(3),Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(3)]),
      icono: new FormControl(''),

    });

    sender.addRow(this.formulario);

  }

  CerrarFilaGrid(grid, rowIndex = this.editedRowIndex){
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formulario = undefined;
  }

  CancelarEdicionFila({ sender, rowIndex }) {
    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
  }

  EditarFilaGrid({ sender, rowIndex, dataItem }){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoMod;

    this.formulario = new FormGroup({
      nombre: new FormControl( dataItem.nombre, [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      anulado: new FormControl(dataItem.anulado, Validators.required),
      codigo: new FormControl(dataItem.codigo, [Validators.required, Validators.minLength(3),Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(3)]),
      icono: new FormControl(dataItem.icono),      
      idAccion : new FormControl(dataItem.idAccion)
    });

    this.formulario.controls['idAccion'].disable();

    this.creacionEdicion = true;
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formulario);
  }

  async GuardarFilaGrid({ sender, rowIndex, formGroup: FormGroup, isNew }){
    let infoAcciones: AclAcciones = await this.ObtenerInfoTAcciones(this.formulario);

    if(this.formulario.invalid){
      return Swal.fire('Información Invalida', 'Debe haber un error el la información ingresada', 'error');
    }

    if(this.InfoDuplicada(this.infoAclAcciones  ,infoAcciones)){
      return Swal.fire('Información duplicada', 'Ya existe un registro con esos detalles. Favor modifique e intente nuevamente', 'error');
    }
    let resp: ModeloRespuesta = undefined;
    if(this.codigoProceso === codigoIng)
      resp = await this.serviveAcciones.Crear(infoAcciones);

    if(this.codigoProceso === codigoMod)
      resp = await this.serviveAcciones.Modificar(infoAcciones);
    

    if (resp.exito === 0) {
      return Swal.fire('Error al grabar', 'Se produjo un error en el sistema intente nuevamente', 'error');
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire('Datos Grabados', 'La información se grabo con exito', 'success');
    return await this.ConsultarAllAcciones();
  }


  InfoDuplicada(infoAclAcciones: AclAcciones[], infoAcciones: AclAcciones): boolean {
    let infoDuplicada : AclAcciones;
    let infoFiltrada: AclAcciones[] = infoAclAcciones.filter(x => x.idAccion !== infoAcciones.idAccion);
    infoDuplicada = infoFiltrada.find(x => x.codigo.trim().toLocaleUpperCase() === infoAcciones.codigo.trim().toLocaleUpperCase() ||
    x.nombre.trim().toLocaleUpperCase() === infoAcciones.nombre.trim().toLocaleUpperCase());
    if (isNullOrUndefined(infoDuplicada)) {
      return false
    } else {
      return true
    }
  }

  ObtenerInfoTAcciones(formulario: FormGroup): AclAcciones {
     return {
      nombre : formulario.controls['nombre'].value,
      codigo: formulario.controls['codigo'].value,
      anulado : formulario.controls['anulado'].value,
      icono: formulario.controls['icono'].value,
      fechaCreacion: undefined,
      fechaModificacion: undefined,
      usrCreacion: this.codigoProceso === codigoIng ? this.usuarioLogin.userName : undefined,
      usrModificacion: this.codigoProceso === codigoMod ? this.usuarioLogin.userName : undefined,
      aclModuloAcciones:undefined,
      idAccion: this.codigoProceso === codigoMod ? formulario.controls['idAccion'].value : undefined
    };
  }

}
