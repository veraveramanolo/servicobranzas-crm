import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { AclModulos } from '../../../models/AclModulos';
import { AclUsuario } from '../../../models/AclUsuario';
import { ModulosService } from '../../../services/modulos.service';
import { VariableGlobalService } from '../../../services/variable-global.service';
import { ModeloRespuesta } from '../../../models/ModeloRespuesta.models';
import Swal from 'sweetalert2';
import { isNullOrUndefined } from 'util';
const codigoIng = 'ING';
const codigoMod = 'MOD';
const listaSiNo = [{ 'Codigo': 'SI', 'Descripcion': 'SI' }, { 'Codigo': 'NO', 'Descripcion': 'NO' }];


@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css']
})
export class ModulosComponent implements OnInit {

  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public infoAclModulos: AclModulos[];
  private editedRowIndex: number;
  public formulario: FormGroup;
  public creacionEdicion: boolean;
  public cargandoGrid: boolean;
  public datoAnulado = listaSiNo;
  public datoPadre = listaSiNo;
  public datoVisible = listaSiNo;
  public codigoProceso: string;
  public usuarioLogin: AclUsuario;
  public listaModuloAso: AclModulos[] = [];
  public valorComboModulo: AclModulos;
  

  constructor(private serviceModulos: ModulosService, private global: VariableGlobalService) { }

  ngOnInit(): void {
    this.estadoGrid = { skip: 0, take: 10, filter: { logic: 'and', filters: [] }};
    this.ConsultarUsuarioLogin();
    this.ConsultarAllModulos();
  }

  
  ConsultarUsuarioLogin(){
    this.usuarioLogin =  this.global.ConsultarUsuarioLogin();
  }


  async ConsultarAllModulos() {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.serviceModulos.All();
    console.log(respuesta.data);
    this.infoAclModulos = respuesta.data;

    if (this.infoAclModulos.length > 0) {
      this.infoAclModulos.forEach((a: AclModulos) => {
        this.formulario = new FormGroup({
          idParentModulo: new FormControl(a.nombreParentModulo)
        });
        debugger
      });
    }

    this.listaModuloAso = this.infoAclModulos;
    this.datoGrid = process(this.infoAclModulos, this.estadoGrid);
    this.cargandoGrid = false;

    console.log(this.infoAclModulos);
  }

  AgregarRegistroGrid({sender}){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      codigo: new FormControl('',[Validators.required, Validators.minLength(3),Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(3)]),
      nombre:  new FormControl('', [Validators.required,  Validators.maxLength(30)]),
      icono: new FormControl(''),
      moduloPadre: new FormControl('NO'),
      rutaDirectorio: new FormControl('', Validators.required),
      idParentModulo: new FormControl(0), 
      visible: new FormControl('SI', Validators.required), 
      ordenPresentacion: new FormControl(0),
      anulado: new FormControl('NO', Validators.required),
      nombreParentModulo: new FormControl('')
    });

    sender.addRow(this.formulario);
  }

  CerrarFilaGrid(grid, rowIndex = this.editedRowIndex){
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formulario = undefined;
  }

  CancelarEdicionFila({ sender, rowIndex }) {
    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
  }


  FiltroComboModulo(valor){
    this.listaModuloAso = this.infoAclModulos.filter(
      (m) => m.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );  
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoAclModulos, this.estadoGrid);
  }

  async GuardarFilaGrid({ sender, rowIndex, formGroup: FormGroup, isNew }){
    let infoModulos: AclModulos = await this.ObtenerInfoTModulos(this.formulario);

    if(this.formulario.invalid){
      return Swal.fire('Información Invalida', 'Debe haber un error el la información ingresada', 'error');
    }

    if(this.InfoDuplicada(this.infoAclModulos  ,infoModulos)){
      return Swal.fire('Información duplicada', 'Ya existe un registro con esos detalles. Favor modifique e intente nuevamente', 'error');
    }

    let resp: ModeloRespuesta = undefined;
    if(this.codigoProceso === codigoIng)
      resp = await this.serviceModulos.Crear(infoModulos);

    if(this.codigoProceso === codigoMod)
      resp = await this.serviceModulos.Modificar(infoModulos);
    

    if (resp.exito === 0) {
      return Swal.fire('Error al grabar', 'Se produjo un error en el sistema intente nuevamente', 'error');
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire('Datos Grabados', 'La información se grabo con exito', 'success');
    return await this.ConsultarAllModulos();

  }

  EditarFilaGrid({ sender, rowIndex, dataItem }){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoMod;

    this.formulario = new FormGroup({
      idModulo : new FormControl(dataItem.idModulo),
      codigo: new FormControl(dataItem.codigo ,[Validators.required, Validators.minLength(3),Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(3)]),
      nombre:  new FormControl(dataItem.nombre, [Validators.required, Validators.maxLength(30)]),
      icono: new FormControl(dataItem.icono),
      moduloPadre: new FormControl(dataItem.moduloPadre),
      rutaDirectorio: new FormControl(dataItem.rutaDirectorio, Validators.required),
      idParentModulo: new FormControl(dataItem.idParentModulo), 
      visible: new FormControl(dataItem.visible, Validators.required), 
      ordenPresentacion: new FormControl(dataItem.ordenPresentacion),
      anulado: new FormControl(dataItem.anulado, Validators.required),
      nombreParentModulo: new FormControl(dataItem.nombreParentModulo)
    });

    this.formulario.controls['idModulo'].disable();
    
    if(dataItem.moduloPadre === 'SI')  {
      this.formulario.controls['nombreParentModulo'].disable(); 
    }

    this.creacionEdicion = true;
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formulario);
  }

  InfoDuplicada(infoAclAcciones: AclModulos[], infoAcciones: AclModulos): boolean {
    let infoDuplicada : AclModulos;
    let infoFiltrada: AclModulos[] = infoAclAcciones.filter(x => x.idModulo !== infoAcciones.idModulo);
    infoDuplicada = infoFiltrada.find(x => x.codigo.trim().toLocaleUpperCase() === infoAcciones.codigo.trim().toLocaleUpperCase() ||
    x.nombre.trim().toLocaleUpperCase() === infoAcciones.nombre.trim().toLocaleUpperCase());
    if (isNullOrUndefined(infoDuplicada)) {
      return false
    } else {
      return true
    }
  }

  ObtenerInfoTModulos(formulario: FormGroup): AclModulos{
    return {
      aclAccesosRoles: undefined,
      aclModuloAcciones: undefined,
      anulado: formulario.controls['anulado'].value,
      codigo: formulario.controls['codigo'].value,
      fechaCreacion: undefined,
      fechaModificacion: undefined,
      icono: formulario.controls['icono'].value,
      idModulo: this.codigoProceso === codigoMod ? formulario.controls['idModulo'].value : undefined,
      idParentModulo: formulario.controls['nombreParentModulo'].value,
      idParentModuloNavigation: undefined,
      inverseIdParentModuloNavigation: undefined,
      moduloPadre: formulario.controls['moduloPadre'].value,
      nombre: formulario.controls['nombre'].value,
      ordenPresentacion: formulario.controls['ordenPresentacion'].value,
      rutaDirectorio: formulario.controls['rutaDirectorio'].value,
      usrCreacion: this.codigoProceso === codigoIng ? this.usuarioLogin.userName : undefined,
      usrModificacion: this.codigoProceso === codigoMod ? this.usuarioLogin.userName : undefined,
      visible: formulario.controls['visible'].value,
      nombreParentModulo: undefined
    };
  }

}
