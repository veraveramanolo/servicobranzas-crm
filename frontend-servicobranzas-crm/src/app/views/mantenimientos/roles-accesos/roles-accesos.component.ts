import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  DataStateChangeEvent,
  GridDataResult,
} from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import { AclRoles } from "../../../models/AclRoles";
import { VariableGlobalService } from "../../../services/variable-global.service";
import { RolesService } from "../../../services/roles.service";
import { ModeloRespuesta } from "../../../models/ModeloRespuesta.models";
import Swal from "sweetalert2";
import { isNullOrUndefined } from "util";
import { AclAccesosRoles } from "../../../models/AclAccesosRoles";
import { AccesosRolesService } from "../../../services/accesos-roles.service";
import { AclModulos } from "../../../models/AclModulos";
import { ModulosService } from "../../../services/modulos.service";
import { AclUsuario } from "../../../models/AclUsuario";
const codigoMod = "MOD";
const codigoIng = "ING";
const listaSiNo = [
  { Codigo: "SI", Descripcion: "SI" },
  { Codigo: "NO", Descripcion: "NO" },
];

@Component({
  selector: "app-roles-accesos",
  templateUrl: "./roles-accesos.component.html",
  styleUrls: ["./roles-accesos.component.css"],
})
export class RolesAccesosComponent implements OnInit {
  public datoGrid: GridDataResult;
  public datoGridAcceso: GridDataResult;
  public estadoGrid: State;
  public estadoGridAcceso: State;
  public infoAclRoles: AclRoles[];
  public infoAclAccesosRoles: AclAccesosRoles[];
  private editedRowIndex: number;
  public formulario: FormGroup;
  public creacionEdicion: boolean;
  public cargandoGrid: boolean;
  public cargandoGridAcceso: boolean;
  public datoAnulado = listaSiNo;
  public codigoProceso: string;
  public RolSeleccionada: AclRoles;
  public infoAclModulos: AclModulos[];
  public listaModuloAso: AclModulos[] = [];
  public usuarioLogin: AclUsuario;

  constructor(
    private global: VariableGlobalService,
    private rolesService: RolesService,
    private accesosRolesService: AccesosRolesService,
    private serviceModulos: ModulosService
  ) {}

  ngOnInit(): void {
    this.estadoGrid = {
      skip: 0,
      take: 10,
      filter: { logic: "and", filters: [] },
    };
    this.estadoGridAcceso = {
      skip: 0,
      take: 10,
      filter: { logic: "and", filters: [] },
    };
    this.ConsultarAllRoles();
    this.ConsultarUsuarioLogin();
    this.ConsultarAllModulos();
  }

  ConsultarUsuarioLogin() {
    this.usuarioLogin = this.global.ConsultarUsuarioLogin();
  }

  async ConsultarAllRoles() {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.rolesService.All();
    this.infoAclRoles = respuesta.data;
    this.datoGrid = process(this.infoAclRoles, this.estadoGrid);
    this.cargandoGrid = false;

    console.log(this.infoAclRoles);
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoAclRoles, this.estadoGrid);
  }

  AgregarRegistroGrid({ sender }) {
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      descripcion: new FormControl("", [
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.maxLength(30),
      ]),
      anulado: new FormControl("NO", Validators.required),
    });

    sender.addRow(this.formulario);
  }

  CerrarFilaGrid(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formulario = undefined;
  }

  CancelarEdicionFila({ sender, rowIndex }) {
    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
  }

  EditarFilaGrid({ sender, rowIndex, dataItem }) {
    this.CerrarFilaGrid(sender);
  }

  async GuardarFilaGrid({ sender, rowIndex, formGroup: FormGroup, isNew }) {
    let infoRoles: AclRoles = await this.ObtenerInfoTipoModulos(
      this.formulario
    );

    if (this.formulario.invalid) {
      return Swal.fire(
        "Información Invalida",
        "Debe haber un error el la imformación ingresada",
        "error"
      );
    }

    if (this.InfoDuplicada(this.infoAclRoles, infoRoles)) {
      return Swal.fire(
        "Informacióm duplicada",
        "Ya existe un registro con esos detalles. Favor modifique e intente nuevamente",
        "error"
      );
    }

    let resp: ModeloRespuesta = await this.rolesService.Crear(infoRoles);
    if (resp.exito === 0) {
      return Swal.fire(
        "Error al grabar",
        "Se produjo un error en el sistema intente nuevamente",
        "error"
      );
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire("Datos Grabados", "La información se grabo con exito", "success");
    return await this.ConsultarAllRoles();
  }

  InfoDuplicada(infoAclRoles: AclRoles[], infoRoles: AclRoles): boolean {
    let infoDuplicada: AclRoles;
    infoDuplicada = infoAclRoles.find(
      (x) =>
        x.descripcion.trim().toLocaleUpperCase() ===
        infoRoles.descripcion.trim().toLocaleUpperCase()
    );
    if (isNullOrUndefined(infoDuplicada)) {
      return false;
    } else {
      return true;
    }
  }

  ObtenerInfoTipoModulos(formulario: FormGroup): AclRoles {
    return {
      descripcion: formulario.controls["descripcion"].value,
      anulado: formulario.controls["anulado"].value,
      fechaCreacion: undefined,
      fechaModificacion: undefined,
      usrCreacion: "Jcontreras",
      usrModificacion: undefined,
      aclAccesosRoles: undefined,
      aclPerfilRol: undefined,
      idRol: undefined,
    };
  }

  async cellClickHandler({ dataItem, rowIndex }) {
    this.RolSeleccionada = dataItem;
    await this.ConsultarAccesoRol(this.RolSeleccionada.idRol);
  }

  async ConsultarAccesoRol(idRol: number) {
    this.cargandoGridAcceso = true;
    this.datoGridAcceso = undefined;

    let resp: ModeloRespuesta = await this.accesosRolesService.GetAccesos(
      idRol
    );
    this.infoAclAccesosRoles = resp.data;

    this.datoGridAcceso = process(
      this.infoAclAccesosRoles,
      this.estadoGridAcceso
    );
    this.cargandoGridAcceso = false;
  }

  EventoEstadoGridAcceso(estadoGrid: DataStateChangeEvent): void {
    this.estadoGridAcceso = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGridAcceso = process(
      this.infoAclAccesosRoles,
      this.estadoGridAcceso
    );
  }

  AgregarRegistroGridAcceso({ sender }) {
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      nombreModulo: new FormControl(0, [Validators.required]),
      anulado: new FormControl("NO", Validators.required),
    });

    sender.addRow(this.formulario);
  }

  async ConsultarAllModulos() {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.serviceModulos.All();
    this.infoAclModulos = respuesta.data;
  }

  FiltroComboModulo(valor) {
    this.listaModuloAso = this.infoAclModulos.filter(
      (m) => m.nombre.toLowerCase().indexOf(valor.toLowerCase()) !== -1
    );
  }

  EditarFilaGridAcceso({ sender, rowIndex, dataItem }) {
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoMod;

    this.formulario = new FormGroup({
      idAccesoRol: new FormControl(dataItem.idAccesoRol),
      nombreModulo: new FormControl(dataItem.nombreModulo, [
        Validators.required,
        Validators.maxLength(30),
      ]),
      anulado: new FormControl(dataItem.anulado, Validators.required),
    });

    this.creacionEdicion = true;
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formulario);
  }

  async GuardarFilaGridAcceso({
    sender,
    rowIndex,
    formGroup: FormGroup,
    isNew,
  }) {
    let infoAccesosRoles: AclAccesosRoles = await this.ObtenerInfoAccessosRoles(
      this.formulario
    );

    if (this.formulario.invalid) {
      return Swal.fire(
        "Información Invalida",
        "Debe haber un error el la información ingresada",
        "error"
      );
    }

    if (
      this.InfoDuplicadaAccesoRol(this.infoAclAccesosRoles, infoAccesosRoles)
    ) {
      return Swal.fire(
        "Información duplicada",
        "Ya existe un registro con esos detalles. Favor modifique e intente nuevamente",
        "error"
      );
    }

    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();

    let resp: ModeloRespuesta = undefined;
    if (this.codigoProceso === codigoIng)
      resp = await this.accesosRolesService.Crear(infoAccesosRoles);

    if (this.codigoProceso === codigoMod)
      resp = await this.accesosRolesService.Modificar(infoAccesosRoles);

    if (resp.exito === 0) {
      return Swal.fire(
        "Error al grabar",
        "Se produjo un error en el sistema intente nuevamente",
        "error"
      );
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire("Datos Grabados", "La información se grabo con exito", "success");
    return await this.ConsultarAccesoRol(this.RolSeleccionada.idRol);
  }

  InfoDuplicadaAccesoRol(
    infoAccesosRoles: AclAccesosRoles[],
    infoAccesos: AclAccesosRoles
  ): boolean {
    let infoDuplicada: AclAccesosRoles;
    let infoFiltrada: AclAccesosRoles[] = infoAccesosRoles.filter(
      (x) => x.idAccesoRol !== infoAccesos.idAccesoRol
    );
    infoDuplicada = infoFiltrada.find(
      (x) => x.idModulo === infoAccesos.idModulo
    );
    if (isNullOrUndefined(infoDuplicada)) {
      return false;
    } else {
      return true;
    }
  }

  ObtenerInfoAccessosRoles(formulario: FormGroup): AclAccesosRoles {
    return {
      anulado: formulario.controls["anulado"].value,
      fechaCreacion: undefined,
      fechaModificacion: undefined,
      idAccesoRol:
        this.codigoProceso === codigoMod
          ? formulario.controls["idAccesoRol"].value
          : undefined,
      idModulo: formulario.controls["nombreModulo"].value,
      idModuloNavigation: undefined,
      idRol: this.RolSeleccionada.idRol,
      idRolNavigation: undefined,
      nombreModulo: undefined,
      usrCreacion:
        this.codigoProceso === codigoIng
          ? this.usuarioLogin.userName
          : undefined,
      usrModificacion:
        this.codigoProceso === codigoMod
          ? this.usuarioLogin.userName
          : undefined,
    };
  }
}
