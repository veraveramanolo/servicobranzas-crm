import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { State , process} from '@progress/kendo-data-query';
import { AclPerfiles } from '../../../models/AclPerfiles';
import { VariableGlobalService } from '../../../services/variable-global.service';
import { PerfilesService } from '../../../services/perfiles.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ModeloRespuesta } from '../../../models/ModeloRespuesta.models';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { AclUsuario } from '../../../models/AclUsuario';
import { isNullOrEmptyString } from '@progress/kendo-angular-grid/dist/es2015/utils';

const codigoIng = 'ING';
const codigoMod = 'MOD';
const listaSiNo = [{ 'Codigo': 'SI', 'Descripcion': 'SI' }, { 'Codigo': 'NO', 'Descripcion': 'NO' }];

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public infoAclPerfiles: AclPerfiles[];
  private editedRowIndex: number;
  public formulario: FormGroup;
  public creacionEdicion: boolean;
  public cargandoGrid: boolean;
  public datoAnulado = listaSiNo;
  public codigoProceso: string;
  public usuarioLogin: AclUsuario;


  constructor(private perfilesService :PerfilesService, private redireccionar: Router,
    public global: VariableGlobalService) {
    
  }


  ngOnInit(): void {
    this.estadoGrid = { skip: 0, take: 10, filter: { logic: 'and', filters: [] }};
    this.ConsultarUsuarioLogin();
    this.ConsultarAllPerfiles();

  

  }

  ConsultarUsuarioLogin(){
    this.usuarioLogin =  this.global.ConsultarUsuarioLogin();
  }

  async ConsultarAllPerfiles() {
    this.cargandoGrid = true;
    this.datoGrid = undefined;

    let respuesta: ModeloRespuesta = await this.perfilesService.All();
    this.infoAclPerfiles = respuesta.data;
    this.datoGrid = process(this.infoAclPerfiles, this.estadoGrid);
    this.cargandoGrid = false;

    console.log(this.infoAclPerfiles);
  }


  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    this.datoGrid = process(this.infoAclPerfiles, this.estadoGrid);
  }

  AgregarRegistroGrid({sender}){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoIng;

    this.formulario = new FormGroup({
      descripcion: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      anulado: new FormControl('NO', [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      
    });

    sender.addRow(this.formulario);

  }

  CerrarFilaGrid(grid, rowIndex = this.editedRowIndex){
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formulario = undefined;
  }

  CancelarEdicionFila({ sender, rowIndex }) {
    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
  }

  EditarFilaGrid({ sender, rowIndex, dataItem }){
    this.CerrarFilaGrid(sender);
    this.codigoProceso = codigoMod;
    
    this.formulario = new FormGroup({
      descripcion: new FormControl(dataItem.descripcion, [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      anulado: new FormControl(dataItem.anulado, [Validators.required, Validators.pattern('^[a-zA-Z]+$'), Validators.maxLength(30)]),
      idPerfil : new FormControl(dataItem.idPerfil)
    });

    this.formulario.controls['idPerfil'].disable();

    this.creacionEdicion = true;
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formulario);
  }

  async GuardarFilaGrid({ sender, rowIndex, formGroup: FormGroup, isNew }){
    let infoPerfil: AclPerfiles = await this.ObtenerInfoPerfil(this.formulario);

    if(this.formulario.invalid){
      return Swal.fire('Información Invalida', 'Debe haber un error el la información ingresada', 'error');
    }

    if(this.InfoDuplicada(this.infoAclPerfiles  ,infoPerfil)){
      return Swal.fire('Informacióm duplicada', 'Ya existe un registro con esos detalles. Favor modifique e intente nuevamente', 'error');
    }
    
    let resp: ModeloRespuesta = undefined;
    if(this.codigoProceso === codigoIng)
      resp = await this.perfilesService.Crear(infoPerfil);

    if(this.codigoProceso === codigoMod)
      resp = await this.perfilesService.Modificar(infoPerfil);
    
    if (resp.exito === 0) {
    return Swal.fire('Error al grabar', 'Se produjo un error en el sistema intente nuevamente', 'error');
    }

    this.CerrarFilaGrid(sender, rowIndex);
    this.creacionEdicion = false;
    Swal.fire('Datos Grabados', 'La información se grabo con exito', 'success');
    return await this.ConsultarAllPerfiles();
  }


  InfoDuplicada(ListinfoAclPerfiles: AclPerfiles[], aclPerfiles: AclPerfiles): boolean {
    let infoDuplicada : AclPerfiles;
    let infoFiltrada: AclPerfiles[] = ListinfoAclPerfiles.filter(x => x.idPerfil !== aclPerfiles.idPerfil)
    infoDuplicada = infoFiltrada.find(x => x.descripcion.trim().toLocaleUpperCase() === aclPerfiles.descripcion.trim().toLocaleUpperCase());
    if (isNullOrUndefined(infoDuplicada)) {
      return false
    } else {
      return true
    }
  }


  ObtenerInfoPerfil(formulario: FormGroup): AclPerfiles {
     return {
      descripcion : formulario.controls['descripcion'].value,
      anulado : formulario.controls['anulado'].value,
      fechaCreacion: undefined,
      fechaModificacion: undefined,
      usrCreacion: this.codigoProceso === codigoIng ? this.usuarioLogin.userName : undefined,
      usrModificacion: this.codigoProceso === codigoMod ? this.usuarioLogin.userName : undefined,
      aclPerfilRol : undefined,
      aclUsuarioPerfil: undefined,
      idPerfil : this.codigoProceso === codigoMod ? formulario.controls['idPerfil'].value : undefined
    };
  }



}
