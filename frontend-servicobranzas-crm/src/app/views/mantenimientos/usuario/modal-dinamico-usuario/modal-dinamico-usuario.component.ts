import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subject } from "rxjs";
import Swal from "sweetalert2";
import { AclUsuario } from "../../../../models/AclUsuario";
import { ConsultauserService } from "../../../../services/consultauser.service";
import { RolesService } from "../../../../services/roles.service";
import { UsuarioMapper } from "../../../../utils/mappers/usuario.mapper";
import { botones } from "../../../gestionar/gestion-normal/mock";

@Component({
  selector: "app-modal-dinamico-usuario",
  templateUrl: "./modal-dinamico-usuario.component.html",
  styleUrls: ["./modal-dinamico-usuario.component.css"],
})
export class ModalDinamicoUsuarioComponent implements OnInit {
  public Roles: any = [];
  public telefonoValue: string = "";
  public telefonoMask: string = "(999) 000-00-00-00";
  public dptos: Array<string> = ["Sistemas", "Financiero"];
  registerForm: FormGroup;
  private stop$ = new Subject<void>();
  titulo: string;
  usuario: AclUsuario;

  constructor(
    public modalRef: BsModalRef,
    private consultauserService: ConsultauserService,
    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService,
    private rolesService: RolesService
  ) {}

  async ngOnInit(): Promise<void> {
    this.fromDinamico();
    await this.allRol();
    console.log(this.Roles.data);
  }

  async allRol() {
    this.Roles = await this.rolesService.All();
  }

  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
  private fromDinamico(): void {
    console.log(this.usuario);
    if (!this.usuario) {
      this.formGuardar();
    } else {
      this.formEditar();
    }
  }

  public submitForm(event: any): void {
    event.preventDefault();
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      const usuario: AclUsuario = UsuarioMapper.crear(this.registerForm.value);
      this.consultauserService.guardar(usuario).subscribe(
        (respuesta) => {
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Usuario agregado correctamente",
              showConfirmButton: false,
              timer: 1500,
            });
            this.registerForm.reset();

            this.modalService.setDismissReason("ok");
            this.modalRef.hide();
            // this.ConsultarUsuarios();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
      console.log(usuario);
    }
  }

  guardadoDinamico(event: any): void {
    if (!this.usuario) {
      this.submitForm(event);
    } else {
      this.editarUsuario(event);
    }
  }

  editarUsuario(event: any): void {
    event.preventDefault();
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      const usuarioEditar: AclUsuario = UsuarioMapper.editar(
        this.registerForm.value,
        this.usuario.idUsuario
      );
      console.log(usuarioEditar);
      this.consultauserService.editar(usuarioEditar).subscribe(
        (respuesta) => {
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Usuario editado correctamente",
              showConfirmButton: false,
              timer: 1500,
            });
            this.registerForm.reset();

            this.modalService.setDismissReason("ok");
            this.modalRef.hide();
            // this.ConsultarUsuarios();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
    }
  }

  private formGuardar(): void {
    this.registerForm = this.fb.group({
      nombres: [""],
      apellidos: [""],
      cedula: [
        "",
        [
          Validators.required,
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(10),
        ],
      ],
      telefono: ["", Validators.required],
      email: ["", [Validators.email]],
      cargo: [""],
      departamento: ["", [Validators.required]],
      userName: [""],
      md5Password: [""],
      agentpass: [""],
      agentname: [""],
      rol: ["", Validators.required],
      // codigo:[''],
    });
  }

  private formEditar(): void {
    this.registerForm = this.fb.group({
      nombres: [this.usuario.nombres],
      apellidos: [this.usuario.apellidos],
      cedula: [
        this.usuario.cedula,
        [
          Validators.required,
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(10),
        ],
      ],
      telefono: [this.usuario.telefono, Validators.required],
      email: [this.usuario.email, [Validators.email]],
      cargo: [this.usuario.cargo],
      departamento: [this.usuario.departamento, [Validators.required]],
      userName: [this.usuario.userName],
      agentpass: [this.usuario.agentpass],
      agentname: [this.usuario.agentname],
      md5Password: [""],
      rol: [this.usuario.idRol, Validators.required],
      // codigo:[''],
    });
  }
}
