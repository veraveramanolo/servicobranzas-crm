import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  DataStateChangeEvent,
  GridDataResult,
} from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { VariableGlobalService } from "../../../services/variable-global.service";
import { AclUsuario } from "../../../models/AclUsuario";
import Swal from "sweetalert2";
import { ConsultauserService } from "../../../services/consultauser.service";
import { UsuarioMapper } from "../../../utils/mappers/usuario.mapper";
import { BsModalService } from "ngx-bootstrap/modal";
import { ModalDinamicoUsuarioComponent } from "./modal-dinamico-usuario/modal-dinamico-usuario.component";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { RolesService } from "../../../services/roles.service";

const itemIndex = (item: any, data: any[]): number => {
  for (let idx = 0; idx < data.length; idx++) {
    if (data[idx].idlote === item.idlote) {
      return idx;
    }
  }
  return -1;
};

@Component({
  selector: "app-usuario",
  templateUrl: "./usuario.component.html",
  styleUrls: ["./usuario.component.css"],
})
export class UsuarioComponent implements OnInit, OnDestroy {
  private stop$ = new Subject<void>();

  registerForm: FormGroup;

  public datoGrid: GridDataResult;
  public estadoGrid: State;
  public cargandoDato: boolean;
  public checkSeleccionado: number[] = [];
  private updatedItems: any[] = [];
  private createdItems: any[] = [];
  public infoAclUsuario: AclUsuario[] = []; //ITA 07/01/2021 AclUser - AclUsuario

  constructor(
    private consultauserService: ConsultauserService,

    private global: VariableGlobalService,
    private readonly fb: FormBuilder,
    private readonly modalService: BsModalService
  ) {}
  ngOnDestroy(): void {
    this.stop();
  }

  ngOnInit(): void {
    this.formGuardar();
    this.estadoGrid = {
      skip: 0,
      take: 10,
      filter: { logic: "and", filters: [] },
    };
    this.ConsultarUsuarios();
    this.cambiosModal();
  }

  open(titulo: string, usuario?: any): void {
    let usuarioEdit: AclUsuario;
    if (usuario) {
      usuarioEdit = usuario.dataItem;
      // console.log(usuario);
    }
    this.modalService.show(ModalDinamicoUsuarioComponent, {
      initialState: { titulo, usuario: usuarioEdit },
      backdrop: "static",
      // class:'gray modal-lg'
    });
  }

  ConsultarUsuarios() {
    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();
    //this.perfilesRequest = {user : '', password :''};
    this.consultauserService.ConsultarUsuarios().subscribe((respuesta) => {
      if (respuesta.exito === 0) {
        Swal.close();
        return Swal.fire({
          allowOutsideClick: false,
          text: respuesta.mensage,
          icon: "error",
        });
      }
      if (respuesta.exito === 1) {
        console.log(respuesta.data);
        this.infoAclUsuario = respuesta.data;
        this.datoGrid = respuesta.data;
      }
      Swal.close();
    });
  }

  EventoEstadoGrid(estadoGrid: DataStateChangeEvent): void {
    this.estadoGrid = this.global.FuncionTrimFiltroGrid(estadoGrid);
    //this.datoGrid = process(this.infoAclUsuario, this.estadoGrid);
  }

  EventoCheckSeleccionado(evento) {}

  cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {}

  cellCloseHandler(args: any) {
    const { formGroup, dataItem } = args;

    if (!formGroup.valid) {
      // prevent closing the edited cell if there are invalid values.
      args.preventDefault();
    } else if (formGroup.dirty) {
      this.assignValues(dataItem, formGroup.value);
      this.update(dataItem);
    }
  }

  assignValues(target: any, source: any): void {
    Object.assign(target, source);
  }

  update(item: any): void {
    if (!this.isNew(item)) {
      const index = itemIndex(item, this.updatedItems);
      if (index !== -1) {
        this.updatedItems.splice(index, 1, item);
      } else {
        this.updatedItems.push(item);
      }
    } else {
      const index = this.createdItems.indexOf(item);
      this.createdItems.splice(index, 1, item);
    }
  }

  isNew(item: any): boolean {
    return !item.idlote;
  }
  public telefonoValue: string = "";
  public telefonoMask: string = "(999) 000-00-00-00";

  public dptos: Array<string> = ["Sistemas", "Financiero"];
  // public registerForm: FormGroup = new FormGroup({
  //   nombres: new FormControl(),
  //   apellidos: new FormControl(),
  //   cedula: new FormControl(),
  //   telefono: new FormControl(),
  //   email: new FormControl('', Validators.email),
  //   cargo: new FormControl(),
  //   departamento: new FormControl('',Validators.required),
  //   userName: new FormControl(),
  //   pass: new FormControl()
  // });

  public submitForm(): void {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      const usuario: AclUsuario = UsuarioMapper.crear(this.registerForm.value);
      this.consultauserService.guardar(usuario).subscribe(
        (respuesta) => {
          if (respuesta.exito === 0) {
            Swal.close();
            Swal.fire({
              allowOutsideClick: false,
              text: respuesta.mensage,
              icon: "error",
            });
          } else {
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Usuario agregado correctamente",
              showConfirmButton: false,
              timer: 1500,
            });
            this.ConsultarUsuarios();
            this.registerForm.reset();
          }
        },
        (err) =>
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: err.message,
          })
      );
      console.log(usuario);
    }
  }

  public clearForm(): void {
    this.registerForm.reset();
  }

  private formGuardar(): void {
    this.registerForm = this.fb.group({
      nombres: [""],
      apellidos: [""],
      cedula: [
        "",
        [
          Validators.required,
          Validators.pattern(/^[0-9]+$/),
          Validators.minLength(10),
        ],
      ],
      telefono: ["", Validators.required],
      email: ["", [Validators.email]],
      cargo: [""],
      departamento: ["", [Validators.required]],
      userName: [""],
      agentpass: [""],
      // codigo:[''],
    });
  }

  cambiosModal(): void {
    console.log("cambiosModal");
    this.modalService.onHidden.pipe(takeUntil(this.stop$)).subscribe((data) => {
      console.log("cambiosModal2");
      if (data === "ok") {
        this.ConsultarUsuarios();
      }
    });
  }
  stop(): void {
    this.stop$.next();
    this.stop$.complete();
  }
}
