import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './perfil/perfil.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AccionesComponent } from './acciones/acciones.component';
import { TiposModulosComponent } from './tipos-modulos/tipos-modulos.component';
import { ModulosComponent } from './modulos/modulos.component';
import { RolesAccesosComponent } from './roles-accesos/roles-accesos.component';
import { PlantillaComponent } from './plantilla/plantilla.component';


const routes: Routes = [
  {
    path: '',
    data: { title: 'Mantenimiento'},
    children:[
      {
        path: '',
        redirectTo: 'mantenimientos'
      },
      {
        path: 'mantenimientos/perfil', component: PerfilComponent , data: {title: 'Mantenimiento Perfiles'}
      },
      {
        path: 'mantenimientos/usuario', component: UsuarioComponent , data: {title: 'Mantenimiento Usuarios'}
      },
      {
        path: 'mantenimientos/acciones', component: AccionesComponent , data: {title: 'Mantenimiento Acciones'}
      },
      {
        path: 'mantenimientos/tipos-modulos', component: TiposModulosComponent , data: {title: 'Mantenimiento Tipos de Módulos'}
      },
      {
        path: 'mantenimientos/modulos', component: ModulosComponent , data: {title: 'Mantenimiento Módulos'}
      },
      {
        path: 'mantenimientos/roles-accesos', component: RolesAccesosComponent , data: {title: 'Mantenimiento Roles y accesos'}
      },
      {
        path: 'mantenimientos/plantilla', component: PlantillaComponent , data: {title: 'Mantenimiento Plantillas'}
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientosRoutingModule { }
