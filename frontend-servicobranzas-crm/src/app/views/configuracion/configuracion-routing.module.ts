import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArbolGestionComponent } from './arbol-gestion/arbol-gestion.component';
/*import { AccionesComponent } from './acciones/acciones.component';
import { TiposModulosComponent } from './tipos-modulos/tipos-modulos.component';
import { ModulosComponent } from './modulos/modulos.component';
import { RolesAccesosComponent } from './roles-accesos/roles-accesos.component';*/


const routes: Routes = [
  {
    path: '',
    data: { title: 'Configuracion'},
    children:[
      {
        path: '',
        redirectTo: 'configuracion'
      },
      {
        path: 'configuracion/arbol-gestion', component: ArbolGestionComponent , data: {title: 'Configuracion Arbol de Gestion'}
      },
      /*{
        path: 'mantenimientos/acciones', component: AccionesComponent , data: {title: 'Mantenimiento Acciones'}
      },
      {
        path: 'mantenimientos/tipos-modulos', component: TiposModulosComponent , data: {title: 'Mantenimiento Tipos de Módulos'}
      },
      {
        path: 'mantenimientos/modulos', component: ModulosComponent , data: {title: 'Mantenimiento Módulos'}
      },
      {
        path: 'mantenimientos/roles-accesos', component: RolesAccesosComponent , data: {title: 'Mantenimiento Roles y accesos'}
      },*/
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionRoutingModule {}
