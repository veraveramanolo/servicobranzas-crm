import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionRoutingModule } from './configuracion-routing.module';
import { ArbolGestionComponent } from './arbol-gestion/arbol-gestion.component';
//import { PerfilComponent } from './perfil/perfil.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
/*import { RolesAccesosComponent } from './roles-accesos/roles-accesos.component';
import { TiposModulosComponent } from './tipos-modulos/tipos-modulos.component';
import { ModulosComponent } from './modulos/modulos.component';
import { AccionesComponent } from './acciones/acciones.component';
import { PlantillaComponent } from './plantilla/plantilla.component';*/
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDinamicoGestionComponent } from './arbol-gestion/modal-dinamico-gestion/modal-dinamico-gestion.component';

@NgModule({
  declarations: [ArbolGestionComponent/*, PerfilComponent, RolesAccesosComponent, TiposModulosComponent, ModulosComponent, AccionesComponent, PlantillaComponent*/, ModalDinamicoGestionComponent],
  entryComponents:[ModalDinamicoGestionComponent],
  imports: [
    CommonModule,
    ConfiguracionRoutingModule,
    GridModule,
    FormsModule,
    ReactiveFormsModule,
    DateInputsModule,
    InputsModule,
    LabelModule,
    DropDownsModule,
    MatCardModule,
    MatTooltipModule,
    LayoutModule,
    DialogModule,
    ModalModule.forRoot(),
  ]
})
export class ConfiguracionModule { }
