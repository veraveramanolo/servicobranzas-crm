import { AclUsuario } from "./../../models/AclUsuario";
import { Component /*, OnInit*/, OnInit } from "@angular/core";
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { AuthenticationService } from "../../services/authentication.service";
import { ModeloRespuesta } from "../../models/ModeloRespuesta.models";
import { AuthRequest } from "../../models/AuthRequest.model";
import { Md5 } from "md5-typescript";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
import { VariableGlobalService } from "../../services/variable-global.service";
import { DinomiService } from "../../services/dinomi.service";
import { NavItemsCabecera } from "../../models/sidebar-nav";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html",
  styleUrls: ["login.component.css"],
})
export class LoginComponent implements OnInit {
  public capStatus: boolean = true;
  public authRequest: AuthRequest;
  public formLogin: FormGroup;
  public respuesta: ModeloRespuesta;
  public aclUsuario: AclUsuario;
  public navItems: NavItemsCabecera[];

  constructor(
    private authenticationService: AuthenticationService,
    private redireccionar: Router,
    public global: VariableGlobalService,
    private readonly _loginDinomiService: DinomiService
  ) {
    this.formLogin = new FormGroup({
      usuario: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
    });
  }
  showResponse(event) {
    // this.messageService.add({severity:'info', summary:'Succees', detail: 'User Responded', sticky: true});
    this.capStatus = false;
    console.log(event);
  }

  ngOnInit(): void {
    this.authRequest = { user: "", password: "" };
  }

  async login(formLogin) {
    if (formLogin.invalid) {
      return;
    }

    Swal.fire({
      allowOutsideClick: false,
      text: "Espere por favor...",
      icon: "info",
    });
    Swal.showLoading();

    this.authRequest.user = formLogin.value["usuario"];
    this.authRequest.password = Md5.init(formLogin.value["password"]);

    let respuesta: ModeloRespuesta = await this.authenticationService.login(
      this.authRequest
    );

    console.log("usuario logeado", respuesta);

    if (respuesta.exito === 0) {
      Swal.close();
      return Swal.fire({
        allowOutsideClick: false,
        text: respuesta.mensage,
        icon: "error",
      });
    } else {
      localStorage.setItem("extension", respuesta.data.usuario.agentname);
      this._loginDinomiService
        .loginDinomi(respuesta)
        .subscribe((data) => console.table(data));
      this.authenticationService.guardarToken(respuesta.data.token);
      this.aclUsuario = respuesta.data.usuario;
      this.navItems = respuesta.data.menu;
      this.GuardarValorUsuario(this.aclUsuario);
      this.GuardarValorNavItems(this.navItems);
      //await this.ConsultarPermiosAccesos(this.aclUsuario.idUsuario);
      this.redireccionar.navigate(["/gestionar/gestionar/gestion-normal"]);
      Swal.close();
    }
  }

  // async ConsultarPermiosAccesos(idUser: number) {
  //   let respuesta: ModeloRespuesta = await this.authenticationService.ConsultarPermiosAccesos(
  //     idUser
  //   );
  //   this.global.navItems = respuesta.data;
  //   this.global.navItems.unshift(this.global.cabeceraNasItems);
  //   console.log(this.global.navItems);
  // }

  GuardarValorUsuario(objGlobal: AclUsuario) {
    // debugger;
    this.global.usuarioLogueado = objGlobal;
    localStorage.setItem(
      this.global.valorusuarioLogueadoSessionStorage,
      JSON.stringify(objGlobal)
    );
  }

  GuardarValorNavItems(navItems: NavItemsCabecera[]) {
    this.global.navItems = navItems;
    localStorage.setItem(
      this.global.valornavItemsSessionStorage,
      JSON.stringify(navItems)
    );
  }
}
