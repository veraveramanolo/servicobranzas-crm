import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LloacanaComponent } from './lloacana.component';

describe('LloacanaComponent', () => {
  let component: LloacanaComponent;
  let fixture: ComponentFixture<LloacanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LloacanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LloacanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
