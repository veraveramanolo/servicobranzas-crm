import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

export interface CanComponentDeactivate {
    canDeactivate: () => Observable<boolean> | boolean;
}

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate{

    constructor(private redirecciona: Router, private authService: AuthenticationService){
        
    }
    canActivate(): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if (this.authService.isAuthenticated()) {
            return true;
        } else {
            this.redirecciona.navigate(['login']);
            return false;
        }
    }



}