﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace backend_servicobranzas_crm.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "acl_acciones",
                columns: table => new
                {
                    id_accion = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    codigo = table.Column<string>(type: "varchar(10)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    nombre = table.Column<string>(type: "varchar(150)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    icono = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    anulado = table.Column<string>(type: "varchar(2)", nullable: false, defaultValueSql: "'NO'")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    usr_creacion = table.Column<string>(type: "varchar(50)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    usr_modificacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_accion);
                });

            migrationBuilder.CreateTable(
                name: "acl_area",
                columns: table => new
                {
                    id_area = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    valor_area = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_area);
                });

            migrationBuilder.CreateTable(
                name: "acl_efecto_acuerdo",
                columns: table => new
                {
                    id_efecto = table.Column<int>(nullable: false),
                    efecto_valor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_efecto);
                });

            migrationBuilder.CreateTable(
                name: "acl_estados",
                columns: table => new
                {
                    estado = table.Column<string>(type: "varchar(200)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    cantidad = table.Column<long>(nullable: true),
                    texto = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    alerta = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "acl_lista_asesoresasig",
                columns: table => new
                {
                    id_usuario = table.Column<int>(nullable: false),
                    asesor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    asignadosClientes = table.Column<int>(nullable: true),
                    activos = table.Column<int>(nullable: true),
                    inactivos = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "acl_modulos",
                columns: table => new
                {
                    id_modulo = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    codigo = table.Column<string>(type: "varchar(4)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    nombre = table.Column<string>(type: "varchar(150)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    icono = table.Column<string>(type: "varchar(150)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    modulo_padre = table.Column<string>(type: "varchar(2)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    ruta_directorio = table.Column<string>(type: "varchar(150)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    id_parent_modulo = table.Column<uint>(nullable: true),
                    visible = table.Column<string>(type: "varchar(2)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    orden_presentacion = table.Column<uint>(nullable: true),
                    anulado = table.Column<string>(type: "varchar(2)", nullable: true, defaultValueSql: "'NO'")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    usr_creacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    usr_modificacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_modulo);
                    table.ForeignKey(
                        name: "FK_acl_modulos_acl_modulos",
                        column: x => x.id_parent_modulo,
                        principalTable: "acl_modulos",
                        principalColumn: "id_modulo",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_multicanal_estados",
                columns: table => new
                {
                    gestion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    mejorgestion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "acl_parametro_objetivo",
                columns: table => new
                {
                    id_parametro = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    valor_parametro = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_parametro);
                });

            migrationBuilder.CreateTable(
                name: "acl_plantilla",
                columns: table => new
                {
                    id_plantilla = table.Column<int>(nullable: false, comment: "Identificador de plantilla")
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    nombre_plantilla = table.Column<string>(type: "varchar(45)", nullable: false, comment: "Nombre de plantilla")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    body_plantilla = table.Column<string>(type: "varchar(200)", nullable: true, comment: "Cuerpo de plantilla")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado_plantilla = table.Column<string>(type: "enum('A','I')", nullable: false, defaultValueSql: "'A'", comment: "Estado: Activo, Inactivo")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    fecha_actualizacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    usr_creacion = table.Column<int>(nullable: false),
                    usr_modificacion = table.Column<int>(nullable: true),
                    tipo_plantilla = table.Column<string>(type: "varchar(30)", nullable: false, comment: "Tipo: Sms, Dinomi, Email")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_plantilla);
                });

            migrationBuilder.CreateTable(
                name: "acl_region",
                columns: table => new
                {
                    id_region = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    region_valor = table.Column<string>(type: "varchar(10)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_region);
                });

            migrationBuilder.CreateTable(
                name: "acl_relacion",
                columns: table => new
                {
                    id_relacion = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    valor_relacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_relacion);
                });

            migrationBuilder.CreateTable(
                name: "acl_roles",
                columns: table => new
                {
                    id_rol = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    descripcion = table.Column<string>(type: "varchar(80)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    anulado = table.Column<string>(type: "varchar(2)", nullable: true, defaultValueSql: "'NO'")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    usr_creacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    usr_modificacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_rol);
                });

            migrationBuilder.CreateTable(
                name: "acl_tipo_contacto",
                columns: table => new
                {
                    id_tipo_contacto = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    texto = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_tipo_contacto);
                });

            migrationBuilder.CreateTable(
                name: "acl_tipo_info",
                columns: table => new
                {
                    id_tipo_info = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    info_valor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    info_nombre = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_tipo_info);
                });

            migrationBuilder.CreateTable(
                name: "acl_tipo_objetivo",
                columns: table => new
                {
                    id_tipoobjetivo = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    valor_tipoobjetivo = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_tipoobjetivo);
                });

            migrationBuilder.CreateTable(
                name: "acl_zona",
                columns: table => new
                {
                    id_zona = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    zona_valor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_zona);
                });

            migrationBuilder.CreateTable(
                name: "crm_arbol_gestion_tipo_contacto",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    texto = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_arbol_gestion_tipo_contacto", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_arbol_gestion_tipo_gestion",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    texto = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_arbol_gestion_tipo_gestion", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_asignacion_cab",
                columns: table => new
                {
                    id_asignacion = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_tarea = table.Column<uint>(nullable: false),
                    tipoasignacion = table.Column<string>(type: "enum('M','A')", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_creacion = table.Column<int>(nullable: true),
                    fecha_actualizacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_actualizacion = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_asignacion);
                });

            migrationBuilder.CreateTable(
                name: "crm_caja",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    identificacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    comentario = table.Column<string>(type: "varchar(250)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_gestor = table.Column<uint>(nullable: false),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    id_cartera = table.Column<uint>(nullable: false),
                    id_campania = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_caja", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_cartera",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    nombre = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    tipo = table.Column<string>(type: "enum('Cedente','Vendida')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cartera", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_consulta_gestion_campania",
                columns: table => new
                {
                    idcuentacampania = table.Column<uint>(nullable: true),
                    idcuenta = table.Column<uint>(nullable: true),
                    asesor = table.Column<string>(type: "varchar(150)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    nombrecliente = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    cedula = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    ultimagestion = table.Column<DateTime>(type: "datetime", nullable: true),
                    acuerdo = table.Column<string>(type: "varchar(100)", nullable: true, defaultValueSql: "''")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    valoracuerdo = table.Column<float>(type: "float(10,4) unsigned", nullable: true, defaultValueSql: "'0.0000'"),
                    pago = table.Column<string>(type: "varchar(100)", nullable: true, defaultValueSql: "''")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    valorpago = table.Column<float>(type: "float(10,4) unsigned", nullable: true, defaultValueSql: "'0.0000'"),
                    idproducto = table.Column<uint>(nullable: true),
                    cuotaspendientes = table.Column<uint>(nullable: true, defaultValueSql: "'0'"),
                    deudavencida = table.Column<float>(type: "float(10,4) unsigned", nullable: true, defaultValueSql: "'0.0000'"),
                    deudatotal = table.Column<float>(type: "float(10,4)", nullable: true, defaultValueSql: "'0.0000'"),
                    estado = table.Column<string>(type: "varchar(50)", nullable: true, defaultValueSql: "''")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA2 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA3 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA4 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA5 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA6 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA7 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA8 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA9 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA10 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    label_estado = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fechamejorestado = table.Column<DateTime>(type: "datetime", nullable: true),
                    mejorestado = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci")
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "crm_consulta_gestion_campania_data",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false),
                    idcuentacampania = table.Column<uint>(nullable: true),
                    idcuenta = table.Column<uint>(nullable: true),
                    asesor = table.Column<string>(type: "varchar(150)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci"),
                    nombrecliente = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci"),
                    cedula = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci"),
                    ultimagestion = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci"),
                    acuerdo = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci"),
                    valoracuerdo = table.Column<float>(type: "float(10,4) unsigned", nullable: true),
                    pago = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci"),
                    valorpago = table.Column<float>(type: "float(10,4) unsigned", nullable: true),
                    idproducto = table.Column<uint>(nullable: true),
                    cuotaspendientes = table.Column<uint>(nullable: true),
                    montoinicial = table.Column<float>(type: "float(10,4) unsigned", nullable: true),
                    estado = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                        .Annotation("MySql:Collation", "utf8mb4_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_consulta_gestion_campania_data", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_productos_articulos",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta_campania_producto = table.Column<uint>(nullable: true),
                    articulo = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<decimal>(type: "decimal(10,4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_productos_articulos", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_notas",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    texto = table.Column<string>(type: "text", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_actualizacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    usuario_creacion = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_notas", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_matriz_acuerdos",
                columns: table => new
                {
                    agenteasignado = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_gestor = table.Column<int>(nullable: true),
                    nodocumento = table.Column<string>(type: "varchar(13)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    noproducto = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    ultgestion = table.Column<string>(type: "varchar(10)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fechacreacion = table.Column<string>(type: "varchar(10)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fechapromesa = table.Column<DateTime>(type: "date", nullable: true),
                    fecha_pago = table.Column<DateTime>(type: "date", nullable: true),
                    claseacuerdo = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    probabilidad = table.Column<string>(type: "varchar(10)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estadoacuerdo = table.Column<string>(type: "varchar(15)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor_acuerdo = table.Column<float>(nullable: true),
                    valor_pago = table.Column<float>(nullable: true),
                    porcentcumpli = table.Column<string>(type: "varchar(4)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    ultimagestion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    plazo = table.Column<int>(nullable: true),
                    saldodeuda = table.Column<float>(nullable: true),
                    saldovencido = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "crm_matriz_pagos",
                columns: table => new
                {
                    agente = table.Column<string>(type: "varchar(50)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    nodocumento = table.Column<string>(type: "varchar(13)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    nombrecliente = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    noproducto = table.Column<string>(type: "varchar(100)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estadocliente = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_pago = table.Column<DateTime>(type: "date", nullable: true),
                    pagoconacuerdo = table.Column<float>(nullable: true),
                    clasepago = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    pagocargado = table.Column<float>(nullable: true),
                    fechacargado = table.Column<DateTime>(type: "datetime", nullable: true),
                    saldodeuda = table.Column<float>(nullable: true),
                    saldovencido = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "crm_multicanal",
                columns: table => new
                {
                    ASESOR = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    ID_USUARIO = table.Column<uint>(nullable: true),
                    NOCUENTA = table.Column<uint>(nullable: false),
                    NOMBRECOMPLETO = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    IDENTIFICACION = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    ULTIMAGESTION = table.Column<string>(type: "varchar(30)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    GESTION = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    DIASMORA = table.Column<int>(nullable: true),
                    MEJORGESTION = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    FECHAMEJORGESTION = table.Column<DateTime>(type: "datetime", nullable: true),
                    DEUDATOTAL = table.Column<float>(type: "float(10,4)", nullable: true),
                    STATUSCARTERA = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    FECHAACUERDO = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    VALORACUERDO = table.Column<double>(type: "double(10,4)", nullable: true),
                    VALORPAGO = table.Column<double>(type: "double(10,4)", nullable: true),
                    FECHA_PAGO = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    TOTALACUERDO = table.Column<double>(type: "double(10,4)", nullable: true),
                    TOTALPAGO = table.Column<double>(type: "double(10,4)", nullable: true),
                    CUOTA = table.Column<int>(nullable: true),
                    TOTALCUOTAS = table.Column<int>(nullable: true),
                    DEUDAVENCIDA = table.Column<float>(nullable: true),
                    EXTRA1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA2 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA3 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA4 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA5 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA6 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA7 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA8 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA9 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA10 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci")
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "crm_multicanal_cab",
                columns: table => new
                {
                    idmulticanal = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    descripcion = table.Column<string>(type: "varchar(50)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    filtro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    status = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "date", nullable: true),
                    user_creacion = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    id_campania = table.Column<uint>(nullable: false),
                    user_modificacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    campo_ref1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    campo_ref2 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    campo_ref3 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.idmulticanal);
                });

            migrationBuilder.CreateTable(
                name: "crm_multicanal_marcadores",
                columns: table => new
                {
                    id_marcador = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_campania = table.Column<uint>(nullable: false),
                    nombre = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    opcion = table.Column<string>(type: "enum('Automatica','Manual')", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_inicio = table.Column<DateTime>(type: "datetime", nullable: true),
                    fecha_fin = table.Column<DateTime>(type: "datetime", nullable: true),
                    numero_cola = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    id_grupo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    id_url = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_creacion = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    campo_ref1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    campo_ref2 = table.Column<string>(type: "varchar(60)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    campo_ref3 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_marcador);
                });

            migrationBuilder.CreateTable(
                name: "crm_pausas",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    crm_pausascol = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_pausas", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_pausas_usuario",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_user = table.Column<uint>(nullable: true),
                    id_pausa = table.Column<uint>(nullable: true),
                    hora_inicio = table.Column<DateTime>(type: "datetime", nullable: true),
                    hora_fin = table.Column<DateTime>(type: "datetime", nullable: true),
                    duracion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_pausas_usuario", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_tarea_cuenta_campania",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_tarea = table.Column<uint>(nullable: true),
                    id_cuenta_campania = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_tarea_cuenta_campania", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "crm_tarea_objetivos",
                columns: table => new
                {
                    id_objetivo = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_tarea = table.Column<uint>(nullable: false),
                    id_tipoobjetivo = table.Column<uint>(nullable: false),
                    id_parametro = table.Column<uint>(nullable: true),
                    ca_valorparametro = table.Column<double>(nullable: true),
                    valorparametro_habilitar = table.Column<bool>(nullable: true),
                    valorparametro_finalizar = table.Column<bool>(nullable: true),
                    ca_valor = table.Column<double>(nullable: true),
                    ca_valorhabilitar = table.Column<bool>(nullable: true),
                    ca_valorfinalizar = table.Column<bool>(nullable: true),
                    ca_cantidad = table.Column<int>(nullable: true),
                    ca_cantidadhabilitar = table.Column<bool>(nullable: true),
                    ca_cantidadfinalizar = table.Column<bool>(nullable: true),
                    idgestion_principal = table.Column<uint>(nullable: true),
                    idgestion_sec1 = table.Column<uint>(nullable: true),
                    idgestion_sec2 = table.Column<uint>(nullable: true),
                    tc_cantidad = table.Column<int>(nullable: true),
                    tc_cantidadhabilitar = table.Column<bool>(nullable: true),
                    tc_cantidadfinalizar = table.Column<bool>(nullable: true),
                    tc_valor = table.Column<double>(nullable: true),
                    tc_valorhabilitar = table.Column<bool>(nullable: true),
                    tc_valorfinalizar = table.Column<bool>(nullable: true),
                    p_cantidad = table.Column<int>(nullable: true),
                    p_cantidadhabilitar = table.Column<bool>(nullable: true),
                    p_cantidadfinalizar = table.Column<bool>(nullable: true),
                    campo_ref1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    campo_ref2 = table.Column<double>(nullable: true),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_creacion = table.Column<uint>(nullable: true),
                    fecha_actualizacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_actualizacion = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_objetivo);
                });

            migrationBuilder.CreateTable(
                name: "acl_modulo_acciones",
                columns: table => new
                {
                    id_modulo_accion = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_modulo = table.Column<uint>(nullable: false),
                    id_accion = table.Column<uint>(nullable: false),
                    anulado = table.Column<string>(type: "varchar(2)", nullable: false, defaultValueSql: "'NO'")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    usr_creacion = table.Column<string>(type: "varchar(50)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    usr_modificacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_modulo_accion);
                    table.ForeignKey(
                        name: "FK_acl_modulo_acciones_acl_acciones",
                        column: x => x.id_accion,
                        principalTable: "acl_acciones",
                        principalColumn: "id_accion",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_acl_modulo_acciones_acl_modulos",
                        column: x => x.id_modulo,
                        principalTable: "acl_modulos",
                        principalColumn: "id_modulo",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_parametros",
                columns: table => new
                {
                    id_parametro = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    nombre_parametro = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor_parametro = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_modulo = table.Column<uint>(nullable: false),
                    Estado = table.Column<string>(type: "enum('A','I')", nullable: false, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    user_creacion = table.Column<uint>(nullable: true),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_modificacion = table.Column<uint>(nullable: true),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    campo_ref1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    campo_ref2 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_parametro);
                    table.ForeignKey(
                        name: "fk_acl_parametros_acl_modulos",
                        column: x => x.id_modulo,
                        principalTable: "acl_modulos",
                        principalColumn: "id_modulo",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_depto",
                columns: table => new
                {
                    id_depto = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    depto_valor = table.Column<string>(type: "varchar(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_region = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_depto);
                    table.ForeignKey(
                        name: "fk_acl_depto_acl_region",
                        column: x => x.id_region,
                        principalTable: "acl_region",
                        principalColumn: "id_region",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_accesos_roles",
                columns: table => new
                {
                    id_acceso_rol = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_rol = table.Column<uint>(nullable: true),
                    id_modulo = table.Column<uint>(nullable: true),
                    anulado = table.Column<string>(type: "varchar(2)", nullable: false, defaultValueSql: "'NO'")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    usr_creacion = table.Column<string>(type: "varchar(50)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    usr_modificacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_acceso_rol);
                    table.ForeignKey(
                        name: "FK__acl_modulos",
                        column: x => x.id_modulo,
                        principalTable: "acl_modulos",
                        principalColumn: "id_modulo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_acl_accesos_roles_acl_roles",
                        column: x => x.id_rol,
                        principalTable: "acl_roles",
                        principalColumn: "id_rol",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_usuario",
                columns: table => new
                {
                    id_usuario = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_rol = table.Column<uint>(nullable: false),
                    user_name = table.Column<string>(type: "varchar(150)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    codigo = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    cedula = table.Column<string>(type: "varchar(13)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    nombres = table.Column<string>(type: "varchar(250)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    apellidos = table.Column<string>(type: "varchar(250)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    email = table.Column<string>(type: "varchar(250)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    cargo = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    departamento = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    telefono = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    agentname = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    agentpass = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    anulado = table.Column<string>(type: "varchar(2)", nullable: true, defaultValueSql: "'NO'")
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    md5_password = table.Column<string>(type: "varchar(500)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    usr_creacion = table.Column<string>(type: "varchar(50)", nullable: false)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    usr_modificacion = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_usuario);
                    table.ForeignKey(
                        name: "FK_acl_usuario_acl_roles",
                        column: x => x.id_rol,
                        principalTable: "acl_roles",
                        principalColumn: "id_rol",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_arbol_gestion",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_parent = table.Column<uint>(nullable: true),
                    descripcion = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    peso = table.Column<int>(nullable: true),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_tipo_gestion = table.Column<uint>(nullable: true),
                    id_tipo_contacto = table.Column<uint>(nullable: true),
                    alerta = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    label_estado = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    label_nivel2 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_arbol_gestion", x => x.id);
                    table.ForeignKey(
                        name: "fk_arbol_gestion",
                        column: x => x.id_parent,
                        principalTable: "crm_arbol_gestion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_arbol_gestion2",
                        column: x => x.id_tipo_contacto,
                        principalTable: "crm_arbol_gestion_tipo_contacto",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_arbol_gestion3",
                        column: x => x.id_tipo_gestion,
                        principalTable: "crm_arbol_gestion_tipo_gestion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_asignacion_det",
                columns: table => new
                {
                    id_asignacion_det = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_asignacion = table.Column<int>(nullable: false),
                    id_usuario = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_asignacion_det);
                    table.ForeignKey(
                        name: "fk_crm_asignacion_det_crm_asignacion_cab",
                        column: x => x.id_asignacion,
                        principalTable: "crm_asignacion_cab",
                        principalColumn: "id_asignacion",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_campania",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    nombre = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_cartera = table.Column<uint>(nullable: true),
                    estado = table.Column<string>(type: "enum('A','I','V','C')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_inicio = table.Column<DateTime>(type: "date", nullable: true),
                    fecha_fin = table.Column<DateTime>(type: "date", nullable: true),
                    user_creacion = table.Column<int>(nullable: true),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_actualizacion = table.Column<int>(nullable: true),
                    fecha_actualizacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_campania", x => x.id);
                    table.ForeignKey(
                        name: "fk_cartera",
                        column: x => x.id_cartera,
                        principalTable: "crm_cartera",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_multicanal_detalle",
                columns: table => new
                {
                    ID = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IDMULTICANAL = table.Column<uint>(nullable: false),
                    ASESOR = table.Column<string>(type: "varchar(50)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    ID_USUARIO = table.Column<uint>(nullable: true),
                    NOCUENTA = table.Column<uint>(nullable: false),
                    NOMBRECOMPLETO = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    IDENTIFICACION = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    ULTIMAGESTION = table.Column<string>(type: "varchar(30)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    GESTION = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    DIASMORA = table.Column<int>(nullable: true),
                    MEJORGESTION = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    FECHAMEJORGESTION = table.Column<DateTime>(type: "datetime", nullable: true),
                    DEUDATOTAL = table.Column<float>(type: "float(10,4)", nullable: true),
                    STATUSCARTERA = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    FECHAACUERDO = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    VALORACUERDO = table.Column<double>(type: "double(10,4)", nullable: true),
                    VALORPAGO = table.Column<double>(type: "double(10,4)", nullable: true),
                    FECHA_PAGO = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    TOTALACUERDO = table.Column<double>(type: "double(10,4)", nullable: true),
                    TOTALPAGO = table.Column<double>(type: "double(10,4)", nullable: true),
                    CUOTA = table.Column<int>(nullable: true),
                    TOTALCUOTAS = table.Column<int>(nullable: true),
                    DEUDAVENCIDA = table.Column<float>(nullable: true),
                    EXTRA1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA2 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA3 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA4 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA5 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA6 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA7 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA8 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA9 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci"),
                    EXTRA10 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "latin1")
                        .Annotation("MySql:Collation", "latin1_swedish_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_multicanal_detalle", x => x.ID);
                    table.ForeignKey(
                        name: "fk_crm_multicanal_detalle",
                        column: x => x.IDMULTICANAL,
                        principalTable: "crm_multicanal_cab",
                        principalColumn: "idmulticanal",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_ciudad",
                columns: table => new
                {
                    id_ciudad = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_depto = table.Column<uint>(nullable: true),
                    ciudad_valor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_ciudad);
                    table.ForeignKey(
                        name: "FK_acl_ciudad_acl_depto",
                        column: x => x.id_depto,
                        principalTable: "acl_depto",
                        principalColumn: "id_depto",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    nombre = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_cartera_inicial = table.Column<uint>(nullable: true),
                    id_campania_inicial = table.Column<uint>(nullable: true),
                    identificacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    estado = table.Column<string>(type: "enum('A','E','P')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    gestor_creacion = table.Column<string>(type: "varchar(30)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas", x => x.id);
                    table.ForeignKey(
                        name: "fk_campania_cuenta",
                        column: x => x.id_campania_inicial,
                        principalTable: "crm_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_cartera_cuenta",
                        column: x => x.id_cartera_inicial,
                        principalTable: "crm_cartera",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_tareas",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    descripcion = table.Column<string>(type: "varchar(100)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_inicio = table.Column<DateTime>(type: "datetime", nullable: true),
                    fecha_fin = table.Column<DateTime>(type: "datetime", nullable: true),
                    meta = table.Column<float>(type: "float(10,4)", nullable: true),
                    idmulticanal = table.Column<uint>(nullable: false),
                    id_campania = table.Column<uint>(nullable: true),
                    Obligatorio = table.Column<string>(type: "enum('Si','No')", nullable: false)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_objetivo = table.Column<int>(nullable: true),
                    user_creacion = table.Column<uint>(nullable: true),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_modificacion = table.Column<uint>(nullable: true),
                    fecha_actualizacion = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_tareas", x => x.id);
                    table.ForeignKey(
                        name: "FK_crm_tareas_crm_campania",
                        column: x => x.id_campania,
                        principalTable: "crm_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "acl_distrito",
                columns: table => new
                {
                    id_distrito = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_ciudad = table.Column<uint>(nullable: true),
                    distrito_valor = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.id_distrito);
                    table.ForeignKey(
                        name: "FK_acl_distrito_acl_ciudad",
                        column: x => x.id_ciudad,
                        principalTable: "acl_ciudad",
                        principalColumn: "id_ciudad",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    id_campania = table.Column<uint>(nullable: true),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_arbol_gestion_mejor_gestion = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania", x => x.id);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_campania_1",
                        column: x => x.id_arbol_gestion_mejor_gestion,
                        principalTable: "crm_arbol_gestion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_cuentas_camp2",
                        column: x => x.id_campania,
                        principalTable: "crm_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_cuentas_camp",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_contacto",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    nombre = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    documento = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_relacion = table.Column<uint>(nullable: true),
                    observacion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "date", nullable: true),
                    fecha_modificacion = table.Column<DateTime>(type: "date", nullable: true),
                    observacionI = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_contacto", x => x.id);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_contacto",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_contacto_relacion",
                        column: x => x.id_relacion,
                        principalTable: "acl_relacion",
                        principalColumn: "id_relacion",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_detalle",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    campo = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<string>(type: "varchar(250)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_cuenta_campania_producto = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_detalle", x => x.id);
                    table.ForeignKey(
                        name: "fk_cuenta_detalle",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_email",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    email = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    tipo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    info = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "date", nullable: true),
                    fecha_modificacion = table.Column<DateTime>(type: "date", nullable: true),
                    estado = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    observacionI = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_email", x => x.id);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_email",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_tarea_gestor",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_tarea = table.Column<uint>(nullable: true),
                    id_usuario = table.Column<uint>(nullable: true),
                    id_cuenta_campania = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_tarea_gestor", x => x.id);
                    table.ForeignKey(
                        name: "FK_crm_tarea_gestor_crm_tareas",
                        column: x => x.id_tarea,
                        principalTable: "crm_tareas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_tarea_gestor_acl_usuario",
                        column: x => x.id_usuario,
                        principalTable: "acl_usuario",
                        principalColumn: "id_usuario",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_direccion",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    tipo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<string>(type: "varchar(250)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_depto = table.Column<uint>(nullable: true),
                    id_ciudad = table.Column<uint>(nullable: true),
                    id_distrito = table.Column<uint>(nullable: true),
                    id_zona = table.Column<uint>(nullable: true),
                    id_area = table.Column<uint>(nullable: true),
                    barrio = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    info1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "date", nullable: true),
                    fecha_modificacion = table.Column<DateTime>(type: "date", nullable: true),
                    estado = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    observacionI = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_direccion", x => x.id);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_direccion_5",
                        column: x => x.id_area,
                        principalTable: "acl_area",
                        principalColumn: "id_area",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_direccion_2",
                        column: x => x.id_ciudad,
                        principalTable: "acl_ciudad",
                        principalColumn: "id_ciudad",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_cuentas_direccion",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_direccion_1",
                        column: x => x.id_depto,
                        principalTable: "acl_depto",
                        principalColumn: "id_depto",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_direccion_3",
                        column: x => x.id_distrito,
                        principalTable: "acl_distrito",
                        principalColumn: "id_distrito",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_direccion_4",
                        column: x => x.id_zona,
                        principalTable: "acl_zona",
                        principalColumn: "id_zona",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_telefono",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta = table.Column<uint>(nullable: true),
                    tipo = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<string>(type: "varchar(25)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_depto = table.Column<uint>(nullable: true),
                    id_ciudad = table.Column<uint>(nullable: true),
                    id_distrito = table.Column<uint>(nullable: true),
                    info2 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_creacion = table.Column<DateTime>(type: "date", nullable: true),
                    fecha_modificacion = table.Column<DateTime>(type: "date", nullable: true),
                    estado = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    observacionI = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    conteo = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_telefono", x => x.id);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_telefono_acl_ciudad",
                        column: x => x.id_ciudad,
                        principalTable: "acl_ciudad",
                        principalColumn: "id_ciudad",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_cuentas_telefono",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_telefono_acl_depto",
                        column: x => x.id_depto,
                        principalTable: "acl_depto",
                        principalColumn: "id_depto",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_telefono_acl_distrito",
                        column: x => x.id_distrito,
                        principalTable: "acl_distrito",
                        principalColumn: "id_distrito",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_detalle_historico",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta_campania = table.Column<uint>(nullable: true),
                    campo = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<string>(type: "varchar(250)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_detalle_historico", x => x.id);
                    table.ForeignKey(
                        name: "fk_cuenta_campania_hist",
                        column: x => x.id_cuenta_campania,
                        principalTable: "crm_cuentas_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_gestor",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta_campania = table.Column<uint>(nullable: true),
                    id_usuario = table.Column<uint>(nullable: true),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_gestor", x => x.id);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_campania_gestor_crm_cuentas_campania",
                        column: x => x.id_cuenta_campania,
                        principalTable: "crm_cuentas_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_campania_gestor_acl_usuario",
                        column: x => x.id_usuario,
                        principalTable: "acl_usuario",
                        principalColumn: "id_usuario",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_productos",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta_campania = table.Column<uint>(nullable: true),
                    valor = table.Column<decimal>(type: "decimal(10,4)", nullable: true),
                    descripcion = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    cuotas_pendientes = table.Column<int>(nullable: true),
                    crm_cuentas_campania_productoscol = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    crm_cuentas_campania_productoscol1 = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    edadmor = table.Column<int>(nullable: true),
                    deudatotal = table.Column<decimal>(type: "decimal(10,4)", nullable: true),
                    valorporvencer = table.Column<decimal>(type: "decimal(10,4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_productos", x => x.id);
                    table.ForeignKey(
                        name: "fk_cuenta_campania",
                        column: x => x.id_cuenta_campania,
                        principalTable: "crm_cuentas_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_acuerdos",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    fecha_acuerdo = table.Column<DateTime>(type: "date", nullable: true),
                    tipo_acuerdo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor_acuerdo = table.Column<float>(type: "float(10,2)", nullable: true),
                    id_cuenta_campania_producto = table.Column<uint>(nullable: true),
                    id_gestor = table.Column<uint>(nullable: true),
                    efecto = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    cuota = table.Column<int>(nullable: true),
                    plazo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    probabilidad = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    comentario = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    fecha_visita = table.Column<DateTime>(type: "datetime", nullable: true),
                    hora_acuerdo = table.Column<TimeSpan>(type: "time", nullable: true),
                    id_direccion = table.Column<uint>(nullable: true),
                    estado_not = table.Column<string>(type: "enum('V','F')", nullable: true, defaultValueSql: "'V'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    usr_modificacion = table.Column<uint>(nullable: true),
                    fecha_modificacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    cant_modificacion = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_acuerdos", x => x.id);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_campania_acuerdos_crm_cuentas_campania_productos",
                        column: x => x.id_cuenta_campania_producto,
                        principalTable: "crm_cuentas_campania_productos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_cuentas_campania_acuerdos_crm_cuentas_direccion",
                        column: x => x.id_direccion,
                        principalTable: "crm_cuentas_direccion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_campania_acuerdos_acl_usuario",
                        column: x => x.id_gestor,
                        principalTable: "acl_usuario",
                        principalColumn: "id_usuario",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_productos_detalle",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuenta_campania_producto = table.Column<uint>(nullable: true),
                    descripcion = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    valor = table.Column<decimal>(type: "decimal(10,4)", nullable: true),
                    estado = table.Column<string>(type: "enum('Cancelada','Pendiente')", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_productos_detalle", x => x.id);
                    table.ForeignKey(
                        name: "fk_cuentas_campania_productos_detalle",
                        column: x => x.id_cuenta_campania_producto,
                        principalTable: "crm_cuentas_campania_productos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_gestion",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    id_cuentas_campania_productos = table.Column<uint>(nullable: true),
                    id_telefono = table.Column<uint>(nullable: true),
                    hora_gestion = table.Column<DateTime>(type: "datetime", nullable: true),
                    id_gestion_dinomi = table.Column<uint>(nullable: true),
                    tiempo_llamada = table.Column<int>(nullable: true),
                    estado_llamada = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_arbol_decision = table.Column<uint>(nullable: true),
                    detalle_gestion = table.Column<string>(type: "text", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    tipo_gestion = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_cuenta = table.Column<uint>(nullable: true),
                    id_cuenta_campania = table.Column<uint>(nullable: true),
                    id_gestor = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_gestion", x => x.id);
                    table.ForeignKey(
                        name: "fk_gestion_3",
                        column: x => x.id_arbol_decision,
                        principalTable: "crm_arbol_gestion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_crm_gestion_cuenta",
                        column: x => x.id_cuenta,
                        principalTable: "crm_cuentas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_gestion_4",
                        column: x => x.id_cuenta_campania,
                        principalTable: "crm_cuentas_campania",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_gestion_1",
                        column: x => x.id_cuentas_campania_productos,
                        principalTable: "crm_cuentas_campania_productos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_gestion_crm_cuentas_telefono",
                        column: x => x.id_telefono,
                        principalTable: "crm_cuentas_telefono",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "crm_cuentas_campania_pagos",
                columns: table => new
                {
                    id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    fecha_creacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    fecha_pago = table.Column<DateTime>(type: "date", nullable: true),
                    valor_pago = table.Column<float>(type: "float(10,4)", nullable: true),
                    id_cuenta_campania_producto = table.Column<uint>(nullable: true),
                    id_cuenta_campania_acuerdo = table.Column<uint>(nullable: true),
                    tipo_pago = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    numero_recibo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    cuota = table.Column<int>(nullable: true),
                    plazo = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    medio_pago = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    estado = table.Column<string>(type: "enum('A','I')", nullable: false, defaultValueSql: "'A'")
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    descripcion = table.Column<string>(type: "varchar(100)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    id_cuenta = table.Column<int>(nullable: false),
                    gestor_oficial = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci"),
                    gestor_ingreso = table.Column<string>(type: "varchar(20)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8")
                        .Annotation("MySql:Collation", "utf8_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_cuentas_campania_pagos", x => x.id);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_campania_pagos_crm_cuentas_campania_acuerdos",
                        column: x => x.id_cuenta_campania_acuerdo,
                        principalTable: "crm_cuentas_campania_acuerdos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_crm_cuentas_campania_pagos_crm_cuentas_campania_productos",
                        column: x => x.id_cuenta_campania_producto,
                        principalTable: "crm_cuentas_campania_productos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK__acl_modulos",
                table: "acl_accesos_roles",
                column: "id_modulo");

            migrationBuilder.CreateIndex(
                name: "FK_acl_accesos_roles_acl_roles",
                table: "acl_accesos_roles",
                column: "id_rol");

            migrationBuilder.CreateIndex(
                name: "codigo",
                table: "acl_acciones",
                column: "codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_acl_ciudad_acl_depto",
                table: "acl_ciudad",
                column: "id_depto");

            migrationBuilder.CreateIndex(
                name: "fk_acl_depto_1_idx",
                table: "acl_depto",
                column: "id_region");

            migrationBuilder.CreateIndex(
                name: "FK_acl_distrito_acl_ciudad",
                table: "acl_distrito",
                column: "id_ciudad");

            migrationBuilder.CreateIndex(
                name: "FK_acl_modulo_acciones_acl_acciones",
                table: "acl_modulo_acciones",
                column: "id_accion");

            migrationBuilder.CreateIndex(
                name: "FK_acl_modulo_acciones_acl_modulos",
                table: "acl_modulo_acciones",
                column: "id_modulo");

            migrationBuilder.CreateIndex(
                name: "codigo",
                table: "acl_modulos",
                column: "codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_acl_modulos_acl_modulos",
                table: "acl_modulos",
                column: "id_parent_modulo");

            migrationBuilder.CreateIndex(
                name: "fk_acl_parametros_acl_modulos_idx",
                table: "acl_parametros",
                column: "id_modulo");

            migrationBuilder.CreateIndex(
                name: "cedula",
                table: "acl_usuario",
                column: "cedula",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_acl_usuario_acl_roles",
                table: "acl_usuario",
                column: "id_rol");

            migrationBuilder.CreateIndex(
                name: "user_name",
                table: "acl_usuario",
                column: "user_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_arbol_gestion_idx",
                table: "crm_arbol_gestion",
                column: "id_parent");

            migrationBuilder.CreateIndex(
                name: "fk_arbol_gestion2_idx",
                table: "crm_arbol_gestion",
                column: "id_tipo_contacto");

            migrationBuilder.CreateIndex(
                name: "fk_arbol_gestion3_idx",
                table: "crm_arbol_gestion",
                column: "id_tipo_gestion");

            migrationBuilder.CreateIndex(
                name: "fk_crm_asignacion_det_crm_asignacion_cab_idx",
                table: "crm_asignacion_det",
                column: "id_asignacion");

            migrationBuilder.CreateIndex(
                name: "fk_cartera_idx",
                table: "crm_campania",
                column: "id_cartera");

            migrationBuilder.CreateIndex(
                name: "fk_campania_idx",
                table: "crm_cuentas",
                column: "id_campania_inicial");

            migrationBuilder.CreateIndex(
                name: "fk_cartera_idx",
                table: "crm_cuentas",
                column: "id_cartera_inicial");

            migrationBuilder.CreateIndex(
                name: "identificacion_UNIQUE",
                table: "crm_cuentas",
                column: "identificacion",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_campania_1_idx",
                table: "crm_cuentas_campania",
                column: "id_arbol_gestion_mejor_gestion");

            migrationBuilder.CreateIndex(
                name: "fk_cuentas_camp2_idx",
                table: "crm_cuentas_campania",
                column: "id_campania");

            migrationBuilder.CreateIndex(
                name: "fk_cuentas_camp_idx",
                table: "crm_cuentas_campania",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_acuerdos_crm_cuentas_campania_productos",
                table: "crm_cuentas_campania_acuerdos",
                column: "id_cuenta_campania_producto");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_acuerdos_crm_cuentas_direccion_idx",
                table: "crm_cuentas_campania_acuerdos",
                column: "id_direccion");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_acuerdos_acl_usuario",
                table: "crm_cuentas_campania_acuerdos",
                column: "id_gestor");

            migrationBuilder.CreateIndex(
                name: "fk_cuenta_campania_hist_idx",
                table: "crm_cuentas_campania_detalle_historico",
                column: "id_cuenta_campania");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_gestor_crm_cuentas_campania",
                table: "crm_cuentas_campania_gestor",
                column: "id_cuenta_campania");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_gestor_acl_usuario",
                table: "crm_cuentas_campania_gestor",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_pagos_crm_cuentas_campania_acuerdos",
                table: "crm_cuentas_campania_pagos",
                column: "id_cuenta_campania_acuerdo");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_campania_pagos_crm_cuentas_campania_productos",
                table: "crm_cuentas_campania_pagos",
                column: "id_cuenta_campania_producto");

            migrationBuilder.CreateIndex(
                name: "fk_cuenta_campania_idx",
                table: "crm_cuentas_campania_productos",
                column: "id_cuenta_campania");

            migrationBuilder.CreateIndex(
                name: "fk_cuentas_campania_productos_detalle_idx",
                table: "crm_cuentas_campania_productos_detalle",
                column: "id_cuenta_campania_producto");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_contacto_idx",
                table: "crm_cuentas_contacto",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_contacto_relacion",
                table: "crm_cuentas_contacto",
                column: "id_relacion");

            migrationBuilder.CreateIndex(
                name: "fk_cuenta_detalle_idx",
                table: "crm_cuentas_detalle",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_direccion_5_idx",
                table: "crm_cuentas_direccion",
                column: "id_area");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_direccion_2_idx",
                table: "crm_cuentas_direccion",
                column: "id_ciudad");

            migrationBuilder.CreateIndex(
                name: "fk_cuentas_direccion_idx",
                table: "crm_cuentas_direccion",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_direccion_1_idx",
                table: "crm_cuentas_direccion",
                column: "id_depto");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_direccion_3_idx",
                table: "crm_cuentas_direccion",
                column: "id_distrito");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_direccion_4_idx",
                table: "crm_cuentas_direccion",
                column: "id_zona");

            migrationBuilder.CreateIndex(
                name: "fk_crm_cuentas_email_idx",
                table: "crm_cuentas_email",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "uq_crm_cuentas_email",
                table: "crm_cuentas_email",
                columns: new[] { "id_cuenta", "email" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_telefono_acl_ciudad",
                table: "crm_cuentas_telefono",
                column: "id_ciudad");

            migrationBuilder.CreateIndex(
                name: "fk_cuentas_telefono_idx",
                table: "crm_cuentas_telefono",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_telefono_acl_depto",
                table: "crm_cuentas_telefono",
                column: "id_depto");

            migrationBuilder.CreateIndex(
                name: "FK_crm_cuentas_telefono_acl_distrito",
                table: "crm_cuentas_telefono",
                column: "id_distrito");

            migrationBuilder.CreateIndex(
                name: "uq_crm_cuentas_telefono",
                table: "crm_cuentas_telefono",
                columns: new[] { "id_cuenta", "valor" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk_gestion_3_idx",
                table: "crm_gestion",
                column: "id_arbol_decision");

            migrationBuilder.CreateIndex(
                name: "fk_crm_gestion_cuenta",
                table: "crm_gestion",
                column: "id_cuenta");

            migrationBuilder.CreateIndex(
                name: "fk_gestion_4_idx",
                table: "crm_gestion",
                column: "id_cuenta_campania");

            migrationBuilder.CreateIndex(
                name: "fk_gestion_1_idx",
                table: "crm_gestion",
                column: "id_cuentas_campania_productos");

            migrationBuilder.CreateIndex(
                name: "FK_crm_gestion_crm_cuentas_telefono",
                table: "crm_gestion",
                column: "id_telefono");

            migrationBuilder.CreateIndex(
                name: "fk_crm_multicanal_detalle_idx",
                table: "crm_multicanal_detalle",
                column: "IDMULTICANAL");

            migrationBuilder.CreateIndex(
                name: "FK_crm_tarea_gestor_crm_tareas",
                table: "crm_tarea_gestor",
                column: "id_tarea");

            migrationBuilder.CreateIndex(
                name: "FK_crm_tarea_gestor_acl_usuario",
                table: "crm_tarea_gestor",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "FK_crm_tareas_crm_campania",
                table: "crm_tareas",
                column: "id_campania");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "acl_accesos_roles");

            migrationBuilder.DropTable(
                name: "acl_efecto_acuerdo");

            migrationBuilder.DropTable(
                name: "acl_estados");

            migrationBuilder.DropTable(
                name: "acl_lista_asesoresasig");

            migrationBuilder.DropTable(
                name: "acl_modulo_acciones");

            migrationBuilder.DropTable(
                name: "acl_multicanal_estados");

            migrationBuilder.DropTable(
                name: "acl_parametro_objetivo");

            migrationBuilder.DropTable(
                name: "acl_parametros");

            migrationBuilder.DropTable(
                name: "acl_plantilla");

            migrationBuilder.DropTable(
                name: "acl_tipo_contacto");

            migrationBuilder.DropTable(
                name: "acl_tipo_info");

            migrationBuilder.DropTable(
                name: "acl_tipo_objetivo");

            migrationBuilder.DropTable(
                name: "crm_asignacion_det");

            migrationBuilder.DropTable(
                name: "crm_caja");

            migrationBuilder.DropTable(
                name: "crm_consulta_gestion_campania");

            migrationBuilder.DropTable(
                name: "crm_consulta_gestion_campania_data");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_detalle_historico");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_gestor");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_pagos");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_productos_articulos");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_productos_detalle");

            migrationBuilder.DropTable(
                name: "crm_cuentas_contacto");

            migrationBuilder.DropTable(
                name: "crm_cuentas_detalle");

            migrationBuilder.DropTable(
                name: "crm_cuentas_email");

            migrationBuilder.DropTable(
                name: "crm_cuentas_notas");

            migrationBuilder.DropTable(
                name: "crm_gestion");

            migrationBuilder.DropTable(
                name: "crm_matriz_acuerdos");

            migrationBuilder.DropTable(
                name: "crm_matriz_pagos");

            migrationBuilder.DropTable(
                name: "crm_multicanal");

            migrationBuilder.DropTable(
                name: "crm_multicanal_detalle");

            migrationBuilder.DropTable(
                name: "crm_multicanal_marcadores");

            migrationBuilder.DropTable(
                name: "crm_pausas");

            migrationBuilder.DropTable(
                name: "crm_pausas_usuario");

            migrationBuilder.DropTable(
                name: "crm_tarea_cuenta_campania");

            migrationBuilder.DropTable(
                name: "crm_tarea_gestor");

            migrationBuilder.DropTable(
                name: "crm_tarea_objetivos");

            migrationBuilder.DropTable(
                name: "acl_acciones");

            migrationBuilder.DropTable(
                name: "acl_modulos");

            migrationBuilder.DropTable(
                name: "crm_asignacion_cab");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_acuerdos");

            migrationBuilder.DropTable(
                name: "acl_relacion");

            migrationBuilder.DropTable(
                name: "crm_cuentas_telefono");

            migrationBuilder.DropTable(
                name: "crm_multicanal_cab");

            migrationBuilder.DropTable(
                name: "crm_tareas");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania_productos");

            migrationBuilder.DropTable(
                name: "crm_cuentas_direccion");

            migrationBuilder.DropTable(
                name: "acl_usuario");

            migrationBuilder.DropTable(
                name: "crm_cuentas_campania");

            migrationBuilder.DropTable(
                name: "acl_area");

            migrationBuilder.DropTable(
                name: "acl_distrito");

            migrationBuilder.DropTable(
                name: "acl_zona");

            migrationBuilder.DropTable(
                name: "acl_roles");

            migrationBuilder.DropTable(
                name: "crm_arbol_gestion");

            migrationBuilder.DropTable(
                name: "crm_cuentas");

            migrationBuilder.DropTable(
                name: "acl_ciudad");

            migrationBuilder.DropTable(
                name: "crm_arbol_gestion_tipo_contacto");

            migrationBuilder.DropTable(
                name: "crm_arbol_gestion_tipo_gestion");

            migrationBuilder.DropTable(
                name: "crm_campania");

            migrationBuilder.DropTable(
                name: "acl_depto");

            migrationBuilder.DropTable(
                name: "crm_cartera");

            migrationBuilder.DropTable(
                name: "acl_region");
        }
    }
}
