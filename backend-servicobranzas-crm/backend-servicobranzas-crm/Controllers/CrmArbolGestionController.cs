using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmArbolGestionController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmArbolGestionController(ps_crmContext context)
        {
            _context = context;
        }


        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllCrmArbolGestion()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.CrmArbolGestion.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpGet("CrmArbolGestion")]
        public async Task<ModeloRespuesta> GetCrmArbolGestion(uint id)
        {
            try
            {
                var crmArbolGestion = await _context.CrmArbolGestion.FindAsync(id);

                if (crmArbolGestion == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Arbol no encontrado"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data =  crmArbolGestion
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpGet("NivelUno")]
        public async Task<ModeloRespuesta> NivelUno(uint IdCuenta)
        {
            try
            {

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.CrmArbolGestion.Where(x => x.IdParent == null  && x.Estado == "A"
                                                                && x.IdArbolGestion == _context.CrmCuentasCampania
                                                                                      .Where(x => x.Id == IdCuenta)
                                                                                      .Select(x => x.IdCampaniaNavigation.IdArbolGestion).FirstOrDefault()
                                                                ).ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpGet("SubNiveles")]
        public async Task<ModeloRespuesta> SubNiveles(uint IdParent)
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.CrmArbolGestion.Where(x => x.IdParent == IdParent && x.Estado == "A").ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmArbolGestion(CrmArbolGestion crmArbolGestion)
        {
            try
            {
                if (!CrmArbolGestionExists(crmArbolGestion.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Arbol que desea modificar no existe"
                    };
                }

                var crmArbolGestionModifi = _context.CrmArbolGestion.FirstOrDefault(x => x.Id == crmArbolGestion.Id);
                if (crmArbolGestion.IdParent != null)
                    crmArbolGestionModifi.IdParent = crmArbolGestion.IdParent;
                if (!string.IsNullOrEmpty(crmArbolGestion.Descripcion))
                    crmArbolGestionModifi.Descripcion = crmArbolGestion.Descripcion;
                if (crmArbolGestion.Peso != null)
                    crmArbolGestionModifi.Peso = crmArbolGestion.Peso;
                if (!string.IsNullOrEmpty(crmArbolGestion.Estado))
                    crmArbolGestionModifi.Estado = crmArbolGestionModifi.Estado;
                if (crmArbolGestion.IdTipoGestion != null)
                    crmArbolGestionModifi.IdTipoGestion = crmArbolGestion.IdTipoGestion;
                if (crmArbolGestion.IdTipoContacto != null)
                    crmArbolGestionModifi.IdTipoContacto = crmArbolGestion.IdTipoContacto;
                if (!string.IsNullOrEmpty(crmArbolGestion.Alerta))
                    crmArbolGestionModifi.Alerta = crmArbolGestion.Alerta;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Arbol Modificado"
            };
        }


        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmArbolGestion(CrmArbolGestion crmArbolGestion)
        {
            try
            {
                _context.CrmArbolGestion.Add(crmArbolGestion);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Arbol Creado",
                Data =  await _context.CrmArbolGestion.FindAsync(crmArbolGestion.Id)
            };
        }


        [HttpGet("CambiarEstado")]
        public async Task<ModeloRespuesta> CambiarEstadoCrmArbolGestion(uint id, string estado)
        {
            try
            {
                if (!CrmArbolGestionExists(id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Arbol que desea modificar no existe"
                    };
                }

                var crmArbolGestionModifi = _context.CrmArbolGestion.FirstOrDefault(x => x.Id == id);
                crmArbolGestionModifi.Estado = estado;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Arbol Modificado"
            };
        }

        [HttpGet("reciboCaja")]
        public async Task<ModeloRespuesta> reciboCaja()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.CrmCaja.Where(x => x.Estado == "A").ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }




        private bool CrmArbolGestionExists(uint id)
        {
            return _context.CrmArbolGestion.Any(e => e.Id == id);
        }
    }
}
