using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaProductosArticulosController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasCampaniaProductosArticulosController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentasCampaniaProductosArticulos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasCampaniaProductosArticulos>>> GetCrmCuentasCampaniaProductosArticulos()
        {
            return await _context.CrmCuentasCampaniaProductosArticulos.ToListAsync();
        }

        // GET: api/CrmCuentasCampaniaProductosArticulos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaProductosArticulos>> CrmCuentasCampaniaProductosArticulos(uint id)
        {
            var CrmCuentasCampaniaProductosArticulos = await _context.CrmCuentasCampaniaProductosArticulos.FindAsync(id);

            if (CrmCuentasCampaniaProductosArticulos == null)
            {
                return NotFound();
            }

            return CrmCuentasCampaniaProductosArticulos;
        }

        // PUT: api/CrmCuentasCampaniaProductosArticulos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentasCampaniaProductosArticulos(uint id, CrmCuentasCampaniaProductosArticulos crmCuentasCampaniaProductosArticulos)
        {
            if (id != crmCuentasCampaniaProductosArticulos.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentasCampaniaProductosArticulos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasCampaniaProductosArticulosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentasCampaniaProductosArticulos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasCampaniaProductosArticulos>> PostCrmCuentasCampaniaProductosArticulos(CrmCuentasCampaniaProductosArticulos crmCuentasCampaniaProductosArticulos)
        {
            _context.CrmCuentasCampaniaProductosArticulos.Add(crmCuentasCampaniaProductosArticulos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasCampaniaProductosArticulos", new { id = crmCuentasCampaniaProductosArticulos.Id }, crmCuentasCampaniaProductosArticulos);
        }

        // DELETE: api/CrmCuentasCampaniaProductosArticulos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaProductosArticulos>> DeleteCrmCuentasCampaniaProductosArticulos(uint id)
        {
            var crmCuentasCampaniaProductosArticulos = await _context.CrmCuentasCampaniaProductosArticulos.FindAsync(id);
            if (crmCuentasCampaniaProductosArticulos == null)
            {
                return NotFound();
            }

            _context.CrmCuentasCampaniaProductosArticulos.Remove(crmCuentasCampaniaProductosArticulos);
            await _context.SaveChangesAsync();

            return crmCuentasCampaniaProductosArticulos;
        }

        private bool CrmCuentasCampaniaProductosArticulosExists(uint id)
        {
            return _context.CrmCuentasCampaniaProductosArticulos.Any(e => e.Id == id);
        }

        // CrmCuentasCampaniaProductosArticulos/GetProductosArticulos?idCuentaCampania

        [HttpGet("GetProductosArticulos")]
        public async Task<ModeloRespuesta> GetProductosArticulos(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampaniaProductos.Any(x => x.IdCuentaCampania == idCuentaCampania))
                {

                    var productosArticulos = await _context.CrmCuentasCampaniaProductosArticulos
                        .Join(_context.CrmCuentasCampaniaProductos, articulo => articulo.IdCuentaCampaniaProducto, producto => producto.Id,
                                (articulo, producto) => new { articulo, producto })
                        .Join(_context.CrmCuentasCampania, acp => acp.producto.IdCuentaCampania, cuencamp => cuencamp.Id,
                                (acp, cuencamp) => new { acp, cuencamp })
                        .Where(x => x.acp.producto.IdCuentaCampania == idCuentaCampania)
                        .Select(x => new
                        {
                            id = x.acp.articulo.Id,
                            idCuentaCampaniaProducto = x.acp.articulo.IdCuentaCampaniaProducto,
                            articulo = x.acp.articulo.Articulo,
                            valortotal = x.acp.articulo.Valor,
                            detalle = x.acp.producto.CrmCuentasCampaniaProductosDetalle.Where(a => a.IdCuentaCampaniaProducto == x.acp.articulo.IdCuentaCampaniaProducto)
                                      .Select(a => new { a.Descripcion, a.Valor, a.Estado })

                        }).ToListAsync();
                    /*var productosArticulos = await _context.CrmCuentasCampaniaProductosDetalle
                        .Join(_context.CrmCuentasCampaniaProductos, detalle => detalle.IdCuentaCampaniaProducto, producto => producto.Id,
                                (detalle, producto) => new { detalle, producto })
                        .Join(_context.CrmCuentasCampaniaProductosArticulos, ap => ap.detalle.IdCuentaCampaniaProducto, articulo => articulo.IdCuentaCampaniaProducto,
                                (ap, articulo) => new { ap, articulo })
                        .Where(x => x.ap.producto.IdCuentaCampania == idCuentaCampania)
                        .Select(x => new
                        {
                            id = x.articulo.Id,
                            idCuentaCampaniaProducto = x.articulo.IdCuentaCampaniaProducto,
                            articulo = x.articulo.Articulo,
                            valortotal = x.articulo.Valor,
                            //detalle = new { x.ap.detalle.Descripcion, x.ap.detalle.Valor, x.ap.detalle.Estado }
                            detalle = 

                        }).ToListAsync();*/

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = productosArticulos

                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "No existen artículos con este cliente."
            };
        }
    }
}
