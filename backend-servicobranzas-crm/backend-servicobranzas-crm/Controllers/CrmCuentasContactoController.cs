using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasContactoController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasContactoController(ps_crmContext context)
        {
            _context = context;
        }

     
       

        private bool CrmCuentasContactoExists(uint id)
        {
            return _context.CrmCuentasContacto.Any(e => e.Id == id);
        }

        //Obtener contactos por cuenta
        //api/CrmCuentasContacto/GetContacto?idCuentaCampania=
        [HttpGet("GetContacto")]
        public async Task<ModeloRespuesta> GetContacto( uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var contactoCliente = new object();
                    var cuenta = _context.CrmCuentasCampania
                        .FirstOrDefault(x => x.Id == idCuentaCampania).IdCuenta;
                    if (cuenta != null)
                    {
                        contactoCliente = await _context.CrmCuentasContacto
                            .Where(x => x.IdCuenta == cuenta)
                            .Select(x => new
                            {
                                idContacto = x.Id,
                                contacto = x.Nombre,
                                documento = x.Documento,
                                relacion = new { x.IdRelacionNavigation.IdRelacion, x.IdRelacionNavigation.ValorRelacion },
                                observacion = x.Observacion,
                                estado = x.Estado,
                                cedula = x.CedulaRef,
                                telefono = x.ValorRef,
                                tipodato = x.TipoDatoRef

                            }).ToListAsync();
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = contactoCliente
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //Guarda contactos por cuenta y campaña.
        // api/CrmCuentasContacto/Crear
        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmCuentasContacto(CrmCuentasContacto crmCuentasContacto)
        {
            try
            {
                _context.CrmCuentasContacto.Add(crmCuentasContacto);
                crmCuentasContacto.FechaCreacion = DateTime.Now;
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Contacto Creado",
                Data = await _context.CrmCuentasContacto.FindAsync(crmCuentasContacto.Id)
            };
        }
        //Modificar contactos.
        // api/CrmCuentasContacto/Modificar
        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmCuentasContacto(CrmCuentasContacto crmCuentasContacto)
        {
            try
            {
                if (!CrmCuentasContactoExists(crmCuentasContacto.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Contacto que desea modificar no existe"
                    };
                }

                var CrmCuentasContactoModificar = _context.CrmCuentasContacto.FirstOrDefault(x => x.Id == crmCuentasContacto.Id);
                if(!String.IsNullOrEmpty(crmCuentasContacto.Nombre))
                    CrmCuentasContactoModificar.Nombre = crmCuentasContacto.Nombre;
                if (!String.IsNullOrEmpty(crmCuentasContacto.CedulaRef))
                    CrmCuentasContactoModificar.CedulaRef = crmCuentasContacto.CedulaRef;
                if (!String.IsNullOrEmpty(crmCuentasContacto.ValorRef))
                    CrmCuentasContactoModificar.ValorRef = crmCuentasContacto.ValorRef;
                if (crmCuentasContacto.IdRelacion!= null)
                    CrmCuentasContactoModificar.IdRelacion = crmCuentasContacto.IdRelacion;
                if (!String.IsNullOrEmpty(crmCuentasContacto.Observacion))
                    CrmCuentasContactoModificar.Observacion = crmCuentasContacto.Observacion;
                if (!String.IsNullOrEmpty(crmCuentasContacto.Estado))
                    CrmCuentasContactoModificar.Estado = crmCuentasContacto.Estado;
                CrmCuentasContactoModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Contacto Modificado"
            };
        }

        //Inactivar contacto por cuenta.
        // api/CrmCuentasContacto/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmCuentasContacto(CrmCuentasContacto crmCuentasContacto)
        {
            try
            {
                if (!CrmCuentasContactoExists(crmCuentasContacto.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Contacto que desea inactivar no existe"
                    };
                }

                var CrmCuentasContactoInactivar = _context.CrmCuentasContacto.FirstOrDefault(x => x.Id == crmCuentasContacto.Id);
                
                if (!String.IsNullOrEmpty(crmCuentasContacto.Estado))
                    CrmCuentasContactoInactivar.Estado = crmCuentasContacto.Estado;
                if (!String.IsNullOrEmpty(crmCuentasContacto.ObservacionI))
                    CrmCuentasContactoInactivar.ObservacionI = crmCuentasContacto.ObservacionI;
                CrmCuentasContactoInactivar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Contacto Inactivo"
            };
        }
    }
}
