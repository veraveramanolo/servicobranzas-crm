using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using System.IdentityModel.Tokens.Jwt;
using TimeZoneConverter;
using backend_servicobranzas_crm.Services.Class;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmTareasController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmTareasController(ps_crmContext context)
        {
            _context = context;
        }


        // api/CrmTareas/AllTareas
        [HttpGet("AllTareas")]
        public async Task<ModeloRespuesta> AllTareas(uint idCampania)
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Id == idCampania))
                {
                    if (_context.CrmTareas.Any(x => x.IdCampania == idCampania && x.Estado == "A"))
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await _context.CrmTareas
                                    .Where(x => x.IdCampania == idCampania && x.Estado == "A")
                                    .OrderByDescending(x => x.FechaCreacion)
                                    .ToListAsync()
                        };
                    }
                    else
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 0,
                            Mensage = "No hay Tareas con la campania "+ idCampania
                        };
                    }
                }
                else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No existe la campania "+ idCampania
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpGet("Tareas")]
        public async Task<ModeloRespuesta> GetCrmTareas(uint id)
        {
            try
            {
                var crmTareas = await _context.CrmTareas.FindAsync(id);

                if (crmTareas == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Tarea no encontrada"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = crmTareas
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/CrmTareas/ModificarTarea
        [HttpPost("ModificarTarea")]
        public async Task<ModeloRespuesta> ModificarTarea(CrmTareas crmTareas)
        {
            try
            {
                if (!CrmTareasExists(crmTareas.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Tarea que desea modificar no existe"
                    };
                }
                else
                {
                    var CrmTareaModificar = _context.CrmTareas.FirstOrDefault(x => x.Id == crmTareas.Id);

                    if (!String.IsNullOrEmpty(crmTareas.Descripcion))
                    CrmTareaModificar.Descripcion = crmTareas.Descripcion;
                    if (!String.IsNullOrEmpty(crmTareas.Obligatorio))
                    CrmTareaModificar.Obligatorio = crmTareas.Obligatorio;
                    TimeZoneInfo info = TZConvert.GetTimeZoneInfo("America/Bogota");
                    DateTime dt_hora_pacifico_ini = TimeZoneInfo.ConvertTime(Convert.ToDateTime(crmTareas.FechaInicio), info);
                    DateTime dt_hora_pacifico_fin = TimeZoneInfo.ConvertTime(Convert.ToDateTime(crmTareas.FechaFin), info);
                    crmTareas.FechaInicio = dt_hora_pacifico_ini;
                    crmTareas.FechaFin = dt_hora_pacifico_fin;
                    if (crmTareas.FechaInicio != null)
                    CrmTareaModificar.FechaInicio = dt_hora_pacifico_ini;
                    if (crmTareas.FechaFin != null)
                    CrmTareaModificar.FechaFin = dt_hora_pacifico_fin;
                    CrmTareaModificar.FechaActualizacion = DateTime.Now;

                    _decoder = new Decoder();
                    uint idusuario = _decoder.GetDecoder(HttpContext);
                    CrmTareaModificar.UserModificacion = idusuario;

                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Tarea Modificada"
            };
        }


        // api/CrmTareas/CrearTarea
        [HttpPost("CrearTarea")]
        public async Task<ModeloRespuesta> CrearTarea(CrmTareas crmTareas)
        {
            try{
                _context.CrmTareas.Add(crmTareas);
                crmTareas.FechaCreacion = DateTime.Now;
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                crmTareas.UserCreacion = idusuario;

                TimeZoneInfo info = TZConvert.GetTimeZoneInfo("America/Bogota");
                DateTime dt_hora_pacifico_ini = TimeZoneInfo.ConvertTime(Convert.ToDateTime(crmTareas.FechaInicio), info);
                DateTime dt_hora_pacifico_fin = TimeZoneInfo.ConvertTime(Convert.ToDateTime(crmTareas.FechaFin), info);
                crmTareas.FechaInicio = dt_hora_pacifico_ini;
                crmTareas.FechaFin = dt_hora_pacifico_fin;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Tarea Creada",
                Data = await _context.CrmTareas.FindAsync(crmTareas.Id)
            };
        }

        private bool CrmTareasExists(uint id)
        {
            return _context.CrmTareas.Any(e => e.Id == id);
        }

        //Inactivar Tarea.
        // api/CrmTareas/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmTareas(CrmTareas crmTareas)
        {
            try
            {
                if (!CrmTareasExists(crmTareas.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Tarea que desea inactivar no existe"
                    };
                }

                var CrmTareasModificar = _context.CrmTareas.FirstOrDefault(x => x.Id == crmTareas.Id);
                
                CrmTareasModificar.Estado = "I";
                CrmTareasModificar.FechaActualizacion = DateTime.Now;

                //Obtener Usuario
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                CrmTareasModificar.UserModificacion = idusuario;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Tarea Inactivo"
            };
        }

        // api/CrmTareas/TareasUsuario
        [HttpGet("TareasUsuario")]
        public async Task<ModeloRespuesta> TareasUsuario()
        {
            try
            {
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);

                if (_context.CrmTareas.Any(x => x.Estado == "A"))
                    {
                        var estadoT = "Ejecutado";
                        var estadoF = "En Proceso";
                        var CrmTareasUsuario= await _context.CrmTareas
                                    .Where(x => x.Estado == "A")
                                    .Join(_context.CrmAsignacionCab, tareas => tareas.Id, asigcab => asigcab.IdTarea,
                                    (tareas, asigcab) => new { tareas, asigcab })
                                    .Join(_context.CrmAsignacionDet, tareasig => tareasig.asigcab.IdAsignacion, asigdet => asigdet.IdAsignacion,
                                    (tareasig, asigdet) => new { tareasig, asigdet })
                                    .Join(_context.CrmMulticanalDetalle, ta => ta.tareasig.tareas.Idmulticanal, detalle => detalle.Idmulticanal,
                                    (ta,detalle) => new { ta,detalle })
                                    .Where(x => x.ta.asigdet.IdUsuario == idusuario)
                                    .OrderByDescending(x => x.ta.tareasig.tareas.FechaCreacion)
                                    .Select(x => new
                                    {
                                        id = x.ta.tareasig.tareas.Id,
                                        descripcion = x.ta.tareasig.tareas.Descripcion,
                                        fechaInicio = x.ta.tareasig.tareas.FechaInicio,
                                        fechaFin = x.ta.tareasig.tareas.FechaFin,
                                        idmulticanal = x.ta.tareasig.tareas.Idmulticanal,
                                        actual = _context.CrmMulticanalDetalle.Where(d => d.Idmulticanal == x.ta.tareasig.tareas.Idmulticanal && d.Gestion != null)
                                                 .Select(d => d.Gestion).Count(),
                                        inicio = _context.CrmMulticanalDetalle.Where(d => d.Idmulticanal == x.ta.tareasig.tareas.Idmulticanal)
                                                 .Select(d => d.Idmulticanal).Count(),
                                        ejecutadonum = _context.CrmMulticanalDetalle.Where(d => d.Idmulticanal == x.ta.tareasig.tareas.Idmulticanal && d.Gestion.Contains(null))
                                                        .Join(_context.CrmGestion, detalle => detalle.Nocuenta, g => g.IdCuenta,
                                                        (detalle,g) => new { detalle,g }).Select(d => d.g.IdArbolDecision).Count()
                                    }).Distinct().Select(x  => new 
                                        {
                                            x.id,
                                            x.descripcion,
                                            x.fechaInicio,
                                            x.fechaFin,
                                            x.idmulticanal,
                                            x.actual,
                                            x.inicio,
                                            estado = (x.ejecutadonum >= (x.inicio - x.actual) ? estadoT : estadoF )
                                        })
                                    .ToListAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = CrmTareasUsuario
                        };
                    }
                    else
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 0,
                            Mensage = "No hay Tareas activas"
                        };
                    }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        // api/CrmTareas/BaseTareas
        [HttpGet("BaseTareas")]
        public async Task<ModeloRespuesta> BaseTareas(uint idtarea)
        {
            try
            {
                if (_context.CrmTareas.Any(x => x.Id == idtarea && x.Estado == "A"))
                    {
                        _decoder = new Decoder();
                        uint idusuario = _decoder.GetDecoder(HttpContext);
                        var asesorlog = _context.AclUsuario.Where(u => u.IdUsuario == idusuario).Select(u => u.UserName).FirstOrDefault();
                        
                        var asesortemp = _context.CrmTareas.Where(x => x.Id == idtarea && x.Estado == "A" 
                                          && x.FechaInicio<=DateTime.Now && DateTime.Now<=x.FechaFin)
                                         .Join(_context.CrmTareaGestor, tareas => tareas.Id, gtareas => gtareas.IdTarea,
                                         (tareas,gtareas) => new { tareas,gtareas })
                                         .Select(x => new
                                            {
                                                x.gtareas.IdUsuario,
                                                x.tareas.FechaInicio,
                                                x.tareas.FechaFin,
                                                x.gtareas.IdCuentaCampania
                                            }).ToList();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await _context.CrmTareas
                                    .Where(x => x.Id == idtarea && x.Estado == "A")
                                    .Join(_context.CrmMulticanalDetalle, tarea => tarea.Idmulticanal, multdet => multdet.Idmulticanal,
                                    (tarea,multdet) => new { tarea,multdet })
                                    .Join(_context.CrmCuentasCampania, tm => tm.multdet.Nocuenta, cc => cc.IdCuenta,
                                    (tm,cc) => new { tm,cc })
                                    .Join(_context.CrmCuentasCampaniaProductos, tmc => tmc.cc.Id, p => p.IdCuentaCampania,
                                    (tmc,p) => new { tmc,p })
                                    .Select(x => new
                                    {
                                        idcuenta = x.tmc.tm.multdet.Nocuenta,
                                        idcuentacampania = x.p.IdCuentaCampania,
                                        asesor = (asesortemp.Any() ? _context.CrmCuentasCampania.Where(y => y.Id == x.p.IdCuentaCampania)
                                                 .Join(_context.CrmTareaGestor, cc => cc.Id, tg => tg.IdCuentaCampania,
                                                 (cc,tg) => new {cc,tg})
                                                 .Join(_context.AclUsuario, ctg => ctg.tg.IdUsuario, usuario => usuario.IdUsuario,
                                                 (ctg,usuario) => new {ctg,usuario})
                                                 .Select(u => u.usuario.UserName).Distinct().FirstOrDefault() : x.tmc.tm.multdet.Asesor),
                                        // asesor = (asesortemp.Any() ? _context.CrmMulticanalDetalle.Where(y => y.Idmulticanal == x.tmc.tm.multdet.Idmulticanal)
                                        //             .Join(_context.CrmCuentasCampania, md1 => md1.Nocuenta, cc => cc.IdCuenta,
                                        //             (md1,cc) => new {md1,cc})
                                        //             .Join(_context.CrmTareaGestor, md => md.cc.Id, tg => tg.IdCuentaCampania,
                                        //             (md,tg) => new {md,tg})
                                        //             .Join(_context.AclUsuario, mtg => mtg.tg.IdUsuario, usuario => usuario.IdUsuario,
                                        //             (mtg,usuario)=> new {mtg,usuario})
                                        //             .Where(y => y.mtg.tg.IdCuentaCampania == x.p.IdCuentaCampania)
                                        //             .Select(y => y.usuario.UserName).FirstOrDefault() : x.tmc.tm.multdet.Asesor),
                                        // asesor = x.tmc.tm.multdet.Asesor,
                                        nombrecliente = x.tmc.tm.multdet.Nombrecompleto,
                                        cedula = x.tmc.tm.multdet.Identificacion,
                                        ultimagestion = x.tmc.tm.multdet.Ultimagestion,
                                        acuerdo = x.tmc.tm.multdet.Fechaacuerdo,
                                        valoracuerdo = x.tmc.tm.multdet.Valoracuerdo,
                                        pago = x.tmc.tm.multdet.FechaPago,
                                        valorpago = x.tmc.tm.multdet.Valorpago,
                                        idproducto = x.p.Id,
                                        cuotaspendientes = x.tmc.tm.multdet.Cuota,
                                        deudavencida = x.p.Valor,
                                        estado = x.tmc.tm.multdet.Mejorgestion,
                                        labelEstado = _context.CrmGestion.Join(_context.CrmCuentasCampaniaProductos, gestion => gestion.IdCuentasCampaniaProductos, producto => producto.Id,
                                                        (gestion,producto) => new { gestion,producto }).Join(_context.CrmArbolGestion, gp => gp.gestion.IdArbolDecision, arbol => arbol.Id,
                                                        (gp,arbol) => new { gp,arbol }).Where(g => g.gp.producto.Id == x.p.Id).OrderByDescending(g => g.arbol.Peso).Select(g => g.arbol.LabelEstado).FirstOrDefault()
                                    }).Where(x => x.asesor == asesorlog).Distinct()
                                    .ToListAsync()
                        };
                    }
                    else
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 0,
                            Mensage = "No existe Tarea o no está activa"
                        };
                    }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
