using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using System.IdentityModel.Tokens.Jwt;
using backend_servicobranzas_crm.Services.Class;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmTareaObjetivosController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmTareaObjetivosController(ps_crmContext context)
        {
            _context = context;
        }


        // api/CrmTareaObjetivos/GetObjetivos
        [HttpGet("GetObjetivos")]
        public async Task<ModeloRespuesta> GetObjetivos(uint idTarea)
        {
            try
            {
                if (_context.CrmTareas.Any(x => x.Id == idTarea))
                {
                    if (_context.CrmTareaObjetivos.Any(x => x.IdTarea == idTarea && x.Estado == "A"))
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await _context.CrmTareas
                                    .Join(_context.CrmTareaObjetivos, tarea => tarea.Id, tareaobj => tareaobj.IdTarea,
                                         (tarea, tareaobj) => new { tarea, tareaobj })
                                    .Join(_context.AclTipoObjetivo, tareatipo => tareatipo.tareaobj.IdTipoobjetivo, tipoobj => tipoobj.IdTipoobjetivo,
                                         (tareatipo, tipoobj) => new { tareatipo, tipoobj })
                                    .Join(_context.AclParametroObjetivo, tareatipoobj => tareatipoobj.tareatipo.tareaobj.IdParametro, paramobj => paramobj.IdParametro,
                                         (tareatipoobj, paramobj) => new { tareatipoobj, paramobj })
                                    .Where(x => x.tareatipoobj.tareatipo.tarea.Id == idTarea && x.tareatipoobj.tareatipo.tarea.Estado == "A"
                                                && x.tareatipoobj.tareatipo.tareaobj.Estado == "A")
                                    .Select(x => new
                                        {
                                            idtarea = x.tareatipoobj.tareatipo.tarea.Id,
                                            idobjetivo = x.tareatipoobj.tareatipo.tareaobj.IdObjetivo,
                                            idtipoobjetivo = x.tareatipoobj.tipoobj.IdTipoobjetivo,
                                            valortipoobjetivo = x.tareatipoobj.tipoobj.ValorTipoobjetivo,
                                            valorparametro = x.paramobj.ValorParametro,
                                            cavalorparametro = x.tareatipoobj.tareatipo.tareaobj.CaValorparametro,
                                            valorparametrofinalizar = x.tareatipoobj.tareatipo.tareaobj.ValorparametroFinalizar,
                                            cavalor = x.tareatipoobj.tareatipo.tareaobj.CaValor,
                                            cavalorfinalizar = x.tareatipoobj.tareatipo.tareaobj.CaValorfinalizar,
                                            cacantidad = x.tareatipoobj.tareatipo.tareaobj.CaCantidad,
                                            cacantidadfinalizar = x.tareatipoobj.tareatipo.tareaobj.CaCantidadfinalizar,
                                            idgestionprincipal = _context.CrmArbolGestion.Where(a => a.Id == x.tareatipoobj.tareatipo.tareaobj.IdgestionPrincipal)
                                                                 .Select(a => a.Descripcion).FirstOrDefault(),
                                            idgestionsec1 = _context.CrmArbolGestion.Where(a => a.Id == x.tareatipoobj.tareatipo.tareaobj.IdgestionSec1)
                                                            .Select(a => a.Descripcion).FirstOrDefault(),
                                            idgestionsec2 = _context.CrmArbolGestion.Where(a => a.Id == x.tareatipoobj.tareatipo.tareaobj.IdgestionSec2)
                                                            .Select(a => a.Descripcion).FirstOrDefault(),
                                            tccantidad = x.tareatipoobj.tareatipo.tareaobj.TcCantidad,
                                            tccantidadfinalizar = x.tareatipoobj.tareatipo.tareaobj.TcCantidadfinalizar,
                                            tcvalor = x.tareatipoobj.tareatipo.tareaobj.TcValor,
                                            tcvalorfinalizar = x.tareatipoobj.tareatipo.tareaobj.TcValorfinalizar,
                                            pcantidad = x.tareatipoobj.tareatipo.tareaobj.PCantidad,
                                            pcantidadfinalizar = x.tareatipoobj.tareatipo.tareaobj.PCantidadfinalizar
                                        }).OrderByDescending(x => x.idobjetivo).ToListAsync()
                        };
                    }
                    else
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 0,
                            Mensage = "No hay Objetivos con la Tarea "+ idTarea
                        };
                    }
                }
                else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No existe la tarea "+ idTarea
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/CrmTareaObjetivos/ModificarTareaObjetivo
        [HttpPost("ModificarTareaObjetivo")]
        public async Task<ModeloRespuesta> ModificarTareaObjetivo(CrmTareaObjetivos crmTareaObjetivos)
        {
            try
            {
                if (!CrmTareaObjetivosExists(crmTareaObjetivos.IdObjetivo))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Objetivo que desea modificar no existe"
                    };
                }
                else
                {
                    var CrmTareaObjModificar = _context.CrmTareaObjetivos.FirstOrDefault(x => x.IdObjetivo == crmTareaObjetivos.IdObjetivo);

                    if (crmTareaObjetivos.IdParametro!= null)
                    CrmTareaObjModificar.IdParametro = crmTareaObjetivos.IdParametro;
                    if (crmTareaObjetivos.CaValorparametro!= null)
                    CrmTareaObjModificar.CaValorparametro = crmTareaObjetivos.CaValorparametro;
                    if (crmTareaObjetivos.ValorparametroHabilitar!= null)
                    CrmTareaObjModificar.ValorparametroHabilitar = crmTareaObjetivos.ValorparametroHabilitar;
                    if (crmTareaObjetivos.ValorparametroFinalizar!= null)
                    CrmTareaObjModificar.ValorparametroFinalizar = crmTareaObjetivos.ValorparametroFinalizar;
                    if (crmTareaObjetivos.CaValor!= null)
                    CrmTareaObjModificar.CaValor = crmTareaObjetivos.CaValor;
                    if (crmTareaObjetivos.CaValorhabilitar!= null)
                    CrmTareaObjModificar.CaValorhabilitar = crmTareaObjetivos.CaValorhabilitar;
                    if (crmTareaObjetivos.CaValorfinalizar!= null)
                    CrmTareaObjModificar.CaValorfinalizar = crmTareaObjetivos.CaValorfinalizar;
                    if (crmTareaObjetivos.CaCantidad!= null)
                    CrmTareaObjModificar.CaCantidad = crmTareaObjetivos.CaCantidad;
                    if (crmTareaObjetivos.CaCantidadhabilitar!= null)
                    CrmTareaObjModificar.CaCantidadhabilitar = crmTareaObjetivos.CaCantidadhabilitar;
                    if (crmTareaObjetivos.CaCantidadfinalizar!= null)
                    CrmTareaObjModificar.CaCantidadfinalizar = crmTareaObjetivos.CaCantidadfinalizar;
                    if (crmTareaObjetivos.IdgestionPrincipal!= null)
                    CrmTareaObjModificar.IdgestionPrincipal = crmTareaObjetivos.IdgestionPrincipal;
                    if (crmTareaObjetivos.IdgestionSec1!= null)
                    CrmTareaObjModificar.IdgestionSec1 = crmTareaObjetivos.IdgestionSec1;
                    if (crmTareaObjetivos.IdgestionSec2!= null)
                    CrmTareaObjModificar.IdgestionSec2 = crmTareaObjetivos.IdgestionSec2;
                    if (crmTareaObjetivos.TcCantidad!= null)
                    CrmTareaObjModificar.TcCantidad = crmTareaObjetivos.TcCantidad;
                    if (crmTareaObjetivos.TcCantidadhabilitar!= null)
                    CrmTareaObjModificar.TcCantidadhabilitar = crmTareaObjetivos.TcCantidadhabilitar;
                    if (crmTareaObjetivos.TcCantidadfinalizar!= null)
                    CrmTareaObjModificar.TcCantidadfinalizar = crmTareaObjetivos.TcCantidadfinalizar;
                    if (crmTareaObjetivos.TcValor!= null)
                    CrmTareaObjModificar.TcValor = crmTareaObjetivos.TcValor;
                    if (crmTareaObjetivos.TcValorhabilitar!= null)
                    CrmTareaObjModificar.TcValorhabilitar = crmTareaObjetivos.TcValorhabilitar;
                    if (crmTareaObjetivos.TcValorfinalizar!= null)
                    CrmTareaObjModificar.TcValorfinalizar = crmTareaObjetivos.TcValorfinalizar;
                    if (crmTareaObjetivos.PCantidad!= null)
                    CrmTareaObjModificar.PCantidad = crmTareaObjetivos.PCantidad;
                    if (crmTareaObjetivos.PCantidadhabilitar!= null)
                    CrmTareaObjModificar.PCantidadhabilitar = crmTareaObjetivos.PCantidadhabilitar;
                    if (crmTareaObjetivos.PCantidadfinalizar!= null)
                    CrmTareaObjModificar.PCantidadfinalizar = crmTareaObjetivos.PCantidadfinalizar;
                    
                    CrmTareaObjModificar.FechaActualizacion = DateTime.Now;

                    _decoder = new Decoder();
                    uint idusuario = _decoder.GetDecoder(HttpContext);
                    CrmTareaObjModificar.UserActualizacion = idusuario;

                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Objetivo Modificado"
            };
        }


        // api/CrmTareaObjetivos/CrearObjetivo
        [HttpPost("CrearObjetivo")]
        public async Task<ModeloRespuesta> CrearObjetivo(CrmTareaObjetivos crmTareaObjetivos)
        {
            try{
                _context.CrmTareaObjetivos.Add(crmTareaObjetivos);
                crmTareaObjetivos.FechaCreacion = DateTime.Now;
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                crmTareaObjetivos.UserCreacion = idusuario;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Objetivo Creado",
                Data = await _context.CrmTareaObjetivos.FindAsync(crmTareaObjetivos.IdObjetivo)
            };
        }

        private bool CrmTareaObjetivosExists(uint id)
        {
            return _context.CrmTareaObjetivos.Any(e => e.IdObjetivo == id);
        }

        //Inactivar Objetivo.
        // api/CrmTareaObjetivos/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmTareas(CrmTareaObjetivos crmTareaObjetivos)
        {
            try
            {
                if (!CrmTareaObjetivosExists(crmTareaObjetivos.IdObjetivo))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Objetivo que desea inactivar no existe"
                    };
                }

                var CrmTareaObjModificar = _context.CrmTareaObjetivos.FirstOrDefault(x => x.IdObjetivo == crmTareaObjetivos.IdObjetivo);
                
                CrmTareaObjModificar.Estado = "I";
                CrmTareaObjModificar.FechaActualizacion = DateTime.Now;

                //Obtener Usuario
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                CrmTareaObjModificar.UserActualizacion = idusuario;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Objetivo Inactivo"
            };
        }



        // LLena ComboBox
        // api/CrmTareaObjetivos/GetTipoObjetivos
        [HttpGet("GetTipoObjetivos")]
        public async Task<ModeloRespuesta> GetTipoObjetivos()
        {
            try
            {
                if (_context.AclTipoObjetivo.Any(x => x.IdTipoobjetivo != 0))
                {
                    return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await _context.AclTipoObjetivo
                                    .Where(x => x.IdTipoobjetivo != 0)
                                    .OrderBy(x => x.IdTipoobjetivo).ToListAsync()
                        };
                }
                else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No existen tipos de objetivos"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        // api/CrmTareaObjetivos/GetParamObj
        [HttpGet("GetParamObj")]
        public async Task<ModeloRespuesta> GetParamObj()
        {
            try
            {
                if (_context.AclParametroObjetivo.Any(x => x.IdParametro != 0))
                {
                    return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await _context.AclParametroObjetivo
                                    .Where(x => x.IdParametro != 0)
                                    .OrderBy(x => x.IdParametro).ToListAsync()
                        };
                }
                else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No existen parámetros"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
