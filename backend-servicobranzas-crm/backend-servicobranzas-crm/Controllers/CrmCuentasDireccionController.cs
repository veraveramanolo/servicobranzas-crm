using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasDireccionController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasDireccionController(ps_crmContext context)
        {
            _context = context;
        }

        

        private bool CrmCuentasDireccionExists(uint id)
        {
            return _context.CrmCuentasDireccion.Any(e => e.Id == id);
        }

        //Consulta de direcciones por cuenta.
        // api/CrmCuentasDireccion/GetDireccion

        [HttpGet("GetDireccion")]
        public async Task<ModeloRespuesta> GetDireccion(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x=>x.Id == idCuentaCampania))
                {
                    var direccionCliente = new object();
                    var cuenta = _context.CrmCuentasCampania
                        .FirstOrDefault(x => x.Id == idCuentaCampania).IdCuenta;
                    if (cuenta != null)
                    {
                        direccionCliente = await _context.CrmCuentasDireccion
                        .Where(x => x.IdCuenta == cuenta)
                        .OrderBy(x => x.Estado)
                        .ThenByDescending(x => x.Id)
                        .Select(x => new
                        {
                            idDireccion = x.Id,
                            direccion = x.Valor,
                            tipo = x.Tipo,
                            contacto = x.IdCuentaNavigation.CrmCuentasCampania
                                .Where(s=> s.Id == idCuentaCampania && s.IdCuenta == x.IdCuenta)
                                .Select(s => new {s.IdArbolGestionMejorGestionNavigation.IdTipoContactoNavigation.Id, s.IdArbolGestionMejorGestionNavigation.IdTipoContactoNavigation.Texto}).FirstOrDefault(),
                            departamento = new { x.IdDeptoNavigation.DeptoValor, x.IdDeptoNavigation.IdDepto },
                            ciudad = new { x.IdCiudadNavigation.CiudadValor, x.IdCiudadNavigation.IdCiudad },
                            distrito = new { x.IdDistritoNavigation.DistritoValor, x.IdDistritoNavigation.IdDistrito },
                            zona = new { x.IdZonaNavigation.ZonaValor, x.IdZonaNavigation.IdZona },
                            area = new { x.IdAreaNavigation.ValorArea, x.IdAreaNavigation.IdArea },
                            barrio = x.Barrio,
                            info1 = x.Info1,
                            estado = x.Estado,
                            observacionInactivo = x.ObservacionI
                        })
                        .ToListAsync();
                    }
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = direccionCliente
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //Guarda direccion por cuenta.
        // api/CrmCuentasDireccion/
        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmCuentasDireccion(CrmCuentasDireccion crmCuentasDireccion)
        {
            try
            {
                crmCuentasDireccion.FechaCreacion = DateTime.Now;
                _context.CrmCuentasDireccion.Add(crmCuentasDireccion);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Dirección Creada",
                Data = await _context.CrmCuentasDireccion.FindAsync(crmCuentasDireccion.Id)
            };

        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmCuentasDireccion(CrmCuentasDireccion crmCuentasDireccion)
        {
            try
            {
                if (!CrmCuentasDireccionExists(crmCuentasDireccion.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Dirección que desea modificar no existe"
                    };
                }

                var crmCuentasDireccionModificar = _context.CrmCuentasDireccion.FirstOrDefault(x => x.Id == crmCuentasDireccion.Id);
                if(!String.IsNullOrEmpty(crmCuentasDireccion.Tipo))
                    crmCuentasDireccionModificar.Tipo = crmCuentasDireccion.Tipo;
                if (!String.IsNullOrEmpty(crmCuentasDireccion.Valor))
                    crmCuentasDireccionModificar.Valor = crmCuentasDireccion.Valor;
                if (crmCuentasDireccion.IdDepto!= null)
                    crmCuentasDireccionModificar.IdDepto = crmCuentasDireccion.IdDepto;
                if (crmCuentasDireccion.IdCiudad!= null)
                    crmCuentasDireccionModificar.IdCiudad = crmCuentasDireccion.IdCiudad;
                if (crmCuentasDireccion.IdDistrito!= null)
                    crmCuentasDireccionModificar.IdDistrito = crmCuentasDireccion.IdDistrito;
                if (crmCuentasDireccion.IdZona!= null)
                    crmCuentasDireccionModificar.IdZona = crmCuentasDireccion.IdZona;
                if (crmCuentasDireccion.IdArea!= null)
                    crmCuentasDireccionModificar.IdArea = crmCuentasDireccion.IdArea;
                if (!String.IsNullOrEmpty(crmCuentasDireccion.Barrio))
                    crmCuentasDireccionModificar.Barrio = crmCuentasDireccion.Barrio;
                if (!String.IsNullOrEmpty(crmCuentasDireccion.Info1))
                    crmCuentasDireccionModificar.Info1 = crmCuentasDireccion.Info1;
                if (!String.IsNullOrEmpty(crmCuentasDireccion.Estado))
                    crmCuentasDireccionModificar.Estado = crmCuentasDireccion.Estado;
                crmCuentasDireccionModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Dirección Modificada"
            };
        }

        //Inactivar direcciones por cuenta.
        // api/CrmCuentasDireccion/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmCuentasDireccion(CrmCuentasDireccion crmCuentasDireccion)
        {
            try
            {
                if (!CrmCuentasDireccionExists(crmCuentasDireccion.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Dirección que desea inactivar no existe"
                    };
                }

                var crmCuentasDireccionModificar = _context.CrmCuentasDireccion.FirstOrDefault(x => x.Id == crmCuentasDireccion.Id);
                
                if (!String.IsNullOrEmpty(crmCuentasDireccion.Estado))
                    crmCuentasDireccionModificar.Estado = crmCuentasDireccion.Estado;
                if (!String.IsNullOrEmpty(crmCuentasDireccion.ObservacionI))
                    crmCuentasDireccionModificar.ObservacionI = crmCuentasDireccion.ObservacionI;
                crmCuentasDireccionModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Dirección Inactiva"
            };
        }
    }
}
