using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclAreaController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclAreaController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclArea
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclArea>>> GetAclArea()
        {
            return await _context.AclArea.ToListAsync();
        }

        // GET: api/AclArea/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclArea>> GetAclArea(int id)
        {
            var aclArea = await _context.AclArea.FindAsync(id);

            if (aclArea == null)
            {
                return NotFound();
            }

            return aclArea;
        }

        // PUT: api/AclArea/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclArea(uint id, AclArea aclArea)
        {
            if (id != aclArea.IdArea)
            {
                return BadRequest();
            }

            _context.Entry(aclArea).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclAreaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclArea
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclArea>> PostAclArea(AclArea aclArea)
        {
            _context.AclArea.Add(aclArea);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclAreaExists(aclArea.IdArea))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclArea", new { id = aclArea.IdArea }, aclArea);
        }

        // DELETE: api/AclArea/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclArea>> DeleteAclArea(int id)
        {
            var aclArea = await _context.AclArea.FindAsync(id);
            if (aclArea == null)
            {
                return NotFound();
            }

            _context.AclArea.Remove(aclArea);
            await _context.SaveChangesAsync();

            return aclArea;
        }

        private bool AclAreaExists(uint id)
        {
            return _context.AclArea.Any(e => e.IdArea == id);
        }

        //Consulta todos los departamentos
        //api/AclArea/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclArea()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclArea.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
