using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasTelefonoController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmCuentasTelefonoController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentasTelefono
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasTelefono>>> GetCrmCuentasTelefono()
        {
            return await _context.CrmCuentasTelefono.ToListAsync();
        }


        [HttpGet("GetTelefono")]
        public async Task<ModeloRespuesta> GetTelefono( uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var relacionContacto = new CrmCuentasContacto();
                    var telefonoCliente = new object();
                    var cuenta = _context.CrmCuentasCampania
                        .FirstOrDefault(x => x.Id == idCuentaCampania).IdCuenta;
                    if (cuenta != null)
                    {
                        telefonoCliente = await _context.CrmCuentasTelefono
                            .Where(x => x.IdCuenta == cuenta)
                            .OrderByDescending(x => x.Id)
                            .ThenBy(x => x.Estado)
                            .Select(x => new
                            {
                                idtelefono = x.Id,
                                numero = x.Valor,
                                tipo = x.Tipo,
                                contacto = x.IdCuentaNavigation.CrmCuentasCampania
                                    .Where(s => s.Id == idCuentaCampania && s.IdCuenta == x.IdCuenta)
                                    .Select(s => new {s.IdArbolGestionMejorGestionNavigation.IdTipoContactoNavigation.Id, s.IdArbolGestionMejorGestionNavigation.IdTipoContactoNavigation.Texto}).FirstOrDefault(),
                                departamento = new { x.IdDeptoNavigation.DeptoValor, x.IdDeptoNavigation.IdDepto, x.IdDeptoNavigation.IdRegionNavigation.RegionValor },
                                ciudad = new { x.IdCiudadNavigation.CiudadValor, x.IdCiudadNavigation.IdCiudad },
                                districto = new { x.IdDistritoNavigation.DistritoValor, x.IdDistritoNavigation.IdDistrito },
                                info2 = x.Info2,
                                estado = x.Estado,
                                conteo = x.Conteo,
                                relacion = _context.CrmCuentasContacto
                                    .Where(s => s.IdCuenta == x.IdCuenta && s.Id == x.IdContacto)
                                    .Select(s => new { s.IdRelacionNavigation.ValorRelacion}).FirstOrDefault()

                            }).ToListAsync();
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = telefonoCliente
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

       
        // POST: api/CrmCuentasTelefono
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasTelefono>> PostCrmCuentasTelefono(CrmCuentasTelefono crmCuentasTelefono)
        {
            _context.CrmCuentasTelefono.Add(crmCuentasTelefono);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasTelefono", new { id = crmCuentasTelefono.Id }, crmCuentasTelefono);
        }

       

        private bool CrmCuentasTelefonoExists(uint id)
        {
            return _context.CrmCuentasTelefono.Any(e => e.Id == id);
        }


        //Guarda telefonos por cuenta.
        // api/CrmCuentasTelefono/Crear
        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmCuentasTelefono(CrmCuentasTelefono crmCuentasTelefono)
        {
            try
            {
                crmCuentasTelefono.FechaCreacion = DateTime.Now;
                _context.CrmCuentasTelefono.Add(crmCuentasTelefono);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Teléfono Creado",
                Data = await _context.CrmCuentasTelefono.FindAsync(crmCuentasTelefono.Id)
            };

        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmCuentasTelefono(CrmCuentasTelefono crmCuentasTelefono)
        {
            try
            {
                if (!CrmCuentasTelefonoExists(crmCuentasTelefono.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Teléfono que desea modificar no existe"
                    };
                }

                var crmCuentasTelefonoModificar = _context.CrmCuentasTelefono.FirstOrDefault(x => x.Id == crmCuentasTelefono.Id);
                if(!String.IsNullOrEmpty(crmCuentasTelefono.Tipo))
                    crmCuentasTelefonoModificar.Tipo = crmCuentasTelefono.Tipo;
                if (!String.IsNullOrEmpty(crmCuentasTelefono.Valor))
                    crmCuentasTelefonoModificar.Valor = crmCuentasTelefono.Valor;
                if (crmCuentasTelefono.IdDepto!= null)
                    crmCuentasTelefonoModificar.IdDepto = crmCuentasTelefono.IdDepto;
                if (crmCuentasTelefono.IdCiudad!= null)
                    crmCuentasTelefonoModificar.IdCiudad = crmCuentasTelefono.IdCiudad;
                if (crmCuentasTelefono.IdDistrito!= null)
                    crmCuentasTelefonoModificar.IdDistrito = crmCuentasTelefono.IdDistrito;
                if (!String.IsNullOrEmpty(crmCuentasTelefono.Info2))
                    crmCuentasTelefonoModificar.Info2 = crmCuentasTelefono.Info2;
                if (!String.IsNullOrEmpty(crmCuentasTelefono.Estado))
                    crmCuentasTelefonoModificar.Estado = crmCuentasTelefono.Estado;
                if (crmCuentasTelefono.Conteo!= null)
                    crmCuentasTelefonoModificar.Conteo = crmCuentasTelefono.Conteo;
                crmCuentasTelefonoModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Teléfono Modificado"
            };
        }

        //Inactivar telefono por cuenta.
        // api/CrmCuentasTelefono/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmCuentasTelefono(CrmCuentasTelefono crmCuentasTelefono)
        {
            try
            {
                if (!CrmCuentasTelefonoExists(crmCuentasTelefono.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Teléfono que desea inactivar no existe"
                    };
                }

                var CrmCuentasTelefonoModificar = _context.CrmCuentasTelefono.FirstOrDefault(x => x.Id == crmCuentasTelefono.Id);
                
                if (!String.IsNullOrEmpty(crmCuentasTelefono.Estado))
                    CrmCuentasTelefonoModificar.Estado = crmCuentasTelefono.Estado;
                if (!String.IsNullOrEmpty(crmCuentasTelefono.ObservacionI))
                    CrmCuentasTelefonoModificar.ObservacionI = crmCuentasTelefono.ObservacionI;
                CrmCuentasTelefonoModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Teléfono Inactivo"
            };
        }

        // api/CrmCuentasTelefono/BusquedaTelefono
        [HttpGet("BusquedaTelefono")]
        public async Task<ModeloRespuesta> BusquedaTelefono(string filtfono)
        {
            try
            {
                if (_context.CrmCuentasTelefono.Any(x => x.Valor.Contains(filtfono)))
                {
                    _decoder = new Decoder();
                    uint idusuario = _decoder.GetDecoder(HttpContext);
                    Console.WriteLine("idusuario ");
                    Console.WriteLine(idusuario);
                    var admin = _context.AclUsuario.Where(a => a.IdUsuario == idusuario).Select(a => a.IdRolNavigation.IdRol).FirstOrDefault();
                    Console.WriteLine("admin ");
                    Console.WriteLine(admin);
                    if (admin.Equals(1)  || admin.Equals(3))
                    {
                        var busqueda = from telefono in _context.CrmCuentasTelefono 
                                        join c in _context.CrmCuentas on telefono.IdCuenta equals c.Id 
                                        join cc in _context.CrmCuentasCampania on c.Id equals cc.IdCuenta 
                                        join cg in _context.CrmCuentasCampaniaGestor on cc.Id equals cg.IdCuentaCampania into ccg
                                        from left in ccg.DefaultIfEmpty() 
                                        join p in _context.CrmCuentasCampaniaProductos on cc.Id equals p.IdCuentaCampania 
                                        where telefono.Valor.Contains(filtfono) 
                                        select new {
                                            idcuentacampania = cc.Id,
                                            idcuenta = c.Id,
                                            documento = c.Identificacion,
                                            nombre = c.Nombre,
                                            asesor = (left.Estado == "A" ? left.IdUsuarioNavigation.UserName : ""),
                                            campania = cc.IdCampaniaNavigation.Nombre,
                                            cliente = cc.IdCampaniaNavigation.IdCarteraNavigation.Nombre,
                                            idproducto = p.Id,
                                            nomproducto = p.Descripcion
                                        };
                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = busqueda.Distinct().OrderBy(x => x.nombre)

                        };
                    }else
                    {
                        var busqueda = await _context.CrmCuentasTelefono
                            .Where(x => x.Valor.Contains(filtfono))
                            .Join(_context.CrmCuentas, telefono => telefono.IdCuenta, cuenta => cuenta.Id,
                            (telefono, cuenta) => new {telefono, cuenta})
                            .Join(_context.CrmCuentasCampania, infocliente => infocliente.cuenta.Id, cuentacampania => cuentacampania.IdCuenta,
                            (infocliente, cuentacampania) => new {infocliente, cuentacampania})
                            .Join(_context.CrmCartera, infocuenta => infocuenta.cuentacampania.IdCampaniaNavigation.IdCartera, cartera => cartera.Id,
                            (infocuenta, cartera) => new {infocuenta, cartera})
                            .Join(_context.CrmCuentasCampaniaGestor, infocartera => infocartera.infocuenta.cuentacampania.Id, gestor => gestor.IdCuentaCampania,
                            (infocartera, gestor) => new {infocartera, gestor})
                            .Join(_context.CrmCuentasCampaniaProductos, infogeneral => infogeneral.gestor.IdCuentaCampania, producto => producto.IdCuentaCampania,
                            (infogeneral, producto) => new {infogeneral, producto})
                            .Select(x => new
                            {
                                idcuentacampania = x.infogeneral.infocartera.infocuenta.cuentacampania.Id,
                                idcuenta = x.infogeneral.infocartera.infocuenta.infocliente.cuenta.Id,
                                documento = x.infogeneral.infocartera.infocuenta.infocliente.cuenta.Identificacion,
                                nombre = x.infogeneral.infocartera.infocuenta.infocliente.cuenta.Nombre,
                                asesor = x.infogeneral.gestor.IdUsuarioNavigation.UserName,
                                campania = x.infogeneral.infocartera.infocuenta.cuentacampania.IdCampaniaNavigation.Nombre,
                                cliente = x.infogeneral.infocartera.cartera.Nombre,
                                idproducto = x.producto.Id,
                                nomproducto = x.producto.Descripcion

                            }).Distinct().OrderBy(x => x.nombre).ToListAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = busqueda

                        };
                    }
                    
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
