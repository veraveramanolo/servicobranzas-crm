using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclDeptoController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclDeptoController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclDepto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclDepto>>> GetAclDepto()
        {
            return await _context.AclDepto.ToListAsync();
        }

        // GET: api/AclDepto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclDepto>> GetAclDepto(int id)
        {
            var aclDepto = await _context.AclDepto.FindAsync(id);

            if (aclDepto == null)
            {
                return NotFound();
            }

            return aclDepto;
        }

        // PUT: api/AclDepto/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclDepto(uint id, AclDepto aclDepto)
        {
            if (id != aclDepto.IdDepto)
            {
                return BadRequest();
            }

            _context.Entry(aclDepto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclDeptoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclDepto
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclDepto>> PostAclDepto(AclDepto aclDepto)
        {
            _context.AclDepto.Add(aclDepto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclDeptoExists(aclDepto.IdDepto))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclDepto", new { id = aclDepto.IdDepto }, aclDepto);
        }

        // DELETE: api/AclDepto/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclDepto>> DeleteAclDepto(int id)
        {
            var aclDepto = await _context.AclDepto.FindAsync(id);
            if (aclDepto == null)
            {
                return NotFound();
            }

            _context.AclDepto.Remove(aclDepto);
            await _context.SaveChangesAsync();

            return aclDepto;
        }

        private bool AclDeptoExists(uint id)
        {
            return _context.AclDepto.Any(e => e.IdDepto == id);
        }

        //Consulta todos los departamentos
        //api/AclDepto/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclDepto()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclDepto.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
