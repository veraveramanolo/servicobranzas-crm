using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using System.IdentityModel.Tokens.Jwt;
using backend_servicobranzas_crm.Services.Class;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmMulticanalMarcadoresController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmMulticanalMarcadoresController(ps_crmContext context)
        {
            _context = context;
        }

        

        private bool CrmMulticanalMarcadoresExists(uint id)
        {
            return _context.CrmMulticanalMarcadores.Any(e => e.IdMarcador == id);
        }

        //Consulta de marcadores por campania.
        // api/CrmMulticanalMarcadores/GetMarcadores

        [HttpGet("GetMarcadores")]
        public async Task<ModeloRespuesta> GetMarcadores(uint idCampania)
        {
            try
            {
                if (_context.CrmCampania.Any(x=>x.Id == idCampania))
                {
                    var marcadoresMulticanal = new object();
                    var campania = _context.CrmCampania
                        .FirstOrDefault(x => x.Id == idCampania).Nombre;
                    if (campania != null)
                    {
                        marcadoresMulticanal = await _context.CrmMulticanalMarcadores
                        .Where(x => x.IdCampania == idCampania)
                        .OrderByDescending(x => x.FechaCreacion)
                        .ToListAsync();
                    }
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = marcadoresMulticanal
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //Guarda marcadores por campania.
        // api/CrmMulticanalMarcadores/CrearMarcador
        [HttpPost("CrearMarcador")]
        public async Task<ModeloRespuesta> CrearCrmMulticanalMarcadores(CrmMulticanalMarcadores crmMulticanalMarcadores)
        {
            try
            {
                crmMulticanalMarcadores.FechaCreacion = DateTime.Now;
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                crmMulticanalMarcadores.UserCreacion = idusuario.ToString();
                _context.CrmMulticanalMarcadores.Add(crmMulticanalMarcadores);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Marcador Creado",
                Data = await _context.CrmMulticanalMarcadores.FindAsync(crmMulticanalMarcadores.IdMarcador)
            };

        }

    }
}
