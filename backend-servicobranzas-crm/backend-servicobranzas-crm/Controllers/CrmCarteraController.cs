using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCarteraController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCarteraController(ps_crmContext context)
        {
            _context = context;
        }


        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllCrmCartera()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.CrmCartera.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpGet("{id}")]
        public async Task<ModeloRespuesta> GetCrmCartera(uint id)
        {
            try
            {
                var crmCartera = await _context.CrmCartera.FindAsync(id);
                if (crmCartera == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Cartera no encontrado"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = crmCartera
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmCartera(CrmCartera crmCartera)
        {
            try
            {
                if (!CrmCarteraExists(crmCartera.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "La cartera que desea modificar no existe"
                    };
                }

                var crmCarteraModificada = _context.CrmCartera.FirstOrDefault(x => x.Id == crmCartera.Id);

                if (!string.IsNullOrEmpty(crmCartera.Nombre))
                    crmCarteraModificada.Nombre = crmCartera.Nombre;
                if (!string.IsNullOrEmpty(crmCartera.Estado))
                    crmCarteraModificada.Estado = crmCartera.Estado;
                if (!string.IsNullOrEmpty(crmCartera.Tipo))
                    crmCarteraModificada.Tipo = crmCartera.Tipo;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Cartera Modificada"
            };
        }

        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmCartera(CrmCartera crmCartera)
        {
            try
            {
                _context.CrmCartera.Add(crmCartera);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Cartera Creada",
                Data = await _context.CrmCartera.FindAsync(crmCartera.Id)
            };
        }

        [HttpGet("CarteraCampania")]
        public async Task<ModeloRespuesta> GetCarteraCampania(uint idCatera)
        {
            try
            {
                if (_context.CrmCartera.Any(x=> x.Id == idCatera))
                {
                    var carteraCampania = await _context.CrmCampania
                        .Where(x => x.IdCartera == idCatera)
                        .Select(x => new
                        {
                            x.Estado,
                            x.Id,
                            x.Nombre,
                            x.IdCartera
                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = carteraCampania,
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        [HttpGet("GetCarteraGestor")]
        public async Task<ModeloRespuesta> GetCarteraGestor(uint idUsuario)
        {
            try
            {
                if (_context.CrmCuentasCampaniaGestor.Any(x=> x.IdUsuario == idUsuario))
                {
                    var carteraGestor = await _context.CrmCuentasCampaniaGestor
                        .Where(x => x.IdUsuario == idUsuario)
                        .Select(x => new
                        {
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Id,
                            // x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Nombre,
                            Nombre = (x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Tipo.Equals("Vendida") ? 
                                      x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Nombre +' '+
                                      x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Tipo : 
                                      x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Nombre),
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Estado,
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCarteraNavigation.Tipo
                        }).Distinct().OrderBy(x => x.Nombre).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = carteraGestor
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //CrmCartera/GetCarteras
        [HttpGet("GetCarteras")]
        public async Task<ModeloRespuesta> GetCarteras()
        {
            try
            {
                if (_context.CrmCartera.Any(x=> x.Estado == "A"))
                {
                    var carteras = await _context.CrmCartera
                        .Where(x => x.Estado == "A")
                        .Select(x => new
                        {
                            x.Id,
                            Nombre = (x.Tipo.Equals("Vendida") ? x.Nombre+' '+x.Tipo : x.Nombre),
                            x.Estado,
                            x.Tipo
                        }).Distinct().OrderBy(x => x.Nombre).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = carteras
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        private bool CrmCarteraExists(uint id)
        {
            return _context.CrmCartera.Any(e => e.Id == id);
        }
    }
}
