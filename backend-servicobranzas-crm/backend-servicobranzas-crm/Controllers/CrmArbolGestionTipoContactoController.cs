using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmArbolGestionTipoContactoController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmArbolGestionTipoContactoController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmArbolGestionTipoContacto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmArbolGestionTipoContacto>>> GetCrmArbolGestionTipoContacto()
        {
            return await _context.CrmArbolGestionTipoContacto.ToListAsync();
        }

        // GET: api/CrmArbolGestionTipoContacto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmArbolGestionTipoContacto>> GetCrmArbolGestionTipoContacto(uint id)
        {
            var crmArbolGestionTipoContacto = await _context.CrmArbolGestionTipoContacto.FindAsync(id);

            if (crmArbolGestionTipoContacto == null)
            {
                return NotFound();
            }

            return crmArbolGestionTipoContacto;
        }

        // PUT: api/CrmArbolGestionTipoContacto/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmArbolGestionTipoContacto(uint id, CrmArbolGestionTipoContacto crmArbolGestionTipoContacto)
        {
            if (id != crmArbolGestionTipoContacto.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmArbolGestionTipoContacto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmArbolGestionTipoContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmArbolGestionTipoContacto
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmArbolGestionTipoContacto>> PostCrmArbolGestionTipoContacto(CrmArbolGestionTipoContacto crmArbolGestionTipoContacto)
        {
            _context.CrmArbolGestionTipoContacto.Add(crmArbolGestionTipoContacto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmArbolGestionTipoContacto", new { id = crmArbolGestionTipoContacto.Id }, crmArbolGestionTipoContacto);
        }

        // DELETE: api/CrmArbolGestionTipoContacto/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmArbolGestionTipoContacto>> DeleteCrmArbolGestionTipoContacto(uint id)
        {
            var crmArbolGestionTipoContacto = await _context.CrmArbolGestionTipoContacto.FindAsync(id);
            if (crmArbolGestionTipoContacto == null)
            {
                return NotFound();
            }

            _context.CrmArbolGestionTipoContacto.Remove(crmArbolGestionTipoContacto);
            await _context.SaveChangesAsync();

            return crmArbolGestionTipoContacto;
        }

        //[HttpGet("GetFiltroEstado")]
        //public async Task<ModeloRespuesta> GetFiltroEstado(uint idC)

        private bool CrmArbolGestionTipoContactoExists(uint id)
        {
            return _context.CrmArbolGestionTipoContacto.Any(e => e.Id == id);
        }
    }
}
