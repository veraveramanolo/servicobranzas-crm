using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmConsultaGestionCampaniaDataController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmConsultaGestionCampaniaDataController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmConsultaGestionCampaniaData
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmConsultaGestionCampaniaData>>> GetCrmConsultaGestionCampaniaData()
        {
            return await _context.CrmConsultaGestionCampaniaData.ToListAsync();
        }

        // GET: api/CrmConsultaGestionCampaniaData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmConsultaGestionCampaniaData>> GetCrmConsultaGestionCampaniaData(uint id)
        {
            var crmConsultaGestionCampaniaData = await _context.CrmConsultaGestionCampaniaData.FindAsync(id);

            if (crmConsultaGestionCampaniaData == null)
            {
                return NotFound();
            }

            return crmConsultaGestionCampaniaData;
        }

        // PUT: api/CrmConsultaGestionCampaniaData/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmConsultaGestionCampaniaData(uint id, CrmConsultaGestionCampaniaData crmConsultaGestionCampaniaData)
        {
            if (id != crmConsultaGestionCampaniaData.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmConsultaGestionCampaniaData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmConsultaGestionCampaniaDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmConsultaGestionCampaniaData
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmConsultaGestionCampaniaData>> PostCrmConsultaGestionCampaniaData(CrmConsultaGestionCampaniaData crmConsultaGestionCampaniaData)
        {
            _context.CrmConsultaGestionCampaniaData.Add(crmConsultaGestionCampaniaData);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CrmConsultaGestionCampaniaDataExists(crmConsultaGestionCampaniaData.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCrmConsultaGestionCampaniaData", new { id = crmConsultaGestionCampaniaData.Id }, crmConsultaGestionCampaniaData);
        }

        // DELETE: api/CrmConsultaGestionCampaniaData/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmConsultaGestionCampaniaData>> DeleteCrmConsultaGestionCampaniaData(uint id)
        {
            var crmConsultaGestionCampaniaData = await _context.CrmConsultaGestionCampaniaData.FindAsync(id);
            if (crmConsultaGestionCampaniaData == null)
            {
                return NotFound();
            }

            _context.CrmConsultaGestionCampaniaData.Remove(crmConsultaGestionCampaniaData);
            await _context.SaveChangesAsync();

            return crmConsultaGestionCampaniaData;
        }

        private bool CrmConsultaGestionCampaniaDataExists(uint id)
        {
            return _context.CrmConsultaGestionCampaniaData.Any(e => e.Id == id);
        }


        //Guarda fila por fila la lista que retorna /CrmCampaniaCuentas/GetCuentas/?idCampania=17&idUsuario=5.
        // public class crmConsultaGestionCampaniaData
        // {
        //         public string Cedula { get; set; }
        //         public string Nombrecliente { get; set; }
        //         public uint Idcuentcampania { get; set; }
        //         public uint Idproducto { get; set; }
        //         public crmConsultaGestionCampaniaData(string cedula, string nombrecliente, uint idcuentacampania, uint idproducto){
        //             Cedula = cedula;
        //             Nombrecliente = nombrecliente;
        //             Idcuentcampania = idcuentacampania;
        //             Idproducto = idproducto;
        //         }
        // }

        [HttpPost("InsertaBD")]
        public async Task<ModeloRespuesta> InsertaBD(List<CrmConsultaGestionCampania> registros)
        {
            try
            {
                _context.CrmConsultaGestionCampaniaData.RemoveRange(_context.CrmConsultaGestionCampaniaData.Where(x => x.Id != 0 ));
                _context.SaveChanges();

                if (registros.Any())
                {
                    uint cont = 0;
                    foreach (CrmConsultaGestionCampania z in registros)
                    {
                        cont = cont +1;
                        Console.WriteLine("\t {0}",cont);
                        CrmConsultaGestionCampaniaData data = new CrmConsultaGestionCampaniaData();
                        data.Id = cont;
                        data.Cedula = z.Cedula;
                        data.Nombrecliente = z.Nombrecliente;
                        data.Idcuentacampania = z.Idcuentacampania;
                        data.Idproducto = z.Idproducto;
                        _context.CrmConsultaGestionCampaniaData.Add(data);
                        // _context.SaveChanges();
                    }
                    _context.SaveChanges();
                }



                //uint cont = 0;
                // List<Task> crmConsultaGestionCampaniaData = new List<Task> { new Task { Cedula = "", Nombrecliente = "" } };
                



                //foreach(var z in registros) {
                //    cont = cont +1;
                //    Console.WriteLine("\t {0}",z.Cedula);
                //    CrmConsultaGestionCampaniaData data = new CrmConsultaGestionCampaniaData();
                //    data.Id = cont;
                //    data.Cedula = z.Cedula;
                //    data.Nombrecliente = z.Nombrecliente;
                //    data.Idcuentacampania = z.Idcuentcampania;
                //    data.Idproducto = z.Idproducto;
                //    _context.CrmConsultaGestionCampaniaData.Add(data);
                //    _context.SaveChanges();
                    
                //    // data.Cedula = z.cedula; --si
                //    // CrmConsultaGestionCampaniaData.AddObject(data); --si
                //    // _context.CrmConsultaGestionCampaniaData.AddRange(z);
                //    // await _context.SaveChangesAsync(z);
                //}
                
                // _campaniaLista = new CrmCampaniaCuentasController(_context);
                // List<CrmCampaniaCuentasController> consultaCampaniaGestions;
                // consultaCampaniaGestions = await _campaniaLista.GetCuentasCampania().;
                // _context.CrmConsultaGestionCampaniaData.Add(crmConsultaGestionCampaniaData);
                // await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "BD insertada",
                Data = await _context.CrmConsultaGestionCampaniaData.ToListAsync()
            };               
        }

        // CrmConsultaGestionCampaniaData/GetBD
        [HttpGet("GetBD")]
        public async Task<ModeloRespuesta> GetBD(uint? idsgte, uint? idantes)
        {
            try
            {
                if (_context.CrmConsultaGestionCampaniaData.Any(x => x.Id != 0))
                {
                    var sgteregistro = await _context.CrmConsultaGestionCampaniaData
                        .Where(x => x.Id == (idsgte+1) || x.Id == (idantes-1))
                        .Select(x => new
                        {
                            id = x.Id,
                            idcuentacampania = x.Idcuentacampania,
                            idproducto = x.Idproducto
                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = sgteregistro
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }


        // CrmConsultaGestionCampaniaData/ObtenerNavBusq
        [HttpGet("ObtenerNavBusq")]
        public async Task<ModeloRespuesta> ObtenerNavBusq(uint idcuentacampania, uint idproducto)
        {
            try
            {
                if (_context.CrmConsultaGestionCampaniaData.Any(x => x.Id != 0))
                {
                    var nav = await _context.CrmConsultaGestionCampaniaData
                        .Where(x => x.Idcuentacampania == idcuentacampania && x.Idproducto == idproducto)
                        .Select(x => x.Id).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = nav.FirstOrDefault()
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultado no encontrado"
            };
        }

    }
}
