using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
using System.IdentityModel.Tokens.Jwt;
using backend_servicobranzas_crm.Helpers;


namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclPlantillaController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public AclPlantillaController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclPlantilla
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclPlantilla>>> GetAclPlantilla()
        {
            return await _context.AclPlantilla.ToListAsync();
        }

        // GET: api/AclPlantilla/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclPlantilla>> GetAclPlantilla(int id)
        {
            var aclPlantilla = await _context.AclPlantilla.FindAsync(id);

            if (aclPlantilla == null)
            {
                return NotFound();
            }

            return aclPlantilla;
        }

        // PUT: api/AclPlantilla/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclPlantilla(int id, AclPlantilla aclPlantilla)
        {
            if (id != aclPlantilla.IdPlantilla)
            {
                return BadRequest();
            }

            _context.Entry(aclPlantilla).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclPlantillaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclPlantilla
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclPlantilla>> PostAclPlantilla(AclPlantilla aclPlantilla)
        {
            _context.AclPlantilla.Add(aclPlantilla);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclPlantillaExists(aclPlantilla.IdPlantilla))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclPlantilla", new { id = aclPlantilla.IdPlantilla }, aclPlantilla);
        }

        // DELETE: api/AclPlantilla/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclPlantilla>> DeleteAclPlantilla(int id)
        {
            var aclPlantilla = await _context.AclPlantilla.FindAsync(id);
            if (aclPlantilla == null)
            {
                return NotFound();
            }

            _context.AclPlantilla.Remove(aclPlantilla);
            await _context.SaveChangesAsync();

            return aclPlantilla;
        }

        private bool AclPlantillaExists(int id)
        {
            return _context.AclPlantilla.Any(e => e.IdPlantilla == id);
        }

        //Crear plantilla.
        // AclPlantilla/CrearPlantilla
        [HttpPost("CrearPlantilla")]
        public async Task<ModeloRespuesta> AclPlantilla(AclPlantilla aclPlantilla)
        {
            try
            {
                _context.AclPlantilla.Add(aclPlantilla);
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                aclPlantilla.UsrCreacion = Convert.ToInt32(idusuario);
                aclPlantilla.FechaCreacion = DateTime.Now;
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Plantilla Creada",
                Data = await _context.AclPlantilla.FindAsync(aclPlantilla.IdPlantilla)
            };

        }

        //Modificar plantilla.
        // AclPlantilla/ModificarPlantilla
        [HttpPost("ModificarPlantilla")]
        public async Task<ModeloRespuesta> ModificarAclPlantilla(AclPlantilla aclPlantilla)
        {
            try
            {
                if(!AclPlantillaExists(aclPlantilla.IdPlantilla))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Plantilla que desea modificar no existe"
                    };
                }

                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);

                var aclPlantillaModificar = _context.AclPlantilla.FirstOrDefault(x => x.IdPlantilla == aclPlantilla.IdPlantilla);
                if(!String.IsNullOrEmpty(aclPlantilla.BodyPlantilla))
                    aclPlantillaModificar.BodyPlantilla = aclPlantilla.BodyPlantilla;
                if(!String.IsNullOrEmpty(aclPlantilla.EstadoPlantilla))
                    aclPlantillaModificar.EstadoPlantilla = aclPlantilla.EstadoPlantilla;
                if(!String.IsNullOrEmpty(aclPlantilla.TipoPlantilla))
                    aclPlantillaModificar.TipoPlantilla = aclPlantilla.TipoPlantilla;
                aclPlantillaModificar.FechaActualizacion = DateTime.Now;
                aclPlantillaModificar.UsrModificacion = Convert.ToInt32(idusuario);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Plantilla Modificada"
            };
        }

        //obtener plantillas por tipo
        // api/AclPlantilla/PlantillasTipo
        [HttpGet("PlantillasTipo")]
        public async Task<ModeloRespuesta> PlantillasTipo(string tipo)
        {
            try
            {
                    var plantillas = await _context.AclPlantilla.Where(x => x.EstadoPlantilla == "A" && string.Equals(x.TipoPlantilla, tipo, StringComparison.CurrentCultureIgnoreCase))
                                     .OrderByDescending(x => x.FechaCreacion)
                                     .Select(x => new
                                             {
                                                x.IdPlantilla,
                                                x.NombrePlantilla,
                                                x.BodyPlantilla
                                             }).Distinct().ToListAsync();


                    if(plantillas.Any()){
                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = plantillas
                        };
                        
                    }else
                    {
                        return new ModeloRespuesta()
                        {
                            Data = plantillas,
                            Exito = 0,
                            Mensage = "No existen Plantillas Configuradas para " + tipo.ToUpper()
                        };
                    }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}