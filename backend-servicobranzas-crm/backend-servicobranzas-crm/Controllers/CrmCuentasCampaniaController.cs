using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasCampaniaController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentasCampania
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasCampania>>> GetCrmCuentasCampania()
        {
            return await _context.CrmCuentasCampania.ToListAsync();
        }

        // GET: api/CrmCuentasCampania/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasCampania>> GetCrmCuentasCampania(uint id)
        {
            var crmCuentasCampania = await _context.CrmCuentasCampania.FindAsync(id);

            if (crmCuentasCampania == null)
            {
                return NotFound();
            }

            return crmCuentasCampania;
        }

        // PUT: api/CrmCuentasCampania/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentasCampania(uint id, CrmCuentasCampania crmCuentasCampania)
        {
            if (id != crmCuentasCampania.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentasCampania).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasCampaniaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentasCampania
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasCampania>> PostCrmCuentasCampania(CrmCuentasCampania crmCuentasCampania)
        {
            _context.CrmCuentasCampania.Add(crmCuentasCampania);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasCampania", new { id = crmCuentasCampania.Id }, crmCuentasCampania);
        }

        // DELETE: api/CrmCuentasCampania/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentasCampania>> DeleteCrmCuentasCampania(uint id)
        {
            var crmCuentasCampania = await _context.CrmCuentasCampania.FindAsync(id);
            if (crmCuentasCampania == null)
            {
                return NotFound();
            }

            _context.CrmCuentasCampania.Remove(crmCuentasCampania);
            await _context.SaveChangesAsync();

            return crmCuentasCampania;
        }

        private bool CrmCuentasCampaniaExists(uint id)
        {
            return _context.CrmCuentasCampania.Any(e => e.Id == id);
        }
    }
}
