using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaProductosDetalleController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasCampaniaProductosDetalleController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentasCampaniaProductosDetalle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasCampaniaProductosDetalle>>> GetCrmCuentasCampaniaProductosDetalle()
        {
            return await _context.CrmCuentasCampaniaProductosDetalle.ToListAsync();
        }

        // GET: api/CrmCuentasCampaniaProductosDetalle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaProductosDetalle>> GetCrmCuentasCampaniaProductosDetalle(uint id)
        {
            var crmCuentasCampaniaProductosDetalle = await _context.CrmCuentasCampaniaProductosDetalle.FindAsync(id);

            if (crmCuentasCampaniaProductosDetalle == null)
            {
                return NotFound();
            }

            return crmCuentasCampaniaProductosDetalle;
        }

        // PUT: api/CrmCuentasCampaniaProductosDetalle/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentasCampaniaProductosDetalle(uint id, CrmCuentasCampaniaProductosDetalle crmCuentasCampaniaProductosDetalle)
        {
            if (id != crmCuentasCampaniaProductosDetalle.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentasCampaniaProductosDetalle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasCampaniaProductosDetalleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentasCampaniaProductosDetalle
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasCampaniaProductosDetalle>> PostCrmCuentasCampaniaProductosDetalle(CrmCuentasCampaniaProductosDetalle crmCuentasCampaniaProductosDetalle)
        {
            _context.CrmCuentasCampaniaProductosDetalle.Add(crmCuentasCampaniaProductosDetalle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasCampaniaProductosDetalle", new { id = crmCuentasCampaniaProductosDetalle.Id }, crmCuentasCampaniaProductosDetalle);
        }

        // DELETE: api/CrmCuentasCampaniaProductosDetalle/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaProductosDetalle>> DeleteCrmCuentasCampaniaProductosDetalle(uint id)
        {
            var crmCuentasCampaniaProductosDetalle = await _context.CrmCuentasCampaniaProductosDetalle.FindAsync(id);
            if (crmCuentasCampaniaProductosDetalle == null)
            {
                return NotFound();
            }

            _context.CrmCuentasCampaniaProductosDetalle.Remove(crmCuentasCampaniaProductosDetalle);
            await _context.SaveChangesAsync();

            return crmCuentasCampaniaProductosDetalle;
        }

        private bool CrmCuentasCampaniaProductosDetalleExists(uint id)
        {
            return _context.CrmCuentasCampaniaProductosDetalle.Any(e => e.Id == id);
        }
    }
}
