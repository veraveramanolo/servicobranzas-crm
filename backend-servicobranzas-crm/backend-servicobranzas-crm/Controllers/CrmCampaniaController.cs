using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using System.IdentityModel.Tokens.Jwt;
using backend_servicobranzas_crm.Services.Class;


namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCampaniaController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmCampaniaController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("All")]
        public async Task<ModeloRespuesta> GetCrmCampania()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.CrmCampania.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpGet("Campania")]
        public async Task<ModeloRespuesta> GetCrmCampania(uint id)
        {
            try
            {
                var crmCampania = await _context.CrmCampania.FindAsync(id);

                if (crmCampania == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Campa�a no encontrada"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = crmCampania
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado ",
                    Data = new string(e.Message)
                };
            }
        }

        
        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> PutCrmCampania(CrmCampania crmCampania)
        {
            try
            {
                if (!CrmCampaniaExists(crmCampania.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Campa�a que desea modificar no existe"
                    };
                }

                var crmCampaniaModificar = _context.CrmCampania.FirstOrDefault(x => x.Id == crmCampania.Id);
                if (!String.IsNullOrEmpty(crmCampania.Nombre))
                    crmCampaniaModificar.Nombre = crmCampania.Nombre;
                if (crmCampania.IdCartera != null)
                    crmCampaniaModificar.IdCartera = crmCampania.IdCartera;
                if (!string.IsNullOrEmpty(crmCampania.Estado))
                    crmCampaniaModificar.Estado = crmCampania.Estado;

                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
           
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Campa�a Modificada"
            };
        }


        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> PostCrmCampania(CrmCampania crmCampania)
        {
            try
            {
                _context.CrmCampania.Add(crmCampania);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Campa�a creada",
                Data = await _context.CrmCampania.FindAsync(crmCampania.Id)
            };
        }


        [HttpGet("CambiarEstado")]
        public async Task<ModeloRespuesta> DeleteCrmCampania(uint id, string estado, string UsrModificacion)
        {
            try
            {
                if (!CrmCampaniaExists(id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Campa�a que desea modificar no existe"
                    };
                }

                var crmCampaniaModificar = _context.CrmCampania.FirstOrDefault(x => x.Id == id);
                crmCampaniaModificar.Estado = estado;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
           
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Campa�a Modificada"
            };
        }


        [HttpGet("CampaniaGestor")]
        public async Task<ModeloRespuesta> ConsultaCampaniaGestor(uint idGestor)
        {
            try
            {
                if (_context.CrmCuentasCampaniaGestor.Any(x => x.IdUsuario == idGestor))
                {
                    var campaniaGestor = await _context.CrmCuentasCampaniaGestor
                        .Where(x => x.IdUsuario == idGestor)
                        .Select(x => new
                        {
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.Id,
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.Nombre,
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.Estado,
                            x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCartera
                        }).Distinct().ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = campaniaGestor
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };

        }

        private bool CrmCampaniaExists(uint id)
        {
            return _context.CrmCampania.Any(e => e.Id == id);
        }

        // CrmCampania/CampaniaCiclos
        [HttpGet("CampaniaCiclos")]
        public async Task<ModeloRespuesta> ConsultaCampania(string estado)
        {
            try
            {
                if (_context.CrmCampania.Any())
                {
                    if(estado == null)
                    {
                        var campaniasEstado = await _context.CrmCampania
                            .Where(x => x.Estado.Equals("A") || x.Estado.Equals("I"))
                            .OrderByDescending(x => x.FechaFin)
                            .Select(x => new
                            {
                                x.Id,
                                x.Nombre,
                                x.Estado,
                                x.FechaFin
                            }).Distinct().ToListAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = campaniasEstado
                        };
                    }else{
                        var campaniasEstado = await _context.CrmCampania
                            .Where(x => x.Estado == estado)  //Solo "A" pendiente, "I" inactivo
                            .OrderByDescending(x => x.FechaFin)
                            .Select(x => new
                            {
                                x.Id,
                                x.Nombre,
                                x.Estado,
                                x.FechaFin
                            }).Distinct().ToListAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = campaniasEstado
                        };
                    }
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };

        }

        // CrmCampania/ModificarEstado
        [HttpPut("ModificarEstado")]
        public async Task<ModeloRespuesta> ModificarEstado(uint id)
        {
            try
            {
                if (!CrmCampaniaExists(id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "CAMPANIA NO EXISTE"
                    };
                }

                var crmCampania = _context.CrmCampania.FirstOrDefault(x => x.Id == id);
                var estadoI = "I";
                var estadoA = "A";
                
                if (crmCampania.Estado == "A")
                {
                    crmCampania.Estado = estadoI;
                }else{
                    crmCampania.Estado = estadoA;
                }

                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                crmCampania.UserActualizacion = Convert.ToInt32(idusuario);
                crmCampania.FechaActualizacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Estado de Campania Modificado"
            };
        }


        //CrmCampania/ConsultaCampania
        [HttpGet("ConsultaCampania")]
        public async Task<ModeloRespuesta> ConsultaCampania()
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Estado == "A"))
                {
                    var campania = await _context.CrmCampania
                        .Where(x => x.Estado == "A")
                        .Select(x => new
                        {
                            x.Id,
                            x.Nombre,
                            x.Estado,
                            x.IdCartera
                        }).Distinct().ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = campania
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };

        }
    }
}
