using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclModulosController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclModulosController(ps_crmContext context)
        {
            _context = context;
        }


        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllModulos()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data =  await _context.AclModulos.Select(x => new
                        {
                            x.Anulado,
                            x.Codigo,
                            x.Nombre,
                            x.UsrModificacion,
                            x.UsrCreacion,
                            x.FechaCreacion,
                            x.Icono,
                            x.FechaModificacion,
                            x.IdModulo,
                            x.IdParentModulo,
                            x.ModuloPadre,
                            x.OrdenPresentacion,
                            x.RutaDirectorio,
                            x.Visible,
                            NombreParentModulo = x.IdParentModuloNavigation.Nombre
                        }
                    ).ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error Inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpGet("Modulos")]
        public async Task<ModeloRespuesta> GetModulo(uint? id)
        {
            try
            {
                var aclModulos = await _context.AclModulos.FindAsync(id);
                if (aclModulos == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Modulo no Encontrado"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = aclModulos
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarAclModulo(AclModulos aclModulos)
        {
            try
            {
                if (!AclModulosExists(aclModulos.IdModulo))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Modulo que desea modificar no existe"
                    };
                }

                var aclModuloModificar = _context.AclModulos.FirstOrDefault(x => x.IdModulo == aclModulos.IdModulo);
                if (!string.IsNullOrEmpty(aclModulos.Codigo))
                    aclModuloModificar.Codigo = aclModulos.Codigo;
                if (!string.IsNullOrEmpty(aclModulos.Nombre))
                    aclModuloModificar.Nombre = aclModulos.Nombre;
                if (!string.IsNullOrEmpty(aclModulos.Icono))
                    aclModuloModificar.Icono = aclModulos.Icono;
                if (!string.IsNullOrEmpty(aclModulos.ModuloPadre))
                    aclModuloModificar.ModuloPadre = aclModulos.ModuloPadre;
                if (!string.IsNullOrEmpty(aclModulos.RutaDirectorio))
                    aclModuloModificar.RutaDirectorio = aclModulos.RutaDirectorio;
                if (aclModulos.IdParentModulo != null)
                    aclModuloModificar.IdParentModulo = aclModulos.IdParentModulo;
                if (!string.IsNullOrEmpty(aclModulos.Visible) )
                    aclModuloModificar.Visible = aclModulos.Visible;
                if (aclModulos.OrdenPresentacion != null)
                    aclModuloModificar.OrdenPresentacion = aclModulos.OrdenPresentacion;
                if (!string.IsNullOrEmpty(aclModulos.Anulado))
                    aclModuloModificar.Anulado = aclModulos.Anulado;
                aclModuloModificar.UsrModificacion = aclModulos.UsrModificacion;
                aclModuloModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Modulo Modificado"
            };
        }

        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearAclModulos(AclModulos aclModulos)
        {
            try
            {
                aclModulos.FechaCreacion = DateTime.Now;
                _context.AclModulos.Add(aclModulos);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Modulo Creado",
            };
        }


        private bool AclModulosExists(uint id)
        {
            return _context.AclModulos.Any(e => e.IdModulo == id);
        }
    }
}
