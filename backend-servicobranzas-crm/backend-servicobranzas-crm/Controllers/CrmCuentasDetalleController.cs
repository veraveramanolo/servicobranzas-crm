using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasDetalleController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasDetalleController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentasDetalle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasDetalle>>> GetCrmCuentasDetalle()
        {
            return await _context.CrmCuentasDetalle.ToListAsync();
        }

        // GET: api/CrmCuentasDetalle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasDetalle>> GetCrmCuentasDetalle(uint id)
        {
            var crmCuentasDetalle = await _context.CrmCuentasDetalle.FindAsync(id);

            if (crmCuentasDetalle == null)
            {
                return NotFound();
            }

            return crmCuentasDetalle;
        }

        // PUT: api/CrmCuentasDetalle/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentasDetalle(uint id, CrmCuentasDetalle crmCuentasDetalle)
        {
            if (id != crmCuentasDetalle.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentasDetalle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasDetalleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentasDetalle
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasDetalle>> PostCrmCuentasDetalle(CrmCuentasDetalle crmCuentasDetalle)
        {
            _context.CrmCuentasDetalle.Add(crmCuentasDetalle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasDetalle", new {id = crmCuentasDetalle.Id}, crmCuentasDetalle);
        }

        // DELETE: api/CrmCuentasDetalle/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentasDetalle>> DeleteCrmCuentasDetalle(uint id)
        {
            var crmCuentasDetalle = await _context.CrmCuentasDetalle.FindAsync(id);
            if (crmCuentasDetalle == null)
            {
                return NotFound();
            }

            _context.CrmCuentasDetalle.Remove(crmCuentasDetalle);
            await _context.SaveChangesAsync();

            return crmCuentasDetalle;
        }

        private bool CrmCuentasDetalleExists(uint id)
        {
            return _context.CrmCuentasDetalle.Any(e => e.Id == id);
        }
        //CrmCuentasDetalle/GetCuentasDetalle
        //[HttpGet("GetCuentasDetalle")]

        [HttpGet("GetCuentasDetalle")]
        public async Task<ModeloRespuesta> GetCuentasDetalle(uint idCuentaCampania, int idCuentaCampaniaProducto)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var detalleCliente = new object();
                    var cuenta = _context.CrmCuentasCampania
                        .FirstOrDefault(x => x.Id == idCuentaCampania).IdCuenta;
                    if (cuenta != null)
                    {
                        detalleCliente = await _context.CrmCuentasDetalle
                        .Where(x => x.IdCuenta == cuenta 
                        && x.IdCuentaCampaniaProducto == idCuentaCampaniaProducto)
                        .Select(x => new
                            {
                                idCuentasDetalle = x.Id,
                                campo = x.Campo,
                                valor = x.Valor
                            }).ToListAsync();

                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = detalleCliente
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }


        [HttpGet("GetDetalleCuentasCampania")]
        public async Task<ModeloRespuesta> GetDetalleCuentasCampania(uint idCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.IdCampania == idCampania))
                {
                    var detalleClienteCampanias = new object();
                    var cuentas = _context.CrmCuentasCampania.Where(x => x.IdCampania == idCampania)
                                    .Select(x => x.IdCuenta);
                    
                    if (cuentas != null)
                    {
                        detalleClienteCampanias = await _context.CrmCuentasDetalle
                        .Where(x => cuentas.Contains(x.IdCuenta))
                        .Select(x => new
                            {
                                x.IdCuenta,
                                campo = x.Campo.TrimStart().TrimEnd(),
                                valor = x.Valor.TrimStart().TrimEnd()
                            })
                        .Where(x => x.campo.Length > 0 && x.valor.Length > 0)
                        .Distinct().OrderBy(x => x.campo).ToListAsync();

                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = detalleClienteCampanias
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        [HttpGet("FiltraValoresXnombre")]
        public async Task<ModeloRespuesta> FiltraValoresXnombre(uint idCampania, string nombre)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.IdCampania == idCampania))
                {
                    var valoresXnombre = new object();
                    var cuentas = _context.CrmCuentasCampania.Where(x => x.IdCampania == idCampania)
                                    .Select(x => x.IdCuenta);
                    
                    if (cuentas != null)
                    {
                        valoresXnombre = await _context.CrmCuentasDetalle
                        .Where(x => cuentas.Contains(x.IdCuenta) && string.Equals(x.Campo, nombre, StringComparison.CurrentCultureIgnoreCase))
                        .Select(x => new
                            {
                                // x.IdCuenta,
                                // campo = x.Campo.TrimStart().TrimEnd(),
                                valor = x.Valor.TrimStart().TrimEnd()
                            })
                        .Distinct().OrderBy(x => x.valor).ToListAsync();

                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = valoresXnombre
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
