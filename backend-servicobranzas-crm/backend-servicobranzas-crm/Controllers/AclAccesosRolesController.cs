using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclAccesosRolesController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclAccesosRolesController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("GetAccesos")]
        public async Task<ModeloRespuesta> GetAccesos(uint? idRol)
        {
            try
            {
                var aclAccesosRoles = await _context.AclAccesosRoles
                    .Where(x => x.IdRol == idRol)
                    .Select(x =>  new
                    {
                        x.Anulado,
                        x.IdRol,
                        x.IdAccesoRol,
                        x.UsrModificacion,
                        x.UsrCreacion,
                        x.IdModulo,
                        nombreModulo = x.IdModuloNavigation.Nombre
                    })
                    .ToListAsync();

                if (aclAccesosRoles == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Accesos no encontrados"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = aclAccesosRoles
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearAccesosRoles(AclAccesosRoles aclAccesosRoles)
        {
            try
            {
                aclAccesosRoles.FechaCreacion = DateTime.Now;
                _context.AclAccesosRoles.Add(aclAccesosRoles);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Acceso Creado"
            };

        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarAccesosRoles(AclAccesosRoles aclAccesosRoles)
        {
            try
            {
                if (!AclAccesosRolesExists(aclAccesosRoles.IdAccesoRol))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Acceso que desea modificar no existe"
                    };
                }

                var aclAccesosRolesModi =
                    _context.AclAccesosRoles.FirstOrDefault(x => x.IdAccesoRol == aclAccesosRoles.IdAccesoRol);

                if (aclAccesosRoles.IdModulo != null)
                    aclAccesosRolesModi.IdModulo = aclAccesosRoles.IdModulo;
                if (!string.IsNullOrEmpty(aclAccesosRoles.Anulado))
                    aclAccesosRolesModi.Anulado = aclAccesosRoles.Anulado;
                aclAccesosRolesModi.FechaModificacion = DateTime.Now;
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Acceso Modificado"
            };
        }

        private bool AclAccesosRolesExists(uint id)
        {
            return _context.AclAccesosRoles.Any(e => e.IdAccesoRol == id);
        }
    }
}
