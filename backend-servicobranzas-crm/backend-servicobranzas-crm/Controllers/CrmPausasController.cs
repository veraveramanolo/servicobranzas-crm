using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmPausasController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmPausasController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmPausas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmPausas>>> GetCrmPausas()
        {
            return await _context.CrmPausas.ToListAsync();
        }

        // GET: api/CrmPausas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmPausas>> GetCrmPausas(uint id)
        {
            var crmPausas = await _context.CrmPausas.FindAsync(id);

            if (crmPausas == null)
            {
                return NotFound();
            }

            return crmPausas;
        }

        // PUT: api/CrmPausas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmPausas(uint id, CrmPausas crmPausas)
        {
            if (id != crmPausas.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmPausas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmPausasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmPausas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmPausas>> PostCrmPausas(CrmPausas crmPausas)
        {
            _context.CrmPausas.Add(crmPausas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmPausas", new { id = crmPausas.Id }, crmPausas);
        }

        // DELETE: api/CrmPausas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmPausas>> DeleteCrmPausas(uint id)
        {
            var crmPausas = await _context.CrmPausas.FindAsync(id);
            if (crmPausas == null)
            {
                return NotFound();
            }

            _context.CrmPausas.Remove(crmPausas);
            await _context.SaveChangesAsync();

            return crmPausas;
        }

        private bool CrmPausasExists(uint id)
        {
            return _context.CrmPausas.Any(e => e.Id == id);
        }
    }
}
