using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclRelacionController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclRelacionController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclRelacion
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclRelacion>>> GetAclRelacion()
        {
            return await _context.AclRelacion.ToListAsync();
        }

        // GET: api/AclRelacion/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclRelacion>> GetAclRelacion(uint id)
        {
            var aclRelacion = await _context.AclRelacion.FindAsync(id);

            if (aclRelacion == null)
            {
                return NotFound();
            }

            return aclRelacion;
        }

        // PUT: api/AclRelacion/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclRelacion(uint id, AclRelacion aclRelacion)
        {
            if (id != aclRelacion.IdRelacion)
            {
                return BadRequest();
            }

            _context.Entry(aclRelacion).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclRelacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclRelacion
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclRelacion>> PostAclRelacion(AclRelacion aclRelacion)
        {
            _context.AclRelacion.Add(aclRelacion);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAclRelacion", new { id = aclRelacion.IdRelacion }, aclRelacion);
        }

        // DELETE: api/AclRelacion/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclRelacion>> DeleteAclRelacion(uint id)
        {
            var aclRelacion = await _context.AclRelacion.FindAsync(id);
            if (aclRelacion == null)
            {
                return NotFound();
            }

            _context.AclRelacion.Remove(aclRelacion);
            await _context.SaveChangesAsync();

            return aclRelacion;
        }

        private bool AclRelacionExists(uint id)
        {
            return _context.AclRelacion.Any(e => e.IdRelacion == id);
        }

        //Consulta todas las relaciones
        //api/AclRelacion/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclRelacion()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclRelacion.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
