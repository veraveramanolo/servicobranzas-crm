using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclUsuarioController : ControllerBase
    {
        private Campania_Gestion _campaniaGestion;
        private readonly ps_crmContext _context;

        public AclUsuarioController(ps_crmContext context)
        {
            _context = context;
        }


        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclUsuario()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclUsuario.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpGet("Usuario")]
        public async Task<ModeloRespuesta> UsuarioAclUsuario(uint id)
        {

            try
            {
                var aclUsuario = await _context.AclUsuario.FindAsync(id);
                if (aclUsuario == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Usuario no encontrado"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = aclUsuario
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado ",
                    Data = new string(e.Message)
                };
            }
        }


        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarAclUsuario(AclUsuario aclUsuario)
        {
            try
            {
                if (!AclUsuarioExists(aclUsuario.IdUsuario))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Usuario que desea modificar no existe"
                    };
                }

                var aclUsuarioModificar = _context.AclUsuario.FirstOrDefault(x => x.IdUsuario == aclUsuario.IdUsuario);
                if(!String.IsNullOrEmpty(aclUsuario.Codigo))
                    aclUsuarioModificar.Codigo = aclUsuario.Codigo;
                if (!String.IsNullOrEmpty(aclUsuario.Nombres))
                    aclUsuarioModificar.Nombres = aclUsuario.Nombres;
                if (!String.IsNullOrEmpty(aclUsuario.Apellidos))
                    aclUsuarioModificar.Apellidos = aclUsuario.Apellidos;
                if (!String.IsNullOrEmpty(aclUsuario.Email))
                    aclUsuarioModificar.Email = aclUsuario.Email;
                if (!String.IsNullOrEmpty(aclUsuario.Cargo))
                    aclUsuarioModificar.Cargo = aclUsuario.Cargo;
                if (!String.IsNullOrEmpty(aclUsuario.Departamento))
                    aclUsuarioModificar.Departamento = aclUsuario.Departamento;
                if (!String.IsNullOrEmpty(aclUsuario.Telefono))
                    aclUsuarioModificar.Telefono = aclUsuario.Telefono;
                if (!String.IsNullOrEmpty(aclUsuario.Agentname))
                    aclUsuarioModificar.Agentname = aclUsuario.Agentname;
                if (!String.IsNullOrEmpty(aclUsuario.Agentpass))
                    aclUsuarioModificar.Agentpass = aclUsuario.Agentpass;
                if (!String.IsNullOrEmpty(aclUsuario.Md5Password))
                    aclUsuarioModificar.Md5Password = aclUsuario.Md5Password;
                if (!String.IsNullOrEmpty(aclUsuario.UsrModificacion))
                    aclUsuarioModificar.UsrModificacion = aclUsuario.UsrModificacion;
                aclUsuarioModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Usuario Modificado"
            };
        }

        
        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearAclUsuario(AclUsuario aclUsuario)
        {
            try
            {
                _context.AclUsuario.Add(aclUsuario);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Usuario Creado",
                Data = await _context.AclUsuario.FindAsync(aclUsuario.IdUsuario)
            };

        }

       
        [HttpGet("CambiarEstado")]
        public async Task<ModeloRespuesta> CambiarEstadoAclUsuario(uint id , string anulado, string UsrModificacion)
        {
            try
            {
                if (!AclUsuarioExists(id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Usuario que desea modificar no existe"
                    };
                }

                var aclUsuarioModificar = _context.AclUsuario.FirstOrDefault(x => x.IdUsuario == id);
                aclUsuarioModificar.Anulado = anulado;
                if (!String.IsNullOrEmpty(UsrModificacion))
                    aclUsuarioModificar.UsrModificacion = UsrModificacion;
                aclUsuarioModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Usuario Modificado"
            };

        }

        private bool AclUsuarioExists(uint id)
        {
            return _context.AclUsuario.Any(e => e.IdUsuario == id);
        }

        // api/AclUsuario/AllAsesor
        [HttpGet("AllAsesor")]
        public async Task<ModeloRespuesta> AllAsesor()
        {
            try
            {
                if (_context.AclUsuario.Any(x => !x.IdUsuario.Equals(null)))
                {
                    if (_context.AclUsuario.Any(x => x.Anulado == "NO"))
                    {
                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await _context.AclUsuario.Where(x => x.Anulado == "NO")
                                    .Select(x => new
                                    {
                                        x.IdUsuario,
                                        x.UserName,
                                        nombres = x.Nombres + " " + x.Apellidos
                                    }).ToListAsync()
                        };
                    } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY ASESORES ACTIVOS"
                    };
                    }
                } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY ASESORES REGISTRADOS"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/AclUsuario/UsuariosAsignados?
        [HttpGet("UsuariosAsignados")]
        public async Task<ModeloRespuesta> GetListaAsesoresAsig(int idCampania)
        {
            List<AclListaAsesoresasig> listaAsesoresAsig;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                listaAsesoresAsig = await _campaniaGestion.GetListaAsesoresAsig(idCampania);
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Data = listaAsesoresAsig
            };
        }

    }
}
