using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrmPausasUsuarioController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmPausasUsuarioController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmPausasUsuario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmPausasUsuario>>> GetCrmPausasUsuario()
        {
            return await _context.CrmPausasUsuario.ToListAsync();
        }

        // GET: api/CrmPausasUsuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmPausasUsuario>> GetCrmPausasUsuario(uint id)
        {
            var crmPausasUsuario = await _context.CrmPausasUsuario.FindAsync(id);

            if (crmPausasUsuario == null)
            {
                return NotFound();
            }

            return crmPausasUsuario;
        }

        // PUT: api/CrmPausasUsuario/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmPausasUsuario(uint id, CrmPausasUsuario crmPausasUsuario)
        {
            if (id != crmPausasUsuario.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmPausasUsuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmPausasUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmPausasUsuario
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmPausasUsuario>> PostCrmPausasUsuario(CrmPausasUsuario crmPausasUsuario)
        {
            _context.CrmPausasUsuario.Add(crmPausasUsuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmPausasUsuario", new { id = crmPausasUsuario.Id }, crmPausasUsuario);
        }

        // DELETE: api/CrmPausasUsuario/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmPausasUsuario>> DeleteCrmPausasUsuario(uint id)
        {
            var crmPausasUsuario = await _context.CrmPausasUsuario.FindAsync(id);
            if (crmPausasUsuario == null)
            {
                return NotFound();
            }

            _context.CrmPausasUsuario.Remove(crmPausasUsuario);
            await _context.SaveChangesAsync();

            return crmPausasUsuario;
        }

        private bool CrmPausasUsuarioExists(uint id)
        {
            return _context.CrmPausasUsuario.Any(e => e.Id == id);
        }
    }
}
