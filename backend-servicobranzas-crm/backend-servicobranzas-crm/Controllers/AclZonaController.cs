using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclZonaController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclZonaController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclZona
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclZona>>> GetAclZona()
        {
            return await _context.AclZona.ToListAsync();
        }

        // GET: api/AclZona/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclZona>> GetAclZona(int id)
        {
            var aclZona = await _context.AclZona.FindAsync(id);

            if (aclZona == null)
            {
                return NotFound();
            }

            return aclZona;
        }

        // PUT: api/AclZona/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclZona(uint id, AclZona aclZona)
        {
            if (id != aclZona.IdZona)
            {
                return BadRequest();
            }

            _context.Entry(aclZona).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclZonaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclZona
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclZona>> PostAclZona(AclZona aclZona)
        {
            _context.AclZona.Add(aclZona);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclZonaExists(aclZona.IdZona))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclZona", new { id = aclZona.IdZona }, aclZona);
        }

        // DELETE: api/AclZona/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclZona>> DeleteAclZona(int id)
        {
            var aclZona = await _context.AclZona.FindAsync(id);
            if (aclZona == null)
            {
                return NotFound();
            }

            _context.AclZona.Remove(aclZona);
            await _context.SaveChangesAsync();

            return aclZona;
        }

        private bool AclZonaExists(uint id)
        {
            return _context.AclZona.Any(e => e.IdZona == id);
        }

        //Consulta todos los departamentos
        //api/AclZona/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclZona()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclZona.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
