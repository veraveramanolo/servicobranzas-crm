using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclCiudadController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclCiudadController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclCiudad
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclCiudad>>> GetAclCiudad()
        {
            return await _context.AclCiudad.ToListAsync();
        }

        // GET: api/AclCiudad/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclCiudad>> GetAclCiudad(int id)
        {
            var aclCiudad = await _context.AclCiudad.FindAsync(id);

            if (aclCiudad == null)
            {
                return NotFound();
            }

            return aclCiudad;
        }

        // PUT: api/AclCiudad/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclCiudad(uint id, AclCiudad aclCiudad)
        {
            if (id != aclCiudad.IdCiudad)
            {
                return BadRequest();
            }

            _context.Entry(aclCiudad).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclCiudadExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclCiudad
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclCiudad>> PostAclCiudad(AclCiudad aclCiudad)
        {
            _context.AclCiudad.Add(aclCiudad);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclCiudadExists(aclCiudad.IdCiudad))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclCiudad", new { id = aclCiudad.IdCiudad }, aclCiudad);
        }

        // DELETE: api/AclCiudad/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclCiudad>> DeleteAclCiudad(int id)
        {
            var aclCiudad = await _context.AclCiudad.FindAsync(id);
            if (aclCiudad == null)
            {
                return NotFound();
            }

            _context.AclCiudad.Remove(aclCiudad);
            await _context.SaveChangesAsync();

            return aclCiudad;
        }

        private bool AclCiudadExists(uint id)
        {
            return _context.AclCiudad.Any(e => e.IdCiudad == id);
        }

        //Consulta todos las ciudades
        //api/AclCiudad/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclCiudad()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclCiudad.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        //Consulta ciudades por id depto.
        // api/CrmCuentasDireccion/GetCiudad

        [HttpGet("GetCiudad")]
        public async Task<ModeloRespuesta> GetCiudad(uint idDepto)
        {
            try
            {
                if (_context.AclCiudad.Any(x=>x.IdDepto == idDepto))
                {
                    var listaCiudad = await _context.AclCiudad
                        .Where(x => x.IdDepto == idDepto)
                        .Select(x => new
                        {
                            x.IdCiudad,
                            x.CiudadValor
                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = listaCiudad
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
