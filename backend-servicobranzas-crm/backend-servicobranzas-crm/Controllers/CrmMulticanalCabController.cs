using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
using System.IdentityModel.Tokens.Jwt;
using System.Transactions;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmMulticanalCabController : ControllerBase
    {
        private Campania_Gestion _campaniaGestion;
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmMulticanalCabController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmMulticanalCab
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmMulticanalCab>>> GetCrmMulticanalCab()
        {
            return await _context.CrmMulticanalCab.ToListAsync();
        }

        //Guarda telefonos por cuenta.
        // api/CrmMulticanalCab/CrearFiltro
        [HttpPost("CrearFiltro")]
        public async Task<ModeloRespuesta> CrearFiltro(CrmMulticanalCab crmMulticanalCab)
        {
            using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                uint dataa = 0;
                try
                {
                    if(!crmMulticanalCab.Equals(null))
                    {
                        CrmMulticanalCab listado = new CrmMulticanalCab();
                        listado = crmMulticanalCab;
                        listado.FechaCreacion = DateTime.Now;
                        listado.Status = "A";
                        _decoder = new Decoder();
                        uint idusuario = _decoder.GetDecoder(HttpContext);
                        listado.UserCreacion = idusuario.ToString();
                        _context.CrmMulticanalCab.Add(listado);
                        await _context.SaveChangesAsync();
                        
                        foreach(CrmMulticanalDetalle z in crmMulticanalCab.CrmMulticanalDetalle.ToList())
                                {
                                    CrmMulticanalDetalle data = new CrmMulticanalDetalle();
                                    data.Idmulticanal = listado.Idmulticanal;
                                    dataa = listado.Idmulticanal;
                                    data.Asesor = z.Asesor;
                                    data.IdUsuario = z.IdUsuario;
                                    data.Nocuenta = z.Nocuenta;
                                    data.Nombrecompleto = z.Nombrecompleto;
                                    data.Identificacion = z.Identificacion;
                                    data.Ultimagestion = z.Ultimagestion;
                                    data.Gestion = z.Gestion;
                                    data.Diasmora = z.Diasmora;
                                    data.Mejorgestion = z.Mejorgestion;
                                    data.Fechamejorgestion = z.Fechamejorgestion;
                                    data.Deudatotal = z.Deudatotal;
                                    data.Statuscartera = z.Statuscartera;
                                    data.Fechaacuerdo = z.Fechaacuerdo;
                                    data.Valoracuerdo = z.Valoracuerdo;
                                    data.Valorpago = z.Valorpago;
                                    data.FechaPago = z.FechaPago;
                                    data.Totalacuerdo = z.Totalacuerdo;
                                    data.Totalpago = z.Totalpago;
                                    data.Cuota = z.Cuota;
                                    data.Totalcuotas = z.Totalcuotas;
                                    data.Deudavencida = z.Deudavencida;
                                    data.Extra1 = z.Extra1;
                                    data.Extra2 = z.Extra2;
                                    data.Extra3 = z.Extra3;
                                    data.Extra4 = z.Extra4;
                                    data.Extra5 = z.Extra5;
                                    data.Extra6 = z.Extra6;
                                    data.Extra7 = z.Extra7;
                                    data.Extra8 = z.Extra8;
                                    data.Extra9 = z.Extra9;
                                    data.Extra10 = z.Extra10;
                                    _context.CrmMulticanalDetalle.Add(data);
                                }
                            if (dataa!= 0){
                                transaction.Complete();
                            }
                    }

                }
                catch (Exception e)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Error inesperado",
                        Data = new string(e.Message)
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Mensage = "Multicanal Filtro Creado",
                    Data = dataa
                };
            }
        }

        //api/CrmMulticanalCab/InsertaDetalleMulticanal
        // [HttpPost("InsertaDetalleMulticanal")]
        // public async Task<ModeloRespuesta> InsertaDetalleMulticanal(List<CrmMulticanal> registros)
        // {
        //     try
        //     {
        //         // _context.CrmMulticanalDetalle.RemoveRange(_context.CrmMulticanalDetalle.Where(x => x.Idmulticanal != 0 ));
        //         // _context.SaveChanges();

        //         if (_context.CrmMulticanalCab.Any())
        //         {
        //             var MulticanalCab = Convert.ToUInt32(_context.CrmMulticanalCab
        //                 .Where(x => x.Idmulticanal != 0 && x.Status == "A")
        //                 .OrderByDescending(x => x.FechaCreacion)
        //                 .Select(x => x.Idmulticanal).FirstOrDefault());

        //             if (MulticanalCab != null && registros.Any())
        //             {
        //                 foreach (CrmMulticanal z in registros)
        //                 {
        //                     CrmMulticanalDetalle data = new CrmMulticanalDetalle();
        //                     data.Idmulticanal = MulticanalCab;
        //                     data.Asesor = z.Asesor;
        //                     data.Nocuenta = z.Nocuenta;
        //                     data.Nombrecompleto = z.Nombrecompleto;
        //                     data.Identificacion = z.Identificacion;
        //                     data.Ultimagestion = z.Ultimagestion;
        //                     data.Gestion = z.Gestion;
        //                     data.Diasmora = z.Diasmora;
        //                     data.Mejorgestion = z.Mejorgestion;
        //                     data.Deudatotal = z.Deudatotal;
        //                     data.Statuscartera = z.Statuscartera;
        //                     data.Fechaacuerdo = z.Fechaacuerdo;
        //                     data.Valoracuerdo = z.Valoracuerdo;
        //                     data.Valorpago = z.Valorpago;
        //                     data.FechaPago = z.FechaPago;
        //                     data.Totalacuerdo = z.Totalacuerdo;
        //                     data.Totalpago = z.Totalpago;
        //                     data.Cuota = z.Cuota;
        //                     data.Totalcuotas = z.Totalcuotas;
        //                     _context.CrmMulticanalDetalle.Add(data);
        //                 }
        //                 await _context.SaveChangesAsync();

        //             }
        //         }
        //         return new ModeloRespuesta()
        //         {
        //             Exito = 1,
        //             Mensage = "BD Filtrada Multicanal insertada",
        //             Data = await _context.CrmMulticanalDetalle.ToListAsync()
        //         };

        //     }
        //     catch (Exception e)
        //     {
        //         return new ModeloRespuesta()
        //         {
        //             Exito = 0,
        //             Mensage = "Error inesperado",
        //             Data = new string(e.Message)
        //         };
        //     }


        // }


        //api/CrmMulticanalCab/GetMulticanalCab
        [HttpGet("GetMulticanalCab")]
        public async Task<ModeloRespuesta> GetMulticanalCab(uint idCampania)
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Id == idCampania))
                {
                    var MulticanalCab = new object();
                    var campania = _context.CrmCampania
                        .FirstOrDefault(x => x.Id == idCampania);
                    if (campania != null)
                    {
                        MulticanalCab = await _context.CrmMulticanalCab
                            .Where(x => x.IdCampania == idCampania && x.Status == "A" && x.CampoRef1.Equals(null))
                            .OrderByDescending(x => x.FechaCreacion).ToListAsync();
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = MulticanalCab
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //api/CrmMulticanalCab/GetMulticanalFiltro
        [HttpGet("GetMulticanalFiltro")]
        public async Task<ModeloRespuesta> GetMulticanalFiltro(uint idCampania, uint idMulticanal)
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Id == idCampania))
                {
                    var MulticanalDet = new object();
                    var campania = _context.CrmCampania
                        .FirstOrDefault(x => x.Id == idCampania);
                    if (campania != null)
                    {
                        if (CrmMulticanalCabExists(idMulticanal))
                        {
                            MulticanalDet = await _context.CrmMulticanalDetalle
                            .Where(x => x.Idmulticanal == idMulticanal)
                            .Select(x => new
                            {
                                Asesor = x.Asesor,
                                IdUsuario = x.IdUsuario,
                                Nocuenta = x.Nocuenta,
                                Nombrecompleto = x.Nombrecompleto,
                                Identificacion = x.Identificacion,
                                Ultimagestion = x.Ultimagestion,
                                Gestion = x.Gestion,
                                Diasmora = x.Diasmora,
                                Mejorgestion = x.Mejorgestion,
                                Deudatotal = x.Deudatotal,
                                Statuscartera = x.Statuscartera,
                                Fechaacuerdo = x.Fechaacuerdo,
                                Valoracuerdo = x.Valoracuerdo,
                                Valorpago = x.Valorpago,
                                FechaPago = x.FechaPago,
                                Totalacuerdo = x.Totalacuerdo,
                                Totalpago = x.Totalpago,
                                Cuota = x.Cuota,
                                Totalcuotas = x.Totalcuotas,
                                Deudavencida = x.Deudavencida,
                                Extra1 = x.Extra1,
                                Extra2 = x.Extra2,
                                Extra3 = x.Extra3,
                                Extra4 = x.Extra4,
                                Extra5 = x.Extra5,
                                Extra6 = x.Extra6,
                                Extra7 = x.Extra7,
                                Extra8 = x.Extra8,
                                Extra9 = x.Extra9,
                                Extra10 = x.Extra10,
                                Correos = _context.CrmCuentasEmail.Where(e => e.IdCuenta == x.Nocuenta)
                                            .Select(e => e.Email).Distinct().ToList(),
                                Telefonos = _context.CrmCuentasTelefono.Where(e => e.IdCuenta == x.Nocuenta && 
                                              (string.Equals(e.Tipo, "CELULAR", StringComparison.CurrentCultureIgnoreCase) || string.Equals(e.Tipo, "MOVIL OTRO", StringComparison.CurrentCultureIgnoreCase)))
                                              .Select(e => e.Valor).Distinct().ToList()
                            })
                            .ToListAsync();
                        }
                        else
                        {
                            return new ModeloRespuesta()
                            {
                                Exito = 0,
                                Mensage = "Filtro no encontrado",
                            };
                        }
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = MulticanalDet
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        private bool CrmMulticanalCabExists(uint id)
        {
            return _context.CrmMulticanalCab.Any(e => e.Idmulticanal == id);
        }


        //Inactivar filtro.
        // api/CrmMulticanalCab/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmMulticanalCab(CrmMulticanalCab crmMulticanalCab)
        {
            try
            {
                if (!CrmMulticanalCabExists(crmMulticanalCab.Idmulticanal))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Teléfono que desea inactivar no existe"
                    };
                }

                var CrmMulticanalCabModificar = _context.CrmMulticanalCab.FirstOrDefault(x => x.Idmulticanal == crmMulticanalCab.Idmulticanal);

                CrmMulticanalCabModificar.Status = "I";
                CrmMulticanalCabModificar.FechaModificacion = DateTime.Now;

                //Obtener Usuario
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                CrmMulticanalCabModificar.UserModificacion = idusuario.ToString();

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Multicanal Filtro Inactivo"
            };
        }


        [HttpGet("GetMulticanalEstados")]
        public async Task<MultiselectResponse> GetMulticanalFiltroFecha(int idCampania, DateTime? fechafiltro)
        {
            List<AclMulticanalEstados> consultaMulticanalEstado;
            List<AclMulticanalEstados> consultaMulticanalAsesor;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                consultaMulticanalEstado = await _campaniaGestion.GetMulticanalFiltroFecha(idCampania, fechafiltro);

            }
            catch (Exception e)
            {
                return new MultiselectResponse()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message),
                };
            }

            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                consultaMulticanalAsesor = await _campaniaGestion.GetMulticanalAsesores(idCampania);

            }
            catch (Exception e)
            {
                return new MultiselectResponse()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Asesores = new string(e.Message),
                };
            }

            return new MultiselectResponse()
            {
                Exito = 1,
                Data = consultaMulticanalEstado,
                Asesores = consultaMulticanalAsesor
            };
        }


        //api/CrmMulticanalCab/GetExtrasMulticanal
        [HttpGet("GetExtrasMulticanal")]
        public async Task<ModeloFiltroExtra> GetExtrasMulticanal(uint idCampania)
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Id == idCampania))
                {
                    var qc = from cc in _context.CrmCuentasCampania 
                                    join b in _context.CrmCuentasCampaniaGestor on cc.Id equals b.IdCuentaCampania into ccb 
                                    from leftccb in ccb.DefaultIfEmpty()
                                    join c in _context.AclUsuario on leftccb.IdUsuario equals c.IdUsuario into cleftccb 
                                    from leftleftccb in cleftccb.DefaultIfEmpty()
                                    join d in _context.CrmCuentas on cc.IdCuenta equals d.Id 
                                    join ge in _context.CrmGestion on cc.Id equals ge.IdCuentaCampania into dge 
                                    from leftdge in dge.DefaultIfEmpty() 
                                    join i in _context.CrmArbolGestion on leftdge.IdArbolDecision equals i.Id into ii 
                                    from leftii in ii.DefaultIfEmpty()
                                    join detail in _context.CrmCuentasDetalle on d.Id equals detail.IdCuenta
                                    where cc.IdCampania == idCampania && detail.Valor.Trim().Length > 0
                                    select new {detail.Valor, detail.Campo};

                    var extra1 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA1"
                                    select a.Valor;
                    var extra2 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA2"
                                    select a.Valor;
                    var extra3 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA3"
                                    select a.Valor;
                    var extra4 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA4"
                                    select a.Valor;
                    var extra5 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA5"
                                    select a.Valor;
                    var extra6 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA6"
                                    select a.Valor;
                    var extra7 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA7"
                                    select a.Valor;
                    var extra8 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA8"
                                    select a.Valor;
                    var extra9 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA9"
                                    select a.Valor;
                    var extra10 = from a in qc where a.Campo.Trim().ToUpper() == "EXTRA10"
                                    select a.Valor;                         
                            
                    return new ModeloFiltroExtra()
                    {
                        Exito = 1,
                        Extra1 = extra1.Distinct().ToList(),
                        Extra2 = extra2.Distinct().ToList(),
                        Extra3 = extra3.Distinct().ToList(),
                        Extra4 = extra4.Distinct().ToList(),
                        Extra5 = extra5.Distinct().ToList(),
                        Extra6 = extra6.Distinct().ToList(),
                        Extra7 = extra7.Distinct().ToList(),
                        Extra8 = extra8.Distinct().ToList(),
                        Extra9 = extra9.Distinct().ToList(),
                        Extra10 = extra10.Distinct().ToList()
                    };
                        
                }

            }
            catch (Exception e)
            {
                return new ModeloFiltroExtra()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Extra1 = new string(e.Message)
                };
            }

            return new ModeloFiltroExtra()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //api/CrmMulticanalCab/GetMulticanalCabGestion
        [HttpGet("GetMulticanalCabGestion")]
        public async Task<ModeloRespuesta> GetMulticanalCabGestion(uint idCampania, string usercreacion)
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Id == idCampania))
                {
                    var MulticanalCab = new object();
                    var campania = _context.CrmCampania
                        .FirstOrDefault(x => x.Id == idCampania);
                    if (campania != null)
                    {
                        MulticanalCab = await _context.CrmMulticanalCab
                            .Where(x => x.IdCampania == idCampania && x.UserCreacion == usercreacion && x.Status == "A" && !x.CampoRef1.Equals(null))
                            .OrderByDescending(x => x.FechaCreacion)
                            .Select(x => new
                            {
                                x.Idmulticanal,
                                x.Descripcion,
                                x.IdCampania,
                                productos = _context.CrmMulticanalDetalle.Where(m => m.Idmulticanal == x.Idmulticanal).Select(m => m.Totalcuotas).Count(),
                                gestionados = _context.CrmMulticanalDetalle.Where(m => m.Idmulticanal == x.Idmulticanal)
                                                .Join(_context.CrmGestion, md => md.Diasmora, g => Convert.ToInt32(g.IdCuentaCampania),
                                                (md,g) => new {md,g}).Select(mg => mg.g.IdArbolDecision).Count(),
                                sgestion = /*(from m in _context.CrmMulticanalDetalle 
                                            join g in _context.CrmGestion on m.Diasmora equals Convert.ToInt32(g.IdCuentaCampania) into mg 
                                            from left in mg.DefaultIfEmpty() 
                                            where m.Idmulticanal == x.Idmulticanal && left.DetalleGestion.Length == 0 
                                            select (m.Idmulticanal)).Count()*/
                                            /*_context.CrmMulticanalDetalle.Where(m => m.Idmulticanal == x.Idmulticanal)
                                                .Join(_context.CrmGestion, md => md.Diasmora, g => Convert.ToInt32(g.IdCuentaCampania),
                                                (md,g) => new {md,g}).DefaultIfEmpty().Where(mg => mg.g.Id.Equals(null)).Select(mg => mg.md.Diasmora).Count()*/
                                                string.Empty,
                                acuerdos = _context.CrmMulticanalDetalle.Where(m => m.Idmulticanal == x.Idmulticanal).Select(m => m.Valoracuerdo).Sum(),
                                pagos = _context.CrmMulticanalDetalle.Where(m => m.Idmulticanal == x.Idmulticanal).Select(m => m.Valorpago).Sum()
                            })
                            .ToListAsync();
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = MulticanalCab
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //api/CrmMulticanalCab/GetMulticanalFiltroGestion
        [HttpGet("GetMulticanalFiltroGestion")]
        public async Task<ModeloRespuesta> GetMulticanalFiltroGestion(uint idCampania, uint idMulticanal)
        {
            try
            {
                if (_context.CrmCampania.Any(x => x.Id == idCampania))
                {
                    var DetalleGestion = new object();
                    var campania = _context.CrmCampania
                        .FirstOrDefault(x => x.Id == idCampania);
                    if (campania != null)
                    {
                        if (CrmMulticanalCabExists(idMulticanal))
                        {
                            DetalleGestion = await _context.CrmMulticanalDetalle
                            .Where(x => x.Idmulticanal == idMulticanal)
                            .Select(x => new
                            {
                                Asesor = x.Asesor,
                                IdUsuario = x.IdUsuario,
                                idcuenta = x.Nocuenta,
                                nombrecliente = x.Nombrecompleto,
                                cedula = x.Identificacion,
                                Ultimagestion = x.Ultimagestion,
                                estado = x.Gestion,
                                idcuentacampania = x.Diasmora,
                                mejorestado = x.Mejorgestion,
                                fechamejorestado = x.Fechamejorgestion,
                                Deudatotal = x.Deudatotal,
                                Statuscartera = x.Statuscartera,
                                acuerdo = x.Fechaacuerdo,
                                Valoracuerdo = x.Valoracuerdo,
                                Valorpago = x.Valorpago,
                                pago = x.FechaPago,
                                cuotaspendientes = x.Cuota,
                                idproducto = x.Totalcuotas,
                                Deudavencida = x.Deudavencida,
                                Extra1 = x.Extra1,
                                Extra2 = x.Extra2,
                                Extra3 = x.Extra3,
                                Extra4 = x.Extra4,
                                Extra5 = x.Extra5,
                                Extra6 = x.Extra6,
                                Extra7 = x.Extra7,
                                Extra8 = x.Extra8,
                                Extra9 = x.Extra9,
                                Extra10 = x.Extra10
                            })
                            .ToListAsync();
                        }
                        else
                        {
                            return new ModeloRespuesta()
                            {
                                Exito = 0,
                                Mensage = "Filtro no encontrado",
                            };
                        }
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = DetalleGestion
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
