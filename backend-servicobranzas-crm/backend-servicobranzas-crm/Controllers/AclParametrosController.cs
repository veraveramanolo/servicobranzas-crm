using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclParametrosController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclParametrosController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclParametros
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclParametros>>> GetAclParametros()
        {
            return await _context.AclParametros.ToListAsync();
        }

        // GET: api/AclParametros/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclParametros>> GetAclParametros(uint id)
        {
            var aclParametros = await _context.AclParametros.FindAsync(id);

            if (aclParametros == null)
            {
                return NotFound();
            }

            return aclParametros;
        }

        // PUT: api/AclParametros/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclParametros(uint id, AclParametros aclParametros)
        {
            if (id != aclParametros.IdParametro)
            {
                return BadRequest();
            }

            _context.Entry(aclParametros).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclParametrosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclParametros
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclParametros>> PostAclParametros(AclParametros aclParametros)
        {
            _context.AclParametros.Add(aclParametros);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAclParametros", new { id = aclParametros.IdParametro }, aclParametros);
        }

        // DELETE: api/AclParametros/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclParametros>> DeleteAclParametros(uint id)
        {
            var aclParametros = await _context.AclParametros.FindAsync(id);
            if (aclParametros == null)
            {
                return NotFound();
            }

            _context.AclParametros.Remove(aclParametros);
            await _context.SaveChangesAsync();

            return aclParametros;
        }

        private bool AclParametrosExists(uint id)
        {
            return _context.AclParametros.Any(e => e.IdParametro == id);
        }

        // AclParametros/GetParametros
        [HttpGet("GetParametros")]
        public async Task<ModeloRespuesta> GetParametros(uint idModulo, string estado, string submodulo)
        {
            try
            {
                var aclParametros = await _context.AclParametros
                                        .Where(p => p.IdModulo == idModulo && p.Estado.Contains(estado) && p.CampoRef1 == submodulo)
                                        .Select(p => new 
                                                { 
                                                    p.NombreParametro,
                                                    p.IdParametro,
                                                    p.ValorParametro
                                                })
                                        .OrderBy(p => p.NombreParametro).ToListAsync();
                if (aclParametros == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Parámetros no Encontrados"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = aclParametros
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
