﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclAccionesController : Controller
    {
        private readonly ps_crmContext _context;

        public AclAccionesController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclAcciones()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclAcciones.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpGet("Acciones")]
        public async Task<ModeloRespuesta> GetAclAcciones(uint? id)
        {
            try
            {
                var aclAcciones = await _context.AclAcciones.FindAsync(id);
                if (aclAcciones == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Acción no encontrada"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = aclAcciones
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarAclAcciones(AclAcciones aclAcciones)
        {
            try
            {
                if (!AclAccionesExists(aclAcciones.IdAccion))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Acción que desea modificar no existe"
                    };
                }

                var aclAccionesModificar = _context.AclAcciones.FirstOrDefault(x => x.IdAccion == aclAcciones.IdAccion);
                if (!string.IsNullOrEmpty(aclAcciones.Nombre))
                    aclAccionesModificar.Nombre = aclAcciones.Nombre;
                if (!string.IsNullOrEmpty(aclAcciones.Codigo))
                    aclAccionesModificar.Codigo = aclAcciones.Codigo;
                if (!string.IsNullOrEmpty(aclAcciones.Icono))
                    aclAccionesModificar.Icono = aclAcciones.Icono;
                if (!string.IsNullOrEmpty(aclAcciones.Anulado))
                    aclAccionesModificar.Anulado = aclAcciones.Anulado;
                if (!string.IsNullOrEmpty(aclAcciones.UsrModificacion))
                    aclAccionesModificar.UsrModificacion = aclAcciones.UsrModificacion;
                aclAccionesModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Perfil Modificado"
            };
        }

        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearAclAcciones( AclAcciones aclAcciones)
        {
            try
            {
                aclAcciones.FechaCreacion = DateTime.Now;
                _context.AclAcciones.Add(aclAcciones);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Acción Creada",
                //Data = await _context.AclPerfiles.FindAsync(aclAcciones.IdAccion)
            };
        }


      
        private bool AclAccionesExists(uint id)
        {
            return _context.AclAcciones.Any(e => e.IdAccion == id);
        }
    }
}
