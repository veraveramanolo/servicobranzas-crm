using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaPagosController : ControllerBase
    {
        private Matriz_Conciliacion _matrizConciliacion;
        private readonly ps_crmContext _context;

        public CrmCuentasCampaniaPagosController(ps_crmContext context)
        {
            _context = context;
        }

        
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<CrmCuentasCampaniaPagos>>> GetCrmCuentasCampaniaPagos()
        //{
        //    return await _context.CrmCuentasCampaniaPagos.ToListAsync();
        //}

        
        //[HttpGet("{id}")]
        //public async Task<ActionResult<CrmCuentasCampaniaPagos>> GetCrmCuentasCampaniaPagos(uint id)
        //{
        //    var crmCuentasCampaniaPagos = await _context.CrmCuentasCampaniaPagos.FindAsync(id);

        //    if (crmCuentasCampaniaPagos == null)
        //    {
        //        return NotFound();
        //    }

        //    return crmCuentasCampaniaPagos;
        //}

        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutCrmCuentasCampaniaPagos(uint id, CrmCuentasCampaniaPagos crmCuentasCampaniaPagos)
        //{
        //    if (id != crmCuentasCampaniaPagos.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(crmCuentasCampaniaPagos).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CrmCuentasCampaniaPagosExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        
        //[HttpPost]
        //public async Task<ActionResult<CrmCuentasCampaniaPagos>> PostCrmCuentasCampaniaPagos(CrmCuentasCampaniaPagos crmCuentasCampaniaPagos)
        //{
        //    _context.CrmCuentasCampaniaPagos.Add(crmCuentasCampaniaPagos);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetCrmCuentasCampaniaPagos", new { id = crmCuentasCampaniaPagos.Id }, crmCuentasCampaniaPagos);
        //}

        //[HttpDelete("{id}")]
        //public async Task<ActionResult<CrmCuentasCampaniaPagos>> DeleteCrmCuentasCampaniaPagos(uint id)
        //{
        //    var crmCuentasCampaniaPagos = await _context.CrmCuentasCampaniaPagos.FindAsync(id);
        //    if (crmCuentasCampaniaPagos == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.CrmCuentasCampaniaPagos.Remove(crmCuentasCampaniaPagos);
        //    await _context.SaveChangesAsync();

        //    return crmCuentasCampaniaPagos;
        //}

        [HttpGet("GetPagos")]
        public async Task<ModeloRespuesta> GetPagos(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var pagosCliente = await _context.CrmCuentasCampaniaPagos
                        .Where(x => x.IdCuentaCampaniaProductoNavigation.IdCuentaCampaniaNavigation.Id ==
                                    idCuentaCampania)
                        .Select(x => new
                        {
                            obligacion = x.IdCuentaCampaniaProducto,
                            fecha = x.FechaPago,
                            valor = x.IdCuentaCampaniaAcuerdoNavigation.ValorAcuerdo,
                            valor_pagado = x.ValorPago,
                            tipo_pago = x.TipoPago,
                            no_recibo = x.NumeroRecibo,
                            cuota = x.Cuota,
                            plazo = x.Plazo,
                            entidad = string.Empty,
                            medio_pago = x.MedioPago,
                            campania = x.IdCuentaCampaniaProductoNavigation.IdCuentaCampaniaNavigation.IdCampaniaNavigation.Nombre,
                            fechaPago = x.FechaPago

                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = pagosCliente
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        private bool CrmCuentasCampaniaPagosExists(uint id)
        {
            return _context.CrmCuentasCampaniaPagos.Any(e => e.Id == id);
        }

        // ---------------------------------------MATRIZ PAGOS--------------------------------------- 
        //                                   PAGOS POR ASIGNACION 
        [HttpGet("GetPagosXasignacion")]
        public async Task<ModeloRespuesta> GetPagosXasignacion(int IdCampania, DateTime fechainicio, DateTime fechafin)
        {
            List<CrmMatrizPagos> consultaPagosXasignacion;
            try
            {
                _matrizConciliacion = new Matriz_Conciliacion(_context);
                consultaPagosXasignacion = await _matrizConciliacion.GetPagosXasignacion(IdCampania, fechainicio, fechafin);
                var ListaGrupoPagosXasignacion = consultaPagosXasignacion.GroupBy(a => a.Agente)
                                                    .Select(a => new
                                                    {
                                                        a.Key,
                                                        nopagoacuerdos = a.Count(pa => pa.Pagoconacuerdo>0),
                                                        pagoconacuerdos = a.Sum(pa => pa.Pagoconacuerdo),
                                                        nopagosinacuerdos = a.Count(pa => pa.Pagoconacuerdo.Equals(null)),
                                                        pagosinacuerdos = a.Sum(pa => pa.Pagocargado),
                                                        pagosactualizados = a.Count(pa => pa.Nodocumento.Length>0),
                                                        saldodeuda = a.Sum(pa => pa.Saldodeuda),
                                                        saldovencido = a.Sum(pa => pa.Saldovencido)
                                                    }).ToList();
                var Listas = new {consultaPagosXasignacion,ListaGrupoPagosXasignacion};

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = Listas
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
