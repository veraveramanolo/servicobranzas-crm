using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasEmailController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmCuentasEmailController(ps_crmContext context)
        {
            _context = context;
        }


        // GET: api/CrmCuentasEmail/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasEmail>> GetCrmCuentasEmail(uint id)
        {
            var crmCuentasEmail = await _context.CrmCuentasEmail.FindAsync(id);

            if (crmCuentasEmail == null)
            {
                return NotFound();
            }

            return crmCuentasEmail;
        }




       

        private bool CrmCuentasEmailExists(uint id)
        {
            return _context.CrmCuentasEmail.Any(e => e.Id == id);
        }


        //Consulta Emails por cuenta.
        //GET: api/CrmCuentasDireccion/GetEmail

        [HttpGet("GetEmail")]
        public async Task<ModeloRespuesta> GetEmail(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x=>x.Id == idCuentaCampania))
                {
                    var emailCliente = new object();
                    var cuenta = _context.CrmCuentasCampania
                        .FirstOrDefault(x => x.Id == idCuentaCampania).IdCuenta;
                    if (cuenta != null)
                    {
                        emailCliente = await _context.CrmCuentasEmail
                        .Where(x => x.IdCuenta == cuenta)
                        .OrderByDescending(x => x.Id)
                        .ThenBy(x => x.Estado)
                        .Select(x => new
                        {
                            idEmail = x.Id,
                            email = x.Email,
                            tipo = x.Tipo,
                            informacion = x.Info,
                            estado = x.Estado
                        })
                        .ToListAsync();
                    }
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = emailCliente
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //Guarda Email por cuenta.
        // api/CrmCuentasEmail/
        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmCuentasEmail(CrmCuentasEmail crmCuentasEmail)
        {
            try
            {
                crmCuentasEmail.FechaCreacion = DateTime.Now;
                _context.CrmCuentasEmail.Add(crmCuentasEmail);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Email Creado",
                Data = await _context.CrmCuentasEmail.FindAsync(crmCuentasEmail.Id)
            };

        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmCuentasEmail(CrmCuentasEmail crmCuentasEmail)
        {
            try
            {
                if (!CrmCuentasEmailExists(crmCuentasEmail.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Email que desea modificar no existe"
                    };
                }

                var crmCuentasEmailModificar = _context.CrmCuentasEmail.FirstOrDefault(x => x.Id == crmCuentasEmail.Id);
                if(!String.IsNullOrEmpty(crmCuentasEmail.Email))
                    crmCuentasEmailModificar.Email = crmCuentasEmail.Email;
                if(!String.IsNullOrEmpty(crmCuentasEmail.Tipo))
                    crmCuentasEmailModificar.Tipo = crmCuentasEmail.Tipo;
                if(!String.IsNullOrEmpty(crmCuentasEmail.Info))
                    crmCuentasEmailModificar.Info = crmCuentasEmail.Info;
                if(!String.IsNullOrEmpty(crmCuentasEmail.Estado))
                    crmCuentasEmailModificar.Estado = crmCuentasEmail.Estado;
                crmCuentasEmailModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Email Modificado"
            };
        }

        //Inactivar email por cuenta.
        // api/CrmCuentasEmail/Inactivar
        [HttpPost("Inactivar")]
        public async Task<ModeloRespuesta> InactivarCrmCuentasEmail(CrmCuentasEmail crmCuentasEmail)
        {
            try
            {
                if (!CrmCuentasEmailExists(crmCuentasEmail.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Email que desea inactivar no existe"
                    };
                }

                var CrmCuentasEmailInactivar = _context.CrmCuentasEmail.FirstOrDefault(x => x.Id == crmCuentasEmail.Id);
                
                if (!String.IsNullOrEmpty(crmCuentasEmail.Estado))
                    CrmCuentasEmailInactivar.Estado = crmCuentasEmail.Estado;
                if (!String.IsNullOrEmpty(crmCuentasEmail.ObservacionI))
                    CrmCuentasEmailInactivar.ObservacionI = crmCuentasEmail.ObservacionI;
                CrmCuentasEmailInactivar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Email Inactivo"
            };
        }

        // api/CrmCuentasEmail/BusquedaEmail
        [HttpGet("BusquedaEmail")]
        public async Task<ModeloRespuesta> BusquedaEmail(string filtemail)
        {
            try
            {
                if (_context.CrmCuentasEmail.Any(x => x.Email.Contains(filtemail)))
                {

                    _decoder = new Decoder();
                    uint idusuario = _decoder.GetDecoder(HttpContext);
                    // var usercontroller = new AclUsuarioController(_context);
                    // var usuario = usercontroller.UsuarioAclUsuario(idusuario).Result.Data;
                    Console.WriteLine("idusuario ");
                    Console.WriteLine(idusuario);
                    var admin = _context.AclUsuario.Where(a => a.IdUsuario == idusuario).Select(a => a.IdRolNavigation.IdRol).FirstOrDefault();
                    Console.WriteLine("admin ");
                    Console.WriteLine(admin);
                    if (admin.Equals(1)  || admin.Equals(3))
                    {
                        var busqueda = from email in _context.CrmCuentasEmail 
                                        join c in _context.CrmCuentas on email.IdCuenta equals c.Id 
                                        join cc in _context.CrmCuentasCampania on c.Id equals cc.IdCuenta 
                                        join cg in _context.CrmCuentasCampaniaGestor on cc.Id equals cg.IdCuentaCampania into ccg
                                        from left in ccg.DefaultIfEmpty() 
                                        join p in _context.CrmCuentasCampaniaProductos on cc.Id equals p.IdCuentaCampania 
                                        where email.Email.Contains(filtemail) 
                                        select new {
                                            idcuentacampania = cc.Id,
                                            idcuenta = c.Id,
                                            documento = c.Identificacion,
                                            nombre = c.Nombre,
                                            asesor = (left.Estado == "A" ? left.IdUsuarioNavigation.UserName : ""),
                                            campania = cc.IdCampaniaNavigation.Nombre,
                                            cliente = cc.IdCampaniaNavigation.IdCarteraNavigation.Nombre,
                                            idproducto = p.Id,
                                            nomproducto = p.Descripcion
                                        };

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = busqueda.Distinct().OrderBy(x => x.nombre)

                        };
                    }else
                    {
                        var busqueda = await _context.CrmCuentasEmail
                            .Where(x => x.Email.Contains(filtemail))
                            .Join(_context.CrmCuentas, email => email.IdCuenta, cuenta => cuenta.Id,
                            (email, cuenta) => new {email, cuenta})
                            .Join(_context.CrmCuentasCampania, infocliente => infocliente.cuenta.Id, cuentacampania => cuentacampania.IdCuenta,
                            (infocliente, cuentacampania) => new {infocliente, cuentacampania})
                            .Join(_context.CrmCartera, infocuenta => infocuenta.cuentacampania.IdCampaniaNavigation.IdCartera, cartera => cartera.Id,
                            (infocuenta, cartera) => new {infocuenta, cartera})
                            .Join(_context.CrmCuentasCampaniaGestor, infocartera => infocartera.infocuenta.cuentacampania.Id, gestor => gestor.IdCuentaCampania,
                            (infocartera, gestor) => new {infocartera, gestor})
                            .Join(_context.CrmCuentasCampaniaProductos, infogeneral => infogeneral.gestor.IdCuentaCampania, producto => producto.IdCuentaCampania,
                            (infogeneral, producto) => new {infogeneral, producto})
                            .Select(x => new
                            {
                                idcuentacampania = x.infogeneral.infocartera.infocuenta.cuentacampania.Id,
                                idcuenta = x.infogeneral.infocartera.infocuenta.infocliente.cuenta.Id,
                                documento = x.infogeneral.infocartera.infocuenta.infocliente.cuenta.Identificacion,
                                nombre = x.infogeneral.infocartera.infocuenta.infocliente.cuenta.Nombre,
                                asesor = x.infogeneral.gestor.IdUsuarioNavigation.UserName,
                                campania = x.infogeneral.infocartera.infocuenta.cuentacampania.IdCampaniaNavigation.Nombre,
                                cliente = x.infogeneral.infocartera.cartera.Nombre,
                                idproducto = x.producto.Id,
                                nomproducto = x.producto.Descripcion

                            }).Distinct().OrderBy(x => x.nombre).ToListAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = busqueda

                        };
                    }
                    
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
