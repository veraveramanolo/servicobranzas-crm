using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaProductosController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasCampaniaProductosController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentasCampaniaProductos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasCampaniaProductos>>> GetCrmCuentasCampaniaProductos()
        {
            return await _context.CrmCuentasCampaniaProductos.ToListAsync();
        }

        [HttpGet("GetProductos")]
        public async Task<ModeloRespuesta> GetProductos(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {

                    var productosClientes = await _context.CrmCuentasCampaniaProductos
                        .Where(x => x.IdCuentaCampania == idCuentaCampania)
                        .Select(x => new
                        {
                            cuenta_producto = x.Id,
                            valor_pagar = x.Valor,
                            edadmor = x.Edadmor,
                            producto = x.Descripcion,
                            campania = x.IdCuentaCampaniaNavigation.IdCampania,
                            cartera = x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.IdCartera

                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = productosClientes

                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }


        /*

        // GET: api/GetProductos obtiene productos por campaña
        [HttpGet("GetProductos")]
        public async Task<ModeloRespuesta> GetCrmCuentasCampaniaProductos(uint id)
        {
            try
            {
                var CuentasCampaniaProductos = await _context.CrmCuentasCampaniaProductos.Where(x => x.IdCuentaCampania == id).ToListAsync();

                if (CuentasCampaniaProductos == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Producto no encontrado"
                    };
                }
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = CuentasCampaniaProductos
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // GET: api/CrmCuentasCampaniaProductos/5
        /*[HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaProductos>> GetCrmCuentasCampaniaProductos(uint id)
        {
            var crmCuentasCampaniaProductos = await _context.CrmCuentasCampaniaProductos.FindAsync(id);

            if (crmCuentasCampaniaProductos == null)
            {
                return NotFound();
            }

            return crmCuentasCampaniaProductos;
        }

        // PUT: api/CrmCuentasCampaniaProductos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentasCampaniaProductos(uint id, CrmCuentasCampaniaProductos crmCuentasCampaniaProductos)
        {
            if (id != crmCuentasCampaniaProductos.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentasCampaniaProductos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasCampaniaProductosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentasCampaniaProductos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasCampaniaProductos>> PostCrmCuentasCampaniaProductos(CrmCuentasCampaniaProductos crmCuentasCampaniaProductos)
        {
            _context.CrmCuentasCampaniaProductos.Add(crmCuentasCampaniaProductos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasCampaniaProductos", new { id = crmCuentasCampaniaProductos.Id }, crmCuentasCampaniaProductos);
        }

        // DELETE: api/CrmCuentasCampaniaProductos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaProductos>> DeleteCrmCuentasCampaniaProductos(uint id)
        {
            var crmCuentasCampaniaProductos = await _context.CrmCuentasCampaniaProductos.FindAsync(id);
            if (crmCuentasCampaniaProductos == null)
            {
                return NotFound();
            }

            _context.CrmCuentasCampaniaProductos.Remove(crmCuentasCampaniaProductos);
            await _context.SaveChangesAsync();

            return crmCuentasCampaniaProductos;
        }
        */
        private bool CrmCuentasCampaniaProductosExists(uint id)
        {
            return _context.CrmCuentasCampaniaProductos.Any(e => e.Id == id);
        }

        // api/CrmCuentasCampaniaProductos/GetDeudasCarteras
        [HttpGet("GetDeudasCarteras")]
        public async Task<ModeloRespuesta> GetDeudasCarteras(uint idCuenta, uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.IdCuenta == idCuenta))
                {

                    var deudasCartera = await _context.CrmCuentasCampaniaProductos
                        .Where(x => x.IdCuentaCampaniaNavigation.IdCuenta == idCuenta
                        && !(x.IdCuentaCampania.Equals(idCuentaCampania)))
                        .Select(x => new
                        {
                            idCuentaCampania = x.IdCuentaCampania,
                            valor_pagar = x.Valor,
                            campania = x.IdCuentaCampaniaNavigation.IdCampaniaNavigation.Nombre,
                            edadmor = x.Edadmor,
                            deudaTotal = x.Deudatotal,
                            valorVencer = x.Valorporvencer,
                            producto = x.Descripcion
                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = deudasCartera

                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
