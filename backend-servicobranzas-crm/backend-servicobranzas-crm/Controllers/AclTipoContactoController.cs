using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclTipoContactoController : Controller
    {
        private readonly ps_crmContext _context;

        public AclTipoContactoController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclTipoContacto()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclTipoContacto.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearAclTipoContacto(AclTipoContacto aclTipoContacto)
        {
            try
            {
                aclTipoContacto.Texto = aclTipoContacto.Texto;
                _context.AclTipoContacto.Add(aclTipoContacto);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Contacto Creado",
                Data = await _context.AclTipoContacto.FindAsync(aclTipoContacto.IdTipoContacto)
            };
        }


      
        private bool AclTipoContactoExists(uint id)
        {
            return _context.AclTipoContacto.Any(e => e.IdTipoContacto== id);
        }

        //Obtener Lista de Tipos de Contacto
        // api/AclTipoContacto/PlantillasTipo
        [HttpGet("GetAclTipoContacto")]
        public async Task<ModeloRespuesta> GetAclTipoContacto()
        {
            try
            {
                var aclTipoContacto = await _context.AclTipoContacto
                                        .OrderBy(x => x.IdTipoContacto)
                                        .ToListAsync();

                if(aclTipoContacto.Any()){
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = aclTipoContacto
                    };
                }else{
                    return new ModeloRespuesta()
                    {
                        Data = aclTipoContacto,
                        Exito = 0,
                        Mensage = "No existen Tipos de Contacto Configurados"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
