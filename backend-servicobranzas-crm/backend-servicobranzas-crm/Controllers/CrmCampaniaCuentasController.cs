﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
using backend_servicobranzas_crm.Services.Interface;
using Microsoft.EntityFrameworkCore;


namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCampaniaCuentasController : Controller
    {
        private Campania_Gestion _campaniaGestion;
        private readonly ps_crmContext _context;

        public CrmCampaniaCuentasController(ps_crmContext context)
        {
            _context = context;
        }


        [HttpGet("GetCuentas")]
        public async Task<ModeloRespuesta> GetCuentasCampania(int idCampania, int idUsuario)
        {
            List<CrmConsultaGestionCampania> consultaCampaniaGestions;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                consultaCampaniaGestions = await _campaniaGestion.GetCuentasCampania(idCampania, idUsuario);
                // -----------------------------INSERTAR EN TBL CRM_CONSULTA_GESTION_CAMPANIA_DATA
                // CrmConsultaGestionCampaniaDataController data = new CrmConsultaGestionCampaniaDataController(_context);
                // var res = data.InsertaBD(consultaCampaniaGestions);

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Data = consultaCampaniaGestions
            };
        }

        // api/CrmArbolGestion/GetBDfiltro?
        [HttpGet("GetBDfiltro")]
        public async Task<ModeloRespuesta> GetBDfiltrada(int idCampania, int idUsuario, string descripcion)
        {
            List<CrmConsultaGestionCampania> bdfiltrada;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                bdfiltrada = await _campaniaGestion.GetBDfiltrada(idCampania, idUsuario, descripcion);
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Data = bdfiltrada
            };
        }

        [HttpGet("GetDetalleCliente")]
        public async Task<ModeloRespuesta> GetDetalleCliente(uint idCuentaCampania)
        {
            List<ModeloDetalleCliente> ListdetalleCliente = new List<ModeloDetalleCliente>();
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {

                    ListdetalleCliente = await _context.CrmCuentasCampania.Where(x => x.Id == idCuentaCampania)
                        .Select(x => new ModeloDetalleCliente()
                        {
                            idCuenta = x.IdCuenta,
                            nombre = x.IdCuentaNavigation.Nombre,
                            apellido = string.Empty,
                            cedula = x.IdCuentaNavigation.Identificacion,
                            estado = x.CrmGestion.Where(x => x.IdCuentaCampania == idCuentaCampania)
                                        .OrderByDescending(x => x.IdArbolDecisionNavigation.Peso)
                                        .Select(a => a.IdArbolDecisionNavigation.LabelNivel2).FirstOrDefault() ?? 
                                        x.CrmGestion.Where(x => x.IdCuentaCampania == idCuentaCampania)
                                        .OrderByDescending(x => x.IdArbolDecisionNavigation.Peso)
                                        .Select(a => a.IdArbolDecisionNavigation.Descripcion).FirstOrDefault(),
                            contacto = x.CrmGestion.Where(x => x.IdCuentaCampania == idCuentaCampania)
                                        .OrderByDescending(x => x.IdArbolDecisionNavigation.Peso)
                                        .Select(a => a.IdArbolDecisionNavigation.IdTipoContactoNavigation.Texto).FirstOrDefault(),
                            labelestado = x.CrmGestion.Where(x => x.IdCuentaCampania == idCuentaCampania)
                                        .OrderByDescending(x => x.IdArbolDecisionNavigation.Peso)
                                        .Select(a => a.IdArbolDecisionNavigation.LabelEstado).FirstOrDefault(),
                            tipoTelefono = x.IdCuentaNavigation.CrmCuentasTelefono.Where(a => a.IdCuenta == x.IdCuenta)
                                .Select(a => a.Tipo).FirstOrDefault(),
                            telefono = x.IdCuentaNavigation.CrmCuentasTelefono.Where(a => a.IdCuenta == x.IdCuenta)
                                .Select(a => a.Valor).FirstOrDefault(),
                            campania = x.IdCampaniaNavigation.Nombre,
                            deuda = x.CrmCuentasCampaniaProductos.Where(a => a.IdCuentaCampania == idCuentaCampania)
                                .Select(a => a.Valor).FirstOrDefault(),
                            deudatotal = x.CrmCuentasCampaniaProductos.Where(a => a.IdCuentaCampania == idCuentaCampania)
                                .Select(a => a.Deudatotal).FirstOrDefault(),
                            porvencer = x.CrmCuentasCampaniaProductos.Where(a => a.IdCuentaCampania == idCuentaCampania)
                                .Select(a => a.Valorporvencer).FirstOrDefault(),
                            pago = x.CrmCuentasCampaniaProductos.Where(a => a.IdCuentaCampania == idCuentaCampania)
                                .Join(_context.CrmCuentasCampaniaPagos, productos => productos.Id, pagos => pagos.IdCuentaCampaniaProducto,
                                    (productos, pagos) => new
                                    {
                                        pagos.ValorPago
                                    }).Sum(x => x.ValorPago).GetValueOrDefault()

                        })
                        .ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = ListdetalleCliente
                        
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados",
                Data = ListdetalleCliente
            };
        }

        // api/CrmArbolGestion/GetEstados?
        [HttpGet("GetEstados")]
        public async Task<ModeloRespuesta> GetListaEstados(int idCampania, int idUsuario)
        {
            List<AclEstados> listaestados;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                listaestados = await _campaniaGestion.GetListaEstados(idCampania, idUsuario);
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Data = listaestados
            };
        }

        [HttpGet("GetMulticanal")]
        public async Task<ModeloRespuesta> GetCuentasMulticanal(int idCuentaCampania)
        {
            List<CrmMulticanal> consultaCuentasMulticanal;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                consultaCuentasMulticanal = await _campaniaGestion.GetCuentasMulticanal(idCuentaCampania);
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Data = consultaCuentasMulticanal
            };
        }

        [HttpGet("GetMulticanalFecha")]
        public async Task<ModeloRespuesta> GetCuentasMulticanalFiltroFecha(int idCampania, DateTime fechafiltro)
        {
            List<CrmMulticanal> consultaCuentasMulticanal;
            try
            {
                _campaniaGestion = new Campania_Gestion(_context);
                consultaCuentasMulticanal = await _campaniaGestion.GetCuentasMulticanalFiltroFecha(idCampania, fechafiltro);
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Data = consultaCuentasMulticanal
            };
        }
    }
}
