using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaDetalleHistoricoController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public CrmCuentasCampaniaDetalleHistoricoController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("GetHistoricoCliente")]
        public async Task<ModeloRespuesta> GetHistoricoCliente(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var qc = _context.CrmGestion
                        .Where(x => x.IdCuentaCampania == idCuentaCampania)
                        .Join(_context.CrmArbolGestion, ges => ges.IdArbolDecision, a => a.Id,
                        (ges,a) => new {ges,a})
                        .OrderByDescending(x => x.ges.HoraGestion).Select(x => new {x.a.IdParent, x.a.Descripcion,x.ges.IdArbolDecision}).ToList();

                    uint? idparent;
                    List<string> todo = new List<string>();

                     var historico = await _context.CrmGestion
                        .Where(x => x.IdCuentaCampania == idCuentaCampania)
                        .OrderByDescending(x => x.HoraGestion)
                        .Select(x => new
                        {
                            fecha = x.HoraGestion,
                            destino = x.IdTelefonoNavigation.Valor,
                            contacto = x.IdArbolDecisionNavigation.LabelEstado,
                            estado = (x.IdArbolDecisionNavigation.LabelNivel2 ?? x.IdArbolDecisionNavigation.Descripcion),
                            canal = x.TipoGestion,
                            agente = x.IdGestorNavigation.UserName,
                            observaciones = " Obs: "+x.DetalleGestion
                        }).ToListAsync();

                    foreach (var item in qc)
                    {
                        var id = item.IdArbolDecision;
                        Console.WriteLine(id);
                        idparent = id;
                        List<string> descripcion = new List<string>();
                        do
                        {
                            var query =_context.CrmArbolGestion.Where(x => x.Id == idparent).Select(x => new {x.IdParent, x.Descripcion}).FirstOrDefault();
                            idparent = query.IdParent;
                            descripcion.Add(query.Descripcion+",");
                            Console.WriteLine(query.Descripcion);
                        } while (!idparent.Equals(null));
                        todo.Add(string.Concat(descripcion));
                        Console.WriteLine(string.Concat(descripcion));
                        Console.WriteLine("\n");
                    }

                    var contj= 0;
                    List<historicodet> results = new List<historicodet>();
                    
                    foreach (var j in historico)
                    {
                        contj ++;
                        Console.WriteLine("----------");
                        var contk= 0;
                            foreach (var k in todo)
                            {
                                contk ++;
                                if(contj-1 == contk-1)
                                {
                                    historicodet data = new historicodet();
                                    var numj = Array.IndexOf(historico.ToArray(),j);
                                    var unido = $"{k}{j.observaciones}";
                                    Console.WriteLine(unido);
                                    data.fecha = j.fecha;
                                    data.destino = j.destino;
                                    data.contacto = j.contacto;
                                    data.estado = j.estado;
                                    data.canal = j.canal;
                                    data.agente = j.agente;
                                    data.observaciones = unido;
                                    results.Add(data);
                                }
                            }
                        Console.WriteLine("----------");
                    }

                    // var historicoCliente = new { historico, todo};   

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = results

                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //[HttpGet("GetHistoricoCliente")]
        //public async Task<ModeloRespuesta> GetHistoricoCliente(uint idCuentaCampania)
        //{
        //    try
        //    {
        //        if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
        //        {
        //            var historicoCliente = await _context.CrmCuentasCampania
        //                .Where(x => x.Id == idCuentaCampania)
        //                .Select(x => new
        //                {
        //                    fecha = string.Empty,
        //                    destino = x.IdCuentaNavigation.CrmCuentasTelefono.Where(a => a.IdCuenta == x.IdCuenta)
        //                        .Select(a => a.Valor).FirstOrDefault(),
        //                    contacto = x.IdArbolGestionMejorGestionNavigation.Descripcion,
        //                    estado = x.IdArbolGestionMejorGestionNavigation.IdTipoContactoNavigation.Texto,
        //                    canal = string.Empty,
        //                    agente = x.CrmCuentasCampaniaGestor
        //                        .Where(s=>s.IdCuentaCampania == x.Id)
        //                        .Select(s => s.IdUsuarioNavigation.UserName).FirstOrDefault(),
        //                    observaciones = x.CrmCuentasCampaniaProductos
        //                        .Where(s => s.IdCuentaCampania == x.Id)
        //                        .Join(_context.CrmGestion, cccp => cccp.Id,
        //                        cg => cg.IdCuentasCampaniaProductos, (cccp,cg) => new {cg.DetalleGestion}).FirstOrDefault().DetalleGestion
        //                }).ToListAsync();

        //            return new ModeloRespuesta()
        //            {
        //                Exito = 1,
        //                Data = historicoCliente

        //            };
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new ModeloRespuesta()
        //        {
        //            Exito = 0,
        //            Mensage = "Error inesperado",
        //            Data = new string(e.Message)
        //        };
        //    }

        //    return new ModeloRespuesta()
        //    {
        //        Exito = 0,
        //        Mensage = "Resultados no encontrados"
        //    };
        //}



        /*
        // GET: api/CrmCuentasCampaniaDetalleHistorico
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentasCampaniaDetalleHistorico>>> GetCrmCuentasCampaniaDetalleHistorico()
        {
            return await _context.CrmCuentasCampaniaDetalleHistorico.ToListAsync();
        }

        // GET: api/CrmCuentasCampaniaDetalleHistorico/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaDetalleHistorico>> GetCrmCuentasCampaniaDetalleHistorico(uint id)
        {
            var crmCuentasCampaniaDetalleHistorico = await _context.CrmCuentasCampaniaDetalleHistorico.FindAsync(id);

            if (crmCuentasCampaniaDetalleHistorico == null)
            {
                return NotFound();
            }

            return crmCuentasCampaniaDetalleHistorico;
        }

        // PUT: api/CrmCuentasCampaniaDetalleHistorico/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentasCampaniaDetalleHistorico(uint id, CrmCuentasCampaniaDetalleHistorico crmCuentasCampaniaDetalleHistorico)
        {
            if (id != crmCuentasCampaniaDetalleHistorico.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentasCampaniaDetalleHistorico).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasCampaniaDetalleHistoricoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentasCampaniaDetalleHistorico
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentasCampaniaDetalleHistorico>> PostCrmCuentasCampaniaDetalleHistorico(CrmCuentasCampaniaDetalleHistorico crmCuentasCampaniaDetalleHistorico)
        {
            _context.CrmCuentasCampaniaDetalleHistorico.Add(crmCuentasCampaniaDetalleHistorico);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentasCampaniaDetalleHistorico", new { id = crmCuentasCampaniaDetalleHistorico.Id }, crmCuentasCampaniaDetalleHistorico);
        }

        // DELETE: api/CrmCuentasCampaniaDetalleHistorico/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentasCampaniaDetalleHistorico>> DeleteCrmCuentasCampaniaDetalleHistorico(uint id)
        {
            var crmCuentasCampaniaDetalleHistorico = await _context.CrmCuentasCampaniaDetalleHistorico.FindAsync(id);
            if (crmCuentasCampaniaDetalleHistorico == null)
            {
                return NotFound();
            }

            _context.CrmCuentasCampaniaDetalleHistorico.Remove(crmCuentasCampaniaDetalleHistorico);
            await _context.SaveChangesAsync();

            return crmCuentasCampaniaDetalleHistorico;
        }

        */
        private bool CrmCuentasCampaniaDetalleHistoricoExists(uint id)
        {
            return _context.CrmCuentasCampaniaDetalleHistorico.Any(e => e.Id == id);
        }
    }
}
