using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using System.IdentityModel.Tokens.Jwt;
using TimeZoneConverter;
using backend_servicobranzas_crm.Services.Class;


namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasCampaniaAcuerdosController : ControllerBase
    {
        private Decoder _decoder;
        private Matriz_Conciliacion _matrizConciliacion;
        private readonly ps_crmContext _context;
       // readonly ITokenAcquisition tokenAcquisition;

        public CrmCuentasCampaniaAcuerdosController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("GetAcuerdoPago")]
        public async Task<ModeloRespuesta> GetAcuerdoPago(uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var acuerdoPago = await _context.CrmCuentasCampaniaAcuerdos
                        .Where(x => x.IdCuentaCampaniaProductoNavigation.IdCuentaCampaniaNavigation.Id == idCuentaCampania)
                        .Select(x => new
                        {
                            idAcuerdo = x.Id,
                            obligacion = x.IdCuentaCampaniaProductoNavigation.Id,
                            fecha = x.FechaCreacion,
                            valor = x.ValorAcuerdo,
                            efecto = x.Efecto,
                            estado = x.Estado,
                            tipo = x.TipoAcuerdo,
                            cuota = x.Cuota,
                            plazo = x.Plazo,
                            probabilidad = x.Probabilidad,
                            comentario = x.Comentario,
                            hora = x.HoraAcuerdo,
                            fechaVisita = x.FechaVisita,
                            direccion = new { x.IdDireccionNavigation.Id, x.IdDireccionNavigation.Valor },
                            fechaAcuerdo = x.FechaAcuerdo
                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = acuerdoPago
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //Guarda un nuevo acuerdo de pago.
        [HttpPost("CrearAcuerdosPago")]
        public async Task<ModeloRespuesta> CrearAcuerdosPago(CrmCuentasCampaniaAcuerdos crmCuentasCampaniaAcuerdos)
        {
            try
            {
                //Determinar la zona para la conversión
                //var info = TZConvert.WindowsToIana("Pacific Standard Time");
                TimeZoneInfo info = TZConvert.GetTimeZoneInfo("America/Bogota");
                Console.WriteLine("\t {0}", info);
                //Creo el objeto DateTimeOffset, que recibe como parámetro el DateTime original y el objeto TimeZoneInfo
                DateTime dt_hora_pacifico = TimeZoneInfo.ConvertTime(Convert.ToDateTime(crmCuentasCampaniaAcuerdos.FechaVisita), info);
                _context.CrmCuentasCampaniaAcuerdos.Add(crmCuentasCampaniaAcuerdos);
                crmCuentasCampaniaAcuerdos.FechaCreacion = DateTime.Now;
                crmCuentasCampaniaAcuerdos.FechaVisita = dt_hora_pacifico;
                Console.WriteLine("\t {0}", dt_hora_pacifico);

                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                crmCuentasCampaniaAcuerdos.IdGestor = idusuario;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Acuerdo de Pago Creado",
                Data = await _context.AclUsuario.FindAsync(crmCuentasCampaniaAcuerdos.Id)
            };
        }


        [HttpPost("ModificarAcuerdosPago")]
        public async Task<ModeloRespuesta> ModificarAcuerdosPago(CrmCuentasCampaniaAcuerdos crmCuentasCampaniaAcuerdos)
        {
            try
            {
                if (!CrmCuentasCampaniaAcuerdosExists(crmCuentasCampaniaAcuerdos.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Acuerdo de pago que desea modificar no existe"
                    };
                }

                var crmAcuerdosPagoModificar = _context.CrmCuentasCampaniaAcuerdos.FirstOrDefault(x => x.Id == crmCuentasCampaniaAcuerdos.Id);
                if (crmCuentasCampaniaAcuerdos.FechaAcuerdo != null)
                    crmAcuerdosPagoModificar.FechaAcuerdo = crmCuentasCampaniaAcuerdos.FechaAcuerdo;
                if (!string.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.TipoAcuerdo))
                    crmAcuerdosPagoModificar.TipoAcuerdo = crmCuentasCampaniaAcuerdos.TipoAcuerdo;
                if (crmCuentasCampaniaAcuerdos.ValorAcuerdo != null)
                    crmAcuerdosPagoModificar.ValorAcuerdo = crmCuentasCampaniaAcuerdos.ValorAcuerdo;
                if (crmCuentasCampaniaAcuerdos.IdCuentaCampaniaProducto != null)
                    crmAcuerdosPagoModificar.IdCuentaCampaniaProducto = crmCuentasCampaniaAcuerdos.IdCuentaCampaniaProducto;
                if (crmCuentasCampaniaAcuerdos.IdGestor != null)
                    crmAcuerdosPagoModificar.IdGestor = crmCuentasCampaniaAcuerdos.IdGestor;
                if (!String.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.Efecto))
                    crmAcuerdosPagoModificar.Efecto = crmCuentasCampaniaAcuerdos.Efecto;
                if (!String.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.Estado))
                    crmAcuerdosPagoModificar.Estado = crmCuentasCampaniaAcuerdos.Estado;
                if (crmCuentasCampaniaAcuerdos.Cuota != null)
                    crmAcuerdosPagoModificar.Cuota = crmCuentasCampaniaAcuerdos.Cuota;
                if (!String.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.Plazo))
                    crmAcuerdosPagoModificar.Plazo = crmCuentasCampaniaAcuerdos.Plazo;
                if (!String.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.Probabilidad))
                    crmAcuerdosPagoModificar.Probabilidad = crmCuentasCampaniaAcuerdos.Probabilidad;
                if (!String.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.Comentario))
                    crmAcuerdosPagoModificar.Comentario = crmCuentasCampaniaAcuerdos.Comentario;
                if (crmCuentasCampaniaAcuerdos.HoraAcuerdo != null)
                    crmAcuerdosPagoModificar.HoraAcuerdo = crmCuentasCampaniaAcuerdos.HoraAcuerdo;
                if (crmCuentasCampaniaAcuerdos.FechaVisita != null)
                    crmAcuerdosPagoModificar.FechaVisita = crmCuentasCampaniaAcuerdos.FechaVisita;
                if (crmCuentasCampaniaAcuerdos.IdDireccion != null)
                    crmAcuerdosPagoModificar.IdDireccion = crmCuentasCampaniaAcuerdos.IdDireccion;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Acuerdo de pago Modificado"
            };
        }


        private bool CrmCuentasCampaniaAcuerdosExists(uint id)
        {
            return _context.CrmCuentasCampaniaAcuerdos.Any(e => e.Id == id);
        }

        // CrmCuentasCampaniaAcuerdos/GetAlertas
        [HttpGet("GetAlertas")]
        public async Task<ModeloRespuesta> GetAlertas()
        {
            try
            {
                if (_context.CrmCuentasCampaniaAcuerdos.Any(x => x.FechaVisita >= DateTime.Today && x.FechaVisita <= DateTime.Today.AddDays(2)))
                {
                    var sub = _context.CrmCuentasCampaniaAcuerdos
                                .Where(x => x.FechaVisita >= DateTime.Today && x.FechaVisita <= DateTime.Today.AddDays(2))
                                .Join(_context.CrmCuentasCampaniaProductos, acuerdo => acuerdo.IdCuentaCampaniaProducto, producto => producto.Id,
                                (acuerdo, producto) => new { acuerdo, producto })
                                .Join(_context.CrmCuentasCampania, acp => acp.producto.IdCuentaCampania, cuencamp => cuencamp.Id,
                                (acp, cuencamp) => new { acp, cuencamp })
                                .Join(_context.CrmGestion, acpcuencamp => acpcuencamp.cuencamp.Id, gestion => gestion.IdCuentaCampania,
                                (acpcuencamp, gestion) => new { acpcuencamp, gestion })
                                .Join(_context.CrmArbolGestion, acpgestion => acpgestion.gestion.IdArbolDecision, arbol => arbol.Id,
                                (acpgestion, arbol) => new { acpgestion, arbol })
                                .GroupBy(x => x.acpgestion.gestion.IdCuentaCampania)
                                .Select(x => x.Max(z => z.acpgestion.gestion.HoraGestion));

                    var alertas = await _context.CrmCuentasCampaniaAcuerdos
                                .Where(x => x.FechaVisita >= DateTime.Today && x.FechaVisita <= DateTime.Today.AddDays(2))
                                .Join(_context.CrmCuentasCampaniaProductos, acuerdo => acuerdo.IdCuentaCampaniaProducto, producto => producto.Id,
                                (acuerdo, producto) => new { acuerdo, producto })
                                .Join(_context.CrmCuentasCampania, acp => acp.producto.IdCuentaCampania, cuencamp => cuencamp.Id,
                                (acp, cuencamp) => new { acp, cuencamp })
                                .Join(_context.CrmGestion, acpcuencamp => acpcuencamp.cuencamp.Id, gestion => gestion.IdCuentaCampania,
                                (acpcuencamp, gestion) => new { acpcuencamp, gestion })
                                .Join(_context.CrmArbolGestion, acpgestion => acpgestion.gestion.IdArbolDecision, arbol => arbol.Id,
                                (acpgestion, arbol) => new { acpgestion, arbol })
                                .Join(_context.CrmCuentas, acparbol => acparbol.acpgestion.gestion.IdCuenta, cuenta => cuenta.Id,
                                (acparbol, cuenta) => new { acparbol, cuenta })
                                .Where(s => sub.Contains(s.acparbol.acpgestion.gestion.HoraGestion))
                        .Select(x => new
                        {
                            idAcuerdo = x.acparbol.acpgestion.acpcuencamp.acp.acuerdo.Id,
                            fechaVisita = x.acparbol.acpgestion.acpcuencamp.acp.acuerdo.FechaVisita,
                            nombre = x.cuenta.Nombre,
                            idarboldecision = x.acparbol.acpgestion.gestion.IdArbolDecision,
                            ultimagestion = x.acparbol.arbol.Descripcion,
                            idcuentacampania = x.acparbol.acpgestion.acpcuencamp.cuencamp.Id,
                            idproducto = x.acparbol.acpgestion.acpcuencamp.acp.producto.Id
                        }).OrderByDescending(x => x.fechaVisita).ToListAsync();

                    /*var subq = from a in _context.CrmCuentasCampaniaAcuerdos 
                            join p in _context.CrmCuentasCampaniaProductos on a.IdCuentaCampaniaProducto equals p.Id 
                            join i in _context.CrmCuentasCampania on p.IdCuentaCampania equals i.Id
                            join g in _context.CrmGestion on i.Id equals g.IdCuentaCampania 
                            join b in _context.CrmArbolGestion on g.IdArbolDecision equals b.Id 
                            join c in _context.CrmCuentas on g.IdCuenta equals c.Id 
                            where a.FechaVisita >= DateTime.Today && a.FechaVisita <= DateTime.Today.AddDays(2) 
                            && sub.Contains(g.HoraGestion) 
                            orderby a.FechaVisita
                            select new 
                                {a.FechaVisita, c.Nombre, g.IdArbolDecision, b.Descripcion};*/

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = alertas
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "No existen Alertas"
            };
        }


        // CrmCuentasCampaniaAcuerdos/GetAlertasHora
        [HttpGet("GetAlertasHora")]
        public async Task<ModeloRespuesta> GetAlertasHora()
        {
            try
            {
                if (_context.CrmCuentasCampaniaAcuerdos.Any(x => //x.HoraAcuerdo >= hora && x.HoraAcuerdo <= hora.Add(new TimeSpan(0, 5, 0)) &&
                x.FechaVisita >= DateTime.Now && x.FechaVisita <= DateTime.Now.AddMinutes(5) 
                && x.EstadoNot == "V"))
                {
                    var sub = _context.CrmCuentasCampaniaAcuerdos
                                .Where(x => x.FechaVisita >= DateTime.Now && x.FechaVisita <= DateTime.Now.AddMinutes(5))
                                .Join(_context.CrmCuentasCampaniaProductos, acuerdo => acuerdo.IdCuentaCampaniaProducto, producto => producto.Id,
                                (acuerdo, producto) => new { acuerdo, producto })
                                .Join(_context.CrmCuentasCampania, acp => acp.producto.IdCuentaCampania, cuencamp => cuencamp.Id,
                                (acp, cuencamp) => new { acp, cuencamp })
                                .Join(_context.CrmGestion, acpcuencamp => acpcuencamp.cuencamp.Id, gestion => gestion.IdCuentaCampania,
                                (acpcuencamp, gestion) => new { acpcuencamp, gestion })
                                .Join(_context.CrmArbolGestion, acpgestion => acpgestion.gestion.IdArbolDecision, arbol => arbol.Id,
                                (acpgestion, arbol) => new { acpgestion, arbol })
                                .GroupBy(x => x.acpgestion.gestion.IdCuentaCampania)
                                .Select(x => x.Max(z => z.acpgestion.gestion.HoraGestion));

                    var alertashora = await _context.CrmCuentasCampaniaAcuerdos
                                .Where(x => x.FechaVisita >= DateTime.Now && x.FechaVisita <= DateTime.Now.AddMinutes(5))
                                .Join(_context.CrmCuentasCampaniaProductos, acuerdo => acuerdo.IdCuentaCampaniaProducto, producto => producto.Id,
                                (acuerdo, producto) => new { acuerdo, producto })
                                .Join(_context.CrmCuentasCampania, acp => acp.producto.IdCuentaCampania, cuencamp => cuencamp.Id,
                                (acp, cuencamp) => new { acp, cuencamp })
                                .Join(_context.CrmGestion, acpcuencamp => acpcuencamp.cuencamp.Id, gestion => gestion.IdCuentaCampania,
                                (acpcuencamp, gestion) => new { acpcuencamp, gestion })
                                .Join(_context.CrmArbolGestion, acpgestion => acpgestion.gestion.IdArbolDecision, arbol => arbol.Id,
                                (acpgestion, arbol) => new { acpgestion, arbol })
                                .Join(_context.CrmCuentas, acparbol => acparbol.acpgestion.gestion.IdCuenta, cuenta => cuenta.Id,
                                (acparbol, cuenta) => new { acparbol, cuenta })
                                .Where(s => sub.Contains(s.acparbol.acpgestion.gestion.HoraGestion))
                        .Select(x => new
                        {
                            idAcuerdo = x.acparbol.acpgestion.acpcuencamp.acp.acuerdo.Id,
                            fechaVisita = x.acparbol.acpgestion.acpcuencamp.acp.acuerdo.FechaVisita,
                            nombre = x.cuenta.Nombre,
                            idarboldecision = x.acparbol.acpgestion.gestion.IdArbolDecision,
                            ultimagestion = x.acparbol.arbol.Descripcion,
                            idcampania = x.acparbol.acpgestion.gestion.IdCuentaCampania,
                            idproducto = x.acparbol.acpgestion.acpcuencamp.acp.producto.Id

                        }).OrderByDescending(x => x.fechaVisita).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = alertashora
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "No hay notificaciones"
            };
        }

        [HttpPost("ModificarAlertaNot")]
        public async Task<ModeloRespuesta> ModificarAlertaNot(CrmCuentasCampaniaAcuerdos crmCuentasCampaniaAcuerdos)
        {
            try
            {
                if (!CrmCuentasCampaniaAcuerdosExists(crmCuentasCampaniaAcuerdos.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Estado de Notificación que desea modificar no existe"
                    };
                }

                var crmAcuerdosPagoModificar = _context.CrmCuentasCampaniaAcuerdos.FirstOrDefault(x => x.Id == crmCuentasCampaniaAcuerdos.Id);
                
                if (!String.IsNullOrEmpty(crmCuentasCampaniaAcuerdos.EstadoNot))
                    if(crmCuentasCampaniaAcuerdos.EstadoNot == "F" || crmCuentasCampaniaAcuerdos.EstadoNot == "V"){
                        crmAcuerdosPagoModificar.EstadoNot = crmCuentasCampaniaAcuerdos.EstadoNot;
                    }

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Estado de Notificación Modificado"
            };
        }

        // ---------------------------------------MATRIZ ACUERDOS--------------------------------------- 
        //                                   ACUERDOS POR ASIGNACION 
        [HttpGet("GetAcuerdosXasignacion")]
        public async Task<ModeloRespuesta> GetAcuerdosXasignacion(int IdCampania, DateTime fechainicio, DateTime fechafin)
        {
            List<CrmMatrizAcuerdos> consultaAcuerdosXasignacion;
            try
            {
                _matrizConciliacion = new Matriz_Conciliacion(_context);
                consultaAcuerdosXasignacion = await _matrizConciliacion.GetAcuerdosXasignacion(IdCampania, fechainicio, fechafin);
                var ListaGrupoAcuerdosXasignacion = consultaAcuerdosXasignacion.GroupBy(a => a.Agenteasignado)
                                                    .Select(a => new
                                                    {
                                                        a.Key,
                                                        noacuerdos = a.Count(ac => ac.ValorAcuerdo>0),
                                                        valoracuerdos = a.Sum(ac => ac.ValorAcuerdo),
                                                        nopagos = a.Count(ac => ac.ValorPago>0),
                                                        valorpagos = a.Sum(ac => ac.ValorPago),
                                                        saldodeuda = a.Sum(ac => ac.Saldodeuda),
                                                        saldovencido = a.Sum(ac => ac.Saldovencido)
                                                    }).ToList();
                var Listas = new {consultaAcuerdosXasignacion,ListaGrupoAcuerdosXasignacion};

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = Listas
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        //                                   ACUERDOS POR GESTION 
        [HttpGet("GetAcuerdosXgestion")]
        public async Task<ModeloRespuesta> GetAcuerdosXgestion(int IdCampania, DateTime fechainicio, DateTime fechafin)
        {
            List<CrmMatrizAcuerdos> consultaAcuerdosXgestion;
            try
            {
                _matrizConciliacion = new Matriz_Conciliacion(_context);
                consultaAcuerdosXgestion = await _matrizConciliacion.GetAcuerdosXgestion(IdCampania, fechainicio, fechafin);
                var ListaGrupoAcuerdosXgestion = consultaAcuerdosXgestion.GroupBy(a => a.Agenteasignado)
                                                    .Select(a => new
                                                    {
                                                        a.Key,
                                                        noacuerdos = a.Count(ac => ac.ValorAcuerdo>0),
                                                        valoracuerdos = a.Sum(ac => ac.ValorAcuerdo),
                                                        nopagos = a.Count(ac => ac.ValorPago>0),
                                                        valorpagos = a.Sum(ac => ac.ValorPago),
                                                        saldodeuda = a.Sum(ac => ac.Saldodeuda),
                                                        saldovencido = a.Sum(ac => ac.Saldovencido)
                                                    }).ToList();
                var Listas = new {consultaAcuerdosXgestion,ListaGrupoAcuerdosXgestion};

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = Listas
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
