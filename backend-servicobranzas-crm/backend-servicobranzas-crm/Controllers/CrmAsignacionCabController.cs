using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using System.IdentityModel.Tokens.Jwt;
using System.Data.SqlClient;
using System.Transactions;
using backend_servicobranzas_crm.Services.Class;


namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmAsignacionCabController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmAsignacionCabController(ps_crmContext context)
        {
            _context = context;
        }


        // api/CrmAsignacionCab/InsertAsignaciones
        [HttpPost("InsertAsignaciones")]
        // public async Task<ModeloRespuesta> InsertAsignaciones(CrmAsignacionCab crmAsignacionCab)
        public async Task<MultiselectResponse> InsertAsignaciones(CrmAsignacionCab crmAsignacionCab)
        {
            // using (TransactionScope scope = new TransactionScope())
            using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)){
                
                
                // SqlTransaction transaction;

                var dataa = 0;
                try{  
                    
                    if(!crmAsignacionCab.Equals(null))
                    {
                        
                        CrmAsignacionCab listado = new CrmAsignacionCab();
                        listado.IdTarea = crmAsignacionCab.IdTarea;
                        listado.Tipoasignacion = crmAsignacionCab.Tipoasignacion;
                        listado.FechaCreacion = DateTime.Now;
                        _decoder = new Decoder();
                        uint idusuario = _decoder.GetDecoder(HttpContext);
                        listado.UserCreacion = Convert.ToInt32(idusuario);
                        _context.CrmAsignacionCab.Add(listado);
                        await _context.SaveChangesAsync();
                        Console.WriteLine("\t {0}",listado.IdAsignacion);

                        foreach(CrmAsignacionDet z in crmAsignacionCab.CrmAsignacionDet)
                             {
                                 CrmAsignacionDet data = new CrmAsignacionDet();
                                 data.IdAsignacion = listado.IdAsignacion;
                                 dataa =  listado.IdAsignacion;
                                 data.IdUsuario = z.IdUsuario;
                                 _context.CrmAsignacionDet.Add(data);
                             }
                    }

                    await _context.SaveChangesAsync();
                    if (dataa!= 0){
                        transaction.Complete();
                    }
                    
                }
                catch (Exception e)
                {
                    // trans.Rollback();
                    return new MultiselectResponse()
                    {
                        Exito = 0,
                        Mensage = "Error inesperado",
                        Data = new string(e.Message)
                    };
                }
                return new MultiselectResponse()
                {
                    Exito = 1,
                    Mensage = "Asignaciones Creadas",
                    Data = dataa,
                    Asesores = crmAsignacionCab.IdTarea
                };
                // scope.Complete();
            }
        }

        // api/CrmAsignacionCab/AsignaAsesor
        [HttpGet("AsignaAsesor")]
        // public async Task<ModeloRespuesta> AsignaAsesor(int idAsignacion)
        public async Task<ModeloRespuesta> AsignaAsesor(int idAsignacion, uint idTarea)
        {
            try
            {
                if (_context.CrmAsignacionCab.Any(x => x.IdAsignacion == idAsignacion))
                {
                    if (_context.CrmAsignacionCab.Where(x => x.IdAsignacion == idAsignacion)
                        .Join(_context.CrmTareas, asignacion => asignacion.IdTarea, tareas => tareas.Id,
                        (asignacion, tareas)  => new { asignacion, tareas }).Any())
                    {
                        var listaClientes = _context.CrmAsignacionCab.Where(x => x.IdAsignacion == idAsignacion)
                                            .Join(_context.CrmTareas, asignacion => asignacion.IdTarea, tareas => tareas.Id,
                                            (asignacion, tareas)  => new { asignacion, tareas })
                                            .Join(_context.CrmMulticanalDetalle, asigtarea => asigtarea.tareas.Idmulticanal, multdet => multdet.Idmulticanal,
                                            (asigtarea, multdet)  => new { asigtarea, multdet })
                                            .Join(_context.CrmCuentasCampania, atm => atm.multdet.Nocuenta, cc => cc.IdCuenta,
                                            (atm, cc)  => new { atm, cc })
                                            .Where(x => x.atm.asigtarea.tareas.Estado == "A")
                                            .Select(x => x.cc.Id).Distinct();

                        var listaAsesores = _context.CrmAsignacionCab.Where(x => x.IdAsignacion == idAsignacion)
                                            .Join(_context.CrmAsignacionDet, asignacion => asignacion.IdAsignacion, detalle => detalle.IdAsignacion,
                                            (asignacion, detalle)  => new { asignacion, detalle })
                                            .Select(x => x.detalle.IdUsuario);
                        var numClientes = listaClientes.Count();
                        var numAsesores = listaAsesores.Count();
                        var mod = numClientes % numAsesores;
                        var total = Convert.ToInt32(numClientes / numAsesores);
                        var cont = 0;
                        var cont2 = 0;
                        uint asesor = 0;
                        uint cliente = 0;
                        
                        for(int index=0; index < numAsesores; index++)
                        {
                            Console.WriteLine("\t {0}", listaAsesores.ToArray()[index]);
                            asesor = Convert.ToUInt32(listaAsesores.ToArray()[index]);
                            
                            if(index == numAsesores - 1)
                            {
                                total = total + mod;
                                Console.WriteLine("\t {0}", "total: ");
                                Console.WriteLine("\t {0}", total);
                                cont2 = cont2 + total;
                                Console.WriteLine("\t {0}", "cont2: ");
                                Console.WriteLine("\t {0}", cont2);
                            }else{
                                cont2 = cont2+total;
                                Console.WriteLine("\t {0}", "cont2 else: ");
                                Console.WriteLine("\t {0}", cont2);
                            }
                            for(int i = cont; i < cont2; i++)
                            {
                                Console.WriteLine("\t {0}", listaClientes.ToArray()[i]);
                                cliente = listaClientes.ToArray()[i];
                                // CrmCuentasCampaniaGestor data = new CrmCuentasCampaniaGestor();
                                // data.IdCuentaCampania = cliente;
                                // data.IdUsuario = asesor;
                                // data.Estado = "A";
                                // _context.CrmCuentasCampaniaGestor.Add(data);
                                CrmTareaGestor data = new CrmTareaGestor();
                                data.IdCuentaCampania = cliente;
                                data.IdTarea = idTarea;
                                data.IdUsuario = asesor;
                                _context.CrmTareaGestor.Add(data);

                            }
                            cont = cont+total;
                            Console.WriteLine("\t {0}", "cont: ");
                            Console.WriteLine("\t {0}", cont);
                            
                        }
                        await _context.SaveChangesAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = "Guardado exitoso"
                        };
                        
                    } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY TAREA CREADA PARA ESTA ASIGNACIÓN"
                    };
                    }
                } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO EXISTE LA ASIGNACIÓN " + idAsignacion
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        private bool CrmAsignacionCabExists(uint id)
        {
            return _context.CrmAsignacionCab.Any(e => e.IdAsignacion == id);
        }

        // api/CrmAsignacionCab/AsignacionAsesor
        // [HttpPost("AsignacionAsesor")]
        // public async Task<ModeloRespuesta> AsignacionAsesor(AclAsignacion aclAsignacion)
        // {
        //     try
        //     {
        //         if (!aclAsignacion.Asesores.Equals(null))
        //         {
        //             if (!aclAsignacion.clientes.Equals(null))
        //             {
        //                 var numAsesores = aclAsignacion.Asesores.Count();
        //                 var numClientes = aclAsignacion.clientes.Count();
        //                 Console.WriteLine("\t {0}", numAsesores);
        //                 Console.WriteLine("\t {0}", numClientes);

        //                 var mod = numClientes % numAsesores;
        //                 var total = Convert.ToInt32(numClientes / numAsesores);
        //                 var cont = 0;
        //                 var cont2 = 0;
        //                 var listaAsesores = aclAsignacion.Asesores.Select(x => x.IdUsuario);
        //                 var listaClientes = aclAsignacion.clientes.Select(x => x.Nocuenta);
                        
        //                 for(int index=0; index < numAsesores; index++)
        //                 {
        //                     Console.WriteLine("\t {0}", listaAsesores.ToArray()[index]);
        //                     var asesor = listaAsesores.ToArray()[index];
                            
        //                     if(index == numAsesores - 1)
        //                     {
        //                         total = total + mod;
        //                         Console.WriteLine("\t {0}", "total: ");
        //                         Console.WriteLine("\t {0}", total);
        //                         cont2 = cont2 + total;
        //                         Console.WriteLine("\t {0}", "cont2: ");
        //                         Console.WriteLine("\t {0}", cont2);
        //                     }else{
        //                         cont2 = cont2+total;
        //                         Console.WriteLine("\t {0}", "cont2 else: ");
        //                         Console.WriteLine("\t {0}", cont2);
        //                     }
        //                     for(int i = cont; i < cont2; i++)
        //                     {
        //                         Console.WriteLine("\t {0}", "cliente: ");
        //                         Console.WriteLine("\t {0}", listaClientes.ToArray()[i]);
        //                         var cliente = listaClientes.ToArray()[i];
        //                         var idcc = _context.CrmCuentasCampania.Where(x => x.IdCuenta == cliente)
        //                                    .Select(x => x.Id).FirstOrDefault();
        //                         Console.WriteLine("\t {0}", "idcc: ");
        //                         Console.WriteLine("\t {0}", idcc);
        //                         if(idcc!=0)
        //                         {
        //                             if(_context.CrmCuentasCampaniaGestor.Any(x => x.IdCuentaCampania == idcc))
        //                             {
        //                                 var crmAsignacion = _context.CrmCuentasCampaniaGestor.FirstOrDefault(x => x.IdCuentaCampania == idcc);
        //                                 crmAsignacion.IdUsuario = asesor;
        //                                 Console.WriteLine("\t {0}", "Update");
        //                                 Console.WriteLine("\t {0}", asesor);
        //                             }else{
        //                                 CrmCuentasCampaniaGestor data = new CrmCuentasCampaniaGestor();
        //                                 data.IdCuentaCampania = idcc;
        //                                 data.IdUsuario = asesor;
        //                                 data.Estado = "A";
        //                                 _context.CrmCuentasCampaniaGestor.Add(data);
        //                                 Console.WriteLine("\t {0}", "Insert");
        //                                 Console.WriteLine("\t {0}", asesor);
        //                             }
        //                             await _context.SaveChangesAsync();
        //                         }else{
        //                             return new ModeloRespuesta()
        //                             {
        //                                 Exito = 1,
        //                                 Data = "CLIENTE NO SE ENCUENTRA ASIGANDO A UNA CAMPAÑA"
        //                             };
        //                         }

        //                     }
        //                     cont = cont+total;
        //                     Console.WriteLine("\t {0}", "cont: ");
        //                     Console.WriteLine("\t {0}", cont);
                            
        //                 }

        //                 return new ModeloRespuesta()
        //                 {
        //                     Exito = 1,
        //                     Data = "Guardado exitoso"
        //                 };
                        
        //             } else {
        //             return new ModeloRespuesta()
        //             {
        //                 Exito = 1,
        //                 Data = "NO HAY CLIENTES PARA ASIGNAR"
        //             };
        //             }
        //         } else {
        //             return new ModeloRespuesta()
        //             {
        //                 Exito = 1,
        //                 Data = "NO HAY ASESORES"
        //             };
        //         }
        //     }
        //     catch (Exception e)
        //     {
        //         return new ModeloRespuesta()
        //         {
        //             Exito = 0,
        //             Mensage = "Error inesperado",
        //             Data = new string(e.Message)
        //         };
        //     }
        // }


        // api/CrmAsignacionCab/AsignacionAsesorManual
        [HttpPost("AsignacionAsesorManual")]
        public async Task<ModeloRespuesta> AsignacionAsesorManual(AclAsignacion aclAsignacion)
        {
            try
            {
                if (!aclAsignacion.Asesores.Equals(null))
                {
                    if (!aclAsignacion.clientes.Equals(null))
                    {
                        Console.WriteLine("AsignacionAsesorManual");
                        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(aclAsignacion));
                        var numClientes = aclAsignacion.clientes.Count();
                        Console.WriteLine("\t {0}", numClientes);
                        var numAsesores = aclAsignacion.Asesores.Count();
                        Console.WriteLine("\t {0}", numAsesores);
                        var contcliente = 0;

                        var listaAsesores = aclAsignacion.Asesores.Select(x => new {x.IdUsuario, x.Cantidad});
                        var listaClientes = aclAsignacion.clientes.Select(x => x.Nocuenta);
                        var total = 0;
                        
                        for(int index=0; index < numAsesores; index++)
                        {
                            Console.WriteLine("\t {0}", listaAsesores.ToArray()[index]);
                            var asesor = listaAsesores.ToArray()[index];
                            Console.WriteLine("\t {0}", asesor.Cantidad);
                            total = contcliente+asesor.Cantidad;
                            
                            for(int i = contcliente; i < total; i++)
                            {
                                Console.WriteLine("\t {0}", "cliente: ");
                                Console.WriteLine("\t {0}", listaClientes.ToArray()[i]);
                                var cliente = listaClientes.ToArray()[i];
                                var idcc = _context.CrmCuentasCampania.Where(x => x.IdCuenta == cliente && x.IdCampania == aclAsignacion.idcampania)
                                           .Select(x => x.Id).FirstOrDefault();
                                Console.WriteLine("\t {0}", "idcc: ");
                                Console.WriteLine("\t {0}", idcc);
                                if(idcc!=0)
                                {
                                    if(_context.CrmCuentasCampaniaGestor.Any(x => x.IdCuentaCampania == idcc))
                                    {
                                        var crmAsignacion = _context.CrmCuentasCampaniaGestor.FirstOrDefault(x => x.IdCuentaCampania == idcc);
                                        crmAsignacion.IdUsuario = asesor.IdUsuario;
                                        crmAsignacion.Estado = "A";
                                        Console.WriteLine("\t {0}", "Update");
                                        Console.WriteLine("\t {0}", asesor);
                                    }else{
                                        CrmCuentasCampaniaGestor data = new CrmCuentasCampaniaGestor();
                                        data.IdCuentaCampania = idcc;
                                        data.IdUsuario = asesor.IdUsuario;
                                        data.Estado = "A";
                                        _context.CrmCuentasCampaniaGestor.Add(data);
                                        Console.WriteLine("\t {0}", "Insert");
                                        Console.WriteLine("\t {0}", asesor);
                                    }
                                    await _context.SaveChangesAsync();
                                }else{
                                    return new ModeloRespuesta()
                                    {
                                        Exito = 1,
                                        Data = "CLIENTE NO SE ENCUENTRA ASIGANDO A UNA CAMPAÑA"
                                    };
                                }

                            }
                            contcliente = contcliente + asesor.Cantidad;
                            Console.WriteLine("\t {0}", "Siguiente i");
                            Console.WriteLine("\t {0}", contcliente);
                                                        
                        }

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = "Guardado exitoso"
                        };
                        
                    } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY CLIENTES PARA ASIGNAR"
                    };
                    }
                } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY ASESORES"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/CrmAsignacionCab/AsignacionAsesor
        [HttpPost("AsignacionAsesor")]
        public async Task<ModeloRespuesta> AsignacionAsesor(AclAsignacion aclAsignacion)
        {
            try
            {
                if (!aclAsignacion.Asesores.Equals(null))
                {
                    if (!aclAsignacion.clientes.Equals(null))
                    {
                        var numAsesores = aclAsignacion.Asesores.Count();
                        var numClientes = aclAsignacion.clientes.Count();
                        Console.WriteLine("Asesores", numAsesores);
                        Console.WriteLine("Clientes", numClientes);

                        var listaAsesores = aclAsignacion.Asesores.Select(x => x.IdUsuario);
                        var listaClientes = aclAsignacion.clientes.Select(x => new {x.Nocuenta, x.Deudatotal, x.Valorpago});
                        var sumvalores = 0.0;
                        
                        switch (aclAsignacion.Tipo)
                        {
                            case "Deudatotal":
                                Console.WriteLine("\t {0}", "Suma DedudaTotal");
                                sumvalores = Convert.ToDouble(aclAsignacion.clientes.Select(x => x.Deudatotal).Sum());
                                break;
                            case "Valorpago":
                                Console.WriteLine("\t {0}", "Suma Valorpago");
                                sumvalores = Convert.ToDouble(aclAsignacion.clientes.Select(x => x.Valorpago).Sum());
                                break;
                            case "Deudavencida": 
                                Console.WriteLine("\t {0}", "Suma Valorvencido");
                                sumvalores = Convert.ToDouble(aclAsignacion.clientes.Select(x => x.Deudavencida).Sum());
                                break;
                        }
                        Console.WriteLine("\t {0}", sumvalores);
                        Console.WriteLine("\t {0}", "totalmonto");
                        var totalmonto = sumvalores/numAsesores;
                        Console.WriteLine("\t {0}", totalmonto);
                        
                        var contcliente = 0;
                        double? montotipo;
                        
                        for(int index=0; index < numAsesores; index++)
                        {
                            
                            double? suma = 0.00;
                            var asesor = listaAsesores.ToArray()[index];

                                for(int i = contcliente; i < numClientes; i++)
                                {
                                    
                                    var cliente = listaClientes.ToArray()[i];
                                    Console.WriteLine("\t {0}", cliente);
                                    suma = suma + cliente.Deudatotal;
                                    contcliente = i;

                                    var idcc = _context.CrmCuentasCampania.Where(x => x.IdCuenta == cliente.Nocuenta && x.IdCampania == aclAsignacion.idcampania)
                                               .Select(x => x.Id).FirstOrDefault();

                                    if(suma > totalmonto && index != numAsesores-1)
                                    {
                                        break;
                                    }
                                    if(idcc!=0)
                                    {
                                        switch (aclAsignacion.Tipo)
                                        {
                                            case "Deudatotal":
                                                montotipo = cliente.Deudatotal;
                                                break;
                                            case "Valorpago":
                                                montotipo = cliente.Valorpago;
                                                break;
                                        }
                                        if(_context.CrmCuentasCampaniaGestor.Any(x => x.IdCuentaCampania == idcc))
                                        {
                                            var crmAsignacion = _context.CrmCuentasCampaniaGestor.FirstOrDefault(x => x.IdCuentaCampania == idcc);
                                            crmAsignacion.IdUsuario = asesor;
                                            crmAsignacion.Estado = "A";
                                            Console.WriteLine("\t {0}", "Update ----------------");
                                            Console.WriteLine("\t {0}", cliente.Nocuenta);
                                            Console.WriteLine("\t {0}", asesor);
                                            Console.WriteLine("\t {0}", idcc);
                                        }else{
                                            CrmCuentasCampaniaGestor data = new CrmCuentasCampaniaGestor();
                                            data.IdCuentaCampania = idcc;
                                            data.IdUsuario = asesor;
                                            data.Estado = "A";
                                            _context.CrmCuentasCampaniaGestor.Add(data);
                                            Console.WriteLine("\t {0}", "Insert ----------------");
                                            Console.WriteLine("\t {0}", asesor);
                                            Console.WriteLine("\t {0}", idcc);
                                        }
                                        await _context.SaveChangesAsync();

                                    }else{
                                        return new ModeloRespuesta()
                                        {
                                            Exito = 1,
                                            Data = "CLIENTE NO SE ENCUENTRA ASIGANDO A UNA CAMPAÑA"
                                        };
                                    }                              
                                }
                        }

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = "Guardado exitoso"
                        };
                        
                    } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY CLIENTES PARA ASIGNAR"
                    };
                    }
                } else {
                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = "NO HAY ASESORES"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/CrmAsignacionCab/ListaAsignacionColumna
        [HttpPost("ListaAsignacionColumna")]
        public async Task<ModeloRespuesta> ListaAsignacionColumna(AclAsignacion clientes)
        {
            try
            {
                List<string> miLista = new List<string>();
                foreach(Clientes lista in clientes.clientes){
                    Clientes data = new Clientes();
                    data.Nocuenta = lista.Nocuenta;
                    var ListaCuentas = await _context.CrmCuentasDetalle
                                        .Where(x => x.IdCuenta == data.Nocuenta && x.Campo.Length > 1)
                                        .Select(x => x.Campo).OrderBy(x => x).ToListAsync(); 
                    foreach(var i in ListaCuentas){
                        miLista.Add(i);
                    }
                }

                var ListaFinal = miLista.Distinct().OrderBy(x => x).ToList();

                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Data = ListaFinal
                };

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/CrmAsignacionCab/AsignacionColumna
        [HttpPost("AsignacionColumna")]
        public async Task<ModeloRespuesta> AsignacionColumna(AclAsignacion aclAsignacion)
        {
            try
            {
                if(!aclAsignacion.Equals(null))
                {
                    // var asesor = _context.CrmCuentasDetalle
                    //                 .Where(x => string.Equals(x.Campo, aclAsignacion.Tipo, StringComparison.CurrentCultureIgnoreCase))
                    //                 .Select(x => x.Valor).Distinct().FirstOrDefault();
                    foreach(Clientes lista in aclAsignacion.clientes){
                        Clientes data = new Clientes();
                        data.Nocuenta = lista.Nocuenta;
                        var asesor = _context.CrmCuentasDetalle
                                            .Where(x => x.IdCuenta == data.Nocuenta && string.Equals(x.Campo, aclAsignacion.Tipo, StringComparison.CurrentCultureIgnoreCase))
                                            .Select(x => x.Valor).Distinct().FirstOrDefault();
                        
                        var idUsuario = _context.AclUsuario.Where(x => x.UserName == asesor)
                                            .Select(x => x.IdUsuario).FirstOrDefault();
                        var idcc = _context.CrmCuentasCampania.Where(x => x.IdCuenta == lista.Nocuenta)
                                            .Select(x => x.Id).FirstOrDefault();
                        if(idUsuario!=0){
                            if(idcc!=0)
                            {
                                if(_context.CrmCuentasCampaniaGestor.Any(x => x.IdCuentaCampania == idcc))
                                {
                                    var crmAsignacion = _context.CrmCuentasCampaniaGestor.FirstOrDefault(x => x.IdCuentaCampania == idcc);
                                    crmAsignacion.IdUsuario = idUsuario;
                                    crmAsignacion.Estado = "A";
                                    Console.WriteLine("\t {0}", "Update");
                                    Console.WriteLine("\t {0}", idUsuario);
                                }else
                                {
                                    CrmCuentasCampaniaGestor dataCliente = new CrmCuentasCampaniaGestor();
                                    dataCliente.IdCuentaCampania = idcc;
                                    dataCliente.IdUsuario = idUsuario;
                                    dataCliente.Estado = "A";
                                    _context.CrmCuentasCampaniaGestor.Add(dataCliente);
                                    Console.WriteLine("\t {0}", "Insert");
                                    Console.WriteLine("\t {0}", idUsuario);
                                }
                                await _context.SaveChangesAsync();
                            }else{
                                return new ModeloRespuesta()
                                {
                                    Exito = 0,
                                    Mensage = "CLIENTE NO SE ENCUENTRA ASIGANDO A UNA CAMPAÑA"
                                };
                            }
                        }else{
                            return new ModeloRespuesta()
                            {
                                Exito = 0,
                                Mensage = "USUARIO NO EXISTE"
                            };
                        }                                               
                    }
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Mensage = "Guardado Existoso"
                };

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }


        // api/CrmAsignacionCab/RemoverAsignacion
        [HttpPut("RemoverAsignacion")]
        public async Task<ModeloRespuesta> RemoverAsignacion(AclAsignacion clientes)
        {
            try
            {
                if(!clientes.Equals(null))
                {
                    Console.WriteLine("REMOVER ASIGNACION");
                    Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(clientes));
                    List<uint> miLista = new List<uint>();
                    foreach(Clientes lista in clientes.clientes)
                    {
                        var idcc = _context.CrmCuentasCampania.Where(x => x.IdCuenta == lista.Nocuenta && clientes.idcampania == x.IdCampania)
                                .Select(x => x.Id).FirstOrDefault();

                        if (idcc!=0){
                            var ccgestormodificar = _context.CrmCuentasCampaniaGestor.FirstOrDefault(x => x.IdCuentaCampania == idcc);
                            ccgestormodificar.Estado = "I";
                            miLista.Add(idcc);
                        }
                        Console.WriteLine(idcc);
                    }
                    var ListaFinal = miLista.Distinct().OrderBy(x => x).ToList();
                    await _context.SaveChangesAsync();
                    return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Mensage = "CUENTAS REMOVIDAS",
                            Data = ListaFinal
                        };
                }else{
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "NO HAY CLIENTES SELECCIONADOS"
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
