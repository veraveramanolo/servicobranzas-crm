using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclEfectoAcuerdoController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclEfectoAcuerdoController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclEfectoAcuerdo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclEfectoAcuerdo>>> GetAclEfectoAcuerdo()
        {
            return await _context.AclEfectoAcuerdo.ToListAsync();
        }

        // GET: api/AclEfectoAcuerdo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclEfectoAcuerdo>> GetAclEfectoAcuerdo(int id)
        {
            var aclEfectoAcuerdo = await _context.AclEfectoAcuerdo.FindAsync(id);

            if (aclEfectoAcuerdo == null)
            {
                return NotFound();
            }

            return aclEfectoAcuerdo;
        }

        // PUT: api/AclEfectoAcuerdo/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclEfectoAcuerdo(int id, AclEfectoAcuerdo aclEfectoAcuerdo)
        {
            if (id != aclEfectoAcuerdo.IdEfecto)
            {
                return BadRequest();
            }

            _context.Entry(aclEfectoAcuerdo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclEfectoAcuerdoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclEfectoAcuerdo
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclEfectoAcuerdo>> PostAclEfectoAcuerdo(AclEfectoAcuerdo aclEfectoAcuerdo)
        {
            _context.AclEfectoAcuerdo.Add(aclEfectoAcuerdo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclEfectoAcuerdoExists(aclEfectoAcuerdo.IdEfecto))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclEfectoAcuerdo", new { id = aclEfectoAcuerdo.IdEfecto }, aclEfectoAcuerdo);
        }

        // DELETE: api/AclEfectoAcuerdo/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclEfectoAcuerdo>> DeleteAclEfectoAcuerdo(int id)
        {
            var aclEfectoAcuerdo = await _context.AclEfectoAcuerdo.FindAsync(id);
            if (aclEfectoAcuerdo == null)
            {
                return NotFound();
            }

            _context.AclEfectoAcuerdo.Remove(aclEfectoAcuerdo);
            await _context.SaveChangesAsync();

            return aclEfectoAcuerdo;
        }

        private bool AclEfectoAcuerdoExists(int id)
        {
            return _context.AclEfectoAcuerdo.Any(e => e.IdEfecto == id);
        }

        //Consulta todos los departamentos
        //api/AclEfectoAcuerdo/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclEfectoAcuerdo()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclEfectoAcuerdo.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}
