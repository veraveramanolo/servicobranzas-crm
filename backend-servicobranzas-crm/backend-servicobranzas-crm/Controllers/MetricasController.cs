using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
using System.IdentityModel.Tokens.Jwt;
using backend_servicobranzas_crm.Helpers;


namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MetricasController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public MetricasController(ps_crmContext context)
        {
            _context = context;
        }
        // ------------------------------------------------ INDICADORES ------------------------------------------------
        
        // Metricas/GetGestiones
        // API que trae toda la data de la pantalla Métricas - Indicadores.
        [HttpGet("GetGestiones")]
        public async Task<ModeloRespuesta> GetGestiones(uint idcampania, DateTime? fechainicio, DateTime? fechafin)
        {
            try
            {
                if (_context.CrmGestion.Any())
                {
                    var ListaGestiones = await _context.AclUsuario
                                        .Join(_context.CrmGestion, usuario => usuario.IdUsuario, gestion => gestion.IdGestor,
                                        (usuario,gestion) => new { usuario,gestion })
                                        .Join(_context.CrmArbolGestion, ug => ug.gestion.IdArbolDecision, arbol => arbol.Id,
                                        (ug,arbol) => new { ug,arbol })
                                        .Join(_context.CrmCuentas, uga => uga.ug.gestion.IdCuenta, cuenta => cuenta.Id,
                                        (uga,cuenta) => new { uga,cuenta })
                                        .Join(_context.CrmCuentasCampania, ugac => ugac.cuenta.Id, cc => cc.IdCuenta,
                                        (ugac,cc) => new { ugac,cc })
                                        .Join(_context.CrmCuentasCampaniaProductos, ugacc => ugacc.cc.Id, p => p.IdCuentaCampania,
                                        (ugacc,p) => new { ugacc,p })
                                        .Join(_context.CrmCuentasCampaniaAcuerdos, ugaccp => ugaccp.p.Id, ac => ac.IdCuentaCampaniaProducto,
                                        (ugaccp,ac) => new { ugaccp,ac })
                                        .Where(x => x.ugaccp.ugacc.cc.IdCampania == idcampania && x.ugaccp.ugacc.cc.Id == x.ugaccp.ugacc.ugac.uga.ug.gestion.IdCuentaCampania 
                                        //&& (fechafiltro.Equals(null) ? x.ugac.uga.ug.gestion.HoraGestion <= DateTime.Now : x.ugac.uga.ug.gestion.HoraGestion >= fechafiltro))
                                        && (!fechainicio.Equals(null)? x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion >= fechainicio && x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion <= fechafin : x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion <= fechafin))
                                        .Select(x => new
                                                {
                                                    x.ugaccp.ugacc.ugac.uga.ug.usuario.IdUsuario,
                                                    x.ugaccp.ugacc.ugac.uga.ug.usuario.UserName,
                                                    x.ugaccp.ugacc.ugac.cuenta.Identificacion,
                                                    x.ugaccp.ugacc.ugac.cuenta.Nombre,
                                                    x.ugaccp.ugacc.ugac.uga.ug.gestion.IdCuenta,
                                                    x.ugaccp.ugacc.ugac.uga.ug.gestion.IdCuentaCampania,
                                                    fechaGestion = x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion,
                                                    tipoContacto = x.ugaccp.ugacc.ugac.uga.arbol.LabelEstado,
                                                    x.ugaccp.ugacc.ugac.uga.arbol.Alerta,
                                                    estado = x.ugaccp.ugacc.ugac.uga.arbol.Descripcion,
                                                    x.ugaccp.ugacc.ugac.uga.ug.gestion.DetalleGestion,
                                                    canal = x.ugaccp.ugacc.ugac.uga.ug.gestion.TipoGestion,
                                                    x.ac.ValorAcuerdo,
                                                    x.ugaccp.p.Deudatotal,
                                                    deudavencida = x.ugaccp.p.Valor
                                                }).OrderBy(x => x.UserName).ToListAsync();
                                        //  (from u in _context.AclUsuario 
                                        //  join g in _context.CrmGestion on u.IdUsuario equals g.IdGestor 
                                        //  join ag in _context.CrmArbolGestion on g.IdArbolDecision equals ag.Id 
                                        //  join c in _context.CrmCuentas on g.IdCuenta equals c.Id 
                                        //  join cc in _context.CrmCuentasCampania on c.Id equals cc.IdCuenta 
                                        //  join p in _context.CrmCuentasCampaniaProductos on cc.Id equals p.IdCuentaCampania 
                                        //  join a in _context.CrmCuentasCampaniaAcuerdos on p.Id equals a.IdCuentaCampaniaProducto into left 
                                        //  from bdleft in left.DefaultIfEmpty() 
                                        //  where cc.IdCampania.Equals(idcampania)  
                                        //  orderby u.UserName 
                                        //  select new { u.IdUsuario, u.UserName, c.Identificacion, c.Nombre, g.IdCuenta, fechaGestion = g.HoraGestion, 
                                        //              tipoContacto = ag.LabelEstado, ag.Alerta, estado = ag.Descripcion, g.DetalleGestion, canal = g.TipoGestion, 
                                        //              bdleft.ValorAcuerdo, p.Deudatotal, deudavencida = p.Valor }).Distinct();
                    
                    var ListaAsesores = ListaGestiones.Select(g => g.IdUsuario).Distinct().ToList();
                    string titular = "CONTACTO TITULAR";
                    string promesa = "PROMESA DE PAGO";
                    string pago = "YA PAGO";

                    List<TodoGestion> results = new List<TodoGestion>();
                    List<TodoGestionDolar> resultsdolar = new List<TodoGestionDolar>();
                    List<TodoGestionPorc> resultsporc = new List<TodoGestionPorc>();
                    List<TodoGestionDolarValorVenc> resultsdolarvv = new List<TodoGestionDolarValorVenc>();
                    List<TodoGestionDolarDeudaTotal> resultsdolardt = new List<TodoGestionDolarDeudaTotal>();
                    
                    foreach (var item in ListaAsesores)
                    {
                        // ------------- #	
                        var gestiones = ListaGestiones.Where(g => g.IdUsuario == item).Count();
                        var clientes = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.IdCuenta).Distinct().Count();
                        var directos = ListaGestiones.Where(g => g.IdUsuario == item && g.tipoContacto == titular).Select(g => g.tipoContacto).Count();
                        var efectivos = ListaGestiones.Where(g => g.IdUsuario == item && g.estado == promesa || g.estado == pago).Count();
                        var acuerdos = ListaGestiones.Where(g => g.IdUsuario == item && g.ValorAcuerdo != null).Count();
                        
                        TodoGestion data = new TodoGestion();
                        data.idUsuario = item;
                        data.asesor = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.UserName).Distinct().First();
                        data.gestiones = gestiones;
                        data.clientes = clientes;
                        data.directos = directos;
                        data.efectivos = efectivos;
                        data.acuerdos = acuerdos;
                        results.Add(data);

                        // ------------- $
                        // ACUERDOS/PAGO
                        // var clientesdolar = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => new{g.IdCuenta,g.Deudatotal}).Distinct().Sum(g => g.Deudatotal);
                        var clientesdolar = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => new{g.IdCuenta,g.ValorAcuerdo}).Distinct().Sum(g => g.ValorAcuerdo);
                        var directosdolar = ListaGestiones.Where(g => g.IdUsuario == item && g.tipoContacto == titular).Select(g => g.ValorAcuerdo).Sum();
                        var efectivosdolar = ListaGestiones.Where(g => g.IdUsuario == item && g.estado == promesa || g.estado == pago).Select(g => g.ValorAcuerdo).Sum();
                        var acuerdosdolar = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.ValorAcuerdo).Sum();

                        TodoGestionDolar datadolar = new TodoGestionDolar();
                        datadolar.idUsuario = item;
                        datadolar.asesor = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.UserName).Distinct().First();
                        datadolar.gestiones = gestiones;
                        datadolar.clientes = clientesdolar;
                        datadolar.directos = directosdolar;
                        datadolar.efectivos = efectivosdolar;
                        datadolar.acuerdos = acuerdosdolar;
                        resultsdolar.Add(datadolar);

                        // VALOR VENCIDO
                        var clientesdolarVV = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => new{g.IdCuenta,g.deudavencida}).Distinct().Sum(g => g.deudavencida);
                        var directosdolarVV = ListaGestiones.Where(g => g.IdUsuario == item && g.tipoContacto == titular).Select(g => g.deudavencida).Sum();
                        var efectivosdolarVV = ListaGestiones.Where(g => g.IdUsuario == item && g.estado == promesa || g.estado == pago).Select(g => g.deudavencida).Sum();
                        var acuerdosdolarVV = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.deudavencida).Sum();

                        TodoGestionDolarValorVenc datadolarValorVenc = new TodoGestionDolarValorVenc();
                        datadolarValorVenc.idUsuario = item;
                        datadolarValorVenc.asesor = datadolar.asesor;
                        datadolarValorVenc.gestiones = gestiones; 
                        datadolarValorVenc.clientes = (float)clientesdolarVV;
                        datadolarValorVenc.directos = (float)directosdolarVV;
                        datadolarValorVenc.efectivos = (float)efectivosdolarVV;
                        datadolarValorVenc.acuerdos = (float)acuerdosdolarVV;
                        resultsdolarvv.Add(datadolarValorVenc);

                        // DEUDA TOTAL
                        var clientesdolarDT = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => new{g.IdCuenta,g.Deudatotal}).Distinct().Sum(g => g.Deudatotal);
                        var directosdolarDT = ListaGestiones.Where(g => g.IdUsuario == item && g.tipoContacto == titular).Select(g => g.Deudatotal).Sum();
                        var efectivosdolarDT = ListaGestiones.Where(g => g.IdUsuario == item && g.estado == promesa || g.estado == pago).Select(g => g.Deudatotal).Sum();
                        var acuerdosdolarDT = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.Deudatotal).Sum();

                        TodoGestionDolarDeudaTotal datadolarDeudaTotal = new TodoGestionDolarDeudaTotal();

                        datadolarDeudaTotal.idUsuario = item;
                        datadolarDeudaTotal.asesor = datadolar.asesor;
                        datadolarDeudaTotal.gestiones = gestiones; 
                        datadolarDeudaTotal.clientes = (float)clientesdolarDT;
                        datadolarDeudaTotal.directos = (float)directosdolarDT;
                        datadolarDeudaTotal.efectivos = (float)efectivosdolarDT;
                        datadolarDeudaTotal.acuerdos = (float)acuerdosdolarDT;
                        resultsdolardt.Add(datadolarDeudaTotal);

                        // ------------- %	
                        var clientesporc = Math.Round(Convert.ToDecimal(((double)clientes*100)/(double)gestiones),2);
                        var directosporc = Math.Round(Convert.ToDecimal(((double)directos*100)/(double)gestiones),2);
                        var efectivosporc = Math.Round(Convert.ToDecimal(((double)efectivos*100)/(double)gestiones),2);
                        var acuerdosporc = Math.Round(Convert.ToDecimal(((double)acuerdos*100)/(double)gestiones),2);

                        TodoGestionPorc dataporc = new TodoGestionPorc();
                        dataporc.idUsuario = item;
                        dataporc.asesor = ListaGestiones.Where(g => g.IdUsuario == item).Select(g => g.UserName).Distinct().First();
                        dataporc.gestiones = gestiones;
                        dataporc.clientes = clientesporc/*.ToString()+"%"+" ("+clientes+"/"+gestiones+")"*/;
                        dataporc.directos = directosporc/*.ToString()+"%"+" ("+directos+"/"+gestiones+")"*/;
                        dataporc.efectivos = efectivosporc/*.ToString()+"%"+" ("+efectivos+"/"+gestiones+")"*/;
                        dataporc.acuerdos = acuerdosporc/*.ToString()+"%"+" ("+acuerdos+"/"+gestiones+")"*/;
                        resultsporc.Add(dataporc);
                    }

                    var CantidadesGestion = new {ListaGestiones,results,resultsdolar,resultsdolarvv,resultsdolardt,resultsporc};

                    return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = CantidadesGestion
                        };         
                }else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No hay gestiones",
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        // Metricas/GetGestionesHora
        // API que trae toda la data de la pantalla Métricas - Indicadores :: Gestiones - Hora.
        [HttpGet("GetGestionesHora")]
        public async Task<ModeloRespuesta> GetGestionesHora(uint idcampania, DateTime? fechainicio, DateTime? fechafin)
        {
            try
            {
                if (_context.CrmGestion.Any())
                {
                    var ListaGestiones = await _context.AclUsuario
                                    .Join(_context.CrmGestion, usuario => usuario.IdUsuario, gestion => gestion.IdGestor,
                                    (usuario,gestion) => new { usuario,gestion })
                                    .Join(_context.CrmArbolGestion, ug => ug.gestion.IdArbolDecision, arbol => arbol.Id,
                                    (ug,arbol) => new { ug,arbol })
                                    .Join(_context.CrmCuentas, uga => uga.ug.gestion.IdCuenta, cuenta => cuenta.Id,
                                    (uga,cuenta) => new { uga,cuenta })
                                    .Join(_context.CrmCuentasCampania, ugac => ugac.cuenta.Id, cc => cc.IdCuenta,
                                    (ugac,cc) => new { ugac,cc })
                                    .Join(_context.CrmCuentasCampaniaProductos, ugacc => ugacc.cc.Id, p => p.IdCuentaCampania,
                                    (ugacc,p) => new { ugacc,p })
                                    .Join(_context.CrmCuentasCampaniaAcuerdos, ugaccp => ugaccp.p.Id, ac => ac.IdCuentaCampaniaProducto,
                                    (ugaccp,ac) => new { ugaccp,ac })
                                    .Where(x => x.ugaccp.ugacc.cc.IdCampania == idcampania && x.ugaccp.ugacc.cc.Id == x.ugaccp.ugacc.ugac.uga.ug.gestion.IdCuentaCampania 
                                    //&& (fechafiltro.Equals(null) ? x.ugac.uga.ug.gestion.HoraGestion <= DateTime.Now : x.ugac.uga.ug.gestion.HoraGestion >= fechafiltro))
                                    && (!fechainicio.Equals(null)? x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion >= fechainicio && x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion <= fechafin : x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion <= fechafin))
                                    .Select(x => new
                                             {
                                                x.ugaccp.ugacc.ugac.uga.ug.usuario.IdUsuario,
                                                x.ugaccp.ugacc.ugac.uga.ug.usuario.UserName,
                                                x.ugaccp.ugacc.ugac.cuenta.Identificacion,
                                                x.ugaccp.ugacc.ugac.cuenta.Nombre,
                                                x.ugaccp.ugacc.ugac.uga.ug.gestion.IdCuenta,
                                                x.ugaccp.ugacc.ugac.uga.ug.gestion.IdCuentaCampania,
                                                fechaGestion = x.ugaccp.ugacc.ugac.uga.ug.gestion.HoraGestion,
                                                tipoContacto = x.ugaccp.ugacc.ugac.uga.arbol.LabelEstado,
                                                x.ugaccp.ugacc.ugac.uga.arbol.Alerta,
                                                estado = x.ugaccp.ugacc.ugac.uga.arbol.Descripcion,
                                                x.ugaccp.ugacc.ugac.uga.ug.gestion.DetalleGestion,
                                                canal = x.ugaccp.ugacc.ugac.uga.ug.gestion.TipoGestion,
                                                x.ac.ValorAcuerdo,
                                                x.ugaccp.p.Deudatotal,
                                                DeudaVencida = x.ugaccp.p.Valor
                                             }).OrderBy(x => x.UserName).ToListAsync();
                    var ListaAsesores = ListaGestiones.Select(g => g.IdUsuario).Distinct().ToList();
                            //.GroupBy(x => new DateTime(x.fechaGestion.Hour,0,0))
                            //Select(g => g.IdUsuario).Distinct().ToList();
                    string titular = "CONTACTO TITULAR";
                    string promesa = "PROMESA DE PAGO";
                    string pago = "YA PAGO";

                    var ListaHoraAll = ListaGestiones.GroupBy(l => Convert.ToDateTime(l.fechaGestion).Hour)
                                        .Select(x => new
                                         {
                                            hora = x.Key,
                                            gestiones = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Count(),
                                            clientes = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => g.IdCuenta).Distinct().Count(),
                                            directos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && g.tipoContacto == titular).Select(g => g.tipoContacto).Count(),
                                            efectivos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && (g.estado == promesa || g.estado == pago)).Count(),
                                            acuerdos =  ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && g.ValorAcuerdo != null).Count(),
                                         }).OrderBy(l => l.hora).ToList();

                    var ListaHoraDolar = ListaGestiones.GroupBy(l => Convert.ToDateTime(l.fechaGestion).Hour)
                                        .Select(x => new
                                         {
                                            hora = x.Key,
                                            gestiones = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Count(),
                                            clientes = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => new{g.IdCuenta,g.Deudatotal}).Distinct().Sum(g => g.Deudatotal),
                                            directos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && g.tipoContacto == titular).Select(g => g.Deudatotal).Sum(),
                                            efectivos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && (g.estado == promesa || g.estado == pago)).Select(g => g.Deudatotal).Sum(),
                                            acuerdos =  ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => g.Deudatotal).Sum()
                                         }).OrderBy(l => l.hora).ToList();

                    var ListaHoraDolarVA = ListaGestiones.GroupBy(l => Convert.ToDateTime(l.fechaGestion).Hour)
                                        .Select(x => new
                                         {
                                            hora = x.Key,
                                            gestiones = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Count(),
                                            clientes = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => new{g.IdCuenta,g.ValorAcuerdo}).Distinct().Sum(g => g.ValorAcuerdo),
                                            directos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && g.tipoContacto == titular).Select(g => g.ValorAcuerdo).Sum(),
                                            efectivos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && (g.estado == promesa || g.estado == pago)).Select(g => g.ValorAcuerdo).Sum(),
                                            acuerdos =  ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => g.ValorAcuerdo).Sum()
                                         }).OrderBy(l => l.hora).ToList();

                    var ListaHoraDolarDV = ListaGestiones.GroupBy(l => Convert.ToDateTime(l.fechaGestion).Hour)
                                        .Select(x => new
                                         {
                                            hora = x.Key,
                                            gestiones = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Count(),
                                            clientes = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => new{g.IdCuenta,g.DeudaVencida}).Distinct().Sum(g => g.DeudaVencida),
                                            directos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && g.tipoContacto == titular).Select(g => g.DeudaVencida).Sum(),
                                            efectivos = ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key && (g.estado == promesa || g.estado == pago)).Select(g => g.DeudaVencida).Sum(),
                                            acuerdos =  ListaGestiones.Where(g => Convert.ToDateTime(g.fechaGestion).Hour == x.Key).Select(g => g.DeudaVencida).Sum()
                                         }).OrderBy(l => l.hora).ToList();

                    var ListaHoraPerc = ListaHoraAll.Select(x => new
                                         {
                                             x.hora,
                                             x.gestiones,
                                             clientes = Math.Round(Convert.ToDecimal(((double)x.clientes*100)/(double)x.gestiones),2),
                                             directos = Math.Round(Convert.ToDecimal(((double)x.directos*100)/(double)x.gestiones),2),
                                             efectivos = Math.Round(Convert.ToDecimal(((double)x.efectivos*100)/(double)x.gestiones),2), 
                                             acuerdos = Math.Round(Convert.ToDecimal(((double)x.acuerdos*100)/(double)x.gestiones),2)
                                         });

                    var CantidadesGestion = new {ListaGestiones,ListaHoraAll,ListaHoraDolar,ListaHoraDolarVA,ListaHoraDolarDV,ListaHoraPerc};
                    
                    return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = CantidadesGestion
                        };
                }else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No hay gestiones",
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        // Metricas/GetEstados
        [HttpGet("GetEstadosCliente")]
        public async Task<ModeloRespuesta> GetEstadosCliente(uint idcampania, DateTime? fechainicio, DateTime? fechafin)
        {
            try
            {
                if (_context.CrmGestion.Any())
                {
                    var ListaEstados = await _context.AclUsuario
                                    .Join(_context.CrmGestion, usuario => usuario.IdUsuario, gestion => gestion.IdGestor,
                                    (usuario,gestion) => new { usuario,gestion })
                                    .Join(_context.CrmArbolGestion, ug => ug.gestion.IdArbolDecision, arbol => arbol.Id,
                                    (ug,arbol) => new { ug,arbol })
                                    .Join(_context.CrmCuentasCampania, uga => uga.ug.gestion.IdCuentaCampania, cc => cc.Id,
                                    (uga,cc) => new { uga,cc })
                                    .Where(x => x.cc.IdCampania == idcampania 
                                    //&& (fechafiltro.Equals(null) ? x.ugac.uga.ug.gestion.HoraGestion <= DateTime.Now : x.ugac.uga.ug.gestion.HoraGestion >= fechafiltro))
                                    && (!fechainicio.Equals(null)? x.uga.ug.gestion.HoraGestion >= fechainicio && x.uga.ug.gestion.HoraGestion <= fechafin : x.uga.ug.gestion.HoraGestion <= fechafin))
                                    .Select(x => new
                                             {
                                                x.uga.ug.usuario.IdUsuario,
                                                x.uga.ug.usuario.UserName,
                                                x.uga.arbol.Id,
                                                Descripcion = x.uga.arbol.LabelNivel2 ?? x.uga.arbol.Descripcion,
                                                Perfilaciones = !x.uga.arbol.LabelNivel2.Equals(null) ? x.uga.arbol.Descripcion : "Sin Perfilaciones",
                                                x.uga.arbol.IdTipoGestion,
                                                x.uga.ug.gestion.IdCuentaCampania
                                             }).OrderBy(x => x.Descripcion).ToListAsync();
                    
                    var ListaEstadosIt = ListaEstados.Select(g => g.Descripcion).Distinct().ToList();
                    var ListaAsesores = ListaEstados.Select(g => g.IdUsuario).Distinct().ToList();

                    List<Metricas> tblprincipal = new List<Metricas>();
                    List<EstadosAsesor> tblAsesoresxestado = new List<EstadosAsesor>();
                    List<EstadosPerfilaciones> tblEstadoPerfilaciones = new List<EstadosPerfilaciones>();
                    List<EstadosTotal> tblEstadosTotal = new List<EstadosTotal>();
                    List<MetricasPerc> tblMetricasPerc = new List<MetricasPerc>();
                    List<MetricasDol> tblMetricasDol = new List<MetricasDol>();
                    List<MetricasDol> tblMetricasDolDV = new List<MetricasDol>();
                    List<EstadosTotalDol> tblEstadosTotalDol = new List<EstadosTotalDol>();
                    List<EstadosTotalDol> tblEstadosTotalDolDV = new List<EstadosTotalDol>();

                    var totalvalor = 0;
                    var totalestados = 0;
                    var totaldol = 0m;
                    var totaldolDV = 0m;
                    uint positivo = 1;
                    uint negativo = 2;
                                        
                    // --------------------------------------------------------------------------- #0
                    foreach (var item in ListaEstadosIt)
                    {
                        var valorestados = ListaEstados.Where(e => e.Descripcion == item).Count();
                        Metricas data = new Metricas();
                        data.Id = ListaEstados.Where(e => e.Descripcion == item).Select(e => e.Id).Distinct().First();
                        data.Descripcion = ListaEstados.Where(e => e.Descripcion == item).Select(e => e.Descripcion).Distinct().First();
                        data.IdTipoGestion = ListaEstados.Where(e => e.Descripcion == item).Select(e => e.IdTipoGestion).First();
                        data.Valor = valorestados;
                        tblprincipal.Add(data);

                        foreach (var j in ListaEstados.Where(e => e.Descripcion == item).Select(e => e.IdUsuario).Distinct())
                        {
                            EstadosAsesor dataA = new EstadosAsesor();
                            dataA.Id = data.Id;
                            dataA.IdAsesor = ListaEstados.Where(e => e.Descripcion == item && e.IdUsuario == j).Select(e => e.IdUsuario).Distinct().First();
                            var valoresasesores = ListaEstados.Where(e => e.Descripcion == item && e.IdUsuario == dataA.IdAsesor).Count();
                            dataA.Asesor = ListaEstados.Where(e => e.Descripcion == item && e.IdUsuario == dataA.IdAsesor).Select(e => e.UserName).Distinct().First();
                            dataA.Valor = valoresasesores;
                            tblAsesoresxestado.Add(dataA);
                        }

                        foreach (var j in ListaEstados.Where(e => e.Descripcion == item).Select(e => e.Perfilaciones).Distinct())
                        {
                            EstadosPerfilaciones dataP = new EstadosPerfilaciones();
                            dataP.Contacto = item;
                            var valorperfilaciones = ListaEstados.Where(e => e.Descripcion == item && e.Perfilaciones == j).Count();
                            dataP.Perfilaciones = j;
                            dataP.Valor = valorperfilaciones;
                            tblEstadoPerfilaciones.Add(dataP);
                        }
                        
                        totalestados++;
                        totalvalor+=valorestados;
                    }
                    
                    var estadospositivos = ListaEstados.Where(e => e.IdTipoGestion == positivo).Select(e => new {e.IdTipoGestion, e.Id}).Count();
                    var estadosnegativos = ListaEstados.Where(e => e.IdTipoGestion == negativo).Select(e => new {e.IdTipoGestion, e.Id}).Count();
                    var estadospositivosporc = (double)Math.Round(Convert.ToDecimal(((double)estadospositivos*100)/(double)totalvalor),2);
                    
                    EstadosTotal dataTotal = new EstadosTotal();
                    dataTotal.Positivos = estadospositivos;
                    dataTotal.Negativos = estadosnegativos;
                    dataTotal.PositivosPorcent = estadospositivosporc;
                    dataTotal.TotalEstados = totalestados;
                    dataTotal.TotalValor = totalvalor;
                    tblEstadosTotal.Add(dataTotal);

                    // --------------------------------------------------------------------------- %0
                    foreach (var item in ListaEstadosIt)
                    {
                        var valorestados = ListaEstados.Where(e => e.Descripcion == item).Count();
                        MetricasPerc dataPerc = new MetricasPerc();
                        dataPerc.Id = ListaEstados.Where(e => e.Descripcion == item).Select(e => e.Id).Distinct().First();
                        dataPerc.Descripcion = ListaEstados.Where(e => e.Descripcion == item).Select(e => e.Descripcion).Distinct().First();
                        dataPerc.IdTipoGestion = ListaEstados.Where(e => e.Descripcion == item).Select(e => e.IdTipoGestion).First();
                        dataPerc.Valor = Math.Round(Convert.ToDecimal(((double)valorestados*100)/(double)totalvalor),2);
                        tblMetricasPerc.Add(dataPerc);

                        // --------------DEUDA TOTAL--------------
                        MetricasDol dataDol = new MetricasDol();
                        dataDol.Id = dataPerc.Id;
                        dataDol.Descripcion = dataPerc.Descripcion;
                        dataDol.IdTipoGestion = dataPerc.IdTipoGestion;
                        dataDol.Valor = ListaEstados.Where(e => e.Descripcion == item)
                                        .Join(_context.CrmCuentasCampaniaProductos, es => es.IdCuentaCampania, p => p.IdCuentaCampania,
                                        (es,p) => new { es,p }).Select(x => x.p.Deudatotal).Sum();
                        tblMetricasDol.Add(dataDol);
                        totaldol+=(decimal)dataDol.Valor;

                        // --------------DEUDA VENCIDA--------------
                        MetricasDol dataDolDV = new MetricasDol();
                        dataDolDV.Id = dataPerc.Id;
                        dataDolDV.Descripcion = dataPerc.Descripcion;
                        dataDolDV.IdTipoGestion = dataPerc.IdTipoGestion;
                        dataDolDV.Valor = ListaEstados.Where(e => e.Descripcion == item)
                                        .Join(_context.CrmCuentasCampaniaProductos, es => es.IdCuentaCampania, p => p.IdCuentaCampania,
                                        (es,p) => new { es,p }).Select(x => x.p.Valor).Sum();
                        tblMetricasDolDV.Add(dataDolDV);
                        totaldolDV+=(decimal)dataDolDV.Valor;
                        
                    }
                    var estadospositivosdol = ListaEstados.Where(e => e.IdTipoGestion == positivo)
                                        .Join(_context.CrmCuentasCampaniaProductos, es => es.IdCuentaCampania, p => p.IdCuentaCampania,
                                        (es,p) => new { es,p }).Select(x => x.p.Deudatotal).Sum();
                    var estadosnegativosdol = ListaEstados.Where(e => e.IdTipoGestion == negativo)
                                        .Join(_context.CrmCuentasCampaniaProductos, es => es.IdCuentaCampania, p => p.IdCuentaCampania,
                                        (es,p) => new { es,p }).Select(x => x.p.Deudatotal).Sum();
                                        
                    EstadosTotalDol dataTotalDol = new EstadosTotalDol();
                    dataTotalDol.Positivos = estadospositivosdol;
                    dataTotalDol.Negativos = estadosnegativosdol;
                    dataTotalDol.PositivosPorcent = (double)Math.Round(Convert.ToDecimal(((double)estadospositivosdol*100)/(double)totaldol),2);
                    dataTotalDol.TotalEstados = totalestados;
                    dataTotalDol.TotalValor = Math.Round(totaldol,2);
                    tblEstadosTotalDol.Add(dataTotalDol);

                    // --------------DEUDA VENCIDA--------------
                    var estadospositivosdolDV = ListaEstados.Where(e => e.IdTipoGestion == positivo)
                                        .Join(_context.CrmCuentasCampaniaProductos, es => es.IdCuentaCampania, p => p.IdCuentaCampania,
                                        (es,p) => new { es,p }).Select(x => x.p.Valor).Sum();
                    var estadosnegativosdolDV = ListaEstados.Where(e => e.IdTipoGestion == negativo)
                                        .Join(_context.CrmCuentasCampaniaProductos, es => es.IdCuentaCampania, p => p.IdCuentaCampania,
                                        (es,p) => new { es,p }).Select(x => x.p.Valor).Sum();
                                        
                    EstadosTotalDol dataTotalDolDV = new EstadosTotalDol();
                    dataTotalDolDV.Positivos = estadospositivosdolDV;
                    dataTotalDolDV.Negativos = estadosnegativosdolDV;
                    dataTotalDolDV.PositivosPorcent = (double)Math.Round(Convert.ToDecimal(((double)estadospositivosdolDV*100)/(double)totaldol),2);
                    dataTotalDolDV.TotalEstados = totalestados;
                    dataTotalDolDV.TotalValor = Math.Round(totaldolDV,2);
                    tblEstadosTotalDolDV.Add(dataTotalDolDV);

                    var CantidadesGestion = new {ListaEstados,tblprincipal,tblMetricasPerc,tblMetricasDol,tblMetricasDolDV,tblAsesoresxestado,tblEstadosTotal,tblEstadosTotalDol,tblEstadosTotalDolDV,tblEstadoPerfilaciones};

                    return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = CantidadesGestion
                        };         
                }else
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "No hay gestiones",
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }
    }
}