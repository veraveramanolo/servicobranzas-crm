using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
using System.IdentityModel.Tokens.Jwt;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmCuentasController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmCuentasController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/CrmCuentas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CrmCuentas>>> GetCrmCuentas()
        {
            return await _context.CrmCuentas.ToListAsync();
        }

        // GET: api/CrmCuentas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CrmCuentas>> GetCrmCuentas(uint id)
        {
            var crmCuentas = await _context.CrmCuentas.FindAsync(id);

            if (crmCuentas == null)
            {
                return NotFound();
            }

            return crmCuentas;
        }

        // PUT: api/CrmCuentas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrmCuentas(uint id, CrmCuentas crmCuentas)
        {
            if (id != crmCuentas.Id)
            {
                return BadRequest();
            }

            _context.Entry(crmCuentas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CrmCuentasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CrmCuentas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CrmCuentas>> PostCrmCuentas(CrmCuentas crmCuentas)
        {
            _context.CrmCuentas.Add(crmCuentas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCrmCuentas", new { id = crmCuentas.Id }, crmCuentas);
        }

        // DELETE: api/CrmCuentas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CrmCuentas>> DeleteCrmCuentas(uint id)
        {
            var crmCuentas = await _context.CrmCuentas.FindAsync(id);
            if (crmCuentas == null)
            {
                return NotFound();
            }

            _context.CrmCuentas.Remove(crmCuentas);
            await _context.SaveChangesAsync();

            return crmCuentas;
        }

        //[HttpGet("GetCuentasGestor")]
        //public async Task<ModeloRespuesta> GetCuentasGestor(uint idGestor)
        //{
        //    try
        //    {
        //        if (_context.CrmCuentasCampaniaGestor.Any(x => x.IdUsuario == idGestor))
        //        {
        //            var cuentasGestor = await _context.CrmCuentasCampaniaGestor
        //                .Where(x => x.IdUsuario == idGestor)
        //                .Select(x => new
        //                {
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Estado,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                    x.IdCuentaCampaniaNavigation.IdCuentaNavigation.Id,
        //                }).ToListAsync();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}


        private bool CrmCuentasExists(uint id)
        {
            return _context.CrmCuentas.Any(e => e.Id == id);
        }

        // api/CrmCuentas/BusquedaIdNom
        [HttpGet("BusquedaIdNom")]
        public async Task<ModeloRespuesta> BusquedaIdNom(string? filtroid, string? filtronom)
        {
            try
            {
                if (_context.CrmCuentas.Any(x => x.Identificacion.Contains(filtroid) || x.Nombre.Contains(filtronom)))
                {

                    _decoder = new Decoder();
                    uint idusuario = _decoder.GetDecoder(HttpContext);
                    Console.WriteLine("idusuario ");
                    Console.WriteLine(idusuario);
                    var admin = _context.AclUsuario.Where(a => a.IdUsuario == idusuario).Select(a => a.IdRolNavigation.IdRol).FirstOrDefault();
                    Console.WriteLine("admin ");
                    Console.WriteLine(admin);
                    if (admin.Equals(1)  || admin.Equals(3))
                    {
                        var busqueda = from c in _context.CrmCuentas  
                                       join cc in _context.CrmCuentasCampania on c.Id equals cc.IdCuenta 
                                       join cg in _context.CrmCuentasCampaniaGestor on cc.Id equals cg.IdCuentaCampania into ccg
                                       from left in ccg.DefaultIfEmpty() 
                                       join p in _context.CrmCuentasCampaniaProductos on cc.Id equals p.IdCuentaCampania 
                                       where c.Identificacion.Contains(filtroid) || c.Nombre.Contains(filtronom)
                                       select new {
                                            idcuentacampania = cc.Id,
                                            idcuenta = c.Id,
                                            documento = c.Identificacion,
                                            nombre = c.Nombre,
                                            asesor = (left.Estado == "A" ? left.IdUsuarioNavigation.UserName : ""),
                                            campania = cc.IdCampaniaNavigation.Nombre,
                                            cliente = cc.IdCampaniaNavigation.IdCarteraNavigation.Nombre,
                                            idproducto = p.Id,
                                            nomproducto = p.Descripcion,
                                            idcampania = cc.IdCampaniaNavigation.Id,
                                            idcartera = cc.IdCampaniaNavigation.IdCarteraNavigation.Id
                                       };
                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = await busqueda.Distinct().ToListAsync()
                        };
                    }else
                    {
                        var busqueda = await _context.CrmCuentas
                        .Where(x => x.Identificacion.Contains(filtroid) 
                        || x.Nombre.Contains(filtronom))
                        .Join(_context.CrmCuentasCampania, cuentas => cuentas.Id, cuentcampania => cuentcampania.IdCuenta,
                        (cuentas, cuentcampania) => new { cuentas, cuentcampania})
                        .Join(_context.CrmCuentasCampaniaGestor, cuentcampania => cuentcampania.cuentcampania.Id, gestor => gestor.IdCuentaCampania,
                        (gestores, gestor) => new {gestores, gestor})
                        .Join(_context.CrmCuentasCampaniaProductos, info => info.gestor.IdCuentaCampania, producto => producto.IdCuentaCampania,
                        (info, producto) => new {info, producto})
                        .Select(x => new
                        {
                            idcuentacampania = x.info.gestores.cuentcampania.Id,
                            idcuenta = x.info.gestores.cuentas.Id,
                            documento = x.info.gestores.cuentas.Identificacion,
                            nombre = x.info.gestores.cuentas.Nombre,
                            asesor = x.info.gestor.IdUsuarioNavigation.UserName,
                            campania = x.info.gestor.IdCuentaCampaniaNavigation.IdCampaniaNavigation.Nombre,                            
                            cliente = x.info.gestor.IdCuentaCampaniaNavigation.IdCuentaNavigation.IdCarteraInicialNavigation.Nombre,
                            idproducto = x.producto.Id,
                            nomproducto = x.producto.Descripcion
                                    
                        }).OrderBy(x => x.nombre).ToListAsync();

                        return new ModeloRespuesta()
                        {
                            Exito = 1,
                            Data = busqueda
                        };
                    }
                    
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
