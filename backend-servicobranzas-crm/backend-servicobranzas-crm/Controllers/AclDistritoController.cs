using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AclDistritoController : ControllerBase
    {
        private readonly ps_crmContext _context;

        public AclDistritoController(ps_crmContext context)
        {
            _context = context;
        }

        // GET: api/AclDistrito
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AclDistrito>>> GetAclDistrito()
        {
            return await _context.AclDistrito.ToListAsync();
        }

        // GET: api/AclDistrito/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AclDistrito>> GetAclDistrito(int id)
        {
            var aclDistrito = await _context.AclDistrito.FindAsync(id);

            if (aclDistrito == null)
            {
                return NotFound();
            }

            return aclDistrito;
        }

        // PUT: api/AclDistrito/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAclDistrito(uint id, AclDistrito aclDistrito)
        {
            if (id != aclDistrito.IdDistrito)
            {
                return BadRequest();
            }

            _context.Entry(aclDistrito).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AclDistritoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AclDistrito
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AclDistrito>> PostAclDistrito(AclDistrito aclDistrito)
        {
            _context.AclDistrito.Add(aclDistrito);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AclDistritoExists(aclDistrito.IdDistrito))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAclDistrito", new { id = aclDistrito.IdDistrito }, aclDistrito);
        }

        // DELETE: api/AclDistrito/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AclDistrito>> DeleteAclDistrito(int id)
        {
            var aclDistrito = await _context.AclDistrito.FindAsync(id);
            if (aclDistrito == null)
            {
                return NotFound();
            }

            _context.AclDistrito.Remove(aclDistrito);
            await _context.SaveChangesAsync();

            return aclDistrito;
        }

        private bool AclDistritoExists(uint id)
        {
            return _context.AclDistrito.Any(e => e.IdDistrito == id);
        }

        //Consulta todos los distritos
        //api/AclDistrito/All
        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllAclDistrito()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclDistrito.ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        //Consulta distritos por id ciudad.
        // api/CrmCuentasDireccion/GetDistrito

        [HttpGet("GetDistrito")]
        public async Task<ModeloRespuesta> GetDistrito(uint idCiudad)
        {
            try
            {
                if (_context.AclDistrito.Any(x=>x.IdCiudad == idCiudad))
                {
                    var listaDistrito = await _context.AclDistrito
                        .Where(x => x.IdCiudad == idCiudad)
                        .Select(x => new
                        {
                            x.IdDistrito,
                            x.DistritoValor
                        }).ToListAsync();

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = listaDistrito
                    };
                }
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }
    }
}
