using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Class;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CrmGestionController : ControllerBase
    {
        private Decoder _decoder;
        private readonly ps_crmContext _context;

        public CrmGestionController(ps_crmContext context)
        {
            _context = context;
        }

        //Consulta gestión por cuenta.
        // api/CrmGestion/GetGestion/idCuentaCampania?=
        [HttpGet("GetGestion")]
        public async Task<ModeloRespuesta> GetGestion( uint idCuentaCampania)
        {
            try
            {
                if (_context.CrmCuentasCampania.Any(x => x.Id == idCuentaCampania))
                {
                    var gestionCliente = new object();
                    var cuenta = _context.CrmCuentasCampania
                        .FirstOrDefault(x => x.Id == idCuentaCampania).IdCuenta;
                    if (cuenta != null)
                    {
                        gestionCliente = await _context.CrmGestion
                            .Where(x => x.IdCuenta == cuenta)
                            .Select(x => new
                            {
                                id = x.Id,
                                idTelefono = x.IdTelefonoNavigation.Id,
                                idProducto = x.IdCuentasCampaniaProductosNavigation.Id,
                                fechaGestion = x.HoraGestion,
                                tiempoCall = x.TiempoLlamada,
                                estado = x.EstadoLlamada,
                                idArbol = x.IdArbolDecisionNavigation.Id,
                                detalle = x.DetalleGestion,
                                tipoGestion = x.TipoGestion

                            }).ToListAsync();
                    }

                    return new ModeloRespuesta()
                    {
                        Exito = 1,
                        Data = gestionCliente
                    };
                }

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 0,
                Mensage = "Resultados no encontrados"
            };
        }

        //Guarda gestión por cuenta.
        // api/CrmGestion/Crear
        [Obsolete]
        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearCrmGestion(CrmGestion crmGestion)
        {
            List<CrmGestion> consultaGestion = new List<CrmGestion>();

            try
            {
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                /*crmGestion.HoraGestion = DateTime.Now;
                
                crmGestion.IdGestorNavigation = _context.AclUsuario.Find(idusuario); ;
                crmGestion.IdGestor = idusuario;
                _context.CrmGestion.Add(crmGestion);
                await _context.SaveChangesAsync();*/
                var detalle_gestion = new MySqlParameter("detalleGestion", crmGestion.DetalleGestion);
                var estado_llamada = new MySqlParameter("estadoLlamada", crmGestion.EstadoLlamada);
                var id_arbol_gestion = new MySqlParameter("idArbolGestion", crmGestion.IdArbolDecision);
                var id_cuenta = new MySqlParameter("idCuenta", crmGestion.IdCuenta);
                var id_cuenta_campania = new MySqlParameter("idCuentaCampania", crmGestion.IdCuentaCampania);
                var id_cuenta_campania_prod = new MySqlParameter("idCuentaCampaniaProducto", crmGestion.IdCuentasCampaniaProductos);
                var id_gestion_dinomi = new MySqlParameter("idGestionDinomi", crmGestion.IdGestionDinomi);
                var id_telefono = new MySqlParameter("idTelefono", crmGestion.IdTelefono);
                var tiempo_llamada= new MySqlParameter("tiempoLlamada", crmGestion.TiempoLlamada);
                var tipo_gestion = new MySqlParameter("tipoGestion", crmGestion.TipoGestion);
                var id_gestor = new MySqlParameter("idGestor", idusuario);

                consultaGestion = await _context.CrmGestion
                    .FromSql("CALL crm_guarda_gestion (@detalleGestion, @estadoLlamada,@idArbolGestion,@idCuenta,@idCuentaCampania,@idCuentaCampaniaProducto,@idGestionDinomi,@idTelefono,@tiempoLlamada,@tipoGestion,@idGestor)",
                       parameters: new[] { detalle_gestion,
                                            estado_llamada,
                                            id_arbol_gestion,
                                            id_cuenta,
                                            id_cuenta_campania,
                                            id_cuenta_campania_prod,
                                            id_gestion_dinomi,
                                            id_telefono,
                                            tiempo_llamada,
                                            tipo_gestion,
                                            id_gestor
                        }).ToListAsync();

                //Se registra mejor gestion
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.ToString()) 
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Gestión Guardada",
                Data = consultaGestion.ToString() /*JsonConvert.SerializeObject(await _context.CrmGestion.FindAsync(crmGestion.Id), Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                })*/


            };
        }

        //Guarda gestión por cuenta.
        // api/CrmGestion/GuardarCaja
        [HttpPost("GuardarCaja")]
        public async Task<ModeloRespuesta> GuardarCajaCrm(CrmCaja crmCaja)
        {
            try
            {
                //crmGestion = new CrmGestion();
                crmCaja.FechaCreacion = DateTime.Now;
                _decoder = new Decoder();
                uint idusuario = _decoder.GetDecoder(HttpContext);
                crmCaja.IdGestor = idusuario;

                //crmGestion.IdGestor = idusuario;
                //crmGestion.DetalleGestion = crmCaja.Comentario;
                

                _context.CrmCaja.Add(crmCaja);
                await _context.SaveChangesAsync(); 
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Gestión Guardada",
                Data = await _context.CrmCaja.FindAsync(crmCaja.Id)
            };
        }
        
        //Guarda gestión por cuenta.
        // api/CrmGestion/CSVupload
        [HttpPost("CSVupload")]
        public async Task<ModeloRespuesta> CSVupload(IFormFile fileupload)
        {
            IFormFile file = Request.Form.Files[0];
            string idCartera = Request.Form["idCartera"];
            string idCampania = Request.Form["idCampania"];
            if (file == null || file.Length <= 0) 
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error al cargar archivo",
                    Data = ""
                };
            }
            if (!file.FileName.EndsWith(".csv")){
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error al cargar archivo, la extension debe ser CSV",
                    Data = file.FileName
                };
            }
            string[] values = null;
            int index = 0;
            CrmCuentas cuenta = null;
            try
            {
                CrmCartera cartera = _context.CrmCartera.Find(Convert.ToUInt32(idCartera));
                CrmCampania campania = _context.CrmCampania.Find(Convert.ToUInt32(idCampania));
                
                string[] cabecera = null;
                
                using (StreamReader csvReader = new StreamReader(file.OpenReadStream())) 
                {
                    while (!csvReader.EndOfStream)
                    {
                        index++;
                        if(index==1){
                            var line = csvReader.ReadLine();
                            cabecera = line.Split(',');
                        }
                        else
                        {
                            var line = csvReader.ReadLine();
                            values = line.Split(',');
                            cuenta = new CrmCuentas();
                            cuenta = _context.CrmCuentas.Where(x => x.Identificacion == values[7]).FirstOrDefault();
                            if(cuenta==null)
                            {
                                cuenta = new CrmCuentas();
                                cuenta.Nombre = values[6];
                                cuenta.Identificacion = values[7];
                                cuenta.IdCarteraInicial = Convert.ToUInt32(idCartera);
                                cuenta.IdCampaniaInicial = Convert.ToUInt32(idCampania);
                                cuenta.Estado = "A";
                                _context.CrmCuentas.Add(cuenta);
                                _context.SaveChanges();
                            }
                            CrmCuentasTelefono telefono = null;
                            int[] numbers = { 12,13,17,18,23,26 };
                            foreach (int i in numbers)
                            {
                                telefono = new CrmCuentasTelefono();
                                telefono = _context.CrmCuentasTelefono.Where(x => x.IdCuenta == (cuenta.Id) && x.Valor == (values[i]) ).FirstOrDefault();
                                if(telefono==null){
                                    try{
                                        telefono = new CrmCuentasTelefono();
                                        telefono.IdCuenta = cuenta.Id;
                                        telefono.Tipo = "Celular";
                                        telefono.Valor = values[i];
                                        _context.CrmCuentasTelefono.Add(telefono);
                                        _context.SaveChanges();
                                    }
                                    catch (Exception e1){
                                        var mensaje = e1.Message;
                                        continue;
                                         /*return new ModeloRespuesta()
                                            {
                                                Exito = 0,
                                                Mensage = "ERROR",
                                                Data = e1.Message
                                            };*/
                                    }
                                }
                            }
                           
                            //Se rgistra los telefonos
                            CrmCuentasDireccion direccion = new CrmCuentasDireccion();
                            direccion = _context.CrmCuentasDireccion.Where(x => x.IdCuenta == (cuenta.Id) && x.Valor == (values[14]) ).FirstOrDefault();
                            if(direccion == null){
                                direccion = new CrmCuentasDireccion();
                                direccion.IdCuenta = cuenta.Id;
                                direccion.Tipo = "DOMICILIO";
                                direccion.Valor = values[14];
                                _context.CrmCuentasDireccion.Add(direccion);
                                _context.SaveChanges();
                            }

                            CrmCuentasDireccion direccion2 = new CrmCuentasDireccion();
                            direccion2 = _context.CrmCuentasDireccion.Where(x => x.IdCuenta == (cuenta.Id) && x.Valor == (values[9]) ).FirstOrDefault();
                            if(direccion2 == null){
                                direccion2 = new CrmCuentasDireccion();
                                direccion2.IdCuenta = cuenta.Id;
                                direccion2.Tipo = "TRABAJO";
                                direccion2.Valor = values[9];
                                _context.CrmCuentasDireccion.Add(direccion2);
                                _context.SaveChanges();
                            }
                            
                            CrmCuentasCampania cuentaCamp = new CrmCuentasCampania();
                            cuentaCamp = _context.CrmCuentasCampania.Where(x => x.IdCuenta == (cuenta.Id) && x.IdCampania == Convert.ToUInt32(idCampania) ).FirstOrDefault();
                            if(cuentaCamp == null){
                                cuentaCamp = new CrmCuentasCampania();
                                cuentaCamp.IdCuenta = cuenta.Id;
                                cuentaCamp.IdCampania = Convert.ToUInt32(idCampania);
                                _context.CrmCuentasCampania.Add(cuentaCamp);
                                _context.SaveChanges();
                            }
                            
                            CrmCuentasCampaniaProductos prod = new CrmCuentasCampaniaProductos();
                            prod.IdCuentaCampania = cuentaCamp.Id;
                            decimal valor;
                            Decimal.TryParse(values[31], out valor);
                            prod.Valor = valor;
                            prod.Descripcion = values[0];
                            prod.Edadmor = Convert.ToInt32(values[28]);
                            decimal dtotal;
                            Decimal.TryParse(values[27], out dtotal);
                            prod.Deudatotal = dtotal;
                            decimal vpvencer;
                            Decimal.TryParse(values[30], out vpvencer);
                            prod.Valorporvencer = vpvencer;
                            _context.CrmCuentasCampaniaProductos.Add(prod);
                            //detalles
                            CrmCuentasDetalle detalle = new CrmCuentasDetalle();
                            int[] idxdet = { 1,2,3,4,5,8,10,11,14,15,16,19,20,21,22,24,25,29 };
                            foreach (int i in numbers)
                            {
                                detalle = _context.CrmCuentasDetalle.Where(x => x.IdCuenta == (cuenta.Id) && x.Campo == (cabecera[i]) ).FirstOrDefault();
                                if(detalle == null)
                                {
                                    detalle = new CrmCuentasDetalle();
                                    
                                }
                                try{
                                    detalle.IdCuenta = cuenta.Id;
                                    detalle.Campo = cabecera[i];
                                    detalle.Valor = values[i];
                                    detalle.IdCuentaCampaniaProducto = prod.Id;
                                    _context.CrmCuentasDetalle.Add(detalle);
                                    _context.SaveChanges();
                                }catch (Exception ex4)
                                {
                                    var data = ex4.Message;
                                } 
                            }
                        }
                        //_context.SaveChanges();
                        
                    }
                }
                //await _context.SaveChangesAsync();
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Mensage = "Exito",
                    Data = ""
                };
            }
            catch (Exception ex4)
            { 
                var message = "---BEGIN XInnerException--- " + Environment.NewLine +
                   "Exception type " + ex4.InnerException.GetType() + Environment.NewLine +
                   "Exception message: " + ex4.InnerException.Message + Environment.NewLine +
                   "Stack trace: " + ex4.StackTrace + Environment.NewLine +
                   "---END Inner Exception";
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = message,//"Error inesperado",
                    Data = ex4.Message
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Gestión Guardada",
                Data = "XX"
            };
        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarCrmGestion(CrmGestion crmGestion)
        {
            try
            {
                if (!CrmGestionExists(crmGestion.Id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Gestión que desea modificar no existe"
                    };
                }

                var crmGestionModificar = _context.CrmGestion.FirstOrDefault(x => x.Id == crmGestion.Id);
                if (crmGestion.IdCuentasCampaniaProductos != null)
                    crmGestionModificar.IdCuentasCampaniaProductos = crmGestion.IdCuentasCampaniaProductos;
                if (crmGestion.IdTelefono != null)
                    crmGestionModificar.IdTelefono = crmGestion.IdTelefono;
                if (crmGestion.IdGestionDinomi != null)
                    crmGestionModificar.IdGestionDinomi = crmGestion.IdGestionDinomi;
                if (crmGestion.TiempoLlamada != null)
                    crmGestionModificar.TiempoLlamada = crmGestion.TiempoLlamada;
                if (!string.IsNullOrEmpty(crmGestion.EstadoLlamada))
                    crmGestionModificar.EstadoLlamada = crmGestion.EstadoLlamada;
                if (crmGestion.IdArbolDecision != null)
                    crmGestionModificar.IdArbolDecision = crmGestion.IdArbolDecision;
                crmGestionModificar.DetalleGestion = crmGestion.DetalleGestion;
                crmGestionModificar.TipoGestion = crmGestion.TipoGestion;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Gestión Modificada"
            };

        }



        private bool CrmGestionExists(uint id)
        {
            return _context.CrmGestion.Any(e => e.Id == id);
        }
    }
}
