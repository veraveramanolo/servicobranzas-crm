﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AclRolesController : Controller
    {
        private readonly ps_crmContext _context;

        public AclRolesController(ps_crmContext context)
        {
            _context = context;
        }

        [HttpGet("All")]
        public async Task<ModeloRespuesta> AllRoles()
        {
            try
            {
                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = await _context.AclRoles.Select(x=> new
                        {
                            x.Descripcion,
                            x.IdRol,
                            x.Anulado,
                            x.FechaCreacion,
                            x.UsrModificacion,
                            x.UsrCreacion,
                            x.FechaModificacion,
                        }
                        ).ToListAsync()
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpGet("Rol")]
        public async Task<ModeloRespuesta> GetRol(uint? id)
        {
            try
            {
                var aclRoles = await _context.AclRoles.FindAsync(id);
                if (aclRoles == null)
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Rol no encontrado"
                    };
                }

                return new ModeloRespuesta()
                {
                    Exito = 1,
                    Data = aclRoles
                };
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
        }

        [HttpPost("Modificar")]
        public async Task<ModeloRespuesta> ModificarRol( AclRoles aclRoles)
        {
            try
            {
                if (!AclRolesExists(aclRoles.IdRol))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Perfil que desea modificar no existe"
                    };
                }

                var aclRolModificar = _context.AclRoles.FirstOrDefault(x => x.IdRol == aclRoles.IdRol);
                if (!string.IsNullOrEmpty(aclRoles.Descripcion))
                    aclRolModificar.Descripcion = aclRoles.Descripcion;
                if (!string.IsNullOrEmpty(aclRoles.UsrModificacion))
                    aclRolModificar.UsrModificacion = aclRoles.UsrModificacion;
                aclRolModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Rol Modificado"
            };
        }


        [HttpPost("Crear")]
        public async Task<ModeloRespuesta> CrearRol(AclRoles aclRoles)
        {
            try
            {
                _context.AclRoles.Add(aclRoles);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }

            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Rol Creado",
                Data = await _context.AclRoles.FindAsync(aclRoles.IdRol)
            };
        }


        [HttpGet("CambiarEstado")]
        public async Task<ModeloRespuesta> CambiarEstadoRol(uint id, string anulado, string UsrModificacion)
        {
            try
            {
                if (!AclRolesExists(id))
                {
                    return new ModeloRespuesta()
                    {
                        Exito = 0,
                        Mensage = "Rol que desea modificar no existe"
                    };
                }

                var aclRolModificar = _context.AclRoles.FirstOrDefault(x => x.IdRol == id);
                aclRolModificar.Anulado = anulado;
                if (!string.IsNullOrEmpty(UsrModificacion))
                    aclRolModificar.UsrModificacion = UsrModificacion;
                aclRolModificar.FechaModificacion = DateTime.Now;

                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Error inesperado",
                    Data = new string(e.Message)
                };
            }
            return new ModeloRespuesta()
            {
                Exito = 1,
                Mensage = "Rol Modificado"
            };
        }

         
        private bool AclRolesExists(uint id)
        {
            return _context.AclRoles.Any(e => e.IdRol == id);
        }
    }
}
