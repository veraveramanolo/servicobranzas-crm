﻿using System.Linq;
using backend_servicobranzas_crm.Modelo.Request;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace backend_servicobranzas_crm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutentificacionController : ControllerBase
    {
        private IUserService _userService;

        public AutentificacionController(IUserService userService)
        {
            _userService = userService;
        }


        [HttpPost("login")]
        public IActionResult Autentificar([FromBody] AuthRequest model)
        {
            ModeloRespuesta respuesta;
            var userResponse = _userService.Autentificacion(model);

            if (userResponse == null)
            {
                respuesta =new ModeloRespuesta()
                {
                    Exito = 0,
                    Mensage = "Usuario o contraseña incorrectos"
                };
                return Ok(respuesta);
            }

            respuesta = new ModeloRespuesta()
            {
                Data = userResponse,
                Exito = 1
            };

            return Ok(respuesta);
        }


        //[Authorize]
        //[HttpGet("accesos")]
        //public IActionResult ConsultarPermiosAccesos(int idUser)
        //{
        //    ModeloRespuesta respuesta;

        //    var Response = _userService.GetRutasAccesos(idUser);

        //    if (!Response.Any())
        //    {
        //        respuesta = new ModeloRespuesta()
        //        {
        //            Exito = 0,
        //            Mensage = "Usuario no Tiene Permisos registrados"
        //        };

        //        return Ok(respuesta);
        //    }

        //    respuesta = new ModeloRespuesta()
        //    {
        //        Exito = 1,
        //        Mensage = "",
        //        Data = Response
        //    };

        //    return Ok(respuesta);
        //}
    }
}
