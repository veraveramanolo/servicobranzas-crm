using System.Net;
using backend_servicobranzas_crm.Helpers;
using backend_servicobranzas_crm.Modelo.Common;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Services.Class;
using backend_servicobranzas_crm.Services.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System.Text;
using Microsoft.AspNetCore.HttpOverrides;

namespace backend_servicobranzas_crm
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();


            services.AddRazorPages();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Add(IPAddress.Parse("172.16.237.131"));
            });

            //Conexion a MariaDB
            //services.AddEntityFrameworkMySql();

            //string strCnnSQL = Configuration.GetSection("ConnectionStrings").GetSection("ps_crm").Value;
            //services.AddDbContextPool<ps_crmContext>(
            //    options => options.UseMySql(strCnnSQL,
            //        mysqlOptions =>
            //        {
            //            mysqlOptions.ServerVersion(new System.Version(10, 4, 8), ServerType.MariaDb);
            //        }
            //));

            services.AddDbContext<ps_crmContext>(options =>
                options.UseMySql(Configuration.GetSection("ConnectionStrings").GetSection("ps_crm").Value));

            services.Configure<UserSecret>(Configuration.GetSection("UserSecret"));

            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            // app.UseCors(x => x
            //     .AllowAnyOrigin()
            //     .AllowAnyMethod()
            //     .AllowAnyHeader());
            app.UseCors(x => x
                              .AllowAnyMethod()
                              .AllowAnyHeader()
                              .SetIsOriginAllowed(origin => true)
                              .AllowCredentials());

            //app.UseHttpsRedirection();
            //app.UseAuthentication();
            //app.UseAuthorization();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseMiddleware<JwtMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
