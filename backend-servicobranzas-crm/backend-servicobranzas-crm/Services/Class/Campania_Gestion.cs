﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Interface;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;

namespace backend_servicobranzas_crm.Services.Class
{
    public class Campania_Gestion : ICampania_Gestion
    {
        private readonly ps_crmContext _context;

        public Campania_Gestion(ps_crmContext _context)
        {
            this._context = _context;
        }

        [Obsolete]
        public async Task<List<CrmConsultaGestionCampania>> GetCuentasCampania(int IdCampania, int idUsuario)
        {
            List<CrmConsultaGestionCampania> consultaCampaniaGestion = new List<CrmConsultaGestionCampania>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var id_Usuario = new  MySqlParameter("id_usuario", idUsuario);

                consultaCampaniaGestion = await _context.CrmConsultaGestionCampania
                    .FromSql("CALL crm_consulta_gestion_campania (@id_campania, @id_usuario)",
                        parameters: new[] {Id_Campania, id_Usuario }).ToListAsync();
                
               
                if (consultaCampaniaGestion.Count == 1)
                    if (consultaCampaniaGestion.Select(x => x.Idcuentacampania).FirstOrDefault() == null)
                    {
                        consultaCampaniaGestion = new List<CrmConsultaGestionCampania>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaCampaniaGestion;
        }


        [Obsolete]
        public async Task<List<CrmConsultaGestionCampania>> GetBDfiltrada(int IdCampania, int idUsuario, string descripcion)
        {
            List<CrmConsultaGestionCampania> consultaBDfiltrada = new List<CrmConsultaGestionCampania>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var id_Usuario = new  MySqlParameter("id_usuario", idUsuario);
                var estado = new MySqlParameter("descripcion", descripcion);

                consultaBDfiltrada = await _context.CrmConsultaGestionCampania
                    .FromSql("CALL crm_consulta_gestion_campania_filt (@id_campania, @id_usuario, @descripcion)",
                        parameters: new[] {Id_Campania, id_Usuario, estado}).ToListAsync();
                
               
                if (consultaBDfiltrada.Count == 1)
                    if (consultaBDfiltrada.Select(x => x.Idcuentacampania).FirstOrDefault() == null)
                    {
                        consultaBDfiltrada = new List<CrmConsultaGestionCampania>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaBDfiltrada;
        }

        [Obsolete]
        public async Task<List<AclEstados>> GetListaEstados(int IdCampania, int idUsuario)
        {
            List<AclEstados> listaEstados = new List<AclEstados>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var id_Usuario = new  MySqlParameter("id_usuario", idUsuario);

                listaEstados = await _context.AclEstados
                    .FromSql("CALL crm_consulta_gestion_campania_total (@id_campania, @id_usuario)",
                        parameters: new[] {Id_Campania, id_Usuario }).ToListAsync();

                if (listaEstados.Count == 1)
                    if (listaEstados.Select(x => x.Estado).FirstOrDefault() == null)
                    {
                        listaEstados = new List<AclEstados>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return listaEstados;
        }

        [Obsolete]
        public async Task<List<CrmMulticanal>> GetCuentasMulticanal(int IdCuentaCampania)
        {
            List<CrmMulticanal> consultaMulticanal = new List<CrmMulticanal>();
            try
            {
                var Id_Cuenta_Campania = new MySqlParameter("id_cuenta_campania", IdCuentaCampania);

                consultaMulticanal = await _context.CrmMulticanal
                    .FromSql("CALL crm_multicanal (@id_cuenta_campania)",
                        parameters: new[] {Id_Cuenta_Campania }).ToListAsync();
                
               
                if (consultaMulticanal.Count == 1)
                    if (consultaMulticanal.Select(x => x.Nocuenta).FirstOrDefault() == null)
                    {
                        consultaMulticanal = new List<CrmMulticanal>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaMulticanal;
        }

        [Obsolete]
        public async Task<List<CrmMulticanal>> GetCuentasMulticanalFiltroFecha(int IdCampania, DateTime fechafiltro)
        {
            List<CrmMulticanal> consultaMulticanalFecha = new List<CrmMulticanal>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var Fecha_filtro = new MySqlParameter("fecha_filtro", fechafiltro);

                consultaMulticanalFecha = await _context.CrmMulticanal
                    .FromSql("CALL crm_multicanal_fecha_filtro (@id_campania, @fecha_filtro)",
                        parameters: new[] {Id_Campania, Fecha_filtro}).ToListAsync();
                
               
                if (consultaMulticanalFecha.Count == 1)
                    if (consultaMulticanalFecha.Select(x => x.Nocuenta).FirstOrDefault() == null)
                    {
                        consultaMulticanalFecha = new List<CrmMulticanal>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaMulticanalFecha;
        }


        [Obsolete]
        public async Task<List<AclMulticanalEstados>> GetMulticanalFiltroFecha(int idCampania, DateTime? fechafiltro)
        {
            List<AclMulticanalEstados> listaEstados = new List<AclMulticanalEstados>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", idCampania);
                var Fecha_filtro = new  MySqlParameter("fecha_filtro", fechafiltro);

                listaEstados = await _context.AclMulticanalEstados
                    .FromSql("CALL crm_multicanal_estados (@id_campania, @fecha_filtro)",
                        parameters: new[] {Id_Campania, Fecha_filtro }).ToListAsync();

                if (listaEstados.Count == 1)
                    if (listaEstados.Select(x => x.Gestion).FirstOrDefault() == null)
                    {
                        listaEstados = new List<AclMulticanalEstados>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return listaEstados;
        }

        [Obsolete]
        public async Task<List<AclMulticanalEstados>> GetMulticanalAsesores(int idCampania)
        {
            List<AclMulticanalEstados> listaAsesores = new List<AclMulticanalEstados>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", idCampania);

                listaAsesores = await _context.AclMulticanalEstados
                    .FromSql("CALL crm_multicanal_asesores (@id_campania)",
                        parameters: new[] {Id_Campania }).ToListAsync();

                if (listaAsesores.Count == 1)
                    if (listaAsesores.Select(x => x.Gestion).FirstOrDefault() == null)
                    {
                        listaAsesores = new List<AclMulticanalEstados>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return listaAsesores;
        }


        [Obsolete]
        public async Task<List<AclListaAsesoresasig>> GetListaAsesoresAsig(int idCampania)
        {
            List<AclListaAsesoresasig> listaAsesoresAsig = new List<AclListaAsesoresasig>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", idCampania);

                listaAsesoresAsig = await _context.AclListaAsesoresasig
                    .FromSql("CALL ListaAsesores (@id_campania)",
                        parameters: new[] {Id_Campania}).ToListAsync();

                if (listaAsesoresAsig.Count == 1)
                    if (listaAsesoresAsig.Select(x => x.IdUsuario).FirstOrDefault().Equals(null))
                    {
                        listaAsesoresAsig = new List<AclListaAsesoresasig>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return listaAsesoresAsig;
        }
        
    }
}
