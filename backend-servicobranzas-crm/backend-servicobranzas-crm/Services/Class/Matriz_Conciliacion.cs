using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Interface;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;

namespace backend_servicobranzas_crm.Services.Class
{
    public class Matriz_Conciliacion : IMatriz_Conciliacion
    {
        private readonly ps_crmContext _context;

        public Matriz_Conciliacion(ps_crmContext _context)
        {
            this._context = _context;
        }

        [Obsolete]
        public async Task<List<CrmMatrizAcuerdos>> GetAcuerdosXasignacion(int IdCampania, DateTime fechainicio, DateTime fechafin)
        {
            List<CrmMatrizAcuerdos> consultaAcuerdosXasignacion = new List<CrmMatrizAcuerdos>();
            try
            {
                // var Fechainicio = fechainicio.ToShortDateString();
                // var Fechafin = fechafin.ToShortDateString();
                Console.WriteLine(fechainicio);
                Console.WriteLine(fechafin);
                Console.WriteLine(IdCampania);
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var Fecha_Inicio = new  MySqlParameter("fecha_inicio", fechainicio);
                var Fecha_Fin = new  MySqlParameter("fecha_fin", fechafin);

                consultaAcuerdosXasignacion = await _context.CrmMatrizAcuerdos
                    .FromSql("CALL matriz_acuerdos_asignacion (@id_campania, @fecha_inicio, @fecha_fin)",
                        parameters: new[] {Id_Campania, Fecha_Inicio, Fecha_Fin}).ToListAsync();
                
                Console.WriteLine(consultaAcuerdosXasignacion);

                if (consultaAcuerdosXasignacion.Count == 1)
                    if (consultaAcuerdosXasignacion.Select(x => x.Noproducto).FirstOrDefault() == null)
                    {
                        consultaAcuerdosXasignacion = new List<CrmMatrizAcuerdos>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaAcuerdosXasignacion;
        }

        [Obsolete]
        public async Task<List<CrmMatrizAcuerdos>> GetAcuerdosXgestion(int IdCampania, DateTime fechainicio, DateTime fechafin)
        {
            List<CrmMatrizAcuerdos> consultaAcuerdosXgestion = new List<CrmMatrizAcuerdos>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var Fecha_Inicio = new  MySqlParameter("fecha_inicio", fechainicio);
                var Fecha_Fin = new  MySqlParameter("fecha_fin", fechafin);

                consultaAcuerdosXgestion = await _context.CrmMatrizAcuerdos
                    .FromSql("CALL matriz_acuerdos_gestion (@id_campania, @fecha_inicio, @fecha_fin)",
                        parameters: new[] {Id_Campania, Fecha_Inicio, Fecha_Fin}).ToListAsync();
                
               
                if (consultaAcuerdosXgestion.Count == 1)
                    if (consultaAcuerdosXgestion.Select(x => x.Noproducto).FirstOrDefault() == null)
                    {
                        consultaAcuerdosXgestion = new List<CrmMatrizAcuerdos>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaAcuerdosXgestion;
        }

        [Obsolete]
        public async Task<List<CrmMatrizPagos>> GetPagosXasignacion(int IdCampania, DateTime fechainicio, DateTime fechafin)
        {
            List<CrmMatrizPagos> consultaPagosXasignacion = new List<CrmMatrizPagos>();
            try
            {
                var Id_Campania = new MySqlParameter("id_campania", IdCampania);
                var Fecha_Inicio = new  MySqlParameter("fecha_inicio", fechainicio);
                var Fecha_Fin = new  MySqlParameter("fecha_fin", fechafin);

                consultaPagosXasignacion = await _context.CrmMatrizPagos
                    .FromSql("CALL matriz_pagos_asignacion (@id_campania, @fecha_inicio, @fecha_fin)",
                        parameters: new[] {Id_Campania, Fecha_Inicio, Fecha_Fin}).ToListAsync();
                
               
                if (consultaPagosXasignacion.Count == 1)
                    if (consultaPagosXasignacion.Select(x => x.Noproducto).FirstOrDefault() == null)
                    {
                        consultaPagosXasignacion = new List<CrmMatrizPagos>();
                    }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return consultaPagosXasignacion;
        }
        
    }
}
