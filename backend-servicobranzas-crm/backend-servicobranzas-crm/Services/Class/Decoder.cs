using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;

namespace backend_servicobranzas_crm.Services.Class
{
    public class Decoder 
    {
        public uint GetDecoder(HttpContext context)
        {
            // Obtener idusuario desde el token
                var accessToken = context.Request.Headers["Authorization"];
                string tokenString = accessToken.ToString();
                var jwtEncodedString = tokenString.Substring(7); // trim 'Bearer'
                var token = new JwtSecurityToken(jwtEncodedString: jwtEncodedString);
                uint idusuario = Convert.ToUInt32(token.Claims.First(c => c.Type == "id").Value);

                return idusuario;
        }
    }
}