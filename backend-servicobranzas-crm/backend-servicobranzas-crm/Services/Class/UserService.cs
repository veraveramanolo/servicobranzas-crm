﻿using System;
using System.Collections.Generic;
using backend_servicobranzas_crm.Modelo.Common;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Request;
using backend_servicobranzas_crm.Modelo.Response;
using backend_servicobranzas_crm.Services.Interface;
using backend_servicobranzas_crm.Tools;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace backend_servicobranzas_crm.Services.Class
{
    public class UserService : IUserService
    {
        private readonly ps_crmContext _context;
        private readonly UserSecret _userSecret;
        private IUserService _userServiceImplementation;


        public UserService(ps_crmContext context, IOptions<UserSecret> userSecret)
        {
            _context = context;
            _userSecret = userSecret.Value;
        }

        public UserResponse Autentificacion(AuthRequest authRequest)
        {
            UserResponse userResponse = null;

            string password = authRequest.Password;

            var usuario = _context.AclUsuario.FirstOrDefault(x => x.UserName == authRequest.User && x.Md5Password == password);

            if (usuario != null)
            {
                userResponse = new UserResponse
                {
                    Usuario = usuario,
                    Token = generateJwtToken(usuario),
                    Menu = GetRutasAccesos(usuario.IdRol)
                };
            }


            return userResponse;

        }

        public AclUsuario GetById(int id)
        {
            return _context.AclUsuario.FirstOrDefault(x => x.IdUsuario == id);
        }


        public List<NavItemsCabecera> GetRutasAccesos(uint idUser)
        {
            List<NavItemsCabecera> listNavItemsCabecera = new List<NavItemsCabecera>();
            NavItemsCabecera itemsCabecera = new NavItemsCabecera();
            List<NavItemsResponse> listnavItemsResponses = new List<NavItemsResponse>();
            NavItemsResponse itemsResponse = new NavItemsResponse();
            Child childData = new Child();
            List<Child> listChildren = new List<Child>(); 
            try
            {

                var listanivel1 = _context.AclAccesosRoles
                    .Where(x => x.IdRol == idUser && x.IdModuloNavigation.ModuloPadre == "SI" && x.IdModuloNavigation.IdParentModulo == null && x.Anulado == "NO"
                        && x.IdModuloNavigation.Anulado == "NO")
                    .Select(x=>x.IdModuloNavigation).ToList();

                if (listanivel1.Any())
                {
                    foreach (var nivel in listanivel1)
                    {
                        var listanivel2 = _context.AclAccesosRoles
                            .Where(x => x.IdRol == idUser && x.IdModuloNavigation.ModuloPadre == "NO" && x.IdModuloNavigation.IdParentModulo == nivel.IdModulo && x.Anulado == "NO" && x.IdModuloNavigation.Anulado == "NO")
                            .Select(x => x.IdModuloNavigation).ToList();

                        if (listanivel2.Any())
                        {
                            listnavItemsResponses = new List<NavItemsResponse>();

                            foreach (var nivel2 in listanivel2)
                            {
                                var listanivel3 = _context.AclAccesosRoles
                                    .Where(x => x.IdRol == idUser && x.IdModuloNavigation.ModuloPadre == "NO" && x.IdModuloNavigation.IdParentModulo == nivel2.IdModulo && x.Anulado == "NO" && x.IdModuloNavigation.Anulado == "NO")
                                    .Select(x => x.IdModuloNavigation).ToList();

                                if (listanivel3.Any())
                                {
                                    listChildren = new List<Child>();

                                    foreach (var nivel3 in listanivel3)
                                    {
                                      childData = new Child()
                                      {
                                          icon = nivel3.Icono,
                                          name = nivel3.Nombre,
                                          url = nivel.RutaDirectorio + nivel2.RutaDirectorio + nivel3.RutaDirectorio
                                      };  

                                      listChildren.Add(childData);
                                    }
                                }

                                itemsResponse = new NavItemsResponse()
                                {
                                    icon = nivel2.Icono,
                                    name = nivel2.Nombre,
                                    url = nivel.RutaDirectorio + nivel2.RutaDirectorio,
                                    children = listChildren.Any() ? listChildren : null 
                                };

                                listnavItemsResponses.Add(itemsResponse);
                            }
                            
                        }

                        itemsCabecera = new NavItemsCabecera()
                        {
                            icon = nivel.Icono,
                            name = nivel.Nombre,
                            url = nivel.RutaDirectorio,
                            children = listnavItemsResponses.Any() ? listnavItemsResponses : null
                        };

                        listNavItemsCabecera.Add(itemsCabecera);
                    }
                }

            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return listNavItemsCabecera;

        }

        private string generateJwtToken(AclUsuario usuario)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var llave = Encoding.ASCII.GetBytes(_userSecret.LlaveSecreta);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(
                    new Claim[]
                    {
                        //new Claim(ClaimTypes.NameIdentifier, authRequest.User),
                        new Claim("id", usuario.IdUsuario.ToString())
                    }),
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = 
                    new SigningCredentials(new SymmetricSecurityKey(llave),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
