﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;

namespace backend_servicobranzas_crm.Services.Interface
{
    public interface ICampania_Gestion
    {
        Task<List<CrmConsultaGestionCampania>> GetCuentasCampania(int IdCampania, int idUsuario);
    }
}
