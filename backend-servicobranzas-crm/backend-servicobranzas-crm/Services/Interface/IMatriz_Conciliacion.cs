using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;

namespace backend_servicobranzas_crm.Services.Interface
{
    public interface IMatriz_Conciliacion
    {
        Task<List<CrmMatrizAcuerdos>> GetAcuerdosXasignacion(int IdCampania, DateTime Fechainicio, DateTime Fechafin);
        Task<List<CrmMatrizAcuerdos>> GetAcuerdosXgestion(int IdCampania, DateTime fechainicio, DateTime fechafin);
    }
}
