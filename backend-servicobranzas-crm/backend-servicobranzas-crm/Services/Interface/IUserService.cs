﻿using System.Collections.Generic;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Request;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Services.Interface
{
    public interface IUserService
    {
        UserResponse Autentificacion(AuthRequest authRequest);
        AclUsuario GetById(int idUsuario);

        List<NavItemsCabecera> GetRutasAccesos(uint idUser);
    }
}
