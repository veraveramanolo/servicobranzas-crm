using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class ps_crmContext : DbContext
    {
        public ps_crmContext()
        {
        }

        public ps_crmContext(DbContextOptions<ps_crmContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AclAccesosRoles> AclAccesosRoles { get; set; }
        public virtual DbSet<AclAcciones> AclAcciones { get; set; }
        public virtual DbSet<AclArea> AclArea { get; set; }
        public virtual DbSet<AclCiudad> AclCiudad { get; set; }
        public virtual DbSet<AclDepto> AclDepto { get; set; }
        public virtual DbSet<AclDistrito> AclDistrito { get; set; }
        public virtual DbSet<AclEfectoAcuerdo> AclEfectoAcuerdo { get; set; }
        public virtual DbSet<AclEstados> AclEstados { get; set; }
        public virtual DbSet<AclListaAsesoresasig> AclListaAsesoresasig { get; set; }
        public virtual DbSet<AclModuloAcciones> AclModuloAcciones { get; set; }
        public virtual DbSet<AclModulos> AclModulos { get; set; }
        public virtual DbSet<AclMulticanalEstados> AclMulticanalEstados { get; set; }
        public virtual DbSet<AclParametroObjetivo> AclParametroObjetivo { get; set; }
        public virtual DbSet<AclParametros> AclParametros { get; set; }
        public virtual DbSet<AclPlantilla> AclPlantilla { get; set; }
        public virtual DbSet<AclRegion> AclRegion { get; set; }
        public virtual DbSet<AclRelacion> AclRelacion { get; set; }
        public virtual DbSet<AclRoles> AclRoles { get; set; }
        public virtual DbSet<AclTipoContacto> AclTipoContacto { get; set; }
        public virtual DbSet<AclTipoInfo> AclTipoInfo { get; set; }
        public virtual DbSet<AclTipoObjetivo> AclTipoObjetivo { get; set; }
        public virtual DbSet<AclUsuario> AclUsuario { get; set; }
        public virtual DbSet<AclZona> AclZona { get; set; }
        public virtual DbSet<CrmArbolGestion> CrmArbolGestion { get; set; }
        public virtual DbSet<CrmArbolGestionTipoContacto> CrmArbolGestionTipoContacto { get; set; }
        public virtual DbSet<CrmArbolGestionTipoGestion> CrmArbolGestionTipoGestion { get; set; }
        public virtual DbSet<CrmAsignacionCab> CrmAsignacionCab { get; set; }
        public virtual DbSet<CrmAsignacionDet> CrmAsignacionDet { get; set; }
        public virtual DbSet<CrmCampania> CrmCampania { get; set; }
        public virtual DbSet<CrmCartera> CrmCartera { get; set; }
        public virtual DbSet<CrmCaja> CrmCaja { get; set; }
        public virtual DbSet<CrmConsultaGestionCampania> CrmConsultaGestionCampania { get; set; }
        public virtual DbSet<CrmConsultaGestionCampaniaData> CrmConsultaGestionCampaniaData { get; set; }
        public virtual DbSet<CrmCuentas> CrmCuentas { get; set; }
        public virtual DbSet<CrmCuentasCampania> CrmCuentasCampania { get; set; }
        public virtual DbSet<CrmCuentasCampaniaAcuerdos> CrmCuentasCampaniaAcuerdos { get; set; }
        public virtual DbSet<CrmCuentasCampaniaDetalleHistorico> CrmCuentasCampaniaDetalleHistorico { get; set; }
        public virtual DbSet<CrmCuentasCampaniaGestor> CrmCuentasCampaniaGestor { get; set; }
        public virtual DbSet<CrmCuentasCampaniaPagos> CrmCuentasCampaniaPagos { get; set; }
        public virtual DbSet<CrmCuentasCampaniaProductos> CrmCuentasCampaniaProductos { get; set; }
        public virtual DbSet<CrmCuentasCampaniaProductosArticulos> CrmCuentasCampaniaProductosArticulos { get; set; }
        public virtual DbSet<CrmCuentasCampaniaProductosDetalle> CrmCuentasCampaniaProductosDetalle { get; set; }
        public virtual DbSet<CrmCuentasContacto> CrmCuentasContacto { get; set; }
        public virtual DbSet<CrmCuentasDetalle> CrmCuentasDetalle { get; set; }
        public virtual DbSet<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
        public virtual DbSet<CrmCuentasEmail> CrmCuentasEmail { get; set; }
        public virtual DbSet<CrmCuentasNotas> CrmCuentasNotas { get; set; }
        public virtual DbSet<CrmCuentasTelefono> CrmCuentasTelefono { get; set; }
        public virtual DbSet<CrmGestion> CrmGestion { get; set; }
        public virtual DbSet<CrmMatrizAcuerdos> CrmMatrizAcuerdos { get; set; }
        public virtual DbSet<CrmMatrizPagos> CrmMatrizPagos { get; set; }
        public virtual DbSet<CrmMulticanal> CrmMulticanal { get; set; }
        public virtual DbSet<CrmMulticanalCab> CrmMulticanalCab { get; set; }
        public virtual DbSet<CrmMulticanalDetalle> CrmMulticanalDetalle { get; set; }
        public virtual DbSet<CrmMulticanalMarcadores> CrmMulticanalMarcadores { get; set; }
        public virtual DbSet<CrmPausas> CrmPausas { get; set; }
        public virtual DbSet<CrmPausasUsuario> CrmPausasUsuario { get; set; }
        public virtual DbSet<CrmTareaCuentaCampania> CrmTareaCuentaCampania { get; set; }
        public virtual DbSet<CrmTareaGestor> CrmTareaGestor { get; set; }
        public virtual DbSet<CrmTareaObjetivos> CrmTareaObjetivos { get; set; }
        public virtual DbSet<CrmTareas> CrmTareas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;database=ps_crm;user=mvera;password=P4los4nt0", x => x.ServerVersion("10.5.8-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AclAccesosRoles>(entity =>
            {
                entity.HasKey(e => e.IdAccesoRol)
                    .HasName("PRIMARY");

                entity.ToTable("acl_accesos_roles");

                entity.HasIndex(e => e.IdModulo)
                    .HasName("FK__acl_modulos");

                entity.HasIndex(e => e.IdRol)
                    .HasName("FK_acl_accesos_roles_acl_roles");

                entity.Property(e => e.IdAccesoRol).HasColumnName("id_acceso_rol");

                entity.Property(e => e.Anulado)
                    .IsRequired()
                    .HasColumnName("anulado")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'NO'")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.IdRol).HasColumnName("id_rol");

                entity.Property(e => e.UsrCreacion)
                    .IsRequired()
                    .HasColumnName("usr_creacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrModificacion)
                    .HasColumnName("usr_modificacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.AclAccesosRoles)
                    .HasForeignKey(d => d.IdModulo)
                    .HasConstraintName("FK__acl_modulos");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.AclAccesosRoles)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_acl_accesos_roles_acl_roles");
            });

            modelBuilder.Entity<AclAcciones>(entity =>
            {
                entity.HasKey(e => e.IdAccion)
                    .HasName("PRIMARY");

                entity.ToTable("acl_acciones");

                entity.HasIndex(e => e.Codigo)
                    .HasName("codigo")
                    .IsUnique();

                entity.Property(e => e.IdAccion).HasColumnName("id_accion");

                entity.Property(e => e.Anulado)
                    .IsRequired()
                    .HasColumnName("anulado")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'NO'")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasColumnName("codigo")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Icono)
                    .HasColumnName("icono")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrCreacion)
                    .IsRequired()
                    .HasColumnName("usr_creacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrModificacion)
                    .HasColumnName("usr_modificacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<AclArea>(entity =>
            {
                entity.HasKey(e => e.IdArea)
                    .HasName("PRIMARY");

                entity.ToTable("acl_area");

                entity.Property(e => e.IdArea).HasColumnName("id_area");

                entity.Property(e => e.ValorArea)
                    .HasColumnName("valor_area")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclCiudad>(entity =>
            {
                entity.HasKey(e => e.IdCiudad)
                    .HasName("PRIMARY");

                entity.ToTable("acl_ciudad");

                entity.HasIndex(e => e.IdDepto)
                    .HasName("FK_acl_ciudad_acl_depto");

                entity.Property(e => e.IdCiudad).HasColumnName("id_ciudad");

                entity.Property(e => e.CiudadValor)
                    .HasColumnName("ciudad_valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdDepto).HasColumnName("id_depto");

                entity.HasOne(d => d.IdDeptoNavigation)
                    .WithMany(p => p.AclCiudad)
                    .HasForeignKey(d => d.IdDepto)
                    .HasConstraintName("FK_acl_ciudad_acl_depto");
            });

            modelBuilder.Entity<AclDepto>(entity =>
            {
                entity.HasKey(e => e.IdDepto)
                    .HasName("PRIMARY");

                entity.ToTable("acl_depto");

                entity.HasIndex(e => e.IdRegion)
                    .HasName("fk_acl_depto_1_idx");

                entity.Property(e => e.IdDepto).HasColumnName("id_depto");

                entity.Property(e => e.DeptoValor)
                    .IsRequired()
                    .HasColumnName("depto_valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdRegion).HasColumnName("id_region");

                entity.HasOne(d => d.IdRegionNavigation)
                    .WithMany(p => p.AclDepto)
                    .HasForeignKey(d => d.IdRegion)
                    .HasConstraintName("fk_acl_depto_acl_region");
            });

            modelBuilder.Entity<AclDistrito>(entity =>
            {
                entity.HasKey(e => e.IdDistrito)
                    .HasName("PRIMARY");

                entity.ToTable("acl_distrito");
                entity.HasIndex(e => e.IdCiudad)
                    .HasName("FK_acl_distrito_acl_ciudad");

                entity.Property(e => e.IdDistrito).HasColumnName("id_distrito");

                entity.Property(e => e.DistritoValor)
                    .HasColumnName("distrito_valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCiudad).HasColumnName("id_ciudad");

                entity.HasOne(d => d.IdCiudadNavigation)
                    .WithMany(p => p.AclDistrito)
                    .HasForeignKey(d => d.IdCiudad)
                    .HasConstraintName("FK_acl_distrito_acl_ciudad");
            });

            modelBuilder.Entity<AclEfectoAcuerdo>(entity =>
            {
                entity.HasKey(e => e.IdEfecto)
                    .HasName("PRIMARY");

                entity.ToTable("acl_efecto_acuerdo");

                entity.Property(e => e.IdEfecto)
                    .HasColumnName("id_efecto")
                    .ValueGeneratedNever();

                entity.Property(e => e.EfectoValor)
                    .HasColumnName("efecto_valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclEstados>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("acl_estados");

                entity.Property(e => e.Alerta)
                    .HasColumnName("alerta")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Texto)
                    .HasColumnName("texto")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclListaAsesoresasig>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("acl_lista_asesoresasig");

                entity.Property(e => e.Activos).HasColumnName("activos");

                entity.Property(e => e.Asesor)
                    .HasColumnName("asesor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.AsignadosClientes).HasColumnName("asignadosClientes");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.Inactivos).HasColumnName("inactivos");
            });

            modelBuilder.Entity<AclModuloAcciones>(entity =>
            {
                entity.HasKey(e => e.IdModuloAccion)
                    .HasName("PRIMARY");

                entity.ToTable("acl_modulo_acciones");

                entity.HasIndex(e => e.IdAccion)
                    .HasName("FK_acl_modulo_acciones_acl_acciones");

                entity.HasIndex(e => e.IdModulo)
                    .HasName("FK_acl_modulo_acciones_acl_modulos");

                entity.Property(e => e.IdModuloAccion).HasColumnName("id_modulo_accion");

                entity.Property(e => e.Anulado)
                    .IsRequired()
                    .HasColumnName("anulado")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'NO'")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdAccion).HasColumnName("id_accion");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.UsrCreacion)
                    .IsRequired()
                    .HasColumnName("usr_creacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrModificacion)
                    .HasColumnName("usr_modificacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.IdAccionNavigation)
                    .WithMany(p => p.AclModuloAcciones)
                    .HasForeignKey(d => d.IdAccion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_acl_modulo_acciones_acl_acciones");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.AclModuloAcciones)
                    .HasForeignKey(d => d.IdModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_acl_modulo_acciones_acl_modulos");
            });

            modelBuilder.Entity<AclModulos>(entity =>
            {
                entity.HasKey(e => e.IdModulo)
                    .HasName("PRIMARY");

                entity.ToTable("acl_modulos");

                entity.HasIndex(e => e.Codigo)
                    .HasName("codigo")
                    .IsUnique();

                entity.HasIndex(e => e.IdParentModulo)
                    .HasName("FK_acl_modulos_acl_modulos");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.Anulado)
                    .HasColumnName("anulado")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'NO'")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasColumnName("codigo")
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Icono)
                    .HasColumnName("icono")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdParentModulo).HasColumnName("id_parent_modulo");

                entity.Property(e => e.ModuloPadre)
                    .HasColumnName("modulo_padre")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.OrdenPresentacion).HasColumnName("orden_presentacion");

                entity.Property(e => e.RutaDirectorio)
                    .HasColumnName("ruta_directorio")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrCreacion)
                    .HasColumnName("usr_creacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrModificacion)
                    .HasColumnName("usr_modificacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.IdParentModuloNavigation)
                    .WithMany(p => p.InverseIdParentModuloNavigation)
                    .HasForeignKey(d => d.IdParentModulo)
                    .HasConstraintName("FK_acl_modulos_acl_modulos");
            });

            modelBuilder.Entity<AclMulticanalEstados>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("acl_multicanal_estados");

                entity.Property(e => e.Gestion)
                    .HasColumnName("gestion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Mejorgestion)
                    .HasColumnName("mejorgestion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclParametroObjetivo>(entity =>
            {
                entity.HasKey(e => e.IdParametro)
                    .HasName("PRIMARY");

                entity.ToTable("acl_parametro_objetivo");

                entity.Property(e => e.IdParametro).HasColumnName("id_parametro");

                entity.Property(e => e.ValorParametro)
                    .IsRequired()
                    .HasColumnName("valor_parametro")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclParametros>(entity =>
            {
                entity.HasKey(e => e.IdParametro)
                    .HasName("PRIMARY");

                entity.ToTable("acl_parametros");

                entity.HasIndex(e => e.IdModulo)
                    .HasName("fk_acl_parametros_acl_modulos_idx");

                entity.Property(e => e.IdParametro).HasColumnName("id_parametro");

                entity.Property(e => e.CampoRef1)
                    .HasColumnName("campo_ref1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CampoRef2).HasColumnName("campo_ref2");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("enum('A','I')")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.NombreParametro)
                    .IsRequired()
                    .HasColumnName("nombre_parametro")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UserCreacion).HasColumnName("user_creacion");

                entity.Property(e => e.UserModificacion).HasColumnName("user_modificacion");

                entity.Property(e => e.ValorParametro)
                    .IsRequired()
                    .HasColumnName("valor_parametro")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.AclParametros)
                    .HasForeignKey(d => d.IdModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_acl_parametros_acl_modulos");
            });

            modelBuilder.Entity<AclPlantilla>(entity =>
            {
                entity.HasKey(e => e.IdPlantilla)
                    .HasName("PRIMARY");

                entity.ToTable("acl_plantilla");

                entity.Property(e => e.IdPlantilla)
                    .HasColumnName("id_plantilla")
                    .HasComment("Identificador de plantilla");

                entity.Property(e => e.BodyPlantilla)
                    .HasColumnName("body_plantilla")
                    .HasColumnType("varchar(200)")
                    .HasComment("Cuerpo de plantilla")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.EstadoPlantilla)
                    .IsRequired()
                    .HasColumnName("estado_plantilla")
                    .HasColumnType("enum('A','I')")
                    .HasDefaultValueSql("'A'")
                    .HasComment("Estado: Activo, Inactivo")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("fecha_actualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.NombrePlantilla)
                    .IsRequired()
                    .HasColumnName("nombre_plantilla")
                    .HasColumnType("varchar(45)")
                    .HasComment("Nombre de plantilla")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.TipoPlantilla)
                    .IsRequired()
                    .HasColumnName("tipo_plantilla")
                    .HasColumnType("varchar(30)")
                    .HasComment("Tipo: Sms, Dinomi, Email")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UsrCreacion).HasColumnName("usr_creacion");

                entity.Property(e => e.UsrModificacion).HasColumnName("usr_modificacion");
            });

            modelBuilder.Entity<AclRegion>(entity =>
            {
                entity.HasKey(e => e.IdRegion)
                    .HasName("PRIMARY");

                entity.ToTable("acl_region");

                entity.Property(e => e.IdRegion).HasColumnName("id_region");

                entity.Property(e => e.RegionValor)
                    .IsRequired()
                    .HasColumnName("region_valor")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclRelacion>(entity =>
            {
                entity.HasKey(e => e.IdRelacion)
                    .HasName("PRIMARY");

                entity.ToTable("acl_relacion");

                entity.Property(e => e.IdRelacion).HasColumnName("id_relacion");

                entity.Property(e => e.ValorRelacion)
                    .HasColumnName("valor_relacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclRoles>(entity =>
            {
                entity.HasKey(e => e.IdRol)
                    .HasName("PRIMARY");

                entity.ToTable("acl_roles");

                entity.Property(e => e.IdRol).HasColumnName("id_rol");

                entity.Property(e => e.Anulado)
                    .HasColumnName("anulado")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'NO'")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(80)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.UsrCreacion)
                    .HasColumnName("usr_creacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrModificacion)
                    .HasColumnName("usr_modificacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<AclTipoContacto>(entity =>
            {
                entity.HasKey(e => e.IdTipoContacto)
                    .HasName("PRIMARY");

                entity.ToTable("acl_tipo_contacto");

                entity.Property(e => e.IdTipoContacto).HasColumnName("id_tipo_contacto");

                entity.Property(e => e.Texto)
                    .IsRequired()
                    .HasColumnName("texto")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclTipoInfo>(entity =>
            {
                entity.HasKey(e => e.IdTipoInfo)
                    .HasName("PRIMARY");

                entity.ToTable("acl_tipo_info");

                entity.Property(e => e.IdTipoInfo).HasColumnName("id_tipo_info");

                entity.Property(e => e.InfoNombre)
                    .HasColumnName("info_nombre")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.InfoValor)
                    .HasColumnName("info_valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclTipoObjetivo>(entity =>
            {
                entity.HasKey(e => e.IdTipoobjetivo)
                    .HasName("PRIMARY");

                entity.ToTable("acl_tipo_objetivo");

                entity.Property(e => e.IdTipoobjetivo).HasColumnName("id_tipoobjetivo");

                entity.Property(e => e.ValorTipoobjetivo)
                    .IsRequired()
                    .HasColumnName("valor_tipoobjetivo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<AclUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PRIMARY");

                entity.ToTable("acl_usuario");

                entity.HasIndex(e => e.Cedula)
                    .HasName("cedula")
                    .IsUnique();

                entity.HasIndex(e => e.IdRol)
                    .HasName("FK_acl_usuario_acl_roles");

                entity.HasIndex(e => e.UserName)
                    .HasName("user_name")
                    .IsUnique();

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.Agentname)
                    .HasColumnName("agentname")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Agentpass)
                    .HasColumnName("agentpass")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Anulado)
                    .HasColumnName("anulado")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'NO'")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Apellidos)
                    .HasColumnName("apellidos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cargo)
                    .HasColumnName("cargo")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cedula)
                    .IsRequired()
                    .HasColumnName("cedula")
                    .HasColumnType("varchar(13)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Codigo)
                    .HasColumnName("codigo")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Departamento)
                    .HasColumnName("departamento")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdRol).HasColumnName("id_rol");

                entity.Property(e => e.Md5Password)
                    .IsRequired()
                    .HasColumnName("md5_password")
                    .HasColumnType("varchar(500)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasColumnName("nombres")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Telefono)
                    .HasColumnName("telefono")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("user_name")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrCreacion)
                    .IsRequired()
                    .HasColumnName("usr_creacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UsrModificacion)
                    .HasColumnName("usr_modificacion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.AclUsuario)
                    .HasForeignKey(d => d.IdRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_acl_usuario_acl_roles");
            });

            modelBuilder.Entity<AclZona>(entity =>
            {
                entity.HasKey(e => e.IdZona)
                    .HasName("PRIMARY");

                entity.ToTable("acl_zona");

                entity.Property(e => e.IdZona).HasColumnName("id_zona");

                entity.Property(e => e.ZonaValor)
                    .HasColumnName("zona_valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<CrmArbolGestion>(entity =>
            {
                entity.ToTable("crm_arbol_gestion");

                entity.HasIndex(e => e.IdParent)
                    .HasName("fk_arbol_gestion_idx");

                entity.HasIndex(e => e.IdTipoContacto)
                    .HasName("fk_arbol_gestion2_idx");

                entity.HasIndex(e => e.IdTipoGestion)
                    .HasName("fk_arbol_gestion3_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Alerta)
                    .HasColumnName("alerta")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdParent).HasColumnName("id_parent");

                entity.Property(e => e.IdTipoContacto).HasColumnName("id_tipo_contacto");

                entity.Property(e => e.IdTipoGestion).HasColumnName("id_tipo_gestion");

                entity.Property(e => e.LabelEstado)
                    .HasColumnName("label_estado")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.LabelNivel2)
                    .HasColumnName("label_nivel2")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Peso).HasColumnName("peso");

                entity.HasOne(d => d.IdParentNavigation)
                    .WithMany(p => p.InverseIdParentNavigation)
                    .HasForeignKey(d => d.IdParent)
                    .HasConstraintName("fk_arbol_gestion");

                entity.HasOne(d => d.IdTipoContactoNavigation)
                    .WithMany(p => p.CrmArbolGestion)
                    .HasForeignKey(d => d.IdTipoContacto)
                    .HasConstraintName("fk_arbol_gestion2");

                entity.HasOne(d => d.IdTipoGestionNavigation)
                    .WithMany(p => p.CrmArbolGestion)
                    .HasForeignKey(d => d.IdTipoGestion)
                    .HasConstraintName("fk_arbol_gestion3");

                entity.Property(e => e.IdArbolGestion)
                    .HasColumnName("id_arbol_gestion")
                    .HasColumnType("int(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

            });

            modelBuilder.Entity<CrmArbolGestionTipoContacto>(entity =>
            {
                entity.ToTable("crm_arbol_gestion_tipo_contacto");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Texto)
                    .HasColumnName("texto")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<CrmArbolGestionTipoGestion>(entity =>
            {
                entity.ToTable("crm_arbol_gestion_tipo_gestion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Texto)
                    .HasColumnName("texto")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<CrmAsignacionCab>(entity =>
            {
                entity.HasKey(e => e.IdAsignacion)
                    .HasName("PRIMARY");

                entity.ToTable("crm_asignacion_cab");

                entity.Property(e => e.IdAsignacion).HasColumnName("id_asignacion");

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("fecha_actualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdTarea).HasColumnName("id_tarea");

                entity.Property(e => e.Tipoasignacion)
                    .IsRequired()
                    .HasColumnName("tipoasignacion")
                    .HasColumnType("enum('M','A')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UserActualizacion).HasColumnName("user_actualizacion");

                entity.Property(e => e.UserCreacion).HasColumnName("user_creacion");
            });

            modelBuilder.Entity<CrmAsignacionDet>(entity =>
            {
                entity.HasKey(e => e.IdAsignacionDet)
                    .HasName("PRIMARY");

                entity.ToTable("crm_asignacion_det");

                entity.HasIndex(e => e.IdAsignacion)
                    .HasName("fk_crm_asignacion_det_crm_asignacion_cab_idx");

                entity.Property(e => e.IdAsignacionDet).HasColumnName("id_asignacion_det");

                entity.Property(e => e.IdAsignacion).HasColumnName("id_asignacion");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.HasOne(d => d.IdAsignacionNavigation)
                    .WithMany(p => p.CrmAsignacionDet)
                    .HasForeignKey(d => d.IdAsignacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_crm_asignacion_det_crm_asignacion_cab");
            });

            modelBuilder.Entity<CrmCampania>(entity =>
            {
                entity.ToTable("crm_campania");

                entity.HasIndex(e => e.IdCartera)
                    .HasName("fk_cartera_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I','V','C')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("fecha_actualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaFin)
                    .HasColumnName("fecha_fin")
                    .HasColumnType("date");

                entity.Property(e => e.FechaInicio)
                    .HasColumnName("fecha_inicio")
                    .HasColumnType("date");

                entity.Property(e => e.IdCartera).HasColumnName("id_cartera");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UserActualizacion).HasColumnName("user_actualizacion");

                entity.Property(e => e.UserCreacion).HasColumnName("user_creacion");

                entity.HasOne(d => d.IdCarteraNavigation)
                    .WithMany(p => p.CrmCampania)
                    .HasForeignKey(d => d.IdCartera)
                    .HasConstraintName("fk_cartera");

                entity.Property(d => d.IdArbolGestion)
                    .HasColumnName("id_arbol_gestion")
                    .HasColumnType("int(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<CrmCartera>(entity =>
            {
                entity.ToTable("crm_cartera");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasColumnType("enum('Cedente','Vendida')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<CrmCaja>(entity =>
            {
                entity.ToTable("crm_caja");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Identificacion)
                    .HasColumnName("identificacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Comentario)
                    .HasColumnName("comentario")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdGestor)
                    .HasColumnName("id_gestor");

                entity.Property(e => e.IdCampania)
                    .HasColumnName("id_campania");

                entity.Property(e => e.IdCartera)
                    .HasColumnName("id_cartera");

            });

            modelBuilder.Entity<CrmConsultaGestionCampania>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("crm_consulta_gestion_campania");

                entity.Property(e => e.Acuerdo)
                    .HasColumnName("acuerdo")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Asesor)
                    .HasColumnName("asesor")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cedula)
                    .HasColumnName("cedula")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cuotaspendientes)
                    .HasColumnName("cuotaspendientes")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Deudatotal)
                    .HasColumnName("deudatotal")
                    .HasColumnType("float(10,4)")
                    .HasDefaultValueSql("'0.0000'");

                entity.Property(e => e.Deudavencida)
                    .HasColumnName("deudavencida")
                    .HasColumnType("float(10,4) unsigned")
                    .HasDefaultValueSql("'0.0000'");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra1)
                    .HasColumnName("EXTRA1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra10)
                    .HasColumnName("EXTRA10")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra2)
                    .HasColumnName("EXTRA2")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra3)
                    .HasColumnName("EXTRA3")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra4)
                    .HasColumnName("EXTRA4")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra5)
                    .HasColumnName("EXTRA5")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra6)
                    .HasColumnName("EXTRA6")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra7)
                    .HasColumnName("EXTRA7")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra8)
                    .HasColumnName("EXTRA8")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra9)
                    .HasColumnName("EXTRA9")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Fechamejorestado)
                    .HasColumnName("fechamejorestado")
                    .HasColumnType("datetime");

                entity.Property(e => e.Idcuenta).HasColumnName("idcuenta");

                entity.Property(e => e.Idcuentacampania).HasColumnName("idcuentacampania");

                entity.Property(e => e.Idproducto).HasColumnName("idproducto");

                entity.Property(e => e.LabelEstado)
                    .HasColumnName("label_estado")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Mejorestado)
                    .HasColumnName("mejorestado")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nombrecliente)
                    .HasColumnName("nombrecliente")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Pago)
                    .HasColumnName("pago")
                    .HasColumnType("varchar(100)")
                    .HasDefaultValueSql("''")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ultimagestion)
                    .HasColumnName("ultimagestion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Valoracuerdo)
                    .HasColumnName("valoracuerdo")
                    .HasColumnType("float(10,4) unsigned")
                    .HasDefaultValueSql("'0.0000'");

                entity.Property(e => e.Valorpago)
                    .HasColumnName("valorpago")
                    .HasColumnType("float(10,4) unsigned")
                    .HasDefaultValueSql("'0.0000'");
            });

            modelBuilder.Entity<CrmConsultaGestionCampaniaData>(entity =>
            {
                entity.ToTable("crm_consulta_gestion_campania_data");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Acuerdo)
                    .HasColumnName("acuerdo")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Asesor)
                    .HasColumnName("asesor")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Cedula)
                    .HasColumnName("cedula")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Cuotaspendientes).HasColumnName("cuotaspendientes");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Idcuenta).HasColumnName("idcuenta");

                entity.Property(e => e.Idcuentacampania).HasColumnName("idcuentacampania");

                entity.Property(e => e.Idproducto).HasColumnName("idproducto");

                entity.Property(e => e.Montoinicial)
                    .HasColumnName("montoinicial")
                    .HasColumnType("float(10,4) unsigned");

                entity.Property(e => e.Nombrecliente)
                    .HasColumnName("nombrecliente")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Pago)
                    .HasColumnName("pago")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Ultimagestion)
                    .HasColumnName("ultimagestion")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Valoracuerdo)
                    .HasColumnName("valoracuerdo")
                    .HasColumnType("float(10,4) unsigned");

                entity.Property(e => e.Valorpago)
                    .HasColumnName("valorpago")
                    .HasColumnType("float(10,4) unsigned");
            });

            modelBuilder.Entity<CrmCuentas>(entity =>
            {
                entity.ToTable("crm_cuentas");

                entity.HasIndex(e => e.IdCampaniaInicial)
                    .HasName("fk_campania_idx");

                entity.HasIndex(e => e.IdCarteraInicial)
                    .HasName("fk_cartera_idx");

                entity.HasIndex(e => e.Identificacion)
                    .HasName("identificacion_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','E','P')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.GestorCreacion)
                    .HasColumnName("gestor_creacion")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCampaniaInicial).HasColumnName("id_campania_inicial");

                entity.Property(e => e.IdCarteraInicial).HasColumnName("id_cartera_inicial");

                entity.Property(e => e.Identificacion)
                    .HasColumnName("identificacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdCampaniaInicialNavigation)
                    .WithMany(p => p.CrmCuentas)
                    .HasForeignKey(d => d.IdCampaniaInicial)
                    .HasConstraintName("fk_campania_cuenta");

                entity.HasOne(d => d.IdCarteraInicialNavigation)
                    .WithMany(p => p.CrmCuentas)
                    .HasForeignKey(d => d.IdCarteraInicial)
                    .HasConstraintName("fk_cartera_cuenta");
            });

            modelBuilder.Entity<CrmCuentasCampania>(entity =>
            {
                entity.ToTable("crm_cuentas_campania");

                entity.HasIndex(e => e.IdArbolGestionMejorGestion)
                    .HasName("fk_crm_cuentas_campania_1_idx");

                entity.HasIndex(e => e.IdCampania)
                    .HasName("fk_cuentas_camp2_idx");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_cuentas_camp_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdArbolGestionMejorGestion).HasColumnName("id_arbol_gestion_mejor_gestion");

                entity.Property(e => e.IdCampania).HasColumnName("id_campania");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.HasOne(d => d.IdArbolGestionMejorGestionNavigation)
                    .WithMany(p => p.CrmCuentasCampania)
                    .HasForeignKey(d => d.IdArbolGestionMejorGestion)
                    .HasConstraintName("fk_crm_cuentas_campania_1");

                entity.HasOne(d => d.IdCampaniaNavigation)
                    .WithMany(p => p.CrmCuentasCampania)
                    .HasForeignKey(d => d.IdCampania)
                    .HasConstraintName("fk_cuentas_camp2");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmCuentasCampania)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_cuentas_camp");
            });

            modelBuilder.Entity<CrmCuentasCampaniaAcuerdos>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_acuerdos");

                entity.HasIndex(e => e.IdCuentaCampaniaProducto)
                    .HasName("FK_crm_cuentas_campania_acuerdos_crm_cuentas_campania_productos");

                entity.HasIndex(e => e.IdDireccion)
                    .HasName("FK_crm_cuentas_campania_acuerdos_crm_cuentas_direccion_idx");

                entity.HasIndex(e => e.IdGestor)
                    .HasName("FK_crm_cuentas_campania_acuerdos_acl_usuario");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CantModificacion).HasColumnName("cant_modificacion");

                entity.Property(e => e.Comentario)
                    .HasColumnName("comentario")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cuota).HasColumnName("cuota");

                entity.Property(e => e.Efecto)
                    .HasColumnName("efecto")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.EstadoNot)
                    .HasColumnName("estado_not")
                    .HasColumnType("enum('V','F')")
                    .HasDefaultValueSql("'V'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaAcuerdo)
                    .HasColumnName("fecha_acuerdo")
                    .HasColumnType("date");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaVisita)
                    .HasColumnName("fecha_visita")
                    .HasColumnType("datetime");

                entity.Property(e => e.HoraAcuerdo)
                    .HasColumnName("hora_acuerdo")
                    .HasColumnType("time");

                entity.Property(e => e.IdCuentaCampaniaProducto).HasColumnName("id_cuenta_campania_producto");

                entity.Property(e => e.IdDireccion).HasColumnName("id_direccion");

                entity.Property(e => e.IdGestor).HasColumnName("id_gestor");

                entity.Property(e => e.Plazo)
                    .HasColumnName("plazo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Probabilidad)
                    .HasColumnName("probabilidad")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.TipoAcuerdo)
                    .HasColumnName("tipo_acuerdo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UsrModificacion).HasColumnName("usr_modificacion");

                entity.Property(e => e.ValorAcuerdo)
                    .HasColumnName("valor_acuerdo")
                    .HasColumnType("float(10,2)");

                entity.HasOne(d => d.IdCuentaCampaniaProductoNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaAcuerdos)
                    .HasForeignKey(d => d.IdCuentaCampaniaProducto)
                    .HasConstraintName("FK_crm_cuentas_campania_acuerdos_crm_cuentas_campania_productos");

                entity.HasOne(d => d.IdDireccionNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaAcuerdos)
                    .HasForeignKey(d => d.IdDireccion)
                    .HasConstraintName("fk_crm_cuentas_campania_acuerdos_crm_cuentas_direccion");

                entity.HasOne(d => d.IdGestorNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaAcuerdos)
                    .HasForeignKey(d => d.IdGestor)
                    .HasConstraintName("FK_crm_cuentas_campania_acuerdos_acl_usuario");
            });

            modelBuilder.Entity<CrmCuentasCampaniaDetalleHistorico>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_detalle_historico");

                entity.HasIndex(e => e.IdCuentaCampania)
                    .HasName("fk_cuenta_campania_hist_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Campo)
                    .HasColumnName("campo")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuentaCampania).HasColumnName("id_cuenta_campania");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdCuentaCampaniaNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaDetalleHistorico)
                    .HasForeignKey(d => d.IdCuentaCampania)
                    .HasConstraintName("fk_cuenta_campania_hist");
            });

            modelBuilder.Entity<CrmCuentasCampaniaGestor>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_gestor");

                entity.HasIndex(e => e.IdCuentaCampania)
                    .HasName("FK_crm_cuentas_campania_gestor_crm_cuentas_campania");

                entity.HasIndex(e => e.IdUsuario)
                    .HasName("FK_crm_cuentas_campania_gestor_acl_usuario");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuentaCampania).HasColumnName("id_cuenta_campania");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.HasOne(d => d.IdCuentaCampaniaNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaGestor)
                    .HasForeignKey(d => d.IdCuentaCampania)
                    .HasConstraintName("FK_crm_cuentas_campania_gestor_crm_cuentas_campania");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaGestor)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_crm_cuentas_campania_gestor_acl_usuario");
            });

            modelBuilder.Entity<CrmCuentasCampaniaPagos>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_pagos");

                entity.HasIndex(e => e.IdCuentaCampaniaAcuerdo)
                    .HasName("FK_crm_cuentas_campania_pagos_crm_cuentas_campania_acuerdos");

                entity.HasIndex(e => e.IdCuentaCampaniaProducto)
                    .HasName("FK_crm_cuentas_campania_pagos_crm_cuentas_campania_productos");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cuota).HasColumnName("cuota");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaPago)
                    .HasColumnName("fecha_pago")
                    .HasColumnType("date");

                entity.Property(e => e.GestorIngreso)
                    .HasColumnName("gestor_ingreso")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.GestorOficial)
                    .HasColumnName("gestor_oficial")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.IdCuentaCampaniaAcuerdo).HasColumnName("id_cuenta_campania_acuerdo");

                entity.Property(e => e.IdCuentaCampaniaProducto).HasColumnName("id_cuenta_campania_producto");

                entity.Property(e => e.MedioPago)
                    .HasColumnName("medio_pago")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NumeroRecibo)
                    .HasColumnName("numero_recibo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Plazo)
                    .HasColumnName("plazo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.TipoPago)
                    .HasColumnName("tipo_pago")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ValorPago)
                    .HasColumnName("valor_pago")
                    .HasColumnType("float(10,4)");

                entity.HasOne(d => d.IdCuentaCampaniaAcuerdoNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaPagos)
                    .HasForeignKey(d => d.IdCuentaCampaniaAcuerdo)
                    .HasConstraintName("FK_crm_cuentas_campania_pagos_crm_cuentas_campania_acuerdos");

                entity.HasOne(d => d.IdCuentaCampaniaProductoNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaPagos)
                    .HasForeignKey(d => d.IdCuentaCampaniaProducto)
                    .HasConstraintName("FK_crm_cuentas_campania_pagos_crm_cuentas_campania_productos");
            });

            modelBuilder.Entity<CrmCuentasCampaniaProductos>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_productos");

                entity.HasIndex(e => e.IdCuentaCampania)
                    .HasName("fk_cuenta_campania_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CrmCuentasCampaniaProductoscol)
                    .HasColumnName("crm_cuentas_campania_productoscol")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CrmCuentasCampaniaProductoscol1)
                    .HasColumnName("crm_cuentas_campania_productoscol1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CuotasPendientes).HasColumnName("cuotas_pendientes");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Deudatotal)
                    .HasColumnName("deudatotal")
                    .HasColumnType("decimal(10,4)");

                entity.Property(e => e.Edadmor).HasColumnName("edadmor");

                entity.Property(e => e.IdCuentaCampania).HasColumnName("id_cuenta_campania");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(10,4)");

                entity.Property(e => e.Valorporvencer)
                    .HasColumnName("valorporvencer")
                    .HasColumnType("decimal(10,4)");

                entity.HasOne(d => d.IdCuentaCampaniaNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaProductos)
                    .HasForeignKey(d => d.IdCuentaCampania)
                    .HasConstraintName("fk_cuenta_campania");
            });

            modelBuilder.Entity<CrmCuentasCampaniaProductosArticulos>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_productos_articulos");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Articulo)
                    .HasColumnName("articulo")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuentaCampaniaProducto).HasColumnName("id_cuenta_campania_producto");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(10,4)");
            });

            modelBuilder.Entity<CrmCuentasCampaniaProductosDetalle>(entity =>
            {
                entity.ToTable("crm_cuentas_campania_productos_detalle");

                entity.HasIndex(e => e.IdCuentaCampaniaProducto)
                    .HasName("fk_cuentas_campania_productos_detalle_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('Cancelada','Pendiente')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuentaCampaniaProducto).HasColumnName("id_cuenta_campania_producto");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(10,4)");

                entity.HasOne(d => d.IdCuentaCampaniaProductoNavigation)
                    .WithMany(p => p.CrmCuentasCampaniaProductosDetalle)
                    .HasForeignKey(d => d.IdCuentaCampaniaProducto)
                    .HasConstraintName("fk_cuentas_campania_productos_detalle");
            });

            modelBuilder.Entity<CrmCuentasContacto>(entity =>
            {
                entity.ToTable("crm_cuentas_contacto");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_crm_cuentas_contacto_idx");

                entity.HasIndex(e => e.IdRelacion)
                    .HasName("fk_crm_cuentas_contacto_relacion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Documento)
                    .HasColumnName("documento")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("date");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.IdRelacion).HasColumnName("id_relacion");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Observacion)
                    .HasColumnName("observacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ObservacionI)
                    .HasColumnName("observacionI")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CedulaRef)
                    .HasColumnName("CEDULA_REF")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.TipoDatoRef)
                    .HasColumnName("tipo_dato_ref")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ValorRef)
                    .HasColumnName("valor_ref")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmCuentasContacto)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_crm_cuentas_contacto");

                entity.HasOne(d => d.IdRelacionNavigation)
                    .WithMany(p => p.CrmCuentasContacto)
                    .HasForeignKey(d => d.IdRelacion)
                    .HasConstraintName("fk_crm_cuentas_contacto_relacion");
            });

            modelBuilder.Entity<CrmCuentasDetalle>(entity =>
            {
                entity.ToTable("crm_cuentas_detalle");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_cuenta_detalle_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Campo)
                    .HasColumnName("campo")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.IdCuentaCampaniaProducto).HasColumnName("id_cuenta_campania_producto");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmCuentasDetalle)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_cuenta_detalle");
            });

            modelBuilder.Entity<CrmCuentasDireccion>(entity =>
            {
                entity.ToTable("crm_cuentas_direccion");

                entity.HasIndex(e => e.IdArea)
                    .HasName("fk_crm_cuentas_direccion_5_idx");

                entity.HasIndex(e => e.IdCiudad)
                    .HasName("fk_crm_cuentas_direccion_2_idx");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_cuentas_direccion_idx");

                entity.HasIndex(e => e.IdDepto)
                    .HasName("fk_crm_cuentas_direccion_1_idx");

                entity.HasIndex(e => e.IdDistrito)
                    .HasName("fk_crm_cuentas_direccion_3_idx");

                entity.HasIndex(e => e.IdZona)
                    .HasName("fk_crm_cuentas_direccion_4_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Barrio)
                    .HasColumnName("barrio")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("date");

                entity.Property(e => e.IdArea).HasColumnName("id_area");

                entity.Property(e => e.IdCiudad).HasColumnName("id_ciudad");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.IdDepto).HasColumnName("id_depto");

                entity.Property(e => e.IdDistrito).HasColumnName("id_distrito");

                entity.Property(e => e.IdZona).HasColumnName("id_zona");

                entity.Property(e => e.Info1)
                    .HasColumnName("info1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ObservacionI)
                    .HasColumnName("observacionI")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdAreaNavigation)
                    .WithMany(p => p.CrmCuentasDireccion)
                    .HasForeignKey(d => d.IdArea)
                    .HasConstraintName("fk_crm_cuentas_direccion_5");

                entity.HasOne(d => d.IdCiudadNavigation)
                    .WithMany(p => p.CrmCuentasDireccion)
                    .HasForeignKey(d => d.IdCiudad)
                    .HasConstraintName("fk_crm_cuentas_direccion_2");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmCuentasDireccion)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_cuentas_direccion");

                entity.HasOne(d => d.IdDeptoNavigation)
                    .WithMany(p => p.CrmCuentasDireccion)
                    .HasForeignKey(d => d.IdDepto)
                    .HasConstraintName("fk_crm_cuentas_direccion_1");

                entity.HasOne(d => d.IdDistritoNavigation)
                    .WithMany(p => p.CrmCuentasDireccion)
                    .HasForeignKey(d => d.IdDistrito)
                    .HasConstraintName("fk_crm_cuentas_direccion_3");

                entity.HasOne(d => d.IdZonaNavigation)
                    .WithMany(p => p.CrmCuentasDireccion)
                    .HasForeignKey(d => d.IdZona)
                    .HasConstraintName("fk_crm_cuentas_direccion_4");
            });

            modelBuilder.Entity<CrmCuentasEmail>(entity =>
            {
                entity.ToTable("crm_cuentas_email");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_crm_cuentas_email_idx");

                entity.HasIndex(e => new { e.IdCuenta, e.Email })
                    .HasName("uq_crm_cuentas_email")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("date");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.Info)
                    .HasColumnName("info")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ObservacionI)
                    .HasColumnName("observacionI")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmCuentasEmail)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_crm_cuentas_email");
            });

            modelBuilder.Entity<CrmCuentasNotas>(entity =>
            {
                entity.ToTable("crm_cuentas_notas");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("fecha_actualizacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.Texto)
                    .HasColumnName("texto")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UsuarioCreacion).HasColumnName("usuario_creacion");
            });

            modelBuilder.Entity<CrmCuentasTelefono>(entity =>
            {
                entity.ToTable("crm_cuentas_telefono");

                entity.HasIndex(e => e.IdCiudad)
                    .HasName("FK_crm_cuentas_telefono_acl_ciudad");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_cuentas_telefono_idx");

                entity.HasIndex(e => e.IdDepto)
                    .HasName("FK_crm_cuentas_telefono_acl_depto");

                entity.HasIndex(e => e.IdDistrito)
                    .HasName("FK_crm_cuentas_telefono_acl_distrito");

                entity.HasIndex(e => new { e.IdCuenta, e.Valor })
                    .HasName("uq_crm_cuentas_telefono")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Conteo).HasColumnName("conteo");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("date");

                entity.Property(e => e.IdCiudad).HasColumnName("id_ciudad");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.IdDepto).HasColumnName("id_depto");

                entity.Property(e => e.IdDistrito).HasColumnName("id_distrito");

                entity.Property(e => e.Info2)
                    .HasColumnName("info2")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ObservacionI)
                    .HasColumnName("observacionI")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("varchar(25)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdContacto)
                    .HasColumnName("id_contacto")
                    .HasColumnType("int(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdCiudadNavigation)
                    .WithMany(p => p.CrmCuentasTelefono)
                    .HasForeignKey(d => d.IdCiudad)
                    .HasConstraintName("FK_crm_cuentas_telefono_acl_ciudad");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmCuentasTelefono)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_cuentas_telefono");

                entity.HasOne(d => d.IdDeptoNavigation)
                    .WithMany(p => p.CrmCuentasTelefono)
                    .HasForeignKey(d => d.IdDepto)
                    .HasConstraintName("FK_crm_cuentas_telefono_acl_depto");

                entity.HasOne(d => d.IdDistritoNavigation)
                    .WithMany(p => p.CrmCuentasTelefono)
                    .HasForeignKey(d => d.IdDistrito)
                    .HasConstraintName("FK_crm_cuentas_telefono_acl_distrito");

                entity.HasOne(d => d.IdContactoNavigation)
                    .WithMany(p => p.CrmCuentasTelefono)
                    .HasForeignKey(d => d.IdContacto)
                    .HasConstraintName("FK_crm_cuentas_telefono_contacto");
            });

            modelBuilder.Entity<CrmGestion>(entity =>
            {
                entity.ToTable("crm_gestion");

                entity.HasIndex(e => e.IdArbolDecision)
                    .HasName("fk_gestion_3_idx");

                entity.HasIndex(e => e.IdCuenta)
                    .HasName("fk_crm_gestion_cuenta");

                entity.HasIndex(e => e.IdCuentaCampania)
                    .HasName("fk_gestion_4_idx");

                entity.HasIndex(e => e.IdCuentasCampaniaProductos)
                    .HasName("fk_gestion_1_idx");

                entity.HasIndex(e => e.IdTelefono)
                    .HasName("FK_crm_gestion_crm_cuentas_telefono");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DetalleGestion)
                    .HasColumnName("detalle_gestion")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.EstadoLlamada)
                    .HasColumnName("estado_llamada")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.HoraGestion)
                    .HasColumnName("hora_gestion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdArbolDecision).HasColumnName("id_arbol_decision");

                entity.Property(e => e.IdCuenta).HasColumnName("id_cuenta");

                entity.Property(e => e.IdCuentaCampania).HasColumnName("id_cuenta_campania");

                entity.Property(e => e.IdCuentasCampaniaProductos).HasColumnName("id_cuentas_campania_productos");

                entity.Property(e => e.IdGestionDinomi).HasColumnName("id_gestion_dinomi");

                entity.Property(e => e.IdGestor).HasColumnName("id_gestor");

                entity.Property(e => e.IdTelefono).HasColumnName("id_telefono");

                entity.Property(e => e.TiempoLlamada).HasColumnName("tiempo_llamada");

                entity.Property(e => e.TipoGestion)
                    .HasColumnName("tipo_gestion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.HasOne(d => d.IdArbolDecisionNavigation)
                    .WithMany(p => p.CrmGestion)
                    .HasForeignKey(d => d.IdArbolDecision)
                    .HasConstraintName("fk_gestion_3");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.CrmGestion)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("fk_crm_gestion_cuenta");

                entity.HasOne(d => d.IdCuentaCampaniaNavigation)
                    .WithMany(p => p.CrmGestion)
                    .HasForeignKey(d => d.IdCuentaCampania)
                    .HasConstraintName("fk_gestion_4");

                entity.HasOne(d => d.IdCuentasCampaniaProductosNavigation)
                    .WithMany(p => p.CrmGestion)
                    .HasForeignKey(d => d.IdCuentasCampaniaProductos)
                    .HasConstraintName("fk_gestion_1");

                entity.HasOne(d => d.IdTelefonoNavigation)
                    .WithMany(p => p.CrmGestion)
                    .HasForeignKey(d => d.IdTelefono)
                    .HasConstraintName("FK_crm_gestion_crm_cuentas_telefono");

                entity.HasOne(d => d.IdGestorNavigation)
                    .WithMany(p => p.CrmGestion)
                    .HasForeignKey(d => d.IdGestor)
                    .HasConstraintName("FK_crm_gestion_acl_usuario");
            });

            modelBuilder.Entity<CrmMatrizAcuerdos>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("crm_matriz_acuerdos");

                entity.Property(e => e.Agenteasignado)
                    .HasColumnName("agenteasignado")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Claseacuerdo)
                    .HasColumnName("claseacuerdo")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estadoacuerdo)
                    .HasColumnName("estadoacuerdo")
                    .HasColumnType("varchar(15)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaPago)
                    .HasColumnName("fecha_pago")
                    .HasColumnType("date");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnName("fechacreacion")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Fechapromesa)
                    .HasColumnName("fechapromesa")
                    .HasColumnType("date");

                entity.Property(e => e.IdGestor).HasColumnName("id_gestor");

                entity.Property(e => e.Nodocumento)
                    .HasColumnName("nodocumento")
                    .HasColumnType("varchar(13)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Noproducto)
                    .HasColumnName("noproducto")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Plazo).HasColumnName("plazo");

                entity.Property(e => e.Porcentcumpli)
                    .HasColumnName("porcentcumpli")
                    .HasColumnType("varchar(4)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Probabilidad)
                    .HasColumnName("probabilidad")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Saldodeuda).HasColumnName("saldodeuda");

                entity.Property(e => e.Saldovencido).HasColumnName("saldovencido");

                entity.Property(e => e.Ultgestion)
                    .HasColumnName("ultgestion")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Ultimagestion)
                    .HasColumnName("ultimagestion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.ValorAcuerdo).HasColumnName("valor_acuerdo");

                entity.Property(e => e.ValorPago).HasColumnName("valor_pago");
            });

            modelBuilder.Entity<CrmMatrizPagos>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("crm_matriz_pagos");

                entity.Property(e => e.Agente)
                    .IsRequired()
                    .HasColumnName("agente")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Clasepago)
                    .HasColumnName("clasepago")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estadocliente)
                    .HasColumnName("estadocliente")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaPago)
                    .HasColumnName("fecha_pago")
                    .HasColumnType("date");

                entity.Property(e => e.Fechacargado)
                    .HasColumnName("fechacargado")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nodocumento)
                    .IsRequired()
                    .HasColumnName("nodocumento")
                    .HasColumnType("varchar(13)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Nombrecliente)
                    .HasColumnName("nombrecliente")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Noproducto)
                    .IsRequired()
                    .HasColumnName("noproducto")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Pagocargado).HasColumnName("pagocargado");

                entity.Property(e => e.Pagoconacuerdo).HasColumnName("pagoconacuerdo");

                entity.Property(e => e.Saldodeuda).HasColumnName("saldodeuda");

                entity.Property(e => e.Saldovencido).HasColumnName("saldovencido");
            });

            modelBuilder.Entity<CrmMulticanal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("crm_multicanal");

                entity.Property(e => e.Asesor)
                    .HasColumnName("ASESOR")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cuota).HasColumnName("CUOTA");

                entity.Property(e => e.Deudatotal)
                    .HasColumnName("DEUDATOTAL")
                    .HasColumnType("float(10,4)");

                entity.Property(e => e.Deudavencida).HasColumnName("DEUDAVENCIDA");

                entity.Property(e => e.Diasmora).HasColumnName("DIASMORA");

                entity.Property(e => e.Extra1)
                    .HasColumnName("EXTRA1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra10)
                    .HasColumnName("EXTRA10")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra2)
                    .HasColumnName("EXTRA2")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra3)
                    .HasColumnName("EXTRA3")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra4)
                    .HasColumnName("EXTRA4")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra5)
                    .HasColumnName("EXTRA5")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra6)
                    .HasColumnName("EXTRA6")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra7)
                    .HasColumnName("EXTRA7")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra8)
                    .HasColumnName("EXTRA8")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra9)
                    .HasColumnName("EXTRA9")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaPago)
                    .HasColumnName("FECHA_PAGO")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Fechaacuerdo)
                    .HasColumnName("FECHAACUERDO")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Fechamejorgestion)
                    .HasColumnName("FECHAMEJORGESTION")
                    .HasColumnType("datetime");

                entity.Property(e => e.Gestion)
                    .HasColumnName("GESTION")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_USUARIO");

                entity.Property(e => e.Identificacion)
                    .HasColumnName("IDENTIFICACION")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Mejorgestion)
                    .HasColumnName("MEJORGESTION")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nocuenta).HasColumnName("NOCUENTA");

                entity.Property(e => e.Nombrecompleto)
                    .HasColumnName("NOMBRECOMPLETO")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Statuscartera)
                    .HasColumnName("STATUSCARTERA")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Totalacuerdo)
                    .HasColumnName("TOTALACUERDO")
                    .HasColumnType("double(10,4)");

                entity.Property(e => e.Totalcuotas).HasColumnName("TOTALCUOTAS");

                entity.Property(e => e.Totalpago)
                    .HasColumnName("TOTALPAGO")
                    .HasColumnType("double(10,4)");

                entity.Property(e => e.Ultimagestion)
                    .HasColumnName("ULTIMAGESTION")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Valoracuerdo)
                    .HasColumnName("VALORACUERDO")
                    .HasColumnType("double(10,4)");

                entity.Property(e => e.Valorpago)
                    .HasColumnName("VALORPAGO")
                    .HasColumnType("double(10,4)");
            });

            modelBuilder.Entity<CrmMulticanalCab>(entity =>
            {
                entity.HasKey(e => e.Idmulticanal)
                    .HasName("PRIMARY");

                entity.ToTable("crm_multicanal_cab");

                entity.Property(e => e.Idmulticanal).HasColumnName("idmulticanal");

                entity.Property(e => e.CampoRef1)
                    .HasColumnName("campo_ref1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CampoRef2)
                    .HasColumnName("campo_ref2")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CampoRef3)
                    .HasColumnName("campo_ref3")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("fecha_modificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Filtro)
                    .HasColumnName("filtro")
                    .HasColumnType("longtext")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdCampania).HasColumnName("id_campania");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UserCreacion)
                    .IsRequired()
                    .HasColumnName("user_creacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UserModificacion)
                    .HasColumnName("user_modificacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<CrmMulticanalDetalle>(entity =>
            {
                entity.ToTable("crm_multicanal_detalle");

                entity.HasIndex(e => e.Idmulticanal)
                    .HasName("fk_crm_multicanal_detalle_idx");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Asesor)
                    .HasColumnName("ASESOR")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cuota).HasColumnName("CUOTA");

                entity.Property(e => e.Deudatotal)
                    .HasColumnName("DEUDATOTAL")
                    .HasColumnType("float(10,4)");

                entity.Property(e => e.Deudavencida).HasColumnName("DEUDAVENCIDA");

                entity.Property(e => e.Diasmora).HasColumnName("DIASMORA");

                entity.Property(e => e.Extra1)
                    .HasColumnName("EXTRA1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra10)
                    .HasColumnName("EXTRA10")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra2)
                    .HasColumnName("EXTRA2")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra3)
                    .HasColumnName("EXTRA3")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra4)
                    .HasColumnName("EXTRA4")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra5)
                    .HasColumnName("EXTRA5")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra6)
                    .HasColumnName("EXTRA6")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra7)
                    .HasColumnName("EXTRA7")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra8)
                    .HasColumnName("EXTRA8")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Extra9)
                    .HasColumnName("EXTRA9")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FechaPago)
                    .HasColumnName("FECHA_PAGO")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Fechaacuerdo)
                    .HasColumnName("FECHAACUERDO")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Fechamejorgestion)
                    .HasColumnName("FECHAMEJORGESTION")
                    .HasColumnType("datetime");

                entity.Property(e => e.Gestion)
                    .HasColumnName("GESTION")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_USUARIO");

                entity.Property(e => e.Identificacion)
                    .HasColumnName("IDENTIFICACION")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Idmulticanal).HasColumnName("IDMULTICANAL");

                entity.Property(e => e.Mejorgestion)
                    .HasColumnName("MEJORGESTION")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nocuenta).HasColumnName("NOCUENTA");

                entity.Property(e => e.Nombrecompleto)
                    .HasColumnName("NOMBRECOMPLETO")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Statuscartera)
                    .HasColumnName("STATUSCARTERA")
                    .HasColumnType("enum('A','I')")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Totalacuerdo)
                    .HasColumnName("TOTALACUERDO")
                    .HasColumnType("double(10,4)");

                entity.Property(e => e.Totalcuotas).HasColumnName("TOTALCUOTAS");

                entity.Property(e => e.Totalpago)
                    .HasColumnName("TOTALPAGO")
                    .HasColumnType("double(10,4)");

                entity.Property(e => e.Ultimagestion)
                    .HasColumnName("ULTIMAGESTION")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Valoracuerdo)
                    .HasColumnName("VALORACUERDO")
                    .HasColumnType("double(10,4)");

                entity.Property(e => e.Valorpago)
                    .HasColumnName("VALORPAGO")
                    .HasColumnType("double(10,4)");

                entity.HasOne(d => d.IdmulticanalNavigation)
                    .WithMany(p => p.CrmMulticanalDetalle)
                    .HasForeignKey(d => d.Idmulticanal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_crm_multicanal_detalle");
            });

            modelBuilder.Entity<CrmMulticanalMarcadores>(entity =>
            {
                entity.HasKey(e => e.IdMarcador)
                    .HasName("PRIMARY");

                entity.ToTable("crm_multicanal_marcadores");

                entity.Property(e => e.IdMarcador).HasColumnName("id_marcador");

                entity.Property(e => e.CampoRef1)
                    .HasColumnName("campo_ref1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CampoRef2)
                    .HasColumnName("campo_ref2")
                    .HasColumnType("varchar(60)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CampoRef3).HasColumnName("campo_ref3");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaFin)
                    .HasColumnName("fecha_fin")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaInicio)
                    .HasColumnName("fecha_inicio")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCampania).HasColumnName("id_campania");

                entity.Property(e => e.IdGrupo)
                    .HasColumnName("id_grupo")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdUrl)
                    .HasColumnName("id_url")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NumeroCola)
                    .HasColumnName("numero_cola")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Opcion)
                    .IsRequired()
                    .HasColumnName("opcion")
                    .HasColumnType("enum('Automatica','Manual')")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.UserCreacion)
                    .IsRequired()
                    .HasColumnName("user_creacion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<CrmPausas>(entity =>
            {
                entity.ToTable("crm_pausas");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CrmPausascol)
                    .HasColumnName("crm_pausascol")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<CrmPausasUsuario>(entity =>
            {
                entity.ToTable("crm_pausas_usuario");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Duracion)
                    .HasColumnName("duracion")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.HoraFin)
                    .HasColumnName("hora_fin")
                    .HasColumnType("datetime");

                entity.Property(e => e.HoraInicio)
                    .HasColumnName("hora_inicio")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPausa).HasColumnName("id_pausa");

                entity.Property(e => e.IdUser).HasColumnName("id_user");
            });

            modelBuilder.Entity<CrmTareaCuentaCampania>(entity =>
            {
                entity.ToTable("crm_tarea_cuenta_campania");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdCuentaCampania).HasColumnName("id_cuenta_campania");

                entity.Property(e => e.IdTarea).HasColumnName("id_tarea");
            });

            modelBuilder.Entity<CrmTareaGestor>(entity =>
            {
                entity.ToTable("crm_tarea_gestor");

                entity.HasIndex(e => e.IdTarea)
                    .HasName("FK_crm_tarea_gestor_crm_tareas");

                entity.HasIndex(e => e.IdUsuario)
                    .HasName("FK_crm_tarea_gestor_acl_usuario");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdCuentaCampania).HasColumnName("id_cuenta_campania");

                entity.Property(e => e.IdTarea).HasColumnName("id_tarea");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.HasOne(d => d.IdTareaNavigation)
                    .WithMany(p => p.CrmTareaGestor)
                    .HasForeignKey(d => d.IdTarea)
                    .HasConstraintName("FK_crm_tarea_gestor_crm_tareas");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.CrmTareaGestor)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_crm_tarea_gestor_acl_usuario");
            });

            modelBuilder.Entity<CrmTareaObjetivos>(entity =>
            {
                entity.HasKey(e => e.IdObjetivo)
                    .HasName("PRIMARY");

                entity.ToTable("crm_tarea_objetivos");

                entity.Property(e => e.IdObjetivo).HasColumnName("id_objetivo");

                entity.Property(e => e.CaCantidad).HasColumnName("ca_cantidad");

                entity.Property(e => e.CaCantidadfinalizar).HasColumnName("ca_cantidadfinalizar");

                entity.Property(e => e.CaCantidadhabilitar).HasColumnName("ca_cantidadhabilitar");

                entity.Property(e => e.CaValor).HasColumnName("ca_valor");

                entity.Property(e => e.CaValorfinalizar).HasColumnName("ca_valorfinalizar");

                entity.Property(e => e.CaValorhabilitar).HasColumnName("ca_valorhabilitar");

                entity.Property(e => e.CaValorparametro).HasColumnName("ca_valorparametro");

                entity.Property(e => e.CampoRef1)
                    .HasColumnName("campo_ref1")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CampoRef2).HasColumnName("campo_ref2");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("fecha_actualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdParametro).HasColumnName("id_parametro");

                entity.Property(e => e.IdTarea).HasColumnName("id_tarea");

                entity.Property(e => e.IdTipoobjetivo).HasColumnName("id_tipoobjetivo");

                entity.Property(e => e.IdgestionPrincipal).HasColumnName("idgestion_principal");

                entity.Property(e => e.IdgestionSec1).HasColumnName("idgestion_sec1");

                entity.Property(e => e.IdgestionSec2).HasColumnName("idgestion_sec2");

                entity.Property(e => e.PCantidad).HasColumnName("p_cantidad");

                entity.Property(e => e.PCantidadfinalizar).HasColumnName("p_cantidadfinalizar");

                entity.Property(e => e.PCantidadhabilitar).HasColumnName("p_cantidadhabilitar");

                entity.Property(e => e.TcCantidad).HasColumnName("tc_cantidad");

                entity.Property(e => e.TcCantidadfinalizar).HasColumnName("tc_cantidadfinalizar");

                entity.Property(e => e.TcCantidadhabilitar).HasColumnName("tc_cantidadhabilitar");

                entity.Property(e => e.TcValor).HasColumnName("tc_valor");

                entity.Property(e => e.TcValorfinalizar).HasColumnName("tc_valorfinalizar");

                entity.Property(e => e.TcValorhabilitar).HasColumnName("tc_valorhabilitar");

                entity.Property(e => e.UserActualizacion).HasColumnName("user_actualizacion");

                entity.Property(e => e.UserCreacion).HasColumnName("user_creacion");

                entity.Property(e => e.ValorparametroFinalizar).HasColumnName("valorparametro_finalizar");

                entity.Property(e => e.ValorparametroHabilitar).HasColumnName("valorparametro_habilitar");
            });

            modelBuilder.Entity<CrmTareas>(entity =>
            {
                entity.ToTable("crm_tareas");

                entity.HasIndex(e => e.IdCampania)
                    .HasName("FK_crm_tareas_crm_campania");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("enum('A','I')")
                    .HasDefaultValueSql("'A'")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("fecha_actualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaFin)
                    .HasColumnName("fecha_fin")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaInicio)
                    .HasColumnName("fecha_inicio")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCampania).HasColumnName("id_campania");

                entity.Property(e => e.IdObjetivo).HasColumnName("id_objetivo");

                entity.Property(e => e.Idmulticanal).HasColumnName("idmulticanal");

                entity.Property(e => e.Meta)
                    .HasColumnName("meta")
                    .HasColumnType("float(10,4)");

                entity.Property(e => e.Obligatorio)
                    .IsRequired()
                    .HasColumnType("enum('Si','No')")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.UserCreacion).HasColumnName("user_creacion");

                entity.Property(e => e.UserModificacion).HasColumnName("user_modificacion");

                entity.HasOne(d => d.IdCampaniaNavigation)
                    .WithMany(p => p.CrmTareas)
                    .HasForeignKey(d => d.IdCampania)
                    .HasConstraintName("FK_crm_tareas_crm_campania");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
