﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmMulticanalDetalle
    {
        public uint Id { get; set; }
        public uint Idmulticanal { get; set; }
        public string Asesor { get; set; }
        public uint? IdUsuario { get; set; }
        public uint Nocuenta { get; set; }
        public string Nombrecompleto { get; set; }
        public string Identificacion { get; set; }
        public string Ultimagestion { get; set; }
        public string Gestion { get; set; }
        public int? Diasmora { get; set; }
        public string Mejorgestion { get; set; }
        public DateTime? Fechamejorgestion { get; set; }
        public float? Deudatotal { get; set; }
        public string Statuscartera { get; set; }
        public string Fechaacuerdo { get; set; }
        public double? Valoracuerdo { get; set; }
        public double? Valorpago { get; set; }
        public string FechaPago { get; set; }
        public double? Totalacuerdo { get; set; }
        public double? Totalpago { get; set; }
        public int? Cuota { get; set; }
        public int? Totalcuotas { get; set; }
        public float? Deudavencida { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Extra3 { get; set; }
        public string Extra4 { get; set; }
        public string Extra5 { get; set; }
        public string Extra6 { get; set; }
        public string Extra7 { get; set; }
        public string Extra8 { get; set; }
        public string Extra9 { get; set; }
        public string Extra10 { get; set; }

        public virtual CrmMulticanalCab IdmulticanalNavigation { get; set; }
    }
}
