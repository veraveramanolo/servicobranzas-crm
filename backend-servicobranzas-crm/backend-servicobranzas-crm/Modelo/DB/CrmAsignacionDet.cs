﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmAsignacionDet
    {
        public int IdAsignacionDet { get; set; }
        public int IdAsignacion { get; set; }
        public int IdUsuario { get; set; }

        public virtual CrmAsignacionCab IdAsignacionNavigation { get; set; }
    }
}
