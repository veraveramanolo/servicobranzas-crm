﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmMulticanalCab
    {
        public CrmMulticanalCab()
        {
            CrmMulticanalDetalle = new HashSet<CrmMulticanalDetalle>();
        }

        public uint Idmulticanal { get; set; }
        public string Descripcion { get; set; }
        public string Filtro { get; set; }
        public string Status { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UserCreacion { get; set; }
        public uint IdCampania { get; set; }
        public string UserModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string CampoRef1 { get; set; }
        public string CampoRef2 { get; set; }
        public string CampoRef3 { get; set; }

        public virtual ICollection<CrmMulticanalDetalle> CrmMulticanalDetalle { get; set; }
    }
}
