using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCaja
    {
        public uint Id { get; set; }
        public string? Identificacion { get; set; }
        public string? Estado { get; set; }
        public string? Valor { get; set; }
        public string? Comentario { get; set; }
        public uint IdGestor { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public uint IdCartera { get; set; }
        public uint IdCampania { get; set; }
    }
}
