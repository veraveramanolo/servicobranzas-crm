﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclRelacion
    {
        public AclRelacion()
        {
            CrmCuentasContacto = new HashSet<CrmCuentasContacto>();
        }

        public uint IdRelacion { get; set; }
        public string ValorRelacion { get; set; }

        public virtual ICollection<CrmCuentasContacto> CrmCuentasContacto { get; set; }
    }
}
