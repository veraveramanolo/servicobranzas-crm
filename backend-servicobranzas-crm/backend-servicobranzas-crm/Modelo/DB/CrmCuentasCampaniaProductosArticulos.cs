﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaProductosArticulos
    {
        public uint Id { get; set; }
        public uint? IdCuentaCampaniaProducto { get; set; }
        public string Articulo { get; set; }
        public decimal? Valor { get; set; }
    }
}
