﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclUser
    {
        public AclUser()
        {
            AclMembership = new HashSet<AclMembership>();
        }

        public uint Id { get; set; }
        public string Nombre { get; set; }
        public string Md5Password { get; set; }
        public string Descripcion { get; set; }
        public string Agentname { get; set; }
        public string Agentpass { get; set; }
        public string Estado { get; set; }

        public virtual ICollection<AclMembership> AclMembership { get; set; }
    }
}
