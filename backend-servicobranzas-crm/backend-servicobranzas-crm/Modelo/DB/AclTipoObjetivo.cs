﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclTipoObjetivo
    {
        public uint IdTipoobjetivo { get; set; }
        public string ValorTipoobjetivo { get; set; }
    }
}
