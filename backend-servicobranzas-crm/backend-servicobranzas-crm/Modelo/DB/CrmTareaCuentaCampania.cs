﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmTareaCuentaCampania
    {
        public uint Id { get; set; }
        public uint? IdTarea { get; set; }
        public uint? IdCuentaCampania { get; set; }
    }
}
