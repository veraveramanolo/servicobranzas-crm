﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclEstados
    {
        public string Estado { get; set; }
        public long? Cantidad { get; set; }
        public string Texto { get; set; }
        public string Alerta { get; set; }
        public uint? Id { get; set; }
    }
}
