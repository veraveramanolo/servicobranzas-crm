﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmMulticanalMarcadores
    {
        public uint IdMarcador { get; set; }
        public uint IdCampania { get; set; }
        public string Nombre { get; set; }
        public string Opcion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public string NumeroCola { get; set; }
        public string IdGrupo { get; set; }
        public string IdUrl { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UserCreacion { get; set; }
        public string CampoRef1 { get; set; }
        public string CampoRef2 { get; set; }
        public int? CampoRef3 { get; set; }
    }
}
