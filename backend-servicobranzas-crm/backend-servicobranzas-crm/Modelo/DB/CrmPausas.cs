﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmPausas
    {
        public uint Id { get; set; }
        public string CrmPausascol { get; set; }
    }
}
