﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmArbolGestionTipoGestion
    {
        public CrmArbolGestionTipoGestion()
        {
            CrmArbolGestion = new HashSet<CrmArbolGestion>();
        }

        public uint Id { get; set; }
        public string Texto { get; set; }

        public virtual ICollection<CrmArbolGestion> CrmArbolGestion { get; set; }
    }
}
