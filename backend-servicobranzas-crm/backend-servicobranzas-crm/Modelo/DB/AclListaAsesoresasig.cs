﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclListaAsesoresasig
    {
        public int IdUsuario { get; set; }
        public string Asesor { get; set; }
        public int? AsignadosClientes { get; set; }
        public int? Activos { get; set; }
        public int? Inactivos { get; set; }
    }
}
