using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public class TodoGestion
    {
        public uint idUsuario { get; set; }
        public string asesor { get; set; }
        public int gestiones { get; set; }
        public int clientes { get; set; }
        public int directos { get; set; }
        public int efectivos { get; set; }
        public int acuerdos { get; set; }
    }

    public class TodoGestionDolar
    {
        public uint idUsuario { get; set; }
        public string asesor { get; set; }
        public int gestiones { get; set; }
        public float? clientes { get; set; }
        public float? directos { get; set; }
        public float? efectivos { get; set; }
        public float? acuerdos { get; set; }
    }

    public class TodoGestionDolarValorVenc
    {
        public uint idUsuario { get; set; }
        public string asesor { get; set; }
        public int gestiones { get; set; }
        public float? clientes { get; set; }
        public float? directos { get; set; }
        public float? efectivos { get; set; }
        public float? acuerdos { get; set; }
    }

    public class TodoGestionDolarDeudaTotal
    {
        public uint idUsuario { get; set; }
        public string asesor { get; set; }
        public int gestiones { get; set; }
        public float? clientes { get; set; }
        public float? directos { get; set; }
        public float? efectivos { get; set; }
        public float? acuerdos { get; set; }
    }

    public class TodoGestionPorc
    {
        public uint idUsuario { get; set; }
        public string asesor { get; set; }
        public int gestiones { get; set; }
        public Decimal clientes { get; set; }
        public Decimal directos { get; set; }
        public Decimal efectivos { get; set; }
        public Decimal acuerdos { get; set; }
    }
    public class Metricas
    {
        public uint Id { get; set; }
        public string Descripcion { get; set; }
        public uint? IdTipoGestion { get; set; }
        public int Valor { get; set; }
    }
    public class MetricasPerc
    {
        public uint Id { get; set; }
        public string Descripcion { get; set; }
        public uint? IdTipoGestion { get; set; }
        public Decimal Valor { get; set; }
    }
    public class MetricasDol
    {
        public uint Id { get; set; }
        public string Descripcion { get; set; }
        public uint? IdTipoGestion { get; set; }
        public Decimal? Valor { get; set; }
    }
    public class EstadosAsesor
    {
        public uint Id { get; set; }
        public uint IdAsesor { get; set; }
        public string Asesor { get; set; }
        public int Valor { get; set; }
    }

    public class EstadosPerfilaciones
    {
        public string Contacto { get; set; }
        public string Perfilaciones { get; set; }
        public int Valor { get; set; }
    }
    public class EstadosTotal
    {
        public int Positivos { get; set; }
        public int Negativos { get; set; }
        public double PositivosPorcent { get; set; }
        public int TotalEstados { get; set; }
        public int TotalValor { get; set; }
    }
    public class EstadosTotalDol
    {
        public decimal? Positivos { get; set; }
        public decimal? Negativos { get; set; }
        public double PositivosPorcent { get; set; }
        public int TotalEstados { get; set; }
        public decimal TotalValor { get; set; }
    }
}
