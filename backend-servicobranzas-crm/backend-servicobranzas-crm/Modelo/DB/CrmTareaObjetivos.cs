﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmTareaObjetivos
    {
        public uint IdObjetivo { get; set; }
        public uint IdTarea { get; set; }
        public uint IdTipoobjetivo { get; set; }
        public uint? IdParametro { get; set; }
        public double? CaValorparametro { get; set; }
        public Boolean? ValorparametroHabilitar { get; set; }
        public Boolean? ValorparametroFinalizar { get; set; }
        public double? CaValor { get; set; }
        public Boolean? CaValorhabilitar { get; set; }
        public Boolean? CaValorfinalizar { get; set; }
        public int? CaCantidad { get; set; }
        public Boolean? CaCantidadhabilitar { get; set; }
        public Boolean? CaCantidadfinalizar { get; set; }
        public uint? IdgestionPrincipal { get; set; }
        public uint? IdgestionSec1 { get; set; }
        public uint? IdgestionSec2 { get; set; }
        public int? TcCantidad { get; set; }
        public Boolean? TcCantidadhabilitar { get; set; }
        public Boolean? TcCantidadfinalizar { get; set; }
        public double? TcValor { get; set; }
        public Boolean? TcValorhabilitar { get; set; }
        public Boolean? TcValorfinalizar { get; set; }
        public int? PCantidad { get; set; }
        public Boolean? PCantidadhabilitar { get; set; }
        public Boolean? PCantidadfinalizar { get; set; }
        public string CampoRef1 { get; set; }
        public double? CampoRef2 { get; set; }
        public string Estado { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public uint? UserCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public uint? UserActualizacion { get; set; }
    }
}
