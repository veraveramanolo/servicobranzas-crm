﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclRegion
    {
        public AclRegion()
        {
            AclDepto = new HashSet<AclDepto>();
        }

        public uint IdRegion { get; set; }
        public string RegionValor { get; set; }

        public virtual ICollection<AclDepto> AclDepto { get; set; }
    }
}
