﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclRoles
    {
        public AclRoles()
        {
            AclAccesosRoles = new HashSet<AclAccesosRoles>();
            AclUsuario = new HashSet<AclUsuario>();
        }

        public uint IdRol { get; set; }
        public string Descripcion { get; set; }
        public string Anulado { get; set; }
        public string UsrCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<AclAccesosRoles> AclAccesosRoles { get; set; }
        public virtual ICollection<AclUsuario> AclUsuario { get; set; }
    }
}
