﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasNotas
    {
        public uint Id { get; set; }
        public uint? IdCuenta { get; set; }
        public string Texto { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaActualizacion { get; set; }
        public uint? UsuarioCreacion { get; set; }
    }
}
