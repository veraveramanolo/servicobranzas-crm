﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmArbolGestion
    {
        public CrmArbolGestion()
        {
            CrmCuentasCampania = new HashSet<CrmCuentasCampania>();
            CrmGestion = new HashSet<CrmGestion>();
            InverseIdParentNavigation = new HashSet<CrmArbolGestion>();
        }

        public uint Id { get; set; }
        public uint? IdParent { get; set; }
        public string Descripcion { get; set; }
        public int? Peso { get; set; }
        public string Estado { get; set; }
        public uint? IdTipoGestion { get; set; }
        public uint? IdTipoContacto { get; set; }
        public string Alerta { get; set; }
        public string LabelEstado { get; set; }
        public string LabelNivel2 { get; set; }
        public uint? IdArbolGestion { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public virtual CrmArbolGestion IdParentNavigation { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public virtual CrmArbolGestionTipoContacto IdTipoContactoNavigation { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public virtual CrmArbolGestionTipoGestion IdTipoGestionNavigation { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public virtual ICollection<CrmCuentasCampania> CrmCuentasCampania { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public virtual ICollection<CrmGestion> CrmGestion { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public virtual ICollection<CrmArbolGestion> InverseIdParentNavigation { get; set; }
    }
}
