﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaAcuerdos
    {
        public CrmCuentasCampaniaAcuerdos()
        {
            CrmCuentasCampaniaPagos = new HashSet<CrmCuentasCampaniaPagos>();
        }

        public uint Id { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaAcuerdo { get; set; }
        public string TipoAcuerdo { get; set; }
        public float? ValorAcuerdo { get; set; }
        public uint? IdCuentaCampaniaProducto { get; set; }
        public uint? IdGestor { get; set; }
        public string Efecto { get; set; }
        public string Estado { get; set; }
        public int? Cuota { get; set; }
        public string Plazo { get; set; }
        public string Probabilidad { get; set; }
        public string Comentario { get; set; }
        public DateTime? FechaVisita { get; set; }
        public TimeSpan? HoraAcuerdo { get; set; }
        public uint? IdDireccion { get; set; }
        public string EstadoNot { get; set; }
        public uint? UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? CantModificacion { get; set; }

        public virtual CrmCuentasCampaniaProductos IdCuentaCampaniaProductoNavigation { get; set; }
        public virtual CrmCuentasDireccion IdDireccionNavigation { get; set; }
        public virtual AclUsuario IdGestorNavigation { get; set; }
        public virtual ICollection<CrmCuentasCampaniaPagos> CrmCuentasCampaniaPagos { get; set; }
    }
}
