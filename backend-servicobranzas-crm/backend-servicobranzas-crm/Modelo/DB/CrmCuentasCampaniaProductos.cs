﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaProductos
    {
        public CrmCuentasCampaniaProductos()
        {
            CrmCuentasCampaniaAcuerdos = new HashSet<CrmCuentasCampaniaAcuerdos>();
            CrmCuentasCampaniaPagos = new HashSet<CrmCuentasCampaniaPagos>();
            CrmCuentasCampaniaProductosDetalle = new HashSet<CrmCuentasCampaniaProductosDetalle>();
            CrmGestion = new HashSet<CrmGestion>();
        }

        public uint Id { get; set; }
        public uint? IdCuentaCampania { get; set; }
        public decimal? Valor { get; set; }
        public string Descripcion { get; set; }
        public int? CuotasPendientes { get; set; }
        public string CrmCuentasCampaniaProductoscol { get; set; }
        public string CrmCuentasCampaniaProductoscol1 { get; set; }
        public int? Edadmor { get; set; }
        public decimal? Deudatotal { get; set; }
        public decimal? Valorporvencer { get; set; }

        public virtual CrmCuentasCampania IdCuentaCampaniaNavigation { get; set; }
        public virtual ICollection<CrmCuentasCampaniaAcuerdos> CrmCuentasCampaniaAcuerdos { get; set; }
        public virtual ICollection<CrmCuentasCampaniaPagos> CrmCuentasCampaniaPagos { get; set; }
        public virtual ICollection<CrmCuentasCampaniaProductosDetalle> CrmCuentasCampaniaProductosDetalle { get; set; }
        public virtual ICollection<CrmGestion> CrmGestion { get; set; }
    }
}
