﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclTipoInfo
    {
        public int IdTipoInfo { get; set; }
        public string InfoValor { get; set; }
        public string InfoNombre { get; set; }
    }
}
