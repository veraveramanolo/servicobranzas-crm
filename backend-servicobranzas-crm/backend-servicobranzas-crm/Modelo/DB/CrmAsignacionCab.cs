﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmAsignacionCab
    {
        public CrmAsignacionCab()
        {
            CrmAsignacionDet = new HashSet<CrmAsignacionDet>();
        }

        public int IdAsignacion { get; set; }
        public uint IdTarea { get; set; }
        public string Tipoasignacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public int? UserCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public int? UserActualizacion { get; set; }

        public virtual ICollection<CrmAsignacionDet> CrmAsignacionDet { get; set; }
    }
}
