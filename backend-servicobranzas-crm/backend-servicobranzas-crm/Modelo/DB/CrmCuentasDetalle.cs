﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasDetalle
    {
        public uint Id { get; set; }
        public uint? IdCuenta { get; set; }
        public string Campo { get; set; }
        public string Valor { get; set; }
        public uint? IdCuentaCampaniaProducto { get; set; }

        public virtual CrmCuentas IdCuentaNavigation { get; set; }
    }
}
