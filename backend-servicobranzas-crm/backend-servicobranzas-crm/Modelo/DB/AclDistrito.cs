﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclDistrito
    {
        public AclDistrito()
        {
            CrmCuentasDireccion = new HashSet<CrmCuentasDireccion>();
            CrmCuentasTelefono = new HashSet<CrmCuentasTelefono>();
        }

        public uint IdDistrito { get; set; }
        public uint? IdCiudad { get; set; }
        public string DistritoValor { get; set; }

        public virtual AclCiudad IdCiudadNavigation { get; set; }
        public virtual ICollection<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
        public virtual ICollection<CrmCuentasTelefono> CrmCuentasTelefono { get; set; }
    }
}
