﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmMatrizAcuerdos
    {
        public string Agenteasignado { get; set; }
        public int? IdGestor { get; set; }
        public string Nodocumento { get; set; }
        public string Noproducto { get; set; }
        public string Ultgestion { get; set; }
        public string Fechacreacion { get; set; }
        public DateTime? Fechapromesa { get; set; }
        public DateTime? FechaPago { get; set; }
        public string Claseacuerdo { get; set; }
        public string Probabilidad { get; set; }
        public string Estadoacuerdo { get; set; }
        public float? ValorAcuerdo { get; set; }
        public float? ValorPago { get; set; }
        public string Porcentcumpli { get; set; }
        public string Ultimagestion { get; set; }
        public int? Plazo { get; set; }
        public float? Saldodeuda { get; set; }
        public float? Saldovencido { get; set; }
    }
}
