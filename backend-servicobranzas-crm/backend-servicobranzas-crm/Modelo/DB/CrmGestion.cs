﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmGestion
    {
        public uint Id { get; set; }
        public uint? IdCuentasCampaniaProductos { get; set; }
        public uint? IdTelefono { get; set; }
        public DateTime? HoraGestion { get; set; }
        public uint? IdGestionDinomi { get; set; }
        public int? TiempoLlamada { get; set; }
        public string EstadoLlamada { get; set; }
        public uint? IdArbolDecision { get; set; }
        public string DetalleGestion { get; set; }
        public string TipoGestion { get; set; }
        public uint? IdCuenta { get; set; }
        public uint? IdCuentaCampania { get; set; }
        public uint IdGestor { get; set; }

        public virtual CrmArbolGestion IdArbolDecisionNavigation { get; set; }
        public virtual CrmCuentasCampania IdCuentaCampaniaNavigation { get; set; }
        public virtual CrmCuentas IdCuentaNavigation { get; set; }
        public virtual CrmCuentasCampaniaProductos IdCuentasCampaniaProductosNavigation { get; set; }
        public virtual CrmCuentasTelefono IdTelefonoNavigation { get; set; }
        public virtual AclUsuario IdGestorNavigation { get; set; }


    }
}
