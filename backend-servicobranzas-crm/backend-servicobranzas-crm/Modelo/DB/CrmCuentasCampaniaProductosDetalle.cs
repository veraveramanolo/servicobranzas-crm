﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaProductosDetalle
    {
        public uint Id { get; set; }
        public uint? IdCuentaCampaniaProducto { get; set; }
        public string Descripcion { get; set; }
        public decimal? Valor { get; set; }
        public string Estado { get; set; }

        public virtual CrmCuentasCampaniaProductos IdCuentaCampaniaProductoNavigation { get; set; }
    }
}
