﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclGroup
    {
        public AclGroup()
        {
            AclMembership = new HashSet<AclMembership>();
        }

        public uint Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<AclMembership> AclMembership { get; set; }
    }
}
