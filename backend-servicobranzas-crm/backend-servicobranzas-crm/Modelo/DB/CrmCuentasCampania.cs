﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampania
    {
        public CrmCuentasCampania()
        {
            CrmCuentasCampaniaDetalleHistorico = new HashSet<CrmCuentasCampaniaDetalleHistorico>();
            CrmCuentasCampaniaGestor = new HashSet<CrmCuentasCampaniaGestor>();
            CrmCuentasCampaniaProductos = new HashSet<CrmCuentasCampaniaProductos>();
            CrmGestion = new HashSet<CrmGestion>();
        }

        public uint Id { get; set; }
        public uint? IdCuenta { get; set; }
        public uint? IdCampania { get; set; }
        public string Estado { get; set; }
        public uint? IdArbolGestionMejorGestion { get; set; }

        public virtual CrmArbolGestion IdArbolGestionMejorGestionNavigation { get; set; }
        public virtual CrmCampania IdCampaniaNavigation { get; set; }
        public virtual CrmCuentas IdCuentaNavigation { get; set; }
        public virtual ICollection<CrmCuentasCampaniaDetalleHistorico> CrmCuentasCampaniaDetalleHistorico { get; set; }
        public virtual ICollection<CrmCuentasCampaniaGestor> CrmCuentasCampaniaGestor { get; set; }
        public virtual ICollection<CrmCuentasCampaniaProductos> CrmCuentasCampaniaProductos { get; set; }
        public virtual ICollection<CrmGestion> CrmGestion { get; set; }
    }
}
