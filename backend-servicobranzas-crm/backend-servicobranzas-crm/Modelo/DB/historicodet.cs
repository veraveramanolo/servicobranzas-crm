using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class historicodet
    {
        public DateTime? fecha { get; set; }
        public string destino { get; set; }
        public string contacto { get; set; }
        public string estado { get; set; }
        public string canal { get; set; }
        public string agente { get; set; }
        public string observaciones { get; set; }
    }
}
