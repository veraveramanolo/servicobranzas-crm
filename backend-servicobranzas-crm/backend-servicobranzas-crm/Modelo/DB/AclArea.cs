﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclArea
    {
        public AclArea()
        {
            CrmCuentasDireccion = new HashSet<CrmCuentasDireccion>();
        }

        public uint IdArea { get; set; }
        public string ValorArea { get; set; }

        public virtual ICollection<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
    }
}
