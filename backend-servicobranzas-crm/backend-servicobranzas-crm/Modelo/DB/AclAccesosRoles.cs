﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclAccesosRoles
    {
        public uint IdAccesoRol { get; set; }
        public uint? IdRol { get; set; }
        public uint? IdModulo { get; set; }
        public string Anulado { get; set; }
        public string UsrCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual AclModulos IdModuloNavigation { get; set; }
        public virtual AclRoles IdRolNavigation { get; set; }
    }
}
