﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmConsultaGestionCampaniaData
    {
        public uint Id { get; set; }
        public uint? Idcuentacampania { get; set; }
        public uint? Idcuenta { get; set; }
        public string Asesor { get; set; }
        public string Nombrecliente { get; set; }
        public string Cedula { get; set; }
        public string Ultimagestion { get; set; }
        public string Acuerdo { get; set; }
        public float? Valoracuerdo { get; set; }
        public string Pago { get; set; }
        public float? Valorpago { get; set; }
        public uint? Idproducto { get; set; }
        public uint? Cuotaspendientes { get; set; }
        public float? Montoinicial { get; set; }
        public string Estado { get; set; }
    }
}
