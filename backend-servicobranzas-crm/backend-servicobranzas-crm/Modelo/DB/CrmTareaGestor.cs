﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmTareaGestor
    {
        public uint Id { get; set; }
        public uint? IdTarea { get; set; }
        public uint? IdUsuario { get; set; }
        public uint IdCuentaCampania { get; set; }

        public virtual CrmTareas IdTareaNavigation { get; set; }
        public virtual AclUsuario IdUsuarioNavigation { get; set; }
    }
}
