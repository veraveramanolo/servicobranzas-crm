﻿using System;
using System.Collections.Generic;
using backend_servicobranzas_crm.Modelo.DB;
using backend_servicobranzas_crm.Modelo.Request;
using backend_servicobranzas_crm.Modelo.Response;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclMulticanalEstados
    {
        public string Gestion { get; set; }
        public string Mejorgestion { get; set; }
    }  
}
