﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclParametroObjetivo
    {
        public uint IdParametro { get; set; }
        public string ValorParametro { get; set; }
    }
}
