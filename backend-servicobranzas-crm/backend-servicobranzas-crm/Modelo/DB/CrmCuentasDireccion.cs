﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasDireccion
    {
        public CrmCuentasDireccion()
        {
            CrmCuentasCampaniaAcuerdos = new HashSet<CrmCuentasCampaniaAcuerdos>();
        }

        public uint Id { get; set; }
        public uint? IdCuenta { get; set; }
        public string Tipo { get; set; }
        public string Valor { get; set; }
        public uint? IdDepto { get; set; }
        public uint? IdCiudad { get; set; }
        public uint? IdDistrito { get; set; }
        public uint? IdZona { get; set; }
        public uint? IdArea { get; set; }
        public string Barrio { get; set; }
        public string Info1 { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string Estado { get; set; }
        public string ObservacionI { get; set; }

        public virtual AclArea IdAreaNavigation { get; set; }
        public virtual AclCiudad IdCiudadNavigation { get; set; }
        public virtual CrmCuentas IdCuentaNavigation { get; set; }
        public virtual AclDepto IdDeptoNavigation { get; set; }
        public virtual AclDistrito IdDistritoNavigation { get; set; }
        public virtual AclZona IdZonaNavigation { get; set; }
        public virtual ICollection<CrmCuentasCampaniaAcuerdos> CrmCuentasCampaniaAcuerdos { get; set; }
    }
}
