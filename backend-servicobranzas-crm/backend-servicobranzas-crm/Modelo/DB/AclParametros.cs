﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclParametros
    {
        public uint IdParametro { get; set; }
        public string NombreParametro { get; set; }
        public string ValorParametro { get; set; }
        public uint IdModulo { get; set; }
        public string Estado { get; set; }
        public uint? UserCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public uint? UserModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string CampoRef1 { get; set; }
        public int? CampoRef2 { get; set; }

        public virtual AclModulos IdModuloNavigation { get; set; }
    }
}
