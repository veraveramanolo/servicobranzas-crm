﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclZona
    {
        public AclZona()
        {
            CrmCuentasDireccion = new HashSet<CrmCuentasDireccion>();
        }

        public uint IdZona { get; set; }
        public string ZonaValor { get; set; }

        public virtual ICollection<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
    }
}
