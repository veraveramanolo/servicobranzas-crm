﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmMatrizPagos
    {
        public string Agente { get; set; }
        public string Nodocumento { get; set; }
        public string Nombrecliente { get; set; }
        public string Noproducto { get; set; }
        public string Estadocliente { get; set; }
        public DateTime? FechaPago { get; set; }
        public float? Pagoconacuerdo { get; set; }
        public string Clasepago { get; set; }
        public float? Pagocargado { get; set; }
        public DateTime? Fechacargado { get; set; }
        public float? Saldodeuda { get; set; }
        public float? Saldovencido { get; set; }
    }
}
