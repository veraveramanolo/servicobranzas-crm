﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasEmail
    {
        public uint Id { get; set; }
        public uint? IdCuenta { get; set; }
        public string Email { get; set; }
        public string Tipo { get; set; }
        public string Info { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string Estado { get; set; }
        public string ObservacionI { get; set; }

        public virtual CrmCuentas IdCuentaNavigation { get; set; }
    }
}
