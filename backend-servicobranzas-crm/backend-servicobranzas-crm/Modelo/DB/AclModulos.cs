﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclModulos
    {
        public AclModulos()
        {
            AclAccesosRoles = new HashSet<AclAccesosRoles>();
            AclModuloAcciones = new HashSet<AclModuloAcciones>();
            AclParametros = new HashSet<AclParametros>();
            InverseIdParentModuloNavigation = new HashSet<AclModulos>();
        }

        public uint IdModulo { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
        public string ModuloPadre { get; set; }
        public string RutaDirectorio { get; set; }
        public uint? IdParentModulo { get; set; }
        public string Visible { get; set; }
        public uint? OrdenPresentacion { get; set; }
        public string Anulado { get; set; }
        public string UsrCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual AclModulos IdParentModuloNavigation { get; set; }
        public virtual ICollection<AclAccesosRoles> AclAccesosRoles { get; set; }
        public virtual ICollection<AclModuloAcciones> AclModuloAcciones { get; set; }
        public virtual ICollection<AclParametros> AclParametros { get; set; }
        public virtual ICollection<AclModulos> InverseIdParentModuloNavigation { get; set; }
    }
}
