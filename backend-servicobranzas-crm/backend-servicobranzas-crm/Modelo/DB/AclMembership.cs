﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclMembership
    {
        public uint Id { get; set; }
        public uint? IdUser { get; set; }
        public uint? IdGroup { get; set; }

        public virtual AclGroup IdGroupNavigation { get; set; }
        public virtual AclUser IdUserNavigation { get; set; }
    }
}
