﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmPausasUsuario
    {
        public uint Id { get; set; }
        public uint? IdUser { get; set; }
        public uint? IdPausa { get; set; }
        public DateTime? HoraInicio { get; set; }
        public DateTime? HoraFin { get; set; }
        public string Duracion { get; set; }
    }
}
