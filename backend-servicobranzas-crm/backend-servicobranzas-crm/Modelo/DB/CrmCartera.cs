﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCartera
    {
        public CrmCartera()
        {
            CrmCampania = new HashSet<CrmCampania>();
            CrmCuentas = new HashSet<CrmCuentas>();
        }

        public uint Id { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string Estado { get; set; }

        public virtual ICollection<CrmCampania> CrmCampania { get; set; }
        public virtual ICollection<CrmCuentas> CrmCuentas { get; set; }
    }
}
