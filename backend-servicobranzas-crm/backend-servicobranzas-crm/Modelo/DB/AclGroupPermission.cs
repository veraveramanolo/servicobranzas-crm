﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclGroupPermission
    {
        public uint Id { get; set; }
        public uint? IdGroup { get; set; }
        public uint? IdResource { get; set; }
    }
}
