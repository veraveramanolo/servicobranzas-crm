﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclTipoContacto
    {
        public uint IdTipoContacto { get; set; }
        public string Texto { get; set; }
    }
}
