using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    
    public class Asesores
    {
        public uint IdUsuario { get; set; }
        public int Cantidad { get; set; }
    }

    public class Clientes
    {
        public uint Nocuenta { get; set; }
        public double? Valorpago { get; set; }
        public double? Deudatotal { get; set; }
        public double? Deudavencida { get; set; }
    }
    public class Campania
    {
        public uint Idcampania { get; set; }
    }
    public partial class AclAsignacion
    {
        public string Tipo { get; set; }
        public List<Asesores> Asesores { get; set; }
        public List<Clientes> clientes { get; set; }
        public uint idcampania { get; set; }
    }
}