﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclDepto
    {
        public AclDepto()
        {
            AclCiudad = new HashSet<AclCiudad>();
            CrmCuentasDireccion = new HashSet<CrmCuentasDireccion>();
            CrmCuentasTelefono = new HashSet<CrmCuentasTelefono>();
        }

        public uint IdDepto { get; set; }
        public string DeptoValor { get; set; }
        public uint? IdRegion { get; set; }

        public virtual AclRegion IdRegionNavigation { get; set; }
        public virtual ICollection<AclCiudad> AclCiudad { get; set; }
        public virtual ICollection<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
        public virtual ICollection<CrmCuentasTelefono> CrmCuentasTelefono { get; set; }
    }
}
