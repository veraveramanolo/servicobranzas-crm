﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCampania
    {
        public CrmCampania()
        {
            CrmCuentas = new HashSet<CrmCuentas>();
            CrmCuentasCampania = new HashSet<CrmCuentasCampania>();
            CrmTareas = new HashSet<CrmTareas>();
        }

        public uint Id { get; set; }
        public string Nombre { get; set; }
        public uint? IdCartera { get; set; }
        public string Estado { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public int? UserCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public int? UserActualizacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public uint? IdArbolGestion { get; set; }

        public virtual CrmCartera IdCarteraNavigation { get; set; }
        public virtual ICollection<CrmCuentas> CrmCuentas { get; set; }
        public virtual ICollection<CrmCuentasCampania> CrmCuentasCampania { get; set; }
        public virtual ICollection<CrmTareas> CrmTareas { get; set; }
    }
}
