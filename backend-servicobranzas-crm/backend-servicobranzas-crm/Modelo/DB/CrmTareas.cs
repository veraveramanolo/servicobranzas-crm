﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmTareas
    {
        public CrmTareas()
        {
            CrmTareaGestor = new HashSet<CrmTareaGestor>();
        }

        public uint Id { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public float? Meta { get; set; }
        public uint Idmulticanal { get; set; }
        public uint? IdCampania { get; set; }
        public string Obligatorio { get; set; }
        public int? IdObjetivo { get; set; }
        public uint? UserCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public uint? UserModificacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }

        public virtual CrmCampania IdCampaniaNavigation { get; set; }
        public virtual ICollection<CrmTareaGestor> CrmTareaGestor { get; set; }
    }
}
