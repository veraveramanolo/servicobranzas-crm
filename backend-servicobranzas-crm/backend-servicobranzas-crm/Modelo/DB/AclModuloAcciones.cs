﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclModuloAcciones
    {
        public uint IdModuloAccion { get; set; }
        public uint IdModulo { get; set; }
        public uint IdAccion { get; set; }
        public string Anulado { get; set; }
        public string UsrCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual AclAcciones IdAccionNavigation { get; set; }
        public virtual AclModulos IdModuloNavigation { get; set; }
    }
}
