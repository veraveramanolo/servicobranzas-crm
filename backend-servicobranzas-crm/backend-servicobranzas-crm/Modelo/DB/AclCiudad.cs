﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclCiudad
    {
        public AclCiudad()
        {
            AclDistrito = new HashSet<AclDistrito>();
            CrmCuentasDireccion = new HashSet<CrmCuentasDireccion>();
            CrmCuentasTelefono = new HashSet<CrmCuentasTelefono>();
        }

        public uint IdCiudad { get; set; }
        public uint? IdDepto { get; set; }
        public string CiudadValor { get; set; }

        public virtual AclDepto IdDeptoNavigation { get; set; }
        public virtual ICollection<AclDistrito> AclDistrito { get; set; }
        public virtual ICollection<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
        public virtual ICollection<CrmCuentasTelefono> CrmCuentasTelefono { get; set; }
    }
}
