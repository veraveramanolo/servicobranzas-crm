﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclAcciones
    {
        public AclAcciones()
        {
            AclModuloAcciones = new HashSet<AclModuloAcciones>();
        }

        public uint IdAccion { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Icono { get; set; }
        public string Anulado { get; set; }
        public string UsrCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<AclModuloAcciones> AclModuloAcciones { get; set; }
    }
}
