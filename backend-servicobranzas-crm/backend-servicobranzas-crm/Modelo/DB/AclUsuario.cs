﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclUsuario
    {
        public AclUsuario()
        {
            CrmCuentasCampaniaAcuerdos = new HashSet<CrmCuentasCampaniaAcuerdos>();
            CrmCuentasCampaniaGestor = new HashSet<CrmCuentasCampaniaGestor>();
            CrmTareaGestor = new HashSet<CrmTareaGestor>();
            CrmGestion = new HashSet<CrmGestion>();
        }

        public uint IdUsuario { get; set; }
        public uint IdRol { get; set; }
        public string UserName { get; set; }
        public string Codigo { get; set; }
        public string Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
        public string Cargo { get; set; }
        public string Departamento { get; set; }
        public string Telefono { get; set; }
        public string Agentname { get; set; }
        public string Agentpass { get; set; }
        public string Anulado { get; set; }
        public string Md5Password { get; set; }
        public string UsrCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsrModificacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual AclRoles IdRolNavigation { get; set; }
        public virtual ICollection<CrmCuentasCampaniaAcuerdos> CrmCuentasCampaniaAcuerdos { get; set; }
        public virtual ICollection<CrmCuentasCampaniaGestor> CrmCuentasCampaniaGestor { get; set; }
        public virtual ICollection<CrmTareaGestor> CrmTareaGestor { get; set; }
        public virtual ICollection<CrmGestion> CrmGestion { get; set; }
    }
}
