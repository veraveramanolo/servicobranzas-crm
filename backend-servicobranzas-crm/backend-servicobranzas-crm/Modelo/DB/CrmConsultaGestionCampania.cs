﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmConsultaGestionCampania
    {
        public uint? Idcuentacampania { get; set; }
        public uint? Idcuenta { get; set; }
        public string Asesor { get; set; }
        public string Nombrecliente { get; set; }
        public string Cedula { get; set; }
        public DateTime? Ultimagestion { get; set; }
        public string Acuerdo { get; set; }
        public float? Valoracuerdo { get; set; }
        public string Pago { get; set; }
        public float? Valorpago { get; set; }
        public uint? Idproducto { get; set; }
        public uint? Cuotaspendientes { get; set; }
        public float? Deudavencida { get; set; }
        public float? Deudatotal { get; set; }
        public string Estado { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Extra3 { get; set; }
        public string Extra4 { get; set; }
        public string Extra5 { get; set; }
        public string Extra6 { get; set; }
        public string Extra7 { get; set; }
        public string Extra8 { get; set; }
        public string Extra9 { get; set; }
        public string Extra10 { get; set; }
        public string LabelEstado { get; set; }
        public DateTime? Fechamejorestado { get; set; }
        public string Mejorestado { get; set; }
    }
}
