﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentas
    {
        public CrmCuentas()
        {
            CrmCuentasCampania = new HashSet<CrmCuentasCampania>();
            CrmCuentasContacto = new HashSet<CrmCuentasContacto>();
            CrmCuentasDetalle = new HashSet<CrmCuentasDetalle>();
            CrmCuentasDireccion = new HashSet<CrmCuentasDireccion>();
            CrmCuentasEmail = new HashSet<CrmCuentasEmail>();
            CrmCuentasTelefono = new HashSet<CrmCuentasTelefono>();
            CrmGestion = new HashSet<CrmGestion>();
        }

        public uint Id { get; set; }
        public string Nombre { get; set; }
        public uint? IdCarteraInicial { get; set; }
        public uint? IdCampaniaInicial { get; set; }
        public string Identificacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string Estado { get; set; }
        public string GestorCreacion { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public virtual CrmCampania IdCampaniaInicialNavigation { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public virtual CrmCartera IdCarteraInicialNavigation { get; set; }
        public virtual ICollection<CrmCuentasCampania> CrmCuentasCampania { get; set; }
        public virtual ICollection<CrmCuentasContacto> CrmCuentasContacto { get; set; }
        public virtual ICollection<CrmCuentasDetalle> CrmCuentasDetalle { get; set; }
        public virtual ICollection<CrmCuentasDireccion> CrmCuentasDireccion { get; set; }
        public virtual ICollection<CrmCuentasEmail> CrmCuentasEmail { get; set; }
        public virtual ICollection<CrmCuentasTelefono> CrmCuentasTelefono { get; set; }
        public virtual ICollection<CrmGestion> CrmGestion { get; set; }
    }
}
