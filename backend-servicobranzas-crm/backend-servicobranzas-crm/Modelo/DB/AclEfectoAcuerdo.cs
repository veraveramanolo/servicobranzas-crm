﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclEfectoAcuerdo
    {
        public int IdEfecto { get; set; }
        public string EfectoValor { get; set; }
    }
}
