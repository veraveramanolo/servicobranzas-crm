﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaGestor
    {
        public uint Id { get; set; }
        public uint? IdCuentaCampania { get; set; }
        public uint? IdUsuario { get; set; }
        public string Estado { get; set; }

        public virtual CrmCuentasCampania IdCuentaCampaniaNavigation { get; set; }
        public virtual AclUsuario IdUsuarioNavigation { get; set; }
    }
}
