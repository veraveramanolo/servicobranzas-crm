﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaDetalleHistorico
    {
        public uint Id { get; set; }
        public uint? IdCuentaCampania { get; set; }
        public string Campo { get; set; }
        public string Valor { get; set; }

        public virtual CrmCuentasCampania IdCuentaCampaniaNavigation { get; set; }
    }
}
