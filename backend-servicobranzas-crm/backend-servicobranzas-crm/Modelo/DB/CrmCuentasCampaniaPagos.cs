﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasCampaniaPagos
    {
        public uint Id { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaPago { get; set; }
        public float? ValorPago { get; set; }
        public uint? IdCuentaCampaniaProducto { get; set; }
        public uint? IdCuentaCampaniaAcuerdo { get; set; }
        public string TipoPago { get; set; }
        public string NumeroRecibo { get; set; }
        public int? Cuota { get; set; }
        public string Plazo { get; set; }
        public string MedioPago { get; set; }
        public string Estado { get; set; }
        public string Descripcion { get; set; }
        public int IdCuenta { get; set; }
        public string GestorOficial { get; set; }
        public string GestorIngreso { get; set; }

        public virtual CrmCuentasCampaniaAcuerdos IdCuentaCampaniaAcuerdoNavigation { get; set; }
        public virtual CrmCuentasCampaniaProductos IdCuentaCampaniaProductoNavigation { get; set; }
    }
}
