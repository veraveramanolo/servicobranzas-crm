﻿using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class AclPlantilla
    {
        public int IdPlantilla { get; set; }
        public string NombrePlantilla { get; set; }
        public string BodyPlantilla { get; set; }
        public string EstadoPlantilla { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public int UsrCreacion { get; set; }
        public int? UsrModificacion { get; set; }
        public string TipoPlantilla { get; set; }
    }
}
