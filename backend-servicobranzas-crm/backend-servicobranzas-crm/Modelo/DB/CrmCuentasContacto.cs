using System;
using System.Collections.Generic;

namespace backend_servicobranzas_crm.Modelo.DB
{
    public partial class CrmCuentasContacto
    {
        public CrmCuentasContacto() {
            CrmCuentasTelefono = new HashSet<CrmCuentasTelefono>();
        }
        public uint Id { get; set; }
        public uint? IdCuenta { get; set; }
        public string Nombre { get; set; }
        public string Documento { get; set; }
        public uint? IdRelacion { get; set; }
        public string Observacion { get; set; }
        public string Estado { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string ObservacionI { get; set; }
        public string TipoDatoRef { get; set; }
        public string ValorRef { get; set; }
        public string CedulaRef { get; set; }

        public virtual CrmCuentas IdCuentaNavigation { get; set; }
        public virtual AclRelacion IdRelacionNavigation { get; set; }
        public virtual ICollection<CrmCuentasTelefono> CrmCuentasTelefono { get; set; }
    }
}
