﻿using System.ComponentModel.DataAnnotations;

namespace backend_servicobranzas_crm.Modelo.Request
{
    public class AuthRequest
    {
        [Required]
        public string User { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
