﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend_servicobranzas_crm.Modelo.Response
{
    public class ModeloDetalleCliente
    {
        public uint? idCuenta { get; set; }
        public decimal? deuda { get; set; }
        public decimal? deudatotal { get; set; }
        public decimal? porvencer { get; set; }
        public float? pago { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string cedula { get; set; }
        public string contacto { get; set; }
        public string estado { get; set; }
        public string labelestado { get; set; }
        public string telefono { get; set; }
        public string tipoTelefono { get; set; }
        public string campania { get; set; }

    }
}
