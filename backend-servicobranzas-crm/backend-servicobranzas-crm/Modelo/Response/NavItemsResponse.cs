﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend_servicobranzas_crm.Modelo.Response
{

    public class Child
    {
        public string name { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
    }

    public class NavItemsResponse
    {
        public string name { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
        public List<Child> children { get; set; }
    }

    public class NavItemsCabecera
    {
        public string name { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
        public List<NavItemsResponse> children { get; set; }
    }


}
