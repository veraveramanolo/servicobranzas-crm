using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;


namespace backend_servicobranzas_crm.Modelo.Response
{
    public class MultiselectResponse
    {
        public string Mensage { get; set; }
        public int Exito { get; set; }
        public object Data { get; set; }
        public object Asesores { get; set; }
    }
}
