﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend_servicobranzas_crm.Modelo.Response
{
    public class ModeloRespuesta
    {
        public string Mensage { get; set; }
        public int Exito { get; set; }
        public object Data { get; set; }
    }
}
