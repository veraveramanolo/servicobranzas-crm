using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;


namespace backend_servicobranzas_crm.Modelo.Response
{
    public class ModeloFiltroExtra
    {
        public int Exito { get; set; }
        public string Mensage { get; set; }
        public object Extra1 { get; set; }
        public object Extra2 { get; set; }
        public object Extra3 { get; set; }
        public object Extra4 { get; set; }
        public object Extra5 { get; set; }
        public object Extra6 { get; set; }
        public object Extra7 { get; set; }
        public object Extra8 { get; set; }
        public object Extra9 { get; set; }
        public object Extra10 { get; set; }
    }
}
