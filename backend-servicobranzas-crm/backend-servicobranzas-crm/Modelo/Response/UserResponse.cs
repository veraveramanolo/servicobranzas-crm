﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend_servicobranzas_crm.Modelo.DB;


namespace backend_servicobranzas_crm.Modelo.Response
{
    public class UserResponse
    {
        public AclUsuario Usuario { get; set; }
        public string Token { get; set; }
        public List<NavItemsCabecera> Menu { get; set; }
    }
}
