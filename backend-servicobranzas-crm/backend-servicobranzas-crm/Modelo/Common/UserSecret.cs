﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend_servicobranzas_crm.Modelo.Common
{
    public class UserSecret
    {
        public string LlaveSecreta { get; set; }
    }
}
