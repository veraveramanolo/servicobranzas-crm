cd /home/admin/servicobranzas-crm/backend-servicobranzas-crm/
export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
nohup /root/dotnet/dotnet run --project backend-servicobranzas-crm/backend-servicobranzas-crm.csproj >/var/log/dotnet &

cd /home/admin/servicobranzas-crm/frontend-servicobranzas-crm
nohup ng serve --host 0.0.0.0 >/var/log/angular.log &

