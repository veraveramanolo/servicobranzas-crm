-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ps_crm
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.21-MariaDB-0+deb11u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__EFMigrationsHistory`
--

DROP TABLE IF EXISTS `__EFMigrationsHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_accesos_roles`
--

DROP TABLE IF EXISTS `acl_accesos_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_accesos_roles` (
  `id_acceso_rol` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_rol` int(10) unsigned DEFAULT NULL,
  `id_modulo` int(10) unsigned DEFAULT NULL,
  `anulado` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'NO',
  `usr_creacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `usr_modificacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_acceso_rol`),
  KEY `FK__acl_modulos` (`id_modulo`),
  KEY `FK_acl_accesos_roles_acl_roles` (`id_rol`),
  CONSTRAINT `FK__acl_modulos` FOREIGN KEY (`id_modulo`) REFERENCES `acl_modulos` (`id_modulo`),
  CONSTRAINT `FK_acl_accesos_roles_acl_roles` FOREIGN KEY (`id_rol`) REFERENCES `acl_roles` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_acciones`
--

DROP TABLE IF EXISTS `acl_acciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_acciones` (
  `id_accion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nombre` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icono` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `anulado` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'NO',
  `usr_creacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `usr_modificacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_accion`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_area`
--

DROP TABLE IF EXISTS `acl_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_area` (
  `id_area` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valor_area` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_ciudad`
--

DROP TABLE IF EXISTS `acl_ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_ciudad` (
  `id_ciudad` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_depto` int(10) unsigned DEFAULT NULL,
  `ciudad_valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_ciudad`),
  KEY `FK_acl_ciudad_acl_depto` (`id_depto`),
  CONSTRAINT `FK_acl_ciudad_acl_depto` FOREIGN KEY (`id_depto`) REFERENCES `acl_depto` (`id_depto`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_depto`
--

DROP TABLE IF EXISTS `acl_depto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_depto` (
  `id_depto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `depto_valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_region` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_depto`),
  KEY `fk_acl_depto_1_idx` (`id_region`),
  CONSTRAINT `fk_acl_depto_acl_region` FOREIGN KEY (`id_region`) REFERENCES `acl_region` (`id_region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_distrito`
--

DROP TABLE IF EXISTS `acl_distrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_distrito` (
  `id_distrito` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ciudad` int(10) unsigned DEFAULT NULL,
  `distrito_valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_distrito`),
  KEY `FK_acl_distrito_acl_ciudad` (`id_ciudad`),
  CONSTRAINT `FK_acl_distrito_acl_ciudad` FOREIGN KEY (`id_ciudad`) REFERENCES `acl_ciudad` (`id_ciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_efecto_acuerdo`
--

DROP TABLE IF EXISTS `acl_efecto_acuerdo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_efecto_acuerdo` (
  `id_efecto` int(11) NOT NULL,
  `efecto_valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_efecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_estados`
--

DROP TABLE IF EXISTS `acl_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_estados` (
  `estado` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cantidad` bigint(20) DEFAULT NULL,
  `texto` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `alerta` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_lista_asesoresasig`
--

DROP TABLE IF EXISTS `acl_lista_asesoresasig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_lista_asesoresasig` (
  `id_usuario` int(11) NOT NULL,
  `asesor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `asignadosClientes` int(11) DEFAULT NULL,
  `activos` int(11) DEFAULT NULL,
  `inactivos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_modulo_acciones`
--

DROP TABLE IF EXISTS `acl_modulo_acciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_modulo_acciones` (
  `id_modulo_accion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_modulo` int(10) unsigned NOT NULL,
  `id_accion` int(10) unsigned NOT NULL,
  `anulado` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'NO',
  `usr_creacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `usr_modificacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_modulo_accion`),
  KEY `FK_acl_modulo_acciones_acl_acciones` (`id_accion`),
  KEY `FK_acl_modulo_acciones_acl_modulos` (`id_modulo`),
  CONSTRAINT `FK_acl_modulo_acciones_acl_acciones` FOREIGN KEY (`id_accion`) REFERENCES `acl_acciones` (`id_accion`),
  CONSTRAINT `FK_acl_modulo_acciones_acl_modulos` FOREIGN KEY (`id_modulo`) REFERENCES `acl_modulos` (`id_modulo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_modulos`
--

DROP TABLE IF EXISTS `acl_modulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_modulos` (
  `id_modulo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nombre` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icono` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `modulo_padre` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ruta_directorio` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_parent_modulo` int(10) unsigned DEFAULT NULL,
  `visible` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `orden_presentacion` int(10) unsigned DEFAULT NULL,
  `anulado` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'NO',
  `usr_creacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `usr_modificacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_modulo`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `FK_acl_modulos_acl_modulos` (`id_parent_modulo`),
  CONSTRAINT `FK_acl_modulos_acl_modulos` FOREIGN KEY (`id_parent_modulo`) REFERENCES `acl_modulos` (`id_modulo`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_multicanal_estados`
--

DROP TABLE IF EXISTS `acl_multicanal_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_multicanal_estados` (
  `gestion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mejorgestion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_parametro_objetivo`
--

DROP TABLE IF EXISTS `acl_parametro_objetivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_parametro_objetivo` (
  `id_parametro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valor_parametro` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_parametro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_parametros`
--

DROP TABLE IF EXISTS `acl_parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_parametros` (
  `id_parametro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_parametro` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `valor_parametro` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_modulo` int(10) unsigned NOT NULL,
  `Estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'A',
  `user_creacion` int(10) unsigned DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `user_modificacion` int(10) unsigned DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `campo_ref1` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `campo_ref2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_parametro`),
  KEY `fk_acl_parametros_acl_modulos_idx` (`id_modulo`),
  CONSTRAINT `fk_acl_parametros_acl_modulos` FOREIGN KEY (`id_modulo`) REFERENCES `acl_modulos` (`id_modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_plantilla`
--

DROP TABLE IF EXISTS `acl_plantilla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_plantilla` (
  `id_plantilla` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de plantilla',
  `nombre_plantilla` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre de plantilla',
  `body_plantilla` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Cuerpo de plantilla',
  `estado_plantilla` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'A' COMMENT 'Estado: Activo, Inactivo',
  `fecha_creacion` datetime NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usr_creacion` int(11) NOT NULL,
  `usr_modificacion` int(11) DEFAULT NULL,
  `tipo_plantilla` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Tipo: Sms, Dinomi, Email',
  PRIMARY KEY (`id_plantilla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_region`
--

DROP TABLE IF EXISTS `acl_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_region` (
  `id_region` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_valor` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_relacion`
--

DROP TABLE IF EXISTS `acl_relacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_relacion` (
  `id_relacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valor_relacion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_relacion`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_roles`
--

DROP TABLE IF EXISTS `acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_roles` (
  `id_rol` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(80) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `anulado` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'NO',
  `usr_creacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `usr_modificacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_tipo_contacto`
--

DROP TABLE IF EXISTS `acl_tipo_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_tipo_contacto` (
  `id_tipo_contacto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_tipo_contacto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_tipo_info`
--

DROP TABLE IF EXISTS `acl_tipo_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_tipo_info` (
  `id_tipo_info` int(11) NOT NULL AUTO_INCREMENT,
  `info_valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `info_nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_tipo_info`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_tipo_objetivo`
--

DROP TABLE IF EXISTS `acl_tipo_objetivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_tipo_objetivo` (
  `id_tipoobjetivo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valor_tipoobjetivo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_tipoobjetivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_usuario`
--

DROP TABLE IF EXISTS `acl_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_usuario` (
  `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_rol` int(10) unsigned NOT NULL,
  `user_name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `codigo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `cedula` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nombres` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `apellidos` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `cargo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `departamento` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `telefono` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `agentname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `agentpass` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `anulado` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'NO',
  `md5_password` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `usr_creacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `usr_modificacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `cedula` (`cedula`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `FK_acl_usuario_acl_roles` (`id_rol`),
  CONSTRAINT `FK_acl_usuario_acl_roles` FOREIGN KEY (`id_rol`) REFERENCES `acl_roles` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_zona`
--

DROP TABLE IF EXISTS `acl_zona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acl_zona` (
  `id_zona` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zona_valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_zona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_arbol_gestion`
--

DROP TABLE IF EXISTS `crm_arbol_gestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_arbol_gestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned DEFAULT NULL,
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `peso` int(11) DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_tipo_gestion` int(10) unsigned DEFAULT NULL,
  `id_tipo_contacto` int(10) unsigned DEFAULT NULL,
  `alerta` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `label_estado` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `label_nivel2` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_arbol_gestion_idx` (`id_parent`),
  KEY `fk_arbol_gestion2_idx` (`id_tipo_contacto`),
  KEY `fk_arbol_gestion3_idx` (`id_tipo_gestion`),
  CONSTRAINT `fk_arbol_gestion` FOREIGN KEY (`id_parent`) REFERENCES `crm_arbol_gestion` (`id`),
  CONSTRAINT `fk_arbol_gestion2` FOREIGN KEY (`id_tipo_contacto`) REFERENCES `crm_arbol_gestion_tipo_contacto` (`id`),
  CONSTRAINT `fk_arbol_gestion3` FOREIGN KEY (`id_tipo_gestion`) REFERENCES `crm_arbol_gestion_tipo_gestion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_arbol_gestion_tipo_contacto`
--

DROP TABLE IF EXISTS `crm_arbol_gestion_tipo_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_arbol_gestion_tipo_contacto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_arbol_gestion_tipo_gestion`
--

DROP TABLE IF EXISTS `crm_arbol_gestion_tipo_gestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_arbol_gestion_tipo_gestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_asignacion_cab`
--

DROP TABLE IF EXISTS `crm_asignacion_cab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_asignacion_cab` (
  `id_asignacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_tarea` int(10) unsigned NOT NULL,
  `tipoasignacion` enum('M','A') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `user_creacion` int(11) DEFAULT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `user_actualizacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_asignacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_asignacion_det`
--

DROP TABLE IF EXISTS `crm_asignacion_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_asignacion_det` (
  `id_asignacion_det` int(11) NOT NULL AUTO_INCREMENT,
  `id_asignacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_asignacion_det`),
  KEY `fk_crm_asignacion_det_crm_asignacion_cab_idx` (`id_asignacion`),
  CONSTRAINT `fk_crm_asignacion_det_crm_asignacion_cab` FOREIGN KEY (`id_asignacion`) REFERENCES `crm_asignacion_cab` (`id_asignacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_caja`
--

DROP TABLE IF EXISTS `crm_caja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_caja` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `comentario` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_gestor` int(10) unsigned NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `id_cartera` int(10) unsigned NOT NULL,
  `id_campania` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_campania`
--

DROP TABLE IF EXISTS `crm_campania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_campania` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_cartera` int(10) unsigned DEFAULT NULL,
  `estado` enum('A','I','V','C') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `user_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `user_actualizacion` int(11) DEFAULT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cartera_idx` (`id_cartera`),
  CONSTRAINT `fk_cartera` FOREIGN KEY (`id_cartera`) REFERENCES `crm_cartera` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cartera`
--

DROP TABLE IF EXISTS `crm_cartera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cartera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tipo` enum('Cedente','Vendida') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_consulta_gestion_campania`
--

DROP TABLE IF EXISTS `crm_consulta_gestion_campania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_consulta_gestion_campania` (
  `idcuentacampania` int(10) unsigned DEFAULT NULL,
  `idcuenta` int(10) unsigned DEFAULT NULL,
  `asesor` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nombrecliente` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `cedula` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ultimagestion` datetime DEFAULT NULL,
  `acuerdo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '',
  `valoracuerdo` float(10,4) unsigned DEFAULT 0.0000,
  `pago` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '',
  `valorpago` float(10,4) unsigned DEFAULT 0.0000,
  `idproducto` int(10) unsigned DEFAULT NULL,
  `cuotaspendientes` int(10) unsigned DEFAULT 0,
  `deudavencida` float(10,4) unsigned DEFAULT 0.0000,
  `deudatotal` float(10,4) DEFAULT 0.0000,
  `estado` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '',
  `EXTRA1` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA2` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA3` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA4` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA5` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA6` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA7` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA8` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA9` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA10` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `label_estado` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fechamejorestado` datetime DEFAULT NULL,
  `mejorestado` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_consulta_gestion_campania_data`
--

DROP TABLE IF EXISTS `crm_consulta_gestion_campania_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_consulta_gestion_campania_data` (
  `id` int(10) unsigned NOT NULL,
  `idcuentacampania` int(10) unsigned DEFAULT NULL,
  `idcuenta` int(10) unsigned DEFAULT NULL,
  `asesor` varchar(150) DEFAULT NULL,
  `nombrecliente` varchar(100) DEFAULT NULL,
  `cedula` varchar(45) DEFAULT NULL,
  `ultimagestion` varchar(100) DEFAULT NULL,
  `acuerdo` varchar(100) DEFAULT NULL,
  `valoracuerdo` float(10,4) unsigned DEFAULT NULL,
  `pago` varchar(100) DEFAULT NULL,
  `valorpago` float(10,4) unsigned DEFAULT NULL,
  `idproducto` int(10) unsigned DEFAULT NULL,
  `cuotaspendientes` int(10) unsigned DEFAULT NULL,
  `montoinicial` float(10,4) unsigned DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas`
--

DROP TABLE IF EXISTS `crm_cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_cartera_inicial` int(10) unsigned DEFAULT NULL,
  `id_campania_inicial` int(10) unsigned DEFAULT NULL,
  `identificacion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` enum('A','E','P') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gestor_creacion` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identificacion_UNIQUE` (`identificacion`),
  KEY `fk_campania_idx` (`id_campania_inicial`),
  KEY `fk_cartera_idx` (`id_cartera_inicial`),
  CONSTRAINT `fk_campania_cuenta` FOREIGN KEY (`id_campania_inicial`) REFERENCES `crm_campania` (`id`),
  CONSTRAINT `fk_cartera_cuenta` FOREIGN KEY (`id_cartera_inicial`) REFERENCES `crm_cartera` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8408 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania`
--

DROP TABLE IF EXISTS `crm_cuentas_campania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `id_campania` int(10) unsigned DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_arbol_gestion_mejor_gestion` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crm_cuentas_campania_1_idx` (`id_arbol_gestion_mejor_gestion`),
  KEY `fk_cuentas_camp2_idx` (`id_campania`),
  KEY `fk_cuentas_camp_idx` (`id_cuenta`),
  CONSTRAINT `fk_crm_cuentas_campania_1` FOREIGN KEY (`id_arbol_gestion_mejor_gestion`) REFERENCES `crm_arbol_gestion` (`id`),
  CONSTRAINT `fk_cuentas_camp` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`),
  CONSTRAINT `fk_cuentas_camp2` FOREIGN KEY (`id_campania`) REFERENCES `crm_campania` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20689 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_acuerdos`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_acuerdos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_acuerdos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_acuerdo` date DEFAULT NULL,
  `tipo_acuerdo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor_acuerdo` float(10,2) DEFAULT NULL,
  `id_cuenta_campania_producto` int(10) unsigned DEFAULT NULL,
  `id_gestor` int(10) unsigned DEFAULT NULL,
  `efecto` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `estado` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cuota` int(11) DEFAULT NULL,
  `plazo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `probabilidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `comentario` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_visita` datetime DEFAULT NULL,
  `hora_acuerdo` time DEFAULT NULL,
  `id_direccion` int(10) unsigned DEFAULT NULL,
  `estado_not` enum('V','F') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'V',
  `usr_modificacion` int(10) unsigned DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `cant_modificacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_crm_cuentas_campania_acuerdos_crm_cuentas_campania_productos` (`id_cuenta_campania_producto`),
  KEY `FK_crm_cuentas_campania_acuerdos_crm_cuentas_direccion_idx` (`id_direccion`),
  KEY `FK_crm_cuentas_campania_acuerdos_acl_usuario` (`id_gestor`),
  CONSTRAINT `FK_crm_cuentas_campania_acuerdos_acl_usuario` FOREIGN KEY (`id_gestor`) REFERENCES `acl_usuario` (`id_usuario`),
  CONSTRAINT `FK_crm_cuentas_campania_acuerdos_crm_cuentas_campania_productos` FOREIGN KEY (`id_cuenta_campania_producto`) REFERENCES `crm_cuentas_campania_productos` (`id`),
  CONSTRAINT `fk_crm_cuentas_campania_acuerdos_crm_cuentas_direccion` FOREIGN KEY (`id_direccion`) REFERENCES `crm_cuentas_direccion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_detalle_historico`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_detalle_historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_detalle_historico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta_campania` int(10) unsigned DEFAULT NULL,
  `campo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_campania_hist_idx` (`id_cuenta_campania`),
  CONSTRAINT `fk_cuenta_campania_hist` FOREIGN KEY (`id_cuenta_campania`) REFERENCES `crm_cuentas_campania` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_gestor`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_gestor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_gestor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta_campania` int(10) unsigned DEFAULT NULL,
  `id_usuario` int(10) unsigned DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_crm_cuentas_campania_gestor_crm_cuentas_campania` (`id_cuenta_campania`),
  KEY `FK_crm_cuentas_campania_gestor_acl_usuario` (`id_usuario`),
  CONSTRAINT `FK_crm_cuentas_campania_gestor_acl_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `acl_usuario` (`id_usuario`),
  CONSTRAINT `FK_crm_cuentas_campania_gestor_crm_cuentas_campania` FOREIGN KEY (`id_cuenta_campania`) REFERENCES `crm_cuentas_campania` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20689 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_pagos`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `valor_pago` float(10,4) DEFAULT NULL,
  `id_cuenta_campania_producto` int(10) unsigned DEFAULT NULL,
  `id_cuenta_campania_acuerdo` int(10) unsigned DEFAULT NULL,
  `tipo_pago` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `numero_recibo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cuota` int(11) DEFAULT NULL,
  `plazo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `medio_pago` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'A',
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_cuenta` int(11) NOT NULL,
  `gestor_oficial` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gestor_ingreso` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_crm_cuentas_campania_pagos_crm_cuentas_campania_acuerdos` (`id_cuenta_campania_acuerdo`),
  KEY `FK_crm_cuentas_campania_pagos_crm_cuentas_campania_productos` (`id_cuenta_campania_producto`),
  CONSTRAINT `FK_crm_cuentas_campania_pagos_crm_cuentas_campania_acuerdos` FOREIGN KEY (`id_cuenta_campania_acuerdo`) REFERENCES `crm_cuentas_campania_acuerdos` (`id`),
  CONSTRAINT `FK_crm_cuentas_campania_pagos_crm_cuentas_campania_productos` FOREIGN KEY (`id_cuenta_campania_producto`) REFERENCES `crm_cuentas_campania_productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_productos`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta_campania` int(10) unsigned DEFAULT NULL,
  `valor` decimal(10,4) DEFAULT NULL,
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cuotas_pendientes` int(11) DEFAULT NULL,
  `crm_cuentas_campania_productoscol` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `crm_cuentas_campania_productoscol1` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `edadmor` int(11) DEFAULT NULL,
  `deudatotal` decimal(10,4) DEFAULT NULL,
  `valorporvencer` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_campania_idx` (`id_cuenta_campania`),
  CONSTRAINT `fk_cuenta_campania` FOREIGN KEY (`id_cuenta_campania`) REFERENCES `crm_cuentas_campania` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22735 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_productos_articulos`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_productos_articulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_productos_articulos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta_campania_producto` int(10) unsigned DEFAULT NULL,
  `articulo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=208228 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_campania_productos_detalle`
--

DROP TABLE IF EXISTS `crm_cuentas_campania_productos_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_campania_productos_detalle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta_campania_producto` int(10) unsigned DEFAULT NULL,
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` decimal(10,4) DEFAULT NULL,
  `estado` enum('Cancelada','Pendiente') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuentas_campania_productos_detalle_idx` (`id_cuenta_campania_producto`),
  CONSTRAINT `fk_cuentas_campania_productos_detalle` FOREIGN KEY (`id_cuenta_campania_producto`) REFERENCES `crm_cuentas_campania_productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=191854 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_contacto`
--

DROP TABLE IF EXISTS `crm_cuentas_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_contacto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `documento` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_relacion` int(10) unsigned DEFAULT NULL,
  `observacion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'A',
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `observacionI` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CEDULA_REF` varchar(45) DEFAULT NULL,
  `tipo_dato_ref` varchar(45) DEFAULT NULL,
  `valor_ref` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crm_cuentas_contacto_idx` (`id_cuenta`),
  KEY `fk_crm_cuentas_contacto_relacion` (`id_relacion`),
  CONSTRAINT `fk_crm_cuentas_contacto` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`),
  CONSTRAINT `fk_crm_cuentas_contacto_relacion` FOREIGN KEY (`id_relacion`) REFERENCES `acl_relacion` (`id_relacion`)
) ENGINE=InnoDB AUTO_INCREMENT=30922 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_detalle`
--

DROP TABLE IF EXISTS `crm_cuentas_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_detalle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `campo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_cuenta_campania_producto` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuenta_detalle_idx` (`id_cuenta`),
  CONSTRAINT `fk_cuenta_detalle` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155641 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_direccion`
--

DROP TABLE IF EXISTS `crm_cuentas_direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_direccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `tipo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_depto` int(10) unsigned DEFAULT NULL,
  `id_ciudad` int(10) unsigned DEFAULT NULL,
  `id_distrito` int(10) unsigned DEFAULT NULL,
  `id_zona` int(10) unsigned DEFAULT NULL,
  `id_area` int(10) unsigned DEFAULT NULL,
  `barrio` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `info1` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'A',
  `observacionI` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_crm_cuentas_direccion_5_idx` (`id_area`),
  KEY `fk_crm_cuentas_direccion_2_idx` (`id_ciudad`),
  KEY `fk_cuentas_direccion_idx` (`id_cuenta`),
  KEY `fk_crm_cuentas_direccion_1_idx` (`id_depto`),
  KEY `fk_crm_cuentas_direccion_3_idx` (`id_distrito`),
  KEY `fk_crm_cuentas_direccion_4_idx` (`id_zona`),
  CONSTRAINT `fk_crm_cuentas_direccion_1` FOREIGN KEY (`id_depto`) REFERENCES `acl_depto` (`id_depto`),
  CONSTRAINT `fk_crm_cuentas_direccion_2` FOREIGN KEY (`id_ciudad`) REFERENCES `acl_ciudad` (`id_ciudad`),
  CONSTRAINT `fk_crm_cuentas_direccion_3` FOREIGN KEY (`id_distrito`) REFERENCES `acl_distrito` (`id_distrito`),
  CONSTRAINT `fk_crm_cuentas_direccion_4` FOREIGN KEY (`id_zona`) REFERENCES `acl_zona` (`id_zona`),
  CONSTRAINT `fk_crm_cuentas_direccion_5` FOREIGN KEY (`id_area`) REFERENCES `acl_area` (`id_area`),
  CONSTRAINT `fk_cuentas_direccion` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8407 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_email`
--

DROP TABLE IF EXISTS `crm_cuentas_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tipo` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `info` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'A',
  `observacionI` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_crm_cuentas_email` (`id_cuenta`,`email`),
  KEY `fk_crm_cuentas_email_idx` (`id_cuenta`),
  CONSTRAINT `fk_crm_cuentas_email` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_notas`
--

DROP TABLE IF EXISTS `crm_cuentas_notas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_notas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_creacion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_actualizacion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `usuario_creacion` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_cuentas_telefono`
--

DROP TABLE IF EXISTS `crm_cuentas_telefono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_cuentas_telefono` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `tipo` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_depto` int(10) unsigned DEFAULT NULL,
  `id_ciudad` int(10) unsigned DEFAULT NULL,
  `id_distrito` int(10) unsigned DEFAULT NULL,
  `info2` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'A',
  `observacionI` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `conteo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_crm_cuentas_telefono` (`id_cuenta`,`valor`),
  KEY `FK_crm_cuentas_telefono_acl_ciudad` (`id_ciudad`),
  KEY `fk_cuentas_telefono_idx` (`id_cuenta`),
  KEY `FK_crm_cuentas_telefono_acl_depto` (`id_depto`),
  KEY `FK_crm_cuentas_telefono_acl_distrito` (`id_distrito`),
  CONSTRAINT `FK_crm_cuentas_telefono_acl_ciudad` FOREIGN KEY (`id_ciudad`) REFERENCES `acl_ciudad` (`id_ciudad`),
  CONSTRAINT `FK_crm_cuentas_telefono_acl_depto` FOREIGN KEY (`id_depto`) REFERENCES `acl_depto` (`id_depto`),
  CONSTRAINT `FK_crm_cuentas_telefono_acl_distrito` FOREIGN KEY (`id_distrito`) REFERENCES `acl_distrito` (`id_distrito`),
  CONSTRAINT `fk_cuentas_telefono` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16348 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_gestion`
--

DROP TABLE IF EXISTS `crm_gestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_gestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cuentas_campania_productos` int(10) unsigned DEFAULT NULL,
  `id_telefono` int(10) unsigned DEFAULT NULL,
  `hora_gestion` datetime DEFAULT NULL,
  `id_gestion_dinomi` int(10) unsigned DEFAULT NULL,
  `tiempo_llamada` int(11) DEFAULT NULL,
  `estado_llamada` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_arbol_decision` int(10) unsigned DEFAULT NULL,
  `detalle_gestion` text CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tipo_gestion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_cuenta` int(10) unsigned DEFAULT NULL,
  `id_cuenta_campania` int(10) unsigned DEFAULT NULL,
  `id_gestor` int(10) unsigned NOT NULL,
  `canal` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gestion_3_idx` (`id_arbol_decision`),
  KEY `fk_crm_gestion_cuenta` (`id_cuenta`),
  KEY `fk_gestion_4_idx` (`id_cuenta_campania`),
  KEY `fk_gestion_1_idx` (`id_cuentas_campania_productos`),
  KEY `FK_crm_gestion_crm_cuentas_telefono` (`id_telefono`),
  KEY `FK_crm_gestion_acl_usuario` (`id_gestor`),
  CONSTRAINT `FK_crm_gestion_acl_usuario` FOREIGN KEY (`id_gestor`) REFERENCES `acl_usuario` (`id_usuario`),
  CONSTRAINT `FK_crm_gestion_crm_cuentas_telefono` FOREIGN KEY (`id_telefono`) REFERENCES `crm_cuentas_telefono` (`id`),
  CONSTRAINT `fk_crm_gestion_cuenta` FOREIGN KEY (`id_cuenta`) REFERENCES `crm_cuentas` (`id`),
  CONSTRAINT `fk_gestion_1` FOREIGN KEY (`id_cuentas_campania_productos`) REFERENCES `crm_cuentas_campania_productos` (`id`),
  CONSTRAINT `fk_gestion_3` FOREIGN KEY (`id_arbol_decision`) REFERENCES `crm_arbol_gestion` (`id`),
  CONSTRAINT `fk_gestion_4` FOREIGN KEY (`id_cuenta_campania`) REFERENCES `crm_cuentas_campania` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_matriz_acuerdos`
--

DROP TABLE IF EXISTS `crm_matriz_acuerdos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_matriz_acuerdos` (
  `agenteasignado` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_gestor` int(11) DEFAULT NULL,
  `nodocumento` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `noproducto` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ultgestion` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fechacreacion` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fechapromesa` date DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `claseacuerdo` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `probabilidad` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `estadoacuerdo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `valor_acuerdo` float DEFAULT NULL,
  `valor_pago` float DEFAULT NULL,
  `porcentcumpli` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ultimagestion` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `plazo` int(11) DEFAULT NULL,
  `saldodeuda` float DEFAULT NULL,
  `saldovencido` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_matriz_pagos`
--

DROP TABLE IF EXISTS `crm_matriz_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_matriz_pagos` (
  `agente` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nodocumento` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nombrecliente` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `noproducto` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estadocliente` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `pagoconacuerdo` float DEFAULT NULL,
  `clasepago` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pagocargado` float DEFAULT NULL,
  `fechacargado` datetime DEFAULT NULL,
  `saldodeuda` float DEFAULT NULL,
  `saldovencido` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_multicanal`
--

DROP TABLE IF EXISTS `crm_multicanal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_multicanal` (
  `ASESOR` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ID_USUARIO` int(10) unsigned DEFAULT NULL,
  `NOCUENTA` int(10) unsigned NOT NULL,
  `NOMBRECOMPLETO` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `IDENTIFICACION` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ULTIMAGESTION` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `GESTION` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `DIASMORA` int(11) DEFAULT NULL,
  `MEJORGESTION` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `FECHAMEJORGESTION` datetime DEFAULT NULL,
  `DEUDATOTAL` float(10,4) DEFAULT NULL,
  `STATUSCARTERA` enum('A','I') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `FECHAACUERDO` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `VALORACUERDO` double(10,4) DEFAULT NULL,
  `VALORPAGO` double(10,4) DEFAULT NULL,
  `FECHA_PAGO` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `TOTALACUERDO` double(10,4) DEFAULT NULL,
  `TOTALPAGO` double(10,4) DEFAULT NULL,
  `CUOTA` int(11) DEFAULT NULL,
  `TOTALCUOTAS` int(11) DEFAULT NULL,
  `DEUDAVENCIDA` float DEFAULT NULL,
  `EXTRA1` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA2` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA3` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA4` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA5` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA6` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA7` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA8` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA9` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA10` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_multicanal_cab`
--

DROP TABLE IF EXISTS `crm_multicanal_cab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_multicanal_cab` (
  `idmulticanal` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `filtro` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status` enum('A','I') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `user_creacion` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_campania` int(10) unsigned NOT NULL,
  `user_modificacion` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `campo_ref1` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `campo_ref2` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `campo_ref3` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`idmulticanal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_multicanal_detalle`
--

DROP TABLE IF EXISTS `crm_multicanal_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_multicanal_detalle` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDMULTICANAL` int(10) unsigned NOT NULL,
  `ASESOR` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ID_USUARIO` int(10) unsigned DEFAULT NULL,
  `NOCUENTA` int(10) unsigned NOT NULL,
  `NOMBRECOMPLETO` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `IDENTIFICACION` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ULTIMAGESTION` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `GESTION` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `DIASMORA` int(11) DEFAULT NULL,
  `MEJORGESTION` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `FECHAMEJORGESTION` datetime DEFAULT NULL,
  `DEUDATOTAL` float(10,4) DEFAULT NULL,
  `STATUSCARTERA` enum('A','I') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `FECHAACUERDO` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `VALORACUERDO` double(10,4) DEFAULT NULL,
  `VALORPAGO` double(10,4) DEFAULT NULL,
  `FECHA_PAGO` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `TOTALACUERDO` double(10,4) DEFAULT NULL,
  `TOTALPAGO` double(10,4) DEFAULT NULL,
  `CUOTA` int(11) DEFAULT NULL,
  `TOTALCUOTAS` int(11) DEFAULT NULL,
  `DEUDAVENCIDA` float DEFAULT NULL,
  `EXTRA1` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA2` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA3` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA4` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA5` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA6` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA7` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA8` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA9` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EXTRA10` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_crm_multicanal_detalle_idx` (`IDMULTICANAL`),
  CONSTRAINT `fk_crm_multicanal_detalle` FOREIGN KEY (`IDMULTICANAL`) REFERENCES `crm_multicanal_cab` (`idmulticanal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_multicanal_marcadores`
--

DROP TABLE IF EXISTS `crm_multicanal_marcadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_multicanal_marcadores` (
  `id_marcador` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_campania` int(10) unsigned NOT NULL,
  `nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `opcion` enum('Automatica','Manual') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `numero_cola` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_grupo` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_url` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `user_creacion` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `campo_ref1` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `campo_ref2` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `campo_ref3` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_marcador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_pausas`
--

DROP TABLE IF EXISTS `crm_pausas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_pausas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `crm_pausascol` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_pausas_usuario`
--

DROP TABLE IF EXISTS `crm_pausas_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_pausas_usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned DEFAULT NULL,
  `id_pausa` int(10) unsigned DEFAULT NULL,
  `hora_inicio` datetime DEFAULT NULL,
  `hora_fin` datetime DEFAULT NULL,
  `duracion` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_tarea_cuenta_campania`
--

DROP TABLE IF EXISTS `crm_tarea_cuenta_campania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_tarea_cuenta_campania` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tarea` int(10) unsigned DEFAULT NULL,
  `id_cuenta_campania` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_tarea_gestor`
--

DROP TABLE IF EXISTS `crm_tarea_gestor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_tarea_gestor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tarea` int(10) unsigned DEFAULT NULL,
  `id_usuario` int(10) unsigned DEFAULT NULL,
  `id_cuenta_campania` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_crm_tarea_gestor_crm_tareas` (`id_tarea`),
  KEY `FK_crm_tarea_gestor_acl_usuario` (`id_usuario`),
  CONSTRAINT `FK_crm_tarea_gestor_acl_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `acl_usuario` (`id_usuario`),
  CONSTRAINT `FK_crm_tarea_gestor_crm_tareas` FOREIGN KEY (`id_tarea`) REFERENCES `crm_tareas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_tarea_objetivos`
--

DROP TABLE IF EXISTS `crm_tarea_objetivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_tarea_objetivos` (
  `id_objetivo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tarea` int(10) unsigned NOT NULL,
  `id_tipoobjetivo` int(10) unsigned NOT NULL,
  `id_parametro` int(10) unsigned DEFAULT NULL,
  `ca_valorparametro` double DEFAULT NULL,
  `valorparametro_habilitar` tinyint(1) DEFAULT NULL,
  `valorparametro_finalizar` tinyint(1) DEFAULT NULL,
  `ca_valor` double DEFAULT NULL,
  `ca_valorhabilitar` tinyint(1) DEFAULT NULL,
  `ca_valorfinalizar` tinyint(1) DEFAULT NULL,
  `ca_cantidad` int(11) DEFAULT NULL,
  `ca_cantidadhabilitar` tinyint(1) DEFAULT NULL,
  `ca_cantidadfinalizar` tinyint(1) DEFAULT NULL,
  `idgestion_principal` int(10) unsigned DEFAULT NULL,
  `idgestion_sec1` int(10) unsigned DEFAULT NULL,
  `idgestion_sec2` int(10) unsigned DEFAULT NULL,
  `tc_cantidad` int(11) DEFAULT NULL,
  `tc_cantidadhabilitar` tinyint(1) DEFAULT NULL,
  `tc_cantidadfinalizar` tinyint(1) DEFAULT NULL,
  `tc_valor` double DEFAULT NULL,
  `tc_valorhabilitar` tinyint(1) DEFAULT NULL,
  `tc_valorfinalizar` tinyint(1) DEFAULT NULL,
  `p_cantidad` int(11) DEFAULT NULL,
  `p_cantidadhabilitar` tinyint(1) DEFAULT NULL,
  `p_cantidadfinalizar` tinyint(1) DEFAULT NULL,
  `campo_ref1` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `campo_ref2` double DEFAULT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'A',
  `fecha_creacion` datetime DEFAULT NULL,
  `user_creacion` int(10) unsigned DEFAULT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `user_actualizacion` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_objetivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crm_tareas`
--

DROP TABLE IF EXISTS `crm_tareas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crm_tareas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estado` enum('A','I') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'A',
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `meta` float(10,4) DEFAULT NULL,
  `idmulticanal` int(10) unsigned NOT NULL,
  `id_campania` int(10) unsigned DEFAULT NULL,
  `Obligatorio` enum('Si','No') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_objetivo` int(11) DEFAULT NULL,
  `user_creacion` int(10) unsigned DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `user_modificacion` int(10) unsigned DEFAULT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_crm_tareas_crm_campania` (`id_campania`),
  CONSTRAINT `FK_crm_tareas_crm_campania` FOREIGN KEY (`id_campania`) REFERENCES `crm_campania` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'ps_crm'
--
/*!50003 DROP PROCEDURE IF EXISTS `crm_consulta_gestion_campania` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mvera`@`%` PROCEDURE `crm_consulta_gestion_campania`(
`id_campania` INT,
`id_usuario` INT
)
BEGIN
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT 
a.id AS idcuentacampania,
d.id AS idcuenta,
c.user_name AS asesor, 
d.nombre AS nombrecliente,
d.identificacion AS cedula,
(SELECT IFNULL(MAX(h.hora_gestion), null) FROM crm_gestion h WHERE h.id_cuentas_campania_productos = e.id) as ultimagestion,
IFNULL(f.fecha_acuerdo , '') AS acuerdo,
IFNULL(f.valor_acuerdo, 0) + 0.0 AS valoracuerdo,
IFNULL(g.fecha_pago ,'') AS pago,
IFNULL(g.valor_pago, 0) + 0.0 AS valorpago,
e.id AS idproducto,
e.cuotas_pendientes AS cuotaspendientes,
e.valor AS montoinicial,
j.texto AS estado,
e.deudatotal,
h.deudavencida,
h.extra1,
h.extra2,
h.extra3,
h.extra4,
h.extra5,
h.extra6,
h.extra7,
h.extra8,
h.extra9,
h.extra10,
h.fechamejorestado,
h.label_estado,
h.mejorestado
FROM crm_cuentas_campania a  
inner join crm_cuentas_campania_gestor b  ON (b.id_cuenta_campania = a.id)
INNER JOIN acl_usuario c  ON (c.id_usuario = b.id_usuario)
INNER JOIN crm_cuentas d ON (d.id = a.id_cuenta)
INNER JOIN crm_cuentas_campania_productos e ON (e.id_cuenta_campania = a.id) 
LEFT JOIN crm_arbol_gestion i ON (i.id = a.id_arbol_gestion_mejor_gestion)
LEFT JOIN crm_arbol_gestion_tipo_contacto j ON (j.id = i.id_tipo_contacto)
LEFT JOIN crm_cuentas_campania_acuerdos f ON (f.id_gestor = c.id_usuario AND f.id_cuenta_campania_producto = e.id AND f.fecha_creacion = (SELECT MAX(u.fecha_creacion) FROM crm_cuentas_campania_acuerdos u WHERE u.id_cuenta_campania_producto = e.id 
AND u.id_gestor = c.id_usuario)) 
LEFT JOIN crm_cuentas_campania_pagos g ON (g.id_cuenta_campania_producto = e.id AND g.id_cuenta_campania_acuerdo = f.id AND g.fecha_creacion = (SELECT MAX(p.fecha_creacion) FROM crm_cuentas_campania_pagos p WHERE p.id_cuenta_campania_producto = e.id
AND  p.id_cuenta_campania_acuerdo = f.id ))
LEFT JOIN crm_consulta_gestion_campania h on (h.idcuentacampania = a.id and h.idcuenta = d.id)
WHERE a.id_campania = id_campania AND c.id_usuario = id_usuario  AND a.estado = 'A'; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crm_consulta_gestion_campania_total` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mvera`@`%` PROCEDURE `crm_consulta_gestion_campania_total`(id_campania int,id_usuario int)
BEGIN
	select count(*) cantidad, estado, '' as texto,alerta, id_cuenta as id
from (
SELECT  max(cag.peso) peso, cag.label_estado as estado, ccc.id_cuenta, cag.alerta 
FROM crm_cuentas_campania ccc 
JOIN crm_arbol_gestion cag on (cag.id = ccc.id_arbol_gestion_mejor_gestion)
JOIN crm_cuentas_campania_gestor cccg on (cccg.id_cuenta_campania = ccc.id)
where ccc.id_campania = id_campania
and cccg.id_usuario = id_usuario
group by ccc.id ) x
group by estado
;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crm_multicanal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`mvera`@`%` PROCEDURE `crm_multicanal`(id_cuenta_campania int)
BEGIN
SELECT 
c.user_name AS asesor, 
c.id_usuario,
d.id nocuenta,
d.nombre nombrecompleto,
d.identificacion,
(SELECT concat(IFNULL(MAX(x.hora_gestion), null),' ') FROM crm_gestion x WHERE x.id_cuentas_campania_productos = e.id) as ultimagestion,
h.ultimagestion gestion,
e.edadmor diasmora,
h.mejorestado mejorgestion,
h.fechamejorestado fechamejorgestion,
e.deudatotal,
cc2.estado statuscartera,
concat(f.fecha_acuerdo,' ') fechaacuerdo,
IFNULL(f.valor_acuerdo, 0) + 0.0 AS valoracuerdo,
IFNULL(g.valor_pago, 0) + 0.0 AS valorpago,
concat(g.fecha_pago,' ') fecha_pago,
IFNULL(f.valor_acuerdo, 0) + 0.0 AS totalacuerdo,
IFNULL(g.valor_pago, 0) + 0.0 AS totalpago,
e.cuotas_pendientes as cuota,
e.cuotas_pendientes AS totalcuotas,
h.deudavencida,
h.extra1,
h.extra2,
h.extra3,
h.extra4,
h.extra5,
h.extra6,
h.extra7,
h.extra8,
h.extra9,
h.extra10
FROM crm_cuentas_campania a  
inner join crm_cuentas_campania_gestor b  ON (b.id_cuenta_campania = a.id)
INNER JOIN acl_usuario c  ON (c.id_usuario = b.id_usuario)
INNER JOIN crm_cuentas d ON (d.id = a.id_cuenta AND  d.id_campania_inicial = a.id_campania)
INNER JOIN crm_cuentas_campania_productos e ON (e.id_cuenta_campania = a.id) 
LEFT JOIN crm_arbol_gestion i ON (i.id = a.id_arbol_gestion_mejor_gestion)
LEFT JOIN crm_arbol_gestion_tipo_contacto j ON (j.id = i.id_tipo_contacto)
LEFT JOIN crm_cuentas_campania_acuerdos f ON (f.id_gestor = c.id_usuario AND f.id_cuenta_campania_producto = e.id AND f.fecha_creacion = (SELECT MAX(u.fecha_creacion) FROM crm_cuentas_campania_acuerdos u WHERE u.id_cuenta_campania_producto = e.id 
AND u.id_gestor = c.id_usuario)) 
LEFT JOIN crm_cuentas_campania_pagos g ON (g.id_cuenta_campania_producto = e.id AND g.id_cuenta_campania_acuerdo = f.id AND g.fecha_creacion = (SELECT MAX(p.fecha_creacion) FROM crm_cuentas_campania_pagos p WHERE p.id_cuenta_campania_producto = e.id
AND  p.id_cuenta_campania_acuerdo = f.id ))
LEFT JOIN crm_consulta_gestion_campania h on (h.idcuentacampania = a.id and h.idcuenta = d.id)
left join crm_campania cc on (cc.id = a.id_campania)
left join crm_cartera cc2 on (cc2.id = cc.id_cartera)
WHERE a.id_campania = id_cuenta_campania   AND a.estado = 'A';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-18 13:20:31
