-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ps_crm
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.21-MariaDB-0+deb11u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `acl_modulo_acciones`
--

LOCK TABLES `acl_modulo_acciones` WRITE;
/*!40000 ALTER TABLE `acl_modulo_acciones` DISABLE KEYS */;
INSERT INTO `acl_modulo_acciones` VALUES (2,1,1,'NO','system','2024-05-06 00:00:00',NULL,NULL),(3,2,1,'NO','system','2024-05-06 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `acl_modulo_acciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_accesos_roles`
--

LOCK TABLES `acl_accesos_roles` WRITE;
/*!40000 ALTER TABLE `acl_accesos_roles` DISABLE KEYS */;
INSERT INTO `acl_accesos_roles` VALUES (1,1,1,'NO','system','2024-05-06 00:00:00',NULL,NULL),(2,1,2,'NO','system','2024-05-06 00:00:00',NULL,NULL),(3,1,3,'NO','system','2024-05-06 00:00:00',NULL,NULL),(4,1,9,'NO','system','2024-05-06 00:00:00',NULL,NULL),(5,1,4,'NO','system','2024-05-06 00:00:00',NULL,NULL),(6,1,8,'NO','system','2024-05-06 00:00:00',NULL,NULL),(7,1,6,'NO','system','2024-05-06 00:00:00',NULL,NULL),(8,1,7,'NO','system','2024-05-06 00:00:00',NULL,NULL),(9,1,5,'NO','system','2024-05-06 00:00:00',NULL,NULL),(10,1,11,'NO','system','2024-05-06 00:00:00',NULL,NULL),(11,1,10,'NO','system','2024-05-06 00:00:00',NULL,NULL),(12,1,12,'NO','admin','2024-05-23 11:28:32',NULL,NULL),(13,1,13,'NO','admin','2024-05-23 11:28:47',NULL,NULL),(14,1,14,'NO','admin','2024-05-23 13:19:13',NULL,NULL),(15,1,18,'NO','admin','2024-05-23 13:19:28',NULL,NULL),(16,1,17,'NO','admin','2024-05-23 13:19:35',NULL,NULL),(17,1,16,'NO','admin','2024-05-23 14:17:05',NULL,NULL),(18,1,19,'NO','admin','2024-05-23 14:43:51',NULL,NULL),(19,1,20,'NO','admin','2024-05-23 14:44:01',NULL,NULL),(20,1,21,'NO','admin','2024-05-23 14:44:16',NULL,NULL),(21,1,22,'NO','admin','2024-05-23 14:44:27',NULL,NULL),(22,1,23,'NO','admin','2024-05-23 14:44:39',NULL,NULL),(23,1,24,'NO','admin','2024-05-23 14:44:46',NULL,NULL),(24,1,25,'NO','admin','2024-05-23 14:45:10',NULL,NULL),(25,1,26,'NO','admin','2024-05-23 15:20:48',NULL,NULL),(26,1,27,'NO','admin','2024-05-23 15:20:58',NULL,NULL),(27,1,31,'NO','admin','2024-05-23 15:21:11',NULL,NULL),(28,1,29,'NO','admin','2024-05-23 15:21:21',NULL,NULL),(29,1,30,'NO','admin','2024-05-23 15:21:28',NULL,NULL),(30,1,28,'NO','admin','2024-05-23 15:21:43',NULL,NULL),(31,1,32,'NO','admin','2024-05-23 15:22:40',NULL,NULL),(32,1,34,'NO','admin','2024-05-23 15:27:50',NULL,NULL),(33,1,35,'NO','admin','2024-05-23 15:28:02',NULL,NULL),(34,1,36,'NO','admin','2024-05-23 15:28:10',NULL,NULL),(35,1,37,'NO','admin','2024-05-23 15:30:54',NULL,NULL);
/*!40000 ALTER TABLE `acl_accesos_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_distrito`
--

LOCK TABLES `acl_distrito` WRITE;
/*!40000 ALTER TABLE `acl_distrito` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_distrito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_estados`
--

LOCK TABLES `acl_estados` WRITE;
/*!40000 ALTER TABLE `acl_estados` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_depto`
--

LOCK TABLES `acl_depto` WRITE;
/*!40000 ALTER TABLE `acl_depto` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_depto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_roles`
--

LOCK TABLES `acl_roles` WRITE;
/*!40000 ALTER TABLE `acl_roles` DISABLE KEYS */;
INSERT INTO `acl_roles` VALUES (1,'Administrador','NO',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_parametros`
--

LOCK TABLES `acl_parametros` WRITE;
/*!40000 ALTER TABLE `acl_parametros` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_tipo_info`
--

LOCK TABLES `acl_tipo_info` WRITE;
/*!40000 ALTER TABLE `acl_tipo_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_tipo_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_multicanal_estados`
--

LOCK TABLES `acl_multicanal_estados` WRITE;
/*!40000 ALTER TABLE `acl_multicanal_estados` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_multicanal_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_region`
--

LOCK TABLES `acl_region` WRITE;
/*!40000 ALTER TABLE `acl_region` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_lista_asesoresasig`
--

LOCK TABLES `acl_lista_asesoresasig` WRITE;
/*!40000 ALTER TABLE `acl_lista_asesoresasig` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_lista_asesoresasig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_plantilla`
--

LOCK TABLES `acl_plantilla` WRITE;
/*!40000 ALTER TABLE `acl_plantilla` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_plantilla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_usuario`
--

LOCK TABLES `acl_usuario` WRITE;
/*!40000 ALTER TABLE `acl_usuario` DISABLE KEYS */;
INSERT INTO `acl_usuario` VALUES (1,1,'admin',NULL,'0924835481','admin','admin','mvera@palosanto.com','admin sistema','Sistemas','593979167529','SIP/101','6a308796720629b3794907926e0612726ea9c494','NO','7a5210c173ea40c03205a5de7dcd4cb0','system','2024-05-06 00:00:00',NULL,'2024-06-13 11:10:05'),(2,1,'rlloacana',NULL,'0924746480','Ronald','Lloacana','lloacana@gmail.com','sistemas','Sistemas','593989531736','SIP/101','6a308796720629b3794907926e0612726ea9c494','NO','bb1be9623b2446f7fd4c294fe9af5c17','jcontreras','2024-05-31 16:04:21',NULL,NULL);
/*!40000 ALTER TABLE `acl_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_efecto_acuerdo`
--

LOCK TABLES `acl_efecto_acuerdo` WRITE;
/*!40000 ALTER TABLE `acl_efecto_acuerdo` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_efecto_acuerdo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_tipo_contacto`
--

LOCK TABLES `acl_tipo_contacto` WRITE;
/*!40000 ALTER TABLE `acl_tipo_contacto` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_tipo_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_relacion`
--

LOCK TABLES `acl_relacion` WRITE;
/*!40000 ALTER TABLE `acl_relacion` DISABLE KEYS */;
INSERT INTO `acl_relacion` VALUES (1,'Padre'),(2,'ABUELO(A)'),(3,'CUNADO(A)'),(4,'PRIMO(A)'),(5,'TIO(A)'),(6,'HERMANO(A)'),(7,'HIJO(A)'),(8,'MADRE'),(9,'SUEGRO(A)'),(10,'SOBRINO(A)'),(11,'AMIGO(A)'),(12,'No definido'),(13,'EX-ESPOSO(A)'),(14,'NOVIO(A)'),(15,'ESPOSO(A)'),(16,'VECINO(A)'),(17,'NIETO(A)'),(18,'NUERA'),(19,'YERNO'),(20,'MADRINA'),(21,'PADRINO'),(25,'PADRASTRO');
/*!40000 ALTER TABLE `acl_relacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_tipo_objetivo`
--

LOCK TABLES `acl_tipo_objetivo` WRITE;
/*!40000 ALTER TABLE `acl_tipo_objetivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_tipo_objetivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_zona`
--

LOCK TABLES `acl_zona` WRITE;
/*!40000 ALTER TABLE `acl_zona` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_zona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_ciudad`
--

LOCK TABLES `acl_ciudad` WRITE;
/*!40000 ALTER TABLE `acl_ciudad` DISABLE KEYS */;
INSERT INTO `acl_ciudad` VALUES (1,NULL,'GUAYAQUIL                '),(2,NULL,'MONTECRISTI              '),(3,NULL,'QUININDE                 '),(4,NULL,'EL EMPALME COD 4         '),(5,NULL,'DURAN                    '),(6,NULL,'MANTA                    '),(7,NULL,'PORTOVIEJO               '),(8,NULL,'DAULE                    '),(9,NULL,'ESMERALDA                '),(10,NULL,'EL GUABO                 '),(11,NULL,'SANTA ISABELA GALAPAGOS  '),(12,NULL,'TOSAGUA                  '),(13,NULL,'CHONE                    '),(14,NULL,'YAGUACHI                 '),(15,NULL,'VENTANAS                 '),(16,NULL,'SANTA ROSA               '),(17,NULL,'VINCES                   '),(18,NULL,'QUEVEDO                  '),(19,NULL,'MARCELINO MARIDUENA      '),(20,NULL,'LOMAS DE SARGENTILLO     '),(21,NULL,'JIPIJAPA                 '),(22,NULL,'MACHALA                  '),(23,NULL,'CERECITA                 '),(24,NULL,'PEDERNALES               '),(25,NULL,'MILAGRO                  '),(26,NULL,'BABAHOYO COD 5           '),(27,NULL,'SALINAS PROV STA ELENA   '),(28,NULL,'EL CARMEN-MANABI         '),(29,NULL,'BALAO                    '),(30,NULL,'STA. LUCIA               '),(31,NULL,'NOBOL                    '),(32,NULL,'PLAYAS                   '),(33,NULL,'STA. ANA                 '),(34,NULL,'BUENA FE                 '),(35,NULL,'SAN VICENTE              '),(36,NULL,'PUERTO AYORA GALAPAGOS   '),(37,NULL,'LA LIBERTAD              '),(38,NULL,'PUEBLOVIEJO              '),(39,NULL,'SANTA ELENA              '),(40,NULL,'PASCUALES                '),(41,NULL,'SAMBORONDON              '),(42,NULL,'CUENCA COD 3             '),(43,NULL,'EL TRIUNFO COD 4         '),(44,NULL,'ROCAFUERTE               '),(45,NULL,'JUJAN                    '),(46,NULL,'BALTRA GALAPAGOS         '),(47,NULL,'HUAQUILLAS               '),(48,NULL,'24 DE MAYO MANABI        '),(49,NULL,'LA MANA                  '),(50,NULL,'BAHIA DE CARAQUEZ        '),(51,NULL,'CALCETA                  '),(52,NULL,'PALENQUE                 '),(53,NULL,'CHONGON                  '),(54,NULL,'VALENCIA                 '),(55,NULL,'SAN LORENZO              '),(56,NULL,'NARANJAL COD 4           '),(57,NULL,'BUCAY                    '),(58,NULL,'POSORJA                  '),(59,NULL,'PROGRESO                 '),(60,NULL,'NARANJITO                '),(61,NULL,'SIMON BOLIVAR            '),(62,NULL,'SALITRE                  '),(63,NULL,'LA TRONCAL CAÑAR         '),(64,NULL,'P.CARBO                  '),(65,NULL,'SANTA CRUZ GALAPAGOS     '),(66,NULL,'PALESTINA                '),(67,NULL,'JUNIN                    '),(68,NULL,'JARAMIJO                 '),(69,NULL,'PTO BOLIVAR              '),(70,NULL,'BALZAR                   '),(71,NULL,'PASAJE                   '),(72,NULL,'ARENILLAS                '),(73,NULL,'BABAHOYO COD 4           '),(74,NULL,'CATARAMA COD 5           '),(75,NULL,'NARANJAL COD 7           '),(76,NULL,'MONTALVO                 '),(77,NULL,'SAN CRISTOBAL GALAPAGOS  '),(78,NULL,'PAJAN                    '),(79,NULL,'JAMA                     '),(80,NULL,'PICHINCHA-MANABI         '),(81,NULL,'QUITO                    '),(82,NULL,'FLAVIO ALFARO            '),(83,NULL,'TRES POSTES              '),(84,NULL,'MOCACHE                  '),(85,NULL,'PUERTO LOPEZ             '),(86,NULL,'ANCON                    '),(87,NULL,'COLIMES COD 4            '),(88,NULL,'LOJA                     '),(89,NULL,'PTO.BOLIVAR              '),(90,NULL,'MANGLARALTO              '),(91,NULL,'JUAN MONTALVO COD 5      '),(92,NULL,'CALUMA                   '),(93,NULL,'QUINSALOMA               '),(94,NULL,'URDANETA                 '),(95,NULL,'PINAS                    '),(96,NULL,'SUCRE MANABI             '),(97,NULL,'ISIDRO AYORA             '),(98,NULL,'ISLA PUNA                '),(99,NULL,'GALAPAGOS/SANTA CRUZ     '),(100,NULL,'ATACAMES                 '),(101,NULL,'MONTANITA                '),(102,NULL,'LA TRONCAL GUAYAS        '),(103,NULL,'CRUCITA                  '),(130,NULL,'PASTAZA                  '),(131,NULL,'SANGOLQUI  PICHINCHA     '),(132,NULL,'MONTUFAR  CARCHI         '),(133,NULL,'TUMBACO                  '),(134,NULL,'IBARRA                   '),(135,NULL,'STO.DOMINGO              '),(136,NULL,'AGUARICO ORELLANA        '),(137,NULL,'LATACUNGA                '),(138,NULL,'OTAVALO                  '),(139,NULL,'VALLE DE LOS CHILLOS     '),(140,NULL,'CUENCA COD 7             '),(141,NULL,'CAYAMBE                  '),(142,NULL,'AMAGUAÑA PARROQ QUITO    '),(143,NULL,'CUMANDA (CHIMBORAZO)     '),(144,NULL,'PIFO                     '),(145,NULL,'RIOBAMBA                 '),(146,NULL,'GUAYLLABAMBA             '),(147,NULL,'TENA  NAPO               '),(148,NULL,'GUALACEO                 '),(149,NULL,'ARCHIDONA  NAPO          '),(150,NULL,'RUMIÑAHUI  PICHINCHA     '),(151,NULL,'CATAMAYO                 '),(152,NULL,'PUYO                     '),(153,NULL,'YARUQUI  PICHINCHA       '),(154,NULL,'SUCUMBIOS                '),(155,NULL,'CUMBAYA                  '),(156,NULL,'NUEVA LOJA               '),(157,NULL,'GUANO  CHIMBORAZO        '),(158,NULL,'LAGO AGRIO               '),(159,NULL,'LASSO COTOPAXI           '),(160,NULL,'AMBATO                   '),(161,NULL,'CANTON CAÑAR             '),(162,NULL,'PUEMBO  PICHINCHA        '),(163,NULL,'PEDRO MONCAYO            '),(164,NULL,'ORELLANA                 '),(165,NULL,'ALAMOR   LOJA            '),(166,NULL,'COCA  ORELLANA           '),(167,NULL,'LA CONCORDIA ESMERALDAS  '),(168,NULL,'LA CONCORDIA PICHINCHA   '),(169,NULL,'MACHACHI                 '),(170,NULL,'TULCAN                   '),(171,NULL,'ALAUSI                   '),(172,NULL,'SAQUISILÍ                '),(173,NULL,'SAN GABRIEL              '),(174,NULL,'EL COCA                  '),(175,NULL,'P V MALDONADO  PICHINCHA '),(176,NULL,'TABACUNDO                '),(177,NULL,'PONCE ENRIQUEZ           '),(178,NULL,'MACAS                    '),(179,NULL,'AZOGUEZ                  '),(180,NULL,'GUARANDA                 '),(181,NULL,'CALACALI                 '),(182,NULL,'BOLIVAR COD 3            '),(183,NULL,'PUJILI COTOPAXI          ');
/*!40000 ALTER TABLE `acl_ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_modulos`
--

LOCK TABLES `acl_modulos` WRITE;
/*!40000 ALTER TABLE `acl_modulos` DISABLE KEYS */;
INSERT INTO `acl_modulos` VALUES (1,'G','Gestionar','fa fa-tasks','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(2,'Gn','Gestion Normal','','NO','/gestionar/gestion-normal',1,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(3,'Gt','Tareas',NULL,'NO','/gestionar/tareas',1,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(4,'M','Mantenimiento','fa fa-wrench','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(5,'Mp','Perfil',NULL,'NO','/mantenimientos/perfil',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(6,'Mu','Mantenimiento Usuarios',NULL,'NO','/mantenimientos/usuario',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(7,'Ma','Acciones',NULL,'NO','/mantenimientos/acciones',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(8,'Mtm','Tipos Modulos',NULL,'NO','/mantenimientos/tipos-modulos',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(9,'Mm','Modulos','','NO','/mantenimientos/modulos',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(10,'Mra','Roles Acceso',NULL,'NO','/mantenimientos/roles-accesos',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(11,'Mpl','Plantillas',NULL,'NO','/mantenimientos/plantilla',4,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(12,'C','Configuracion','fa fa-cog','Si',NULL,NULL,'',NULL,'NO',NULL,NULL,NULL,NULL),(13,'Cag','Arbol Gestion',NULL,'NO','/configuracion/arbol-gestion',12,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(14,'S','Supervisor','fa fa-user-circle-o','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(15,'Ss','Modulo Supervisor',NULL,'NO','/supervisor',14,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(16,'Scd','Cargar Dato',NULL,'NO','/supervisor/carga-dato',14,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(17,'Se','Exportar',NULL,'NO','/supervisor/exportar',14,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(18,'Si','Importar',NULL,'NO','/supervisor/importar',14,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(19,'Mult','Multicanal','fa fa-cloud-upload','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(20,'MulC','Contenedor',NULL,'NO','/multicanal/contenedor',19,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(21,'MulP','Plantilla',NULL,'NO','/multicanal/plantilla',19,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(22,'A','Asignacion','fa fa-exchange','SI','',NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(23,'Arg','Remover Asignacion',NULL,'NO','/asignacion/remover-asignacion',22,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(24,'Aa','Asignacion Agentes',NULL,'NO','/asignacion/asignacion',22,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(25,'Ace','Cambio Estado',NULL,'NO','/asignacion/cambiar-estado',22,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(26,'Met','Metricas','fa fa-line-chart','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(27,'MetI','Indicadores',NULL,'NO','/metricas/indicadores',26,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(28,'MetM','Metricas Conciliacion',NULL,'NO','/metricas/matriz-conciliacion',26,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(29,'Ca','Caja','fa fa-money','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(30,'CaC','Modulo Caja',NULL,'NO','/caja/caja',29,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(31,'CaMc','Matriz Conciliacion',NULL,'NO','/caja/matriz-conciliacion',29,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(32,'U','Usuarios','fa fa-users','SI',NULL,NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(34,'Uli','Listado',NULL,'NO','/usuarios/usuario/usuario',32,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(35,'Up','Perfiles',NULL,'NO','/usuarios/perfil/perfil',32,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(36,'D','Dashboard','fa fa-tachometer','SI','',NULL,NULL,NULL,'NO',NULL,NULL,NULL,NULL),(37,'Dda','Dashboard  M','fa','NO','/dashboard',36,'SI',1,'NO',NULL,NULL,'admin','2024-05-23 15:54:30');
/*!40000 ALTER TABLE `acl_modulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_parametro_objetivo`
--

LOCK TABLES `acl_parametro_objetivo` WRITE;
/*!40000 ALTER TABLE `acl_parametro_objetivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_parametro_objetivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_area`
--

LOCK TABLES `acl_area` WRITE;
/*!40000 ALTER TABLE `acl_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acl_acciones`
--

LOCK TABLES `acl_acciones` WRITE;
/*!40000 ALTER TABLE `acl_acciones` DISABLE KEYS */;
INSERT INTO `acl_acciones` VALUES (1,'A','Acceso',NULL,'NO','system','2024-05-06 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `acl_acciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-18 13:22:42
